Google Analytics-en oinarritutako ecommerce aplikazioen analisi estatistikorako tresna.
Google Analytics-eko kontu bat izate beharrezkoa da Google developer programan erregistratua dagoena eta Analytics API-ra sarbidea duena.
bd karpetan eskuragarri dagoen datu base bat erabiltzen du. 

SOFTWAREA HAU INONGO BERMERIK GABE ZABALTZEN DA. ESKARITARAKO EDOZEIN GERTAEREN AURREAN , KALTEAK EDO BESTELAKOAK, EGILEAK EDO COPYRIGHT-EUSLEAK EZ DIRA ERANTZULE IZANGO.
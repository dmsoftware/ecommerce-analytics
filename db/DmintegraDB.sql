USE [DMI_DMINTEGRA]
GO
/****** Object:  Table [dbo].[LOC_CAMPANAS_COMENTARIOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_CAMPANAS_COMENTARIOS](
	[LOCCO_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCCO_SUCAMPANA] [int] NOT NULL,
	[LOCCO_FECHA_USUARIO] [date] NOT NULL,
	[LOCCO_COMENTARIO] [varchar](max) NOT NULL,
	[LOCCO_USUARIO] [varchar](50) NULL,
	[LOCCO_FECHA_ALTA] [datetime] NULL,
	[LOCCO_FECHA_MODIFICACION] [datetime] NULL,
 CONSTRAINT [PK_LOC_CAMPANAS_COMENTARIOS] PRIMARY KEY CLUSTERED 
(
	[LOCCO_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_CAMPANAS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_CAMPANAS](
	[LOCC_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCC_ANALYTICS_ID_PROPIEDAD] [varchar](25) NULL,
	[LOCC_CAMPAING] [nvarchar](100) NULL,
	[LOCC_MEDIUM] [nvarchar](50) NULL,
	[LOCC_SOURCE] [nvarchar](50) NULL,
	[LOCC_DESCRIPCION] [ntext] NULL,
	[LOCC_FECHA_PRIMERAVISITA] [datetime] NULL,
	[LOCC_FECHA_ULTIMAVISITA] [datetime] NULL,
	[LOCC_FECHA_ENVIOS_INICIO] [datetime] NULL,
	[LOCC_FECHA_ENVIOS_FIN] [datetime] NULL,
	[LOCC_ENVIOS_EMAILS] [varchar](255) NULL,
	[LOCC_URLDESTINO] [varchar](255) NULL,
	[LOCC_FECHA_ALTA] [datetime] NULL,
	[LOCC_FECHA_MODIFICACION] [datetime] NULL,
	[LOCC_HISTORICO] [bit] NULL,
	[LOCC_ACTIVARSEGUIMIENTO] [bit] NULL,
 CONSTRAINT [PK_LOC_CAMPANAS] PRIMARY KEY CLUSTERED 
(
	[LOCC_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_CACHE]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_CACHE](
	[LOCCA_ID] [int] IDENTITY(1,1) NOT NULL,
	[LOCCA_ID_VISTA] [varchar](25) NULL,
	[LOCCA_START_DATE] [date] NULL,
	[LOCCA_END_DATE] [date] NULL,
	[LOCCA_METRICS] [varchar](255) NULL,
	[LOCCA_OPT_PARAMS] [varchar](max) NULL,
	[LOCCA_TIPO] [varchar](4) NULL,
	[LOCCA_DATA] [varchar](max) NULL,
	[LOCCA_FECHA_CREACION] [datetime] NULL,
	[LOCCA_ULTIMO_USO] [datetime] NULL,
	[LOCCA_CONTADOR] [int] NULL,
	[LOCCA_DURACION] [float] NULL,
	[LOCCA_FECHA_CADUCIDAD] [datetime] NULL,
	[LOCCA_URL_REFERENCIA] [varchar](255) NULL,
	[LOCCA_CONTADOR_LLAMADAS] [int] NULL,
 CONSTRAINT [PK_LOC_CACHE] PRIMARY KEY CLUSTERED 
(
	[LOCCA_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_BUSCADORESAUX]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_BUSCADORESAUX](
	[LOCBA_DOMINIO] [varchar](50) NULL,
	[LOCBA_EJEMPLO] [varchar](50) NULL,
	[LOCBA_COINCIDENCIAS] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'虪ㆤ蚱䠛ᄔ䡋㘈' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCBA_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCBA_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORESAUX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'䢉膱꤆᲋ঋ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCBA_EJEMPLO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCBA_EJEMPLO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORESAUX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_EJEMPLO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'櫉�仠꒩ࡽﲒ鸐' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCBA_COINCIDENCIAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCBA_COINCIDENCIAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORESAUX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX', @level2type=N'COLUMN',@level2name=N'LOCBA_COINCIDENCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'16/11/2011 12:56:13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'26/12/2011 9:56:36' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabla auxiliar para que el script acumule contadores de los buscadores desconocidas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_BUSCADORESAUX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORESAUX'
GO
/****** Object:  Table [dbo].[LOC_BUSCADORES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_BUSCADORES](
	[LOCB_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCB_NOMBRE] [varchar](50) NULL,
	[LOCB_PARAMREFERER] [varchar](50) NULL,
	[LOCB_PARAMTIPOPAGINA] [varchar](50) NULL,
	[LOCB_PARAMPAGINA] [varchar](50) NULL,
	[LOCB_SUMARPAGINA] [varchar](50) NULL,
 CONSTRAINT [aaaaaLOC_BUSCADORES_PK] PRIMARY KEY NONCLUSTERED 
(
	[LOCB_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1920' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ᵋꔉ譺䑽檝詟龠ᄃ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCB_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCB_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del dominio por el que buscaremos coincidencia (Ejem.:  ".google."'')' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCB_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCB_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2940' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del parametro donde se almacena el REFERER' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCB_PARAMREFERER' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCB_PARAMREFERER' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMREFERER'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-->No tiene control de paginación ; 1--> Numero Paginas ; 2--> Nuemo Registros; ( Si es registro, habra que dividirlo entre 10 y sacar la parte entera + 1.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCB_PARAMTIPOPAGINA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCB_PARAMTIPOPAGINA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMTIPOPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2520' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del parametro donde se guarda la pagina del buscador. Si es vacio, no tiene control de paginación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCB_PARAMPAGINA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCB_PARAMPAGINA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_PARAMPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3450' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Si PARAMTIPOPAGINA es 1, en algunos pasos en vez de almacenarse la pagina real, se almacena uno menos,(en base 0) por que en esta columna, almacenamos el valor que hay que sumarle. Si es vacio no hay que hacer nada.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCB_SUMARPAGINA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCB_SUMARPAGINA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_BUSCADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES', @level2type=N'COLUMN',@level2name=N'LOCB_SUMARPAGINA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'18/10/2011 13:19:44' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'19/10/2011 12:00:29' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_BUSCADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'44' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_BUSCADORES'
GO
/****** Object:  Table [dbo].[LOC_AUX_OBTENERPOSICION]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOC_AUX_OBTENERPOSICION](
	[LOCA_ULTIMAFECHAHORA] [datetime] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCA_ULTIMAFECHAHORA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCA_ULTIMAFECHAHORA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_AUX_OBTENERPOSICION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION', @level2type=N'COLUMN',@level2name=N'LOCA_ULTIMAFECHAHORA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'28/03/2010 21:43:40' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'28/03/2010 21:43:40' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_AUX_OBTENERPOSICION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_AUX_OBTENERPOSICION'
GO
/****** Object:  Table [dbo].[ISP_DOMINIOS_NIC]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ISP_DOMINIOS_NIC](
	[DOMNIC_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[DOMNIC_DOMINIO] [varchar](255) NULL,
	[DOMNIC_COM] [smallint] NULL,
	[DOMNIC_NET] [smallint] NULL,
	[DOMNIC_ORG] [smallint] NULL,
	[DOMNIC_INFO] [smallint] NULL,
	[DOMNIC_ES] [smallint] NULL,
	[DOMNIC_BIZ] [smallint] NULL,
	[DOMNIC_EU] [smallint] NULL,
	[DOMNIC_MOBI] [smallint] NULL,
	[DOMNIC_WS] [smallint] NULL,
	[DOMNIC_FECHA] [datetime] NULL,
 CONSTRAINT [aaaaaISP_DOMINIOS_NIC_PK] PRIMARY KEY NONCLUSTERED 
(
	[DOMNIC_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ⴛ浕❈䃄ន챚䑾㳭' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sin el www' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'┮䤑艍䈰熉ᠠᮨ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_COM' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_COM' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_COM'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'襸�蟼䂗⢭驾摨啵' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_NET' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_NET' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'챚塥ݼ䚮삟䪠竔瞸' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_ORG' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_ORG' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ORG'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'伜Ṇ耑丮趟䃐ς蟳' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_INFO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_INFO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'่䥯ꪡ妟Ⰷŷ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_ES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_ES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_ES'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'뱲炕竨䲽ᒶ褑룻䨖' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_BIZ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_BIZ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_BIZ'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'흹팪밣䧡䲇ꅲ�ፏ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_EU' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_EU' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_EU'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'馶喛方䨙徭㡘긇' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_MOBI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_MOBI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_MOBI'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'㡒ꪑ鮃䄄ঁ剀푤' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_WS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_WS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_WS'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'듳彻瀜䀏䚍蚻灏' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOMNIC_FECHA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOMNIC_FECHA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC', @level2type=N'COLUMN',@level2name=N'DOMNIC_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'02/10/2007 10:14:44' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'02/10/2007 10:31:56' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ISP_DOMINIOS_NIC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'583' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS_NIC'
GO
/****** Object:  Table [dbo].[ISP_DOMINIOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ISP_DOMINIOS](
	[DOM_CODIGO] [int] NOT NULL,
	[DOM_DOMINIO] [varchar](100) NULL,
	[DOM_SUCLIENTE] [int] NULL,
	[DOM_PUBLICARWEB] [bit] NOT NULL,
	[DOM_SUSECTOR1] [int] NULL,
	[DOM_SUSECTOR2] [int] NULL,
	[DOM_SUSECTOR3] [int] NULL,
	[DOM_SUPRODUCTOWEB1] [int] NULL,
	[DOM_SUPRODUCTOWEB2] [int] NULL,
	[DOM_SUPRODUCTOWEB3] [int] NULL,
	[DOM_NPORTALES] [bit] NULL,
	[DOM_REDIRECCIONDNS] [varchar](255) NULL,
	[DOM_ESDMINTEGRA] [bit] NULL,
 CONSTRAINT [aaaaaISP_DOMINIOS_PK] PRIMARY KEY NONCLUSTERED 
(
	[DOM_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1500' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2340' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ꎒ턧엶䛎㦓ꄹᱞ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1890' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'יﺚꝺ䇿螿䰨箯�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_SUCLIENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_SUCLIENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TRUE - SE PUBLICA EN LA SECCIÓN DE PROYECTOS/CLIENTES EN WWW.DMACROWEB.COM' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_PUBLICARWEB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_PUBLICARWEB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_PUBLICARWEB'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clasificación por sectores de este trabajo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_SUSECTOR1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_SUSECTOR1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR1'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clasificación por sectores de este trabajo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_SUSECTOR2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_SUSECTOR2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR2'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clasificación por sectores de este trabajo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_SUSECTOR3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_SUSECTOR3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUSECTOR3'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clasificación por producto de este trabajo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_SUPRODUCTOWEB1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_SUPRODUCTOWEB1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB1'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clasificación por producto de este trabajo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_SUPRODUCTOWEB2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_SUPRODUCTOWEB2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB2'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clasificación por producto de este trabajo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'DOM_SUPRODUCTOWEB3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'DOM_SUPRODUCTOWEB3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS', @level2type=N'COLUMN',@level2name=N'DOM_SUPRODUCTOWEB3'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'12/12/2007 9:07:09' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'07/09/2009 18:11:44' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ISP_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'989' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISP_DOMINIOS'
GO
/****** Object:  Table [dbo].[IDIOMAS_DESCRIPCIONES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IDIOMAS_DESCRIPCIONES](
	[IDID_SUIDIOMA] [nvarchar](2) NOT NULL,
	[IDID_TABLA] [nvarchar](50) NOT NULL,
	[IDID_CAMPO] [nvarchar](50) NOT NULL,
	[IDID_INDICE] [int] NOT NULL,
	[IDID_DESCRIPCION] [ntext] NULL,
 CONSTRAINT [PK_IDIOMAS_DESCRIPCIONES] PRIMARY KEY CLUSTERED 
(
	[IDID_SUIDIOMA] ASC,
	[IDID_TABLA] ASC,
	[IDID_CAMPO] ASC,
	[IDID_INDICE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ext_translations]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ext_translations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[locale] [nvarchar](8) NOT NULL,
	[object_class] [nvarchar](255) NOT NULL,
	[field] [nvarchar](32) NOT NULL,
	[foreign_key] [nvarchar](64) NOT NULL,
	[content] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ESTADOS_ASISTENCIA]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ESTADOS_ASISTENCIA](
	[ESTA_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[ESTA_DESCRIPCION] [varchar](50) NULL,
 CONSTRAINT [aaaaaESTADOS_ASISTENCIA_PK] PRIMARY KEY NONCLUSTERED 
(
	[ESTA_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'㿶䁏䲩瞷샨�淚' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ESTA_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ESTA_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ESTADOS_ASISTENCIA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1800' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'Ȕ땈䓴犪뻦䫯︉' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ESTA_DESCRIPCION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ESTA_DESCRIPCION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ESTADOS_ASISTENCIA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA', @level2type=N'COLUMN',@level2name=N'ESTA_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'28/02/2005 9:57:00' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'28/02/2005 9:57:41' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ESTADOS_ASISTENCIA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADOS_ASISTENCIA'
GO
/****** Object:  Table [dbo].[ESTADO_CLIENTES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ESTADO_CLIENTES](
	[ESCL_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[ESCL_NOMBRE] [varchar](50) NULL,
 CONSTRAINT [aaaaaESTADO_CLIENTES_PK] PRIMARY KEY NONCLUSTERED 
(
	[ESCL_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ESCL_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ESCL_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ESTADO_CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3150' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ESCL_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ESCL_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'ESTADO_CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES', @level2type=N'COLUMN',@level2name=N'ESCL_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'31/01/2005 10:08:59' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'31/01/2005 10:12:18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ESTADO_CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ESTADO_CLIENTES'
GO
/****** Object:  Table [dbo].[ENV_EMAILS_PROCESOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ENV_EMAILS_PROCESOS](
	[ENVEP_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[ENVEP_IDENTIFICACION] [nchar](255) NULL,
	[ENVEP_ID_VISTA] [int] NULL,
	[ENVEP_IDIOMA] [char](2) NULL,
	[ENVEP_ASUNTO] [nchar](150) NULL,
	[ENVEP_CUERPO] [varchar](max) NULL,
	[ENVEP_PROCESO_IMAGEN] [varchar](255) NULL,
	[ENVEP_PROCESO_ADJUNTO1] [varchar](255) NULL,
	[ENVEP_PROCESO_ADJUNTO2] [varchar](255) NULL,
	[ENVEP_PROCESO_IMAGEN_OK] [varchar](255) NULL,
	[ENVEP_PROCESO_ERROR] [varchar](255) NULL,
	[ENVEP_PROCESO_ADJUNTO1_OK] [bit] NULL,
	[ENVEP_PROCESO_ADJUNTO2_OK] [bit] NULL,
	[ENVEP_PROCESO_PDF] [varchar](255) NULL,
 CONSTRAINT [PK_ENV_EMAILS_PROCESOS] PRIMARY KEY CLUSTERED 
(
	[ENVEP_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ENV_EMAILS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ENV_EMAILS](
	[ENVE_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[ENVE_USUARIO] [varchar](50) NULL,
	[ENVE_TIPO] [varchar](10) NULL,
	[ENVE_SU_EMAIL_PROCESO] [int] NULL,
	[ENVE_FECHA_HORA_ALTA] [datetime] NULL,
	[ENVE_FECHA_HORA_ENVIO] [datetime] NULL,
	[ENVE_FECHA_HORA_LEIDO] [datetime] NULL,
 CONSTRAINT [PK_ENV_EMAILS] PRIMARY KEY CLUSTERED 
(
	[ENVE_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CONF_CONFIGURACIONES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CONF_CONFIGURACIONES](
	[CONF_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[CONF_VERSION_TRACK_GA] [varchar](25) NULL,
	[CONF_VERSION_COOKIES] [varchar](25) NULL,
 CONSTRAINT [PK_CONF_CONFIGURACIONES] PRIMARY KEY CLUSTERED 
(
	[CONF_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CLIENTES_PRODUCTOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CLIENTES_PRODUCTOS](
	[CLIPRO_CODIGO] [int] NOT NULL,
	[CLIPRO_SUCLIENTE] [int] NULL,
	[CLIPRO_SUPRODUCTO] [int] NULL,
	[CLIPRO_FECHAALTA] [datetime] NULL,
	[CLIPRO_COMENTARIOS] [ntext] NULL,
	[CLIPRO_DOMINIOPRUEBA] [varchar](50) NULL,
	[CLIPRO_DOMINIOEXPLOTACION] [varchar](255) NULL,
	[CLIPRO_CARPETAINTERNA] [varchar](100) NULL,
	[CLIPRO_HOSTFTPPRUEBA] [varchar](50) NULL,
	[CLIPRO_USUARIOFTPPRUEBA] [nvarchar](50) NULL,
	[CLIPRO_PASSFTPPRUEBA] [varchar](50) NULL,
	[CLIPRO_HOSTFTPEXPLOTACION] [varchar](255) NULL,
	[CLIPRO_USUARIOFTPEXPLOTACION] [varchar](255) NULL,
	[CLIPRO_PASSFTPEXPLOTACION] [varchar](255) NULL,
 CONSTRAINT [aaaaaCLIENTES_PRODUCTOS_PK] PRIMARY KEY NONCLUSTERED 
(
	[CLIPRO_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1695' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2055' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'쉀嚔훨䜝쒘홅㘆࠵' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_SUCLIENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_SUCLIENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'賅㴱뷐䲈䪅酐૭䓸' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_SUPRODUCTO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_SUPRODUCTO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_SUPRODUCTO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2130' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'俚꼤选䧤뚼ᙥ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_FECHAALTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_FECHAALTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2415' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'仇쪢숁䃥ꊅ迤졚唙' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_COMENTARIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_COMENTARIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'TextFormat', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_COMENTARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3450' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ⶉ터䇹㶦툻媂㞸' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_DOMINIOPRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_DOMINIOPRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3360' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'鯶彄ꪼ伺⚏ﴘ꽕﷛' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_DOMINIOEXPLOTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_DOMINIOEXPLOTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_DOMINIOEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_CARPETAINTERNA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_CARPETAINTERNA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_CARPETAINTERNA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_HOSTFTPPRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_HOSTFTPPRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_USUARIOFTPPRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_USUARIOFTPPRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_PASSFTPPRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_PASSFTPPRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPPRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_HOSTFTPEXPLOTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_HOSTFTPEXPLOTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_HOSTFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_USUARIOFTPEXPLOTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_USUARIOFTPEXPLOTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_USUARIOFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIPRO_PASSFTPEXPLOTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIPRO_PASSFTPEXPLOTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS', @level2type=N'COLUMN',@level2name=N'CLIPRO_PASSFTPEXPLOTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'23/01/2008 10:42:41' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'22/12/2009 17:12:30' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIENTES_PRODUCTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'537' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES_PRODUCTOS'
GO
/****** Object:  Table [dbo].[CLIENTES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CLIENTES](
	[CLIE_CODIGO] [int] NOT NULL,
	[CLIE_COMERCIAL] [smallint] NULL,
	[CLIE_PROVINCIA] [smallint] NULL,
	[CLIE_LOCALIDAD] [varchar](255) NULL,
	[CLIE_NOMBRE] [varchar](255) NULL,
	[CLIE_EMAIL] [varchar](50) NULL,
	[CLIE_TELEFONO] [varchar](50) NULL,
	[CLIE_FAX] [varchar](15) NULL,
	[CLIE_DIRECCION] [varchar](150) NULL,
	[CLIE_DISTRIBUIDOR] [bit] NOT NULL,
	[CLIE_URL] [varchar](50) NULL,
	[CLIE_CIF] [varchar](10) NULL,
	[CLIE_CP] [varchar](6) NULL,
	[CLIE_USUARIO] [varchar](8) NULL,
	[CLIE_PASSWORD] [varchar](8) NULL,
	[CLIE_CODIGOFACTURAPLUS] [varchar](20) NULL,
	[CLIE_CODIGOSPYRO] [varchar](20) NULL,
	[CLIE_COMERCIALNOMBRE] [varchar](50) NULL,
 CONSTRAINT [aaaaaCLIENTES_PK] PRIMARY KEY NONCLUSTERED 
(
	[CLIE_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ឍ淃뽫䏰ꮝ媓⿭며' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1365' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'唜䂊乀ʻ໅ꉍ꼊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_COMERCIAL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_COMERCIAL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_COMERCIAL'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1695' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'㯾솶䘐잧幪ⷾ狶' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_PROVINCIA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_PROVINCIA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3615' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'뵁㕔ם䗖ᚵ瞌뚲梊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_LOCALIDAD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_LOCALIDAD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_LOCALIDAD'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'4710' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'깧全ꞧ䂎妏줘⛲峗' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'艂暄Ό䝁骶껆㶾࿣' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_EMAIL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_EMAIL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_EMAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'彴딡క䚖⦗⑜葍' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_TELEFONO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_TELEFONO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_TELEFONO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'灳熯썄䒢ʺ뎮䓛璀' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_FAX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_FAX' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_FAX'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'囐仟ᥔﯸ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_DIRECCION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_DIRECCION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DIRECCION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'瓣婐錎䜀ࢴ慷ੀ튄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_DISTRIBUIDOR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_DISTRIBUIDOR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_DISTRIBUIDOR'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'两뢡儗䃿ꮸ᭙䤨' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_URL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_URL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_URL'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'몟ꛃ�䃣鎱ꌗ홊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_CIF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_CIF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CIF'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'杋쏦ꏂ䮻䶛盆淑' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_CP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_CP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CP'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'賓帅꽪䡨粣终䄤끝' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_USUARIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_USUARIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_USUARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'䖗逊鱈亠废㋶閦' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_PASSWORD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'14' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_PASSWORD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_PASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'摹좡ಫ䦷⚽맒៴' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_CODIGOFACTURAPLUS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_CODIGOFACTURAPLUS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOFACTURAPLUS'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIE_CODIGOSPYRO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'16' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CLIE_CODIGOSPYRO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES', @level2type=N'COLUMN',@level2name=N'CLIE_CODIGOSPYRO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'23/01/2007 9:46:14' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'15/07/2011 13:19:51' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CLIENTES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'566' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'RowHeight', @value=N'300' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CLIENTES'
GO
/****** Object:  Table [dbo].[CAMPANAS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CAMPANAS](
	[CAMP_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[ID] [varchar](50) NULL,
	[CAMP_NOMBRE] [varchar](50) NULL,
	[CAMP_SUPROVINCIA] [int] NULL,
	[CAMP_SUPERFIL] [int] NULL,
	[CAMP_FINICIO] [datetime] NULL,
	[CAMP_FFIN] [datetime] NULL,
	[perfil] [varchar](50) NULL,
	[CAMP_CARPETA] [varchar](50) NULL,
 CONSTRAINT [aaaaaCAMPANAS_PK] PRIMARY KEY NONCLUSTERED 
(
	[CAMP_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'⬞蠙㒦䵩䚦瓝莽킛' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CAMP_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CAMP_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'味ꞎ큚䢩㶫驃ᜄꮍ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'䢓讀䒪嚳뙌垛厞' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CAMP_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CAMP_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'啕⊲꫟䏬삼༝遣骃' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CAMP_SUPROVINCIA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CAMP_SUPROVINCIA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPROVINCIA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'�Ĉ䅇㞞慏碬䠡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CAMP_SUPERFIL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CAMP_SUPERFIL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_SUPERFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'雅筕喌䔗뮎؉⵴Ǌ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CAMP_FINICIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CAMP_FINICIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FINICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'퇬�璥䒳躡䉸莧鋎' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CAMP_FFIN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CAMP_FFIN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_FFIN'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'팝썅䔌퇉最ﭤ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'perfil' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'perfil' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'perfil'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CAMP_CARPETA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'CAMP_CARPETA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS', @level2type=N'COLUMN',@level2name=N'CAMP_CARPETA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'25/01/2005 11:13:26' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'02/03/2011 10:55:53' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CAMPANAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'108' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CAMPANAS'
GO
/****** Object:  Table [dbo].[PROYECTOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PROYECTOS](
	[PROY_CODIGO] [int] NOT NULL,
	[PROY_SUCLIENTE] [int] NULL,
	[PROY_SUCONTACTO] [int] NULL,
	[PROY_SUANALISTA] [int] NULL,
	[PROY_FECHAANALISIS] [datetime] NULL,
	[PROY_DESCRIPCION] [varchar](255) NULL,
 CONSTRAINT [aaaaaPROYECTOS_PK] PRIMARY KEY NONCLUSTERED 
(
	[PROY_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'뮢讀걜䗿钐괫ካᲴ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROY_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROY_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROYECTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROY_SUCLIENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROY_SUCLIENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROYECTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCLIENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROY_SUCONTACTO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROY_SUCONTACTO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROYECTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUCONTACTO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROY_SUANALISTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROY_SUANALISTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROYECTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_SUANALISTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROY_FECHAANALISIS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROY_FECHAANALISIS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROYECTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_FECHAANALISIS'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROY_DESCRIPCION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROY_DESCRIPCION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROYECTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS', @level2type=N'COLUMN',@level2name=N'PROY_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/02/2005 19:08:35' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'15/07/2011 11:50:40' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROYECTOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'719' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROYECTOS'
GO
/****** Object:  Table [dbo].[PROVINCIAS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PROVINCIAS](
	[PROV_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[PROV_NOMBRE] [varchar](50) NULL,
 CONSTRAINT [aaaaaPROVINCIAS_PK] PRIMARY KEY NONCLUSTERED 
(
	[PROV_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'玓�ῶ仭㒞淨瞸눆' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROV_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROV_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROVINCIAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'闳�妠䃒놴궺鼝镂' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROV_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROV_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROVINCIAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS', @level2type=N'COLUMN',@level2name=N'PROV_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'22/12/2004 13:12:54' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'18/01/2005 16:37:19' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROVINCIAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROVINCIAS'
GO
/****** Object:  Table [dbo].[PROCESOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PROCESOS](
	[PROC_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[PROC_TABLA] [varchar](50) NULL,
	[PROC_NUMERO] [int] NULL,
	[PROC_NOMBRE] [varchar](50) NULL,
 CONSTRAINT [aaaaaPROCESOS_PK] PRIMARY KEY NONCLUSTERED 
(
	[PROC_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROC_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROC_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROCESOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROC_TABLA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROC_TABLA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROCESOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_TABLA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:AÑADIR 2:BORRAR 3: MODIFICAR (despues) -1:AÑADIR 2:BORRAR 3:MODIFICAR (antes)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROC_NUMERO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROC_NUMERO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROCESOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NUMERO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROC_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PROC_NOMBRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PROCESOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS', @level2type=N'COLUMN',@level2name=N'PROC_NOMBRE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'14/03/2005 17:14:45' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'10/05/2005 8:37:48' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PROCESOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PROCESOS'
GO
/****** Object:  Table [dbo].[POS_TERMINOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POS_TERMINOS](
	[TERM_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[TERM_DOMINIO] [int] NULL,
	[TERM_NOMBREDOMINIO] [varchar](100) NULL,
	[TERM_DOMINIO_ASOCIADO] [varchar](255) NULL,
	[TERM_TERMINO] [varchar](255) NULL,
	[TERM_FECHAALTA] [datetime] NULL,
	[TERM_GOOGLE] [varchar](255) NULL,
	[TERM_GOOGLE_IDIOMA] [varchar](255) NULL,
	[TERM_YAHOO] [varchar](255) NULL,
	[TERM_LIVE] [varchar](255) NULL,
	[TERM_PRUEBA] [bit] NOT NULL,
	[TERM_POSICIONCONTRATADA] [smallint] NULL,
	[TERM_COSTE] [real] NULL,
 CONSTRAINT [aaaaaPOS_TERMINOS_PK] PRIMARY KEY NONCLUSTERED 
(
	[TERM_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'膚��䤢႘檯䪾楂' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1935' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'椻ஆ䥃檞㗶꺈昬' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3015' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_DOMINIO_ASOCIADO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_DOMINIO_ASOCIADO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_DOMINIO_ASOCIADO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2595' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ญ쨼㼀亻⋭ឝ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_TERMINO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_TERMINO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_TERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ﰐ࿸䤉뢼ꆙ䖰渚' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_FECHAALTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_FECHAALTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2505' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ꖒ褔Ⓞ䃝⎺ﰤ懲' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_GOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_GOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2775' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_GOOGLE_IDIOMA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_GOOGLE_IDIOMA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_GOOGLE_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1980' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_YAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_YAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_YAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1755' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_LIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_LIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Si no está contratado por el cliente lo marcamos, será una prueba' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_PRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_PRUEBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_PRUEBA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3060' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Posicion: top 40, top 20, top 10, top 1 ...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_POSICIONCONTRATADA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_POSICIONCONTRATADA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_POSICIONCONTRATADA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cobro al cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TERM_COSTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TERM_COSTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS', @level2type=N'COLUMN',@level2name=N'TERM_COSTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'19/09/2007 10:56:23' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'16/03/2012 14:08:51' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_TERMINOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'827' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_TERMINOS'
GO
/****** Object:  Table [dbo].[POS_POSICIONES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[POS_POSICIONES](
	[POS_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[POS_SUTERMINO] [int] NULL,
	[POS_FECHA] [datetime] NULL,
	[POS_POSICIONGOOGLE] [int] NULL,
	[POS_CANTIDADGOOGLE] [real] NULL,
	[pos_posicionGOOGLE_es] [real] NULL,
	[pos_cantidadGOOGLE_es] [real] NULL,
	[POS_POSICIONYAHOO] [int] NULL,
	[POS_CANTIDADYAHOO] [real] NULL,
	[POS_POSICIONLIVE] [int] NULL,
	[POS_CANTIDADLIVE] [real] NULL,
	[POS_AUXPOSICIONESVISITADAS] [smallint] NULL,
 CONSTRAINT [aaaaaPOS_POSICIONES_PK] PRIMARY KEY NONCLUSTERED 
(
	[POS_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'뒇ꁪ䫱語ଇ垞능' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'�典붲䳿鲳绔꫍' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_SUTERMINO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_SUTERMINO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_SUTERMINO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2790' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'虜ᆢ雉䰏ঁ诶簞싄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_FECHA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_FECHA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3585' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'銍黦↏䵱㪨ᣇ뮗࿝' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_POSICIONGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_POSICIONGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2115' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'嬪᳡䪈삱馑⬸樄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_CANTIDADGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_CANTIDADGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'牃ၸ佻횅悛湯ῥ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'pos_posicionGOOGLE_es' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'pos_posicionGOOGLE_es' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_posicionGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'嵍ʾ佑羣䭓捈' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'pos_cantidadGOOGLE_es' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'pos_cantidadGOOGLE_es' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'pos_cantidadGOOGLE_es'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'䂎倏�䘙좲鉟ﲩ瞕' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_POSICIONYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_POSICIONYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'䓶쒮嫡䮍梷鸣⹲㢖' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_CANTIDADYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_CANTIDADYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1995' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'捠쒙䵦؜畓픟' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_POSICIONLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_POSICIONLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_POSICIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2370' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'粢煣䟼⮟춿߸߻' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_CANTIDADLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_CANTIDADLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_CANTIDADLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3900' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'䴺ઐ漭䄞䮉ꯞ䝲뛅' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de posiciones visitadas en los buscadores.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_AUXPOSICIONESVISITADAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'POS_AUXPOSICIONESVISITADAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES', @level2type=N'COLUMN',@level2name=N'POS_AUXPOSICIONESVISITADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'25/09/2008 8:43:31' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'22/11/2011 13:48:40' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Filter', @value=N'((POS_POSICIONES.POS_SUTERMINO=505))' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderBy', @value=N'POS_POSICIONES.POS_FECHA DESC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'POS_POSICIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'358064' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POS_POSICIONES'
GO
/****** Object:  Table [dbo].[MEN_MENUS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MEN_MENUS](
	[MEN_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[MEN_SUPADRE] [int] NULL,
	[MEN_NIVEL] [int] NULL,
	[MEN_MENU_ES] [nchar](100) NULL,
	[MEN_ENLACE] [nchar](255) NULL,
	[MEN_ORDEN] [int] NULL,
 CONSTRAINT [PK_MEN_MENUS] PRIMARY KEY CLUSTERED 
(
	[MEN_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LOC_USUARIOS_LOG_USO]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_USUARIOS_LOG_USO](
	[LOCULU_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCULU_USUARIO] [varchar](50) NULL,
	[LOCULU_CLIENTE] [int] NULL,
	[LOCULU_ID_VISTA] [varchar](25) NULL,
	[LOCULU_DESC_VISTA] [varchar](255) NULL,
	[LOCULU_URL] [varchar](255) NULL,
	[LOCULU_FECHAHORA] [datetime] NULL,
 CONSTRAINT [PK_LOC_USUARIOS_LOG_USO] PRIMARY KEY CLUSTERED 
(
	[LOCULU_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_USUARIOS_CLIENTES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_USUARIOS_CLIENTES](
	[LOCUC_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCUC_USUARIO] [varchar](50) NULL,
	[LOCUC_CLIENTE] [int] NULL,
	[LOCUC_ESADMINISTRADOR] [bit] NULL,
	[LOCUC_ACCESO_PERFIL] [bit] NULL,
	[LOCUC_ACCESO_HISTORICO] [bit] NULL,
	[LOCUC_ACCESO_M_INFORME] [bit] NULL,
	[LOCUC_ACCESO_M_SEO] [bit] NULL,
	[LOCUC_ACCESO_M_ECOMMERCE] [bit] NULL,
	[LOCUC_ACCESO_M_CAMPANAS] [bit] NULL,
	[LOCUC_ACCESO_M_CUADROMANDO] [bit] NULL,
 CONSTRAINT [PK_LOC_USUARIOS_CLIENTES] PRIMARY KEY CLUSTERED 
(
	[LOCUC_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_USUARIOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_USUARIOS](
	[LOCU_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCU_EMAIL] [varchar](50) NOT NULL,
	[LOCU_PASSWORD] [char](128) NULL,
	[LOCU_SALT] [char](128) NULL,
	[LOCU_NOMBRE] [varchar](50) NULL,
	[LOCU_APELLIDOS] [varchar](100) NULL,
	[LOCU_SEXO] [char](1) NULL,
	[LOCU_IDIOMA] [char](2) NULL,
 CONSTRAINT [PK_LOC_USUARIOS] PRIMARY KEY CLUSTERED 
(
	[LOCU_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_TIPOSLOCALIZACION]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_TIPOSLOCALIZACION](
	[LOCT_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCT_DESCRIPCION] [varchar](255) NULL,
 CONSTRAINT [aaaaaLOC_TIPOSLOCALIZACION_PK] PRIMARY KEY NONCLUSTERED 
(
	[LOCT_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'६覒㘗䘸ꆋ팪깖炷' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCT_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCT_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_TIPOSLOCALIZACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2475' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'◎ཫ䡩➶틃ꊂ뵠' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCT_DESCRIPCION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCT_DESCRIPCION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_TIPOSLOCALIZACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION', @level2type=N'COLUMN',@level2name=N'LOCT_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'25/05/2009 18:14:28' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'25/05/2009 18:17:37' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_TIPOSLOCALIZACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_TIPOSLOCALIZACION'
GO
/****** Object:  Table [dbo].[LOC_SPAM_REFERER]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_SPAM_REFERER](
	[LOCSR_REFERER] [varchar](150) NOT NULL,
	[LOCSR_FECHAALTA] [date] NULL,
	[LOCSR_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_LOC_SPAM_REFERER] PRIMARY KEY CLUSTERED 
(
	[LOCSR_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_LOC_SPAM_REFERER] UNIQUE NONCLUSTERED 
(
	[LOCSR_REFERER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_SPAM_IP]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_SPAM_IP](
	[LOCSI_IP] [varchar](15) NOT NULL,
	[LOCSI_FECHAALTA] [date] NULL,
	[LOCSI_PAIS] [varchar](2) NULL,
 CONSTRAINT [PK_LOC_SPAM_IP] PRIMARY KEY CLUSTERED 
(
	[LOCSI_IP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_ROBOTS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_ROBOTS](
	[LOCRO_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCRO_ROBOT] [varchar](100) NULL,
	[LOCRO_STRINGAGENTE] [varchar](100) NULL,
	[LOCRO_STRINGROBOTSTXT] [varchar](100) NULL,
	[LOCRO_ORDENUSO] [int] NULL,
	[LOCRO_COMENTARIO] [varchar](255) NULL,
	[LOCRO_FECHAALTA] [datetime] NULL,
 CONSTRAINT [aaaaaLOC_ROBOTS_PK] PRIMARY KEY NONCLUSTERED 
(
	[LOCRO_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'냃베䲑涱嗧숔㫅' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRO_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRO_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_ROBOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2370' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'Ⴋ竚㧭䚧㾞ꞥ⋺' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRO_ROBOT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRO_ROBOT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_ROBOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ROBOT'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'㢺跸ᕏ䨠즓犴ꢧ⎝' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRO_STRINGAGENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRO_STRINGAGENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_ROBOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGAGENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3195' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'님㆓뎸䅳⪄緅ૡ쿆' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRO_STRINGROBOTSTXT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRO_STRINGROBOTSTXT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_ROBOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_STRINGROBOTSTXT'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'�嶻쯗䈶ඪ搔⪖' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRO_ORDENUSO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRO_ORDENUSO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_ROBOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_ORDENUSO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'孶೐忻䘌ゕ㡨≕볩' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRO_COMENTARIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRO_COMENTARIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_ROBOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_COMENTARIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRO_FECHAALTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRO_FECHAALTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_ROBOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS', @level2type=N'COLUMN',@level2name=N'LOCRO_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'27/10/2008 16:03:13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'31/10/2008 8:55:54' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_ROBOTS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'85' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'RowHeight', @value=N'300' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_ROBOTS'
GO
/****** Object:  Table [dbo].[LOC_RESULTADOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_RESULTADOS](
	[LOCR_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCR_SUDOMINIO] [int] NULL,
	[LOCR_NOMBREDOMINIO] [varchar](100) NULL,
	[LOCR_FECHA] [datetime] NULL,
	[LOCR_GOOGLE] [int] NULL,
	[LOCR_LIVE] [int] NULL,
	[LOCR_CONTADORES98] [int] NULL,
	[LOCR_CONTADORES99] [int] NULL,
	[LOCR_CONTADORES100] [int] NULL,
	[LOCR_CONTADORES101] [int] NULL,
	[LOCR_CONTADORES102] [int] NULL,
	[LOCR_CONTADORES103] [int] NULL,
	[LOCR_PAGINASSITEMAP] [int] NULL,
	[locr_enlacesyahoo] [int] NULL,
	[LOCR_FHULTIMACONSULTACONTADORES] [datetime] NULL,
	[LOCR_CONTADORES98_EXC] [int] NULL,
	[LOCR_CONTADORES99_EXC] [int] NULL,
	[LOCR_CONTADORES100_EXC] [int] NULL,
	[LOCR_CONTADORES101_EXC] [int] NULL,
	[LOCR_CONTADORES102_EXC] [int] NULL,
	[LOCR_CONTADORES103_EXC] [int] NULL,
	[LOCR_SEMANA] [int] NULL,
 CONSTRAINT [aaaaaLOC_RESULTADOS_PK] PRIMARY KEY NONCLUSTERED 
(
	[LOCR_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ࡱ嗠ᚋ佒誇驱컯' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'恌辦䓟䤁撵웁窆ศ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_SUDOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_SUDOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_SUDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2295' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'此訊⁽䟢㪲Ș솤샂' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_FECHA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_FECHA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FECHA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1965' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'굕ౡ儑䌸햣ꯌ槒䋞' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_GOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_GOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_GOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'순ᣏ⊵䷱ᗏ᪞' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_LIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_LIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_LIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1905' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'hits generales al sitio web' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_CONTADORES98' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_CONTADORES98' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES98'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2625' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sesiones generales al sitio web' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_CONTADORES99' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_CONTADORES99' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES99'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'hits de robots' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_CONTADORES100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_CONTADORES100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES100'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'hits de sospechosos de ser robots' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_CONTADORES101' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_CONTADORES101' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES101'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sesiones de sospechosos de ser robots' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_CONTADORES102' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_CONTADORES102' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES102'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'sesiones de robots' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_CONTADORES103' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_CONTADORES103' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_CONTADORES103'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1500' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero de entradas en el sitemap' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_PAGINASSITEMAP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_PAGINASSITEMAP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_PAGINASSITEMAP'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'locr_enlacesyahoo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'locr_enlacesyahoo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'locr_enlacesyahoo'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'4140' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCR_FHULTIMACONSULTACONTADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCR_FHULTIMACONSULTACONTADORES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS', @level2type=N'COLUMN',@level2name=N'LOCR_FHULTIMACONSULTACONTADORES'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'14/10/2008 10:40:20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'16/03/2012 12:00:29' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_RESULTADOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'124656' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_RESULTADOS'
GO
/****** Object:  Table [dbo].[LOC_REGIONES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_REGIONES](
	[LOCRE_PAIS] [varchar](255) NOT NULL,
	[LOCRE_CODIGOREGION] [varchar](255) NOT NULL,
	[LOCRE_REGION] [nvarchar](255) NULL,
 CONSTRAINT [aaaaaLOC_REGIONES_PK] PRIMARY KEY CLUSTERED 
(
	[LOCRE_PAIS] ASC,
	[LOCRE_CODIGOREGION] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1530' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ଗ撎濬䒊禓焋㎅訟' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRE_PAIS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRE_PAIS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_REGIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2580' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'咛Ш䋙銧㛵ꩫ꭪' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRE_CODIGOREGION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRE_CODIGOREGION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_REGIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_CODIGOREGION'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2010' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'铰㲗⋸俋톃ଚ࿆㉋' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCRE_REGION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCRE_REGION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_REGIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES', @level2type=N'COLUMN',@level2name=N'LOCRE_REGION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'22/11/2009 22:26:16' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'19/11/2010 20:45:14' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_REGIONES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'4258' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_REGIONES'
GO
/****** Object:  Table [dbo].[LOC_PROYECTOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_PROYECTOS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[proyecto] [varchar](255) NOT NULL,
	[apiConsoleId] [varchar](255) NULL,
	[keyFile] [varchar](255) NULL,
 CONSTRAINT [PK_LOC_PROYECTOS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_PAISES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_PAISES](
	[LOCP_CODIGO] [nvarchar](2) NOT NULL,
	[LOCP_PAIS] [varchar](50) NULL,
	[LOCP_CONTINENTE] [varchar](20) NULL,
	[LOCP_COUNTRY] [varchar](100) NULL,
 CONSTRAINT [aaaaaLOC_PAISES_PK] PRIMARY KEY NONCLUSTERED 
(
	[LOCP_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1755' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'묻嚍䁢맿퐯萠' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCP_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCP_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_PAISES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2370' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'�駉솲䅙ﰢﮣ鸃' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCP_PAIS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCP_PAIS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_PAISES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_PAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ꅯ䪿䡕ࢷ娄･㞉' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCP_CONTINENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCP_CONTINENTE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_PAISES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES', @level2type=N'COLUMN',@level2name=N'LOCP_CONTINENTE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'22/11/2009 22:26:11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'22/11/2009 22:26:11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_PAISES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'237' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_PAISES'
GO
/****** Object:  Table [dbo].[LOC_LOGIN_ATTEMPTS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_LOGIN_ATTEMPTS](
	[LOCU_CODIGO] [int] NULL,
	[LOCU_TIME] [varchar](30) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_IDIOMASNAVEGADOR]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_IDIOMASNAVEGADOR](
	[LOCI_CODIGO] [nvarchar](2) NOT NULL,
	[LOCI_IDIOMA] [varchar](50) NULL,
	[LOCI_IDIOMA_EN] [varchar](50) NULL,
 CONSTRAINT [aaaaaLOC_IDIOMASNAVEGADOR_PK] PRIMARY KEY NONCLUSTERED 
(
	[LOCI_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'枤௄㬯䤺蚘뿱ⴔ⨂' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Basado en ISO 639-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCI_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCI_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_IDIOMASNAVEGADOR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2505' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCI_IDIOMA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCI_IDIOMA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_IDIOMASNAVEGADOR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR', @level2type=N'COLUMN',@level2name=N'LOCI_IDIOMA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/02/2012 9:03:08' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/02/2012 10:53:03' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderBy', @value=N'LOC_IDIOMASNAVEGADOR.LOCI_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_IDIOMASNAVEGADOR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'186' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_IDIOMASNAVEGADOR'
GO
/****** Object:  Table [dbo].[LOC_GA_SOCIALNETWORKS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_GA_SOCIALNETWORKS](
	[LOCGS_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCGS_SOCIALNETWORK] [varchar](100) NULL,
	[LOCGS_FILTRO_EXPREG] [varchar](128) NULL,
 CONSTRAINT [PK_LOC_GA_SOCIALNETWORKS] PRIMARY KEY CLUSTERED 
(
	[LOCGS_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_FUENTES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_FUENTES](
	[LOCF_SESION] [varchar](33) NULL,
	[LOCF_IP] [varchar](15) NULL,
	[LOCF_FUENTE] [varchar](100) NULL,
	[LOCF_VALOR] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL](
	[LOCDVUC_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDVUC_NOMBREDOMINIO] [nchar](100) NULL,
	[LOCDVUC_URLCONTROL] [nchar](255) NULL,
	[LOCDVUC_URLCONTROL_CATEGORIA] [nchar](50) NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL] PRIMARY KEY CLUSTERED 
(
	[LOCDVUC_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_URLS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_URLS](
	[LOCDVP_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDVP_NOMBREDOMINIO] [varchar](100) NULL,
	[LOCDVP_ANOMES] [varchar](6) NULL,
	[LOCDVP_URL] [varchar](255) NULL,
	[LOCDVP_W_IMPRESIONES] [int] NULL,
	[LOCDVP_W_CLICS] [int] NULL,
	[LOCDVP_W_CTR] [float] NULL,
	[LOCDVP_W_POSICIONMEDIA] [float] NULL,
	[LOCDVP_M_IMPRESIONES] [int] NULL,
	[LOCDVP_M_CLICS] [int] NULL,
	[LOCDVP_M_CTR] [float] NULL,
	[LOCDVP_M_POSICIONMEDIA] [float] NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS_WEBMASTER_URLS] PRIMARY KEY CLUSTERED 
(
	[LOCDVP_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES](
	[LOCDVT_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDVT_NOMBREDOMINIO] [nchar](100) NULL,
	[LOCDVT_ANOMES] [varchar](6) NULL,
	[LOCDVT_W_IMPRESIONES] [int] NULL,
	[LOCDVT_W_CLICS] [int] NULL,
	[LOCDVT_M_IMPRESIONES] [int] NULL,
	[LOCDVT_M_CLICS] [int] NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES] PRIMARY KEY CLUSTERED 
(
	[LOCDVT_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL](
	[LOCDVTC_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDVTC_NOMBREDOMINIO] [varchar](100) NULL,
	[LOCDVTC_TERMINOCONTROL] [varchar](255) NULL,
	[LOCDVTC_TERMINOCONTROL_CATEGORIA] [varchar](50) NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL] PRIMARY KEY CLUSTERED 
(
	[LOCDVTC_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS](
	[LOCDVS_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDVS_NOMBREDOMINIO] [varchar](100) NOT NULL,
	[LOCDVS_ANOMES] [varchar](6) NULL,
	[LOCDVS_TERMINO] [varchar](255) NULL,
	[LOCDVS_W_IMPRESIONES] [int] NULL,
	[LOCDVS_W_CLICS] [int] NULL,
	[LOCDVS_W_CTR] [float] NULL,
	[LOCDVS_W_POSICIONMEDIA] [float] NULL,
	[LOCDVS_M_IMPRESIONES] [int] NULL,
	[LOCDVS_M_CLICS] [int] NULL,
	[LOCDVS_M_CTR] [float] NULL,
	[LOCDVS_M_POSICIONMEDIA] [float] NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS] PRIMARY KEY CLUSTERED 
(
	[LOCDVS_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS](
	[LOCDVUF_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDVUF_ANALYTICS_ID_VISTA] [varchar](25) NULL,
	[LOCDVUF_USUARIO] [varchar](50) NULL,
	[LOCDVUF_FILTRO] [varchar](50) NULL,
	[LOCDVUF_FILTRODESCRIPCION] [varchar](100) NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS] PRIMARY KEY CLUSTERED 
(
	[LOCDVUF_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS_USUARIOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS_USUARIOS](
	[LOCDVU_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDVU_ANALYTICS_ID_VISTA] [varchar](25) NOT NULL,
	[LOCDVU_USUARIO] [varchar](50) NOT NULL,
	[LOCDVU_ENVIOEMAIL_INFORME_SEMANAL] [bit] NULL,
	[LOCDVU_ENVIOEMAIL_INFORME_MENSUAL] [bit] NULL,
	[LOCDVU_ENVIOEMAIL_INFORME_TRIMESTRAL] [bit] NULL,
	[LOCDVU_ENVIOEMAIL_CAMPANAS_MENSUAL] [bit] NULL,
	[LOCDVU_ENVIOEMAIL_CAMPANAS_SEMANAL] [bit] NULL,
	[LOCDVU_ENVIOEMAIL_CAMPANAS_TRIMESTRAL] [bit] NULL,
	[LOCDVU_FILTROPERSONALIZADO] [varchar](100) NULL,
	[LOCDVU_FILTROPERSONALIZADO_DESCRIPCION] [varchar](150) NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS_USUARIOS] PRIMARY KEY CLUSTERED 
(
	[LOCDVU_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS_GA_SOCIALNETWORKS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS_GA_SOCIALNETWORKS](
	[LOCDVGS_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDVGS_ANALYTICS_ID_VISTA] [varchar](25) NULL,
	[LOCDVGS_SOURCE] [varchar](100) NULL,
	[LOCDVGS_SOCIALNETWORK] [varchar](100) NULL,
	[LOCDVGS_SESIONES] [int] NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS_GA_SOCIALNETWORKS] PRIMARY KEY CLUSTERED 
(
	[LOCDVGS_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_VISTAS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_VISTAS](
	[LOCDV_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCDV_SUDOMINIO] [int] NULL,
	[LOCDV_ANALYTICS_ID_VISTA] [varchar](25) NULL,
	[LOCDV_ANALYTICS_NAME_VISTA] [varchar](255) NULL,
	[LOCDV_ANALYTICS_URL_VISTA] [varchar](100) NULL,
	[LOCDV_ANALYTICS_DEFAULT_VISTA] [bit] NULL,
	[LOCDV_ANALYTICS_FECHAPAGINASINDEXADAS] [datetime] NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_VISTAS] PRIMARY KEY CLUSTERED 
(
	[LOCDV_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS_IMPACTOSAMEDIDA]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS_IMPACTOSAMEDIDA](
	[LOCDI_CODIGO] [smallint] NOT NULL,
	[LOCDI_SUDOMINIO] [smallint] NOT NULL,
	[LOCDI_DESCRIPCION] [varchar](25) NOT NULL,
	[LOCDI_FORMULA] [varchar](50) NOT NULL,
	[LOCDI_CARACTERIDENTIFICACION] [varchar](1) NULL,
 CONSTRAINT [PK_LOC_DOMINIOS_IMPACTOSAMEDIDA] PRIMARY KEY CLUSTERED 
(
	[LOCDI_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_DOMINIOS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_DOMINIOS](
	[LOCD_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCD_DOMINIO] [int] NULL,
	[LOCD_NOMBREDOMINIO] [varchar](100) NULL,
	[LOCD_DOMINIO_PADRE] [varchar](25) NULL,
	[LOCD_ANALYTICS_TRACK] [varchar](15) NULL,
	[LOCD_ENVIARINFORMES] [bit] NOT NULL,
	[LOCD_DESHABILITAREMAILFORMULARIOS] [bit] NOT NULL,
	[LOCD_FECHAALTA] [datetime] NULL,
	[LOCD_URLGOOGLE] [bit] NOT NULL,
	[LOCD_URLYAHOO] [bit] NOT NULL,
	[LOCD_URLLIVE] [bit] NOT NULL,
	[LOCD_SITEMAPGOOGLE] [bit] NOT NULL,
	[LOCD_SITEMAPYAHOO] [bit] NOT NULL,
	[LOCD_SITEMAPLIVE] [bit] NOT NULL,
	[LOCD_SITEMAPASK] [bit] NOT NULL,
	[LOCD_EMAILINFORMES] [varchar](255) NULL,
	[LOCD_VERIFICACIONGOOGLE] [varchar](50) NULL,
	[LOCD_VERIFICACIONYAHOO] [varchar](50) NULL,
	[LOCD_VERIFICACIONLIVE] [varchar](50) NULL,
	[LOCD_TIPOLOCALIZACION] [int] NULL,
	[LOCD_PAGINAENTRADA] [varchar](50) NULL,
	[LOCD_ULTIMOENVIO] [datetime] NULL,
	[LOCD_ULTIMOENVIOINFORME2] [datetime] NULL,
	[LOCD_HISTORICO] [ntext] NULL,
	[LOCD_FECHAHORAOBTENERPOSICION] [datetime] NULL,
	[LOCD_FECHAHORAACTUALIZACIONDMINTEGRA] [datetime] NULL,
	[LOCD_FECHACOMPACTACION] [datetime] NULL,
	[LOCD_FECHAPAGINASINDEXADAS] [datetime] NULL,
	[LOCD_CM_PAISES] [varchar](255) NULL,
	[LOCD_EXCLUSIONIPS] [varchar](1000) NULL,
	[LOCD_ACTIVARFILTRADOIPS] [tinyint] NULL,
	[LOCD_FECHAHORAOBTENERPOSICIONINICIO] [datetime] NULL,
	[LOCD_EMAILCUADROMANDO] [varchar](255) NULL,
	[LOCD_ULTIMOENVIOCUADROMANDO] [datetime] NULL,
	[LOCD_ULTIMOENVIOCAMANASMARKETING] [datetime] NULL,
	[LOCD_EMAILCAMANASMARKETING] [varchar](255) NULL,
	[LOCD_ULTIMOENVIOCAMANAS_MENSUAL] [datetime] NULL,
	[LOCD_ANALYTICS_ID_CUENTA] [varchar](25) NULL,
	[LOCD_ANALYTICS_ID_PROPIEDAD] [varchar](25) NULL,
	[LOCD_ANALYTICS_NAME_PROPIEDAD] [varchar](255) NULL,
	[LOCD_ANALYTICS_INTERESES] [bit] NULL,
	[LOCD_ANALYTICS_EXCLUSIONES_REF] [bit] NULL,
	[LOCD_ANALYTICS_DIMENSIONES] [varchar](255) NULL,
	[LOCD_ANALYTICS_CUENTAGOOGLE] [varchar](100) NULL,
	[LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO] [varchar](1) NULL,
	[LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO] [bit] NULL,
	[LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D4] [varchar](200) NULL,
	[LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D5] [varchar](200) NULL,
	[LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D6] [varchar](200) NULL,
	[LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D7] [varchar](200) NULL,
	[LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_EMAILSEMANAL] [char](1) NULL,
	[LOCD_ANALYTICS_PROPIEDAD_COMERCIOELECTRONICO] [bit] NULL,
	[LOCD_ANALYTICS_PROPIEDAD_APPMOBILE] [bit] NULL,
	[LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM] [bit] NULL,
	[LOCD_COOKIES_URL] [varchar](300) NULL,
	[LOCD_COOKIES_VERSION] [varchar](15) NULL,
	[LOCD_COOKIES_NOACTUALIZAR] [bit] NULL,
	[LOCD_EMAILS_COMUNICACIONES] [varchar](200) NULL,
	[LOCD_EMAILS_COMUNICACIONES_NOMBRES] [varchar](200) NULL,
	[LOCD_BORRADOR2] [varchar](200) NULL,
	[LOCD_ADDTHIS] [varchar](50) NULL,
 CONSTRAINT [aaaaaLOC_DOMINIOS_PK] PRIMARY KEY NONCLUSTERED 
(
	[LOCD_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1920' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ꂨ聎ƴ䎎࢘腸眺뫉' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_CODIGO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CODIGO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2220' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_DOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2490' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'REPETIMOS EL NOMBRE DOMINIO PARA LAS BAJAS!' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_NOMBREDOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'100' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_NOMBREDOMINIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_NOMBREDOMINIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1995' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_ENVIARINFORMES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_ENVIARINFORMES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ENVIARINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'4095' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True-Deshabilitado / False-Habilitar' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_DESHABILITAREMAILFORMULARIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_DESHABILITAREMAILFORMULARIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_DESHABILITAREMAILFORMULARIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1950' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'㶑肘෪䣰펫䵃ⳍ抷' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_FECHAALTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_FECHAALTA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAALTA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_URLGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_URLGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_URLYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_URLYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_URLLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_URLLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_URLLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_SITEMAPGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_SITEMAPGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_SITEMAPYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_SITEMAPYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2085' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_SITEMAPLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_SITEMAPLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'106' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_SITEMAPASK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_SITEMAPASK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_SITEMAPASK'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2445' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EMAILS SEPARADOS POR ";" para el envío de los informes de localizacion (y posicionamiento)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_EMAILINFORMES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_EMAILINFORMES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILINFORMES'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1785' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_VERIFICACIONGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'14' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_VERIFICACIONGOOGLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONGOOGLE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_VERIFICACIONYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_VERIFICACIONYAHOO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONYAHOO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_VERIFICACIONLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'16' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_VERIFICACIONLIVE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_VERIFICACIONLIVE'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 dmcorporative / 2 externa-asp / 3 externa-php 4/ DMeasyCommerce' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_TIPOLOCALIZACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_TIPOLOCALIZACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_TIPOLOCALIZACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Página física de entrada al dominio por defecto (SOLO PARA EXTERNO ASP O PHP)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_PAGINAENTRADA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'50' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_PAGINAENTRADA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_PAGINAENTRADA'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha/hora del último envío de las estadísticas al emailinformes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_ULTIMOENVIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'19' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_ULTIMOENVIO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha/hora del último envío de las estadísticas DE POSICIONAMIENTO al emailinformes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_ULTIMOENVIOINFORME2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'20' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_ULTIMOENVIOINFORME2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOINFORME2'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comentarios por fecha con las acciones efectuadas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_HISTORICO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'21' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_HISTORICO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'TextFormat', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_HISTORICO'
GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3885' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha/hora de última modificación de LOC_RESULTADOS (de paso de ObtenerPosiciona.asp)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_FECHAHORAOBTENERPOSICION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'22' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'ShowDatePicker', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_FECHAHORAOBTENERPOSICION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAOBTENERPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'4665' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'23' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAHORAACTUALIZACIONDMINTEGRA'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3030' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_FECHACOMPACTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'24' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_FECHACOMPACTACION' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHACOMPACTACION'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha / hora de la última captación de las páginas indexadas en Google y Bing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_FECHAPAGINASINDEXADAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'25' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_FECHAPAGINASINDEXADAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_FECHAPAGINASINDEXADAS'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'3082' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Paises (menos global) separados por comas. Ej. ES,FR,US ...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOCD_CM_PAISES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'26' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'LOCD_CM_PAISES' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_CM_PAISES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EMAILS SEPARADOS POR ";" para el envío de los informes de localizacion (y posicionamiento)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_EMAILCUADROMANDO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha/hora del último envío de las estadísticas al emailinformes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOCUADROMANDO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha/hora del último envío de las estadísticas DE POSICIONAMIENTO al emailinformes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS', @level2type=N'COLUMN',@level2name=N'LOCD_ULTIMOENVIOCAMANASMARKETING'
GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'14/10/2008 10:39:42' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'18/06/2012 13:41:33' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'LOC_DOMINIOS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'160' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LOC_DOMINIOS'
GO
/****** Object:  Table [dbo].[LOC_CUENTAS_GOOGLE]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_CUENTAS_GOOGLE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cuenta] [varchar](255) NOT NULL,
 CONSTRAINT [PK_LOC_CUENTAS_GOOGLE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_CAMPANAS_USUARIOS_DISTRIBUCION]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_CAMPANAS_USUARIOS_DISTRIBUCION](
	[LOCCUD_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCCUD_SUCAMPANA] [int] NOT NULL,
	[LOCCUD_USUARIO] [varchar](50) NOT NULL,
	[LOCCUD_ENVIOEMAIL_SEMANAL] [bit] NULL,
	[LOCCUD_ENVIOEMAIL_MENSUAL] [bit] NULL,
	[LOCCUD_ENVIOEMAIL_TRIMESTRAL] [bit] NULL,
 CONSTRAINT [PK_LOC_CAMPANAS_USUARIOS_DISTRIBUCION] PRIMARY KEY CLUSTERED 
(
	[LOCCUD_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_CAMPANAS_URLDESTINO]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_CAMPANAS_URLDESTINO](
	[LOCUD_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCUD_SUCAMPANA] [int] NOT NULL,
	[LOCUD_URL] [varchar](255) NOT NULL,
	[LOCUD_FECHA_ALTA] [datetime] NULL,
	[LOCUD_FECHA_MODIFICACION] [datetime] NULL,
 CONSTRAINT [PK_LOC_CAMPANAS_URLDESTINO] PRIMARY KEY CLUSTERED 
(
	[LOCUD_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_CAMPANAS_PROPIEDAD_MARGEN_MES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOC_CAMPANAS_PROPIEDAD_MARGEN_MES](
	[LOCCPR_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCCPR_ANALYTICS_ID_PROPIEDAD] [varchar](25) NULL,
	[LOCCPR_SUANO] [int] NULL,
	[LOCCPR_SUMES] [int] NULL,
	[LOCCPR_MARGEN] [real] NULL,
 CONSTRAINT [PK_LOC_CAMPANAS_PROPIEDAD_MARGEN_MES] PRIMARY KEY CLUSTERED 
(
	[LOCCPR_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOC_CAMPANAS_COSTEMES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOC_CAMPANAS_COSTEMES](
	[LOCCC_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCCC_SUCAMPANA] [int] NULL,
	[LOCCC_SUANO] [int] NULL,
	[LOCCC_SUMES] [int] NULL,
	[LOCCC_COSTE] [float] NULL,
 CONSTRAINT [PK_LOC_CAMPANAS_COSTEMES] PRIMARY KEY CLUSTERED 
(
	[LOCCC_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LOC_CAMPANAS_COMPARACION_TRANS_MES]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOC_CAMPANAS_COMPARACION_TRANS_MES](
	[LOCCCM_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCCCM_ANALYTICS_ID_VISTA] [nchar](25) NULL,
	[LOCCCM_SUMES] [int] NULL,
	[LOCCCM_SUANO] [int] NULL,
	[LOCCCM_TIPO] [nchar](15) NULL,
	[LOCCCM_CAMPANA] [nchar](100) NULL,
	[LOCCCM_MEDIO] [nchar](50) NULL,
	[LOCCCM_FUENTE] [nchar](50) NULL,
	[LOCCCM_TRANSACCIONES_LINEAL_NUMERO] [real] NULL,
	[LOCCCM_TRANSACCIONES_LINEAL_EUROS] [real] NULL,
	[LOCCCM_SESIONES] [int] NULL,
	[LOCCCM_CONVERSIONES_ASISTIDAS_NUM] [real] NULL,
	[LOCCCM_CONVERSIONES_ASISTIDAS_EUROS] [real] NULL,
	[LOCCCM_CONVERSIONES_DIRECTAS_NUM] [real] NULL,
	[LOCCCM_CONVERSIONES_DIRECTAS_EUROS] [real] NULL,
 CONSTRAINT [PK_LOC_CAMPANAS_COMPARACION_MES] PRIMARY KEY CLUSTERED 
(
	[LOCCCM_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LOC_CAMPANAS_COMPARACION_TRANS_FECHAS]    Script Date: 07/22/2015 14:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOC_CAMPANAS_COMPARACION_TRANS_FECHAS](
	[LOCCF_CODIGO] [int] IDENTITY(1,1) NOT NULL,
	[LOCCF_ANALYTICS_ID_VISTA] [nchar](25) NULL,
	[LOCCF_SUANO] [int] NULL,
	[LOCCF_SUMES] [int] NULL,
	[LOCCF_FECHA_ULTIMO_FICHERO_TRANSACCIONES] [datetime] NULL,
	[LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES] [datetime] NULL,
 CONSTRAINT [PK_LOC_CAMPANAS_COMPARACION_TRANS_FECHAS] PRIMARY KEY CLUSTERED 
(
	[LOCCF_CODIGO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[LOC_CAMPANAS_COMPARACION]    Script Date: 07/22/2015 14:21:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alberto>
-- Create date: <20/05/2015>
-- Modified date: <20/05/2015>
-- Description:	<Datos de la página de comparación de campañas>
-- =============================================
CREATE PROCEDURE [dbo].[LOC_CAMPANAS_COMPARACION] 
	--PARAMETROS
	@ANALYTICS_ID_VISTA INT,
	@FECHA_INICIO DATE,
	@FECHA_FIN DATE,
	-- Utilizamos este parámetro para "desglosar" todos los registros de una campaña
	@CAMPANA_DESGLOSE VARCHAR(255),
	-- Utilizamos este parámetro para "desglosar" los tipos SOCIAL, SEO y ENLACES
	@TIPO_DESGLOSE VARCHAR(25),
	-- Utilizamos este parámetro para "AGRUPAR" sólo las campañas bien, por campañas o bien por el medio. Valores "MEDIO" / "CAMPANA"
	@AGRUPACION VARCHAR(25),
	-- Utilizamos este parámetro para "FILTRAR" sólo las campañas que son del usuario (El usuario debe estar en el campo LOCC_ENVIOS_EMAILS de LOC_CAMPANAS)
	@USUARIO VARCHAR(150)
	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--CALCULOS
	DECLARE @ANOMES_INICIO varchar(6)
	DECLARE @ANOMES_FIN varchar(6)
	select @ANOMES_INICIO = ltrim(str(YEAR(@FECHA_INICIO))) + RIGHT('00'+ ltrim(str(MONTH(@FECHA_INICIO))),2)
	select @ANOMES_FIN = ltrim(str(YEAR(@FECHA_FIN))) + RIGHT('00'+ ltrim(str(MONTH(@FECHA_FIN))),2)
	--VARIABLE PARA LAS SQL
	DECLARE @strSql nvarchar(MAX), @strSqlAuxSelectFrom nvarchar(MAX)	

	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- TABLA: TODAS LAS CAMPAÑAS AGRUPADAS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	IF @CAMPANA_DESGLOSE = '' AND @TIPO_DESGLOSE = ''
	BEGIN
		
		-----------------------------------------------
		--TABLA DE CAMPAÑAS ---------------------------
		-----------------------------------------------
		SET @strSql = 
		N'SELECT ANO AS ANO,MES AS MES,
				TIPO_MAESTRO,
				LOCCCM_SUANO, LOCCCM_SUMES,
				CAMPANA_MAESTRO,NUM_MEDIO_FUENTE,LOCCCM_MEDIO,LOCCCM_FUENTE,
				LOCCCM_TRANSACCIONES_LINEAL_NUMERO,
				LOCCCM_TRANSACCIONES_LINEAL_EUROS,
				LOCCC_COSTE,
				PEDIDOMEDIO,
				RENDIMIENTO,
				LOCCCM_SESIONES,
				COSTE_ADQUISICION,
				COMISION,
				TASA_CONVERSION,
				SESIONES_UNPEDIDO,
				RETORNO_INVERSION_PUBLI,
				LOCCCM_CONVERSIONES_NUMERO,
				LOCCCM_CONVERSIONES_EUROS,
				TASA_CONVERSION_CONVERSIONES,
				SESIONES_UNACONVERSION,
				RENDIMIENTO_CONVERSIONES,
				RETORNO_INVERSION_PUBLI_CONVERSIONES,
				COSTE_ADQUISICION_CONVERSION	
		FROM
		(	
		SELECT LOCCCM_SUANO, LOCCCM_SUMES,LOCCCM_TIPO,
				-- Campo dependiente de la agrupación.  Si agrupamos por MEDIO, comprobamos si el tipo es SEO,DIRECTO.. (no es CAMPANAS) y devolvemos LOCCCM_CAMPANA 
				'
				IF @AGRUPACION='MEDIO'
				BEGIN
					  SET @strSql = @strSql + N'
						  CASE
								WHEN LOCCCM_TIPO=''CAMPANAS'' THEN LOCCCM_MEDIO 
								WHEN LOCCCM_TIPO<>''CAMPANAS'' THEN LOCCCM_CAMPANA
						  END '
				END
				ELSE
				BEGIN
					  SET @strSql = @strSql + N' LOCCCM_CAMPANA '
				END
				
				SET @strSql = @strSql + N' AS LOCCCM_CAMPANA,				
				COUNT(*) AS NUM_MEDIO_FUENTE,'''' AS LOCCCM_MEDIO, '''' AS LOCCCM_FUENTE, '		
				SET @strSqlAuxSelectFrom = ' 
				round(SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO),2) AS LOCCCM_TRANSACCIONES_LINEAL_NUMERO,
				round(SUM(LOCCCM_TRANSACCIONES_LINEAL_EUROS),2) AS LOCCCM_TRANSACCIONES_LINEAL_EUROS,
				round(SUM(LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM),2) AS LOCCCM_CONVERSIONES_NUMERO,
				round(SUM(LOCCCM_CONVERSIONES_ASISTIDAS_EUROS + LOCCCM_CONVERSIONES_DIRECTAS_EUROS),2) AS LOCCCM_CONVERSIONES_EUROS,				
				SUM(LOCCC_COSTE) AS LOCCC_COSTE, 
				round(CASE WHEN SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO)=0 THEN 0 ELSE SUM(LOCCCM_TRANSACCIONES_LINEAL_EUROS)/SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO) END,2) AS PEDIDOMEDIO,
				round(SUM(LOCCCM_TRANSACCIONES_LINEAL_EUROS) * (AVG(CASE WHEN LOCCPR_MARGEN IS NULL THEN 100 ELSE LOCCPR_MARGEN END) / 100) - SUM(CASE WHEN LOCCC_COSTE IS NULL THEN 0 ELSE LOCCC_COSTE END),2) AS RENDIMIENTO,
				round(SUM(LOCCCM_CONVERSIONES_ASISTIDAS_EUROS + LOCCCM_CONVERSIONES_DIRECTAS_EUROS) - SUM(CASE WHEN LOCCC_COSTE IS NULL THEN 0 ELSE LOCCC_COSTE END),2) AS RENDIMIENTO_CONVERSIONES,		
				SUM(LOCCCM_SESIONES) AS LOCCCM_SESIONES, 
				round(SUM(LOCCC_COSTE) / CASE WHEN SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO) = 0 THEN 1 ELSE SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO) END,2) AS COSTE_ADQUISICION,
				round(SUM(LOCCC_COSTE) / CASE WHEN SUM(LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM) = 0 THEN 1 ELSE SUM(LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM) END,2) AS COSTE_ADQUISICION_CONVERSION,
				round(SUM(LOCCC_COSTE*100) / CASE WHEN SUM(LOCCCM_TRANSACCIONES_LINEAL_EUROS) = 0 THEN 100 ELSE SUM(LOCCCM_TRANSACCIONES_LINEAL_EUROS) END,2) AS COMISION,
				round(CASE WHEN SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO)=0 THEN 0 ELSE SUM(LOCCCM_SESIONES)/SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO) END,2) AS SESIONES_UNPEDIDO,
				round(CASE WHEN SUM(LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM)=0 THEN 0 ELSE SUM(LOCCCM_SESIONES)/SUM(LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM) END,2) AS SESIONES_UNACONVERSION,
				round(CASE WHEN SUM(LOCCC_COSTE)=0 THEN 0 ELSE (SUM(LOCCCM_TRANSACCIONES_LINEAL_EUROS) * (AVG(CASE WHEN LOCCPR_MARGEN IS NULL THEN 100 ELSE LOCCPR_MARGEN END) / 100) - SUM(LOCCC_COSTE)) / SUM(LOCCC_COSTE) * 100 END,2) AS RETORNO_INVERSION_PUBLI,
				round(CASE WHEN SUM(LOCCC_COSTE)=0 THEN 0 ELSE (SUM(LOCCCM_CONVERSIONES_ASISTIDAS_EUROS + LOCCCM_CONVERSIONES_DIRECTAS_EUROS) - SUM(LOCCC_COSTE)) / SUM(LOCCC_COSTE) * 100 END,2) AS RETORNO_INVERSION_PUBLI_CONVERSIONES,				
				round(SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO*100)/SUM(LOCCCM_SESIONES),2) AS TASA_CONVERSION,
				round(SUM((LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM)*100)/SUM(LOCCCM_SESIONES),2) AS TASA_CONVERSION_CONVERSIONES					
		FROM (((LOC_CAMPANAS_COMPARACION_TRANS_MES LEFT JOIN LOC_CAMPANAS ON
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_CAMPANA=LOC_CAMPANAS.LOCC_CAMPAING AND
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_MEDIO=LOC_CAMPANAS.LOCC_MEDIUM AND
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_FUENTE=LOC_CAMPANAS.LOCC_SOURCE )
			  LEFT JOIN LOC_CAMPANAS_COSTEMES ON
				LOC_CAMPANAS.LOCC_CODIGO = LOC_CAMPANAS_COSTEMES.LOCCC_SUCAMPANA AND
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_SUANO = LOC_CAMPANAS_COSTEMES.LOCCC_SUANO AND
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_SUMES = LOC_CAMPANAS_COSTEMES.LOCCC_SUMES )
			  LEFT JOIN LOC_CAMPANAS_PROPIEDAD_MARGEN_MES ON 
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_SUANO = LOC_CAMPANAS_PROPIEDAD_MARGEN_MES.LOCCPR_SUANO AND 
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_SUMES = LOC_CAMPANAS_PROPIEDAD_MARGEN_MES.LOCCPR_SUMES AND 
				LOC_CAMPANAS_PROPIEDAD_MARGEN_MES.LOCCPR_ANALYTICS_ID_PROPIEDAD = (SELECT DISTINCT(LOCD_ANALYTICS_ID_PROPIEDAD) 
																				   FROM LOC_DOMINIOS INNER JOIN LOC_DOMINIOS_VISTAS ON LOC_DOMINIOS.LOCD_CODIGO = LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO
																				   WHERE LOCDV_ANALYTICS_ID_VISTA= LOCCCM_ANALYTICS_ID_VISTA) )
																				   '
		SET @strSql = @strSql + @strSqlAuxSelectFrom + ' 	  
		WHERE LOCCCM_ANALYTICS_ID_VISTA = ' + CAST(@ANALYTICS_ID_VISTA AS VARCHAR(50)) + '

		GROUP BY LOCCCM_SUANO, LOCCCM_SUMES,STR(LOCCCM_SUANO)+STR(LOCCCM_SUMES),LOCCCM_TIPO,
			-- Campo dependiente de la agrupacion.  Si agrupamos por MEDIO, comprobamos si el tipo es SEO,DIRECTO.. (no es CAMPANAS) y devolvemos LOCCCM_CAMPANA 
			'
			IF @AGRUPACION='MEDIO'
			BEGIN
				  SET @strSql = @strSql + N'
					  CASE
							WHEN LOCCCM_TIPO=''CAMPANAS'' THEN LOCCCM_MEDIO 
							WHEN LOCCCM_TIPO<>''CAMPANAS'' THEN LOCCCM_CAMPANA
					  END '
			END
			ELSE
			BEGIN
				  SET @strSql = @strSql + N' LOCCCM_CAMPANA '
			END
			
			SET @strSql = @strSql + N') AS CAMPANAS			

		-- UNIMOS CON LA TABLA DONDE TENEMOS TODAS LAS CAMPAÑAS Y LOS AÑOS-MES ENTRE LAS FECHAS SELECCIONADAS.
		RIGHT JOIN (
			SELECT DISTINCT 
				-- Campo dependiente de la agrupación.  Si agrupamos por MEDIO, comprobamos si el tipo es SEO,DIRECTO.. (no es CAMPANAS)
				'
				IF @AGRUPACION='MEDIO'
				BEGIN
					  SET @strSql = @strSql + N'
						  CASE
								WHEN LOCCCM_TIPO=''CAMPANAS'' THEN LOCCCM_TIPO + LOCCCM_MEDIO 
								WHEN LOCCCM_TIPO<>''CAMPANAS'' THEN LOCCCM_TIPO + LOCCCM_CAMPANA
						  END '
				END
				IF @AGRUPACION='CAMPANA'
				BEGIN
					  SET @strSql = @strSql + N' LOCCCM_TIPO + LOCCCM_CAMPANA '
				END		
				SET @strSql = @strSql + N' AS TIPO_CAMPANA_MAESTRO,
				LOCCCM_TIPO AS TIPO_MAESTRO, 
				-- Campo dependiente de la agrupación.  Si agrupamos por MEDIO, comprobamos si el tipo es SEO,DIRECTO.. (no es CAMPANAS)
				'
				IF @AGRUPACION='MEDIO'
				BEGIN
					  SET @strSql = @strSql + N'
						  CASE
								WHEN LOCCCM_TIPO=''CAMPANAS'' THEN LOCCCM_MEDIO 
								WHEN LOCCCM_TIPO<>''CAMPANAS'' THEN LOCCCM_CAMPANA
						  END '
				END		
				IF @AGRUPACION='CAMPANA'
				BEGIN
					  SET @strSql = @strSql + N' LOCCCM_CAMPANA '
				END			
				SET @strSql = @strSql + N' AS CAMPANA_MAESTRO,							
				ANO,MES
			FROM LOC_CAMPANAS_COMPARACION_TRANS_MES 
				 INNER JOIN 
				 (SELECT year(DATEADD(month, VAUX.number,''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''')) as ANO, month(DATEADD(month, VAUX.number,''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''')) as MES
				  FROM [master].[dbo].[spt_values] VAUX
				  WHERE VAUX.type = ''P'' AND DATEADD(month, VAUX.number, ''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''') <= ''' + CAST(@FECHA_FIN AS VARCHAR(15)) + '''				 
				 ) ANOS_MESES ON 1=1
				 -- UNIMOS CON LOC_CAMPANAS PARA ELIMINAR LAS CAMPAÑAS HISTÓRICAS
				 LEFT JOIN LOC_CAMPANAS ON 
					LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_CAMPANA=LOC_CAMPANAS.LOCC_CAMPAING AND
					LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_MEDIO=LOC_CAMPANAS.LOCC_MEDIUM AND
					LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_FUENTE=LOC_CAMPANAS.LOCC_SOURCE AND
					LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_ANALYTICS_ID_VISTA = ' + CAST(@ANALYTICS_ID_VISTA AS VARCHAR(50)) + '
					 				 
			WHERE LOCCCM_ANALYTICS_ID_VISTA = ' + CAST(@ANALYTICS_ID_VISTA AS VARCHAR(50)) + '
				AND ltrim(str(LOCCCM_SUANO)) + RIGHT(''00''+ ltrim(str(LOCCCM_SUMES)),2) >= ' + CAST(@ANOMES_INICIO AS VARCHAR(15)) + '
				AND ltrim(str(LOCCCM_SUANO)) + RIGHT(''00''+ ltrim(str(LOCCCM_SUMES)),2) <= ' + CAST(@ANOMES_FIN AS VARCHAR(15)) + '
				-- ELIMINAMOS LAS CAMPAÑAS HISTÓRICAS
				AND (LOCC_HISTORICO=0 OR LOCC_HISTORICO IS NULL) ) TODAS_CAMPANAS_ANOS_MESES
				
		ON CAMPANAS.LOCCCM_TIPO+CAMPANAS.LOCCCM_CAMPANA = TODAS_CAMPANAS_ANOS_MESES.TIPO_CAMPANA_MAESTRO  
		   AND CAMPANAS.LOCCCM_SUANO = TODAS_CAMPANAS_ANOS_MESES.ANO 
		   AND CAMPANAS.LOCCCM_SUMES = TODAS_CAMPANAS_ANOS_MESES.MES 
		   ' 
		--------------------------------------------- 

		-----------------------------------------------
		--UNION ALL
		-----------------------------------------------
				
		SET @strSql = @strSql + ' UNION ALL '

		-----------------------------------------------
		--TABLA DE TOTALES ----------------------------
		-----------------------------------------------
		
		SET @strSql = @strSql + '
		SELECT  ANO,MES,
				''TOTAL'' AS LOCCCM_TIPO,
				LOCCCM_SUANO, LOCCCM_SUMES,
				LOCCCM_CAMPANA,NUM_MEDIO_FUENTE,LOCCCM_MEDIO,LOCCCM_FUENTE,			
				LOCCCM_TRANSACCIONES_LINEAL_NUMERO,
				LOCCCM_TRANSACCIONES_LINEAL_EUROS,
				LOCCC_COSTE,
				PEDIDOMEDIO,
				RENDIMIENTO,
				LOCCCM_SESIONES	,
				COSTE_ADQUISICION,
				COMISION,
				TASA_CONVERSION,
				SESIONES_UNPEDIDO,
				RETORNO_INVERSION_PUBLI,
				LOCCCM_CONVERSIONES_NUMERO,
				LOCCCM_CONVERSIONES_EUROS,
				TASA_CONVERSION_CONVERSIONES,
				SESIONES_UNACONVERSION,
				RENDIMIENTO_CONVERSIONES,
				RETORNO_INVERSION_PUBLI_CONVERSIONES,
				COSTE_ADQUISICION_CONVERSION						
		FROM
		(
		SELECT LOCCCM_SUANO,LOCCCM_SUMES,''TOTAL'' AS LOCCCM_TIPO,'''' AS LOCCCM_CAMPANA,NULL AS NUM_MEDIO_FUENTE,'''' AS LOCCCM_MEDIO, '''' AS LOCCCM_FUENTE,		
		'
		SET @strSql = @strSql + @strSqlAuxSelectFrom + ' 
					
		WHERE LOCCCM_ANALYTICS_ID_VISTA = ' + CAST(@ANALYTICS_ID_VISTA AS VARCHAR(50)) + '                    
		GROUP BY LOCCCM_SUANO, LOCCCM_SUMES,STR(LOCCCM_SUANO)+STR(LOCCCM_SUMES) 
		) AS TOTALES 
		
		-- UNIMOS CON LA TABLA DONDE TENEMOS TODOS LOS AÑOS-MES ENTRE LAS FECHAS SELECCIONADAS.
		RIGHT JOIN (SELECT year(DATEADD(month, VAUX.number,''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''')) as ANO, month(DATEADD(month, VAUX.number,''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''')) as MES
					FROM [master].[dbo].[spt_values] VAUX
					WHERE VAUX.type = ''P'' AND DATEADD(month, VAUX.number, ''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''') <= ''' + CAST(@FECHA_FIN AS VARCHAR(15)) + ''' ) ANO_MESES_2
		ON TOTALES.LOCCCM_SUANO = ANO_MESES_2.ANO 
		AND TOTALES.LOCCCM_SUMES = ANO_MESES_2.MES 
		'
		-----------------------------------------------
			    
		SET @strSql = @strSql + ' ORDER BY ANO,MES,TIPO_MAESTRO,CAMPANA_MAESTRO '
	    
	    /*
	    print LEN(@strSql)
		print substring (@strSql,0,4000)	 
		print substring (@strSql,4001,4000)	
		print substring (@strSql,8001,4000)
		print substring (@strSql,12001,4000)
		*/
		
		execute sp_executesql @strSql 
	END 
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- fin TABLA: TODAS LAS CAMPAÑAS AGRUPADAS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	IF @CAMPANA_DESGLOSE <> '' OR @TIPO_DESGLOSE<>''
	BEGIN
		--CAMPOS DE SELECT Y FROM COMUNES A LAS SIGUIENTES DOS LLAMADAS SQL
		SET @strSqlAuxSelectFrom = ' 		
				round(LOCCCM_TRANSACCIONES_LINEAL_NUMERO,2) AS LOCCCM_TRANSACCIONES_LINEAL_NUMERO,
				round(LOCCCM_TRANSACCIONES_LINEAL_EUROS,2) AS LOCCCM_TRANSACCIONES_LINEAL_EUROS,
				round(LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM,2) AS LOCCCM_CONVERSIONES_NUMERO,
				round(LOCCCM_CONVERSIONES_ASISTIDAS_EUROS + LOCCCM_CONVERSIONES_DIRECTAS_EUROS,2) AS LOCCCM_CONVERSIONES_EUROS,				
				LOCCC_COSTE,
				round(CASE WHEN LOCCCM_TRANSACCIONES_LINEAL_NUMERO=0 THEN 0 ELSE LOCCCM_TRANSACCIONES_LINEAL_EUROS/LOCCCM_TRANSACCIONES_LINEAL_NUMERO END,2) AS PEDIDOMEDIO,
				round((CASE WHEN LOCCCM_TRANSACCIONES_LINEAL_EUROS IS NULL THEN 0 ELSE LOCCCM_TRANSACCIONES_LINEAL_EUROS END) * (CASE WHEN LOCCPR_MARGEN IS NULL THEN 100 ELSE LOCCPR_MARGEN END / 100) - (CASE WHEN LOCCC_COSTE IS NULL THEN 0 ELSE LOCCC_COSTE END),2) AS RENDIMIENTO,
				round((LOCCCM_CONVERSIONES_ASISTIDAS_EUROS + LOCCCM_CONVERSIONES_DIRECTAS_EUROS) - CASE WHEN LOCCC_COSTE IS NULL THEN 0 ELSE LOCCC_COSTE END,2) AS RENDIMIENTO_CONVERSIONES,								
				LOCCCM_SESIONES, 
				round(LOCCC_COSTE / (CASE WHEN LOCCCM_TRANSACCIONES_LINEAL_NUMERO = 0 or LOCCCM_TRANSACCIONES_LINEAL_NUMERO IS NULL THEN 1 ELSE LOCCCM_TRANSACCIONES_LINEAL_NUMERO END),2) AS COSTE_ADQUISICION,
				round(LOCCC_COSTE / CASE WHEN (LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM) = 0 THEN 1 ELSE LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM END,2) AS COSTE_ADQUISICION_CONVERSION,
				round(LOCCC_COSTE*100 / CASE WHEN LOCCCM_TRANSACCIONES_LINEAL_EUROS = 0 or LOCCCM_TRANSACCIONES_LINEAL_NUMERO IS NULL THEN 100 ELSE LOCCCM_TRANSACCIONES_LINEAL_EUROS END,2) AS COMISION,
				round(LOCCCM_TRANSACCIONES_LINEAL_NUMERO*100/LOCCCM_SESIONES,2) AS TASA_CONVERSION,
				round((LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM)*100/LOCCCM_SESIONES,2) AS TASA_CONVERSION_CONVERSIONES,
				round(CASE WHEN LOCCCM_TRANSACCIONES_LINEAL_NUMERO=0 THEN 0 ELSE LOCCCM_SESIONES/LOCCCM_TRANSACCIONES_LINEAL_NUMERO END,2) AS SESIONES_UNPEDIDO,
				round(CASE WHEN LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM=0 THEN 0 ELSE LOCCCM_SESIONES/(LOCCCM_CONVERSIONES_ASISTIDAS_NUM + LOCCCM_CONVERSIONES_DIRECTAS_NUM) END,2) AS SESIONES_UNACONVERSION,			
				round(CASE WHEN LOCCC_COSTE=0 THEN 0 ELSE (LOCCCM_TRANSACCIONES_LINEAL_EUROS * (CASE WHEN LOCCPR_MARGEN IS NULL THEN 100 ELSE LOCCPR_MARGEN END / 100) - LOCCC_COSTE) / LOCCC_COSTE * 100 END,2) AS RETORNO_INVERSION_PUBLI,
				round(CASE WHEN LOCCC_COSTE=0 THEN 0 ELSE ((LOCCCM_CONVERSIONES_ASISTIDAS_EUROS + LOCCCM_CONVERSIONES_DIRECTAS_EUROS) - LOCCC_COSTE) / LOCCC_COSTE * 100 END,2) AS RETORNO_INVERSION_PUBLI_CONVERSIONES								
				
		FROM (((LOC_CAMPANAS_COMPARACION_TRANS_MES LEFT JOIN LOC_CAMPANAS ON
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_CAMPANA=LOC_CAMPANAS.LOCC_CAMPAING AND
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_MEDIO=LOC_CAMPANAS.LOCC_MEDIUM AND
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_FUENTE=LOC_CAMPANAS.LOCC_SOURCE )
			  LEFT JOIN LOC_CAMPANAS_COSTEMES ON
				LOC_CAMPANAS.LOCC_CODIGO = LOC_CAMPANAS_COSTEMES.LOCCC_SUCAMPANA AND
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_SUANO = LOC_CAMPANAS_COSTEMES.LOCCC_SUANO AND
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_SUMES = LOC_CAMPANAS_COSTEMES.LOCCC_SUMES )
			  LEFT JOIN LOC_CAMPANAS_PROPIEDAD_MARGEN_MES ON 
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_SUANO = LOC_CAMPANAS_PROPIEDAD_MARGEN_MES.LOCCPR_SUANO AND 
				LOC_CAMPANAS_COMPARACION_TRANS_MES.LOCCCM_SUMES = LOC_CAMPANAS_PROPIEDAD_MARGEN_MES.LOCCPR_SUMES AND 
				LOC_CAMPANAS_PROPIEDAD_MARGEN_MES.LOCCPR_ANALYTICS_ID_PROPIEDAD = (SELECT DISTINCT(LOCD_ANALYTICS_ID_PROPIEDAD) 
																				   FROM LOC_DOMINIOS INNER JOIN LOC_DOMINIOS_VISTAS ON LOC_DOMINIOS.LOCD_CODIGO = LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO
																				   WHERE LOCDV_ANALYTICS_ID_VISTA= LOCCCM_ANALYTICS_ID_VISTA) )
			   '
	END

	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- TABLA: DESGLOSE DE UNA CAMPAÑA ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	IF @CAMPANA_DESGLOSE <> ''
	BEGIN
		
		-----------------------------------------------
		--TABLA DE CAMPAÑAS ---------------------------
		-----------------------------------------------
		SET @strSql = '
		SELECT  ANO AS ANO,MES AS MES,
				TIPO_MAESTRO,
				LOCCCM_SUANO, LOCCCM_SUMES,
				CAMPANA_MAESTRO,NUM_MEDIO_FUENTE,
				TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.LOCCCM_MEDIO,
				TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.LOCCCM_FUENTE,
				LOCCCM_TRANSACCIONES_LINEAL_NUMERO,
				LOCCCM_TRANSACCIONES_LINEAL_EUROS,
				LOCCC_COSTE,
				PEDIDOMEDIO,
				RENDIMIENTO,
				LOCCCM_SESIONES,
				COSTE_ADQUISICION,
				COMISION,
				TASA_CONVERSION,
				SESIONES_UNPEDIDO,
				RETORNO_INVERSION_PUBLI,
				LOCCCM_CONVERSIONES_NUMERO,
				LOCCCM_CONVERSIONES_EUROS,
				TASA_CONVERSION_CONVERSIONES,
				SESIONES_UNACONVERSION,
				RENDIMIENTO_CONVERSIONES,
				RETORNO_INVERSION_PUBLI_CONVERSIONES,
				COSTE_ADQUISICION_CONVERSION							
		FROM
		(	
		SELECT LOCCCM_SUANO, LOCCCM_SUMES,LOCCCM_TIPO,LOCCCM_CAMPANA,NULL AS NUM_MEDIO_FUENTE, LOCCCM_MEDIO, LOCCCM_FUENTE, '
			   SET @strSql = @strSql + @strSqlAuxSelectFrom + '
			   WHERE LOCCCM_ANALYTICS_ID_VISTA = ' + CAST(@ANALYTICS_ID_VISTA AS VARCHAR(50))  
			   
			   -- Condición según la agrupación
			   IF @AGRUPACION='MEDIO'
			   BEGIN
					  SET @strSql = @strSql + N' AND LOCCCM_MEDIO = ''' + @CAMPANA_DESGLOSE + ''''
			   END	
			   IF @AGRUPACION='CAMPANA'
			   BEGIN
					  SET @strSql = @strSql + N' AND LOCCCM_CAMPANA = ''' + @CAMPANA_DESGLOSE + ''''
			   END			   
			   SET @strSql = @strSql + N' ) AS CAMPANAS '	
	   
		SET @strSql = @strSql + N'
		-- UNIMOS CON LA TABLA DONDE TENEMOS TODAS LAS CAMPAÑAS Y LOS AÑOS-MES ENTRE LAS FECHAS SELECCIONADAS.
		RIGHT JOIN (
			SELECT DISTINCT LOCCCM_TIPO + LOCCCM_CAMPANA AS TIPO_CAMPANA_MAESTRO, LOCCCM_TIPO AS TIPO_MAESTRO, LOCCCM_CAMPANA AS CAMPANA_MAESTRO, LOCCCM_MEDIO, LOCCCM_FUENTE ,ANO,MES
			FROM LOC_CAMPANAS_COMPARACION_TRANS_MES 
				 INNER JOIN 
				 (SELECT year(DATEADD(month, VAUX.number,''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''')) as ANO, month(DATEADD(month, VAUX.number,''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''')) as MES
				  FROM [master].[dbo].[spt_values] VAUX
				  WHERE VAUX.type = ''P'' AND DATEADD(month, VAUX.number, ''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''') <= ''' + CAST(@FECHA_FIN AS VARCHAR(15)) + '''				 
				 ) ANOS_MESES ON 1=1
			WHERE LOCCCM_ANALYTICS_ID_VISTA = ' + CAST(@ANALYTICS_ID_VISTA AS VARCHAR(50))
			   -- Condición según la agrupación
			   IF @AGRUPACION='MEDIO'
			   BEGIN
					  SET @strSql = @strSql + N' AND LOCCCM_MEDIO = ''' + @CAMPANA_DESGLOSE + ''''
			   END	
			   IF @AGRUPACION='CAMPANA'
			   BEGIN
					  SET @strSql = @strSql + N' AND LOCCCM_CAMPANA = ''' + @CAMPANA_DESGLOSE + ''''
			   END			   
			   SET @strSql = @strSql + N'			
				AND ltrim(str(LOCCCM_SUANO)) + RIGHT(''00''+ ltrim(str(LOCCCM_SUMES)),2) >= ' + CAST(@ANOMES_INICIO AS VARCHAR(15)) + '
				AND ltrim(str(LOCCCM_SUANO)) + RIGHT(''00''+ ltrim(str(LOCCCM_SUMES)),2) <= ' + CAST(@ANOMES_FIN AS VARCHAR(15)) + '
				AND LOCCCM_TIPO=''CAMPANAS'' 				
				) TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES				
				
		ON CAMPANAS.LOCCCM_TIPO+CAMPANAS.LOCCCM_CAMPANA = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.TIPO_CAMPANA_MAESTRO  
		   AND CAMPANAS.LOCCCM_MEDIO = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.LOCCCM_MEDIO
		   AND CAMPANAS.LOCCCM_FUENTE = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.LOCCCM_FUENTE  
		   AND CAMPANAS.LOCCCM_SUANO = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.ANO 
		   AND CAMPANAS.LOCCCM_SUMES = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.MES
		   ' 		   
		---------------------------------------------
		SET @strSql = @strSql + '
		ORDER BY ANO,MES,TIPO_MAESTRO,CAMPANA_MAESTRO, LOCCCM_MEDIO,LOCCCM_FUENTE '

	    /*
	    print LEN(@strSql)
		print substring (@strSql,0,4000)	 
		print substring (@strSql,4001,4000)	
		print substring (@strSql,8001,4000)	
		*/
		execute sp_executesql @strSql 
				
	END
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- fin TABLA: DESGLOSE DE UNA CAMPAÑA ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- TABLA: DESGLOSE DE UN TIPO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	IF @TIPO_DESGLOSE <> ''
	BEGIN

		--------------------------------------------------------------------------
		--TABLA DE REGISTROS DE TIPOS SEO,RRSS,ENLACES ---------------------------
		--------------------------------------------------------------------------
		SET @strSql = '
		SELECT  ANO AS ANO,MES AS MES,
				TIPO_MAESTRO,
				LOCCCM_SUANO, LOCCCM_SUMES,
				CAMPANA_MAESTRO,NUM_MEDIO_FUENTE,
				TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.LOCCCM_MEDIO,
				TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.LOCCCM_FUENTE,
				LOCCCM_TRANSACCIONES_LINEAL_NUMERO,
				LOCCCM_TRANSACCIONES_LINEAL_EUROS,
				LOCCC_COSTE,
				PEDIDOMEDIO,
				RENDIMIENTO,
				LOCCCM_SESIONES,
				COSTE_ADQUISICION,
				COMISION,
				TASA_CONVERSION,
				SESIONES_UNPEDIDO,
				RETORNO_INVERSION_PUBLI,
				ORDEN1_TRANSACCIONES_LINEAL_NUMERO, 
				ORDEN2_SESIONES,
				LOCCCM_CONVERSIONES_NUMERO,
				LOCCCM_CONVERSIONES_EUROS,
				TASA_CONVERSION_CONVERSIONES,
				SESIONES_UNACONVERSION,
				RENDIMIENTO_CONVERSIONES,
				RETORNO_INVERSION_PUBLI_CONVERSIONES,
				COSTE_ADQUISICION_CONVERSION		
		FROM
		(	
		SELECT LOCCCM_SUANO, LOCCCM_SUMES,LOCCCM_TIPO,LOCCCM_CAMPANA,NULL AS NUM_MEDIO_FUENTE, LOCCCM_MEDIO, LOCCCM_FUENTE, 
		'
	    SET @strSql = @strSql + @strSqlAuxSelectFrom + '
	    WHERE LOCCCM_ANALYTICS_ID_VISTA = ' + CAST(@ANALYTICS_ID_VISTA AS VARCHAR(50)) + ' 
			  AND LOCCCM_TIPO = ''' + @TIPO_DESGLOSE + '''                              
		) AS CAMPANAS 

		-- UNIMOS CON LA TABLA DONDE TENEMOS TODAS LAS CAMPAÑAS Y LOS AÑOS-MES ENTRE LAS FECHAS SELECCIONADAS.  SELECCIONAMOS LAS FUENTES CON MÁS TRANSACCIONES Y SESIONES
		RIGHT JOIN (		
				
			SELECT LOCCCM_TIPO + LOCCCM_CAMPANA AS TIPO_CAMPANA_MAESTRO, LOCCCM_TIPO AS TIPO_MAESTRO, LOCCCM_CAMPANA AS CAMPANA_MAESTRO, LOCCCM_MEDIO, LOCCCM_FUENTE ,ANO,MES, ORDEN1_TRANSACCIONES_LINEAL_NUMERO, ORDEN2_SESIONES
			FROM 
				(SELECT TOP 10 LOCCCM_TIPO,LOCCCM_CAMPANA,LOCCCM_MEDIO,LOCCCM_FUENTE,SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO) AS ORDEN1_TRANSACCIONES_LINEAL_NUMERO,SUM(LOCCCM_SESIONES) AS ORDEN2_SESIONES
				 FROM LOC_CAMPANAS_COMPARACION_TRANS_MES
				 WHERE  LOCCCM_ANALYTICS_ID_VISTA = ' + CAST(@ANALYTICS_ID_VISTA AS VARCHAR(50)) + '
					AND LOCCCM_TIPO = ''' + @TIPO_DESGLOSE + '''      
					AND ltrim(str(LOCCCM_SUANO)) + RIGHT(''00''+ ltrim(str(LOCCCM_SUMES)),2) >= ' + CAST(@ANOMES_INICIO AS VARCHAR(15)) + '
					AND ltrim(str(LOCCCM_SUANO)) + RIGHT(''00''+ ltrim(str(LOCCCM_SUMES)),2) <= ' + CAST(@ANOMES_FIN AS VARCHAR(15)) + '
				 GROUP BY LOCCCM_TIPO,LOCCCM_CAMPANA,LOCCCM_MEDIO,LOCCCM_FUENTE
				 ORDER BY SUM(LOCCCM_TRANSACCIONES_LINEAL_NUMERO) DESC, SUM(LOCCCM_SESIONES) DESC) AS CAMPANAS_FILTRADAS
			
				 INNER JOIN 
				 
				(SELECT year(DATEADD(month, VAUX.number,''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''')) as ANO, month(DATEADD(month, VAUX.number,''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''')) as MES
				 FROM [master].[dbo].[spt_values] VAUX
				 WHERE VAUX.type = ''P'' AND DATEADD(month, VAUX.number, ''' + CAST(@FECHA_INICIO AS VARCHAR(15)) + ''') <= ''' + CAST(@FECHA_FIN AS VARCHAR(15)) + ''') ANOS_MESES 
				 
				 ON 1=1		
				
			) TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES
		ON CAMPANAS.LOCCCM_TIPO+CAMPANAS.LOCCCM_CAMPANA = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.TIPO_CAMPANA_MAESTRO  
		   AND CAMPANAS.LOCCCM_MEDIO = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.LOCCCM_MEDIO
		   AND CAMPANAS.LOCCCM_FUENTE = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.LOCCCM_FUENTE  
		   AND CAMPANAS.LOCCCM_SUANO = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.ANO 
		   AND CAMPANAS.LOCCCM_SUMES = TODAS_CAMPANAS_MEDIOS_FUENTES_ANOS_MESES.MES '
		---------------------------------------------
		
		SET @strSql = @strSql + '
		ORDER BY ANO,MES,TIPO_MAESTRO,ORDEN1_TRANSACCIONES_LINEAL_NUMERO DESC, ORDEN2_SESIONES DESC '

	    print LEN(@strSql)
		print substring (@strSql,0,4000)	 
		print substring (@strSql,4001,4000)	
		print substring (@strSql,8001,4000)	
		
		execute sp_executesql @strSql 		
		
	END
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- fin TABLA: DESGLOSE DE UN TIPO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- TABLA: MESES CON DATOS DE TRANSACCIONES DESACTUALIZADOS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	SELECT ANOS_MESES.ANO,ANOS_MESES.MES,LOCCF_FECHA_ULTIMO_FICHERO_TRANSACCIONES
	FROM LOC_CAMPANAS_COMPARACION_TRANS_FECHAS	
		 -- SÓLO LOS MESES ENTRE LAS FECHAS DADAS
		 RIGHT JOIN 
		 (SELECT year(DATEADD(month, VAUX.number,@FECHA_INICIO)) as ANO, month(DATEADD(month, VAUX.number,@FECHA_INICIO)) as MES
		  FROM [master].[dbo].[spt_values] VAUX
		  WHERE VAUX.type = 'P' AND DATEADD(month, VAUX.number, @FECHA_INICIO) <= @FECHA_FIN				 
		 ) ANOS_MESES 
		 ON ANOS_MESES.ANO = LOC_CAMPANAS_COMPARACION_TRANS_FECHAS.LOCCF_SUANO
		    AND ANOS_MESES.MES = LOC_CAMPANAS_COMPARACION_TRANS_FECHAS.LOCCF_SUMES		
	WHERE LOCCF_ANALYTICS_ID_VISTA = @ANALYTICS_ID_VISTA
		-- Comprobamos que la fecha del fichero sea el día 2 del mes siguiente al mes del dato (EN REALIDAD MOSTRAMOS AQUELLOS DATOS QUE NO ESTÁN ACTUALIZADOS A PARTIR DEL DÍA 2 DEL SIGUIENTE MES AL DATO)
		AND ( LOCCF_FECHA_ULTIMO_FICHERO_TRANSACCIONES < dateadd(d,1,dateadd(m,1,ltrim(str(LOCCF_SUANO)) + RIGHT('00'+ ltrim(str(LOCCF_SUMES)),2) + '01'))
			  OR LOCCF_FECHA_ULTIMO_FICHERO_TRANSACCIONES IS NULL)
			  
	UNION ALL 
		                 
	--AÑADIMOS LOS AÑOS-MESES QUE NO ESTÁN CREADOS EN LOC_CAMPANAS_COMPARACION_TRANS_FECHAS
	SELECT ANOS_MESES.ANO,ANOS_MESES.MES,NULL AS LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES
	FROM (SELECT year(DATEADD(month, VAUX.number,@FECHA_INICIO)) as ANO, month(DATEADD(month, VAUX.number,@FECHA_INICIO)) as MES 
		  FROM [master].[dbo].[spt_values] VAUX WHERE VAUX.type = 'P' AND DATEADD(month, VAUX.number, @FECHA_INICIO) <= @FECHA_FIN ) ANOS_MESES 
	WHERE NOT EXISTS (SELECT LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES
					  FROM LOC_CAMPANAS_COMPARACION_TRANS_FECHAS
					  WHERE LOCCF_ANALYTICS_ID_VISTA = @ANALYTICS_ID_VISTA
						AND LOC_CAMPANAS_COMPARACION_TRANS_FECHAS.LOCCF_SUANO = ANOS_MESES.ANO 
						AND LOC_CAMPANAS_COMPARACION_TRANS_FECHAS.LOCCF_SUMES = ANOS_MESES.MES ) 	
	
	ORDER BY ANO, MES		  
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- FIN TABLA: MESES CON DATOS DE TRANSACCIONES DESACTUALIZADOS +++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- TABLA: MESES SIN MARGENES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	  SELECT year(DATEADD(month, VAUX.number,@FECHA_INICIO)) as ANO, month(DATEADD(month, VAUX.number,@FECHA_INICIO)) as MES
	  FROM [master].[dbo].[spt_values] VAUX
	  WHERE VAUX.type = 'P' AND DATEADD(month, VAUX.number, @FECHA_INICIO) <= @FECHA_FIN	
	  AND NOT EXISTS(SELECT LOCCPR_MARGEN 
					 FROM LOC_CAMPANAS_PROPIEDAD_MARGEN_MES 
					 WHERE LOCCPR_SUANO = year(DATEADD(month, VAUX.number,@FECHA_INICIO))
				       AND LOCCPR_SUMES = month(DATEADD(month, VAUX.number,@FECHA_INICIO)) 
				       AND LOCCPR_ANALYTICS_ID_PROPIEDAD =  (SELECT DISTINCT(LOCD_ANALYTICS_ID_PROPIEDAD) 
															 FROM LOC_DOMINIOS INNER JOIN LOC_DOMINIOS_VISTAS ON LOC_DOMINIOS.LOCD_CODIGO = LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO
															 WHERE LOCDV_ANALYTICS_ID_VISTA= @ANALYTICS_ID_VISTA)
				       )
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- FIN TABLA: MESES SIN MARGENES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


END
GO
/****** Object:  Default [DF__CAMPANAS__CAMP_S__03F0984C]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CAMPANAS] ADD  DEFAULT ((0)) FOR [CAMP_SUPROVINCIA]
GO
/****** Object:  Default [DF__CAMPANAS__CAMP_S__04E4BC85]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CAMPANAS] ADD  DEFAULT ((0)) FOR [CAMP_SUPERFIL]
GO
/****** Object:  Default [DF__CLIENTES__CLIE_C__787EE5A0]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CLIENTES] ADD  CONSTRAINT [DF__CLIENTES__CLIE_C__787EE5A0]  DEFAULT ((0)) FOR [CLIE_CODIGO]
GO
/****** Object:  Default [DF__CLIENTES__CLIE_C__797309D9]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CLIENTES] ADD  CONSTRAINT [DF__CLIENTES__CLIE_C__797309D9]  DEFAULT ((0)) FOR [CLIE_COMERCIAL]
GO
/****** Object:  Default [DF__CLIENTES__CLIE_P__7A672E12]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CLIENTES] ADD  CONSTRAINT [DF__CLIENTES__CLIE_P__7A672E12]  DEFAULT ((0)) FOR [CLIE_PROVINCIA]
GO
/****** Object:  Default [DF__CLIENTES__CLIE_D__7B5B524B]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CLIENTES] ADD  CONSTRAINT [DF__CLIENTES__CLIE_D__7B5B524B]  DEFAULT ((0)) FOR [CLIE_DISTRIBUIDOR]
GO
/****** Object:  Default [DF__CLIENTES___CLIPR__71D1E811]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CLIENTES_PRODUCTOS] ADD  DEFAULT ((0)) FOR [CLIPRO_CODIGO]
GO
/****** Object:  Default [DF__CLIENTES___CLIPR__72C60C4A]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CLIENTES_PRODUCTOS] ADD  DEFAULT ((0)) FOR [CLIPRO_SUCLIENTE]
GO
/****** Object:  Default [DF__CLIENTES___CLIPR__73BA3083]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[CLIENTES_PRODUCTOS] ADD  DEFAULT ((0)) FOR [CLIPRO_SUPRODUCTO]
GO
/****** Object:  Default [DF__ISP_DOMIN__DOM_C__182C9B23]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[ISP_DOMINIOS] ADD  DEFAULT ((0)) FOR [DOM_CODIGO]
GO
/****** Object:  Default [DF__ISP_DOMIN__DOM_S__1920BF5C]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[ISP_DOMINIOS] ADD  DEFAULT ((0)) FOR [DOM_SUCLIENTE]
GO
/****** Object:  Default [DF__ISP_DOMIN__DOM_P__1A14E395]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[ISP_DOMINIOS] ADD  DEFAULT ((0)) FOR [DOM_PUBLICARWEB]
GO
/****** Object:  Default [DF__LOC_BUSCA__LOCBA__09DE7BCC]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_BUSCADORESAUX] ADD  DEFAULT ((0)) FOR [LOCBA_COINCIDENCIAS]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___690797E6]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___690797E6]  DEFAULT ((0)) FOR [LOCD_ENVIARINFORMES]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___69FBBC1F]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___69FBBC1F]  DEFAULT ((0)) FOR [LOCD_DESHABILITAREMAILFORMULARIOS]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___6AEFE058]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___6AEFE058]  DEFAULT ((0)) FOR [LOCD_URLGOOGLE]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___6BE40491]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___6BE40491]  DEFAULT ((0)) FOR [LOCD_URLYAHOO]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___6CD828CA]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___6CD828CA]  DEFAULT ((0)) FOR [LOCD_URLLIVE]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___6DCC4D03]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___6DCC4D03]  DEFAULT ((0)) FOR [LOCD_SITEMAPGOOGLE]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___6EC0713C]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___6EC0713C]  DEFAULT ((0)) FOR [LOCD_SITEMAPYAHOO]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___6FB49575]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___6FB49575]  DEFAULT ((0)) FOR [LOCD_SITEMAPLIVE]
GO
/****** Object:  Default [DF__LOC_DOMIN__LOCD___70A8B9AE]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_DOMINIOS] ADD  CONSTRAINT [DF__LOC_DOMIN__LOCD___70A8B9AE]  DEFAULT ((0)) FOR [LOCD_SITEMAPASK]
GO
/****** Object:  Default [DF__LOC_RESUL__LOCR___39237A9A]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_RESULTADOS] ADD  CONSTRAINT [DF__LOC_RESUL__LOCR___39237A9A]  DEFAULT ((0)) FOR [LOCR_SUDOMINIO]
GO
/****** Object:  Default [DF__LOC_RESUL__LOCR___3A179ED3]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_RESULTADOS] ADD  CONSTRAINT [DF__LOC_RESUL__LOCR___3A179ED3]  DEFAULT ((0)) FOR [LOCR_GOOGLE]
GO
/****** Object:  Default [DF_LOC_USUARIOS_CLIENTES_LOCUC_ESADMINISTRADOR]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_USUARIOS_CLIENTES] ADD  CONSTRAINT [DF_LOC_USUARIOS_CLIENTES_LOCUC_ESADMINISTRADOR]  DEFAULT ((0)) FOR [LOCUC_ESADMINISTRADOR]
GO
/****** Object:  Default [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_PERFIL]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_USUARIOS_CLIENTES] ADD  CONSTRAINT [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_PERFIL]  DEFAULT ((1)) FOR [LOCUC_ACCESO_PERFIL]
GO
/****** Object:  Default [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_HISTORICO]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_USUARIOS_CLIENTES] ADD  CONSTRAINT [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_HISTORICO]  DEFAULT ((1)) FOR [LOCUC_ACCESO_HISTORICO]
GO
/****** Object:  Default [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_INFORME]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_USUARIOS_CLIENTES] ADD  CONSTRAINT [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_INFORME]  DEFAULT ((1)) FOR [LOCUC_ACCESO_M_INFORME]
GO
/****** Object:  Default [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_SEO]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_USUARIOS_CLIENTES] ADD  CONSTRAINT [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_SEO]  DEFAULT ((1)) FOR [LOCUC_ACCESO_M_SEO]
GO
/****** Object:  Default [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_ECOMMERCE]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_USUARIOS_CLIENTES] ADD  CONSTRAINT [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_ECOMMERCE]  DEFAULT ((1)) FOR [LOCUC_ACCESO_M_ECOMMERCE]
GO
/****** Object:  Default [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_CAMPANAS]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_USUARIOS_CLIENTES] ADD  CONSTRAINT [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_CAMPANAS]  DEFAULT ((1)) FOR [LOCUC_ACCESO_M_CAMPANAS]
GO
/****** Object:  Default [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_CUADROMANDO]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[LOC_USUARIOS_CLIENTES] ADD  CONSTRAINT [DF_LOC_USUARIOS_CLIENTES_LOCUC_ACCESO_M_CUADROMANDO]  DEFAULT ((1)) FOR [LOCUC_ACCESO_M_CUADROMANDO]
GO
/****** Object:  Default [DF__POS_POSIC__POS_S__00DF2177]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[POS_POSICIONES] ADD  DEFAULT ((0)) FOR [POS_SUTERMINO]
GO
/****** Object:  Default [DF__POS_POSIC__POS_P__01D345B0]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[POS_POSICIONES] ADD  DEFAULT ((0)) FOR [POS_POSICIONGOOGLE]
GO
/****** Object:  Default [DF__POS_TERMI__TERM___756D6ECB]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[POS_TERMINOS] ADD  CONSTRAINT [DF__POS_TERMI__TERM___756D6ECB]  DEFAULT ((0)) FOR [TERM_DOMINIO]
GO
/****** Object:  Default [DF__POS_TERMI__TERM___76619304]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[POS_TERMINOS] ADD  CONSTRAINT [DF__POS_TERMI__TERM___76619304]  DEFAULT ((0)) FOR [TERM_PRUEBA]
GO
/****** Object:  Default [DF__PROCESOS__PROC_N__6442E2C9]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[PROCESOS] ADD  DEFAULT ((0)) FOR [PROC_NUMERO]
GO
/****** Object:  Default [DF__PROYECTOS__PROY___58D1301D]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[PROYECTOS] ADD  DEFAULT ((0)) FOR [PROY_CODIGO]
GO
/****** Object:  Default [DF__PROYECTOS__PROY___59C55456]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[PROYECTOS] ADD  DEFAULT ((0)) FOR [PROY_SUCLIENTE]
GO
/****** Object:  Default [DF__PROYECTOS__PROY___5AB9788F]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[PROYECTOS] ADD  DEFAULT ((0)) FOR [PROY_SUCONTACTO]
GO
/****** Object:  Default [DF__PROYECTOS__PROY___5BAD9CC8]    Script Date: 07/22/2015 14:21:46 ******/
ALTER TABLE [dbo].[PROYECTOS] ADD  DEFAULT ((0)) FOR [PROY_SUANALISTA]
GO

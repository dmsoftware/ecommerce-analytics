<?php

class CacheDatosApi {
	protected $apiObj;
	protected $apiType;
  protected $data = false;
  protected $id = 0;
  protected $fInicio;
  protected $fFin;
  protected $hEstable = 24;
  protected $hVariable = 1;

	public function __construct($apiObj, $apiType = 'ga'){
		$this->apiObj = array(
      'LOCCA_ID_VISTA' => str_replace('ga:', '', $apiObj->idVista),
      'LOCCA_START_DATE' => $apiObj->startdate,
      'LOCCA_END_DATE' => $apiObj->enddate,
      'LOCCA_METRICS' => $apiObj->metrics,
      'LOCCA_OPT_PARAMS' => $apiObj->optParams ? json_encode($apiObj->optParams) : ''
    );
		$this->apiType = $apiType;
    $this->fInicio = $this->microtime_float();
	}

	public function isCached(){
    $isCached = false;

    $this->getCacheData();

    if($this->data){
      $isCached = true;
      $this->updateData();
    }elseif ($this->id) {
      $this->updateData(true);
    }

    return $isCached;
	}

  private function getCacheData(){
    $pValues = array();
    $c = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);

    $sql = "SELECT TOP 1 * FROM dbo.LOC_CACHE WHERE 1=1 ";
    
    foreach(array_keys($this->apiObj) as $col){
      $sql .= " AND {$col} = :{$col} ";
      $pValues[':'.$col] = $this->apiObj[$col];
    }
    
    $stmt = $c->prepare($sql);
    $result = $stmt->execute($pValues);
    
    if ($result && $stmt->rowCount() != 0) {
      while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $this->id = $fila['LOCCA_ID'];

        if($fila['LOCCA_FECHA_CADUCIDAD'] == '' || (strtotime($fila['LOCCA_FECHA_CADUCIDAD']) > strtotime('now'))){
          $this->data = unserialize($fila['LOCCA_DATA']);
        }
      }
    }
  }

  public function setData($data){
    $c = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);

    if($this->id){
      $sql = "UPDATE dbo.LOC_CACHE SET LOCCA_DATA = :data, LOCCA_CONTADOR_LLAMADAS = LOCCA_CONTADOR_LLAMADAS + 1 WHERE LOCCA_ID = :id";
      
      $pValues = array(
        ':data' => serialize($data), 
        ':id' => $this->id
      );
    }else{
      $this->fFin = $this->microtime_float();

      $cols = array_keys($this->apiObj);
      $pValues = array(
        ':tipo' => $this->apiType,
        ':fecha_creacion' => date('Y-m-d H:i:s.000'),
        ':data' => serialize($data),
        ':duracion' => round($this->fFin - $this->fInicio, 2),
        ':fecha_caducidad' => null,
        ':url_ref' => $_SERVER['HTTP_REFERER']
      );

      if(strtotime($pValues[':fecha_creacion'] . ' - ' . $this->hEstable . ' hours') < strtotime($this->apiObj['LOCCA_END_DATE']. '23:59:59')){
        $pValues[':fecha_caducidad'] = date('Y-m-d H:i:s.000', strtotime($pValues[':fecha_creacion'] . ' + ' . $this->hVariable . ' hours'));
      }

      foreach($this->apiObj as $col => $value){
        $pValues[':'.$col] = $value;
      }

      $sql = "INSERT INTO dbo.LOC_CACHE ( " . implode(' , ', $cols) . " , LOCCA_TIPO , LOCCA_FECHA_CREACION , LOCCA_DATA , LOCCA_CONTADOR, LOCCA_DURACION , LOCCA_FECHA_CADUCIDAD , LOCCA_URL_REFERENCIA, LOCCA_CONTADOR_LLAMADAS ) ";
      $sql .= "VALUES ( :" . implode(' , :', $cols) . " , :tipo , :fecha_creacion , :data , 1 , :duracion , :fecha_caducidad , :url_ref , 1 )";
    }

    $stmt = $c->prepare($sql);
    $result = $stmt->execute($pValues);

    $this->data = $data;
  }

  public function updateData($refresh = false){
    $pValues = array(':ultimo_uso' => date('Y-m-d H:i:s.000'), ':id' => $this->id);

    $c = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);
    $sql = "UPDATE dbo.LOC_CACHE SET LOCCA_ULTIMO_USO = :ultimo_uso, LOCCA_CONTADOR = LOCCA_CONTADOR + 1";

    if($refresh){
      $sql .= ", LOCCA_FECHA_CREACION = :fecha_creacion, LOCCA_FECHA_CADUCIDAD = :fecha_caducidad";
      $pValues[':fecha_creacion'] = date('Y-m-d H:i:s.000');
      $pValues[':fecha_caducidad'] = date('Y-m-d H:i:s.000', strtotime($pValues[':fecha_creacion'] . ' + ' . $this->hVariable . ' hours'));
    }

    $sql .=" WHERE LOCCA_ID = :id";

    $stmt = $c->prepare($sql);
    $result = $stmt->execute($pValues);
  }

  public function getData(){
    return $this->data;
  }

  function microtime_float(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
  }
}
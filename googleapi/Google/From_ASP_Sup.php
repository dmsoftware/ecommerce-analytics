﻿<?php
// Controlamos mínimamente que la llamada es válida. User_agent="DM-Integra"
if ($_SERVER["HTTP_USER_AGENT"] != "DM-Integra" ) {
  echo 'error';
  exit();
}

require_once ('Google/Client.php');
require_once ('Google/Service/Analytics.php');
session_start();

echo('Fin');
exit();
//---------------------------------------------------------------------------------------------------------------
// Declaramos nuestra configuración -----------------------------------------------------------------------------
// Tenemos 2 proyectos para evitar las restricciones.  Ponemos las dos configuraciones.
switch ($_POST['proyecto']) {
        
case "1":
      
  // proyecto DMIntegra
  $googleApi = array(
	  'id' => "1084948507911-172rem69lbfufrbpbscfs5a9rk665glr.apps.googleusercontent.com", // Id que nos ha dado la APIs Console
	  'email' => "1084948507911-172rem69lbfufrbpbscfs5a9rk665glr@developer.gserviceaccount.com", // email que nos ha dado la APIs Console
	  'keyFile' => "ae59d70932d41cec8201e6bf7c2c5b9041dbad67-privatekey.p12", // nombre del fichero llave
    'nombre_app' => "DMIntegra",
  ); 
  break;
  
case "2":

  // proyecto DMIntegra-2
  $googleApi = array(
	  'id' => "89086662620-pbuhmvg724cn49m5h9vlr469jk8h6ss9.apps.googleusercontent.com", // Id que nos ha dado la APIs Console
	  'email' => "89086662620-pbuhmvg724cn49m5h9vlr469jk8h6ss9@developer.gserviceaccount.com", // email que nos ha dado la APIs Console
	  'keyFile' => "DMIntegra-2-07e273dc6e28.p12", // nombre del fichero llave
    'nombre_app' => "DMIntegra-2",
  ); 
  break;
  
} //Cierre switch --------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------
//Dependiendo de la llamada asignaremos un permiso u otro al cliente --------------------------------------------
switch ($_POST['tipo']) {
        
case "GOALS_INSERT":
case "GOALS_UPDATE":
   $strPermiso = 'https://www.googleapis.com/auth/analytics.edit';
   break;
    
case "PROPERTY_USERS":
   $strPermiso = 'https://www.googleapis.com/auth/analytics.manage.users';
   break;
   
default:
   $strPermiso = 'https://www.googleapis.com/auth/analytics.readonly';
   break;
   
} //Cierre switch --------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------

// Creamos el cliente de conexión
$client = new Google_Client();
$client->setApplicationName($googleApi['nombre_app']);
$client->setAssertionCredentials(
	new Google_Auth_AssertionCredentials(
		$googleApi['email'],
		array($strPermiso),  
		file_get_contents($googleApi['keyFile'])
	)
);
$client->setClientId($googleApi['id']);
$client->setAccessType('offline_access');
 
// y con este cliente creamos el objeto para lanzar consultas a la API
$service = new Google_Service_Analytics($client);
?>

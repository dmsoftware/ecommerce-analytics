<?php
require_once ($_SERVER['DOCUMENT_ROOT']."\public\includes\conf.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\Google\Client.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\Google\Service\Analytics.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\CacheDatosApi.php");

//Clase para el objeto "llamada a la api de administración"
//Partiendo de las propiedades de creación, llamamos a la api de google y creamos las funciones para tratar los datos devueltos de forma "amigable"
class Datos_Api_Admin {
    
	//Propiedades de creación --------------------------------------------------------
	var $strProyectoAsociado;
	var $idVista; 
	var $TipoLlamada;

	//Propiedades de uso interno --------------------------------------------------------
	var $Google_Service_Analytics_GaData; //Contenedor de la resupuesta de las llamadas al API
	var $idPropiedad;
	var $idCuenta;

	//----------------------------------------------------------------------------------------------------------------------------
	//Funcion constructora.  Dadas las propiedades de entrada llamamos a la api de Google y dejamos el resultado en $Google_Service_Analytics_GaData
	//Llamamos a la Api Informes de Google Analytics (v3)
	function Construccion ()
	{
		// Declaramos nuestra configuración -----------------------------------------------------------------------------
		    $c = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);
		    $sql = "SELECT TOP 1 * FROM dbo.LOC_PROYECTOS WHERE id = {$this->strProyectoAsociado}";
		    $stmt = $c->prepare($sql);
		    $result = $stmt->execute();
		    
		    if ($result && $stmt->rowCount() != 0) {
		      while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
		        $googleApi = array(
		          'id' => $fila['apiConsoleId'], 
		          'email' => $fila['proyecto'], 
		          'keyFile' => $_SERVER['DOCUMENT_ROOT'] . $fila['keyFile'],
		          'nombre_app' => $fila['nombre']
		        );
		      }
		    }
		//Asignamos permiso de sólo lectura -------------------------------------------
		$strPermiso = 'https://www.googleapis.com/auth/analytics.readonly';

		// Creamos el cliente de conexión
		$client = new Google_Client();
		$client->setApplicationName($googleApi['nombre_app']);
		$client->setAssertionCredentials(
		  new Google_Auth_AssertionCredentials(
			  $googleApi['email'],
			  array($strPermiso),  
			  file_get_contents($googleApi['keyFile'])
		  )
		);
		$client->setClientId($googleApi['id']);
		$client->setAccessType('offline_access');

		// y con este cliente creamos el objeto para lanzar consultas a la API
		$service = new Google_Service_Analytics($client);

		//Hallamos en la base de datos, dado &idVista cuál es su propiedad y cuenta ($idPropiedad, $idCuenta)
		$c = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);
    	$sql = "SELECT D.LOCD_ANALYTICS_ID_PROPIEDAD, D.LOCD_ANALYTICS_ID_CUENTA
				FROM dbo.LOC_DOMINIOS_VISTAS DV JOIN dbo.LOC_DOMINIOS D ON DV.LOCDV_SUDOMINIO = D.LOCD_CODIGO
				WHERE dv.LOCDV_ANALYTICS_ID_VISTA = :vista";
    	$stmt = $c->prepare($sql);
    	$result = $stmt->execute(array(':vista' => $this->idVista));

 		if ($result && $stmt->rowCount() != 0){

 		    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
 		      $this->idPropiedad = $fila[0];
 		      $this->idCuenta = $fila[1];
 		    }

 		}

		// creamos el objeto para la cache
 		$apiObj = new stdClass();
 		$apiObj->idVista = $this->idVista;
 		$apiObj->startdate = date('Y-m-d');
 		$apiObj->enddate = date('Y-m-d');
 		$apiObj->metrics = $this->TipoLlamada;
    $apiObj->optParams = array(
    	'idCuenta' => $this->idCuenta,
    	'idPropiedad' => $this->idPropiedad
    );

 		$cache = new CacheDatosApi($apiObj, 'adm');
 		if( $cache->isCached() ){
 			$this -> Google_Service_Analytics_GaData = $cache->getData();
 		}else{
			//Hacemos la llamada final dependiendo de la llamada deseada
			switch ($this -> TipoLlamada) {		  			
				
				//***************************************************************************************************************
				// LISTADO DE OBJETIVOS DE UNA VISTA CONCRETA *******************************************************************
				//***************************************************************************************************************
				case "GOALS_VISTA":		
					$this -> Google_Service_Analytics_GaData = $service->management_goals
																->listManagementGoals($this->idCuenta,
																					  $this->idPropiedad,
																					  $this->idVista);	            											   
				break;
				//***************************************************************************************************************

				//***************************************************************************************************************
				// DETALLES DE UN FILTRO CONCRETO *******************************************************************
				//***************************************************************************************************************
				case "FILTRO_DETALLE":		
					$this -> Google_Service_Analytics_GaData = $service->management_filters
																->get($this->idCuenta,
																	  $this->idVista);	
																					  
				break;
				//***************************************************************************************************************

				
			} //CIERRE SWITCH

			$cache->setData($this->Google_Service_Analytics_GaData);
		}
		

		return true;
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//'Devuelve Array de todos los objetivos de la vista con su nombre e id.
	function GOALS_VISTA_ () {            		
	
		//Recorremos todos los Goals para sacar su nombre.  Sólo seleccionamos los que tienen active=1
		$items = $this->Google_Service_Analytics_GaData->getItems();
	 
		if (count($items) != 0) {
		  $array_goals = array();
		  foreach($items as &$goal) {
		
				if ($goal->getActive()==1) {
				  
				  $array_goals[$goal->getId()]["id"] = $goal->getId();
				  $array_goals[$goal->getId()]["nombre"] = $goal->getName();
				  $array_goals[$goal->getId()]["activo"] = $goal->getActive();						  

				}
		  }
		}	
	
		return $array_goals;
	}

	//'Devuelve Array de todos los objetivos de la vista con su nombre e id.
	function GOALS_VISTA_ORDENADOS () { 

		//Hallamos un array de todos los goals de la vista
		$Array_goals_desordenado = $this -> GOALS_VISTA_ ();

		//Ordenamos el array según el orden predefinido en DMIntegra 
		$ordenado_goals = array();
		$otros = 10;
		foreach ($Array_goals_desordenado as $key => $g) {
			switch ($g["nombre"]) {
				case 'DM_VisitaLarga':
					$ordenado_goals[0]["id"]           = $g["id"];
					$ordenado_goals[0]["nombre"]       = 'Visitas largas';
					$ordenado_goals[0]["nombre_corto"] = 'VL';
					$ordenado_goals[0]["activo"]       = $g["activo"];
					break;
				case 'DM_Fomularios':
					$ordenado_goals[1]["id"]           = $g["id"];
					$ordenado_goals[1]["nombre"]       = 'Se han rellenado formularios (no de contacto)';
					$ordenado_goals[1]["nombre_corto"] = 'F';
					$ordenado_goals[1]["activo"]       = $g["activo"];
					break;
				case 'DM_Fomularios_Contacto':
					$ordenado_goals[4]["id"]           = $g["id"];
					$ordenado_goals[4]["nombre"]       = 'Formulario de contacto';
					$ordenado_goals[4]["nombre_corto"] = 'FC';
					$ordenado_goals[4]["activo"]       = $g["activo"];
					break;
				case 'DM_Fomularios_Newsletter':
					$ordenado_goals[9]["id"]           = $g["id"];
					$ordenado_goals[9]["nombre"]       = 'Altas en newsletter';
					$ordenado_goals[9]["nombre_corto"] = 'FN';
					$ordenado_goals[9]["activo"]       = $g["activo"];
					break;
				case 'DM_Descarga':
					$ordenado_goals[3]["id"]           = $g["id"];
					$ordenado_goals[3]["nombre"]       = 'Se han descargado archivos';
					$ordenado_goals[3]["nombre_corto"] = 'D';
					$ordenado_goals[3]["activo"]       = $g["activo"];
					break;
				case 'DM_Contacto_Mailto':
					$ordenado_goals[5]["id"]           = $g["id"];
					$ordenado_goals[5]["nombre"]       = 'Consulta por email';
					$ordenado_goals[5]["nombre_corto"] = 'CM';
					$ordenado_goals[5]["activo"]       = $g["activo"];
					break;
				case 'DM_Buscador_Interno':
					$ordenado_goals[2]["id"]           = $g["id"];
					$ordenado_goals[2]["nombre"]       = 'Se han realizado búsquedas';
					$ordenado_goals[2]["nombre_corto"] = 'BI';
					$ordenado_goals[2]["activo"]       = $g["activo"];
					break;
				case 'DM_Compartir_Email':
					$ordenado_goals[7]["id"]     	   = $g["id"];
					$ordenado_goals[7]["nombre"] 	   = 'Compartido por email';
					$ordenado_goals[7]["nombre_corto"] = 'CI';
					$ordenado_goals[7]["activo"] 	   = $g["activo"];
					break;
				case 'DM_Compartir_Redes_Sociales':
					$ordenado_goals[8]["id"]     	   = $g["id"];
					$ordenado_goals[8]["nombre"]       = 'Compartido por RRSS';
					$ordenado_goals[8]["nombre_corto"] = 'CR';
					$ordenado_goals[8]["activo"]       = $g["activo"];
					break;
				case 'DM_Mostrar_Telefono_Fax':
					$ordenado_goals[6]["id"]           = $g["id"];
					$ordenado_goals[6]["nombre"]       = 'Consulta de teléfono';
					$ordenado_goals[6]["nombre_corto"] = 'TF';
					$ordenado_goals[6]["activo"]       = $g["activo"];
					break;      
				default:
					$ordenado_goals[$otros]["id"]           = $g["id"];
					$ordenado_goals[$otros]["nombre"]       = $g["nombre"];
					$ordenado_goals[$otros]["nombre_corto"] = $g["nombre"];
					$ordenado_goals[$otros]["activo"]       = $g["activo"];
					$otros++;    
					break;
			}
			$intOrden = $intOrden + 1;
		}
		ksort($ordenado_goals);
		return $ordenado_goals;
	}


		//'Devuelve Array de todos los objetivos de la vista con su nombre e id.
	function GOALS_VISTA_ORDENADOS_RESUMEN () { 

		//Hallamos un array de todos los goals de la vista
		$Array_goals_desordenado = $this -> GOALS_VISTA_ ();

		//Ordenamos el array según el orden predefinido en DMIntegra 
		$ordenado_goals = array();
		$otros = 1;
		foreach ($Array_goals_desordenado as $key => $g) {

			switch ($g["nombre"]) {
				case 'DM_VisitaLarga':

					break;
				case 'DM_Fomularios':

					break;
				case 'DM_Fomularios_Contacto':

					break;
				case 'DM_Fomularios_Newsletter':

					break;
				case 'DM_Descarga':

					break;
				case 'DM_Contacto_Mailto':

					break;
				case 'DM_Buscador_Interno':

					break;
				case 'DM_Compartir_Email':

					break;
				case 'DM_Compartir_Redes_Sociales':

					break;
				case 'DM_Mostrar_Telefono_Fax':

					break;      
				default:
					$ordenado_goals[$otros]["id"]           = $g["id"];
					$ordenado_goals[$otros]["nombre"]       = $g["nombre"];
					$ordenado_goals[$otros]["nombre_corto"] = $g["nombre"];
					$ordenado_goals[$otros]["activo"]       = $g["activo"];
					$otros++;    
					break;
			}

		}
		
		$otros--;

		foreach ($Array_goals_desordenado as $key => $g) {


			switch ($g["nombre"]) {
				case 'DM_VisitaLarga':
					$ordenado_goals[$cont+9]["id"]           = $g["id"];
					$ordenado_goals[$cont+9]["nombre"]       = 'Visitas largas';
					$ordenado_goals[$cont+9]["nombre_corto"] = 'VL';
					$ordenado_goals[$cont+9]["activo"]       = $g["activo"];
					break;
				case 'DM_Fomularios':
					$ordenado_goals[$cont+2]["id"]           = $g["id"];
					$ordenado_goals[$cont+2]["nombre"]       = 'Se han rellenado formularios (no de contacto)';
					$ordenado_goals[$cont+2]["nombre_corto"] = 'F';
					$ordenado_goals[$cont+2]["activo"]       = $g["activo"];
					break;
				case 'DM_Fomularios_Contacto':
					$ordenado_goals[$cont+1]["id"]           = $g["id"];
					$ordenado_goals[$cont+1]["nombre"]       = 'Formulario de contacto';
					$ordenado_goals[$cont+1]["nombre_corto"] = 'FC';
					$ordenado_goals[$cont+1]["activo"]       = $g["activo"];
					break;
				case 'DM_Fomularios_Newsletter':
					$ordenado_goals[$cont+3]["id"]           = $g["id"];
					$ordenado_goals[$cont+3]["nombre"]       = 'Altas en newsletter';
					$ordenado_goals[$cont+3]["nombre_corto"] = 'FN';
					$ordenado_goals[$cont+3]["activo"]       = $g["activo"];
					break;
				case 'DM_Descarga':
					$ordenado_goals[$cont+8]["id"]           = $g["id"];
					$ordenado_goals[$cont+8]["nombre"]       = 'Se han descargado archivos';
					$ordenado_goals[$cont+8]["nombre_corto"] = 'D';
					$ordenado_goals[$cont+8]["activo"]       = $g["activo"];
					break;
				case 'DM_Contacto_Mailto':
					$ordenado_goals[$cont+5]["id"]           = $g["id"];
					$ordenado_goals[$cont+5]["nombre"]       = 'Consulta por email';
					$ordenado_goals[$cont+5]["nombre_corto"] = 'CM';
					$ordenado_goals[$cont+5]["activo"]       = $g["activo"];
					break;
				case 'DM_Buscador_Interno':
					$ordenado_goals[$cont+10]["id"]           = $g["id"];
					$ordenado_goals[$cont+10]["nombre"]       = 'Se han realizado búsquedas';
					$ordenado_goals[$cont+10]["nombre_corto"] = 'BI';
					$ordenado_goals[$cont+10]["activo"]       = $g["activo"];
					break;
				case 'DM_Compartir_Email':
					$ordenado_goals[$cont+6]["id"]     	   = $g["id"];
					$ordenado_goals[$cont+6]["nombre"] 	   = 'Compartido por email';
					$ordenado_goals[$cont+6]["nombre_corto"] = 'CI';
					$ordenado_goals[$cont+6]["activo"] 	   = $g["activo"];
					break;
				case 'DM_Compartir_Redes_Sociales':
					$ordenado_goals[$cont+7]["id"]     	   = $g["id"];
					$ordenado_goals[$cont+7]["nombre"]       = 'Compartido por RRSS';
					$ordenado_goals[$cont+7]["nombre_corto"] = 'CR';
					$ordenado_goals[$cont+7]["activo"]       = $g["activo"];
					break;
				case 'DM_Mostrar_Telefono_Fax':
					$ordenado_goals[$cont+4]["id"]           = $g["id"];
					$ordenado_goals[$cont+4]["nombre"]       = 'Consulta de teléfono';
					$ordenado_goals[$cont+4]["nombre_corto"] = 'TF';
					$ordenado_goals[$cont+4]["activo"]       = $g["activo"];
					break;      
			}
			$intOrden = $intOrden + 1;
		}
		ksort($ordenado_goals);
		return $ordenado_goals;
	}
	
	//'Devuelve Array con las métricas "goalXXCompletions" y "goalXXConversionRate" dividiendo los objetivos ordenados de 5 en cinco.
	function GOALS_VISTA_ORDENADOS_METRICAS () { 

		//Hallamos un array ordenado según el estándar de DMIntegra de todos los goals de la vista
		$Array_goals_ordenado = $this -> GOALS_VISTA_ORDENADOS();
		
		//Creamos un array con las métricas correspondientes.  GA nos permite llamar de 10 en 10 métricas, con lo que podemos tener un array de 1 o de 4 posiciones.
		$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = array();
		$intC=0;

		foreach ($Array_goals_ordenado as $key => $objetivo) {

			if ($intC >= 0 and $intC <= 4) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] .= "ga:goal".$objetivo["id"]."Completions, ga:goal".$objetivo["id"]."ConversionRate";
			}
			if ($intC >= 5 and $intC <= 9) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] .= "ga:goal".$objetivo["id"]."Completions, ga:goal".$objetivo["id"]."ConversionRate";
			}
			if ($intC >= 10 and $intC <= 14) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] .= "ga:goal".$objetivo["id"]."Completions, ga:goal".$objetivo["id"]."ConversionRate";
			}
			if ($intC >= 15 and $intC <= 19) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[3] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[3] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[3] .= "ga:goal".$objetivo["id"]."Completions, ga:goal".$objetivo["id"]."ConversionRate";
			}	
			$intC++;		
		}
		
		return $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS;
		
	}
	
	function GOALS_VISTA_ORDENADOS_METRICAS_GOAL () { 

		//Hallamos un array ordenado según el estándar de DMIntegra de todos los goals de la vista
		$Array_goals_ordenado = $this -> GOALS_VISTA_ORDENADOS();
		
		//Creamos un array con las métricas correspondientes.  GA nos permite llamar de 10 en 10 métricas, con lo que podemos tener un array de 1 o de 4 posiciones.
		$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = array();
		$intC=0;

		foreach ($Array_goals_ordenado as $key => $objetivo) {

			if ($intC >= 0 and $intC <= 9) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] .= "ga:goal".$objetivo["id"]."Completions";
			}
			if ($intC >= 10 and $intC <= 19) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] .= "ga:goal".$objetivo["id"]."Completions";
			}
			if ($intC >= 20 and $intC <= 29) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] .= "ga:goal".$objetivo["id"]."Completions";
			}
			if ($intC >= 30 and $intC <= 39) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[3] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[3] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[3] .= "ga:goal".$objetivo["id"]."Completions";
			}	
			$intC++;		
		}
		
		return $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS;
		
	}

	//'Devuelve Array con las métricas "goalXXCompletions" dividiendo los objetivos ordenados de 6 en 6 ya que luego, en la función FUENTES_EXTRA se concatenarán las métricas ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession.
function GOALS_VISTA_ORDENADOS_METRICAS_SESIONES ($app=0) { 


		if($app == 0){
			$pags = 'pageviewsPerSession';
		}else{
			$pags = 'screenviewsPerSession';
		}
		//Hallamos un array ordenado según el estándar de DMIntegra de todos los goals de la vista
		$Array_goals_ordenado = $this -> GOALS_VISTA_ORDENADOS();
		
		//Creamos un array con las métricas correspondientes.  GA nos permite llamar de 10 en 10 métricas, con lo que podemos tener un array de 1 o de 4 posiciones.
		$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = array();
		$intC=0;
		$sw=0;
		foreach ($Array_goals_ordenado as $key => $objetivo) {

			if ($intC >= 0 and $intC <= 5) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] .= ",";}
				if($sw==0){
					$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] .= "ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:".$pags.",ga:goal".$objetivo["id"]."Completions";
					$sw++;
				}else{
					$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[0] .= "ga:goal".$objetivo["id"]."Completions";
				}
				
			}
			if ($intC >= 6 and $intC <= 16) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[1] .= "ga:goal".$objetivo["id"]."Completions";
			}
			if ($intC >= 17 and $intC <= 20) {
				if($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] !=""){ $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] .= ",";}
				$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[2] .= "ga:goal".$objetivo["id"]."Completions";
			}

			$intC++;		
		}

		return $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS;
		
	}

	
}


?>






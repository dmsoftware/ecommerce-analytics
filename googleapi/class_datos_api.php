<?php

require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\Google\Client.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\Google\Service\Analytics.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\ga-api-stack\ga-api-stack.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\CacheDatosApi.php");

//Clase para el objeto "llamada a la api de informes"
//Partiendo de las propiedades de creaci�n, llamamos a la api de google y creamos las funciones para tratar los datos devueltos de forma "amigable"
class Datos_Api_Informes {
    
	//Propiedades de creaci�n --------------------------------------------------------
	var $strProyectoAsociado;
	var $idVista; 
	var $startdate;
	var $enddate;
	var $metrics;
	var $optParams;
	var $optForzarNousarFiltroSpam; 

	//Propiedad de uso interno --------------------------------------------------------
	var $Google_Service_Analytics_GaData; 


	//----------------------------------------------------------------------------------------------------------------------------
	//Funcion constructora.  Dadas las propiedades de entrada llamamos a la api de Google y dejamos el resultado en $Google_Service_Analytics_GaData
	//Llamamos a la Api Informes de Google Analytics (v3)
	function Construccion ()
	{		
		  // Declaramos nuestra configuraci�n -----------------------------------------------------------------------------
		  // Tenemos 2 proyectos para evitar las restricciones.  Buscamos la configuraci�n correcta para el proyecto actual ($strProyectoAsociado).
			$c = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);
			$sql = "SELECT TOP 1 * FROM dbo.LOC_PROYECTOS WHERE id = {$this->strProyectoAsociado}";
			$stmt = $c->prepare($sql);
			$result = $stmt->execute();
			
			if ($result && $stmt->rowCount() != 0) {
			  while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$googleApi = array(
				  'id' => $fila['apiConsoleId'], 
				  'email' => $fila['proyecto'], 
				  'keyFile' => $_SERVER['DOCUMENT_ROOT'] . $fila['keyFile'],
				  'nombre_app' => $fila['nombre']
				);
			  }
			}

			//Quitamos el ga: a idvista
			$iv = $this -> idVista;
			$idVista = "";
			$cont = 1;
			for ($i=0; $i <strlen($iv) ; $i++) { 
				if($i >= 3){
					$idvista .= $iv[$i];
				}
			}



		if($this -> optForzarNousarFiltroSpam != 1){
			//BUSCAMOS EL FILTRO ANTIESPAN Y SI HAY CONCATENAMOS FILTRO
			$f = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);			
			$sqlf = " SELECT LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM, ";
			$sqlf .= " STUFF(( SELECT ',ga:hostname=@' + LOCDV_ANALYTICS_URL_VISTA ";
			$sqlf .= "		  FROM LOC_DOMINIOS_VISTAS INNER JOIN LOC_DOMINIOS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO ";
			$sqlf .= "		  WHERE LOCD_ANALYTICS_ID_PROPIEDAD=LD.LOCD_ANALYTICS_ID_PROPIEDAD   ";
			$sqlf .= "		  GROUP BY LOCDV_ANALYTICS_URL_VISTA FOR XML PATH('')) ,1,1,'') AS FILTRO_SPAM_HOST, ";
			$sqlf .= "  STUFF(( SELECT ';ga:source!@' + LOCSR_REFERER  ";
			$sqlf .= "		  FROM LOC_SPAM_REFERER FOR XML PATH('')) ,1,1,'') AS FILTRO_SPAM_REFERER ";
			$sqlf .= "  FROM LOC_DOMINIOS LD INNER JOIN LOC_DOMINIOS_VISTAS ON LD.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO ";
			$sqlf .= "  WHERE LOCDV_ANALYTICS_ID_VISTA='".$idvista."' ";
			$sqlf .= "  GROUP BY LOCD_ANALYTICS_ID_PROPIEDAD,LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM	";	
			
			$stmtf = $f->prepare($sqlf);
			$resultf = $stmtf->execute();
			
			if ($resultf && $stmtf->rowCount() != 0) {
			  	while ($filaf = $stmtf->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
					$antiespam = $filaf[0];
					$filtroa   = $filaf[1];
					$filtrob   = $filaf[2];
			  	}
			  	$filtro = "";
			  	if($antiespam == 1){
			  		$filtro = $filtroa.";".$filtrob;
			  	}
			  	//echo $this -> optParams;
			  	//exit();
			    if(!empty($filtro)){
				  $varFiltro = $this -> optParams["filters"];
				  if(isset($varFiltro) AND !empty($varFiltro) AND $varFiltro != ""){
				  	$this -> optParams["filters"] .= ";".$filtro;
				  }else{
				  	$this -> optParams["filters"]  = $filtro;
				  }
				}
			}else{
				//$varFiltro["filters"] = $sqlf;
			}	

		}

		  //Asignamos permiso de s�lo lectura -------------------------------------------
		  $strPermiso = 'https://www.googleapis.com/auth/analytics.readonly';

		  // Creamos el cliente de conexi�n
		  $client = new Google_Client();
		  $client->setApplicationName($googleApi['nombre_app']);
		  $client->setAssertionCredentials(
			  new Google_Auth_AssertionCredentials(
				  $googleApi['email'],
				  array($strPermiso),  
				  file_get_contents($googleApi['keyFile'])
			  )
		  );
		  $client->setClientId($googleApi['id']);
		  $client->setAccessType('offline_access');

		  // y con este cliente creamos el objeto para lanzar consultas a la API
		  $service = new Google_Service_Analytics($client);

		// Hacemos la llamada final
		$fechaIni = $this->microtimeToString();
		$response = GoogleAnalyticsAPIStack::getAccess($this->strProyectoAsociado);
		$fechaMid = $this->microtimeToString();

		try{
			$cache = new CacheDatosApi($this);
			if( $cache->isCached() ){
				$this -> Google_Service_Analytics_GaData = $cache->getData();
			} else {
			  $this -> Google_Service_Analytics_GaData = $service->data_ga->get(
							  $this -> idVista,
							  $this -> startdate,
							  $this -> enddate,
							  $this -> metrics,
							  $this -> optParams);

			  $cache->setData($this->Google_Service_Analytics_GaData);
			}
		} catch (Exception $e){
			$error = $e->getMessage();
		}

		$fechaFin = $this->microtimeToString();
		

		/* LOG */
		if($archivo = fopen('/logs.txt' . $nombre_archivo, "a")){
			if(fwrite($archivo, "Inicio: " . $fechaIni . str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "Prop: " . $this -> strProyectoAsociado . str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "Vista: " . $this -> idVista . str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "Bloqueo: " . (($response) ? "SI (" . $fechaMid . ")" : "NO" .  str_repeat(' ', 24)) .  str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "FechFin: " . $fechaFin .  str_repeat(' ', 4) . "||" . str_repeat(' ', 4). "Antispam:" . $antiespam . str_repeat(' ', 4). "||" .  str_repeat(' ', 4) .  "Filtro:". $this -> optParams["filters"] .  str_repeat(' ', 4) . "||".  str_repeat(' ', 4) .  "Sql:". /*$sqlf.*/  str_repeat(' ', 4) . "||" .  str_repeat(' ', 4) .  "Error: " . $error . "\r\n"))
			fclose($archivo);
		} 
		
		return true;
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	// Microtime to String
	function microtimeToString(){
		$t = explode(" ", microtime());
		return date("d/m/y H:i:s",$t[1]).substr((string)$t[0],1,4);
	}	
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de registros existentes para los filtros aplicados.
	function NumValoresReal () {            		
		return $this->Google_Service_Analytics_GaData->totalResults;
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de registros traidos en esta "p�gina/llamada". Depende del valor MAXRESULTS enviado a la Api.
	function NumValores () {            		
		return count($this->Google_Service_Analytics_GaData->rows);
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de columnas (tanto DIMENSIONES como METRICAS)
	function NumColumnas () {            		
		return count($this->Google_Service_Analytics_GaData->columnHeaders);
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de columnas M�TRICAS
	function NumMetricas () {            		
		return count($this->Google_Service_Analytics_GaData->query->metrics);
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de columnas DIMENSIONES
	function NumDimensiones () {            		
		return ($this->numColumnas() - $this->numMetricas());
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el nombre de las m�tricas y dimensiones
	//Columna: n�mero �ndice. El array empieza en 0 y nos llamar�n a partir del �ndice 1
	function NombreColumna ($Columna) {            		
		if (is_numeric($Columna) ) {
			$header = $this->Google_Service_Analytics_GaData->columnHeaders[$Columna-1];
			$headerName = ucwords(preg_replace('/(\w+)([A-Z])/U', '\\1 \\2', str_replace('ga:', '', $header->name)));
			return $headerName;
		}else{
			return $Columna;
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el tipo de las columnas (METRICA / DIMENSION)
	//Columna: n�mero �ndice o nombre de la columna (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	function TipoColumna ($Columna) {            		
		If (is_numeric($Columna) ) {
			$header = $this->Google_Service_Analytics_GaData->columnHeaders[$Columna-1];
			$headerType = $header->columnType;
			return $headerType;
		} else {
		  //Buscamos el Tipo de este Nombre
		  //Recorremos todos los columnHeaders, hasta que nos coincide el [name]
		  foreach ($this->Google_Service_Analytics_GaData->columnHeaders as $header) {
		      $strNombreColumna = strtoupper($header->name);
			  $strNombreColumna = str_replace(" ","",$strNombreColumna);
			  $strNombreColumna = str_replace("GA:","",$strNombreColumna);
			  $strAuxValor	    = strtoupper($Columna);
			  $strAuxValor		= str_replace(" ","",$strAuxValor);

			  if ( $strNombreColumna == $strAuxValor ) {
				return $header->columnType;
				break;
			  }
		   }
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el tipo de dato de las columnas (INTEGER / STRING / ...)
	//Columna: n�mero �ndice o nombre de la columna (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	function TipoDatoColumna ($Columna) {            		
		If (is_numeric($Columna) ) {
			$header = $this->Google_Service_Analytics_GaData->columnHeaders[$Columna-1];
			$headerDataType = $header->dataType;
			return $headerDataType;
		} else {
		  //Buscamos el Tipo de dato de este Nombre de columna
		  //Recorremos todos los columnHeaders, hasta que nos coincide el [name]
		  foreach ($this->Google_Service_Analytics_GaData->columnHeaders as $header) {
		      $strNombreColumna = strtoupper($header->name);
			  $strNombreColumna = str_replace(" ","",$strNombreColumna);
			  $strNombreColumna = str_replace("GA:","",$strNombreColumna);
			  $strAuxValor	    = strtoupper($Columna);
			  $strAuxValor		= str_replace(" ","",$strAuxValor);

			  if ( $strNombreColumna == $strAuxValor ) {
				return $header->dataType;
				break;
			  }
		   }
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el valor devuelto para la COLUMNA y FILA dada 
	//Columna: n�mero �ndice o nombre de la columna (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	//Fila: indice de la fila. El array empieza en 0 y nos llamar�n a partir del �ndice 1
	function Valor ($Columna,$Fila) {            		
		If (is_numeric($Columna) ) {
			//El array de datos est� "al rev�s".
			return $this->Google_Service_Analytics_GaData->rows[$Fila-1][$Columna-1];
		} else {
		  //Buscamos el �ndice de este Nombre de columna
		  //Recorremos todos los columnHeaders, hasta que nos coincide el [name]
		  $IndiceColumna = 0;
		  foreach ($this->Google_Service_Analytics_GaData->columnHeaders as $header) {
		      $strNombreColumna = strtoupper($header->name);
			  $strNombreColumna = str_replace(" ","",$strNombreColumna);
			  $strNombreColumna = str_replace("GA:","",$strNombreColumna);
			  $strAuxValor	    = strtoupper($Columna);
			  $strAuxValor		= str_replace(" ","",$strAuxValor);

			  if ( $strNombreColumna == $strAuxValor ) {
				//El array de datos est� "al rev�s".
				return $this->Google_Service_Analytics_GaData->rows[$Fila-1][$IndiceColumna];
				break;
			  }
			  $IndiceColumna = $IndiceColumna + 1;
		   }
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el valor formateado devuelto para la COLUMNA y FILA dada 
	//Columna: n�mero �ndice o nombre de la columna (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	//Fila: indice de la fila. El array empieza en 0 y nos llamar�n a partir del �ndice 1
	function ValorF ($Columna,$Fila,$iso="") {            		

		$AuxValorF = $this->Valor($Columna,$Fila);
		
		//Formateamos seg�n el tipo o dato
		switch ($this -> TipoDatoColumna($Columna)) {
			case "TIME":
				$strSalida = $this->segundos_tiempo($AuxValorF);
				break;
			case "FLOAT":
				$strSalida =  number_format($AuxValorF, 2, ',', '.'); 
				break;				
			case "INTEGER":
				$strSalida =  number_format($AuxValorF, 0, ',', '.');
				break;
			case "PERCENT":
				$strSalida =  number_format($AuxValorF, 2, ',', '.') . '%';
				break;  
			default:
				$strSalida =  $AuxValorF;
				break;  
		}//Cierre switch 		
		//Formateamos seg�n el nombre del campo	
		switch (strtoupper($this -> NombreColumna($Columna))) {
			case "YEARWEEK":
				$Resultado = $this -> getStartAndEndDate(substr($AuxValorF,4,2),substr($AuxValorF,0,4));
				if ($this -> mes_ano($Resultado[0])==$this -> mes_ano($Resultado[1])) {
					$strSalida = date('Y',strtotime($Resultado[0])) .' '. $this -> mes_ano($Resultado[0]).' '.date('d',strtotime($Resultado[0])) . ' - '.date('d',strtotime($Resultado[1])) ; 
				}else{
					$strSalida = date('Y',strtotime($Resultado[0])) .' '. $this -> mes_ano($Resultado[0]).' '.date('d',strtotime($Resultado[0])) . ' - '. $this -> mes_ano($Resultado[1]).' '.date('d',strtotime($Resultado[1])) ; 
				}
				break;  
			case "YEARMONTH":
				$strSalida = substr($AuxValorF,0,4) .' '. $this -> mes_ano(substr($AuxValorF,4,2)); 
				break; 
			case "HOUR":
				$strSalida = $AuxValorF .':00 - '. $AuxValorF .':59'; 
				break; 		
			case "DAYOFWEEK":
				//Para google 0:domingo -- 6:s�bado
				switch ($AuxValorF) {
					case 0: //domingo
						$strSalida = $this -> dia_semana(7) ; 
						break;
					default:
						$strSalida = $this -> dia_semana($AuxValorF) ; 
						break;
				}		
				break; 	
			case "COUNTRY":
				$strSalida = "<img src='../public/images/flags/flags-iso/flat/16/".$iso.".png' />".$strSalida; 
				break; 						
		}//Cierre switch 
		return $strSalida;
	}
	//----------------------------------------------------------------------------------------------------------------------------	
	
	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el total de la Metrica dada. 
	//Metrica: n�mero �ndice o nombre de la Metrica (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	function Total($Metrica) {            		
		If (is_numeric($Metrica) ) {
				//Hallamos el nombre de la m�trica correcta
				$IndiceReal = $this->Google_Service_Analytics_GaData->query->metrics[$Metrica-1];			
				return $this->Google_Service_Analytics_GaData->totalsForAllResults[$IndiceReal];	
		} else {
				//Recorremos todos los columnHeaders, hasta que nos coincide el [name]
				foreach ($this->Google_Service_Analytics_GaData->query->metrics as $metric) {
					$strNombreMetrica = strtoupper($metric);
					$strNombreMetrica = str_replace(" ","",$strNombreMetrica);
					$strNombreMetrica = str_replace("GA:","",$strNombreMetrica);
					$strAuxColumna	    = strtoupper($Metrica);
					$strAuxColumna		= str_replace(" ","",$strAuxColumna);		
					if ( $strNombreMetrica == $strAuxColumna ) {
						return $this->Google_Service_Analytics_GaData->totalsForAllResults[$metric];
						break;
					}
				}
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el valor formateado devuelto para la METRICA dada
	function TotalF ($Metrica) {            		
		$AuxValorF = $this->Total($Metrica);
		//Formateamos seg�n el tipo o nombre de dato
		switch ($this -> TipoDatoColumna($Metrica)) {
			case "TIME":
				return $this->segundos_tiempo($AuxValorF);
				break;
			case "FLOAT":
				return number_format($AuxValorF, 2, ',', '.');
				break;				
			case "INTEGER":
				return number_format($AuxValorF, 0, ',', '.');
				break;
			case "PERCENT":
				return number_format($AuxValorF, 2, ',', '.') . '%';
				break;  
			default:
				return $AuxValorF;
				break;  
		}//Cierre switch 		
	}
	//----------------------------------------------------------------------------------------------------------------------------		
	
	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos la media de la Metrica dada. 
	//Metrica: n�mero �ndice o nombre de la Metrica (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	function Media($Metrica) {            		
		//La media depende del tipo de dato
		switch ($this -> TipoDatoColumna($Metrica)) {
			case "TIME":
				$strSalida = $this->Total($Metrica);
				break;
			case "FLOAT":
			case "INTEGER":
			case "PERCENT": 
			default:
				$strSalida = $this->Total($Metrica) / $this->NumValores();
				break;  
		}//Cierre switch 
		//Hallamos la media seg�n el nombre del campo	
		switch (strtoupper($this -> NombreColumna($Metrica))) {
			case "PAGEVIEWSPERSESSION":
			case "BOUNCERATE":
			case "AVGSESSIONDURATION":
				$strSalida = $this->Total($Metrica); 
				break; 
		}//Cierre switch 
		return $strSalida;		
	}
	//----------------------------------------------------------------------------------------------------------------------------
	
	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el valor formateado devuelto para la METRICA dada
	function MediaF ($Metrica) {            		
		$AuxValorF = $this->Media($Metrica);
		//Formateamos seg�n el tipo o nombre de dato
		switch ($this -> TipoDatoColumna($Metrica)) {
			case "TIME":
				return $this->segundos_tiempo($AuxValorF);
				break;
			case "FLOAT":
				return number_format($AuxValorF, 2, ',', '.');
				break;				
			case "INTEGER":
				return number_format($AuxValorF, 0, ',', '.');
				break;
			case "PERCENT":
				return number_format($AuxValorF, 2, ',', '.') . '%';
				break;  
			default:
				return $AuxValorF;
				break;  
		}//Cierre switch 		
	}
	//----------------------------------------------------------------------------------------------------------------------------	
	
	//----------------------------------------------------------------------------------------------------------------------------
	//FUNCIONES AUXILIARES
	//----------------------------------------------------------------------------------------------------------------------------
	function segundos_tiempo($segundos){
		$minutos=$segundos/60;
		$horas=floor($minutos/60);
		$minutos2=$minutos%60;
		$segundos_2=$segundos%60%60%60;
		if($minutos2<10)$minutos2='0'.$minutos2;
		if($segundos_2<10)$segundos_2='0'.$segundos_2;

		if($segundos<60){ /* segundos */
			$resultado= round($segundos).' seg.';
		}elseif($segundos>60 && $segundos<3600){/* minutos */
			$resultado= $minutos2.':'.$segundos_2.' min.';
		}else{/* horas */
		
		$resultado= $horas.':'.$minutos2.':'.$segundos_2.' h.';
		}
		return $resultado;
	}
	
	function getStartAndEndDate($week, $year)
	{
		//Dado una semana y un a�o devolvemos el lunes de esa semana y el domingo de esa semana
		$week = sprintf("%02s", $week);
		$return[0] = date('Y-m-d', strtotime("$year-W$week-1"));
		$return[1] = date('Y-m-d', strtotime('+6 days', strtotime($return[0])));
		return $return;			
	}	
	
	function mes_ano($valor) {
		$meses = array("ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC");
		//En valor puede venir una fecha o el n�mero de mes
		if (is_numeric($valor)) {
			//viene un n�mero de mes
			return $meses[$valor-1];
		}else{ 
			//viene una fecha
			return $meses[date('m', strtotime($valor))-1];
		}
	}
	
	function dia_semana($valor) {
		$dias = array("LUN","MAR","MIE","JUE","VIE","SAB","DOM");
		//En valor puede venir una fecha o el n�mero de mes
		if (is_numeric($valor)) {
			//viene un n�mero de dia
			return $dias[$valor-1];
		}else{ 
			//viene una fecha
			return $dias[date('N', strtotime($valor))-1];
		}		
	}	
	//----------------------------------------------------------------------------------------------------------------------------	

}

?>






<?php

require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\Google\Client.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\Google\Service\Analytics.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\ga-api-stack\ga-api-stack.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\CacheDatosApi.php");

//Clase para el objeto "llamada a la api de informes"
//Partiendo de las propiedades de creaci�n, llamamos a la api de google y creamos las funciones para tratar los datos devueltos de forma "amigable"
class Datos_Api_Mcf {
    
	//Propiedades de creaci�n --------------------------------------------------------
	var $strProyectoAsociado;
	var $idVista; 
	var $startdate;
	var $enddate;
	var $metrics;
	var $optParams;

	//Propiedad de uso interno --------------------------------------------------------
	var $Google_Service_Analytics_McfData; 


	//----------------------------------------------------------------------------------------------------------------------------
	//Funcion constructora.  Dadas las propiedades de entrada llamamos a la api de Google y dejamos el resultado en $Google_Service_Analytics_McfData
	//Llamamos a la Api Informes de Google Analytics (v3)
	function Construccion ()
	{
		  // Declaramos nuestra configuraci�n -----------------------------------------------------------------------------

		  $c = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);
		  $sql = "SELECT TOP 1 * FROM dbo.LOC_PROYECTOS WHERE id = {$this->strProyectoAsociado}";
		  $stmt = $c->prepare($sql);
		  $result = $stmt->execute();
		  
		  if ($result && $stmt->rowCount() != 0) {
		    while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
		      $googleApi = array(
		        'id' => $fila['apiConsoleId'], 
		        'email' => $fila['proyecto'], 
		        'keyFile' => $_SERVER['DOCUMENT_ROOT'] . $fila['keyFile'],
		        'nombre_app' => $fila['nombre']
		      );
		    }
		  }
		  //Asignamos permiso de s�lo lectura -------------------------------------------
		  $strPermiso = 'https://www.googleapis.com/auth/analytics.readonly';

		  // Creamos el cliente de conexi�n
		  $client = new Google_Client();
		  $client->setApplicationName($googleApi['nombre_app']);
		  $client->setAssertionCredentials(
			  new Google_Auth_AssertionCredentials(
				  $googleApi['email'],
				  array($strPermiso),  
				  file_get_contents($googleApi['keyFile'])
			  )
		  );
		  $client->setClientId($googleApi['id']);
		  $client->setAccessType('offline_access');
 
		  // y con este cliente creamos el objeto para lanzar consultas a la API
		  $service = new Google_Service_Analytics($client);


		// Hacemos la llamada final
		$fechaIni = $this->microtimeToString();
		$response = GoogleAnalyticsAPIStack::getAccess($this->strProyectoAsociado);
		$fechaMid = $this->microtimeToString();
		
		try{
			 $cache = new CacheDatosApi($this, 'mcf');
			 if( $cache->isCached() ){
			 	$this -> Google_Service_Analytics_McfData = $cache->getData();
			 } else {
			  $this -> Google_Service_Analytics_McfData = $service->data_mcf->get(
							  $this -> idVista,
							  $this -> startdate,
							  $this -> enddate,
							  $this -> metrics,
							  $this -> optParams);

			  $cache->setData($this->Google_Service_Analytics_McfData);
			}
		} catch (Exception $e){
			$error = $e->getMessage();
		}

		$fechaFin = $this->microtimeToString();
		
		/* LOG */
		if($archivo = fopen('C:/Inetpub/vhosts/dmintegra.acc.com.es/httpdocs/googleapi/logs.txt' . $nombre_archivo, "a")){
			if(fwrite($archivo, "Inicio: " . $fechaIni . str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "Prop: " . $this -> strProyectoAsociado . str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "Vista: " . $this -> idVista . str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "Bloqueo: " . (($response) ? "SI (" . $fechaMid . ")" : "NO" .  str_repeat(' ', 24)) .  str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "FechFin: " . $fechaFin .  str_repeat(' ', 4) . "||" . str_repeat(' ', 4) . "Error: " . $error . "\r\n"))
			fclose($archivo);
		}
		
		return true;
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	// Microtime to String
	function microtimeToString(){
		$t = explode(" ", microtime());
		return date("d/m/y H:i:s",$t[1]).substr((string)$t[0],1,4);
	}	
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de registros existentes para los filtros aplicados.
	function NumValoresReal () {            		
		return $this->Google_Service_Analytics_McfData->totalResults;
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de registros traidos en esta "p�gina/llamada". Depende del valor MAXRESULTS enviado a la Api.
	function NumValores () {            		
		return count($this->Google_Service_Analytics_McfData->rows);
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de columnas (tanto DIMENSIONES como METRICAS)
	function NumColumnas () {            		
		return count($this->Google_Service_Analytics_McfData->columnHeaders);
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de columnas M�TRICAS
	function NumMetricas () {            		
		return count($this->Google_Service_Analytics_McfData->query->metrics);
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//'N�mero de columnas DIMENSIONES
	function NumDimensiones () {            		
		return ($this->numColumnas() - $this->numMetricas());
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el nombre de las m�tricas y dimensiones
	//Columna: n�mero �ndice. El array empieza en 0 y nos llamar�n a partir del �ndice 1
	function NombreColumna ($Columna) {            		
		if (is_numeric($Columna) ) {
			$header = $this->Google_Service_Analytics_McfData->columnHeaders[$Columna-1];
			$headerName = ucwords(preg_replace('/(\w+)([A-Z])/U', '\\1 \\2', str_replace('mcf:', '', $header->name)));
			return $headerName;
		}else{
			return $Columna;
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el tipo de las columnas (METRICA / DIMENSION)
	//Columna: n�mero �ndice o nombre de la columna (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	function TipoColumna ($Columna) {            		
		If (is_numeric($Columna) ) {
			$header = $this->Google_Service_Analytics_McfData->columnHeaders[$Columna-1];
			$headerType = $header->columnType;
			return $headerType;
		} else {
		  //Buscamos el Tipo de este Nombre
		  //Recorremos todos los columnHeaders, hasta que nos coincide el [name]
		  foreach ($this->Google_Service_Analytics_McfData->columnHeaders as $header) {
		      $strNombreColumna = strtoupper($header->name);
			  $strNombreColumna = str_replace(" ","",$strNombreColumna);
			  $strNombreColumna = str_replace("mcf:","",$strNombreColumna);
			  $strAuxValor	    = strtoupper($Columna);
			  $strAuxValor		= str_replace(" ","",$strAuxValor);

			  if ( $strNombreColumna == $strAuxValor ) {
				return $header->columnType;
				break;
			  }
		   }
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el tipo de dato de las columnas (INTEGER / STRING / ...)
	//Columna: n�mero �ndice o nombre de la columna (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	function TipoDatoColumna ($Columna) {            		
		If (is_numeric($Columna) ) {
			$header = $this->Google_Service_Analytics_McfData->columnHeaders[$Columna-1];
			$headerDataType = $header->dataType;
			return $headerDataType;
		} else {
		  //Buscamos el Tipo de dato de este Nombre de columna
		  //Recorremos todos los columnHeaders, hasta que nos coincide el [name]
		  foreach ($this->Google_Service_Analytics_McfData->columnHeaders as $header) {
		      $strNombreColumna = strtoupper($header->name);
			  $strNombreColumna = str_replace(" ","",$strNombreColumna);
			  $strNombreColumna = str_replace("mcf:","",$strNombreColumna);
			  $strAuxValor	    = strtoupper($Columna);
			  $strAuxValor		= str_replace(" ","",$strAuxValor);

			  if ( $strNombreColumna == $strAuxValor ) {
				return $header->dataType;
				break;
			  }
		   }
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el valor devuelto para la COLUMNA y FILA dada 
	//Columna: n�mero �ndice o nombre de la columna (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	//Fila: indice de la fila. El array empieza en 0 y nos llamar�n a partir del �ndice 1
	function Valor ($Columna,$Fila) {            		
		If (is_numeric($Columna) ) {
			//El array de datos est� "al rev�s".
			return $this->Google_Service_Analytics_McfData->rows[$Fila-1]["modelData"][$Columna-1]["primitiveValue"];
		} else {
		  //Buscamos el �ndice de este Nombre de columna
		  //Recorremos todos los columnHeaders, hasta que nos coincide el [name]
		  $IndiceColumna = 0;
		  //return $this->Google_Service_Analytics_McfData->rows[1]["modelData"][$IndiceColumna]["primitiveValue"];
		  foreach ($this->Google_Service_Analytics_McfData->columnHeaders as $header) {
		      $strNombreColumna = strtoupper($header->name);
			  $strNombreColumna = str_replace(" ","",$strNombreColumna);
			  $strNombreColumna = str_replace("mcf:","",$strNombreColumna);
			  $strAuxValor	    = strtoupper($Columna);
			  $strAuxValor		= str_replace(" ","",$strAuxValor);

				//return $this->Google_Service_Analytics_McfData->columnHeaders[0]["name"];
			  	//return $strAuxValor;
			  
			  if ( $strNombreColumna == $strAuxValor ) {
				//El array de datos est� "al rev�s".
				return $this->Google_Service_Analytics_McfData->rows[$Fila-1]["modelData"][$IndiceColumna]["primitiveValue"];

				break;
			  }
			  $IndiceColumna = $IndiceColumna + 1;
		   }
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el valor formateado devuelto para la COLUMNA y FILA dada 
	//Columna: n�mero �ndice o nombre de la columna (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	//Fila: indice de la fila. El array empieza en 0 y nos llamar�n a partir del �ndice 1
	function ValorF ($Columna,$Fila,$iso="") {            		

		$AuxValorF = $this->Valor($Columna,$Fila);
		
		//Formateamos seg�n el tipo o dato
		switch ($this -> TipoDatoColumna($Columna)) {
			case "TIME":
				$strSalida = $this->segundos_tiempo($AuxValorF);
				break;
			case "FLOAT":
				$strSalida =  number_format($AuxValorF, 2, ',', '.'); 
				break;				
			case "INTEGER":
				$strSalida =  number_format($AuxValorF, 0, ',', '.');
				break;
			case "PERCENT":
				$strSalida =  number_format($AuxValorF, 2, ',', '.') . '%';
				break;  
			default:
				$strSalida =  $AuxValorF;
				break;  
		}//Cierre switch 		

		return $strSalida;
	}
	//----------------------------------------------------------------------------------------------------------------------------	
	
	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el total de la Metrica dada. 
	//Metrica: n�mero �ndice o nombre de la Metrica (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	function Total($Metrica) {            		
		If (is_numeric($Metrica) ) {
				//Hallamos el nombre de la m�trica correcta
				$IndiceReal = $this->Google_Service_Analytics_McfData->query->metrics[$Metrica-1];			
				return $this->Google_Service_Analytics_McfData->totalsForAllResults[$IndiceReal];	
		} else {
				//Recorremos todos los columnHeaders, hasta que nos coincide el [name]
				foreach ($this->Google_Service_Analytics_McfData->query->metrics as $metric) {
					$strNombreMetrica = strtoupper($metric);
					$strNombreMetrica = str_replace(" ","",$strNombreMetrica);
					$strNombreMetrica = str_replace("mcf:","",$strNombreMetrica);
					$strAuxColumna	    = strtoupper($Metrica);
					$strAuxColumna		= str_replace(" ","",$strAuxColumna);		
					if ( $strNombreMetrica == $strAuxColumna ) {
						return $this->Google_Service_Analytics_McfData->totalsForAllResults[$metric];
						break;
					}
				}
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el valor formateado devuelto para la METRICA dada
	function TotalF ($Metrica) {            		
		$AuxValorF = $this->Total($Metrica);
		//Formateamos seg�n el tipo o nombre de dato
		switch ($this -> TipoDatoColumna($Metrica)) {
			case "TIME":
				return $this->segundos_tiempo($AuxValorF);
				break;
			case "FLOAT":
				return number_format($AuxValorF, 2, ',', '.');
				break;				
			case "INTEGER":
				return number_format($AuxValorF, 0, ',', '.');
				break;
			case "PERCENT":
				return number_format($AuxValorF, 2, ',', '.') . '%';
				break;  
			default:
				return $AuxValorF;
				break;  
		}//Cierre switch 		
	}
	//----------------------------------------------------------------------------------------------------------------------------		
	
	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos la media de la Metrica dada. 
	//Metrica: n�mero �ndice o nombre de la Metrica (string). El array empieza en 0 y nos llamar�n a partir del �ndice 1.
	function Media($Metrica) {            		
		//La media depende del tipo de dato
		switch ($this -> TipoDatoColumna($Metrica)) {
			case "TIME":
				$strSalida = $this->Total($Metrica);
				break;
			case "FLOAT":
			case "INTEGER":
			case "PERCENT": 
			default:
				$strSalida = $this->Total($Metrica) / $this->NumValores();
				break;  
		}//Cierre switch 

		return $strSalida;		
	}
	//----------------------------------------------------------------------------------------------------------------------------
	
	//----------------------------------------------------------------------------------------------------------------------------
	//Hallamos el valor formateado devuelto para la METRICA dada
	function MediaF ($Metrica) {            		
		$AuxValorF = $this->Media($Metrica);
		//Formateamos seg�n el tipo o nombre de dato
		switch ($this -> TipoDatoColumna($Metrica)) {
			case "TIME":
				return $this->segundos_tiempo($AuxValorF);
				break;
			case "FLOAT":
				return number_format($AuxValorF, 2, ',', '.');
				break;				
			case "INTEGER":
				return number_format($AuxValorF, 0, ',', '.');
				break;
			case "PERCENT":
				return number_format($AuxValorF, 2, ',', '.') . '%';
				break;  
			default:
				return $AuxValorF;
				break;  
		}//Cierre switch 		
	}
	//----------------------------------------------------------------------------------------------------------------------------	
	

}

?>






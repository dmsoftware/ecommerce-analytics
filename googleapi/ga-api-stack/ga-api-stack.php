<?php
class GoogleAnalyticsAPIStack {
	
	protected static $path = 'C:\\Inetpub\\vhosts\\dmintegra.acc.com.es\\httpdocs\\googleapi\\ga-api-stack\\';

	/** EN SISTEMAS BASADOS EN WINDOWS NO EXISTE LA EXTENSION Y FLOCK NO FUNCIONA ASI QUE PROGRAMAMOS UN SISTEMA DE BLOQUEO BASADO EN FICHEROS **/
	function sem_get($key) {
		$fAux = fopen(self::$path . 'sem.' . $key, 'w+');
		fclose($fAux);
	}

	function sem_acquire($key) {
		$fName = self::$path . 'sem.' . $key . '.LOCK_EX';	
		if (@file_exists ( $fName )){
			return false;
		} else {
			$fAux = @fopen($fName, 'w+');
			if ($fAux){
				fclose($fAux);
				return true;
			} else {
				return false;
			}
		}
	}

	function sem_release($key) {
		$fName = self::$path . 'sem.' . $key . '.LOCK_EX';
		@unlink ($fName);
	}
	
	function sem_check($key){
		$fName = self::$path . 'semprueba.' . $key . '.LOCK_EX';
		if(strtotime("now") - @filemtime($fName) > 2) {
			self::sem_release($key);
		}
	}
		
	function ftok($filename = '', $proj = ''){
		$filename = $filename . (string) $proj;
		for($key = array(); sizeof($key) < strlen($filename); $key[] = ord(substr($filename, sizeof($key), 1)));
		return dechex(array_sum($key));
	}
	/********************************************************************************************************************************************/
	
	/** PARAMETROS 
		 ** $semKey >> Identificador del semaforo
		 ** $memorySize >> Tamaño de memoria reservado
		 ** $start >> Reloj para evitar inanicion de procesos
		 ** $maxWaitTime >> Tiempo maximo de espera de un proceso en segundos
		 ** $wait >> Variable que indica si el proceso tiene que esperar o no
	**/
	public static function getAccess($semKey) {
		$memorySize = 128;
		$start = microtime(true);
		$maxWaitTime = 2;
		$wait = true;
		$response = false;

		/** SI SE CUMPLE LAS CONDICIONES DEJAMOS EL ACCESO EN CASO CONTRARIO SE MANTIENE A LA ESPERA **/
		while ($wait && ((microtime(true) - $start) < $maxWaitTime)){
			$semID = self::ftok(self::$path . 'ga-api-stack.php', $semKey);
			if (self::sem_acquire($semKey)){
				@$shmID = shmop_open($semID, 'w', 0, 0);	
				if (!empty($shmID)) {
					$shmData = shmop_read($shmID, 0, $memorySize);
					$shmDataArray = unserialize($shmData);		
					$rcTime = $shmDataArray['rcTime'];
					$rcCount = $shmDataArray['rcCount'];		
					if ((microtime(true) - $rcTime) > $maxWaitTime) {
						$shmDataArray['rcTime'] = microtime(true);
						$shmDataArray['rcCount'] = 1;
						shmop_write($shmID, serialize($shmDataArray), 0);
						$wait = false;
					} else if($shmDataArray['rcCount'] < 10) {
						$shmDataArray['rcCount']++;
						shmop_write($shmID, serialize($shmDataArray), 0);
						$wait = false;
					} 
					shmop_close($shmID);
				} else {
					$shmID = shmop_open($semID, 'c', 0666, $memorySize);
					if ($shmID) {
						$shmDataArray = array('rcTime' => microtime(true), 'rcCount' => 1);
						shmop_write($shmID, serialize($shmDataArray), 0);
						shmop_close($shmID);
					}
					$wait = false;
				}
				self::sem_release($semKey);
			} else {
				self::sem_check($semKey);
			}
			$response = $response || $wait;
		}
		return $response;
	}
}
?>
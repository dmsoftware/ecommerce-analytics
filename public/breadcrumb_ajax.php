<?php include("includes/conf.php");
	  include("informes/funciones_ajax.php");

if(isset($_POST["idvista"]) && isset($_POST["dev"])){

	$vistaDev = propiedadVista($_POST["idvista"]);

	foreach ($vistaDev["elementos"] as $key => $vDev) {

		switch ($_POST["dev"]) {

			case 'propiedad':

				$appico = "globe";
				if($vDev["app"] == 1){
					$appico = "mobile";
				}
				$devuelve = '<i class="fa fa-'.$appico.'"></i> '.$vDev["nombre_propiedad"];

				break;

			case 'vista':

				$devuelve = $vDev["nombre_vista"];
				
				break;

			case 'url':

				$devuelve = $vDev["url_vista"];
			
				break;
			
			default:
				
				break;

		}//Fin switch

	}//Fin foreach

	echo $devuelve;

}else if( isset($_POST["fechaini"]) && isset($_POST["fechafin"]) ){

	echo rangoFechas($_POST["fechaini"],$_POST["fechafin"]);

}//Fin si existen los post

?>
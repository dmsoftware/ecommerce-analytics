﻿<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side_campanas.php"); 
	include("../informes/funciones_ajax.php"); 

?>

<div class="main-content">

<?php

include("../includes/breadcrumb.php");

//SI ESTAMOS EN CAMPAÑAS ACUTALIZAMOS LA BASE DE DATOS

function rellenar_loc_campanas_comparacion_trans_mes($anno,$mes,$vista){

	$strDimensions="ga:campaign,ga:source,ga:medium";
	$pr = propiedadVista($vista);
	  foreach ($pr["elementos"] as $key => $pasoc) {
	      $strProyectoAsociado = $pasoc["proyectoasociado"];
	  }
	 if($mes < 10){
	 	$nmes = '0'.$mes;
	 }else{
	 	$nmes = $mes;
	 }
	 $datFechaInicioFiltro = $anno.'-'.$nmes.'-01';
	$nuevafecha = strtotime ( '+1 month' , strtotime ( $datFechaInicioFiltro ) ) ;
	$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
	$nuevafecha = strtotime ( '-1 day' , strtotime ( $nuevafecha ) ) ;
	$datFechaFinFiltro = date ( 'Y-m-d' , $nuevafecha );
	//echo $datFechaFinFiltro;
	//Si está vacio insertamos
	//GA
	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = 'ga:'. $vista;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = "ga:sessions";
	$obj_Datos_Api -> optParams            = array(
												'dimensions' => $strDimensions,
												'start-index' => 1,
	                                            'max-results' => 1000);
	$obj_Datos_Api -> Construccion();

	//echo 'total:'.$obj_Datos_Api->NumValores().'<br>';

	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		//echo $obj_Datos_Api->valor('sessions',$x)."<br>";
		//Si existe la fila actualizamos si no insertamos
		$sql  = "IF EXISTS (SELECT LOCCCM_CODIGO FROM LOC_CAMPANAS_COMPARACION_TRANS_MES ";
	     $sql .= "   WHERE LOCCCM_ANALYTICS_ID_VISTA='".$vista."' ";
	     $sql .= "         and LOCCCM_SUMES=".$mes;
	     $sql .= "         and LOCCCM_SUANO=".$anno;
	     $sql .= "         and LOCCCM_CAMPANA ='".$obj_Datos_Api->Valor("campaign",$x)."'";
	     $sql .= "         and LOCCCM_MEDIO ='".$obj_Datos_Api->Valor("medium",$x)."'";
	     $sql .= "         and LOCCCM_FUENTE = '".$obj_Datos_Api->Valor("source",$x)."' )" ;
	     $sql .= "       UPDATE  LOC_CAMPANAS_COMPARACION_TRANS_MES ";
	     $sql .= "       SET     LOCCCM_SESIONES =".$obj_Datos_Api->Valor("sessions",$x);
	     $sql .= "       WHERE   LOCCCM_ANALYTICS_ID_VISTA='".$vista."' ";
	     $sql .= "               and LOCCCM_SUMES=".$mes; 
	     $sql .= "               and LOCCCM_SUANO=".$anno;
	     $sql .= "               and LOCCCM_CAMPANA ='".$obj_Datos_Api->Valor("campaign",$x)."'";
	     $sql .= "               and LOCCCM_MEDIO ='".$obj_Datos_Api->Valor("medium",$x)."'";
	     $sql .= "               and LOCCCM_FUENTE = '".$obj_Datos_Api->Valor("source",$x)."'";
	     $sql .= " ELSE ";
	     $sql .= "       INSERT INTO LOC_CAMPANAS_COMPARACION_TRANS_MES ";
	     $sql .= "       (LOCCCM_ANALYTICS_ID_VISTA, LOCCCM_SUMES, LOCCCM_SUANO, LOCCCM_CAMPANA, LOCCCM_MEDIO, LOCCCM_FUENTE, LOCCCM_SESIONES, LOCCCM_TIPO) ";
	     $sql .= "       VALUES ";
	     $sql .= "       ('".$vista."',".$mes.",".$anno.",'".$obj_Datos_Api->Valor('campaign',$x)."','".$obj_Datos_Api->Valor('medium',$x)."','".$obj_Datos_Api->Valor('source',$x)."',".$obj_Datos_Api->Valor('sessions',$x).",'".comprobarTipo($obj_Datos_Api->Valor('medium',$x),$obj_Datos_Api->Valor('source',$x),$obj_Datos_Api->Valor('campaign',$x))."')";

	     $c = Nuevo_PDO();
	     $stmt = $c->prepare($sql);
	     $result = $stmt->execute();
		//echo $sql."<br><br>";
	}
	//MCF
	$metricas = 'mcf:firstInteractionConversions,mcf:assistedConversions,mcf:lastInteractionConversions,mcf:assistedValue,mcf:firstInteractionValue,mcf:lastInteractionValue';
	$strDimensions = 'mcf:campaignName,mcf:medium,mcf:source,mcf:adwordsCampaignID';
	$filtro = 'mcf:conversionType==Goal';

	$obj_Datos_Api_mcf = new Datos_Api_Mcf(); // Instanciamos la clase Datos_Api_Mcf
	$obj_Datos_Api_mcf -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_mcf -> idVista              ='ga:'. $vista;
	$obj_Datos_Api_mcf -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_mcf -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_mcf -> metrics              = $metricas;
	$obj_Datos_Api_mcf -> optParams            = array(
												'dimensions' => $strDimensions,
	                                            'filters' => $filtro,
												'start-index' => 1,
	                                            'max-results' => 1000
	                                            );  
	$obj_Datos_Api_mcf -> Construccion();
	for ($x=1; $x<=$obj_Datos_Api_mcf->NumValores(); $x++) {

		$sql  = "IF EXISTS (SELECT LOCCCM_CODIGO FROM LOC_CAMPANAS_COMPARACION_TRANS_MES ";
	    $sql .= "   WHERE LOCCCM_ANALYTICS_ID_VISTA='".$vista."' ";
	    $sql .= "         and LOCCCM_SUMES=".$mes;
	    $sql .= "         and LOCCCM_SUANO=".$anno;
	    $sql .= "         and LOCCCM_CAMPANA ='".$obj_Datos_Api_mcf->Valor(1,$x)."'";
	    $sql .= "         and LOCCCM_MEDIO ='".$obj_Datos_Api_mcf->Valor(2,$x)."'";
	    $sql .= "         and LOCCCM_FUENTE = '".$obj_Datos_Api_mcf->Valor(3,$x)."' )" ;
	    $sql .= "       UPDATE  LOC_CAMPANAS_COMPARACION_TRANS_MES ";
	    $sql .= "       SET     LOCCCM_CONVERSIONES_ASISTIDAS_NUM =".$obj_Datos_Api_mcf->Valor(6,$x).",LOCCCM_CONVERSIONES_DIRECTAS_NUM = ".$obj_Datos_Api_mcf->Valor(7,$x);
	    $sql .= "       WHERE   LOCCCM_ANALYTICS_ID_VISTA='".$vista."' ";
	    $sql .= "               and LOCCCM_SUMES=".$mes; 
	    $sql .= "               and LOCCCM_SUANO=".$anno;
	    $sql .= "               and LOCCCM_CAMPANA ='".$obj_Datos_Api_mcf->Valor(1,$x)."'";
	    $sql .= "               and LOCCCM_MEDIO ='".$obj_Datos_Api_mcf->Valor(2,$x)."'";
	    $sql .= "               and LOCCCM_FUENTE = '".$obj_Datos_Api_mcf->Valor(3,$x)."'";
	    $sql .= " ELSE ";
	    $sql .= "       INSERT INTO LOC_CAMPANAS_COMPARACION_TRANS_MES ";
	    $sql .= "       (LOCCCM_ANALYTICS_ID_VISTA, LOCCCM_SUMES, LOCCCM_SUANO, LOCCCM_CAMPANA, LOCCCM_MEDIO, LOCCCM_FUENTE, LOCCCM_CONVERSIONES_ASISTIDAS_NUM, LOCCCM_CONVERSIONES_DIRECTAS_NUM, LOCCCM_TIPO) ";
	    $sql .= "       VALUES ";
	    $sql .= "       ('".$vista."',".$mes.",".$anno.",'".$obj_Datos_Api_mcf->Valor(1,$x)."','".$obj_Datos_Api_mcf->Valor(2,$x)."','".$obj_Datos_Api_mcf->Valor(3,$x)."',".$obj_Datos_Api_mcf->Valor(6,$x).",".$obj_Datos_Api_mcf->Valor(7,$x).",'".comprobarTipo($obj_Datos_Api_mcf->Valor(2,$x),$obj_Datos_Api_mcf->Valor(3,$x),$obj_Datos_Api_mcf->Valor(1,$x))."')";

	    $c = Nuevo_PDO();
	    $stmt = $c->prepare($sql);
	    $result = $stmt->execute();
	}

	//Actualizamos LOC_CAMPANAS_COMPARACION_TRANS_FECHAS con la fecha actual
	$sql  = "IF EXISTS (SELECT LOCCF_CODIGO FROM LOC_CAMPANAS_COMPARACION_TRANS_FECHAS ";
	$sql .= "   WHERE LOCCF_ANALYTICS_ID_VISTA='".$vista."' ";
	$sql .= "         and LOCCF_SUMES=".$mes;
	$sql .= "         and LOCCF_SUANO=".$anno.")";
	$sql .= "       UPDATE  LOC_CAMPANAS_COMPARACION_TRANS_FECHAS ";
	$sql .= "       SET     LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES = '".date("Y-m-d H:i:s")."'";
	$sql .= "       WHERE   LOCCF_ANALYTICS_ID_VISTA = '".$vista."' ";
	$sql .= "               and LOCCF_SUMES=".$mes; 
	$sql .= "               and LOCCF_SUANO=".$anno;
	$sql .= " ELSE ";
	$sql .= "       INSERT INTO LOC_CAMPANAS_COMPARACION_TRANS_FECHAS ";
	$sql .= "       (LOCCF_ANALYTICS_ID_VISTA, LOCCF_SUMES, LOCCF_SUANO, LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES) ";
	$sql .= "       VALUES ";
	$sql .= "       ('".$vista."',".$mes.",".$anno.", '".date("Y-m-d H:i:s")."')";

	$c = Nuevo_PDO();
	$stmt = $c->prepare($sql);
	$result = $stmt->execute();
	//echo $sql;
	//Actualizamos LOC_CAMPANAS_COMPARACION_TRANS_FECHAS
	//echo"<br>Filas actualizadas<br>";
      	
}


	
?>





	<div class="col-sm-12">
	<!--******************DATATABLE DE CAMPAÑAS***************************-->

		


<script type="text/javascript">
    $(document).ready(function () {
        $(".VerTabla").click(function () {
            $('.TablaGeneralDatos').toggle();
        });
        $(".NuevaBusqueda").click(function () {
            $('#CapaOcultoTodo').toggle();
            return false;
        });
        $('#BotoNuevaBusqueda').click(function () {
			if ($("#FormBusqueda input:checkbox:checked").length > 0){
		    	$('#CapaCargando').toggle();
	            $('.FiltradoCab').toggle();
			}else{
				alert('Debes seleccionar alguna campaña');
				return false;
			}
        });

        $(".SelectAll").click(function () {
            $(".CamSelInput").attr('checked', true);
        });
        $(".SelectNone").click(function () {
            $(".CamSelInput").attr('checked', false);
        });

        $(".ActualizoMesAnio").click(function () {
            var valorMesAnio = $('#m option:selected').attr("id");
            $(this).attr('href', '?m=' + valorMesAnio);
            $('#CapaCargando').toggle();
            $('.FiltradoCab').toggle();
        });

        var htmlString = $('#DatosTotalesTabla').html();
        $('#table_div2').html(htmlString);
        $('#DatosTotalesTabla').remove();

        qTablaMuestro(1);

    });
	function qTablaMuestro(DatoGrafico) {
		$('.graficoTablas').addClass('OcultoTabla');
		$('.graficoTablas').removeClass('MuestroTabla');
		$('#grafico' + DatoGrafico).removeClass('OcultoTabla');
		$('#grafico' + DatoGrafico).addClass('MuestroTabla');
	};	
</script> 




<?php

define("DOMINIO_LISCAMP",$strDominio);
define("VISTA_LISCAMP",$primera_vista);

$fechafin = date("Y-m-d");
$fechaini = strtotime ( '-1 year' , strtotime ( $fechafin ) ) ;
$fechaini = date ( 'Y-m-d' , $fechaini );

$sql="SELECT ANOS_MESES.ANO,ANOS_MESES.MES,LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES 
FROM LOC_CAMPANAS_COMPARACION_TRANS_FECHAS 
      RIGHT JOIN (SELECT year(DATEADD(month, VAUX.number,'".$fechaini."')) as ANO, month(DATEADD(month, VAUX.number,'".$fechaini."')) as MES 
                        FROM [master].[dbo].[spt_values] VAUX WHERE VAUX.type = 'P' AND DATEADD(month, VAUX.number, '".$fechaini."') <= '".$fechafin."' ) ANOS_MESES 
            ON ANOS_MESES.ANO = LOC_CAMPANAS_COMPARACION_TRANS_FECHAS.LOCCF_SUANO 
                  AND ANOS_MESES.MES = LOC_CAMPANAS_COMPARACION_TRANS_FECHAS.LOCCF_SUMES 
WHERE LOCCF_ANALYTICS_ID_VISTA = ".VISTA_LISCAMP."
      AND (LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES < dateadd(d,1,dateadd(m,1,ltrim(str(LOCCF_SUANO)) + RIGHT('00'+ ltrim(str(LOCCF_SUMES)),2) + '01')) OR LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES IS NULL )      


UNION ALL 
                       
SELECT ANOS_MESES.ANO,ANOS_MESES.MES,NULL AS LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES
FROM (SELECT year(DATEADD(month, VAUX.number,'".$fechaini."')) as ANO, month(DATEADD(month, VAUX.number,'".$fechaini."')) as MES 
        FROM [master].[dbo].[spt_values] VAUX WHERE VAUX.type = 'P' AND DATEADD(month, VAUX.number, '".$fechaini."') <= '".$fechafin."' ) ANOS_MESES 
WHERE NOT EXISTS (SELECT LOCCF_FECHA_ULTIMO_FUNCION_CONVERSIONES
                          FROM LOC_CAMPANAS_COMPARACION_TRANS_FECHAS
                          WHERE LOCCF_ANALYTICS_ID_VISTA = ".VISTA_LISCAMP."
                             AND LOC_CAMPANAS_COMPARACION_TRANS_FECHAS.LOCCF_SUANO = ANOS_MESES.ANO 
                             AND LOC_CAMPANAS_COMPARACION_TRANS_FECHAS.LOCCF_SUMES = ANOS_MESES.MES )
ORDER BY ANO,MES";
//echo $sql;
$c = Nuevo_PDO();
$stmt = $c->prepare($sql);
$result = $stmt->execute();

if ($result && $stmt->rowCount() != 0){
   $count = 0;
   //echo "Filas: ".$stmt->rowCount()."<br>";
   while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
   	
   	rellenar_loc_campanas_comparacion_trans_mes($fila[0],$fila[1],VISTA_LISCAMP);

  }

}

function sufijo_comp($metrica){

	switch ($metrica) {
		case 'transacciones':
			$sub = '';
			break;
		case 'pedido_medio':
			$sub = '€';
			break;
		case 'valor':
			$sub = '€';
			break;
		case 'gastos':
			$sub = '€';
			break;
		case 'rendimiento':
			$sub = '€';
			break;
		case 'sesiones':
			$sub = '';
			break;
		case 'adquisicion':
			$sub = '€';
			break;
		case 'comision':
			$sub = '%';
			break;
		case 'tasa_conversion':
			$sub = '%';
			break;
		case 'sesiones_unpedido':
			$sub = '';
			break;
		
		default:
			$sub = '';
			break;
	}

	return $sub;
}


if (isset( $_GET["agrupacion"] )) {
	$agrupacion = $_GET["agrupacion"];
	define("CAMP_AGRUPACION",$agrupacion);
}else{
	$agrupacion = 'CAMPANA';
	define("CAMP_AGRUPACION",$agrupacion);
}

$dataComparativas_array= array();


//Función que devuelve el historial de informes
function dataComparativas($vista){

	global $dataComparativas_array;
	if (empty($dataComparativas_array)) {
			
		//calculamos fechas actual y un año atras
		$fechafin = date("Y-m-d");
		$fechaini = strtotime ( '-1 year' , strtotime ( $fechafin ) ) ;
		$fechaini = date ( 'Y-m-d' , $fechaini );

		$devuelve = array();
		$c = Nuevo_PDO();
		$sql = "EXEC [dbo].[LOC_CAMPANAS_COMPARACION]
		            @ANALYTICS_ID_VISTA = :vista,
		            @FECHA_INICIO = :fechaini,
		            @FECHA_FIN = :fechafin,
		            @CAMPANA_DESGLOSE = '',
		            @TIPO_DESGLOSE = '',
		            @AGRUPACION = '".CAMP_AGRUPACION."',
		            @USUARIO = ''";
		$stmt = $c->prepare($sql);
		$result = $stmt->execute(array(':vista' => $vista,':fechaini' => $fechaini,':fechafin' => $fechafin));


		if ($result && $stmt->rowCount() != 0){
		   
		    $count = 0;
		    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
		      $count++;
		      $id = $fila[0];
		      $devuelve["elementos"][$count]["anno"] 		  = $fila[0];
		      $devuelve["elementos"][$count]["mes"] 		  = $fila[1];
		      $devuelve["elementos"][$count]["tipo"] 		  = $fila[2];
		      $devuelve["elementos"][$count]["campana"] 	  = $fila[5];
		      $n = $fila[6];
		      if(empty($n)){ $n = 0; }
		      $devuelve["elementos"][$count]["medio_fuente"] 	        = $n;
		      $n = $fila[9]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["transacciones"]           = $n;
		      $n = $fila[10]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["valor"] 		  	        = $n;
		      $n = $fila[11]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["gastos"]                  = $n;
		      $n = $fila[12]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["pedido_medio"] 	        = $n;
		      $n = $fila[13]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["rendimiento"]  	        = $n;
		      $n = $fila[14]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["sesiones"]     	        = $n;
		      $n = $fila[15]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["adquisicion"]  	        = $n;
		      $n = $fila[16]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["comision"]   	            = $n;
		      $n = $fila[17]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["tasa_conversion"]         = $n;
		      $n = $fila[18]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["sesiones_unpedido"]       = $n;
		      $n = $fila[19]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["retorno_inversion"]       = $n;
		      $n = $fila[20]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["conversiones"] 	        = $n;
		      $n = $fila[21]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["conversiones_euros"]      = $n;
		      $n = $fila[22]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["tasa_conversiones"]       = $n;
		      $n = $fila[23]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["sesiones_unaconversion"]  = $n;
		      $n = $fila[24]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["rendimiento_conversion"]  = $n;
		      $n = $fila[25]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["retorno_conversion"] 	    = $n;
		      $n = $fila[26]; 
		      if(empty($n)){ $n = 0; } 
		      $devuelve["elementos"][$count]["coste_conversion"] 	    = $n;

		    }
		    $devuelve["cantidad"]=$count;

		}else{
		    $devuelve["cantidad"]=0;
		    $count =0;
		}

		$dataComparativas_array = $devuelve;
		return $devuelve; 

	}//Si está vacio la array
	else{
		return $dataComparativas_array;
	}

}

$dataFechaFichero_array= array();
//Función que devuelve la última fecha de los ficheros
function dataFechaFichero($vista){

	global $dataFechaFichero_array;
	if (empty($dataFechaFichero_array)) {

		//calculamos fechas actual y un año atras
		$fechafin = date("Y-m-d");
		$fechaini = strtotime ( '-1 year' , strtotime ( $fechafin ) ) ;
		$fechaini = date ( 'Y-m-d' , $fechaini );

		$devuelve = array();
		$c = Nuevo_PDO();
		$sql = "EXEC [dbo].[LOC_CAMPANAS_COMPARACION]
		            @ANALYTICS_ID_VISTA = :vista,
		            @FECHA_INICIO = :fechaini,
		            @FECHA_FIN = :fechafin,
		            @CAMPANA_DESGLOSE = '',
		            @TIPO_DESGLOSE = '',
		            @AGRUPACION = '".CAMP_AGRUPACION."',
		            @USUARIO = ''";
		$stmt = $c->prepare($sql);
		$result = $stmt->execute(array(':vista' => $vista,':fechaini' => $fechaini,':fechafin' => $fechafin));
		
		$fila = $stmt->fetch();
		$stmt->nextRowset();

		$count = 0;
		while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
		  $devuelve["elementos"][$count]["anno"] 		  = $fila[0];
		  $devuelve["elementos"][$count]["mes"] 		  = $fila[1];
		  $devuelve["elementos"][$count]["fecha_fichero"] = $fila[2];
		  $count++;
		}
		$devuelve["cantidad"]=$count;

		$dataFechaFichero_array = $devuelve;
	  return $devuelve; 
	}else{
	  return $dataFechaFichero_array;	
	}

}

$dataMargenes_array= array();
//Función que devuelve los margenes
function dataMargenes($vista){

	global $dataMargenes_array;
	if (empty($dataMargenes_array)) {
		//calculamos fechas actual y un año atras
		$fechafin = date("Y-m-d");
		$fechaini = strtotime ( '-1 year' , strtotime ( $fechafin ) ) ;
		$fechaini = date ( 'Y-m-d' , $fechaini );

		$devuelve = array();
		$c = Nuevo_PDO();
		$sql = "EXEC [dbo].[LOC_CAMPANAS_COMPARACION]
		            @ANALYTICS_ID_VISTA = :vista,
		            @FECHA_INICIO = :fechaini,
		            @FECHA_FIN = :fechafin,
		            @CAMPANA_DESGLOSE = '',
		            @TIPO_DESGLOSE = '',
		            @AGRUPACION = '".CAMP_AGRUPACION."',
		            @USUARIO = ''";
		$stmt = $c->prepare($sql);
		$result = $stmt->execute(array(':vista' => $vista,':fechaini' => $fechaini,':fechafin' => $fechafin));
		
		$fila = $stmt->fetch();
		$stmt->nextRowset();
		$stmt->nextRowset();

		$count = 0;
		while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
		  $devuelve["elementos"][$count]["anno"] 		  = $fila[0];
		  $devuelve["elementos"][$count]["mes"] 		  = $fila[1];
		  $count++;
		}
		$devuelve["cantidad"]=$count;

		$dataMargenes_array = $devuelve;
	  return $devuelve;  

  }else{
	return $dataMargenes_array;	
  }
}



function colores_campanas(){

	$colores = array();
	$colores[0]  = "#3366cc";
	$colores[1]  = "#dc3912";
	$colores[2]  = "#ff9900";
	$colores[3]  = "#109618";
	$colores[4]  = "#990099";
	$colores[5]  = "#0099c6";
	$colores[6]  = "#dd4477";
	$colores[7]  = "#66aa00";
	$colores[8]  = "#b82e2e";
	$colores[9]  = "#f5a6ef";
	$colores[10] = "#22aa99";
	$colores[11] = "#8fabe3";
	$colores[12] = "#b96c70";
	$colores[13] = "#dc8338";
	$colores[14] = "#6c8d26";
	$colores[15] = "#7b853d";
	$colores[16] = "#615d6a";
	$colores[17] = "#56a5ff";
	$colores[18] = "#db25ff";
	$colores[19] = "#1447ff";
	$colores[20] = "#ffda0a";
	$colores[21] = "#ff9b0a";

	$compinfo_camp = dataComparativas(VISTA_LISCAMP);
	$cont=0;
	$sw=0;
	foreach ($compinfo_camp["elementos"] as $key => $cc) {
		if($sw==0){

			$campana = $cc["campana"];
			if(!empty($campana)){
				if( trim($campana) == '(not set)'){
					$campana = $cc["tipo"];
				}else{
					$cont++;
				}
			}else{
				$sw=1;
			}
		}
	}
	//directo
	$colores[$cont] = "#7D5FA9";
	//enlaces
	$colores[$cont+1] = "#01D5FB";
	//seo
	$colores[$cont+2] = "#FE6B5B";
	//social
	$colores[$cont+3] = "#FBC63C";

	return $colores;

}


function dataComparativas_campanas($vista,$campana){

	//calculamos fechas actual y un año atras
	$fechafin = date("Y-m-d");
	$fechaini = strtotime ( '-1 year' , strtotime ( $fechafin ) ) ;
	$fechaini = date ( 'Y-m-d' , $fechaini );
	
	switch ($campana) {
		case 'seo':
			$camp = '';
			$tipo = 'seo'; 
			break;
		case 'enlaces':
			$camp = '';
			$tipo = 'enlaces'; 
			break;
		case 'directo':
			$camp = '';
			$tipo = 'directo'; 
			break;
		case 'social':
			$camp = '';
			$tipo = 'social'; 
			break;
		default:
			$camp = $campana;
			$tipo = ''; 
			break;
	}

	$devuelve = array();
	$c = Nuevo_PDO();
	$sql = "EXEC [dbo].[LOC_CAMPANAS_COMPARACION]
	            @ANALYTICS_ID_VISTA = :vista,
	            @FECHA_INICIO = :fechaini,
	            @FECHA_FIN = :fechafin,
	            @CAMPANA_DESGLOSE = '".$camp."',
	            @TIPO_DESGLOSE ='".$tipo."',
	            @AGRUPACION ='".CAMP_AGRUPACION."',
	            @USUARIO = ''";
	//echo $sql;
	$stmt = $c->prepare($sql);
	$result = $stmt->execute(array(':vista' => $vista,':fechaini' => $fechaini,':fechafin' => $fechafin));


	if ($result && $stmt->rowCount() != 0){
	    $count = 0;
	    
	    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
	      $count++;
	      $id = $fila[0];
	      $devuelve["elementos"][$count]["anno"] 		= $fila[0];
	      $devuelve["elementos"][$count]["mes"] 		= $fila[1];
	      $devuelve["elementos"][$count]["tipo"] 		= $fila[2];
	      $devuelve["elementos"][$count]["campana"] 	= $fila[5];
	      $devuelve["elementos"][$count]["desglose"] 	= $fila[6];
	      $devuelve["elementos"][$count]["medio"] 	   	= $fila[7];
	      $devuelve["elementos"][$count]["fuente"] 	   	= $fila[8];
	      $n = $fila[9];
	      if(empty($n)){ $n = 0; }
	      $devuelve["elementos"][$count]["transacciones"]     		= $n;
	      $n = $fila[10];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["valor"] 		  	  		= $n;
	      $n = $fila[11];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["gastos"]            		= $n;
	      $n = $fila[12];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["pedido_medio"] 	  		= $n;
	      $n = $fila[13];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["rendimiento"]  	  		= $n;
	      $n = $fila[14];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["sesiones"]     	  		= $n;
	      $n = $fila[15];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["adquisicion"]  	  		= $n;
	      $n = $fila[16];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["comision"]   	      		= $n;
	      $n = $fila[17];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["tasa_conversion"]   		= $n;
	      $n = $fila[18];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["sesiones_unpedido"] 		= $n;
	      $n = $fila[19];		
	      if(empty($n)){ $n = 0; }		
	      $devuelve["elementos"][$count]["retorno_inversion"] 		= $n;
	      $n = $fila[20]; 
		  if(empty($n)){ $n = 0; } 
		  $devuelve["elementos"][$count]["conversiones"] 	        = $n;
		  $n = $fila[21]; 
		  if(empty($n)){ $n = 0; } 
		  $devuelve["elementos"][$count]["conversiones_euros"]      = $n;
		  $n = $fila[22]; 
		  if(empty($n)){ $n = 0; } 
		  $devuelve["elementos"][$count]["tasa_conversiones"]       = $n;
		  $n = $fila[23]; 
		  if(empty($n)){ $n = 0; } 
		  $devuelve["elementos"][$count]["sesiones_unaconversion"]  = $n;
		  $n = $fila[24]; 
		  if(empty($n)){ $n = 0; } 
		  $devuelve["elementos"][$count]["rendimiento_conversion"]  = $n;
		  $n = $fila[25]; 
		  if(empty($n)){ $n = 0; } 
		  $devuelve["elementos"][$count]["retorno_conversion"] 	    = $n;
		  $n = $fila[26]; 
		  if(empty($n)){ $n = 0; } 
		  $devuelve["elementos"][$count]["coste_conversion"] 	    = $n;
	      //echo "data:".$fila[5]."<br>";
	    }

	    $devuelve["cantidad"]=$count;
	}else{
	    $devuelve["cantidad"]=0;
	    $count =0;
	}

	return $devuelve;

}

$cabeceras_global = '';
function mostrarCantidad(){

	global $cabeceras_global;
	if (empty($cabeceras_global)) {

		$cabeceras = 0;
		$compinfo = dataComparativas(VISTA_LISCAMP);
		foreach ($compinfo["elementos"] as $key => $ci) {
			
			if($ci["campana"] == ""){
				$cabeceras_sw = 1;
			}
			if($cabeceras_sw == 0){
				$cabeceras++;
			}	

		}

		$cabeceras_global = $cabeceras;
		return $cabeceras;

	}else{
		return $cabeceras_global;
	}
}	



function mostrarGraficos($metrica){
	global $trans;
	$cabeceras = "['Mes',";
	$cabeceras_sw = 0;
	$contenido = "";
	$contenido_sw = 0;
	$contenido_fecha = "";
	$camanno = '';
		$compinfo = dataComparativas(VISTA_LISCAMP);
		foreach ($compinfo["elementos"] as $key => $ci) {
			
			if($ci["campana"] == ""){
				$cabeceras_sw = 1;
			}
			if($cabeceras_sw == 0){
				$campana = $ci["campana"];
				if(trim($campana) == "(not set)"){
						$campana = $trans->__(ucwords($ci["tipo"]),false);
					}
				$cabeceras .= "'".trim($campana)."',";
			}

			if($contenido_fecha != $ci["mes"].$ci["anno"]){
				if($contenido_fecha != ""){ 
					$contenido = substr($contenido, 0, -1); 
					$contenido .= "],"; 
				}
				$mescf = traducirMes($ci["mes"]);
				if($camanno != $ci["anno"]){
					$camanno_mostrar = "(".$ci["anno"][2].$ci["anno"][3].")";
				}else{
					$camanno_mostrar = "";
				}
				$camanno = $ci["anno"];
				$contenido .= "['".$trans->__($mescf[$ci["mes"]]["res"], false).$camanno_mostrar."',".$ci[$metrica].",";
				$contenido_fecha = $ci["mes"].$ci["anno"];
			}else{
				$contenido .= "".$ci[$metrica].",";
			}
			
		}
	$contenido = substr($contenido, 0, -1);
	$contenido .= "]";
	$cabeceras .= "'www.".DOMINIO_LISCAMP."',";
	$cabeceras = substr($cabeceras, 0, -1);
	$cabeceras .= "]";

	$filaChart = "";
		$filaChart .= $cabeceras.",";
		$filaChart .= $contenido;
	$filaChart .= "";

	return $filaChart;
}	



function mostrarAgrupaciones($campana,$metrica,$color,$grupo){
	$compinfo = dataComparativas_campanas(VISTA_LISCAMP,trim($campana));
	$cabeceras_camp = 0;
	$compinfo_camp = dataComparativas_campanas(VISTA_LISCAMP,trim($campana));
	$totales = array();
	$guardar_fuentes = array();
	$guardar_fuentes_sw = 0;
		$top = 10;
		$top_sw = 0;
	
	if($compinfo_camp["cantidad"] != 0){
	foreach ($compinfo_camp["elementos"] as $key => $ci_camp) {
		if($top_sw < $top){
		
		if($ci_camp["campana"] == ""){
			$cabeceras_sw_camp = 1;
		}
		if($cabeceras_sw_camp == 0){
			$cabeceras_camp++;

			if(trim($ci_camp["campana"]) !=  "(not set)"){
				$clasenom = str_replace(" ","_", trim($ci_camp["campana"]) );
			}else{
				$clasenom = str_replace(" ","_", trim($ci_camp["tipo"]) );
			}
		
			$contenido .= '<tr class="grupo'.$grupo.' agrupacion '.$clasenom.'">';
			$contenido_fecha = '';
			$contenido_sw = 0;
			$contenido_clase = 'class="MesPar" align="right"';
			$total = 0;
			$totales_sw = 0;
			

			//Comprobar si existe en la array
			$existearray = 0;
			foreach ($guardar_fuentes as $key => $gf) {
				if(CAMP_AGRUPACION =="CAMPANA"){

					if(trim($ci_camp["fuente"].'/'.strtolower($ci_camp["medio"])) == trim($gf)){
						$existearray = 1;
					}

				}else{

					if(trim($ci_camp["campana"].'/'.strtolower($ci_camp["medio"]).'/'.$ci_camp["fuente"]) == trim($gf)){
						$existearray = 1;
					}

				}
			}

			if($existearray==0){
				
				foreach ($compinfo["elementos"] as $key => $ci) {

					if(CAMP_AGRUPACION =="CAMPANA"){
						$campana = $ci["fuente"].'/'.strtolower($ci["medio"]);
						$campana_camp = $ci_camp["fuente"].'/'.strtolower($ci_camp["medio"]);
						$fuente = $ci["fuente"].'/'.strtolower($ci["medio"]);
					}else{
						$campana = $ci["campana"].'/'.$ci["fuente"].'/'.strtolower($ci["medio"]);
						$campana_camp = $ci_camp["campana"].'/'.$ci_camp["fuente"].'/'.strtolower($ci_camp["medio"]);;
						if(trim($ci["campana"])=="(not set)"){
							$fuente = $ci["fuente"];
						}else{
							$fuente = $ci["campana"];
						}
						
					}
						
					
					
					if(trim($campana)==trim($campana_camp)){
						
						if($contenido_fecha != $ci["mes"].$ci["anno"]){
							if($contenido_sw == 0){
								$contenido .= '<td class="NomCampana" style="border-left:solid 10px '.$color.';">';//Aquí colores
								$colcamp++;
								$contenido .= '<span style="font-size: 11px; font-style: italic; padding-left: 5px;">'.trim($fuente).'</span>';
								$contenido .= '</td>';
								$contenido_sw = 1;
							}
							
							if($contenido_clase == 'class="MesInpar" align="right"'){
								$contenido_clase = 'class="MesPar" align="right"';
							}else{
								$contenido_clase = 'class="MesInpar" align="right"';
							}

							$contenido .= '<td '.$contenido_clase.'>';
							if($ci[$metrica] != 0){
								if($ci[$metrica] < 0){
									$contenido .= "<span class='rojocomparar'>".number_format($ci[$metrica],2,',','.').sufijo_comp($metrica)."<span>";	
								}else{
									$contenido .= number_format($ci[$metrica],2,',','.').sufijo_comp($metrica);	
								}
							}else{
								$contenido .= $ci[$metrica];
							}
							$totales[$totales_sw] += $ci[$metrica];
							$totales_sw++;
							
							$contenido .= '</td>';	
							$total += $ci[$metrica];
							$contenido_fecha = $ci["mes"].$ci["anno"];							

						}
					}

					//}//fin top

					
				}
				if($total == 0){
					$contenido .= '<td class="mediaCampanaTD" align="right">0</td>';
					$contenido .= '<td class="totalCampanaTD" align="right">0</td>';
				}else{
					$media = ($total/12);

					if($media < 0){
						$spanini = "<span class='rojocomparar'>";
						$spanfin = "</span>";
					}else{
						$spanini = "";
						$spanfin = "";
					}
					$contenido .= '<td class="mediaCampanaTD" align="right">'.$spanini.number_format($media,2,',','.').sufijo_comp($metrica).$spanfin.'</td>';
					if($total < 0){
						$spanini = "<span class='rojocomparar'>";
						$spanfin = "</span>";
					}else{
						$spanini = "";
						$spanfin = "";
					}
					$contenido .= '<td class="totalCampanaTD" align="right">'.$spanini.number_format($total,2,',','.').sufijo_comp($metrica).$spanfin.'</td>';
					//$contenido .= '<td class="mediaCampanaTD" align="right">'.number_format($media,2,',','.').sufijo_comp($metrica).'</td>';
					//$contenido .= '<td class="totalCampanaTD" align="right">'.number_format($total,2,',','.').sufijo_comp($metrica).'</td>';
				}
				
				$contenido .= '</tr>';

				if(CAMP_AGRUPACION =="CAMPANA"){
					$guardar_fuentes[$guardar_fuentes_sw] = $ci_camp["fuente"].'/'.strtolower($ci_camp["medio"]);
				}else{
					$guardar_fuentes[$guardar_fuentes_sw] = $ci_camp["campana"].'/'.strtolower($ci_camp["medio"]).'/'.$ci_camp["fuente"];
				}
				
				$guardar_fuentes_sw++;
			}
				
			
			
			
		}
		}
		//echo $top_sw;
								
		$top_sw++;
		
	}
	}//Si no es 0 la cantidad de elementos
	else{
		//si es 0 
	}

	//Guardamos las campañas 
	$tot_totales = 0;

	//return $contenido;
return $contenido;
	
}	


function mostrarTabla($metrica){
	global $trans;

	$colores = colores_campanas();
	$colcamp = 0;
	$grupo = 1;
	$cabeceras = '<tr><td class="MesPar esMes"></td>';
	$contenido_fecha = '';
	$cabeceras_clase = 'class="MesInpar esMes" align="center"';
	$contenido = '';
	$compinfo = dataComparativas(VISTA_LISCAMP);
	$cabanno = '';
	foreach ($compinfo["elementos"] as $key => $ci) {
		if($contenido_fecha != $ci["mes"].$ci["anno"]){
			$mescf = traducirMes($ci["mes"]);
			if($cabanno == $ci["anno"]){
				$cabanno_mostrar = '';
			}else{
				$cabanno_mostrar = '('.$ci["anno"][2].$ci["anno"][3].')';
			}
			$cabanno = $ci["anno"];


			$cabeceras .= '<td '.$cabeceras_clase.'>'.$trans->__($mescf[$ci["mes"]]["res"], false).$cabanno_mostrar.'</td>';				
			$contenido_fecha = $ci["mes"].$ci["anno"];

			//***CLASES CABECERAS
			if($cabeceras_clase == 'class="MesInpar esMes" align="center"'){
				$cabeceras_clase = 'class="MesPar esMes" align="center"';
			}else{
				$cabeceras_clase = 'class="MesInpar esMes" align="center"';
			}

		}	
	}
	
	
	$cabeceras_camp = 0;
	$compinfo_camp = dataComparativas(VISTA_LISCAMP);
	$totales = array();
	foreach ($compinfo_camp["elementos"] as $key => $ci_camp) {
		//['MAR (14)',0,21.85,0,0,96,2.4,220]
		if($ci_camp["campana"] == ""){
			$cabeceras_sw_camp = 1;
		}
		if($cabeceras_sw_camp == 0){
			$cabeceras_camp++;

			//for ($i=0; $i < mostrarCantidad() ; $i++) { 
			if(trim($ci_camp["campana"]) !=  "(not set)"){
				$clasenom = str_replace(" ","_", trim($ci_camp["campana"]) );
			}else{
				$clasenom = str_replace(" ","_", trim($ci_camp["tipo"]) );
			}

				$contenido .= '<tr class="selectorclick  '.$clasenom.' tr_'.$grupo.'" name="'.$grupo.'">';
				$contenido_fecha = '';
				$contenido_sw = 0;
				$contenido_clase = 'class="MesPar" align="right"';
				$total = 0;
				$totales_sw = 0;
				$medio_fuente = 0;
				
				foreach ($compinfo["elementos"] as $key => $ci) {
					$campana = $ci["campana"];
					if(trim($campana) == "(not set)"){
						$campana = $ci["tipo"];
					}
					$campana_camp = $ci_camp["campana"];
					if(trim($campana_camp) == "(not set)"){
						$campana_camp = $ci_camp["tipo"];
					}
					if(trim($campana)==trim($campana_camp)){
						if($contenido_fecha != $ci["mes"].$ci["anno"]){
							if($contenido_sw == 0){
								
								if(trim($campana) == "(not set)"){
									$campana = $ci["tipo"];
								}

									$color_env = $colores[$colcamp];
								//}
								$contenido .= '<td class="NomCampana" style="border-left:solid 10px '.$color_env.';">';
								$colcamp++;
								$contenido .= $trans->__($campana, false);
								$contenido .= '</td>';
								$contenido_sw = 1;
							}
							
							if($contenido_clase == 'class="MesInpar" align="right"'){
								$contenido_clase = 'class="MesPar" align="right"';
							}else{
								$contenido_clase = 'class="MesInpar" align="right"';
							}

							$contenido .= '<td '.$contenido_clase.'>';
							if($ci[$metrica] != 0){
								if($ci[$metrica] < 0){
									$contenido .= "<span class='rojocomparar'>".number_format($ci[$metrica],2,',','.').sufijo_comp($metrica)."<span>";	
								}else{
									$contenido .= number_format($ci[$metrica],2,',','.').sufijo_comp($metrica);	
								}								
							}else{
								$contenido .= $ci[$metrica];
							}
							$totales[$totales_sw] += $ci[$metrica];
							$totales_sw++;
							
							$contenido .= '</td>';	
							$total += $ci[$metrica];
							$contenido_fecha = $ci["mes"].$ci["anno"];	

							

							if($ci["medio_fuente"] > $medio_fuente){
								$medio_fuente = $ci["medio_fuente"];
							}

						}
					}					
					
				}
				if($total == 0){
					$contenido .= '<td class="mediaCampanaTD" align="right">0</td>';
					$contenido .= '<td class="totalCampanaTD" align="right">0</td>';
				}else{
					$media = ($total/12);
					if($media < 0){
						$spanini = "<span class='rojocomparar'>";
						$spanfin = "</span>";
					}else{
						$spanini = "";
						$spanfin = "";
					}
					$contenido .= '<td class="mediaCampanaTD" align="right">'.$spanini.number_format($media,2,',','.').sufijo_comp($metrica).$spanfin.'</td>';
					if($total < 0){
						$spanini = "<span class='rojocomparar'>";
						$spanfin = "</span>";
					}else{
						$spanini = "";
						$spanfin = "";
					}
					$contenido .= '<td class="totalCampanaTD" align="right">'.$spanini.number_format($total,2,',','.').sufijo_comp($metrica).$spanfin.'</td>';
				}
				
				$contenido .= '</tr>';

				$tru = "";
				if (trim($ci_camp['campana']) != '(not set)') {
					if($medio_fuente>1){
						$tru = mostrarAgrupaciones($ci_camp['campana'],$metrica,$color_env,$grupo);
					}
				}else{
					if(trim($ci_camp['tipo']) != "directo"){
						$tru = mostrarAgrupaciones($ci_camp['tipo'],$metrica,$color_env,$grupo);
					}
				}

				if(!empty($tru)){
					$contenido .= '<script type="text/javascript">$(document).ready(function(){$(".tr_'. $grupo.'").addClass("tiene_agrupaciones");})</script>';
				}

				$contenido .= $tru;

				$grupo++;


			//}



		}

		
	}


	$tot_totales = 0;

	$cabeceras .= '<td class="mediaCampana">'.$trans->__('Media',false).'</td>
				   <td class="totalCampana">'.$trans->__('Total',false).'</td>';
	$cabeceras .= '</tr>';

	$filaChart = '<tbody>';
		$filaChart .= $cabeceras;
		$filaChart .= $contenido;

		//Totales
		$filaChart .= '<tr class="totalPorMeses"><td class="NomCampana" style="border-left:solid 10px #cdcdcd;">www.'.DOMINIO_LISCAMP.'</td>';
		foreach ($totales as $key => $tot) {
			$totc = 0;
			if($tot != 0){
				if($tot<0){
					$totc = "<span class='rojocomparar'>".number_format($tot,2,',','.').sufijo_comp($metrica)."</span>";
				}else{
					$totc = number_format($tot,2,',','.').sufijo_comp($metrica);
				}
				
			}
			$filaChart .= '<td class="MesInpar" align=right>'.$totc.'</td>';
			$tot_totales += $tot;
		}
		if ($tot_totales==0) {
			$filaChart .= '<td class="mediaCampanaTD" style="text-align:right" >0</td>';
			$filaChart .= '<td class="totalCampanaTD" style="text-align:right" >0</td>';
		}else{
			if(($tot_totales/12)<0){
				$spanini = "<span class='rojocomparar'>";
				$spanfin = "</span>";
			}else{
				$spanini = "";
				$spanfin = "";
			}
			$filaChart .= '<td class="mediaCampanaTD" style="text-align:right" >'.$spanini.number_format(($tot_totales/12),2,',','.').sufijo_comp($metrica).$spanfin.'</td>';
			if($tot_totales<0){
				$spanini = "<span class='rojocomparar'>";
				$spanfin = "</span>";
			}else{
				$spanini = "";
				$spanfin = "";
			}
			$filaChart .= '<td class="totalCampanaTD" style="text-align:right" >'.$spanini.number_format($tot_totales,2,',','.').sufijo_comp($metrica).$spanfin.'</td>';
		}
		
		$filaChart .= '</tr>';

	$filaChart .= '</tbody>';

	
	return $filaChart;

}



?>

<br>

    <div id="BotonesOpciones">
    	<div class="cajamenucomp">
    		<h2><?php $trans->__('Información general'); ?></h2>

	        <a id="b8"><?php $trans->__('Número de sesiones'); ?>
		        <span class="masOpcionesNEW">
		            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info8">
		              
		            <div class="infoMas">
		                <?php $trans->__('Número de sesiones (visitas) procedentes de la campaña.'); ?>
		            </div>         
		        </span>
			</a>

			<?php
			/*********************/
			if($pageuser == "campanas"){

				?>
				<a id="b1" class="activo"><?php $trans->__('Número de conversiones'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info1">
			              
			            <div class="infoMas">
			                <?php $trans->__('Número de pedidos en los que ha participado la campaña.'); ?>
			            </div>         
			        </span>
				</a>
				<a id="b3"><?php $trans->__('Valor de las conversiones'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info3">
			              
			            <div class="infoMas">
			                <?php $trans->__('Valor de las conversiones en las que ha participado cada campaña.'); ?>
			            </div>         
			        </span>
				</a>
				<a id="b2" style="display:none" ><?php $trans->__('Gastos de la campaña'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info2">
			              
			            <div class="infoMas">
			                <?php $trans->__('Importe bruto medio de los pedidos en los que ha participado la campaña.'); ?>
			            </div>         
			        </span>
				</a>
				<?php

			}else{
				?>
				<a id="b1"><?php $trans->__('Número de pedidos'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info1">
			              
			            <div class="infoMas">
			                <?php $trans->__('Número de conversiones en los que ha participado la campaña'); ?>
			            </div>         
			        </span>
				</a>
				<a id="b3"><?php $trans->__('Importe bruto de los pedidos');?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info3">
			              
			            <div class="infoMas">
			                <?php $trans->__('Importe bruto de los pedidos en los que ha participado cada campaña.'); ?>
			            </div>         
			        </span>
				</a>
				<a id="b2"><?php $trans->__('Pedido medio'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info2">
			              
			            <div class="infoMas">
			                <?php $trans->__('Importe bruto medio de los pedidos en los que ha participado la campaña.'); ?>
			            </div>         
			        </span>
				</a>
				
				<?php
			}
			/*********************/
			?>
	        <a id="b5"><?php $trans->__('Gastos de la campaña'); ?>
			    <span class="masOpcionesNEW">
			        <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info5">
			          
			        <div class="infoMas">
			            <?php $trans->__('Inversión realizada en la campaña (gastos).'); ?>
			        </div>         
			    </span>
			</a>
			<!--Ocultar
	        <a id="b11" style="display:none">Número de pedidos (Ponderado)
		        <span class="masOpcionesNEW">
		            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info11">
		              
		            <div class="infoMas">
		                Número de pedidos ponderado según la contribución de cada campaña.
		            </div>         
		        </span>
			</a>-->

			<!--Ocultar-->
	        <!--<a id="b4" style="display:none">Importe bruto de los pedidos (Ponderado)
		        <span class="masOpcionesNEW">
		            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info4">
		              
		            <div class="infoMas">
		                Importe bruto de los pedidos ponderado según la contribución de cada campaña.
		            </div>         
		        </span>
			</a>-->
		</div>

		<!--Ocultar-->
        <!--<a id="b7" style="display:none">Número de clientes nuevos (Ponderado)
	        <span class="masOpcionesNEW">
	            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info7">
	              
	            <div class="infoMas">
	                Número de nuevos clientes obtenidos gracias a la campaña. Este valor se calcula de forma ponderaa según la contribución realizada por la campaña.
	            </div>         
	        </span>
		</a>-->
		<div class="cajamenucomp">
			<h2><?php $trans->__('Tasa de conversión'); ?></h2>

			<?php
			/*********************/
			if($pageuser == "campanas"){
			?>
			<a id="b10"><?php $trans->__('Tasa de conversión'); ?>
		        <span class="masOpcionesNEW">
		            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info10">
		              
		            <div class="infoMas">
		                <?php $trans->__('La tasa de conversión de la campaña indica el porcentaje de pedidos realizados sobre las visitas desde la campaña.'); ?>
		            </div>         
		        </span>
			</a>
			<a id="b101"><?php $trans->__('Sesiones para conseguir una conversión'); ?>
		        <span class="masOpcionesNEW">
		            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info101">
		              
		            <div class="infoMas">
		                <?php $trans->__('Este dato indica el número de vistas (sesiones) que son necesarias para conseguir una conversión.'); ?>
		            </div>         
		        </span>
			</a>
			<?php
			}else{
			?>
				<a id="b10"><?php $trans->__('Tasa de conversión'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info10">
			              
			            <div class="infoMas">
			                <?php $trans->__('La tasa de conversión de la campaña indica el porcentaje de pedidos realizados sobre las visitas desde la campaña.'); ?>
			            </div>         
			        </span>
				</a>
				<a id="b101"><?php $trans->__('Sesiones para conseguir una venta'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info101">
			              
			            <div class="infoMas">
			                <?php $trans->__('Este dato indica el número de vistas (sesiones) que son necesarias para conseguir un pedido.'); ?>
			            </div>         
			        </span>
				</a>
			<?php
			}
			/*********************/
			?>
			
			<!--Ocultar-->
	        <!--<a id="b9" style="display:none">Coste de adquisición de un cliente nuevo (Ponderado)
		        <span class="masOpcionesNEW">
		            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info9">
		              
		            <div class="infoMas">
		                Coste de obtener un nuevo cliente. Se calcula mediante la fórmula: Gastos de la campaña / Número ponderado de nuevos clientes.
		            </div>         
		        </span>
			</a>-->
			
		</div>

		<div class="cajamenucomp">
			<h2><?php $trans->__('Rendimiento'); ?></h2>

			<?php
			/*********************/
			if($pageuser == "campanas"){
				?>
				<a id="b6" ><?php $trans->__('Rendimiento de la campaña'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info6">
			              
			            <div class="infoMas">
							<?php $trans->__('Rendimiento obtenido por la campaña.  Para calcularlo restamos al valor de las conversiones conseguidas los gastos de la campaña (Valor conversiones  - Gastos de la campaña).'); ?>			            
						</div>         
			        </span>
				</a>
				<a id="b91"><?php $trans->__('Retorno de la inversión en publicidad (ROAS)'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info91">
			              
			            <div class="infoMas">
			                <?php $trans->__('Porcentaje del rendimiento del dinero invertido en una campaña. Un porcentaje del 100% equivale a decir que hemos ganado tanto como hemos invertido (hemos obtenido dos veces el valor de lo invertido). Un porcentaje menor que 100 indica que hemos gastado más que el valor obtenido.'); ?>
			            </div>         
			        </span>
				</a>
		        <a id="b911"><?php $trans->__('Coste de adquisición de una conversión (CPA)'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info911">
			              
			            <div class="infoMas">
							<?php $trans->__('Coste de obtener una conversión. Se calcula mediante la fórmula: Costes de la campaña / Número de conversiones de la campaña'); ?>			            
						</div>         
			        </span>
				</a>
				<?php

			}else{
				?>
				<a id="b6" class="activo"><?php $trans->__('Rendimiento de la campaña'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info6">
			              
			            <div class="infoMas">
			                <?php $trans->__('Rendimiento obtenido por la campaña. Para calcularlo hallamos el importe neto de los pedidos y le restamos los gastos de la campaña (Importe bruto * Margen mensual / 100) - Gastos de la campaña'); ?>
			            </div>         
			        </span>
				</a>
				<a id="b911"><?php $trans->__('Retorno de la inversión en publicidad (ROAS)'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info911">
			              
			            <div class="infoMas">
							<?php $trans->__('Porcentaje del rendimiento del dinero invertido en una campaña. Lo calculamos sobre el la cantidad neta de los pedidos.'); ?>
							<?php $trans->__('Un porcentaje del 100% equivale a decir que hemos ganado tanto como hemos invertido (hemos vendido dos veces lo invertido). Un porcentaje menor que 100 indica que hemos gastado más que lo que hemos obtenido de margen.'); ?>
			            </div>         
			        </span>
				</a>
				<a id="b91"><?php $trans->__('Coste de adquisición de un pedido (CPA)'); ?>
			        <span class="masOpcionesNEW">
			            <img class="imgInfo" src="../public/images/info.png" alt="Información" rel="info91">
			              
			            <div class="infoMas">
			                <?php $trans->__('Coste de obtener un pedido. Se calcula mediante la fórmula: Costes de la campaña / Número de pedidos de la campaña.'); ?>
			            </div>         
			        </span>
				</a>
				<?php
			}
			/*********************/
			?>


		</div>
    </div>


    <div id="GraficosDatosGeneral">
        	<div id="chart_div" style="height: 350px; margin-left: -35px;"></div>
        <div id="table_div2">

        	<div class="capa_combos" style="width:215px">
				<select name="test" id="combo_agrupar" class="selectboxit visible" data-allow-clear="false"  data-placeholder="<?=$agrupacion?>">
					<option value="CAMPANA" <?php if($agrupacion == 'CAMPANA'){ echo 'selected'; } ?> ><?php $trans->__('Agrupar por campañas'); ?></option>
					<option value="MEDIO" <?php if($agrupacion == 'MEDIO'){ echo 'selected'; } ?> ><?php $trans->__('Agrupar por medio'); ?></option>
				</select>
				
			</div>
<script type="text/javascript">
$(document).ready(function(){

	$("#combo_agrupar").change(function(){
		var valor = $(this).val();
		$("#combo_agrupar").val(valor);

		self.location = "../public/campanas/campanas_comparativa.php?agrupacion="+valor;
		
		
					
	});

	

})//ready
</script>

   			<?php
   			if($pageuser == "campanas"){
            	?>
				<div class="graficoTablas MuestroTabla" id="grafico1">
	   				<strong class="TitTabla"><?php $trans->__('Número de conversiones'); ?></strong>
	   				<table class="graficoTabla graficoTabla1" border="0" cellspacing="0">
						<?=mostrarTabla('conversiones');?>
					</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico3">
	   				<strong class="TitTabla"><?php $trans->__('Valor de las conversiones'); ?></strong>
	   				<table class="graficoTabla graficoTabla3" border="0" cellspacing="0">
	   					<?=mostrarTabla('conversiones_euros');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico101">
	   				<strong class="TitTabla"><?php $trans->__('Sesiones para conseguir una conversion'); ?></strong>
	   				<table class="graficoTabla graficoTabla101" border="0" cellspacing="0">
	   					<?=mostrarTabla('sesiones_unaconversion');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico6">
	   				<strong class="TitTabla"><?php $trans->__('Rendimiento de la campaña'); ?></strong>
	   				<table class="graficoTabla graficoTabla6" border="0" cellspacing="0">
	   					<?=mostrarTabla('rendimiento_conversion');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico91">
	   				<strong class="TitTabla"><?php $trans->__('Retorno de la inversión en publicidad (ROAS)'); ?></strong>
	   				<table class="graficoTabla graficoTabla91" border="0" cellspacing="0">
	   					<?=mostrarTabla('retorno_conversion');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico911">
	   				<strong class="TitTabla"><?php $trans->__('Coste de adquisición de una conversión (CPA)'); ?></strong>
	   				<table class="graficoTabla graficoTabla911" border="0" cellspacing="0">
	   					<?=mostrarTabla('coste_conversion');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico10">
	   				<strong class="TitTabla"><?php $trans->__('Tasa de conversión'); ?></strong>
	   				<table class="graficoTabla graficoTabla10" border="0" cellspacing="0">
	   					<?=mostrarTabla('tasa_conversion');?>
	   				</table>
	   			</div>
            	<?php
            }else{
            	?>
				<div class="graficoTablas OcultoTabla" id="grafico1">
	   				<strong class="TitTabla"><?php $trans->__('Número de pedidos'); ?></strong>
	   				<table class="graficoTabla graficoTabla1" border="0" cellspacing="0">
						<?=mostrarTabla('transacciones');?>
					</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico3">
	   				<strong class="TitTabla"><?php $trans->__('Importe bruto de los pedidos'); ?></strong>
	   				<table class="graficoTabla graficoTabla3" border="0" cellspacing="0">
	   					<?=mostrarTabla('valor');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico101">
	   				<strong class="TitTabla"><?php $trans->__('Sesiones para conseguir una venta'); ?></strong>
	   				<table class="graficoTabla graficoTabla101" border="0" cellspacing="0">
	   					<?=mostrarTabla('sesiones_unpedido');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas MuestroTabla" id="grafico6">
	   				<strong class="TitTabla"><?php $trans->__('Rendimiento de la campaña'); ?></strong>
	   				<table class="graficoTabla graficoTabla6" border="0" cellspacing="0">
	   					<?=mostrarTabla('rendimiento');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico91">
	   				<strong class="TitTabla"><?php $trans->__('Coste de adquisición de un pedido (CPA)'); ?></strong>
	   				<table class="graficoTabla graficoTabla91" border="0" cellspacing="0">
	   					<?=mostrarTabla('adquisicion');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico911">
	   				<strong class="TitTabla"><?php $trans->__('Comisión por adquisición de un pedido %'); ?></strong>
	   				<table class="graficoTabla graficoTabla911" border="0" cellspacing="0">
	   					<?=mostrarTabla('comision');?>
	   				</table>
	   			</div>
	   			<div class="graficoTablas OcultoTabla" id="grafico10">
	   				<strong class="TitTabla"><?php $trans->__('Tasa de conversión'); ?></strong>
	   				<table class="graficoTabla graficoTabla10" border="0" cellspacing="0">
	   					<?=mostrarTabla('tasa_conversion');?>
	   				</table>
	   			</div>
            	<?php
            }
            ?>
   			
   			<!--<div class="graficoTablas OcultoTabla" id="grafico11">
   				<strong class="TitTabla">Número de pedidos (Ponderado)</strong>
   				<table class="graficoTabla graficoTabla11" border="0" cellspacing="0">

   				</table>
   			</div>-->
   			<div class="graficoTablas OcultoTabla" id="grafico2">
   				<strong class="TitTabla"><?php $trans->__('Pedido medio'); ?></strong>
   				<table class="graficoTabla graficoTabla2" border="0" cellspacing="0">
   					<?=mostrarTabla('pedido_medio');?>
   				</table>
   			</div>
   			
   			<!--<div class="graficoTablas OcultoTabla" id="grafico4">
   				<strong class="TitTabla">Importe bruto de los pedidos (Ponderado)</strong>
   				<table class="graficoTabla graficoTabla4" border="0" cellspacing="0">

   				</table>
   			</div>-->
   			<div class="graficoTablas OcultoTabla" id="grafico5">
   				<strong class="TitTabla"><?php $trans->__('Gastos de la campaña'); ?></strong>
   				<table class="graficoTabla graficoTabla5" border="0" cellspacing="0">
   					<?=mostrarTabla('gastos');?>
   				</table>
   			</div>
   			<!--<div class="graficoTablas OcultoTabla" id="grafico7">
   				<strong class="TitTabla">Número de clientes nuevos (Ponderado)</strong>
   				<table class="graficoTabla graficoTabla7" border="0" cellspacing="0">

   				</table>
   			</div>-->
   			<div class="graficoTablas OcultoTabla" id="grafico8">
   				<strong class="TitTabla"><?php $trans->__('Número de sesiones'); ?></strong>
   				<table class="graficoTabla graficoTabla8" border="0" cellspacing="0">
   					<?=mostrarTabla('sesiones');?>
   				</table>
   			</div>
   			<!--<div class="graficoTablas OcultoTabla" id="grafico9">
   				<strong class="TitTabla">Coste de adquisición de un cliente nuevo (Ponderado)</strong>
   				<table class="graficoTabla graficoTabla9" border="0" cellspacing="0">

   				</table>
   			</div>-->
   			

		</div>
        <div id="table_div"></div><br>


        <div class="botones_campanas">
        	
        	<?php


        	//Comprobamos si este cliente es administrador
        	if(IsAdmin($primera_vista,$_COOKIE["usuario"]["email"])){
        		?>
        		<button id="btn_anadir_campana" class="btn btn-primary" onclick="mantenimientoCampana();" type="button">
					<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
					<?php $trans->__('Mantenimiento de campañas'); ?>
				</button>
				<button id="btn_anadir_campana" class="btn btn-primary" onclick="mantenimientoMargenes();" type="button">
					<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
					<?php $trans->__('Mantenimiento de margenes mensuales'); ?>
				</button>
        		<?php
        	}else{
        		?>
        		<button id="btn_anadir_campana" class="btn btn-primary"  disabled="disabled" type="button" title="Solo administradores">
					<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
					<?php $trans->__('Mantenimiento de campañas'); ?>
				</button>
				<button id="btn_anadir_campana" class="btn btn-primary"  disabled="disabled" type="button" title="Solo administradores">
					<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
					<?php $trans->__('Mantenimiento de margenes mensuales'); ?>
				</button>
        		<?php
        	}
		
        ?>
	        
		</div><br>


		<div class="avisos_campanas">
			<?php
			$fechaFic = dataFechaFichero(VISTA_LISCAMP);
			
			if($fechaFic["cantidad"] != 0){

				if($fechaFic["cantidad"] == 1){
					foreach ($fechaFic["elementos"] as $key => $fF) {
						$mescf = traducirMes($fF["mes"]);
						$print = "<i class='fa fa-warning'></i> ".vsprintf($trans->__('El mes de %1$s del %2$s está desactualizado.', false), array($trans->__($mescf[$fF["mes"]]["nom"], false), $fF["anno"]));
					}
				}else{
					$print = "<i class='fa fa-warning'></i> ".$trans->__('Los meses',false).' ';
					foreach ($fechaFic["elementos"] as $key => $fF) {
						$mescf = traducirMes($fF["mes"]);
						$print .= $trans->__($mescf[$fF["mes"]]["nom"],false).",";
					}
					$printb = substr($printb, 0, -1);
					$print .= ' '.$trans->__("están desactualizados.",false);
				}

			}

			$fechaFic = dataMargenes(VISTA_LISCAMP);
			
			if($fechaFic["cantidad"] != 0){

				if($fechaFic["cantidad"] == 1){
					foreach ($fechaFic["elementos"] as $key => $fF) {
						$mescf = traducirMes($fF["mes"]);
						$printb = "<i class='fa fa-warning'></i> ".$trans->__('No se han detectado márgenes para el mes de',false)." ".$mescf[$fF["mes"]]["nom"]." del ".$fF["anno"].".";
					}
				}else{
					$printb = "<i class='fa fa-warning'></i> ".$trans->__('No se han detectado márgenes para los meses de',false);
					foreach ($fechaFic["elementos"] as $key => $fF) {
						$mescf = traducirMes($fF["mes"]);
						$printb .= " ".$trans->__($mescf[$fF["mes"]]["nom"], false).",";
					}
					$printb = substr($printb, 0, -1);
					$printb .= " .";
				}

			}
			?>
			<p class="avisos"><?=$print?></p>
	        <p class="avisos"><?=$printb?></p>
			
		</div>
    </div>




    
    <script id="jqueryui" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js" defer async></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <script type="text/javascript">

    	$(document).ready(function(){
    		$(".selectorclick").on("click",function(){
    			var grupo = $(this).attr("name");
    			if($(".grupo"+grupo).css("display")=="none"){
    				$(".grupo"+grupo).fadeIn(300);
    			}else{
    				$(".grupo"+grupo).hide();
    			}
    		})

    		
    	})

        google.load('visualization', '1.1', { packages: ['corechart','table'] });

        google.setOnLoadCallback(drawVisualization);
		var dataBakRelleno=false;
		var dataBak = []
        function drawVisualization() {
            var dataTitulo = []
            dataTitulo[5] = 'Rendimiento de la campaña';
            <?php
            if($pageuser == "campanas"){
            	echo "dataTitulo[0] = 'Número de conversiones';";
            	echo "dataTitulo[2] = 'Valor de las conversiones';";
            }else{
            	echo "dataTitulo[0] = 'Número de pedidos';";
            	echo "dataTitulo[2] = 'Importe bruto de los pedidos';";
            }
            ?>
            
            dataTitulo[10] = 'Número de pedidos (Ponderado)';
            dataTitulo[1] = 'Pedido medio';
            
            dataTitulo[3] = 'Importe bruto de los pedidos (Ponderado)';
            dataTitulo[4] = 'Gastos de la campaña';
            
            dataTitulo[6] = 'Número de clientes nuevos (Ponderado)';
            dataTitulo[7] = 'Número de visitas únicas';
            dataTitulo[8] = 'Coste de adquisición de un cliente nuevo (Ponderado)';
            dataTitulo[91] = 'Coste de adquisición de un pedido';
            dataTitulo[911] = 'Comisión por adquisición de un pedido (Ponderado) %';
            dataTitulo[9] = 'Tasa de conversión (Ponderada)';
            dataTitulo[101] = 'Visitas para conseguir una venta (Ponderada)';
            var datavAxis = []
            datavAxis[0] = 'Número';
            datavAxis[10] = 'Número';
            datavAxis[1] = 'Euros';
            datavAxis[2] = 'Euros';
            datavAxis[3] = 'Euros';
            datavAxis[4] = 'Euros';
            datavAxis[5] = 'Euros';
            datavAxis[6] = 'Clientes';
            datavAxis[7] = 'Sesiones';
            datavAxis[8] = 'Euros';
            datavAxis[91] = 'Euros';
            datavAxis[911] = 'Porcentaje';
            datavAxis[9] = 'Porcentaje';
            datavAxis[101] = 'Sesiones';
            // Some raw data (not necessarily accurate)
            var data = []
            
            //data[0] = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0,28,0,0,143,6,220],['ABR (14)',0,3,0,0,91,2,145],['MAY (14)',0,14,0,0,141,4,241],['JUN (14)',0,23,0,0,108,0,238],['JUL (14)',0,37,0,0,230,31,491],['AGO (14)',0,36,0,0,243,18,464],['SEP (14)',0,11,0,0,120,3,232],['OCT (14)',0,3,0,0,66,1,146],['NOV (14)',20,3,0,0,152,12,368],['DIC (14)',12,3,0,0,126,11,281],['ENE (15)',21,5,0,0,235,22,574],['FEB (15)',26,2,0,0,167,15,376],['MAR (15)',11,11,0,0,116,7,252],['ABR (15)',20,39,22,1,114,3,278],['MAY (15)',11,15,48,0,91,1,199]]);
            
			<?php
            if($pageuser == "campanas"){
            	$mostrarGrafico0 = mostrarGraficos('conversiones');
            	$mostrarGrafico1 = mostrarGraficos('conversiones_euros');
            	$mostrarGrafico2 = mostrarGraficos('sesiones_unaconversion');
            	$mostrarGrafico3 = mostrarGraficos('rendimiento_conversion');
            	$mostrarGrafico4 = mostrarGraficos('adquisicion');
            	$mostrarGrafico5 = mostrarGraficos('comision');
            }else{
            	$mostrarGrafico0 = mostrarGraficos('transacciones');
            	$mostrarGrafico1 = mostrarGraficos('valor');
            	$mostrarGrafico2 = mostrarGraficos('sesiones_unpedido');
            	$mostrarGrafico3 = mostrarGraficos('rendimiento');
            	$mostrarGrafico4 = mostrarGraficos('adquisicion');
            	$mostrarGrafico5 = mostrarGraficos('comision');
            }
            ?>
            data[0]   = google.visualization.arrayToDataTable([<?=$mostrarGrafico0?>]);
			data[1]   = google.visualization.arrayToDataTable([<?=mostrarGraficos('pedido_medio')?>]);
			data[2]   = google.visualization.arrayToDataTable([<?=$mostrarGrafico1?>]);
			data[4]   = google.visualization.arrayToDataTable([<?=mostrarGraficos('gastos')?>]);
			data[5]   = google.visualization.arrayToDataTable([<?=$mostrarGrafico3?>]);
			data[7]   = google.visualization.arrayToDataTable([<?=mostrarGraficos('sesiones')?>]);

			data[91]  = google.visualization.arrayToDataTable([<?=$mostrarGrafico4?>]);
			data[911] = google.visualization.arrayToDataTable([<?=$mostrarGrafico5?>]);
			data[9]  = google.visualization.arrayToDataTable([<?=mostrarGraficos('tasa_conversion')?>]);
			data[101] = google.visualization.arrayToDataTable([<?=$mostrarGrafico2?>]);

            data[10]  = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0,21.85,0,0,96,2.4,220],['ABR (14)',0,2.1,0,0,64.99,2,145],['MAY (14)',0,9.85,0,0,91.98,3,241],['JUN (14)',0,21.5,0,0,73.1,0,238],['JUL (14)',0,31.1,0,0,151.43,25.1,491],['AGO (14)',0,29.99,0,0,158.52,12.9,464],['SEP (14)',0,7.2,0,0,83,1.9,232],['OCT (14)',0,2,0,0,49.7,0.3,146],['NOV (14)',11.73,0.7,0,0,98.89,11.3,368],['DIC (14)',3.1,2.4,0,0,79.93,8.8,281],['ENE (15)',7.13,3.9,0,0,158.12,13.86,574],['FEB (15)',9.95,2,0,0,112.15,11.7,376],['MAR (15)',4,9.6,0,0,82.45,5.8,252],['ABR (15)',7.6,35.3,16.6,1,67.1,2.1,278],['MAY (15)',5.35,13.3,36.6,0,57.85,0.6,199]]);
            //data[1]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,87.7,0.00001,0.00001,83.58,64.73,85.74],['ABR (14)',0.00001,113.63,0.00001,0.00001,95.27,142.5,96.97],['MAY (14)',0.00001,96.48,0.00001,0.00001,87.07,132.48,95.02],['JUN (14)',0.00001,95.95,0.00001,0.00001,87.71,0.00001,90.29],['JUL (14)',0.00001,76.79,0.00001,0.00001,74.3,71.55,73.94],['AGO (14)',0.00001,72.54,0.00001,0.00001,70.27,92.78,69.62],['SEP (14)',0.00001,82.98,0.00001,0.00001,76.12,82,78.35],['OCT (14)',0.00001,86.67,0.00001,0.00001,111.74,59.95,110.27],['NOV (14)',106.21,111,0.00001,0.00001,115.25,117.91,112.75],['DIC (14)',108.45,99.82,0.00001,0.00001,109.98,134.25,114.12],['ENE (15)',95.91,86.97,0.00001,0.00001,102.32,103.3,99.27],['FEB (15)',99.92,90.5,0.00001,0.00001,87.91,76.31,89.82],['MAR (15)',78.42,110.22,0.00001,0.00001,85,66.29,87.2],['ABR (15)',87.4,87.07,92.41,26,88.51,100.3,90.07],['MAY (15)',93.2,90.69,89.28,0.00001,92.9,65,93.8]]);
            //data[2]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0,2455.67,0,0,11952.31,388.38,18863.38],['ABR (14)',0,340.9,0,0,8669.77,285,14060.42],['MAY (14)',0,1350.78,0,0,12277.3,529.9,22899.8],['JUN (14)',0,2206.75,0,0,9472.84,0,21490.19],['JUL (14)',0,2841.06,0,0,17089.77,2218,36304.34],['AGO (14)',0,2611.55,0,0,17076.13,1670,32301.38],['SEP (14)',0,912.75,0,0,9134.99,246,18177.23],['OCT (14)',0,260,0,0,7375.11,59.95,16099.88],['NOV (14)',2124.19,333,0,0,17517.46,1414.89,41490.32],['DIC (14)',1301.41,299.46,0,0,13857.04,1476.79,32068.06],['ENE (15)',2014.14,434.84,0,0,24044.93,2272.58,56982.52],['FEB (15)',2597.83,181,0,0,14681.17,1144.62,33770.85],['MAR (15)',862.64,1212.42,0,0,9860.27,464,21974.1],['ABR (15)',1747.9,3395.76,2032.93,26,10090.15,300.9,25039.66],['MAY (15)',1025.24,1360.32,4285.23,0,8453.99,65,18666.86]]);
            data[3]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,1888.02,0.00001,0.00001,8159.59,112.99,18863.38],['ABR (14)',0.00001,250.94,0.00001,0.00001,6290.07,285,14060.42],['MAY (14)',0.00001,975.32,0.00001,0.00001,8201.28,419.9,22899.8],['JUN (14)',0.00001,2014.48,0.00001,0.00001,6311.32,0.00001,21490.19],['JUL (14)',0.00001,2377.71,0.00001,0.00001,11248.6,1832.6,36304.34],['AGO (14)',0.00001,2146.28,0.00001,0.00001,11107.3,1283.7,32301.38],['SEP (14)',0.00001,666.04,0.00001,0.00001,6161.92,180.7,18177.23],['OCT (14)',0.00001,140,0.00001,0.00001,5475.25,17.98,16099.88],['NOV (14)',1136.33,65.8,0.00001,0.00001,11345.5,1325.99,41490.32],['DIC (14)',294.57,238.09,0.00001,0.00001,8615.67,1156.56,32068.06],['ENE (15)',736.72,356.04,0.00001,0.00001,15888.2,1376.77,56982.52],['FEB (15)',996.13,181,0.00001,0.00001,9927.11,899.63,33770.85],['MAR (15)',295.47,1085.72,0.00001,0.00001,6722.77,407.1,21974.1],['ABR (15)',639,3076.85,1453.15,26,5798.6,224.49,25039.66],['MAY (15)',458.96,1231,3303.05,0.00001,4975.09,39,18666.86]]);
            //data[4]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['ABR (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['MAY (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['JUN (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['JUL (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['AGO (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['SEP (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['OCT (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['NOV (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['DIC (14)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['ENE (15)',0.00001,141.7,0.00001,0.00001,0.00001,0.00001,1911.88],['FEB (15)',0.00001,121.3,0.00001,0.00001,0.00001,0.00001,1581.34],['MAR (15)',0.00001,212,0.00001,0.00001,0.00001,0.00001,212],['ABR (15)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001],['MAY (15)',0.00001,0.00001,0.00001,0.00001,0.00001,0.00001,0.00001]]);
            //data[5]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0,811.85,0,0,3508.62,48.59,8111.25],['ABR (14)',0,107.9,0,0,2704.73,122.55,6045.98],['MAY (14)',0,419.39,0,0,3526.55,180.56,9846.91],['JUN (14)',0,866.23,0,0,2713.87,0,9240.78],['JUL (14)',0,1022.42,0,0,4836.9,788.02,15610.87],['AGO (14)',0,922.9,0,0,4776.14,551.99,13889.59],['SEP (14)',0,286.4,0,0,2649.63,77.7,7816.21],['OCT (14)',0,60.2,0,0,2354.36,7.73,6922.95],['NOV (14)',488.62,28.29,0,0,4878.56,570.18,17840.84],['DIC (14)',126.67,102.38,0,0,3704.74,497.32,13789.27],['ENE (15)',316.79,11.4,0,0,6831.93,592.01,22590.6],['FEB (15)',428.33,-43.47,0,0,4268.66,386.84,12940.13],['MAR (15)',127.05,254.86,0,0,2890.79,175.05,9236.86],['ABR (15)',274.77,1323.05,624.85,11.18,2493.4,96.53,10767.05],['MAY (15)',197.35,529.33,1420.31,0,2139.29,16.77,8026.75]]);
            data[6]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,19.55,0.00001,0.00001,87.8,0,192],['ABR (14)',0.00001,2.1,0.00001,0.00001,61.93,0.00001,133],['MAY (14)',0.00001,8.85,0.00001,0.00001,82.78,0.00001,211],['JUN (14)',0.00001,18.5,0.00001,0.00001,65.3,0.00001,209],['JUL (14)',0.00001,30.2,0.00001,0.00001,142.8,5,417],['AGO (14)',0.00001,28.59,0.00001,0.00001,142.92,3.94,398],['SEP (14)',0.00001,7.2,0.00001,0.00001,69,0.00001,186],['OCT (14)',0.00001,2,0.00001,0.00001,46,0.00001,130],['NOV (14)',9.33,0.7,0.00001,0.00001,91.99,2.9,319],['DIC (14)',2.8,1.7,0.00001,0.00001,74.03,1,250],['ENE (15)',6.4,3.9,0.00001,0.00001,148.99,2.7,515],['FEB (15)',6.55,2,0.00001,0.00001,99.15,1,321],['MAR (15)',3.1,8.6,0.00001,0.00001,70.05,2.3,210],['ABR (15)',7.6,34.9,16.6,1,57.6,0.00001,234],['MAY (15)',5.35,13.3,35.2,0.00001,51.75,0.00001,177]]);
            //data[7]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,1673,0.00001,0.00001,21859,37,38490],['ABR (14)',0.00001,523,0.00001,0.00001,22208,512,40506],['MAY (14)',0.00001,2257,0.00001,0.00001,25800,45,48463],['JUN (14)',0.00001,2279,0.00001,0.00001,18506,11,41017],['JUL (14)',0.00001,3034,0.00001,0.00001,22199,1996,51291],['AGO (14)',0.00001,2429,0.00001,0.00001,18400,954,45752],['SEP (14)',0.00001,1405,0.00001,0.00001,24138,35,50443],['OCT (14)',0.00001,538,0.00001,0.00001,25591,8,48854],['NOV (14)',3234,399,0.00001,0.00001,24363,718,48775],['DIC (14)',2926,639,0.00001,0.00001,20106,688,39703],['ENE (15)',4486,988,0.00001,0.00001,27430,1467,66288],['FEB (15)',3376,883,0.00001,0.00001,19675,894,47389],['MAR (15)',3070,1613,0.00001,0.00001,19622,517,49046],['ABR (15)',3288,3921,1088,2,19852,358,51089],['MAY (15)',2455,2071,2952,5,15529,31,35384]]);
            data[8]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,0,0.00001,0.00001,0,0.00001,0],['ABR (14)',0.00001,0,0.00001,0.00001,0,0.00001,0],['MAY (14)',0.00001,0,0.00001,0.00001,0,0.00001,0],['JUN (14)',0.00001,0,0.00001,0.00001,0,0.00001,0],['JUL (14)',0.00001,0,0.00001,0.00001,0,0,0],['AGO (14)',0.00001,0,0.00001,0.00001,0,0,0],['SEP (14)',0.00001,0,0.00001,0.00001,0,0.00001,0],['OCT (14)',0.00001,0,0.00001,0.00001,0,0.00001,0],['NOV (14)',0,0,0.00001,0.00001,0,0,0],['DIC (14)',0,0,0.00001,0.00001,0,0,0],['ENE (15)',0,36.33,0.00001,0.00001,0,0,3.71],['FEB (15)',0,60.65,0.00001,0.00001,0,0,4.93],['MAR (15)',0,24.65,0.00001,0.00001,0,0,1.01],['ABR (15)',0,0,0,0,0,0.00001,0],['MAY (15)',0,0,0,0.00001,0,0.00001,0]]);
            //data[91]  = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,0,0.00001,0.00001,0,0,0],['ABR (14)',0.00001,0,0.00001,0.00001,0,0,0],['MAY (14)',0.00001,0,0.00001,0.00001,0,0,0],['JUN (14)',0.00001,0,0.00001,0.00001,0,0.00001,0],['JUL (14)',0.00001,0,0.00001,0.00001,0,0,0],['AGO (14)',0.00001,0,0.00001,0.00001,0,0,0],['SEP (14)',0.00001,0,0.00001,0.00001,0,0,0],['OCT (14)',0.00001,0,0.00001,0.00001,0,0,0],['NOV (14)',0,0,0.00001,0.00001,0,0,0],['DIC (14)',0,0,0.00001,0.00001,0,0,0],['ENE (15)',0,36.33,0.00001,0.00001,0,0,3.33],['FEB (15)',0,60.65,0.00001,0.00001,0,0,4.21],['MAR (15)',0,22.08,0.00001,0.00001,0,0,0.84],['ABR (15)',0,0,0,0,0,0,0],['MAY (15)',0,0,0,0.00001,0,0,0]]);
            //data[911] = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,0,0.00001,0.00001,0,0,0],['ABR (14)',0.00001,0,0.00001,0.00001,0,0,0],['MAY (14)',0.00001,0,0.00001,0.00001,0,0,0],['JUN (14)',0.00001,0,0.00001,0.00001,0,0.00001,0],['JUL (14)',0.00001,0,0.00001,0.00001,0,0,0],['AGO (14)',0.00001,0,0.00001,0.00001,0,0,0],['SEP (14)',0.00001,0,0.00001,0.00001,0,0,0],['OCT (14)',0.00001,0,0.00001,0.00001,0,0,0],['NOV (14)',0,0,0.00001,0.00001,0,0,0],['DIC (14)',0,0,0.00001,0.00001,0,0,0],['ENE (15)',0,39.8,0.00001,0.00001,0,0,3.36],['FEB (15)',0,67.02,0.00001,0.00001,0,0,4.68],['MAR (15)',0,19.53,0.00001,0.00001,0,0,0.96],['ABR (15)',0,0,0,0,0,0,0],['MAY (15)',0,0,0,0.00001,0,0,0]]);
            //data[9]   = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,1.31,0.00001,0.00001,0.44,6.49,0.57],['ABR (14)',0.00001,0.4,0.00001,0.00001,0.29,0.39,0.36],['MAY (14)',0.00001,0.44,0.00001,0.00001,0.36,6.67,0.5],['JUN (14)',0.00001,0.94,0.00001,0.00001,0.4,0,0.58],['JUL (14)',0.00001,1.03,0.00001,0.00001,0.68,1.26,0.96],['AGO (14)',0.00001,1.23,0.00001,0.00001,0.86,1.35,1.01],['SEP (14)',0.00001,0.51,0.00001,0.00001,0.34,5.43,0.46],['OCT (14)',0.00001,0.37,0.00001,0.00001,0.19,3.75,0.3],['NOV (14)',0.36,0.18,0.00001,0.00001,0.41,1.57,0.75],['DIC (14)',0.11,0.38,0.00001,0.00001,0.4,1.28,0.71],['ENE (15)',0.16,0.39,0.00001,0.00001,0.58,0.94,0.87],['FEB (15)',0.29,0.23,0.00001,0.00001,0.57,1.31,0.79],['MAR (15)',0.13,0.6,0.00001,0.00001,0.42,1.12,0.51],['ABR (15)',0.23,0.9,1.53,50,0.34,0.59,0.54],['MAY (15)',0.22,0.64,1.24,0,0.37,1.94,0.56]]);
            //data[101] = google.visualization.arrayToDataTable([['Mes','Remarketing','stylight','gShoppingAdwords2015','gShopping2015','SEO','Todos los boletines','www.kukimba.com'],['MAR (14)',0.00001,76.57,0.00001,0.00001,227.7,15.42,174.95],['ABR (14)',0.00001,249.05,0.00001,0.00001,341.71,256,279.35],['MAY (14)',0.00001,229.14,0.00001,0.00001,280.5,15,201.09],['JUN (14)',0.00001,106,0.00001,0.00001,253.16,0.00001,172.34],['JUL (14)',0.00001,97.56,0.00001,0.00001,146.6,79.52,104.46],['AGO (14)',0.00001,80.99,0.00001,0.00001,116.07,73.95,98.6],['SEP (14)',0.00001,195.14,0.00001,0.00001,290.82,18.42,217.43],['OCT (14)',0.00001,269,0.00001,0.00001,514.91,26.67,334.62],['NOV (14)',275.7,570,0.00001,0.00001,246.36,63.54,132.54],['DIC (14)',943.87,266.25,0.00001,0.00001,251.55,78.18,141.29],['ENE (15)',629.17,253.33,0.00001,0.00001,173.48,105.84,115.48],['FEB (15)',339.3,441.5,0.00001,0.00001,175.43,76.41,126.03],['MAR (15)',767.5,168.02,0.00001,0.00001,237.99,89.14,194.63],['ABR (15)',432.63,111.08,65.54,2,295.86,170.48,183.77],['MAY (15)',458.88,155.71,80.66,0.00001,268.44,51.67,177.81]]);

            <?php
            if($pageuser == "campanas"){
            	?>var current = 0;<?php
            }else{
            	?>var current = 5;<?php
            }
            ?>
            
            
            // Create and draw the visualization.
            var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
            var table = new google.visualization.Table(document.getElementById('table_div'));
            var button1 = document.getElementById('b1');
            //var button11 = document.getElementById('b11');
            var button2 = document.getElementById('b2');
            var button3 = document.getElementById('b3');
            //var button4 = document.getElementById('b4');
            var button5 = document.getElementById('b5');
            var button6 = document.getElementById('b6');
            //var button7 = document.getElementById('b7');
            var button8 = document.getElementById('b8');
            //var button9 = document.getElementById('b9');
            var button91 = document.getElementById('b91');
            var button911 = document.getElementById('b911');
            var button10 = document.getElementById('b10');
            var button101 = document.getElementById('b101');
			var CapaDatosTabla = document.getElementsByClassName('graficoTablas');

			//QuitoClases();
			qTablaMuestro(current+1);
			//button6.className = button1.className + " activo";
			//var capaGeneralA = document.getElementById('BotonesOpciones');

            function drawChart() {

                var options = {
                    //title: dataTitulo[current],
                    vAxis: { title: trans.__(datavAxis[current]) },
                    hAxis: { _title: "Mes", textStyle: {fontSize:12} },
                    legend: { textStyle: {fontSize: 12 }},
                    seriesType: "line",
                    colors: [
                    			<?php
                    			$colores_camp = colores_campanas();
                    			foreach ($colores_camp as $key => $clc) {
                    				$filaColor .=  '"'.$clc.'",';
                    			}
                    			$filaColor = substr($filaColor, 0, -1);
                    			echo $filaColor;
                    			?>
                    		],
                   
                    
                        series: { <?=mostrarCantidad();?>: { type: "steppedArea", color: '#cdcdcd', } },
                    
                    animation: {
                        duration: 1000,
                        easing: 'out'
                    },
                };

                chart.draw(data[current], options);
                

            }
            drawChart();

			function QuitoClases() {
				button1.className = "";
                //button11.className = "";
				button2.className = "";
				button3.className = "";
				//button4.className = "";
				button5.className = "";
				button6.className = "";
				//button7.className = "";
				button8.className = "";
				//button9.className = "";
                button91.className = "";
                button911.className = "";
				button10.className = "";
                button101.className = "";
            }

            button1.onclick = function() {
                current = 0;
				QuitoClases();
				qTablaMuestro(current+1);
				button1.className = button1.className + " activo";
                drawChart();
            }
            /*button11.onclick = function() {
                current = 10;
				QuitoClases();
				qTablaMuestro(current+1);
				button11.className = button11.className + " activo";
                drawChart();
            }*/
            button2.onclick = function() {
                current = 1
				QuitoClases();
				qTablaMuestro(current+1);
				button2.className = button1.className + " activo";
                drawChart();
            }
            button3.onclick = function() {
                current = 2
				QuitoClases();
				qTablaMuestro(current+1);
				button3.className = button1.className + " activo";
                drawChart();
            }
            /*button4.onclick = function() {
                current = 3
				QuitoClases();
				qTablaMuestro(current+1);
				button4.className = button1.className + " activo";
                drawChart();
            }*/
            button5.onclick = function() {
                current = 4
				QuitoClases();
				qTablaMuestro(current+1);
				button5.className = button1.className + " activo";
                drawChart();
            }
            button6.onclick = function() {
                current = 5
				QuitoClases();
				qTablaMuestro(current+1);
				button6.className = button1.className + " activo";
                drawChart();
            }
            /*button7.onclick = function() {
                current = 6
				QuitoClases();
				qTablaMuestro(current+1);
				button7.className = button1.className + " activo";
                drawChart();
            }*/
            button8.onclick = function() {
                current = 7
				QuitoClases();
				qTablaMuestro(current+1);
				button8.className = button1.className + " activo";
                drawChart();
            }
            /*button9.onclick = function() {
                current = 8
				QuitoClases();
				qTablaMuestro(current+1);
				button9.className = button9.className + " activo";
                drawChart();
            }*/
            button91.onclick = function() {
                current = 91
				QuitoClases();
				qTablaMuestro(current);
				button91.className = button91.className + " activo";
                drawChart();
            }
            button911.onclick = function() {
                current = 911
				QuitoClases();
				qTablaMuestro(current);
				button911.className = button911.className + " activo";
                drawChart();
            }
            button10.onclick = function() {
                current = 9
				QuitoClases();
				qTablaMuestro(current+1);
				button10.className = button10.className + " activo";
                drawChart();
            }
            button101.onclick = function() {
                current = 101
				QuitoClases();
				qTablaMuestro(current);
				button101.className = button101.className + " activo";
                drawChart();
            }
        }
    </script><script src="https://www.google.com/uds/?file=visualization&amp;v=1.1&amp;packages=corechart%2Ctable" type="text/javascript"></script><link href="https://www.google.com/uds/api/visualization/1.1/9543863e4f7c29aa0bc62c0051a89a8a/ui+es,table+es.css" type="text/css" rel="stylesheet"><script src="https://www.google.com/uds/api/visualization/1.1/9543863e4f7c29aa0bc62c0051a89a8a/webfontloader,format+es,default+es,ui+es,table+es,corechart+es.I.js" type="text/javascript"></script>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
	
    

		 




	<!--**************************************************************-->
	</div>


</div>

<script src="<?=RUTA_ABSOLUTA?>js/campanas_comparativa_scripts.js"></script>	
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_campanas_comparativa.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



﻿<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{
	//echo "Tiene usuario";
	//echo "tiene cookie";
	//SI TIENE COOKIE VAMOS A VALIDAR LA VISTA EN LA QUE ENTRA POR URL
	//if (isset($_GET["cat"])){
		
		//if(!UsuariosVistas($_COOKIE["usuario[email]"],$_GET["cat"],$com_emails)){
			//header("Location: ../informes/metricas_habituales.php");
		//}

	//}
	


	include("../includes/head.php");
	include("../includes/side_campanas.php"); 
?>

<div class="main-content">

<?php

include("../includes/breadcrumb.php")
?>
<br>

<div class="col-sm-12">
	<!--******************DATATABLE DE CAMPAÑAS***************************-->

		<?php
		if(isset($_COOKIE["userpage"])){
			$pageuser = $_COOKIE["userpage"];
		}else{
			$pageuser = "informes";
		}

		if($pageuser=="campanas"){
		?>
		<!--Tab campañas-->
		<div class="table_campanas">
			<table class="table table-bordered datatable tableajax" id="table_datatable_campanas">
				<thead>
					<tr>
						<th width="20%" class="primera"><span class="cab_tit" id="Nombre_Dimension_campanas"><?php $trans->__('Campaña') ;?></span></th>
						<th width="15%" class="cab_bb"><?php $trans->__('Medio') ;?></th>
						<th width="15%" class="cab_bb"><?php $trans->__('Fuente') ;?></th>
						<th width="10%"><?php $trans->__('Sesiones') ;?></th>
						<th width="10%"><?php $trans->__('Usuarios') ;?></th>
						<th width="10%"><?php $trans->__('Tas. rebote') ;?></th>
						<th width="10%"><?php $trans->__('Págs/ses') ;?></th>				
						<!--<th width="10%">Conversiones primera interacción</th>
						<th width="10%">Conversiones asistidas</th>
						<th width="10%">Conversiones última interacción</th>-->
						<th width="10%"><?php $trans->__('Total conversiones') ;?></th>
						<th width="10%"><?php $trans->__('Valor conversiones') ;?></th>
						<!--<th width="10%">Conversiones ga</th>-->
						<th width="1%"></th>
					</tr>	
				</thead>
				<tbody>
					<tr>
					<td colspan="14"><?php $trans->__('Cargando campañas...') ;?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<?php }else{ ?>
		<!--Tab campañas-->
		<div class="table_campanas">
			<table class="table table-bordered datatable tableajax" id="table_datatable_campanas_ecomerce">
				<thead>
					<tr>
						<th width="20%" class="primera"><span class="cab_tit" id="Nombre_Dimension_campanas"><?php $trans->__('Campaña') ;?></span></th>
						<th width="15%" class="cab_bb"><?php $trans->__('Medio') ;?></th>
						<th width="15%" class="cab_bb"><?php $trans->__('Fuente') ;?></th>
						<th width="10%"><?php $trans->__('Sesiones') ;?></th>
						<th width="10%"><?php $trans->__('Usuarios') ;?></th>
						<th width="10%"><?php $trans->__('Tas. rebote') ;?></th>
						<th width="10%"><?php $trans->__('Págs/ses') ;?></th>		
						<!--<th width="10%">Transacciones primera interacción</th>-->
						<!--<th width="10%">Transacciones asistidas</th>-->
						<!--<th width="10%">Transacciones última interacción</th>-->
						<th width="10%"><?php $trans->__('Total Transacciones') ;?></th>
						<th width="10%"><?php $trans->__('Valor Transacciones') ;?></th>
						<!--<th width="10%">Transacciones ga</th>-->
						<th width="1%"></th>
					</tr>	
				</thead>
				<tbody>
					<tr>
					<td colspan="14"><?php $trans->__('Cargando campañas...') ;?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<?php } ?>
		<button id="btn_anadir_campana" class="btn btn-primary" onclick="mantenimientoCampana('<?php echo $primera_propiedad; ?>');" type="button">
			<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
			<?php $trans->__('Mantenimiento de campañas') ;?>
		</button>
		
	</div>
	<!--**************************************************************-->
</div>


</div>
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_campanas_comparativa.css">
<?php if($pageuser=="campanas"){ ?>
<script src="<?=RUTA_ABSOLUTA?>js/campanas_resumen_scripts.js"></script>	
<?php }else{ ?>
<script src="<?=RUTA_ABSOLUTA?>js/campanas_resumen_ecomerce_scripts.js"></script>	
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



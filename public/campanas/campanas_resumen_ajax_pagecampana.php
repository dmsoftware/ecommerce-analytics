﻿<?php include("../includes/conf.php");
	
	if(isset($_POST["camp"])){

		$pagecampana ="";
		$filtro = "";
		if($_POST["camp"]!=""){
			$filtro .= 'ga:campaign**'.$_POST["camp"];
			$pagecampana .= $_POST["camp"];
		}

		
		if($_POST["medi"]!=""){
			if(!empty($filtro)){ $filtro .=";"; $pagecampana .=" | "; }
			$filtro .= 'ga:medium**'.$_POST["medi"];
			$pagecampana .= $_POST["medi"];
		}


		if($_POST["sour"]!=""){
			if(!empty($filtro)){ $filtro .=";"; $pagecampana .=" | "; }
			$filtro .= 'ga:source**'.$_POST["sour"];
			$pagecampana .= $_POST["sour"];
		}

		setcookie( "pagecampana", $pagecampana, time() + (86400), "/"); //86400 es un dia
		setcookie( "pagefiltro", $filtro, time() + (86400), "/"); //86400 es un dia

		

	}else if(isset($_POST["filt"])){

		switch ($_POST["filt"]) {
			case 'seo':
				$pagecampana = "SEO";
				$filtro 	 = "ga:medium**organic;ga:SocialNetwork==(not set)";
				break;
			case 'enlaces':
				$pagecampana = "Enlaces";
				$filtro 	 = "ga:medium**referral;ga:SocialNetwork**(not set)";
				break;
			case 'social':
				$pagecampana = "Social";
				$filtro 	 = "ga:SocialNetwork!=(not set);ga:campaign==(not set)";
				break;
			case 'total':
				$pagecampana = "Total";
				$filtro 	 = "";
				break;
			default:
				# code...
				break;
		}

		setcookie( "pagecampana", $pagecampana, time() + (86400), "/"); //86400 es un dia
		setcookie( "pagefiltro", $filtro, time() + (86400), "/"); //86400 es un dia
	
	}else{
		return false;
	}
	

?>


<?php include("includes/conf.php");


echo "<h1>Cargando p&aacute;gina...</h1>";

//Primero comprobamos que existan los parametros gets necesarios para funcionar

if(isset($_GET["campana"]) and isset($_GET["medio"]) and isset($_GET["fuente"]) and isset($_GET["fechaini"]) and isset($_GET["fechafin"]) and isset($_GET["fechaini_ant"]) and isset($_GET["fechafin_ant"]) and isset($_GET["idVista"])){

	//Primero comprobamos Si existe ya una sesion abierta si no existe guardamos la ruta en una sesión para luego mostrarla
	if(!isset($_COOKIE["usuario"])) {

		//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
		$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
		$_SESSION["rutair"] = $url;
		
		header('Location: http://dmintegra.acc.com.es/public/index.php');

	}else{

		//Tiene usuario
		if($_GET["campana"] != "seo" and $_GET["campana"] != "enlaces" and $_GET["campana"] != "social"){

			$pagecampana ="";
			$filtro = "";
			if($_GET["campana"]!=""){
				$filtro .= 'ga:campaign**'.$_GET["campana"];
				$pagecampana .= $_GET["campana"];
			}

			
			if($_GET["medio"]!=""){
				if(!empty($filtro)){ $filtro .=";"; $pagecampana .=" | "; }
				$filtro .= 'ga:medium**'.$_GET["medio"];
				$pagecampana .= $_GET["medio"];
			}


			if($_GET["fuente"]!=""){
				if(!empty($filtro)){ $filtro .=";"; $pagecampana .=" | "; }
				$filtro .= 'ga:source**'.$_GET["fuente"];
				$pagecampana .= $_GET["fuente"];
			}

		}else{

			switch ($_GET["campana"]) {
				case 'seo':
					$pagecampana = "SEO";
					$filtro 	 = "ga:medium**organic;ga:SocialNetwork==(not set)";
					break;
				case 'enlaces':
					$pagecampana = "Enlaces";
					$filtro 	 = "ga:medium**referral;ga:SocialNetwork**(not set)";
					break;
				case 'social':
					$pagecampana = "Social";
					$filtro 	 = "ga:SocialNetwork!=(not set);ga:campaign==(not set)";
					break;
				default:
					$pagecampana = "Total";
					$filtro 	 = "";
					break;
			}
		}

		//Eliminamos la sesion si existe de rutair
		if(isset($_SESSION["rutair"])){
			unset($_SESSION["rutair"]);
		}

		setcookie( "pagecampana", $pagecampana, time() + (86400), "/"); //86400 es un dia
		setcookie( "pagefiltro", $filtro, time() + (86400), "/"); //86400 es un dia

		//Vista
		setcookie( "vista", $_GET["idVista"], time() + (86400), "/"); //86400 es un dia
		
		//Fechas
		//formateados
		setcookie( "fechaIni_f", date("F j, Y", strtotime($_GET["fechaini"])), time() + (86400), "/"); //86400 es un dia
		setcookie( "fechaFin_f", date("F j, Y", strtotime($_GET["fechafin"])), time() + (86400), "/"); //86400 es un dia
		//no formateados
		setcookie( "fechaIni", $_GET["fechaini"], time() + (86400), "/"); //86400 es un dia
		setcookie( "fechaFin", $_GET["fechafin"], time() + (86400), "/"); //86400 es un dia

		//Pag camapanas
		setcookie( "userpage","campanas", time() + (86400), "/");
		
		header("Location: http://dmintegra.acc.com.es/public/informes/metricas_habituales.php");

	}

}else{

	echo "<h1>Error de carga de la campaña</h1>";
	header("Location: http://dmintegra.acc.com.es/");

}

?>
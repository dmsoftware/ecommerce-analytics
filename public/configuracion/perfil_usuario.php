<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	



	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
$titulo = "Perfil de usuario";
include("../includes/breadcrumb.php");
?>

<br />

<style type="text/css">
	.formulario_perfil{ max-width: 400px; padding: 30px; padding-top: 0px;}
	.msggood{ color: green; display: none}
</style>

<div class="row">

	<?php

		$userdata = UsuariosData($_COOKIE["usuario"]["email"]);

		$nombre_user    = $userdata["elementos"]["nombre"];
		$apellidos_user = $userdata["elementos"]["apellidos"];
		$sexo_user      = $userdata["elementos"]["sexo"];
		$idioma_user    = $userdata["elementos"]["idioma"];


		switch ($sexo_user) {
			case 'm':
				$select_m = "checked";
				$select_h = "";
				break;
			case 'h':
				$select_h = "checked";
				$select_m = "";
				break;
			default:
				$select_m = "";
				$select_h = "";
				break;
		}

		switch ($idioma_user) {
			case 'es':
				$ididoma_es = 'selected';
				$ididoma_en = '';
				$ididoma_eu = '';
				break;
			case 'eu':
				$ididoma_es = '';
				$ididoma_en = '';
				$ididoma_eu = 'selected';
				break;
			case 'en':
				$ididoma_es = '';
				$ididoma_en = 'selected';
				$ididoma_eu = '';
				break;
			default:
				$ididoma_es = '';
				$ididoma_en = '';
				$ididoma_eu = '';
				break;
		}
	?>

	<div class="formulario_perfil">
		<h2><?php $trans->__('Datos de perfil'); ?></h2> <br>
		<form>
		  	<div class="form-group">
		  	  <label for="txt_nombre"><?php $trans->__('Nombre'); ?>:</label>
		  	  <input type="text" class="form-control" id="txt_nombre" placeholder="" value="<?=$nombre_user?>">
		  	</div>
		  	<div class="form-group">
		  	  <label for="txt_apellidos"><?php $trans->__('Apellidos'); ?>:</label>
		  	  <input type="text" class="form-control" id="txt_apellidos" placeholder="" value="<?=$apellidos_user?>">
		  	</div>
		  	<div class="form-group">
		  		<label for="radio_sexo"><?php $trans->__('Sexo'); ?>:</label><br>
		  		<label class="radio-inline">
				  <input type="radio" name="radio_sexo" id="radio_hombre" value="h" <?=$select_h?> > <?php $trans->__('Hombre'); ?>
				</label>
				<label class="radio-inline">
				  <input type="radio" name="radio_sexo" id="radio_mujer" value="m" <?=$select_m?> > <?php $trans->__('Mujer'); ?>
				</label>
		  	</div>
		  	<div class="form-group">
		  		<label for="select_idioma"><?php $trans->__('Idioma'); ?>:</label>
			  	<select class="form-control" id="select_idioma">
				  <option value=""><?php $trans->__('Selecciona un idioma'); ?>...</option>
				  <option value="es" <?=$ididoma_es?> ><?php $trans->__('Español'); ?></option>
				  <option value="en" <?=$ididoma_en?> ><?php $trans->__('Inglés'); ?></option>
				  <option value="eu" <?=$ididoma_eu?> ><?php $trans->__('Euskera'); ?></option>
				</select>
			</div>
			<p class="msggood"><?php $trans->__('Datos guardados correctamente'); ?></p>
		 	<button type="button" class="btn btn-default" id="btn_guardarcambios"><?php $trans->__('Guardar cambios'); ?></button>
		</form>
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#btn_guardarcambios").on("click",function(){
			var nombre    = $("#txt_nombre").val();
			var apellidos = $("#txt_apellidos").val();
			var sexo      = $("input:radio[name=radio_sexo]:checked").val();
			var idioma    = $("#select_idioma").val();
			//Editamos los cambios
			$.ajax({
		        type: 'POST',
		        url: '../configuracion/perfil_usuario_ajax.php',
		        data: {
		          nom:  nombre,
		          apel: apellidos,
		          sex:  sexo,
		          idi:  idioma       
		          },
		        dataType: 'text',
		        success: function(data){

		        	location.reload();

		        	
		          
		          },
		        error: function(data){
		          
		        }
		      })
		})
	})
	</script>

</div>

<script src="<?=RUTA_ABSOLUTA?>js/sitioweb_scripts.js"></script>	
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



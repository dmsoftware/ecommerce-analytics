<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	



	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
$titulo = "Permisos";
include("../includes/breadcrumb.php")
?>

<br />

<style type="text/css">
	
	#tabla_permisos{ margin-top: 30px;}
	/*#tabla_permisos thead tr{ background-color: #3FABE2; }
	#tabla_permisos thead tr th{color: white;}*/
	.admins{ margin-top: 20px;}
	.admins span{ font-size: 17px; margin-left: 5px; }
	.clit{ float: left; margin-right: 10px;margin-top: 15px;font-size: 17px;}
	.inptex{ display: block; padding: 5px; width: 100%}
	.fila{ cursor: cell;}
	.btnelimuser{ display: none;}
	.btnedituser{ display: none;}
	.ttinfor{ width: 35px; margin: auto; display: block; }
	.titfo{ font-size: 14px;}
	.filafiltro{ list-style: none; overflow: hidden; display: block;}
	.filafiltro li.cajat{ display: block; width: 35%; float: left;padding-left: 3%;}
	.filafiltro li.cajab{ display: block; width: auto; float: left; padding-left: 3%;}
	.filafiltro li button{ display: block; margin: auto;}
	.filafiltro li input{ display: block;  padding: 6px; width: 100%;}
	.anauser{ margin-top: 25px; display: block; margin-bottom: 5px;}
	.filtrotit span{ display: block;  font-weight: bold; font-size: 18px;}
	.filtrotit span{ border-bottom: 1px solid gray;}
	.capa_anadirusuario{ width: 50%;min-width: 700px; display: none; padding: 20px; background-color: #f8f8f8; clear: both; overflow: auto; border: dotted 1px gray; }
	.capa_anadirusuario .caja_combo{ float: left; width: 36%; padding-right: 20px; background-image: url("../images/flechader.jpg"); background-position: -5px center; background-size: 13%; }
	.capa_anadirusuario .caja_input{ float: left; width: 36%; padding-right: 20px;}
	.capa_anadirusuario .caja_btn{ float: left;  width: 20%;}
	.capa_anadirusuario .caja_btn_c{ float: left; width: 8%;}
	.btnhg{float: right;}
	.titanau{ font-weight: bold; border-bottom:  1px solid gray;}
	.p_error{ display: none; overflow: auto; clear: both; margin-top: 47px; padding: 6px; color: red; border-radius: 10px;  }
	.infor_men_click{ cursor: pointer;}
	.infor_sem_click{ cursor: pointer;}
	.infor_tri_click{ cursor: pointer;}
	.camp_men_click{ cursor: pointer;}
	.camp_sem_click{ cursor: pointer;}
	.camp_tri_click{ cursor: pointer;}
	.btn_anadirfper{ cursor: pointer;}
	.izquierda{ text-align: left !important;}
	.filauser p{ margin-bottom: 0px; }
	.filauser {position: relative;}
	.nomapidi{ color: gray; font-style: italic;}
	.listvisto{position: absolute;
				bottom: 0px;
				right: 6px;
				color: gray;
				font-size: 13px;}

	.listvisto i {
	    
	    margin-right: 4px;
	    padding: 0px 0px 0px;
	    width: 11px;
	    text-align: center;
	    height: 11px;
	    
	}
</style>

<div class="row">

<?php

function traducirIdioma($user_idioma){
	switch ($user_idioma) {
		case 'es':
			 $idi = "Español";
			break;
		case 'en':
			 $idi = "Inglés"; 
			break;
		case 'eu':
			 $idi = "Euskera";
			 break;
		default:
			 $idi = $user_idioma;
			break;
	}
	return $idi;
}


if(isset($_GET["cliente"])){
	$primer_cliente=$_GET["cliente"];
}else{
	$primer_cliente="";
}

$count=0;
?>
	<input type="hidden" id="usuario_nuevo" value="<?php echo isset($_GET["codigo"]) ? $_GET["codigo"] : ''; ?>"/>

	<div class="col-sm-12">
		<p></p>
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<h4 class="clit"><?php $trans->__('Cliente'); ?>: </h4>

			<select name="test" id="combo_cliente" class="selectboxit visible select2" data-allow-clear="false"  data-placeholder="<?php $trans->__('Elige cliente') ?>...">
				<?php

				$clientes = clientesUsuario($_COOKIE["usuario"]["email"],$com_emails);
				if($clientes["cantidad"]==0){
					echo "<option>".$trans->__('No tienes clientes vinculados', false)."</option>";
				}else{
					$class="";
					foreach ($clientes["elementos"] as $key => $client) {
						if(empty($primer_cliente)){ 
							$primer_cliente = $client["id"];
						}else{
							if($primer_cliente == $client["id"]){ $class= "selected"; }else{ $class="";}
						}
						echo "<option value='".$client["id"]."' ".$class.">".$client["nombre"]."</option>";
					}
				}
				?>

				<option></option>
			</select>
			<input type="hidden" value="<?=$primer_cliente?>" id="client_id">
		</div>	
		<div class="col-sm-4"></div>

	</div>	
	<br clear="both">
	<div class="col-sm-12">
	<p class="admins"><?php $trans->__('Administradores del cliente'); ?>: 
		<?php
		
		$permisos = clientePermisos($primer_cliente,$admin,$_COOKIE["usuario"]["email"],$com_emails);
		if($permisos["admin"] == 0){ $admin_comp=0; }else{ $admin_comp=1;}
	
		$vu = 0 ;
		$admins = clienteAdmins($primer_cliente);
		if($admins["cantidad"]==0){
			echo $trans->__("Este cliente no tiene administradores.");
		}else{
			$admin=0;
			foreach ($admins["elementos"] as $key => $adm) {
				if($vu > 0){ echo ", "; }
				$vu++;
				if($_COOKIE["usuario"]["email"] == $adm["admin"]){
					echo "<span><i><b>(TÚ) ".$adm["admin"]."</b></i></span>";
					$admin=1;
				}else{
					echo "<span><i>".$adm["admin"]."</i></span>";
				}

				
			}
		}
		?>
		</p>

	</div>	

	
	<!--Dos inputs para el seleccionado vista y user-->
	<input type="hidden" id="txt_elimi_email" />
	<input type="hidden" id="txt_elimi_vista" />
	<input type="hidden" id="txt_elimi_codigo" />
	<input type="hidden" id="txt_edit_cliente" value="<?=$primer_cliente?>" />
	<?php
	if($permisos["cantidad"]==0){

			echo $trans->__("Este cliente no tiene ningún permiso.");

		}else{
			$idvista_comp = "";
			$email_comp = "";
			$cod_comp = "";
			foreach ($permisos["elementos"] as $key => $perm) {
				
				//Empezamos a pintar la tabla
				//comprobamos si la vista que se esta guardando es la misma.
				//si es la misma añade una nueva fila sino una nnueva tabla
				if($perm["id_vista"] != $idvista_comp){
					//Si hay que añadir nueva tabla hay que comprobar que primero es necesario cerar la ultima
					if(!empty($idvista_comp)){
						//Pie de la tabla
						?>
								</tbody>
							</table>

							<?php if ($admin_comp==1) { ?>
							
							<button type="button" class="btn btn-primary btnanadiruser" id="btn_anadir_<?=$idvista_comp?>" onclick="jQuery('#modal-1').modal('show');"  name="<?=$idvista_comp?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php $trans->__('Añadir usuario'); ?></button>
							<button type="button" class="btn btn-primary btnedituser" id="btn_editar_<?=$idvista_comp?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <?php $trans->__('Editar usuario'); ?></button>
							<button type="button" class="btn btn-danger btnelimuser" id="btn_eliminar_<?=$idvista_comp?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <?php $trans->__('Eliminar usuario'); ?></button>
							<!--CAPA_AÑADIRUSER-->
							<div class="capa_anadirusuario" id="capa_anadiruser_<?=$idvista_comp?>">
								<p class="titanau"><?php $trans->__('Añadir usuario'); ?></p>
								<div class="caja_combo">
									<select class="select_recomendados form-control" name="<?=$idvista_comp?>">
									<?php
									$vista_cr   = $idvista_comp;
									$cliente_cr = $primer_cliente;
									$correos_cr = CorreosRecomendados($vista_cr, $cliente_cr);
									if($correos_cr["cantidad"]==0){
										echo "<option>".$trans->__('No existen correos recomendados para esta vista',false)."</option>";
									}else{
										
										echo "<option value=''>".$trans->__('Elige un usuario recomendado',false)."</option>";
										
										foreach ($correos_cr["elementos"] as $key => $corr) {
											
											?>
											<option value="<?=$corr['email']?>"><?=$corr["email"]?></option>
											<?php

										}

									}
									?>
									</select>
								</div>
								<div class="caja_input">
									<input type="text" class="inputanadir form-control" id="txt_user_<?=$idvista_comp?>" />
								</div>
								<div class="caja_btn">
									<button type="button" name="<?=$idvista_comp?>" class="btn btn-primary btnhg btn_defi_anauser"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php $trans->__('Añadir usuario'); ?></button>
								</div>
								<div class="caja_btn_c">
									<button type="button" name="<?=$idvista_comp?>" class="btn btn-danger btnhg btncancel"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
								</div>
								<p class="p_error" id="p_error_<?=$idvista_comp?>"><?php $trans->__('Mensaje de error'); ?></p>
							</div>
							<!--CAPA_AÑADIRUSER-->
							<?php }//cierre de si es admin ?>
							
						</div>
						<?php
						//Pie de la tabla
					}

					$idvista_comp = $perm["id_vista"];
					$email_comp   = $perm["user_email"];
					$cod_comp     = $perm["cod"];
					//Cabecera de la tabla
					?>	
						<br>
						<div class="col-sm-12">
							<table class="table table-bordered datatable" id="tabla_permisos">
								<thead>
									<tr>
										<th width="25%" rowspan="2"><span class="titfo"><?=$perm["propiedad"]?> - <b><?=$perm["vista"]?></b></span><br><i><?=$perm["vista_url"]?></i></th>
										<th width="15%" colspan="3"><?php $trans->__('Informes'); ?></th>
										<th width="15%" colspan="3"><?php $trans->__('Campañas'); ?></th>
										<th width="50%" rowspan="2"><?php $trans->__('Filtros personalizados'); ?></th>
									</tr>
									<tr>
										<th width="5%"><?php $trans->__('Sem'); ?></th>
										<th width="5%"><?php $trans->__('Men'); ?></th>
										<th width="5%"><?php $trans->__('Tri'); ?></th>
										<th width="5%"><?php $trans->__('Sem'); ?></th>
										<th width="5%"><?php $trans->__('Men'); ?></th>
										<th width="5%"><?php $trans->__('Tri'); ?></th>
									</tr>
								</thead>
								<tbody>

					<?php
					}			
					?>	
					<!--Fila-->
					<tr name="<?=$perm['id_vista']?>">
						<?php
						if(!empty($perm["user_email"])){
							$userdata = UsuariosData($perm["user_email"]);
							$user_nombre 	= $userdata["elementos"]["nombre"];
							$user_apellidos = $userdata["elementos"]["apellidos"];
							$user_sexo 		= $userdata["elementos"]["sexo"];
							$user_idioma    = $userdata["elementos"]["idioma"];
							$user_codigo = $userdata["elementos"]["codigo"];
							?>
							<!--Email-->
							<td class="fila filauser" name="<?=$perm["user_email"]?>" data-codigo="<?php echo $user_codigo; ?>">
								<p><?=$perm["user_email"]?></p>
								<p class="nomapidi"><?=$user_nombre?> <?=$user_apellidos?>
								<?php if(!empty($user_idioma)){ ?>
								<span>(<?=$trans->__(traducirIdioma($user_idioma))?>)</span></p>
								<?php } 
								$mesd = traducirMes(intval(date('m')));
								$mesactual  = $mesd[intval(date('m'))]["nom"];
								$anoviews   = pagesViews("ano",$primer_cliente,$perm["user_email"],$perm['id_vista']);
								$monthviews = pagesViews("mes",$primer_cliente,$perm["user_email"],$perm['id_vista']);
								$dayviews   = pagesViews("dia",$primer_cliente,$perm["user_email"],$perm['id_vista']);
								//echo "dia - ".$primer_cliente ." - ". $perm["user_email"];
								?>
								<p class="listvisto"><i data-original-title="Páginas vistas en <?=date('Y')?>, <?=$mesactual?> y hoy." class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="" style="padding-rigth:0px;padding-left:0px;"></i> 
									<span><?=$anoviews?> | <?=$monthviews?>  | <?=$dayviews?> </span> 
								</p>
							</td>
							<?php  
							
							//*******************INFORMES****************
							?>
							<!--Informe semanal-->
						  	<td>
						  	<?php 
						  		if(!empty($perm["user_email"])){

						  			if ($admin_comp==1) { 
						  				$semclass="infor_sem_click";
						  			}else{
						  				$semclass="";
						  			}

							  		if($perm["informe_sem"] == 1){
							  			?><img class="ttinfor <?=$semclass?>" src="../images/infor-verde.png" name="1" /><?php
							  		}else{
							  			?><img class="ttinfor <?=$semclass?>" src="../images/infor-rojo.png"  name="0" /><?php
							  		}	
						  		}else{
						  			echo "";
						  		}
						  	?>
						  	</td>
						  	<!--Informe semanal-->

						  	<!--Informe mensual-->
						  	<td>
						  	<?php
						  		if(!empty($perm["user_email"])){

						  			if ($admin_comp==1) { 
						  				$menclass="infor_men_click";
						  			}else{
						  				$menclass="";
						  			}

							  		if($perm["informe_men"] == 1){
							  			?><img class="ttinfor <?=$menclass?>" src="../images/infor-verde.png" name="1" /><?php
							  		}else{
							  			?><img class="ttinfor <?=$menclass?>" src="../images/infor-rojo.png" name="0" /><?php
							  		}	
							  	}else{
						  			echo "";
						  		}
						  	?>
						  	</td>
						  	<!--Informe mensual-->

						  	<!--Informe Trimestral-->
						  	<td>
						  	<?php
						  		if(!empty($perm["user_email"])){

						  			if ($admin_comp==1) { 
						  				$menclass="infor_tri_click";
						  			}else{
						  				$menclass="";
						  			}

							  		if($perm["informe_tri"] == 1){
							  			?><img class="ttinfor <?=$menclass?>" src="../images/infor-verde.png" name="1" /><?php
							  		}else{
							  			?><img class="ttinfor <?=$menclass?>" src="../images/infor-rojo.png" name="0" /><?php
							  		}	
							  	}else{
						  			echo "";
						  		}
						  	?>
						  	</td>
						  	<!--Campanas mensual-->

						  	<?php
						  	//*******************INFORMES****************
							
							//*******************CAMPANAS****************
							?>
							<!--Campanas semanal-->
						  	<td>
						  	<?php 
						  		if(!empty($perm["user_email"])){

						  			if ($admin_comp==1) { 
						  				$semclass="camp_sem_click";
						  			}else{
						  				$semclass="";
						  			}

							  		if($perm["campana_sem"] == 1){
							  			?><img class="ttinfor <?=$semclass?>" src="../images/infor-verde.png" name="1" /><?php
							  		}else{
							  			?><img class="ttinfor <?=$semclass?>" src="../images/infor-rojo.png"  name="0" /><?php
							  		}	
						  		}else{
						  			echo "";
						  		}
						  	?>
						  	</td>
						  	<!--Campanas semanal-->

						  	<!--Campanas mensual-->
						  	<td>
						  	<?php
						  		if(!empty($perm["user_email"])){

						  			if ($admin_comp==1) { 
						  				$menclass="camp_men_click";
						  			}else{
						  				$menclass="";
						  			}

							  		if($perm["campana_men"] == 1){
							  			?><img class="ttinfor <?=$menclass?>" src="../images/infor-verde.png" name="1" /><?php
							  		}else{
							  			?><img class="ttinfor <?=$menclass?>" src="../images/infor-rojo.png" name="0" /><?php
							  		}	
							  	}else{
						  			echo "";
						  		}
						  	?>
						  	</td>
						  	<!--Campanas mensual-->

						  	<!--Campanas Trimestral-->
						  	<td>
						  	<?php
						  		if(!empty($perm["user_email"])){

						  			if ($admin_comp==1) { 
						  				$menclass="camp_tri_click";
						  			}else{
						  				$menclass="";
						  			}

							  		if($perm["campana_tri"] == 1){
							  			?><img class="ttinfor <?=$menclass?>" src="../images/infor-verde.png" name="1" /><?php
							  		}else{
							  			?><img class="ttinfor <?=$menclass?>" src="../images/infor-rojo.png" name="0" /><?php
							  		}	
							  	}else{
						  			echo "";
						  		}
						  	?>
						  	</td>
						  	<!--Campanas mensual-->
						  	<?php
						  	//*******************CAMPANAS****************

						  	//Ahora añadimos los permisos personalizados

						  	$per_pers = filtrosUsuariosVista($perm["user_email"],$perm["id_vista"]);
						  	if($per_pers["cantidad"]==0){

								echo "<td name='".$perm["user_email"]."'><div class='loc_fpers' id='loc_".$count."' name='".$idvista_comp."'></div><a class='btn_anadirfper' id='btn".$count."' name='".$count."' ><span class='glyphicon glyphicon-plus' aria-hidden='true'></span> ".$trans->__('Nuevo filtro',false)."</a></td>";

								$count++;
							}else{

								//Descripcion
								?>
								<td  name='<?=$perm["user_email"]?>'>				
									<div class="loc_fpers" id='loc_<?=$count?>'  name='<?=$idvista_comp?>'>
									<ul class="filafiltro filtrotit">
										<li class="cajat"><span><?php $trans->__('Descripción'); ?></span></li>
										<li class="cajat"><span><?php $trans->__('Técnico'); ?></span></li>
										<li class="cajab"></li>
										<li class="cajab"></li>			
									</ul>

									<?php
									foreach ($per_pers["elementos"] as $key => $prpr) {
									?>
									<ul class="filafiltro">
									<li class="cajat">
										<?php if ($admin_comp==1) { ?>
										<input class="inptex" type="text" id="txt_filtrodesc_save_<?=$prpr['id']?>" value="<?=$prpr['filtro_desc']?>"></li>
										<?php }else{ ?>
										<p class="fsinperm"><?=$prpr['filtro_desc']?></p>
										<?php } ?>
									<li class="cajat">
										<?php if ($admin_comp==1) { ?>
										<input class="inptex" type="text" id="txt_filtro_save_<?=$prpr['id']?>" value="<?=$prpr['filtro']?>">
										<?php }else{ ?>
										<p class="fsinperm"><?=$prpr['filtro']?></p>
										<?php } ?>
									</li>
									<li class="cajab">
										<?php if ($admin_comp==1) { ?>
										<button type="button" class="btn btn-primary" onclick="GuardarFiltro(<?=$prpr['id']?>)">
											<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
										</button>
										<?php } ?>
									</li>
									<li class="cajab">
										<?php if ($admin_comp==1) { ?>
										<button type="button" class="btn btn-primary" onclick="EliminarFiltro(<?=$prpr['id']?>)">
											<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
										</button>
										<?php } ?>
									</li>
									</ul>
									<?php
									}
									?>
									</div>
									<?php if ($admin_comp==1) { ?>
									<a class='btn_anadirfper' id='btn<?=$count?>' name='<?=$count?>'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span> <?php $trans->__('Nuevo filtro'); ?></a>
									<?php } ?>
								</td>
								<?php							

								}
						}else{
								?><td colspan="6"><?php $trans->__('No existen usuarios para esta vista'); ?></td><?php
							}
					  	?>

					</tr>
					<!--Fila-->


			<?php
			}//for
			?>
				</tbody>
				</table>
				<?php if ($admin_comp==1) { ?>
							
					<button type="button" class="btn btn-primary btnanadiruser" id="btn_anadir_<?=$idvista_comp?>" name="<?=$idvista_comp?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php $trans->__('Añadir usuario'); ?></button>
					<button type="button" class="btn btn-primary btnedituser" id="btn_editar_<?=$idvista_comp?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <?php $trans->__('Editar usuario'); ?></button>
					<button type="button" class="btn btn-danger btnelimuser" id="btn_eliminar_<?=$idvista_comp?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <?php $trans->__('Eliminar usuario'); ?></button>
					<!--CAPA_AÑADIRUSER-->
					<div class="capa_anadirusuario" id="capa_anadiruser_<?=$idvista_comp?>">
						<p class="titanau"><?php $trans->__('Añadir usuario'); ?></p>
						<div class="caja_combo">
							<select class="select_recomendados form-control" name="<?=$idvista_comp?>">
							<?php
							$vista_cr   = $idvista_comp;
							$cliente_cr = $primer_cliente;
							$correos_cr = CorreosRecomendados($vista_cr, $cliente_cr);
							if($correos_cr["cantidad"]==0){
								echo "<option>No existen correos recomendados para esta vista</option>";
							}else{
								
								echo "<option value=''>Elige un usuario recomendado</option>";
								
								foreach ($correos_cr["elementos"] as $key => $corr) {
									
									?>
									<option value="<?=$corr['email']?>"><?=$corr["email"]?></option>
									<?php

								}

							}
							?>
							</select>
						</div>
						<div class="caja_input">
							<input type="text" class="inputanadir form-control" id="txt_user_<?=$idvista_comp?>" />
						</div>
						<div class="caja_btn">
							<button type="button" name="<?=$idvista_comp?>" class="btn btn-primary btnhg btn_defi_anauser"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php $trans->__('Añadir usuario'); ?></button>
						</div>
						<div class="caja_btn_c">
							<button type="button" name="<?=$idvista_comp?>" class="btn btn-danger btnhg btncancel"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
						</div>
						<p class="p_error" id="p_error_<?=$idvista_comp?>"><?php $trans->__('Mensaje de error'); ?></p>
					</div>
					<!--CAPA_AÑADIRUSER-->
				<?php }//cierre de si es admin ?>
			</div>
			<?php
	}//else
	?>
	<?php if($admin_comp) : ?>
		<hr/>
		<div class="col-sm-12">
		  <a class="btn btn-primary pull-right" onclick="mantenimientoLogEnvios('<?php echo $primer_cliente; ?>');"><?php $trans->__('Log de envios'); ?></a>
		</div>
	<?php endif; ?>

</div>

<style type="text/css">
	
	#anadirmodaluser{ width: 600px; position: fixed; left: 50%; margin-left: -300px; top: 0; margin-top: 00px; height: 600px; background-color: white; z-index: 99; display: none}
	#capamodal{ position: fixed; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.39);; z-index: 98; top:0; left: 0; overflow: hidden; display: none}

.page-container .main-content{ z-index: auto;}
</style>


	
</div>

<script src="<?=RUTA_ABSOLUTA?>js/permisos_scripts.js"></script>	
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



<?php
include("../includes/conf.php");

if(isset($_POST["modo"])){

	
	switch ($_POST["modo"]) {

		//Informes
		case 'informe_semanal':

			if($_POST["actual"]==1){
				$a = 0;
			}else{
				$a = 1;
			}
			
			ActualizarInformeSemanal($_POST["vista"],$_POST["email"],$a);

			return true;

			break;

		case 'informe_mensual':

			if($_POST["actual"]==1){
				$a = 0;
			}else{
				$a = 1;
			}
			
			ActualizarInformeMensual($_POST["vista"],$_POST["email"],$a);

			return true;

			break;

		case 'informe_trimestral':

			if($_POST["actual"]==1){
				$a = 0;
			}else{
				$a = 1;
			}
			
			ActualizarInformeTrimestral($_POST["vista"],$_POST["email"],$a);

			return true;

			break;
		//Informes

		//Campanas
		case 'campana_semanal':

			if($_POST["actual"]==1){
				$a = 0;
			}else{
				$a = 1;
			}
			
			ActualizarCampanaSemanal($_POST["vista"],$_POST["email"],$a);

			return true;

			break;

		case 'campana_mensual':

			if($_POST["actual"]==1){
				$a = 0;
			}else{
				$a = 1;
			}
			
			ActualizarCampanaMensual($_POST["vista"],$_POST["email"],$a);

			return true;

			break;

		case 'campana_trimestral':

			if($_POST["actual"]==1){
				$a = 0;
			}else{
				$a = 1;
			}
			
			ActualizarCampanaTrimestral($_POST["vista"],$_POST["email"],$a);

			return true;

			break;
		//Campanas


		case 'anadir_usuario':
			$codigo = '';
			
			$data = InsertarUsuario($_POST["vista"],$_POST["email"]);
			
			if($data && isset($data['elementos']) && isset($data['elementos']['codigo'])){
				$codigo = $data['elementos']['codigo'];
			}

			echo $codigo;

			return true;

			break;

		case 'eliminar_usuario':

			
			EliminarUsuario($_POST["vista"],$_POST["email"]);

			return true;

			break;

		case 'anadir_filtro':

			
			AnadirFiltro($_POST["vista"],$_POST["email"],$_POST["filtro"],$_POST["filtrodesc"]);

			return true;

			break;

		case 'eliminar_filtro':

			
			EliminarFiltro($_POST["idf"]);

			return true;

			break;

		case 'guardar_filtro':

			
			GuardarFiltro($_POST["idf"],$_POST["filtrof"],$_POST["filtrodescf"]);

			return true;

			break;

		
		default:
			//no hay modo alguno
			break;
	}


}else{
	//no existe modo
}

?>
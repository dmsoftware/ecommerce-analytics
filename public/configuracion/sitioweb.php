<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	



	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
$titulo = "Sitio web";
include("../includes/breadcrumb.php")
?>

<br />

<style type="text/css">
	
.inputb{ width: 100% !important;}
</style>

<div class="row">

	<div class="col-sm-12">

		<h3><?php $trans->__('Informe sobre tráfico / Contenido más visto'); ?></h3>
		<p><b><?php $trans->__('¿Las caregorías o menus de las dimensiones están enlazadas o desenlazadas entre si?'); ?></b></p>


		<script type="text/javascript">

		function comprobarEnlazada(){
			
			$.ajax({
				  type: 'POST',
				  url: '../configuracion/sitioweb_ajax_comprobarenlazada.php',
				  data: {
				    modo: 'comprobarEnlazada',
				    vista:  $("#dat_idvista").val()         
				    },
				  dataType: 'text',
				  success: function(data){
				  	$(".btn_en").removeClass("btn_activo")
					$(".btn_enlz").parent().removeClass("active");
					  if (/enlazado/.test(data)){  	
					    $("#op_enlazado").parent().addClass("btn_activo");				  							  	
					  	$("#op_enlazado").parent().addClass("active");
					  	$("#op_enlazado").prop("checked", true);
					  	$("#caja_entrelazada").hide();
					  }else{
					  	$("#op_entrelazado").parent().addClass("btn_activo");
					  	$("#op_entrelazado").parent().addClass("active");
					  	$("#op_entrelazado").prop("checked", true);
					  	$("#caja_entrelazada").show();
					  }

				    },
				  error: function(){
				    
				  }
			})//fin ajax
		}

			$(document).ready(function(){
				comprobarEnlazada();


				$(".btn_en").on("click",function(){

					/*Estética clases de activo e inactivo*/
					$(".btn_en").removeClass("btn_activo")
					$(this).addClass("btn_activo");

					var opc_enlx = 	$(this).children().val();

					$.ajax({
					  type: 'POST',
					  url: '../configuracion/sitioweb_ajax_comprobarenlazada.php',
					  data: {
					    modo: 'actualizarenlazada',
					    vista:  $("#dat_idvista").val(),
					    opc:  $(this).children().val()        
					    },
					  dataType: 'text',
					  success: function(data){

						$(".btn_enlz").parent().removeClass("active");

						  if (opc_enlx == "enlazada"){  
						  	//alert($("#dat_idvista").val())					  							  	
						  	$("#op_enlazado").parent().addClass("active");
						  	$("#op_enlazado").prop("checked", true);
						  	$("#caja_entrelazada").hide();
						  }else{
						  	$("#op_entrelazado").parent().addClass("active");
						  	$("#op_entrelazado").prop("checked", true);
						  	$("#caja_entrelazada").fadeIn(600);
						  }

					    },
					  error: function(){
					    
					  }
					})//fin ajax

				})//btn click


				$("#btn_guardarentrelazados").on("click",function(){

					var opc_enlx = 	$(this).children().val();

					$.ajax({
					  type: 'POST',
					  url: '../configuracion/sitioweb_ajax_comprobarenlazada.php',
					  data: {
					    modo: 'guardarentrelazados',
					    vista:  $("#dat_idvista").val(),
					    d1:  $("#etr1").val(),
					    d2:  $("#etr2").val(),
					    d3:  $("#etr3").val(),
					    d4:  $("#etr4").val(),
					    re:  $('input:radio[name=resum]:checked').val()
					    },
					  dataType: 'text',
					  success: function(data){
					  		$(".guardados").stop();
							$(".guardados").fadeIn(600,function(){
								$(".guardados").delay(2000)
								$(".guardados").fadeOut(600);
								
							})	

					    },
					  error: function(){
					    
					  }
					})//fin ajax

				})//btn click


				$(".btn_spam").on("click",function(){

					/*Estética clases de activo e inactivo*/
					$(".btn_spam").removeClass("btn_activo")
					$(this).addClass("btn_activo");

					var opc_enlx = 	$(this).children().val();

					$(".btn_spam i").hide();
					$(this).children("i").fadeIn(300);

					
					if(opc_enlx== "activado"){
						$("#propiedadmen i.fa-shield").removeClass("icoantispam_gris")
						$("#propiedadmen i.fa-shield").addClass("icoantispam_green")
					}else{
						$("#propiedadmen i.fa-shield").removeClass("icoantispam_green")
						$("#propiedadmen i.fa-shield").addClass("icoantispam_gris")
					}

					$.ajax({
					  type: 'POST',
					  url: '../configuracion/sitioweb_ajax_comprobarenlazada.php',
					  data: {
					    modo: 'actualizarantispam',
					    vista:  $("#dat_idvista").val(),
					    opc:  $(this).children().val()        
					    },
					  dataType: 'text',
					  success: function(data){



					    },
					  error: function(){
					    
					  }
					})//fin ajax

				})//btn click


			})//ready	
		</script>
		<style type="text/css">
		.btn_en{ background-color:#949899 !important; color: white !important;}
		.btn_activo{ background-color: #3FABE2 !important;}
		</style>
		<div class="btn-group" data-toggle="buttons">

		  <label class="btn btn_en">
		    <input type="radio" name="options" class="btn_enlz" id="op_enlazado" autocomplete="off" value="enlazada"> <?php $trans->__('Enlazadas');?>
		  </label>

		  <label class="btn btn_en">
		    <input type="radio" name="options" class="btn_enlz" id="op_entrelazado" autocomplete="off" value="entrelazada"> <?php $trans->__('Desenlazadas');?>
		  </label>

		</div>


	</div>

	<style type="text/css">
		.caja_inputs{ width: 400px; display: block;overflow: auto; margin: 15px; margin-top: 25px; padding: 20px; 
						border: solid 1px gray;}
		.caja_inputs label{ width: 30%; float: left; padding-top: 5px; }
		.caja_inputs input{ width: 70%; float: left; margin-bottom: 10px; padding: 5px; }
		.caja_inputs br{ clear: both;}
		.caja_inputs h3{ margin-top: 0px;}
		.caja_inputs button{ float: right; padding: 5px;}
		#caja_entrelazada{display: none}
		.guardados{ color: green; font-weight: bold; float: left; width: 150px; padding-top: 10px;padding-bottom: 0px;margin: 0px; display: none;}
		.comst_izq{ display: block; width: 80%; float: left;}
		.comst_dch{ display: block; width: 20%; float: left;}
		.caja_inputs input.raidost{ margin-top: 7px; margin-bottom: 19px;}
		.legendcheck{ color: gray; font-size: 12px; text-align: right;}
	</style>
	<div class="caja_inputs" id="caja_entrelazada">
		<?php
			
			$enlz_nom = ContenidoEnlazado_nombres($primera_vista);
		?>
		<h3><?php $trans->__('Indicamos los nombres de las categorías');?></h3><br>
		<p class="legendcheck"> <?php $trans->__('*Indica cual aparecerá en el informe semanal');?></p>
		<div class="comst_izq">
			<label><?php $trans->__('Categoría');?> 1</label><input id="etr1" type="text" class="inputtext" value="<?=$enlz_nom['d4']?>"/><br>
			<label><?php $trans->__('Categoría');?> 2</label><input id="etr2" type="text" class="inputtext" value="<?=$enlz_nom['d5']?>"/><br>
			<label><?php $trans->__('Categoría');?> 3</label><input id="etr3" type="text" class="inputtext" value="<?=$enlz_nom['d6']?>"/><br>
			<label><?php $trans->__('Categoría');?> 4</label><input id="etr4" type="text" class="inputtext" value="<?=$enlz_nom['d7']?>"/><br>	
		</div>
		<div class="comst_dch">
			<?php $emailmostrar = ContenidoEnlazado_emailsemanal($primera_vista);
			 if($emailmostrar != false){
			 	if($emailmostrar == 4){ $class="checked";}else{ $class=""; } ?>
			 <?php
			 }else{
			 	$class="checked";
			 }	
			 ?>
			<input type="radio" class="raidost" name="resum" value="4" <?=$class?> /><br>
			<?php if($emailmostrar == 5){ $class="checked";}else{ $class=""; } ?>
			<input type="radio" class="raidost" name="resum" value="5" <?=$class?> /><br>
			<?php if($emailmostrar == 6){ $class="checked";}else{ $class=""; } ?>
			<input type="radio" class="raidost" name="resum" value="6" <?=$class?> /><br>
			<?php if($emailmostrar == 7){ $class="checked";}else{ $class=""; } ?>
			<input type="radio" class="raidost" name="resum" value="7" <?=$class?> /><br>
		</div>
		<p class="guardados"><?php $trans->__('Nombres actualizados');?></p>
		<button type="button" class="btn btn-default btn-lg" id="btn_guardarentrelazados">
		  <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> <?php $trans->__('Guardar');?>
		</button>
	</div>

</div>



<div class="row">
	<div class="col-sm-12">

		<h3 style="margin-top: 50px;"><?php $trans->__('Filtro anti-spam'); ?></h3>

		<?php
		$hoy = date("d-m-Y");
		$haceanno = strtotime ( '-1 year' , strtotime ( $hoy ) ) ;
		$haceanno = date ( 'd-m-Y' , $haceanno );
		?>

		<p><?php $trans->__('Es posible que muchas visitas que llegan a nuestro sitio web sean falsas ya que están provocadas por robots que pretenden hacer lo que se denomina en el argot de la analítica, spam referer. El objetivo de estas organizaciones que practican el llamado spam-referer es que es que los que visitan los informes analíticos hagan click en los dominios desde los que nos han llegado esas falsas visitas. Esos clicks, normalmente, les hacen ganar dinero por programas de afiliación.'); ?></p>
		<p><?php $trans->__('Otras visitas pueden venir de servicios de traducción automática o de cacheo y también es preferible no tenerlas en cuenta ya que pueden resultar duplicadas.'); ?></p>
		



		<p><b><?php $trans->__('¿Quiere activar el filtro anti-spam?'); ?></b></p>

		<style type="text/css">
		.btn_spam{ background-color:#949899 !important; color: white !important;}
		.active{ background-color: #3FABE2 !important;}
		.izquierda{ text-align: left; display: block;}
		.capabtns{margin-bottom: 20px;}
		.quefiltroes{background-color: #F5F5F5;padding: 10px;}
		.filtm{ margin-bottom: 0px; font-style: italic; font-size: 14px; color: #4D8657;}
		#tot_sesiones{ font-size: 15px;}
		</style>

		<div class="btn-group capabtns" data-toggle="buttons">

			<?php
			$antispam=0;
			//BUSCAMOS EL FILTRO ANTIESPAN Y SI HAY CONCATENAMOS FILTRO
			$f = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);			
			$sqlf = " SELECT LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM, ";
			$sqlf .= " STUFF(( SELECT ';ga:hostname!@' + LOCDV_ANALYTICS_URL_VISTA ";
			$sqlf .= "		  FROM LOC_DOMINIOS_VISTAS INNER JOIN LOC_DOMINIOS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO ";
			$sqlf .= "		  WHERE LOCD_ANALYTICS_ID_PROPIEDAD=LD.LOCD_ANALYTICS_ID_PROPIEDAD   ";
			$sqlf .= "		  GROUP BY LOCDV_ANALYTICS_URL_VISTA FOR XML PATH('')) ,1,1,'') AS FILTRO_SPAM_HOST, ";
			$sqlf .= "  STUFF(( SELECT ',ga:source=@' + LOCSR_REFERER  ";
			$sqlf .= "		  FROM LOC_SPAM_REFERER FOR XML PATH('')) ,1,1,'') AS FILTRO_SPAM_REFERER ";
			$sqlf .= "  FROM LOC_DOMINIOS LD INNER JOIN LOC_DOMINIOS_VISTAS ON LD.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO ";
			$sqlf .= "  WHERE LOCDV_ANALYTICS_ID_VISTA='".$primera_vista."' ";
			$sqlf .= "  GROUP BY LOCD_ANALYTICS_ID_PROPIEDAD,LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM	";	

				
			
			$stmtf = $f->prepare($sqlf);
			$resultf = $stmtf->execute();
			
			if ($resultf && $stmtf->rowCount() != 0) {
			  	while ($filaf = $stmtf->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
					$antiespam = $filaf[0];
					$filtroa   = $filaf[1];
					$filtrob   = $filaf[2];
			  	}
			  	$filtro = "";
			  	    
				if(!empty($filtro)){
					$filtromostrar = ";".$filtrob.",".$filtroa;
				}else{
					$filtromostrar = $filtrob.",".$filtroa;
				}

				if ($antiespam == 1) {
					$classa  = "active";
					$classb  = "";
					$escudoa = "block";
					$escudob = "none";
					
				}else{
					$classa = "";
					$classb = "active";
					$escudoa = "none";
					$escudob = "block";
				}
				
			}
			?>

			<?php
				
			?>
		 	<label class="btn btn_spam <?=$classa?>">
		 	  <input type="radio" name="optionspam" class="btn_spamm" id="op_spamact" autocomplete="off" value="activado"> <i class="fa fa-shield icoantispam_green" style="font-size:13px; width:10px; float:left; margin-top:2px; margin-right:8px; display:<?=$escudoa?>"></i> <?php $trans->__('Activado');?>
		 	</label>

			<label class="btn btn_spam <?=$classb?>">
		 	  <input type="radio" name="optionspam" class="btn_spamm" id="op_spandes" autocomplete="off" value="desactivado"> <i class="fa fa-shield icoantispam_gris" style="font-size:13px; width:10px; float:left; margin-top:2px; margin-right:8px; display:<?=$escudob?>"></i> <?php $trans->__('Desactivado');?>
		 	</label>

		</div>
		<p><?php $trans->__('Presentamos una muestra de visitas a nuestro sitio web que creemos que son spam durante el último año:'); ?> <span class="fechamostrandose">(Del <?=$haceanno?> al <?=$hoy?>)</span></p>
		<div class="tabla_spam">
			<table class="table table-bordered datatable" id="table_datatable_sitioweb">
				<thead>
					<tr>
						<th width="25%"><?php $trans->__('Hostname'); ?></th>
						<th width="15%"><?php $trans->__('Medio'); ?></th>
						<th width="15%"><?php $trans->__('Fuente'); ?></th>
						<th width="15%"><?php $trans->__('Sesiones'); ?></th>
						<th width="15%"><?php $trans->__('Páginas vistas por sesión'); ?></th>
						<th width="15%"><?php $trans->__('Tas. rebote'); ?></th>
					</tr>	
				</thead>		
				<tbody>
				<tfoot>
					<tr>
						<th colspan="3"><?php $trans->__('Total spam'); ?></th>
						<th><span id="tot_sesiones"></span></th>
						<th><span id="tot_paginasvistassesion"></th>
						<th><span id="tot_tasarebote"></span></th>
					</tr>
					<tr>
						<th colspan="3"><?php $trans->__('Total (Todo incluido)'); ?></th>
						<th><span id="tot_tot_sesiones"></span></th>
						<th><span id="tot_tot_paginasvistassesion"></th>
						<th><span id="tot_tot_tasarebote"></span></th>
					</tr>					
				</tfoot>
			</table>
		</div>

		<div class="quefiltroes">
			<p><?php $trans->__('El filtro aplicado para activar el filtro anti-spam es:'); ?></p>
			<p class="filtm"><?=$filtromostrar?></p>
		</div>

	</div>

</div>

<script src="<?=RUTA_ABSOLUTA?>js/sitioweb_scripts.js"></script>	
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



﻿<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side_campanas.php"); 
?>

<div class="main-content">

<?php

include("../includes/breadcrumb.php")
?>
<br>
<style type="text/css">
	.DTTT_container { margin-top: -1px;	}
	.izquierda{ text-align: left; display: block;}
	.btn_vercampana{ cursor: pointer;}
	.agrupacion{ background-color: #F0F0F0;}
	.total{ background-color: rgba(149, 196, 243, 0.15);
		    font-weight: bold;
		    border-bottom: 1px solid rgb(203, 203, 203) !important;
		}
	.subtotal{background-color: #EAEAEA;
		    font-weight: bold;
		    font-style: italic;
		    border-bottom: 1px solid rgb(203, 203, 203) !important}
	.subtotal_tit{ display: block; color: gray !important}
	.filaSeo{ background-color: rgba(254, 107, 91, 0.42);}
	.filaEnlaces{ background-color: rgba(1, 213, 251, 0.28);}
	.filaSocial{ background-color: rgba(251, 198, 60, 0.45);}
	.filaTotal{ background-color: rgba(0, 0, 0, 0.13);}
	.porcpri{ font-size: 14px !important;
			  color: #EBA351;
			  font-weight: normal;}
	.porcseg{ font-size: 14px !important;
			  color: #74BFC4;
			  font-weight: normal;}

	.porcpri_dt{ font-size: 11px !important;
			  color: #EBA351;
			  font-weight: normal;}
	.porcseg_dt{ font-size: 11px !important;
			  color: #74BFC4;
			  font-weight: normal;}
	.barrita{ color: #D4D2D2;
		};
	.num .barrita{
		font-size: 16px !important;
	}

	.dataTables_wrapper table tfoot tr th .porcpri{ font-size: 11px !important; }
	.dataTables_wrapper table tfoot tr th .porcseg{ font-size: 11px !important; }
	#total_graf{ padding: 0px;}
	#imglista{ list-style: none;}
	#imglista li{ display: block;float: left; width: 20%; }
	#imglista li span{  text-align: center;
					background-color: #74BFC4;
					color: #FFF;
					border-radius: 6px;
					width: 15px;
					margin: auto;
					display: block;
					height: 17px;}
	#imglegend{overflow: auto;
				position: absolute;
				bottom: 28px;
				width: 88%;}
	.mdc{text-align: center;
					background-color: #74BFC4;
					color: #FFF;
					border-radius: 6px;
					width: 15px;
					margin: auto;
					display: block;
					height: 17px;
					float: left;
					margin-right: 4px;
					font-weight: normal;
					font-size: 12px;
				padding-top: 2px;}
	.mdcm{text-align: center;
					background-color: #eba351;
					color: #FFF;
					border-radius: 6px;
					width: 15px;
					margin: auto;
					display: block;
					height: 17px;
					float: left;
					margin-right: 4px;
					font-weight: normal;
					font-size: 12px;
				padding-top: 2px;}
	h3{ font-weight: bold; margin-bottom: 0px !important;}
	.tile-stats.tile-white-gray .num, .tile-stats.tile-white-gray h3, .tile-stats.tile-white-gray p {
	    color: #505050 !important;
	}
	.num{ margin-bottom: 11px !important; color: gray !important;}
	.tile-stats.tile-white-gray .num {
	    color: #928888 !important;
	}
	h3, .h3 {
	    margin-bottom: 0px !important;
	}
</style>

<script id="jqueryui" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js" defer async></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<div class="col-sm-12">
	<!--******************DATATABLE DE CAMPAÑAS***************************-->

	<div class="row">
	
		<div class="col-sm-3">
		
			<div class="tile-stats tile-white-gray" id="total_impactos">
				<div class="icon"><i class="entypo-user"></i></div>
				
				<h3 style="margin-bottom: 0px !important"><?php $trans->__('Tasa de conversión'); ?></h3>
				<div id="resumen_conversion" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
				<p><span id="resumen_producto1_icono"></span>&nbsp;</p>

				<h3 style="margin-bottom: 0px !important"><span class="mdcm">1</span> <?php $trans->__('Sesiones'); ?></h3>
				<div id="resumen_sesiones" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
				<p><span id="resumen_producto2_icono"></span>&nbsp;</p>

				<h3 style="margin-bottom: 0px !important"><span class="mdcm">2</span> <?php $trans->__('Vistas de producto'); ?></h3>
				<div id="resumen_vistasproducto" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
				<p><span id="resumen_producto2_icono"></span>&nbsp;</p>
			</div>
		</div>
		
		<div class="col-sm-3">
		
			<div class="tile-stats tile-white-gray" id="total_rate">
				<div class="icon"><i class="entypo-user"></i></div>
				
				<h3 style="margin-bottom: 0px !important"><span class="mdc">3</span> <?php $trans->__('Añadir al carrito'); ?></h3>
				<div id="resumen_anadircarrito" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
				<p><span id="resumen_producto1_icono"></span>&nbsp;</p>

				<h3 style="margin-bottom: 0px !important"><span class="mdc">4</span> <?php $trans->__('Login/Registro'); ?></h3>
				<div id="resumen_loginregistro" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
				<p><span id="resumen_producto2_icono"></span>&nbsp;</p>

				<h3 style="margin-bottom: 0px !important"><span class="mdc">5</span> <?php $trans->__('Revisar pedido'); ?></h3>
				<div id="resumen_revisarpedido" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
				<p><span id="resumen_producto2_icono"></span>&nbsp;</p>

				<h3 style="margin-bottom: 0px !important"><span class="mdc">6</span> <?php $trans->__('Finalizar pedido'); ?></h3>
				<div id="resumen_finalizarpedido" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
				<p><span id="resumen_producto2_icono"></span>&nbsp;</p>

				<h3 style="margin-bottom: 0px !important"><span class="mdc">7</span> <?php $trans->__('Pedido pagado'); ?></h3>
				<div id="resumen_pagorealizado" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
				<p><span id="resumen_producto2_icono"></span>&nbsp;</p>
			</div>
			
		</div>

		<div class="col-sm-6">
		
			<div class="tile-stats tile-white-gray" id="total_graf">
				<div id="chart_div" style="width: 110%; margin-left: -63px; margin-top: -30px; height: 325px;"></div>
				<div id="imglegend">
					<ul id="imglista">
						<li><span>3</span></li>
						<li><span>4</span></li>
						<li><span>5</span></li>
						<li><span>6</span></li>
						<li><span>7</span></li>
					</ul>
				</div>
			</div>
			
		</div>
		
	</div>	
		

	<div class="row">

		<br>
		<div class="grupo-perspectivas">
			
			<div class="btn-group" data-toggle="buttons">
				<p><?php $trans->__('¿Bajo qué perspectiva deseas ver los datos?'); ?></p>

				<!---<div class="caja-perspectiva">
					<p><?php $trans->__('Ecommerce'); ?></p>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Producto"><?php $trans->__('Productos'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Categoria1"><?php $trans->__('Categoría 1'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Categoria2"><?php $trans->__('Categoría 2'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Marca"><?php $trans->__('Marca'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="ListaProducto"><?php $trans->__('Lista de productos'); ?></button>
				</div>-->

				<div class="caja-perspectiva">
					<p><?php $trans->__('Tecnología'); ?></p>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Dispositivos"><?php $trans->__('Dispositivos'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Navegadores"><?php $trans->__('Navegadores'); ?></button>				
				</div>

				<div class="caja-perspectiva">
					<p><?php $trans->__('Temporal'); ?></p>
					<button type="button" class="btn btn-default radiopers_per active_pri" name="perspectiva_per" value="Diario"><?php $trans->__('Diario'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Semanal"><?php $trans->__('Semanal'); ?></button>	
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Mensual"><?php $trans->__('Mensual'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Anual"><?php $trans->__('Anual'); ?></button>	
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Horas"><?php $trans->__('Horas del día'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="DiasSemana"><?php $trans->__('Días de la semana'); ?></button>	
				</div>

				<div class="caja-perspectiva">
					<p><?php $trans->__('Geográfico'); ?></p>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Pais"><?php $trans->__('País'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Region"><?php $trans->__('Región'); ?></button>	
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Ciudad"><?php $trans->__('Ciudad'); ?></button>
				</div>

				<div class="caja-perspectiva">
					<p><?php $trans->__('Usuarios'); ?></p>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Sexo"><?php $trans->__('Sexo'); ?></button>
					<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Edad"><?php $trans->__('Edad'); ?></button>
				</div>

			</div>

		</div><!--cierre grupo perspectivas-->

		<input type="hidden" id="perspectiva_camb" value="" />
		<input type="hidden" id="perspectiva" value="Diario" />
		<input type="hidden" id="perspectiva_seg" value="" />

		<br>
		<div class="row">
			
			<table class="table table-bordered datatable" id="table_datatable_embudos">
				<thead>
					<tr>
						<th width="12%" id="cab_perspectiva_camb"><span class="cab_tit" id="cab_perspectiva"><?php $trans->__('Diario'); ?></span></th>
						<th width="12%" id="cab_perspectiva_seg_camb"><span class="cab_tit" id="cab_perspectiva_seg"></span></th>
						<th width="6%"><?php $trans->__('Tasa de conversión'); ?></th>
						<th width="10%"><?php $trans->__('Sesiones'); ?></th>
						<th width="10%"><?php $trans->__('Vistas de producto'); ?></th>
						<th width="10%"><?php $trans->__('Añadir al carrito'); ?></th>
						<th width="10%"><?php $trans->__('Login/Registro'); ?></th>
						<th width="10%"><?php $trans->__('Revisar pedido'); ?></th>
						<th width="10%"><?php $trans->__('Finalizar pedido'); ?></th>
						<th width="10%"><?php $trans->__('Pedido pagado'); ?></th>
					</tr>	
				</thead>
				<tbody>
				<tfoot>
				<tr>
					<th><?php $trans->__('TOTALES'); ?></th>
					<th></th>
					<th><span id="total_ts"></span></th>
					<th><span id="total_s"></span></th>
					<th><span id="total_vp"></span></th>
					<th><span id="total_ac"></span></th>
					<th><span id="total_lr"></span></th>
					<th><span id="total_rp"></span></th>
					<th><span id="total_fp"></span></th>
					<th><span id="total_pp"></span></th>
				</tr>
				</tfoot>

			</table>

		</div>

	</div>	
	
	<!--**************************************************************-->
</div>


</div>

<script src="<?=RUTA_ABSOLUTA?>js/campanas_embudos_pedidos_scripts.js"></script>	

<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_ecommerce_informes.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



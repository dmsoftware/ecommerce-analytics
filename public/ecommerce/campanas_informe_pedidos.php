﻿<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side_campanas.php"); 
?>

<div class="main-content">

<?php

include("../includes/breadcrumb.php")
?>
<br>

<div class="col-sm-12">


<div class="row">
	
	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray" id="total_impactos">

			<div class="icon"><i class="entypo-user"></i></div>

			<div id="resumen_pedidos" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3><?php $trans->__('Pedidos'); ?></h3>
			<p><span id="resumen_pedidos_icono"></span>&nbsp;</p>

			<div id="resumen_pedidos_vendidos" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3><?php $trans->__('Productos vendidos'); ?></h3>
			<p><span id="resumen_pedidos_vendidos_icono"></span>&nbsp;</p>
		</div>
	</div>
	
	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray" id="total_rate">
			<div class="icon"><i class="entypo-monitor"></i></div>

			<div id="resumen_ingresosb" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3><?php $trans->__('Ingresos bruto'); ?></h3>
			<p><span id="resumen_ingresosb_icono"></span>&nbsp;</p>

			<div id="resumen_ingresosn" class="num bumb" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 class="bumb"><?php $trans->__('Ingresos neto'); ?></h3>
			<p><span id="resumen_ingresosn_icono"></span>&nbsp;</p>

			<div id="resumen_pmedio" class="num bumb" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 class="bumb"><?php $trans->__('Pedido medio'); ?></h3>
			<p><span id="resumen_pmedio_icono"></span>&nbsp;</p>
		</div>
		
	</div>

	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray" id="total_rate">
			<div class="icon"><i class="entypo-monitor"></i></div>

			<div id="resumen_tconversion" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3><?php $trans->__('Tasa conversión'); ?></h3>
			<p><span id="resumen_tconversion_icono"></span>&nbsp;</p>
		</div>
		
	</div>
	
</div>

<div class="row">

	<br>
	<div class="grupo-perspectivas">
		
		<div class="btn-group" data-toggle="buttons">
			<p><?php $trans->__('¿Bajo qué perspectiva deseas ver los datos?'); ?></p>
			
			<div class="caja-perspectiva">
				<p><?php $trans->__('Ecommerce'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="CodPedido"><?php $trans->__('Código pedido'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="DiasTransaccion"><?php $trans->__('Días para la transacción'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="SesionesTransaccion"><?php $trans->__('Sesiones para la transacción'); ?></button>
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Tecnología'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Dispositivos"><?php $trans->__('Dispositivos'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Navegadores"><?php $trans->__('Navegadores'); ?></button>				
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Temporal'); ?></p>
				<button type="button" class="btn btn-default radiopers_per active_pri" name="perspectiva_per" value="Diario"><?php $trans->__('Diario'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Semanal"><?php $trans->__('Semanal'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Mensual"><?php $trans->__('Mensual'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Anual"><?php $trans->__('Anual'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Horas"><?php $trans->__('Horas del día'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="DiasSemana"><?php $trans->__('Días de la semana'); ?></button>	
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Geográfico'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Pais"><?php $trans->__('País'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Region"><?php $trans->__('Región'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Ciudad"><?php $trans->__('Ciudad'); ?></button>
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Usuarios'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Sexo"><?php $trans->__('Sexo'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Edad"><?php $trans->__('Edad'); ?></button>
			</div>

		</div>

	</div><!--cierre grupo perspectivas-->

	<input type="hidden" id="perspectiva_camb" value="" />
	<input type="hidden" id="perspectiva" value="Diario" />
	<input type="hidden" id="perspectiva_seg" value="" />

	<div class="row">
		
		<table class="table table-bordered datatable" id="table_datatable_pedidos">
			<thead>
				<tr>
					<th width="20%" id="cab_perspectiva_camb" ><span class="cab_tit" id="cab_perspectiva"><?php $trans->__('Diario'); ?></span></th>
					<th width="20%" id="cab_perspectiva_seg_camb"><span class="cab_tit" id="cab_perspectiva_seg"></span></th>
					<th width="10%"><?php $trans->__('Ingresos bruto'); ?></th>
					<th width="10%"><?php $trans->__('Ingresos neto'); ?></th>
					<th width="10%"><?php $trans->__('N. pedidos'); ?></th>
					<th width="10%"><?php $trans->__('N. de productos vendidos'); ?></th>
					<th width="10%"><?php $trans->__('Tasa conversión'); ?></th>
					<th width="10%"><?php $trans->__('Pedido medio'); ?></th>
				</tr>	
			</thead>
			<tfoot>
			<tr>
				<th><?php $trans->__('TOTALES'); ?></th>
				<th></th>
				<th><span id="total_ibruto"></th>
				<th><span id="total_ineto"></th>
				<th><span id="total_npedidos"></span></th>
				<th><span id="total_nproductos"></span></th>
				<th><span id="total_tconversion"></span></th>
				<th><span id="total_pmedio"></span></th>
			</tr>
			</tfoot>
			<tbody>

		</table>

	</div>

</div>



</div>

<script src="<?=RUTA_ABSOLUTA?>js/informes_pedidos_scripts.js"></script>	

<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_ecommerce_informes.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



﻿<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side_campanas.php"); 
?>

<div class="main-content">

<?php

include("../includes/breadcrumb.php")
?>
<br>

<div class="col-sm-12">


<div class="row">
	
	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray" id="total_impactos">
			<div class="icon"><i class="entypo-user"></i></div>
			
			<h2><?php $trans->__('Productos top'); ?></h2>	

			<div id="resumen_producto1" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_nombre_producto1"></h3>
			<p><span id="resumen_producto1_icono"></span>&nbsp;</p>

			<div id="resumen_producto2" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_nombre_producto2"></h3>
			<p><span id="resumen_producto2_icono"></span>&nbsp;</p>
		</div>
	</div>
	
	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray" id="total_rate">
			<div class="icon"><i class="entypo-user"></i></div>
			
			<h2><?php $trans->__('Marcas top'); ?></h2>	

			<div id="resumen_marca1" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_nombre_marca1"></h3>
			<p><span id="resumen_marca1_icono"></span>&nbsp;</p>

			<div id="resumen_marca2" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_nombre_marca2"></h3>
			<p><span id="resumen_marca2_icono"></span>&nbsp;</p>
		</div>
		
	</div>

	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray" id="total_rate">
			<div class="icon"><i class="entypo-user"></i></div>
			
			<h2><?php $trans->__('Categorías top'); ?></h2>	

			<div id="resumen_categoria1" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_nombre_categoria1"></h3>
			<p><span id="resumen_categoria1_icono"></span>&nbsp;</p>

			<div id="resumen_categoria2" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_nombre_categoria2"></h3>
			<p><span id="resumen_categoria2_icono"></span>&nbsp;</p>
		</div>
		
	</div>

	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray" id="total_rate">
			<div class="icon"><i class="entypo-user"></i></div>
			
			<h2><?php $trans->__('Listas top'); ?></h2>	

			<div id="resumen_listas1" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_nombre_listas1" ></h3>
			<p><span id="resumen_lista1_icono"></span>&nbsp;</p>

			<div id="resumen_listas2" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_nombre_listas2" ></h3>
			<p><span id="resumen_lista2_icono"></span>&nbsp;</p>
		</div>
		
	</div>
	
</div>

<div class="row">

	<br>
	<div class="grupo-perspectivas">
		
		<div class="btn-group" data-toggle="buttons">
			<p><?php $trans->__('¿Bajo qué perspectiva deseas ver los datos?'); ?></p>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Ecommerce'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Producto"><?php $trans->__('Productos'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Categoria1"><?php $trans->__('Categoría 1'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Categoria2"><?php $trans->__('Categoría 2'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Marca"><?php $trans->__('Marca'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="ListaProducto"><?php $trans->__('Lista de productos'); ?></button>
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Tecnología'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Dispositivos"><?php $trans->__('Dispositivos'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Navegadores"><?php $trans->__('Navegadores'); ?></button>				
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Temporal'); ?></p>
				<button type="button" class="btn btn-default radiopers_per active_pri" name="perspectiva_per" value="Diario"><?php $trans->__('Diario'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Semanal"><?php $trans->__('Semanal'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Mensual"><?php $trans->__('Mensual'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Anual"><?php $trans->__('Anual'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Horas"><?php $trans->__('Horas del día'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="DiasSemana"><?php $trans->__('Días de la semana'); ?></button>	
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Geográfico'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Pais"><?php $trans->__('País'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Region"><?php $trans->__('Región'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Ciudad"><?php $trans->__('Ciudad'); ?></button>
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Usuarios'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Sexo"><?php $trans->__('Sexo'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="Edad"><?php $trans->__('Edad'); ?></button>
			</div>

		</div>

	</div><!--cierre grupo perspectivas-->

	<input type="hidden" id="perspectiva_camb" value="" />
	<input type="hidden" id="perspectiva" value="Diario" />
	<input type="hidden" id="perspectiva_seg" value="" />

	<br>
	<div class="row">
		
		<table class="table table-bordered datatable" id="table_datatable_productos">
			<thead>
				<tr>
					<th width="20%" id="cab_perspectiva_camb"><span class="cab_tit" id="cab_perspectiva"><?php $trans->__('Diario'); ?></span></th>
					<th width="20%" id="cab_perspectiva_seg_camb"><span class="cab_tit" id="cab_perspectiva_seg"></span></th>
					<th width="10%"><?php $trans->__('N. de productos vendidos'); ?></th>
					<th width="10%"><?php $trans->__('N. de productos únicos vendidos'); ?></th>
					<th width="10%"><?php $trans->__('Total vendido'); ?> (€)</th>
					<th width="10%"><?php $trans->__('Precio medio del producto vendido'); ?></th>
					<th width="10%"><?php $trans->__('Media de productos en un pedido'); ?></th>
				</tr>	
			</thead>
			<tfoot>
			<tr>
				<th><?php $trans->__('TOTALES'); ?></th>
				<th></th>
				<th><span id="total_nv"></th>
				<th><span id="total_nuv"></th>
				<th><span id="total_tv"></span></th>
				<th><span id="total_pmpv"></span></th>
				<th><span id="total_mpp"></span></th>
			</tr>
			</tfoot>
			<tbody>

		</table>

	</div>

</div>



</div>

<script src="<?=RUTA_ABSOLUTA?>js/informes_productos_scripts.js"></script>	

<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_ecommerce_informes.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



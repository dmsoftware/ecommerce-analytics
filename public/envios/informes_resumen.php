<?php
include("../includes/conf.php"); 
include("../includes/usuarios/validaciones_usuarios.php");
include("../informes/funciones_ajax.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	<title>DMIntegra | Email</title>

	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/neon-core.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/neon-forms.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/custom.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/skins/white.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>css/estilos_head.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>css/estilos_side.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>css/estilos_informes_resumen.css">

	<script src="<?=RUTA_ABSOLUTA?>js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<?php
if(isset($_GET["vista"]) && isset($_GET["fechaini"]) && isset($_GET["fechafin"])){

//Variable que obtengo
$idVista    = $_GET["vista"];   //Id de la vista
$fechaIni   = $_GET["fechaini"]; //Fecha de inicio
$fechaFin   = $_GET["fechafin"]; //Fecha final
if(isset($_GET["filtro"])){
	$filtro = $_GET["filtro"]; 	 //filtro a añadir
}else{
	$filtro = 'no';
}

if(isset($_GET["filtrodesc"])){
	$filtrodesc = $_GET["filtrodesc"]; 	 //filtro a añadir
	if($filtrodesc=="Sin ningún filtro"){
		$filtrodesc="";
	}
}else{
	$filtrodesc = '';
}

if(isset($_GET["tipo"])){
	$tipo   = $_GET["tipo"];//Semanal o mensual
	if($tipo == "Mes" || $tipo == "mes"){
		$mesi = date("m", strtotime($fechaIni));
		$mess = traducirMes( $mesi );
		$tipo   = ucwords($mess[intval($mesi)]["nom"]);
	}
}else{
	$tipo   = "Semana";
}



/*//Variable que obtengo
$idVista    = '23433234';   //Id de la vista
$fechaIni   = '2015-02-01'; //Fecha de inicio
$fechaFin   = '2015-02-12'; //Fecha final
$filtro     = 'no'; 		  //filtro a añadir
$filtrodesc = 'no'; 		  //filtro a añadir
$tipo       = 'Semanal';    //Semanal o mensual*/


$proy = propiedadVista($idVista);
foreach ($proy["elementos"] as $key => $pr) {
	$proyectasco = $pr["proyectoasociado"];
	$nombre_vista = $pr["nombre_vista"];
	$nombre_propiedad = $pr["nombre_propiedad"];
}

//PASAMOS EL FILTRO POR ** PARA QUE LOS == PUEDAN IR POR LA URL
$filtro = str_replace("==","**",$filtro);

//Variable que necesito
$proyectoasociado = $proyectasco; //Proyecto asociado
$fechaIni_ant = '2015-01-01';//La fecha de incio del periodo anterior
$fechaFin_ant = '2015-01-24';//La fecha final del periodo anterior

?>
<input type="hidden" id="dat_proyectoasociado" value="<?=$proyectasco?>" />
<input type="hidden" id="dat_idvista"          value="<?=$idVista?>" />
<input type="hidden" id="dat_fechaini"         value="<?=$fechaIni?>" />
<input type="hidden" id="dat_fechafin"         value="<?=$fechaFin?>" />
<input type="hidden" id="dat_fechaini_ant"     value="<?=$fechaIni_ant?>" />
<input type="hidden" id="dat_fechafin_ant"     value="<?=$fechaFin_ant?>" />
<input type="hidden" id="dat_filtro"           value="<?=$filtro?>" />
<input type="hidden" id="dat_tipo"             value="<?=$tipo?>" />
        
<div class="main-content" id="contenedor_resumen">

<div class="cabecera_resumen">
	<div class="pinfo">
		<p id="txt_propiedad"><span class="nprop"><?=$nombre_propiedad ."</span> <span class='nvist'> >> ".$nombre_vista?></span></p>
		<p id="txt_url">(http://www.<?=DominioVista($idVista)?>)</p>
		<p id="txt_fecha"><span>Del <?=rangoFechas($fechaIni,$fechaFin)?>,</span> <span class="compdate">comparado con el <?=rangoFechas($fechaIni_ant,$fechaFin_ant)?></span></p>
		<p id="rxr_filtros"><?=$filtrodesc?></p>
	</div>
	<div class="pdispo">
	<?php
	$resumdispo = resumen_dispositivos($proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
	$dd = 0;
	$dm = 0;
	$dt = 0;
	foreach ($resumdispo as $key => $rdispo) {
		if ($rdispo["nombre"] == "desktop") {
			$desktop = $rdispo["rate"];
			$dd = 1;
		}
		if ($rdispo["nombre"] == "mobile") {
			$mobile = $rdispo["rate"];
			$dm = 1;
		}
		if ($rdispo["nombre"] == "tablet") {
			$tablet = $rdispo["rate"];
			$dt = 1;
		}
	}

	if($dd==0){
		$desktop = "0%";
	}
	if($dm==0){
		$mobile  = "0%";
	}
	if($dt==0){
		$tablet  = "0%";
	}

	?>
		<div class="dispo escritorio">
			<?php
			
			?>
			<p><?=$desktop?></p>
			<img src="../images/escritorio-res.jpg" />
		</div>
		<div class="dispo tableta">
			<p><?=$tablet?></p>
			<img src="../images/tablet-res.jpg" />
		</div>
		<div class="dispo movil">
			<p><?=$mobile?></p>
			<img src="../images/movil-res.jpg" />
		</div>
	</div>
</div>

<div class="row">
	
	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray">
			<h3 class="restit">Conversiones</h3>
			<table class="table table-bordered datatable datatableres">
				<thead>
					<tr>
						<th width="25%"><?=$tipo?></th>
						<th width="25%"><?=date('Y')?></th>
						<th width="50%"><span class="ndimension"></span></th>
					</tr>	
				</thead>	
				<tbody>
				<?php
				$conconv = resumen_datatable_conversiones_res ("datatable", $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
				echo $conconv;
				?>
				</tbody>
			</table>
		</div>
		
	</div>
	
	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray">
			<h3 class="restit">Tráfico</h3>
			<table class="table table-bordered datatable datatableres">
				<thead>
					<tr>
						<th width="25%"><?=$tipo?></th>
						<th width="25%"><?=date('Y')?></th>
						<th width="50%"><span class="ndimension"></span></th>
					</tr>	
				</thead>	
				<tbody>
				<?php
				$restrafico = resumen_datatable_trafico_res ("datatable", $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
				echo $restrafico;
				?>
				</tbody>
			</table>
		</div>
		
	</div>

	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray">
			<h3 class="restit">Procedencia de usuarios</h3>
			<table class="table table-bordered datatable datatableres" >
				<thead>
					<tr>
						<th width="25%"><?=$tipo?></th>
						<th width="25%"><?=date('Y')?></th>
						<th width="50%"><span class="ndimension"></span></th>
					</tr>	
				</thead>	
				<tbody>
				<?php
				$respais = resumen_datatable_pais_res ("datatable", $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
				echo $respais;
				?>
				</tbody>
			</table>
		</div>
		
	</div>

	<div class="col-sm-3" style="padding-right: 15px;">
	
		<div class="tile-stats tile-white-gray">
			<h3 class="restit">Indexación</h3>
			<?php
			$pagstotal = pagsIndexadas($idVista,$fechaIni,$fechaFin,'media');
			$pasporapi = resumen_indexacion_com ($proyectasco, $idVista, $fechaIni, $fechaFin, $filtro )
			?>
			<span><span class="res_titindex"><?=number_format($pagstotal,0,',','.')?></span><span class="res_descindex"> páginas indexadas</span></span><br>
			<?php
			if($pagstotal>$pasporapi){
				//bien
				?><img src="../images/bien_index.png" /><span class="descimg"> Estado óptimo de indexación</span><?php
			}else{
				//mal
				?><img src="../images/mal_index.png" /><span class="descimg"> Revisar indexación</span><?php
			}
			?>
		</div>
		
	</div>
	
</div>


<!--Columna entera-->
<div class="row">

<!--*****************************************************CONVERSIONES****************************************************-->
	<!--Fila entera-->
	<div class="col-sm-12">

		<h2>Conversiones</h2>

		<!--CONVERSIONES-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">¿Qué son las conversiones?</p>
				<p class="expl_desc text_res">Las conversiones son indicadores numéricos seleccionados que nos ayudan a medir cómo de eficaz es nuestro sitio web.</p>
				<p class="expl_desc text_res">Las conversiones presentadas son “únicas”, es decir, si un usuario en una misma visita realiza dos conversiones (por ejemplo dos descargas de distintos archivos) sólo se cuenta como uno.</p>

			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">
				
				<table class="table table-bordered datatable" style="margin-top:5px;" >
					<thead>
						<tr>
							<th><span class="ndimension"></span></th>
							<th><span class="centrar_dch"><?=$tipo?></span></th>
							<th><span class="centrar_dch"><?=date('Y')?></span></th>
						</tr>	
					</thead>
					<tbody>
					<?php
					$conconv = resumen_datatable_conversiones ( "datatable", $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
					echo $conconv;
					?>
					</tbody>
					<tfoot>
						<tr>
							<th><b>TOTALES</b></th>						
							<?php
							$conconvtot = resumen_datatable_conversiones ( "totales", $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
							echo $conconvtot;
							?>
						</tr>
					</tfoot>
					
				</table>

			</div>
		</div>
		<!--FILA EN CONTENIDO-->

		<!--CONVERSIONES PAIS Y REGIÓN-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">Conversiones según la ubicación del usuario</p>
				<p class="expl_desc text_res">La ubicación presentada se basa en las direcciones IP de los usuarios.  No debe considerarse estos datos como 100% precisos.</p>

			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">

				<!--PAIS-->
				<div class="col-sm-6">
					
					<table class="table table-bordered datatable">
						<thead>
							<tr>
								<th><span class="ndimension">Por país</span></th>
								<th><span class="centrar_dch"><?=$tipo?></span></th>
								<th><span class="centrar_dch"><?=date('Y')?></span></th>
							</tr>	
						</thead>	
						<tbody>
						<?php
						$conpais = resumen_datatable_conversiones_paisregion ( "pais", $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
						echo $conpais;
						?>
						</tbody>
					</table>

				</div>

				<!--REGION-->
				<div class="col-sm-6">
					
					<table class="table table-bordered datatable">
						<thead>
							<tr>
								<th><span class="ndimension">Por región</span></th>
								<th><span class="centrar_dch"><?=$tipo?></span></th>
								<th><span class="centrar_dch"><?=date('Y')?></span></th>
							</tr>	
						</thead>	
						<tbody>
						<?php
						$conregion = resumen_datatable_conversiones_paisregion ( "region", $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
						echo $conregion;
						?>
						</tbody>
					</table>

				</div>

			</div>
		</div>
		<!--FILA EN CONTENIDO-->

	</div>
	<!--Fila entera-->
<!--*****************************************************CONVERSIONES****************************************************-->
<br>
<!--*******************************************************TRÁFICO*******************************************************-->
	<!--Fila entera-->
	<div class="col-sm-12">

		<h2>Tráfico</h2>

		<!--MÉTRICAS HABITUALES-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">Métricas habituales</p>
				<p class="expl_desc text_res"><span class="edsub">Sesiones:</span> una sesión o visita es el conjunto de páginas que un usuario visita en un tiempo determinado.</p>
				<p class="expl_desc text_res"><span class="edsub">Usuarios:</span> personas que han visitado el sitio web de forma única durante el periodo.</p>
				<p class="expl_desc text_res"><span class="edsub">Páginas/Sesión:</span> media de páginas que se visitan en una sesión.</p>
				<p class="expl_desc text_res"><span class="edsub">Rebote:</span> porcentaje de sesiones que sólo han tenido una página vista respecto al total de sesiones.</p><br>
				<p class="expl_desc text_res">El gráfico representa la evolución de las sesiones en el periodo actual y en el periodo anterior</p>

			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">
				
				<!--MÉTRICAS-->
				<div class="col-sm-6">
					
					<table class="table table-bordered datatable">
					<thead>
						<tr>
							<th></th>
							<th><span class="centrar_dch"><?=$tipo?></span></th>
							<th><span class="centrar_dch"><?=date('Y')?></span></th>
						</tr>	
					</thead>	
					<tbody>
					<?php
					$genero = resumen_datatable_trafico ( "trafico", $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
					echo $genero;
					?>
					</tbody>
				</table>

				</div>

				<!--MÉTRICAS GRAFICO-->
				<div class="col-sm-6">
					
					<div class="contgraficos_trafico">

						<div id="curve_chart_ses" style="display:block; width: 100%; margin:auto" ></div>
						<img class="legend maxpos" src="../images/legend_char_sesiones_v2.jpg" />

					</div>

				</div>

			</div>
		</div>
		<!--FILA EN CONTENIDO-->

		<!--FUENTES DE ENTRADA-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">Fuentes de entrada</p>
				<p class="expl_desc text_res">La ubicación presentada se basa en las direcciones IP de los usuarios.  No debe considerarse estos datos como 100% precisos.</p>

			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">

				<!--FUENTES DE ENTRADA-->
				<div class="col-sm-6">
					
					<table class="table table-bordered datatable" >
						<thead>
							<tr>
								<th>Fuentes de entrada</th>
								<th><span class="centrar_dch"><?=$tipo?></span></th>
								<th><span class="centrar_dch"><?=date('Y')?></span></th>
							</tr>	
						</thead>	
						<tbody>
						<?php
						$fe = resumen_datatable_fuentes ( $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
						echo $fe;
						?>
						</tbody>
					</table>

				</div>

				<!--FUENTES DE ENTRADA GRÁFICOS-->
				<div class="col-sm-6">
					
					<div class="contgraficos_trafico">
						<div class="col-sm-6">

							<h3 class="titchart"><?=$tipo?></h3>
							<div class="cajadiv_dispo_imgs" id="trafico_sem_cont">
								<div id="trafico_sem_div" style="display:block; width: 100%; margin:auto"></div>
							</div>

						</div>
						<div class="col-sm-6">

							<h3 class="titchart titchartb"><?=date('Y')?></h3>
							<div class="cajadiv_dispo_imgs">
								<div id="trafico_anno_div" style="display:block; width: 100%; margin:auto"></div>
							</div>

						</div>
						<img class="legend" id="legend_trafico" src="../images/legend_char_trafico.jpg" />
					</div>

				</div>

			</div>
		</div>
		<!--FILA EN CONTENIDO-->
	</div>
	<!--Fila entera-->
<!--*******************************************************TRÁFICO*******************************************************-->

<!--*******************************************************USUARIOS******************************************************-->
	<!--Fila entera-->
	<div class="col-sm-12">

		<h2>Público</h2>

		<!--DE DONDE SON-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">¿De dónde son nuestros usuarios?</p>
				<p class="expl_desc text_res">Presentamos los países desde donde se realizan más sesiones .</p>
		
			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">
				
				<!--DATATABLE-->
				<div class="col-sm-6">
					
					<table class="table table-bordered datatable">
						<thead>
							<tr>
								<th>De qué paises</th>
								<th><span class="centrar_dch"><?=$tipo?></span></th>
								<th><span class="centrar_dch"><?=date('Y')?></span></th>
							</tr>	
						</thead>	
						<tbody>
						<?php
						$paises = resumen_datatable ( "pais", $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
						echo $paises;
						?>
						</tbody>
					</table>
		
				</div>
		
				<!--GRÁFICO-->
				<div class="col-sm-6">
					
					<div class="cajadiv_dispo_imgsb">
						<div id="pais_div" style="display:block; width: 100%; margin:auto"></div>
					</div>
		
				</div>
		
			</div>
		</div>
		<!--FILA EN CONTENIDO-->
		
		<!--GÉNERO-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">Usuarios según su género</p>
				<p class="expl_desc text_res">Los siguientes datos se basan sólo en el XX % de los usuarios que han visitado el sitio web.</p>
		
			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">
		
				<!--DATATABLE-->
				<div class="col-sm-6">
					
					<table class="table table-bordered datatable" >
						<thead>
							<tr>
								<th>Género</th>
								<th><span class="centrar_dch"><?=$tipo?></span></th>
								<th><span class="centrar_dch"><?=date('Y')?></span></th>
							</tr>	
						</thead>	
						<tbody>
						<?php
						$genero = resumen_datatable ( "sexo", $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
						echo $genero;
						?>
						</tbody>
					</table>
				</div>
		
				<!--GRÁFICO-->
				<div class="col-sm-6">
					
					<div class="cajadiv_dispo_imgs charfullpos">
						<div id="sexo_div" style="display:block; width: 100%; margin:auto"></div>
						<img class="legendb" src="../images/char_desc_sexo.jpg" />
					</div>
		
				</div>
		
			</div>
		</div>
		<!--FILA EN CONTENIDO-->
		
		<!--EDADES-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">Edad de nuestros usuarios</p>
				<p class="expl_desc text_res">Los siguientes datos se basan sólo en el XX % de los usuarios que han visitado el sitio web.</p>
		
			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">
		
				<!--DATATABLE-->
				<div class="col-sm-6">
					
					<table class="table table-bordered datatable" >
						<thead>
							<tr>
								<th>Edades</th>
								<th><span class="centrar_dch"><?=$tipo?></span></th>
								<th><span class="centrar_dch"><?=date('Y')?></span></th>
							</tr>	
						</thead>	
						<tbody>
						<?php
						$edad = resumen_datatable ( "edad", $fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
						echo $edad;
						?>
						</tbody>
					</table>
					
				</div>
		
				<!--GRÁFICO-->
				<div class="col-sm-6">
					
					<div class="cajadiv_dispo_imgs charfullpos">
						<div id="edad_div" style="display:block; width: 100%; margin:auto"></div>
						<img class="legendb" src="../images/char_desc_edad.jpg" />
					</div>
		
				</div>
		
			</div>
		</div>
		<!--FILA EN CONTENIDO-->

	</div>
	<!--Fila entera-->
<!--*******************************************************USUARIOS******************************************************-->
<br>
<!--***********************************************INDEXACIÓN**********************************************-->
	<!--Fila entera-->
	<div class="col-sm-12">

		<h2>Indexación</h2>

		<!--CONVERSIONES-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">¿Qué es la indexación en Google?</p>
				<p class="expl_desc text_res">La indexación es el número de páginas de nuestro sitio web que Google “conoce”, es decir, que tiene almacenadas y sobre las que puede buscar.</p>
				<p class="expl_desc text_res">El nivel óptimo de indexación se produce cuando todas las páginas de nuestro sitio web son conocidas por Google.  Para conocer cuántas páginas tiene nuestro sitio web calculamos las páginas únicas visitadas.</p>

			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">
				
				<span class="indexval"><?=number_format($pagstotal,0,',','.')?></span>
				<span class="indexdesc">Media de páginas indexadas por Google </span>
				<div class="filaop">
				<?php
				if($pagstotal>$pasporapi){
					//bien
					?><img src="../images/bien_index.png" /><span class="descimgb"> Estado óptimo de indexación</span><?php
				}else{
					//mal
					?><img src="../images/mal_index.png" /><span class="descimgb"> Revisar indexación</span><?php
				}
				?>
				</div>
				<div class="cajacontline">
					<div id="curve_chart" >
						

					</div>
					<p class="fechas_index"><span class="fecha1 ftm"></span><span class="fecha2 ftm"></span><span  class="fecha3 ftm"></span></p>
					<p class="explindex">Evolución del último año</p>
				</div>

			</div>
		</div>
		<!--FILA EN CONTENIDO-->

	</div>
	<!--Fila entera-->
<!--***********************************************INDEXACIÓN**********************************************-->
<br>
<!--***********************************************CONTENIDOS**********************************************-->
	<!--Fila entera-->
	<div class="col-sm-12">

		<h2>Contenidos</h2>

		<!--CONVERSIONES-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">Secciones más visitadas</p>
				<p class="expl_desc text_res">Presentamos aquellas secciones más vistas por los usuarios.</p>

			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">
				
				<table class="table table-bordered datatable"  style="margin-top:5px;">
					<thead>
						<tr>
							<th>Secciones más vistas</th>
							<th>Idioma</th>
							<th><span class="centrar_dch"><?=$tipo?></span></th>
							<th><span class="centrar_dch"><?=date('Y')?></span></th>
						</tr>	
					</thead>	
					<tbody>
					<?php
					if(ContenidoEnlazado($idVista)){
						//ENLAZADO
						$contenidos = resumen_datatable_indexacion ($fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro, 'todos');

					}else{
						//DESENLAZADO
						$dim = ContenidoEnlazado_emailsemanal($idVista);
						$contenidos = resumen_datatable_indexacion ($fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro, $dim);
					}
					
					echo $contenidos;
					?>
					</tbody>
				</table>

			</div>
		</div>
		<!--FILA EN CONTENIDO-->
		
	</div>
	<!--Fila entera-->
<!--***********************************************CONTENIDOS**********************************************-->
<br>
<!--************************************************IDIOMAS************************************************-->

	<!--Fila entera-->
	<div class="col-sm-12">

		<h2>Idiomas</h2>

		<!--IDIOMAS-->
		<!--FILA EN CONTENIDO-->
		<div class="content_fila">
			<!--COLUMNA DE EXPLICACION-->
			<div class="col_explicacion">
				
				<p class="expl_tit text_res">Idiomas de los usuarios e idiomas del sitio web</p>
				<p class="expl_desc text_res">Es importante identificar qué idiomas de nuestro sitio web leen los usuarios según sea el idioma que hablan.</p>

			</div>
			<!--COLUMNA DE DATOS-->
			<div class="col_datos">
				
				<table class="table table-bordered datatable table_datatable_idioma"  style="margin-top:5px;">
					<thead>
						<?php
						$top = 10;
						$cab = $top+1;
						?>
						<tr>
							<th id="libre_caja"></th>
							<th colspan="<?=$cab?>" class="centrartexto">Idioma de los usuarios</th>
						</tr>
						<tr>
						<th>Idioma web</th>

					<?php
					$filtrob="ga:dimension8!=(not set)";	

					if ($filtro!="no"){
						if(!empty($filtrob)){ $filtrob .= ","; }
						$filtrob .= $filtro;
					}
					//Montamos la table trayendo cabeceras
					   //Arriba
				    $obj_Datos_Api_top = new Datos_Api_Informes(); 
				    $obj_Datos_Api_top -> strProyectoAsociado  = $proyectasco;
				    $obj_Datos_Api_top -> idVista              ='ga:'. $idVista;
				    $obj_Datos_Api_top -> startdate            = $fechaIni;
				    $obj_Datos_Api_top -> enddate              = $fechaFin;
				    $obj_Datos_Api_top -> metrics              = 'ga:pageviews';
				    $obj_Datos_Api_top -> optParams            = array(
																'dimensions' => 'ga:dimension8',
																'sort' => '-ga:pageviews',
																'filters' => $filtrob,
																'start-index' => 0 + 1,
					                                            'max-results' => 50);  

				    $obj_Datos_Api_top -> Construccion();

				    
				    $count = 0;

				    for ($x=1; $x<=$obj_Datos_Api_top->NumValores(); $x++) {

				    	if($count < $top){
				    		echo "<th><span class='centrar_dch'>".$obj_Datos_Api_top->Valor("dimension8",$x)."</span></th>";
				    		$count++;
				    	}else if($count == $top){
				    		echo "<th>Otros</th>";
				    		$count++;
				    	}
				    	
				    }
				    $count=0;
					?>

						</tr>	
					</thead>	
					<tbody>
					<?php
					$idiomas = resumen_datatable_idioma ($proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
					echo $idiomas;
					?>
					</tbody>
				</table>

			</div>
		</div>
		<!--FILA EN CONTENIDO-->

	</div>
	<!--Fila entera-->
<!--*******************************************************IDIOMAS*******************************************************-->
<br>
</div>
<!--Columna entera-->

	<script type="text/javascript">

		//Funcion para el script de geochart
		function drawRegionsMap() {

			//Llamamos al ajax para traer el json del char
			$.ajax({
				type: 'POST',
				url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=todos&strIdioma=todos&strFiltro=" + $("#dat_filtro").val(),
				data: { strFuncion: 'contenido_chart_pais'}
			})
			.done(function (respuesta) {

				var data = google.visualization.arrayToDataTable(eval(respuesta));
				var options = {};
				var chart = new google.visualization.GeoChart(document.getElementById('pais_div'));
				chart.draw(data, options);
				
						
			})

		}

		<?php
		$charts = array('sexo','edad','trafico_sem','trafico_anno');
		foreach ($charts as $key => $cts) {
				
		?>
		//Funcion para el script de piechart
		function drawChart_<?=$cts?>(straux) {

			$.ajax({
				type: 'POST',
				url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFiltro=" + $("#dat_filtro").val(),
				data: { strFuncion: 'resumen_piechart_'+straux}
			})
			.done(function (respuesta) {

				var objJson = jQuery.parseJSON(respuesta);
				//alert(objJson.colores)

				var data = google.visualization.arrayToDataTable(eval(objJson.llamada));


				//var options = eval(objJson.colores);
				var options = {
			       	title: '',
			        is3D: true,
			        legend: 'none',
			        slices: //objJson.colores
			        <?php
			    	switch ($cts) {
						case "trafico_sem":
							$drawcolors = resumen_chart_trafico($proyectasco, $idVista, $fechaIni, $fechaFin, $filtro, 0, 1);
							$tam = 185;
							break;
						case "trafico_anno":
							$drawcolors = resumen_chart_trafico($proyectasco, $idVista, $fechaIni, $fechaFin, $filtro, 1, 1);
							$tam = 185;
							break;
						case "sexo":
							$drawcolors = resumen_piechart("sexo", $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro, 1);
							$tam = 240;
							break;
						case "edad":
							$drawcolors = resumen_piechart("edad", $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro, 1);
							$tam = 240;
							break;
					}
					echo $drawcolors;
			        ?>
			        ,
		          	heigth: <?=$tam?>,
		          	width:  <?=$tam?>

			    };
			    var chart = new google.visualization.PieChart(document.getElementById(straux+'_div'));
			    chart.draw(data, options);
		    })//done
		 }

		 <?php } ?>

		 function lineChart_ses() {
		     var data = google.visualization.arrayToDataTable([
		       ['Día', 'Periodo Actual', 'Periodo anterior'],
		       	<?php
		      	$chartses = resumen_linechart_ses ($fechaIni_ant, $fechaFin_ant, $proyectasco, $idVista, $fechaIni, $fechaFin, $filtro);
				echo $chartses
				?>
		     ]);

		     var options = {
		       title: '',
		       legend: 'none'
		     };

		     var chart = new google.visualization.LineChart(document.getElementById('curve_chart_ses'));

		     chart.draw(data, options);
		   }


		   function lineChart() {
		     var data = google.visualization.arrayToDataTable([
		     	['Fecha', 'Páginas indexadas'],
		     	<?php
		
					$fecha1 = "";
					$fecha2 = "";
					$fecha3 = "";

					$fechaCom = $fechaIni;
					$fechaCom_manno = strtotime ( '-1 year' , strtotime ( $fechaCom ) ) ; 
					$fechaCom_manno = date ( 'Y-m' , $fechaCom_manno );
					$fechaCom_ano = date("Y", strtotime($fechaCom_manno)); 
					$fechaCom_mes = date("m", strtotime($fechaCom_manno)); 
					$fechaCom_dia = date("d", strtotime($fechaCom_manno)); 
					//Aqui tenemos la fecha del año anterior
					//Recorremos 12 veces hasta llegar la fecha inicial y en la 6 pintamos cabecera
					$fechaCom_manno = $fechaCom_ano."-".$fechaCom_mes."-01";
					$mes_format = date("m", strtotime($fechaCom_manno)); 
					$mes_trac = traducirMes($mes_format);
					$nfecha = $fechaCom_ano."-".$mes_trac[intval($mes_format)]["nom"];
					$nfechab =  ucwords($mes_trac[intval($mes_format)]["nom"])." ".$fechaCom_ano;
					$fechacomp_ini = $fechaCom_ano."-".$fechaCom_mes."-01";
					$diasMes = cal_days_in_month(CAL_GREGORIAN, $fechaCom_mes, $fechaCom_ano);
					$fechacomp_fin = $fechaCom_ano."-".$fechaCom_mes."-".$diasMes;
					//parche
					$fecha1 = $nfechab;
					$nfecha = "";
					$linechartData = "['".$nfecha ."',".pagsIndexadasMes($idVista,$fechacomp_ini,$fechacomp_fin)."],";
					//echo $fechaCom_manno."-cab<br>";
					$count = 0;
					while ($count < 11) {

						$fechaCom_mes++;
						if($fechaCom_mes > 12){
							$fechaCom_mes = 1;
							$fechaCom_ano++;
						}
						$nfecha = $fechaCom_ano."-".$fechaCom_mes."-01";
						$mes_format = date("m", strtotime($nfecha)); 
						$mes_trac = traducirMes($mes_format);
						$nfecha = $fechaCom_ano."-".$mes_trac[intval($mes_format)]["nom"];
						$nfechab =  ucwords($mes_trac[intval($mes_format)]["nom"])." ".$fechaCom_ano;
						$fechacomp_ini = $fechaCom_ano."-".$fechaCom_mes."-01";
						$diasMes = cal_days_in_month(CAL_GREGORIAN, $fechaCom_mes, $fechaCom_ano);
						$fechacomp_fin = $fechaCom_ano."-".$fechaCom_mes."-".$diasMes;
						if($count == 5){
							//parche
							$fecha2 = $nfechab;
							$nfecha = "";
							$linechartData .= "['".$nfecha."',".pagsIndexadasMes($idVista,$fechacomp_ini,$fechacomp_fin)."],";
						}else{
							$linechartData .= "['',".pagsIndexadasMes($idVista,$fechacomp_ini,$fechacomp_fin)."],";
						}
						
						//echo $fechaCom_ano."-".$fechaCom_mes."<br>";
						$count++;

					}

					$fechaCom_mes++;
					if($fechaCom_mes > 12){
						$fechaCom_mes = 1;
						$fechaCom_ano++;
					}
					$nfecha = $fechaCom_ano."-".$fechaCom_mes."-01";
					$mes_format = date("m", strtotime($nfecha)); 
					$mes_trac = traducirMes($mes_format);
					$nfecha = $fechaCom_ano."-".$mes_trac[intval($mes_format)]["nom"];
					$nfechab =  ucwords($mes_trac[intval($mes_format)]["nom"])." ".$fechaCom_ano;
					$fechacomp_ini = $fechaCom_ano."-".$fechaCom_mes."-01";
					$diasMes = cal_days_in_month(CAL_GREGORIAN, $fechaCom_mes, $fechaCom_ano);
					$fechacomp_fin = $fechaCom_ano."-".$fechaCom_mes."-".$diasMes;
					$fechacomp_ini = $fechaCom_ano."-".$fechaCom_mes."-01";
					$fechacomp_fin = $fechaCom_ano."-".$fechaCom_mes."-".$diasMes;

					//parche
					$fecha3 = $nfechab;
					$nfecha = "";
					$linechartData .= "['".$nfecha."',".pagsIndexadasMes($idVista,$fechacomp_ini,$fechacomp_fin)."]";

					//echo $fechaIni."-cab<br>";

					echo $linechartData;


				?>
		     ]);

		     var options = {
		       title: '',
		       legend: 'none',
		       colors:['#ff9a02']
		     };

		     var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

		     chart.draw(data, options);

		     //Pintamos las fecha
		     $(".fecha1").text("<?=$fecha1?>");
		     $(".fecha2").text("<?=$fecha2?>");
		     $(".fecha3").text("<?=$fecha3?>");


		   }


		//Funcion que carga todos los graficos neccesarios 
		function cargar_cabeceras_ini(){
			//Comparacion_rango_fechas();
			//Inicializar_datatables();
			drawRegionsMap();
			drawChart_sexo("sexo");
			drawChart_edad("edad");
			drawChart_trafico_sem("trafico_sem");
			drawChart_trafico_anno("trafico_anno");
			lineChart();
			lineChart_ses();
		}

		google.load("visualization", "1", {
			packages:["geochart"],
			packages:["corechart"]
		});
		google.setOnLoadCallback(cargar_cabeceras_ini);


	</script>

<?php }else{
?>
<div class="main-content errorcontainer" id="contenedor_resumen">
	<p>¡Error! Debes enviar datos basicos por URL.</p><br>
	<table class="table table-striped">
	  <tr>
	  	<th>Descripción</th>
	  	<th>Parámetro(ejemplo)</th>
	  	<th>Obligatorio</th>
	  </tr>
	  <tr>
	  	<td class="centrar_izqi" >Id de la vista de Analytics</td>
	  	<td class="centrar_izqi" >&vista=idvista</td>
	  	<td class="centrar_izqi" >SÍ</td>
	  </tr>
	  <tr>
	  	<td>Fecha del periodo inicial (Formato: AAAA-MM--DD)</td>
	  	<td class="centrar_izqi" >&fechaini=fechainicial</td>
	  	<td class="centrar_izqi" >SÍ</td>
	  </tr>
	  <tr>
	  	<td>Fecha del periodo final (Formato: AAAA-MM--DD)</td>
	  	<td class="centrar_izqi" >&fechafin=fechafinal</td>
	  	<td class="centrar_izqi" >SÍ</td>
	  </tr>
	  <tr>
	  	<td>Fecha del perido inicial para comparación (Formato: AAAA-MM--DD)</td>
	  	<td class="centrar_izqi" >&fechainiant=fechainicial</td>
	  	<td class="centrar_izqi" >SÍ</td>
	  </tr>
	  <tr>
	  	<td>Fecha del periodo final para comparación (Formato: AAAA-MM--DD)</td>
	  	<td class="centrar_izqi" >&fechafinant=fechafinal</td>
	  	<td class="centrar_izqi" >SÍ</td>
	  </tr>
	  <tr>
	  	<td>Tipo de informe (Semana o Mes). Por defecto: 'Semana'</td>
	  	<td class="centrar_izqi" >&tipo=semanaOmes</td>
	  	<td class="centrar_izqi" >NO</td>
	  </tr>
	  <tr>
	  	<td>Filtro personalizado (Los == deben sustituirse por **). Por defecto: 'no'</td>
	  	<td class="centrar_izqi" >&filtro=ga:country**Spain</td>
	  	<td class="centrar_izqi" >NO</td>
	  </tr>
	  <tr>
	  	<td>Nombre del filtro personalizado. Por defecto: 'Ningún filtro'</td>
	  	<td class="centrar_izqi" >&filtrodesc=NombreFiltro</td>
	  	<td class="centrar_izqi" >NO</td>
	  </tr>
	</table>
</div>
<?php
} ?>

<!--<script src="<?=RUTA_ABSOLUTA?>js/resumen_scripts.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php") ?>



<?php
	include("includes/conf.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>DMIntegra | <?php $trans->__('Error'); ?></title>
	

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">
	<link rel="stylesheet" href="css/estilos.css">

	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<script src="js/translation.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<!--Estilos-->
	
	
</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<h1>Error</h1>
	<?php
		if( isset($_GET["error"]) ){

				switch ($_GET["error"]) {
					case 1:
						$errorDesc = 'El usuario con el que has iniciado sesión no tiene ninguna vista asociada. Hable con el administrador para que resuelva la situación.';
						break;
					case 2:
						setcookie("filtroper", "", time() - 3600, "/");
						setcookie("filtroperdesc", "", time() - 3600, "/");
						setcookie("ididioma", "", time() - 3600, "/");
						setcookie("idioma", "", time() - 3600, "/");
						setcookie("idpais", "", time() - 3600, "/");
						setcookie("pais", "", time() - 3600, "/");

						setcookie("vista", "", time() - 3600, "/");
						setcookie("proyectoasociado", "", time() - 3600, "/");
						$errorDesc = 'No tiene permisos para ver esa vista.';
						break;
					
					default:
						$errorDesc = 'Ha habido un error interno';
						break;
				}

			?>
			<p><?=$errorDesc?></p>
			<?php
		}else{
			?>
			<p>Ha habido un error interno</p>
			<?php
		}
	?>


	<!-- Bottom Scripts -->
	<script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="js/index_scripts.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>
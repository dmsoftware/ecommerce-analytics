<?php
  require_once("includes/lang/class.translation.php");


  $file = "informes/funciones_ajax.php";
  extractFileData($file, $trans);

  function extractFileData($file, $trans){
    $source = file_get_contents($file);
    $tmp = explode('.', $file);
    $type = $tmp[count($tmp) - 1];

    $trans->extract($source, $type);
  }
  
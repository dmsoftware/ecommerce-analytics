var table_datatable_clientes;

$(document).ready(function(){

  table_datatable_clientes = $('#table_datatable_clientes').dataTable({
    "language": {
      "url": "assets/js/datatables/spanish.txt"
    },
    "paging": true,
    "lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
    "ordering": true,
    "order": [[0,'desc']],
    //Exportaciones 
    "dom": 'frtT <ilp >',
    /*"dom": 'frtT ilp ',*/
    "tableTools": {
           "aButtons": [
              {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='images/pdf_ico.png'/>"
              },
              /*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
              },*/
              {
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='images/csv_ico.png'/>"
              },

           ],
           "sSwfPath": "js/copy_csv_xls_pdf.swf"
       },
    "columns":[
      null,
      {"orderable": false },
      {"orderable": false },
      {"orderable": false },
      {"orderable": false },
      {"orderable": false }
      ],
    "processing": true,     
    "serverSide": true,
    "ajax": {
      "type": "POST",
      "url":  "ajax/informes_ajax.php",
      "data": {"strFuncion": 'datatatable_clientes'}
    }   

  });//Fin conf datatable


$("#opcclose").on("click",function(){
  $(".caja_opcs").hide();
    $("#opcclose").hide()
})

})//Fin ready

function opcMostrar(id){
  $(".caja_opcs").stop();
  if($("#opc_"+id).css("display")=="none"){
    $(".caja_opcs").hide();
    $("#opc_"+id).fadeIn(300)
    $("#opcclose").show()
  }else{
    $(".caja_opcs").hide();
    $("#opcclose").hide()
  }
}
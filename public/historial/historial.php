﻿<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side_historial.php"); 
	
?>
<style type="text/css">
body {	font-family: "Helvetica Neue", Helvetica, "Noto Sans", sans-serif, Arial, sans-serif; font-size: 12px; background-color:#f3f5f7;}
table {	font-size: 12px;}
.izquierda {	text-align: left;	display: block;}
.btn_vercampana {	cursor: pointer;}
.agrupacion {	background-color: #f1f1f1;}
.total {	background-color: #E1EFFC;}
.subtotal {	background-color: #EAEAEA;	font-style: italic;	border-bottom: 1px solid rgb(203, 203, 203) !important}
.porcDT { color: #999; font-size: 11px; font-style: italic; display: block; }
tr.filaDirecto {
	background: #c8bcdb; text-align: left; 
}
tr.filaSeo {
	background-color:#ffc1ba; text-align: right; 
}
tr.filaEnlaces {
	background-color:#b8f3fe; text-align: right; 
}
tr.filaSocial {
	background-color:#fde5a7; text-align: right; 
}
tr.filaTotal {
	background-color:#dedede; text-align: right; 
}
tr.filaDirecto td.primera {
	border-left: solid 10px #7D5FA9;
}
tr.filaSeo td.primera {
	border-left: solid 10px #FE6B5B;
	text-align: left;
}
tr.filaEnlaces td.primera {
	border-left: solid 10px #01D5FB;
	text-align: left;
}
tr.filaSocial td.primera {
	border-left: solid 10px #FBC63C;
	text-align: left;
}
tr.filaTotal td.primera { 
	border-left: solid 10px #cdcdcd; 
	text-align: left;
}


table#table_datatable_campanas tr th{ border-top: solid 1px #6cb5f4; background: #6cb5f4; color: #fff; }
table#table_datatable_campanas_ecomerce tr th { border-top: solid 1px #6cb5f4; background: #6cb5f4; color: #fff; }

.par td{ border-top: solid 1px #b4b4b4; }
.impar td { border-top: solid 1px #b4b4b4; }
td.agrupacion{ border-top: none; }
td.total { border-top: none; }

.imagensearch{width: 21px; margin: auto; display: block;}

table#table_datatable_campanas tr.par td.primera { border-left: solid 10px #4992d0; text-align: left; }
table#table_datatable_campanas_ecomerce tr.par td.primera { border-left: solid 10px #4992d0; }
table#table_datatable_campanas tr.impar td.primera{ border-left: solid 10px #6cb5f4; text-align: left; }
table#table_datatable_campanas_ecomerce tr.impar td.primera { border-left: solid 10px #6cb5f4; }

table#table_datatable_campanas tr:first-child{border-left: solid 10px #6cb5f4;}
table#table_datatable_campanas_ecomerce tr:first-child{border-left: solid 10px #6cb5f4;}

table#table_datatable_campanas tr th.primera{ border-left: solid 10px #6cb5f4; }
table#table_datatable_campanas_ecomerce tr th.primera {border-left: solid 10px #6cb5f4;}
table#table_datatable_campanas tr.nodetectadas{border-left:solid 10px #959595}
table#table_datatable_campanas_ecomerce tr.nodetectadas{border-left:solid 10px #959595}

.desc{ background-color: #B0B0B0;text-align: left;}
.desc td{ color: white !important; border-left: solid 10px #969696 !important; text-align: left;  }
#table_datatable_campanas img{ display: block;}
#table_datatable_campanas_ecomerce img{ display: block;}

.conjcampanas_cont{padding-top: 30px; padding-left: 16px; padding-right: 15px;}
</style>

<div class="main-content">

<p id="txt_propiedad"><?=$nombre_vista?></p>
<p id="txt_vistaurl"> <span id="txt_vista_br"><?=$nvista?></span> - <span id="txt_url_br">www.<?=$strDominio?></span></p>
<p id="cab_historial_tit"><?=$hist_asunt_pri?></p>

<br />

<style type="text/css">
	

</style>

<div class="row">

	<div class="contenedor_informse">
		<div class="rtet">
			<span class="btnpdf" id="btn_pdf" data-proceso="<?=$proceso?>" data-procesoimagen="<?=$procesoImagen?>" data-imagen="<?=$imagen?>" ><img width= "45px"  src="../images/pdf_download.png" /></span>
			<div class="conjinformes" style="display:<?=$displaya?>">
				<div class="precarga"><img class="loadgif" src="../images/load.gif" /></div>
				<img id="parteImg1" class="imgPartes" src="<?=$imgp1?>" width="800" />
				<img id="parteImg2" class="imgPartes" src="<?=$imgp2?>" width="800" />
				<img id="parteImg3" class="imgPartes" src="<?=$imgp3?>" width="800" />
				<img id="parteImg4" class="imgPartes" src="<?=$imgp4?>" width="800" />
			</div>
			<div class="conjcampanas" style="display:<?=$displayb?>">
				<div class="precarga"><img class="loadgif" src="../images/load.gif" /></div>
				<div class="conjcampanas_cont"><br style="display:block"><br style="display:block"><?php

				$pos = strripos($cuerpo, '<div class="table_campanas">');
				$posb = strripos($cuerpo, '<br id="cortehistorial">');
				
				$pruebab = substr($cuerpo, $pos,$posb-2);
 
				echo "<div class='cajahist'>".$pruebab."</div>";
				

				?>
				</div>
			</div>
		</div>
		
	</div>

</div>

<style type="text/css">
	.conjcampanas_cont p{ display: none; }
	.conjcampanas_cont img{ display: none; }
	.conjcampanas_cont br{ display: none; }
	.loadgif{width: 200px;
			position: absolute;
			left: 50%;
			margin-left: -100px;
			margin-top: 300px;}
	.precarga{ position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; background-color: rgba(255, 255, 255, 0.46);; display: none}
	.conjinformes{ width: 808px; margin: auto; display: block; border: 1px solid #A7A1A1; position: relative;
padding: 6px;
box-shadow: 2px 5px 7px rgba(0, 0, 0, 0.26); overflow: auto;}
.conjcampanas{ width: 914px; margin: auto; display: block; border: 1px solid #A7A1A1; position: relative;
padding: 6px;
box-shadow: 2px 5px 7px rgba(0, 0, 0, 0.26); overflow: auto;}
	.btnpdf{ float: right; display: block; overflow: auto;margin-bottom: 10px; cursor: pointer; padding: 3px;position: absolute;
top: -50px;right: 0px;}
	.btnpdf:hover{ border-radius: 7px; background-color: #dedede;}
	.rtet{ width: 808px; display: block;overflow: auto;position: relative;
overflow: initial;}
table tr{ display: block;}
#table_datatable_campanas tr{ display: table-row !important;}
/*.page-container .main-content { padding: 0px;}*/
</style>
<script type="text/javascript">
	$(document).ready(function(){

	



		$("#btn_pdf").on("click",function(){
			
			$(".precarga").fadeIn(600)
			var proceso = $(this).attr("data-proceso");
			var imagen = $(this).attr("data-imagen");
			var procesoimg = $(this).attr("data-procesoimagen")

			$.ajax({
		        type: 'POST',
		        url: 'historial_ajax_pdf.php',
		        data: {
		          proc:    proceso,
		          procimg: procesoimg,
		          img:     imagen    
		          },
		        dataType: 'text',
		        success: function(data){
		        	//alert(data);
		        	$("#btn_pdf").attr("data-imagen",data);

		        	$(".precarga").fadeOut(400,function(){
		        		//$(this).attr("id","btn_pdf")
		        		$("li[data-proceso='"+proceso+"']").attr("data-imagen",data);
		        		
		        		//alert(data);
		        		self.location = data;
		        	})
		          
		          },
		        error: function(data){
		          	alert('Error: '+data);
		        }
		    })
		})
	})
</script>

<script src="<?=RUTA_ABSOLUTA?>js/permisos_scripts.js"></script>	
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



<?php include("../includes/conf.php");
	
ini_set('max_execution_time', 720); //300 seconds = 5 minutes
ini_set('memory_limit', '300M');

/*** CONF ***/
define('RUTA', 'http://informes.acc.com.es/_htmlToPdfService/htmlToPdf.svc?singleWsdl');
require_once("../../admin/Envios/nusoap/nusoap.php");
	
if( isset($_POST["proc"]) || isset($_POST["procimg"]) || isset($_POST["img"]) ){

	$file = $_POST["img"];

	if(empty($_POST["img"])){
		$file_headers[0] = 'HTTP/1.1 500 Internal Server Error';
	}else{
		$file_headers = @get_headers($file);
	}
	
	//echo $file_headers[0];HTTP/1.1 500 Internal Server Error
	if($file_headers[0] != 'HTTP/1.1 500 Internal Server Error' AND $_POST["img"] != "" ) {
		//EXISTE EL PDF
	    echo $_POST["img"];
	    //echo $file_headers[0];
	    //echo $file_headers[0]." -Existe";
	
	}else {
		//NO EXISTE EL PDF POR LO QUE HAY QUE GENERARLO
	    
		$client = new SoapClient(RUTA);
		$client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		//Partes a dividir la imagen
		$division_img = 1;

		$values = array(
		    "domain" => 'Dmintegra.acc.com.es', 
		    "user" => '', 
		    "baseUrl" => '', 
		    "documentName" => 'img_'.$_POST["proc"], 
		    "urlToPrint" => ''.$_POST["procimg"].'&pdf=pdf', 
		    "htmlToPrint" => '', 
		    "headerUrl" => '../admin/envios/estructura_email/estructura_email_cabecera.php', //url cabecera
		    "footerUrl" => '../admin/envios/estructura_email/estructura_email_pie.php', //url pie
		    "footerPaginationHtml" => '<p style=" width:50%; float:left; margin:0 0 10px 2%; text-align: left; font-family: arial; font-size: 13px; color: #777777; margin-top:80px">&nbsp;&nbsp;Página <strong>&p;</strong> de <strong>&P;</strong> páginas</p>', //string paginación
		    "printOutput" => 1, 
		    "pdfOrientation" => 0, 
		    "imageMaxWidth" => 900,
		    "imageSlides" => $division_img,
		    "htmlViewerWidth" => 900,
		    "conversionDelay" => 5
		);
		$result = $client->createDocument($values);
		$resultados = $result->createDocumentResult;
		
		if(empty($resultados->Error)){
			$pdf_generado = $resultados->Path;

			guardarPdf($_POST["proc"],$pdf_generado);

			echo $pdf_generado;
		}else{
			$pdf_generado = "";
			echo $resultados->ErrorMessage."<br>";
		}
		/*echo $file_headers[0] ." -No existe";*/

	}	

}


/*if($division_img > 1){
	$pdf_generado_array = array();
	for ($i=1; $i <= 4 ; $i++) { 
		$pdf_generado_array[$i] = str_replace(".png", "-".$i.".png", $pdf_generado);
	}
}*/
?>

<?php 
 //Estilos en estilos_head
 $rutacomparacion = substr( $_SERVER['PHP_SELF'], 8, 13 );
 if($rutacomparacion != "configuracion"){ 

 	if(empty($nombre_vista)){
 		if($primera_vista != ''){
	 		$buscaVista = propiedadVista($primera_vista);
	 		foreach ($buscaVista["elementos"] as $key => $v) {
	 			$nombre_vista = $v["nombre_propiedad"];
	 		}
 		}
 	}
	$thisUrl = $_SERVER['REQUEST_URI'];
 	?>
	<p id="txt_propiedad"><i class="fa fa-<?=$appico?> icoappptit"></i> <?=$nombre_vista?></p>
	<?php
	if($appico != 'mobile'){
			$vvurl = '<span id="txt_url_br">'.$strDominio.'</span> -';
		}else{
			$vvurl = "";
		}
	?>
	<p id="txt_vistaurl"><?=$vvurl?><span id="txt_vista_br"><?=$nombre_vistab?></span></p>

	<?php
	//ELiminamos fecha si esta en comparativa
	$posicion_coincidencia = strpos($thisUrl, "/campanas_comparativa.php");
	if ($posicion_coincidencia == false){ 
	?>
		<p id="txt_fecha"></p>
	<?php } ?>

	<?php if($pageuser != "campanas" AND $pageuser != "ecommerce" AND $pageuser != "seo"){ ?>
	<div class="caja_filtros">
		<span class="filtropart" id="caja_br_pais">   <i class="fa fa-map-marker"></i> <span id="info_pais"><?=$trans->__($pais); ?></span> </span>
		<span class="filtropart" id="caja_br_idioma"> <i class="fa fa-globe"></i> <span id="info_idioma"><?=$trans->__($idioma); ?></span> </span>
		<span class="filtropart" id="caja_br_filtro"> <i class="fa fa-filter"></i> <span id="info_filtro"><?=$trans->__($filtroperdesc); ?></span> </span>	
	</div>
	<?php } ?>
	<!--Parte de seguimiento de ruta-->
	<div class="contenedor_ruta">

		
		<?php
		if($pageuser=="ecommerce"){
				?>
			<ul class="<?=$classul?>">
				<?php
				$posicion_coincidencia = strpos($thisUrl, "/campanas_informe_pedidos.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/ecommerce/campanas_informe_pedidos.php"';
					$corta= "corta";
				}else{
					
					$class ="";
					$img ="";
					$href = 'href="../public/ecommerce/campanas_informe_pedidos.php"';
					$corta= "";							
				}				
				?>
				<li>
					<a class=" <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">1</span><span class="amplio<?=$corta?>">1. <?php echo $trans->__('INFORME DE PEDIDOS',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png">
					</a>
				</li>
				<?php
				$posicion_coincidencia = strpos($thisUrl, "/campanas_informe_productos.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/ecommerce/campanas_informe_productos.php"';
					$corta= "corta";
				}else{
					
					$class ="";
					$img ="";
					$href = 'href="../public/ecommerce/campanas_informe_productos.php"';
					$corta= "";							
				}				
				?>
				<li>
					<a class=" <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">2</span><span class="amplio<?=$corta?>">2. <?php echo $trans->__('INFORME DE PRODUCTOS',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png">
					</a>
				</li>
				<?php
				$posicion_coincidencia = strpos($thisUrl, "/campanas_embudos_pedidos.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/ecommerce/campanas_embudos_pedidos.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$corta = "";
					$href = 'href="../public/ecommerce/campanas_embudos_pedidos.php"';
				}
				?>
				<li>
					<a class=" <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">3</span><span class="amplio<?=$corta?>">3. <?php echo $trans->__('EMBUDOS DE CONVERSIÓN DE PEDIDOS',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png">
					</a>
				</li>

				<?php
				$posicion_coincidencia = strpos($thisUrl, "/campanas_embudos_productos.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/ecommerce/campanas_embudos_productos.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$corta = "";
					$href = 'href="../public/ecommerce/campanas_embudos_productos.php"';
				}
				?>
				<li>
					<a class=" <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">3</span><span class="amplio<?=$corta?>">4. <?php echo $trans->__('EMBUDOS DE CONVERSIÓN DE PRODUCTOS',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png">
					</a>
				</li>
				<?php

				$posicion_coincidencia = strpos($thisUrl, "/campanas_resumen.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/campanas/campanas_resumen.php"';
					$corta= "corta";
				}else{
					$posicion_coincidencia = strpos($thisUrl, "/informes/");
					if ($posicion_coincidencia != false){ 
						$class ="sel";
						$img ="_sel";
						$href = 'href="../public/campanas/campanas_resumen.php"';
						$corta= "corta";
					}else{
						$class ="";
						$img ="";
						$corta = "";
						$href = 'href="../public/campanas/campanas_resumen.php"';
					}
				}
				?>
				<li>
					<a class="<?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">4</span><span class="amplio<?=$corta?>">5. <?php echo $trans->__('LISTADO DE CAMPAÑAS',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png" />
					</a>
				</li>
				<?php
				$posicion_coincidencia = strpos($thisUrl, "/campanas_comparativa.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/campanas/campanas_comparativa.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$corta = "";
					$href = 'href="../public/campanas/campanas_comparativa.php"';
				}
				?>
				<li>
					<a class="ult <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">5</span><span class="amplio<?=$corta?>">6. <?php echo $trans->__('OPTIMIZACIÓN DE CAMPAÑAS',false); ?></span> 
						<!--<img src="../public/images/menu_cajetilla<?=$img?>.png">-->
					</a>
				</li>
			</ul>
		<?php
		}
		?>

		<?php
		if($pageuser=="campanas"){
				?>
			<ul class="<?=$classul?>">
				<?php

				$posicion_coincidencia = strpos($thisUrl, "/campanas_resumen.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/campanas/campanas_resumen.php"';
					$corta= "corta";
				}else{
					$posicion_coincidencia = strpos($thisUrl, "/informes/");
					if ($posicion_coincidencia != false){ 
						$class ="sel";
						$img ="_sel";
						$href = 'href="../public/campanas/campanas_resumen.php"';
						$corta= "corta";
					}else{
						$class ="";
						$img ="";
						$corta = "";
						$href = 'href="../public/campanas/campanas_resumen.php"';
					}
				}
				?>
				<li>
					<a class="<?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">1</span><span class="amplio<?=$corta?>">1. <?php echo $trans->__('LISTADO DE CAMPAÑAS',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png" />
					</a>
				</li>
				<?php
				$posicion_coincidencia = strpos($thisUrl, "/campanas_comparativa.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/campanas/campanas_comparativa.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$corta = "";
					$href = 'href="../public/campanas/campanas_comparativa.php"';
				}
				?>
				<li>
					<a class="ult <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">2</span><span class="amplio<?=$corta?>">2. <?php echo $trans->__('COMPARATIVA DE CAMPAÑAS',false); ?></span> 
						<!--<img src="../public/images/menu_cajetilla<?=$img?>.png">-->
					</a>
				</li>
			</ul>
		<?php
		}
		?>


		<!--quitamos el nombre de campaña y el menu de migas si estamos en la página principal de campañas-->
		<?php
			$posicion_coincidencia = strpos($thisUrl, "as_resumen.php");
			$posicion_coincidenciab = strpos($thisUrl, "campanas_comparativa.php");
			$posicion_coincidenciac = strpos($thisUrl, "campanas_informe_pedidos.php");
			$posicion_coincidenciad = strpos($thisUrl, "campanas_informe_productos.php");
			$posicion_coincidenciae = strpos($thisUrl, "campanas_embudos_pedidos.php");
			$posicion_coincidenciaf = strpos($thisUrl, "campanas_embudos_productos.php");
			//echo "es: ".$thisUrl;
			if ($posicion_coincidencia == false AND $posicion_coincidenciab == false AND $posicion_coincidenciac == false AND $posicion_coincidenciad == false AND $posicion_coincidenciae == false AND $posicion_coincidenciaf == false){
		?>

		<!--Comprobamos si hay sesion con campañas y si la hay escribimos el nombre de la campaña-->
		<?php
		if($pageuser=="campanas" || $pageuser == "ecommerce"){
			?><div class="subcamapana"><?php
			if(isset($_COOKIE["pagecampana"])){
				?><p class="nombrecampana"><?=$_COOKIE["pagecampana"]?></p><?php
				$classul="ulcamp";
			}else{
				$trans->__('Error no hay campaña');
				$classul="";
			}
		}

		if($pageuser != "seo"){ ?>
		

		<ul class="<?=$classul?>">
			<?php
			$posicion_coincidencia = strpos($thisUrl, "/metricas_habituales.php");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$img ="_sel";
				$href = 'href="../public/informes/metricas_habituales.php"';
				$corta= "corta";
			}else{
				$class ="";
				$img ="";
				$href = 'href="../public/informes/metricas_habituales.php"';
				$corta= "";
			}				
			?>
			<li>
				<a class=" <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">1</span><span class="amplio<?=$corta?>">1. <?php echo $trans->__('TRÁFICO',false); ?></span> 
					<img src="../public/images/menu_cajetilla<?=$img?>.png">
				</a>
			</li>
			<?php
			$posicion_coincidencia = strpos($thisUrl, "/caracteristicas_usuarios.php");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$img ="_sel";
				$href = 'href="../public/informes/caracteristicas_usuarios.php"';
				$corta= "corta";
			}else{
				$class ="";
				$img ="";
				$corta = "";
				$href = 'href="../public/informes/caracteristicas_usuarios.php"';
			}
			?>
			<li>
				<a class=" <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">2</span><span class="amplio<?=$corta?>">2. <?php echo $trans->__('PÚBLICO',false); ?></span> 
					<img src="../public/images/menu_cajetilla<?=$img?>.png">
				</a>
			</li>
			<?php
			$posicion_coincidencia = strpos($thisUrl, "/geo-localizacion.php");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$img ="_sel";
				$href = 'href="../public/informes/geo-localizacion.php"';
				$corta= "corta";
			}else{
				$class ="";
				$img ="";
				$corta = "";
				$href = 'href="../public/informes/geo-localizacion.php"';
			}
			?>
			<li>
				<a class=" <?=$class?>" <?=$href?>  ><span class="estrecho<?=$corta?>">3</span><span class="amplio<?=$corta?>">3. <?php echo $trans->__('GEO-LOCALIZACIÓN',false); ?></span> 
					<img src="../public/images/menu_cajetilla<?=$img?>.png">
				</a>
			</li>
			<?php
			$posicion_coincidencia = strpos($thisUrl, "/idiomas.php");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$img ="_sel";
				$href = 'href="../public/informes/idiomas.php"';
				$corta= "corta";
			}else{
				$class ="";
				$img ="";
				$corta = "";
				$href = 'href="../public/informes/idiomas.php"';
			}
			?>
			<li>
				<a class=" <?=$class?>" <?=$href?>  class=""><span class="estrecho<?=$corta?>">4</span><span class="amplio<?=$corta?>">4. <?php echo strtoupper($trans->__('Idiomas',false)); ?></span> 
					<img src="../public/images/menu_cajetilla<?=$img?>.png">
				</a>
			</li>
			<?php
			$posicion_coincidencia = strpos($thisUrl, "/contenido_visto.php");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$img ="_sel";
				$href = 'href="../public/informes/contenido_visto.php"';
				$corta= "corta";
			}else{
				$class ="";
				$img ="";
				$corta = "";
				$href = 'href="../public/informes/contenido_visto.php"';
			}
			?>
			<li>
				<a class=" <?=$class?>" <?=$href?>  class=""><span class="estrecho<?=$corta?>">5</span><span class="amp<?=$corta?>lio">5. <?php echo strtoupper($trans->__('Contenidos',false)); ?></span> 
					<img src="../public/images/menu_cajetilla<?=$img?>.png">
				</a>
			</li>
			<?php
			if($primera_app == 0){
				$posicion_coincidencia = strpos($thisUrl, "/navegacion_dispositivos.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/informes/navegacion_dispositivos.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$corta = "";
					$href = 'href="../public/informes/navegacion_dispositivos.php"';
				}
				?>
				<li>
					<a class=" <?=$class?>" <?=$href?>  class=""><span class="estrecho<?=$corta?>">6</span><span class="amplio<?=$corta?>">6. <?php echo strtoupper($trans->__('Dispositivos',false)); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png">
					</a>
				</li>
			<?php
			}else{
				$posicion_coincidencia = strpos($thisUrl, "/aplicacion_dispositivos.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/informes/aplicacion_dispositivos.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$corta = "";
					$href = 'href="../public/informes/aplicacion_dispositivos.php"';
				}
				?>
				<li>
					<a class=" <?=$class?>" <?=$href?>  class=""><span class="estrecho<?=$corta?>">6</span><span class="amplio<?=$corta?>">6. <?php echo $trans->__('APLICACIÓN – DISPOSITIVOS',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png">
					</a>
				</li>
			<?php
			}
			if( $pageuser != "campanas" || $pageuser == "ecommerce"){ 
			$posicion_coincidencia = strpos($thisUrl, "/fuentes-entrada.php");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$img ="_sel";
				$href = 'href="../public/informes/fuentes-entrada.php"';
				$corta= "corta";
			}else{
				$class ="";
				$img ="";
				$corta = "";
				$href = 'href="../public/informes/fuentes-entrada.php"';
			}
			?>
			<li>
				<a class=" <?=$class?>" <?=$href?>  class=""><span class="estrecho<?=$corta?>">7</span><span class="amplio<?=$corta?>">7. <?php echo strtoupper($trans->__('Fuentes de entrada',false)); ?></span> 
					<img src="../public/images/menu_cajetilla<?=$img?>.png">
				</a>
			</li>
			<?php
			}

			if( $pageuser == "campanas" || $pageuser == "ecommerce"){ 
			$posicion_coincidencia = strpos($thisUrl, "/terminos.php");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$img ="_sel";
				$href = 'href="../public/informes/terminos.php"';
				$corta= "corta";
			}else{
				$class ="";
				$img ="";
				$corta = "";
				$href = 'href="../public/informes/terminos.php"';
			}
			?>
			<li>
				<a class=" <?=$class?>" <?=$href?>  class=""><span class="estrecho<?=$corta?>">7</span><span class="amplio<?=$corta?>">7. <?php echo $trans->__('TÉRMINOS',false); ?></span> 
					<img src="../public/images/menu_cajetilla<?=$img?>.png">
				</a>
			</li>
			<?php
			}

			$posicion_coincidencia = strpos($thisUrl, "/impactos1.php");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$img ="_sel";
				$href = 'href="../public/informes/impactos1.php"';
				$corta= "corta";
			}else{
				$class ="";
				$img ="";
				$corta = "";
				$href = 'href="../public/informes/impactos1.php"';
			}
			if( $pageuser == "campanas"){ $ultimaCampana = "ult"; $imgo = "style='display:none'"; }else{ $ultimaCampana = ""; $imgo = "style='display:block'";}
			?>
			<li>
				<a class="ult <?=$ultimaCampana?> <?=$class?>" <?=$href?> class=""><span class="estrecho<?=$corta?>">8</span><span class="amplio<?=$corta?>">8. <?php echo strtoupper($trans->__('Conversiones',false)); ?></span> 
					<!--<img src="../public/images/menu_cajetilla<?=$img?>.png" <?=$imgo?> />-->
				</a>
			</li>
			<?php
			
			?>
		</ul>
		<?php
		if($pageuser=="campanas" || $pageuser == "ecommerce"){
			?></div><?php

		}	
		?>
		
		<?php }else{



			//SEO
			?>
			<ul class="<?=$classul?>">
				<?php
				$posicion_coincidencia = strpos($thisUrl, "/paginas_indexadas.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/seo/paginas_indexadas.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$href = 'href="../public/seo/paginas_indexadas.php"';
					$corta= "";
				}				
				?>
				<li>
					<a class=" <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">1</span><span class="amplio<?=$corta?>">1. <?php echo $trans->__('PÁGINAS INDEXADAS',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png">
					</a>
				</li>
				<?php
				$posicion_coincidencia = strpos($thisUrl, "/busquedas_google.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/seo/busquedas_google.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$corta = "";
					$href = 'href="../public/seo/busquedas_google.php"';
				}
				?>
				<li>
					<a class=" <?=$class?>" <?=$href?> ><span class="estrecho<?=$corta?>">2</span><span class="amplio<?=$corta?>">2. <?php echo $trans->__('BÚSQUEDAS EN GOOGLE',false); ?></span> 
						<img src="../public/images/menu_cajetilla<?=$img?>.png">
					</a>
				</li>
				<?php

				$posicion_coincidencia = strpos($thisUrl, "/enlaces_externos.php");
				if ($posicion_coincidencia != false){ 
					$class ="sel";
					$img ="_sel";
					$href = 'href="../public/seo/enlaces_externos.php"';
					$corta= "corta";
				}else{
					$class ="";
					$img ="";
					$corta = "";
					$href = 'href="../public/seo/enlaces_externos.php"';
				}
				if( $pageuser == "campanas"){ $ultimaCampana = "ult"; $imgo = "style='display:none'"; }else{ $ultimaCampana = ""; $imgo = "style='display:block'";}
				?>
				<li>
					<a class="ult <?=$ultimaCampana?> <?=$class?>" <?=$href?> class=""><span class="estrecho<?=$corta?>">3</span><span class="amplio<?=$corta?>">3. <?php echo $trans->__('ENLACES EXTERNOS ACTIVOS',false); ?></span> 
						<!--<img src="../public/images/menu_cajetilla<?=$img?>.png" <?=$imgo?> />-->
					</a>
				</li>
			</ul>
			<?php
			}//fin seo ?>

		<?php }else{
			//echo "es la página principal de campañas";
		}//fin de si es la pagina principal de campañas

		//}else{

		

		//}//Fin de si es campañas ?>





	</div>
	<style type="text/css">
		.ayuda{position: absolute;right: 118px;top: 25px;background-color: rgb(242, 242, 242);padding: 7px;color: black;cursor: pointer;}
		.caja_ayuda{width: 95%;padding: 15px;background-color: #F3F3F3;color: black;position: relative; display: none}
		.icoinfo{position: absolute;top: 3px;left: 3px;font-size: 18px;color: #ADADAD;}
		.listayuda{ list-style-type: circle;}
		.cerrarayuda{ position: absolute; right: 0px; top: 0px;position: absolute;right: 0px;top: 0px;padding: 10px;font-size: 23px; padding-top: 0px; cursor: pointer;}
		.cerrarayuda:hover{ text-decoration: underline;}
	</style>
	

	<!--Imprimir-->
	<a style="display:none" id="btn_imprimir" href="http://informes.acc.com.es/" title="<?php $trans->__('Imprimir'); ?>" class="venoboxbig" data-type="iframe"><i class="fa fa-print"></i> <?php $trans->__('Imprimir'); ?></a>

	<?php
	//AYUDAS
	$thisUrl = $_SERVER['REQUEST_URI'];
	$posicion_coincidencia_listado_campanas = strpos($thisUrl, "/campanas_resumen.php");
	$posicion_coincidencia_comparativa      = strpos($thisUrl, "/campanas_comparativa.php");
	$posicion_coincidencia_pedidos          = strpos($thisUrl, "/campanas_informe_pedidos.php");
	$posicion_coincidencia_productos        = strpos($thisUrl, "/campanas_informe_productos.php");
	//Si estamos en la pagina de comparativa
	if ($posicion_coincidencia_listado_campanas != false){ 

		if($pageuser == "campanas"){ 

		$mostrar = "display:block";
		$ayuda   = "<h2>".$trans->__('Ayuda',false).":</h2>";
		$ayuda   .= "<p>".$trans->__('Presentamos las campañas detectadas entre las fechas seleccionadas.',false)."  
					".$trans->__('Si la campaña está distribuida en varios medios y fuentes podrás ampliar la información clickando en el nombre de la misma. Las métricas que se muestran de cada campaña son:',false)."</p>";
		$ayuda   .= "<ul class='listayuda'>";
		$ayuda   .= "<li><u>".$trans->__('Sesiones',false)."</u></li>";
		$ayuda   .= "<li><u>".$trans->__('Usuarios',false)."</u></li>";
		$ayuda   .= "<li><u>".$trans->__('Tasa de rebote',false)."</u></li>";
		$ayuda   .= "<li><u>".$trans->__('Páginas por sesión',false)."</u></li>";
		$ayuda   .= "<li><u>".$trans->__('Total conversiones.',false)."</u>  ".$trans->__('Este valor indica el número de conversiones en las que la campaña ha participado, a veces junto con otras campañas o fuentes de entrada.',false)."  
						".$trans->__('Esto quiere decir que si un usuario antes de rellenar un formulario de contacto, ha entrado al sitio web a través de un anuncio de Adwords y a su vez ha llegado a la web a través de un emailing,',false)." 
						".$trans->__('en ambas campañas contará la misma conversión.',false)."</li>";
		$ayuda   .= "<li><u>".$trans->__('Valor de las conversiones.',false)."</u>  ".$trans->__('Es la suma de valores asignados a las conversiones en las que ha participado esta campaña.',false)."</li>";
		$ayuda   .= "</ul>";

		}else{

		$mostrar = "display:block";
		$ayuda   = "<h2>".$trans->__('Ayuda',false).":</h2>";
		$ayuda   .= "<p>".$trans->__('Presentamos las campañas detectadas entre las fechas seleccionadas.',false)."  
					".$trans->__('Si la campaña está distribuida en varios medios y fuentes podrás ampliar la información clickando en el nombre de la misma. Las métricas que se muestran de cada campaña son:',false)."</p>";
		$ayuda   .= "<ul class='listayuda'>";
		$ayuda   .= "<li><u>".$trans->__('Sesiones',false)."</u></li>";
		$ayuda   .= "<li><u>".$trans->__('Usuarios',false)."</u></li>";
		$ayuda   .= "<li><u>".$trans->__('Tasa de rebote',false)."</u></li>";
		$ayuda   .= "<li><u>".$trans->__('Páginas por sesión',false)."</u></li>";
		$ayuda   .= "<li><u>".$trans->__('Total transacciones.',false)."</u> ".$trans->__('Este valor indica el número de transacciones (pedidos) en las que la campaña ha participado, a veces junto con otras campañas o fuentes de entrada.',false)."  
						".$trans->__('Esto quiere decir que si un usuario antes de realizar una compra, ha entrado al sitio web a través de un anuncio de Adwords y a su vez ha llegado a la web a través de un emailing,',false)." 
						".$trans->__('en ambas campañas contará la misma transacción.',false)."</li>";
		$ayuda   .= "<li><u>".$trans->__('Valor de las transacciones.',false)."</u> ".$trans->__('Es la suma del valor de los pedidos asignados a esta campaña.',false)."</li>";
		$ayuda   .= "</ul>";

		}

	}else if($posicion_coincidencia_comparativa != false){

		if($pageuser != "campanas"){ 

		$mostrar = "display:block";
		$ayuda   = "<h2>".$trans->__('Ayuda',false).":</h2>";
		$ayuda   .= "<p>".$trans->__('En esta pantalla presentamos la comparación de diversas métricas para el conjunto de campañas detectadas en los últimos doce meses.',false)." </p>";
		$ayuda   .= "<p>".$trans->__('Utilice la ayuda situada en cada métrica para ampliar su información.',false)."</p>";
		$ayuda   .= "<p>";
		$ayuda   .= "".$trans->__('Es importante notar que, en esta pantalla, para saber el <u>número de pedidos</u> que se atribuyen a una campaña <u>utilizamos un modelo de atribución “lineal”</u>.',false)." 
					".$trans->__('Es decir, damos la misma importancia a cada campaña o fuente de entrada que ha utilizado el cliente hasta que ha llegado a realizar su pedido (en los últimos 30 días).',false)."  
					".$trans->__('Por ejemplo, si un cliente primero entró a través de una búsqueda en google, y luego clickó en un anuncio en adwords y al cabo de 3 días entró al sitio web a través de una campañas en el comparador Ciao y ahí realizó el pedido, se atribuirá 1/3 del pedido al SEO, otro 1/3 a la campaña de Adwords y otro 1/3 a la campaña de Ciao.',false);
		$ayuda   .= "</p>";

		}else{

		$mostrar = "display:block";
		$ayuda   = "<h2>".$trans->__('Ayuda',false).":</h2>";
		$ayuda   .= "<p>".$trans->__('En esta pantalla presentamos la comparación de diversas métricas para el conjunto de campañas detectadas en los últimos doce meses.',false)." </p>";
		$ayuda   .= "<p>".$trans->__('Utilice la ayuda situada en cada métrica para ampliar su información.',false)."</p>";
		$ayuda   .= "<p>";
		$ayuda   .= "".$trans->__('Es importante notar que, en esta pantalla, para saber el <u>número de conversiones</u> que se atribuyen a una campaña, no utilizamos un método de atribución que valore la participación de la campaña de forma prorrateada. Al contrario, el número de conversiones es el número “total” de conversiones en las que la campaña ha participado, a veces junto con otras campañas o fuentes de entrada.',false)."  
					".$trans->__('Esto quiere decir que si un usuario antes de rellenar un formulario, ha entrado al sitio web a través de un anuncio de Adwords y a su vez ha llegado a la web a través de un emailing,',false)." 
					".$trans->__('en ambas campañas contará la misma conversión.',false)." ".$trans->__('Por lo tanto, este valor no tiene nada que ver con un valor “prorrateado” o “atribuido” a la campaña dependiendo de su colaboración en cada conversión, sino que tiene que ver con la suma “total” de todas las conversiones en los que ha participado.',false)."";
		$ayuda   .= "</p>";

		}

	}else if($posicion_coincidencia_pedidos != false || $posicion_coincidencia_productos != false){

		$mostrar = "display:block";
		$ayuda   = "<h2>".$trans->__('Ayuda',false).":</h2>";
		$ayuda   .= "<p>".$trans->__('En esta pantalla presentamos la opción de poder comparar con hasta dos perspectivas diferenteres.',false)." </p>";
		$ayuda   .= "<p>";
		$ayuda   .= "".$trans->__('Siempre estará seleccionada la primera pespectiva (Azul) pudiendo elegir otra (Verde) pinchando en el menú de pespectiva. Para poder cambiar la primera pespectiva se deberá pinchar en ella dejándola seleccionada y después pinchando en otra para que se desplace a ella.',false)."";
		$ayuda   .= "</p>";

	}else{
		//Si no estamos en la página de comparativa
		$mostrar = "display:none";
		$ayuda  =  ""; 
	}
	?>
	<!--Ayuda-->
	<a class="ayuda" style="<?=$mostrar?>"><i class="fa fa-info-circle"></i> <?php $trans->__('Ayuda'); ?></a>
	<div class="caja_ayuda">
		<i class="fa fa-info-circle icoinfo"></i>
		<span class="cerrarayuda">x</span>
		<span><?=$ayuda?></span>
	</div>



	<script type="text/javascript">
	$(document).ready(function(){
		$(".ayuda").on("click",function(){
			$(".caja_ayuda").stop();
			if($(".caja_ayuda").css("display")=='none'){
				$(".caja_ayuda").slideDown(600);
			}else{
				$(".caja_ayuda").slideUp(600);
			}
		})

		$(".cerrarayuda").on("click",function(){
			$(".caja_ayuda").stop();
			$(".caja_ayuda").slideUp(600);	
		})
	})
	</script>
<?php } ?>




<?php

include("usuarios/funciones_usuarios.php");
include("funciones_globales.php");

//Sistema de inicio de sesion seguro
//session_save_path(HostingDirURL());
//session_start(); 
sec_session_start();


//GLOBALES
	//Variable global que detenrmina el numero minimos de intentos de conexión
	//Si son más bloquea el acceso por seguridad.
	define("INTENTOS_CONEXION", 50000);//Desctivado
	//Correo electrónico para el envio de correos
	define("EMAIL_CORREOS", "");
	//Ruta absoluta para los links
	define("RUTA_ABSOLUTA", "");
	//Variable para concretar el numero de vistas que aparece en el combo
	//del head antes de que aparezca el buscado
	define("NBUSCADOR", 10);
	//Array de subdominio para hacerlos como superadministrador
	global $com_emails;
	$com_emails["emails"][0] = "";
	$com_emails["emails"][1] = "";


//Conexion a la base de datos
	define("HOST", ""); 
	define("USER", ""); 
	define("PASSWORD", ""); 
	define("DATABASE", "");
	$c = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);
	if ($c) {
		//echo "Conectado";
	}else{
		echo $trans->__("Hay problemas de conexión en la base de datos");
	}


//CONFIGURADOR DE CORREO ELECTRÓNICO

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that


require 'emails/PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
//Pregunte por la salida de depuración HTML-amigable
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = "";
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = 25;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication
$mail->Username = "";
//Password to use for SMTP authentication
$mail->Password = "";
//Establecer que el mensaje va a ser enviado desde
$mail->setFrom('', '');



//DATOS API DE SPEEDPAGE
define("APIKEY", ""); 


//Traduccion de contenidos
require_once('lang/class.translation.php');
?>

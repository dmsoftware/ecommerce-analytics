<!-- Footer -->
<!-- <footer class="main">
	
	
	
</footer>	 -->

</div>
	
</div>

	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/datatables/datatables.responsive.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/rickshaw/rickshaw.min.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/daterangepicker/daterangepicker-bs3.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/select2/select2.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>css/venobox.css">

	<!-- Bottom Scripts -->
	<script src="<?=RUTA_ABSOLUTA?>js/venobox.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/gsap/main-gsap.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/bootstrap.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/joinable.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/resizeable.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/neon-api.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/jquery.sparkline.min.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/raphael-min.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/morris.min.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/toastr.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/neon-chat.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/neon-custom.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/neon-demo.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>

	<!-- SELECTOR DE FECHA -->
	<script src="<?=RUTA_ABSOLUTA?>assets/js/daterangepicker/moment.min.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/daterangepicker/daterangepicker.js"></script>

	<!-- TABLAS -->
	<script src="<?=RUTA_ABSOLUTA?>assets/js/datatables/jquery.dataTables.min.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/dataTables.bootstrap.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/datatables/dataTables.responsive.min.js"></script>	
	<script src="<?=RUTA_ABSOLUTA?>assets/js/datatables/dataTables.tableTools.min.js"></script>
	
	<!-- GRAFICOS -->
	<script src="<?=RUTA_ABSOLUTA?>assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>assets/js/rickshaw/rickshaw.js"></script>
	
<!--
	<script src="assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="assets/js/datatables/lodash.min.js"></script>
-->

	<!-- SELECT -->
	<script src="<?=RUTA_ABSOLUTA?>assets/js/select2/select2.min.js"></script>

</body>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal para cargar el iframe con las pantallas de edición de symfony -->
<style type="text/css">
  .iframContainer {
    position: relative;
    padding-bottom: 56.25%;
    overflow: hidden;
  }
  .iframContainer iframe{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    /*overflow-x: hidden;*/
  }
</style>
    
<div class="modal fade bs-example-modal-lg" id="myModaliFrame" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"><div class="iframContainer"></div></div>
  </div>
</div>

</html>


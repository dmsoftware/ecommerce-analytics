<?php
include("../includes/conf.php"); 
include("../includes/usuarios/validaciones_usuarios.php");


//var_dump($_COOKIE["usuario"]);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	

	<title>DMIntegra</title>
	
	<link rel="shortcut icon" type="image/x-icon" href="../public/images/favicon.ico" />

	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/neon-core.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/neon-forms.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/custom.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/css/skins/white.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>css/estilos_head.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>css/estilos_side.css">

	<script src="<?=RUTA_ABSOLUTA?>js/jquery-1.11.0.min.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>js/translation.js"></script>
	<script src="<?=RUTA_ABSOLUTA?>js/global_scripts.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body class="page-body" data-url="http://neon.dev">
<!--	incluimos los modales globales-->

<?php
if(isset($_COOKIE["vista"])){

	//Validamos la primera vista
	$valiVista = val_vistauser($_COOKIE["usuario"]["email"],$_COOKIE["vista"],$com_emails);
	if(!$valiVista){

		?>
		<script type="text/javascript">
			$(document).ready(function(){
				self.location = "../public/error.php?error=2";
			})
		</script>
		<?php
		
	}else{
		//echo "Permiso concedido";
	}

}

if(!isset($_COOKIE["vista"])){
	$vistas = UsuariosPropiedadesVistas($_COOKIE["usuario"]["email"],$com_emails,"");
	if($vistas["cantidad"]==0){
		//Aqui es lo que ocurre cuando no tiene ninguna vista enlazada
	}else{
		$comparar_propiedad = "";
		$sw="";
		$primera_proyectoasoc = "";
		$primera_vista = "";
		foreach ($vistas["elementos"] as $key => $vista) {
			$propiedad = $vista["id_propiedad_analytics"];
			if($comparar_propiedad==""){
				$primera_proyectoasoc = $vista['analytics_proyectoasociado'];
			}
			if($propiedad != $comparar_propiedad){
				if(empty($vista["name_propiedad_analytics"])){
					$nvista = $vista["id_propiedad"];
				}else{
					$nvista = $vista["name_propiedad_analytics"];
					$npropiedad = $vista["name_propiedad_analytics"];
				}
				$comparar_propiedad = $propiedad;
				
			}
			//opciones
			if(empty($vista["name_vista_analytics"])){
				$nvista = $vista["id_vista_analytics"];
			}else{
				$nvista = $vista["name_vista_analytics"];
			}

			if($primera_vista==""){
					$primera_vista = $vista["id_vista_analytics"];
				}


			$selected="";	
			if(isset($_GET["vista"])){
				if($_GET["vista"]==$vista["id_vista_analytics"]){
					$selected="selected";
				}
				$primera_vista = $_GET["vista"];
			}else{
				if(isset($_COOKIE["vista"])) {
					if($_COOKIE["vista"]==$vista["id_vista_analytics"]){
						$selected="selected";
					}
					$primera_vista = $_COOKIE["vista"];	
				}
			}
			?>
			<?php
		}//for
	}
	$vista_permi = $primera_vista;
}else{
	$vista_permi = $_COOKIE["vista"];
}



$pag_perfil      = 1;
$pag_historico   = 1;
$pag_informes    = 1;
$pag_seo         = 1;
$pag_ecommerce   = 1;
$pag_campanas    = 1;
$pag_cuadromando = 1;

//Ahora comprobamos Cuales son las páginas que puede ver
$permisosUser = permisosUser($_COOKIE["usuario"]["email"],$vista_permi,$com_emails);
if($permisosUser["cantidad"] != 0){

	if($permisosUser["elementos"][1]["admin"] != 1){
   
		$pag_perfil      = $permisosUser["elementos"][1]["perfil"];     
		$pag_historico   = $permisosUser["elementos"][1]["historico"];  
		$pag_informes    = $permisosUser["elementos"][1]["informes"];   
		$pag_seo         = $permisosUser["elementos"][1]["seo"];        
		$pag_ecommerce   = $permisosUser["elementos"][1]["ecommerce"];  
		$pag_campanas    = $permisosUser["elementos"][1]["campanas"];   
		$pag_cuadromando = $permisosUser["elementos"][1]["cuadromando"];

	}
	
}

//echo "<br><h3>Infor: ".$_COOKIE["usuario"]["email"].$_COOKIE["vista"]."</h3><br>";
if($pag_informes==1){$disblaypags_informes="block";}else{$disblaypags_informes="none";}
if($pag_historico==1){$disblaypags_historico="block";}else{$disblaypags_historico="none";}
if($pag_perfil==1){$disblaypags_perfil="block";}else{$disblaypags_perfil="none";}
if($pag_seo==1){$disblaypags_seo="block";}else{$disblaypags_seo="none";}
if($pag_ecommerce==1){$disblaypags_ecommerce="block";}else{$disblaypags_ecommerce="none";}
if($pag_campanas==1){$disblaypags_campanas="block";}else{$disblaypags_campanas="none";}
if($pag_cuadromando==1){$disblaypags_mando="block";}else{$disblaypags_mando="none";}
//echo "<br><h3>Infor: ".$disblaypags_informes."</h3><br>";
?>

<div class="page-container horizontal-menu with-sidebar">
	
	<header class="navbar navbar-fixed-top"><!-- set fixed position by adding class "navbar-fixed-top" -->
		
		<div class="navbar-inner">
		
			<!-- logo -->
			<div class="navbar-brand">
				<a href="<?=RUTA_ABSOLUTA?>index.php">
					<img src="<?=RUTA_ABSOLUTA?>images/DMintegra.png"  alt="" />
				</a>
			</div>
			<!--logo-->

			<!--contenedor_menu-->
			<div class="contenedor_menu">
			
					<?php
						$pageuser ="";
						//Si es historial o configuracion
						$thisUrl = $_SERVER['REQUEST_URI'];

						$posicion_coincidencia = strpos($thisUrl, "historial.php");
						if ($posicion_coincidencia != false){

							//setcookie( "userpage", "historial", time() + (86400 * 30), "/"); //86400 es un dia
							$pageuser = "historial";

						}
						$posicion_coincidencia = strpos($thisUrl, "perfil_usuario.php");
						if ($posicion_coincidencia != false){

							//setcookie( "userpage", "configuracion", time() + (86400 * 30), "/"); //86400 es un dia
							$pageuser = "configuracion";

						}	

						$posicion_coincidencia = strpos($thisUrl, "permisos.php");
						if ($posicion_coincidencia != false){

							//setcookie( "userpage", "configuracion", time() + (86400 * 30), "/"); //86400 es un dia
							$pageuser = "configuracion";

						}
						$posicion_coincidencia = strpos($thisUrl, "sitioweb.php");
						if ($posicion_coincidencia != false){

							//setcookie( "userpage", "configuracion", time() + (86400 * 30), "/"); //86400 es un dia
							$pageuser = "configuracion";

						}
						//*************************

						if(empty($pageuser)){
							if(isset($_COOKIE["userpage"])){
								$pageuser = $_COOKIE["userpage"];
							}else{
								//Si no comprobamos que url es si añadimos cookie de donde sea
								$pageuser = "informes";
								//Si es campañas
								$thisUrl = $_SERVER['REQUEST_URI'];
								$posicion_coincidencia = strpos($thisUrl, "as_resumen.php");
								if ($posicion_coincidencia != false){

									setcookie( "userpage", "campanas", time() + (86400 * 30), "/"); //86400 es un dia
									$pageuser = "campanas";

								}
								$posicion_coincidencia = strpos($thisUrl, "seo.php");
								if ($posicion_coincidencia != false){

									setcookie( "userpage", "seo", time() + (86400 * 30), "/"); //86400 es un dia
									$pageuser = "seo";

								}
								
							}

							if($pageuser == "campanas"){
								
							}
						}//Si pageuser esta vacio
						
					?>
					<ul id="menu">
						<li class="menu_arriba" name="informes"><a style="display:<?=$disblaypags_informes?>" name="../public/informes/metricas_habituales.php" <?php if($pageuser == "informes"){ echo 'class="activo"'; } ?> ><span><?php $trans->__('Informes'); ?></span><span class="beta">beta</span></a></li>
						<li class="menu_arriba" name="seo"><a style="display:<?=$disblaypags_seo?>" name="../public/seo/paginas_indexadas.php" <?php if($pageuser == "seo"){ echo 'class="activo"'; } ?>><span><?php $trans->__('Seo'); ?></span><span class="beta">beta</span></a></li>
						<li class="menu_arriba" name="campanas"><a style="display:<?=$disblaypags_campanas?>" name="../public/campanas/campanas_resumen.php" <?php if($pageuser == "campanas"){ echo 'class="activo"'; } ?>><span><?php $trans->__('Campañas'); ?></span><span class="beta">beta</span></a></li>
						<!--COMPROBAMOS SI LA PROPIEDAD ELEGIDA ES ECOMMERCE-->

						<li class="menu_arriba"  id="menuecomerce" name="ecommerce" style="display:none"><a style="display:<?=$disblaypags_ecommerce?>" name="../public/ecommerce/campanas_informe_pedidos.php" <?php if($pageuser == "ecommerce"){ echo 'class="activo"'; } ?>><span><?php $trans->__('eCommerce'); ?></span><span class="beta">beta</span></a></li>
						

						<li class="menu_arriba" name="mando"><a style="display:<?=$disblaypags_mando?>; cursor:not-allowed" name="../public/cuadromando/cuadromando.php" <?php if($pageuser == "mando"){ echo 'class="activo"'; } ?>><span><?php $trans->__('Cuadro de mando'); ?></span></a></li>		
					</ul> 
				<script type="text/javascript">
					$(document).ready(function(){

						$('.dropdown-toggle').dropdown()

						$(".menu_arriba").on("click",function(){
							var ruta   = $(this).children("a").attr("name");
							var sesion = $(this).attr("name");
							$.ajax({
						        type: 'POST',
						        url: '../head_ajax_selectpage.php',
						        data: {
						          ses:   sesion    
						          },
						        dataType: 'text',
						        success: function(data){

						        	self.location= ruta;
						          
						          },
						        error: function(data){
						          
					       	 	}
					    	})
						})
					});//ready
				</script>

			</div><!--contenedor_menu-->

			<!--contenedor_vistas-->
			
			<script type="text/javascript">
				$(document).ready(function(){
						$(".contenedor_desplegar,.contenedor_seleccionado").on("click",function(){
						if($(".contenedor_listadovistas").css("display") == "none"){
							$(".maskara_out").show();
							$(".contenedor_listadovistas").show();
						}else{
							$(".maskara_out").hide();
							$(".contenedor_listadovistas").hide();
						}
					})
					$(".maskara_out").on("click",function(){
						$(".maskara_out").hide();
						$(".contenedor_listadovistas").hide();
					})
				});
			</script>

			<div class="contenedor_vistas" style="display:none">
				<select name="test" id="combo_vistas" class="selectboxit visible select2" data-allow-clear="false" data-placeholder="<?php $trans->__('Selecciona una vista'); ?>">
						
					<?php
					//$_COOKIE["usuario"]["email"]
					
					$vistas = UsuariosPropiedadesVistas($_COOKIE["usuario"]["email"],$com_emails,"");
					if($vistas["cantidad"]==0){
						//Aqui es lo que ocurre cuando no tiene ninguna vista enlazada
					}else{
						if($vistas["cantidad"] < NBUSCADOR){
							?><script type="text/javascript">
							$(document).ready(function(){
								$("#combo_vistas").removeClass("select2");
							});
							</script>
							<?php
						}
						$comparar_propiedad = "";
						$sw="";
						$primera_proyectoasoc = "";
						$primera_vista = "";
						foreach ($vistas["elementos"] as $key => $vista) {
							$propiedad = $vista["id_propiedad_analytics"];
							if($comparar_propiedad==""){
								$primera_proyectoasoc = $vista['analytics_proyectoasociado'];
							}
							if($propiedad != $comparar_propiedad){
								if(empty($vista["name_propiedad_analytics"])){
									$nvista = $vista["id_propiedad"];
								}else{
									$nvista = $vista["name_propiedad_analytics"];
									$npropiedad = $vista["name_propiedad_analytics"];
								}
								$comparar_propiedad = $propiedad;
								?><?=$sw?><optgroup label="<?=$nvista?>"><?php
							}
							//opciones
							if(empty($vista["name_vista_analytics"])){
								$nvista = $vista["id_vista_analytics"];
							}else{
								$nvista = $vista["name_vista_analytics"];
							}

							if($primera_vista==""){
									$primera_vista = $vista["id_vista_analytics"];
								}


							$selected="";	
							if(isset($_GET["vista"])){
								if($_GET["vista"]==$vista["id_vista_analytics"]){
									$selected="selected";
								}
								$primera_vista = $_GET["vista"];
							}else{
								if(isset($_COOKIE["vista"])) {
									if($_COOKIE["vista"]==$vista["id_vista_analytics"]){
										$selected="selected";
									}
									$primera_vista = $_COOKIE["vista"];	
								}
							}
							?>
							<option value="<?=$vista["id_vista_analytics"]?>*<?=$vista["analytics_proyectoasociado"]?>" class="opcion" <?=$selected?>> <?=$npropiedad?> >> <?=$nvista?></option>
							<?php
							$sw="</optgroup>";
						}//for
					}

					?>
						
				</select>

			</div><!--contenedor_vistas-->

			

			<?php

			if($primera_vista != ''){

				
			 		$buscaVista = propiedadVista($primera_vista);
			 		
				 	foreach ($buscaVista["elementos"] as $key => $v) {
				 		$nombre_propiedad = $v["nombre_propiedad"];
				 		$nombre_vistab = $v["nombre_vista"];
				 	}
			 		
			 	
			 	$strDominio = DominioVista($primera_vista);

		 	}else{
		 		?>
		 		<script type="text/javascript">
			 		$(document).ready(function(){
			 			self.location = "../public/error.php?error=1";
			 		})
		 		</script>
		 		<?php
		 	}

		 	//Buscamos si es app
		 	$papp = propiedadVista($primera_vista);
		 	$primera_app = 0;
		 	if($papp["cantidad"]!=0){
			 	foreach ($papp["elementos"] as $key => $swapp) {
			 		$primera_app = $swapp["app"]; 
			 		$primera_propiedad = $swapp["idpropiedad"];
			 	}
		 	}

		 	if($primera_app == 1){
		 		$primera_app = 1;
		 	}else{
		 		$primera_app = 0;
		 	}

		 	$appico = "globe";
			if($primera_app == 1){
				$appico = "mobile";
			}
			//echo "app: ".$primera_app;
		 	//nombre user
		 	$userdata = UsuariosData($_COOKIE["usuario"]["email"]);
		 	$nombre_user    = $userdata["elementos"]["nombre"];
			$apellidos_user = $userdata["elementos"]["apellidos"];
			$sexo_user      = $userdata["elementos"]["sexo"];
			$idioma_user    = $userdata["elementos"]["idioma"];
		 	if(empty($nombre_user)){

		 	}else{
		 		$nombre_user = $nombre_user." - ";
		 	}

		 	if(empty($nombre_user) || empty($apellidos_user) || empty($sexo_user) || empty($idioma_user)){
		 		$falta = '<i class="fa fa-warning" data-toggle="tooltip" data-placement="top" title="Completa tus datos de perfil" style="padding-rigth:0px;padding-left:0px;"></i>';
		 	}else{
		 		$falta = "";
		 	}

		 	//Comprobamos escudo antiespan
		 	if(antiSpam($primera_propiedad)){
		 		$antiIco = "icoantispam_green";
		 	}else{
		 		$antiIco = "icoantispam_gris";
		 	}
			?>
			<div class="maskara_out"></div>
			<!--contenedor_vistas_nuevo-->
			<div class="contenedor_vistas">
				<div class="contenedor_seleccionado">
					<p id="usermen"><?=$_COOKIE["usuario"]["email"]?> </p>
					<p id="propiedadmen"><i class="fa fa-shield <?=$antiIco?>" style="font-size:13px"></i> <i class="fa fa-<?=$appico?> icoappp" style="font-size:13px"></i> <?=$nombre_propiedad?></p>
					<?php
					if($primera_app==0){
						?>
						<p id="vistamen"><?=$strDominio?> - <?=$nombre_vistab?></p>
						<?php
					}else{
						?>
						<p id="vistamen"><?=$nombre_vistab?></p>
						<?php
					}
					?>
				</div>
				<div class="contenedor_desplegar">
					<p>v</p>
				</div>

				<div class="contenedor_listadovistas">
					
					<div class="parte ususario">
						<div class="parteperfilb">
							<p><?=$nombre_user?> <?=$_COOKIE["usuario"]["email"]?></p>
						</div>
						<div class="parteperfil">
							<button type="button" class="btn btn-default btnperfila" id="btn_configurarperfil" style="display:<?=$disblaypags_perfil?>"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <?php $trans->__('Configurar perfil'); ?></button>
							<span class="avisoperfil"  style="display:<?=$disblaypags_perfil?>"><?=$falta?></span>
							<button type="button" id="btn_historial" class="btn btn-default btnperfila" id="btn_configurarperfil" style="display:<?=$disblaypags_historico?>" ><i class="fa fa-clock-o"></i> <?php $trans->__('Histórico'); ?></button>
							<button type="button" class="btn btn-default btnperfilb" id="cerrar-sesion"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> <?php $trans->__('Cerrar sesión'); ?></button>
						</div>
					</div>
					<div class="parte_listado">

						<form class="form-inline">
						    <div class="input-group">
						    	<div class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
						      	<input type="text" class="form-control inputbuscavista" id="txt_buscavistas" placeholder="">
						    </div>
						</form>
						<div class="listadovistas" id="listadovistas">
						
						</div>
					</div>

				</div>
			</div>
			

		</div><!--navbar-inner-->


		
		<style type="text/css">
			.tooltip-inner{ padding: 7px }
			.tooltip-arrow{ }
		</style>


		<?php
			if($primera_app==0){ $funcvista = $strDominio." | ".$nombre_vistab;}else{ $funcvista = $nombre_vistab;}
			pageLogs($_COOKIE["usuario"]["email"],$_SERVER['REQUEST_URI'],$nombre_propiedad,$funcvista,clienteVista($primera_vista),$primera_vista);
		?>









		<script type="text/javascript">


			function leerCookie(name) {
			  var nameEQ = name + "=";
			  var ca = document.cookie.split(';');
			  for(var i=0;i < ca.length;i++) {
			    var c = ca[i];
			    while (c.charAt(0)==' ') c = c.substring(1,c.length);
			    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			  }
			  return null;
			}


			$(document).ready(function(){

				//document.title = 'DmIntegra | <?=$strDominio?>';

				recargacombovistas();

				$("#txt_buscavistas").keyup(function(){
					$("#listadovistas").html("");
				    recargacombovistas();
				}); 

				$("#btn_configurarperfil").on("click",function(){
					self.location = "../public/configuracion/perfil_usuario.php";
				})
				$("#btn_historial").on("click",function(){
					self.location = "../public/historial/historial.php";
				})

				$("#btn_cerrarsesion").on("click",function(){
					var emaill = $(this).attr("name");
					$.ajax({
				        type: 'POST',
				        url: '../head_ajax_cerrar-sesion.php',
				        data: {
				          email: emaill
				          },
				        dataType: 'text',
				        success: function(data){
				          self.location ="../index.php";
				          },
				        error: function(data){
				          //error
				          //motivo lo trae el data
				          
				      	}
				     })

				})//fin btn

				$("#cerrar-sesion").on("click",function(){
					$('#modal_cerrarsesion').modal()
				})
			})//fin ready

			function recargacombovistas(){
					var valor = leerCookie("idioma_usuario");
					//alert(valor);
					if(valor == 'es' || valor == 'en' || valor == 'eu' ){
						$("#listadovistas").load("../head_ajax_listadovistas.php?buscador="+$("#txt_buscavistas").val());
					}else{
						self.location = "../public/index.php";
					}
				
			}
		</script>

		<?php
			//ADQUIRIR EL PROYECTO ASOCIADO DE LA VISTA QUE TRAEMOS Y EL NOMBRE
			//DE LA PROPIEDAD PARA EL TITLE
			?>
			<script type="text/javascript">$(document).ready(function(){document.title = 'DmIntegra | <?=$strDominio?>';});</script>
			<?php
			$primera_proyectoasoc_busqueda ="";
			//Teniendo la vista optenemos el proyecto asociado en caso de tener sesion o get
			if(isset($_GET["vista"])){
				$primera_proyectoasoc_busqueda = $_GET["vista"];
			}else{
				if(isset($_COOKIE["vista"])) {
					$primera_proyectoasoc_busqueda = $_COOKIE["vista"];	
				}
			}
			if($primera_proyectoasoc_busqueda!=""){
				$proyectoasociados = propiedadVista($primera_proyectoasoc_busqueda);
				if ($proyectoasociados["cantidad"]!=0){
					foreach ($proyectoasociados["elementos"] as $key => $proy) {
						$primera_proyectoasoc = $proy["proyectoasociado"];
						$nombre_vista = $proy["nombre_propiedad"];
						?>
						<script type="text/javascript">$(document).ready(function(){document.title = 'DmIntegra | <?=$nombre_vista?>';});</script>
						<?php
					}
				}
			}
		?>

</header>

<!--Modal para cerrar sesión-->

	<div class="modal fade" id="modal_cerrarsesion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php $trans->__('Close'); ?></span></button>
	        <h4 class="modal-title"><?php $trans->__('Cerrar sesión'); ?></h4>
	      </div>
	      <div class="modal-body">
	        <p><?php $trans->__('¿Seguro que quiere cerrar sesión?'); ?></p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"><?php $trans->__('Cancelar'); ?></button>
	        <button type="button" class="btn btn-primary" name="<?=$_COOKIE["usuario"]["email"]?>" id="btn_cerrarsesion"><?php $trans->__('Cerrar sesión'); ?></button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<!--Modal para cerrar sesión-->

<!--modal para ampliacion de graficos-->

<div class="modal fade" id="modal_ampliar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
	    <div class="modal-header" id="rickshaw_header">
	      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php $trans->__('Close'); ?></span></button>
	      <h4 class="modal-title">&nbsp;</h4>
	    </div>
	    <div class="modal-body" id="cont_ampliacion">
	      <!--CONTENIDO-->
	      <div id="contenedor_rickshaw_ampliacion">
			<div id="y_axis_rickshaw_ampliacion"></div>
			<div id="rickshaw_ampliacion"></div>
			<div id="x_axis_rickshaw_ampliacion"></div>
		  </div>
	    </div>
	    <div id="rickshaw_legenda">
	    	<p><span class="color_rickshaw_legenda_act"></span><span class="expl_legenda"> <?php $trans->__('Periodo actual'); ?></span></p>
	    	<p><span class="color_rickshaw_legenda_ant"></span><span class="expl_legenda"> <?php $trans->__('Periodo anterior'); ?></span></p>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal"><?php $trans->__('Cerrar'); ?></button>
	    </div>
	  </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--modal para ampliacion de graficos-->
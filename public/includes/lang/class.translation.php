<?php


//Inicia funcion para las traducciones


if (isset($_GET['lang'])) {
  $idioma_usuario = $_GET['lang'];
  setcookie( "idioma_usuario", $_GET['lang'], time() + (86400 * 30), "/"); //86400 es un dia
} elseif (!isset($_COOKIE["idioma_usuario"])) { 
  $idioma_usuario = 'es';
  setcookie( "idioma_usuario", 'es', time() + (86400 * 30), "/"); //86400 es un dia
} else {
  $idioma_usuario = $_COOKIE["idioma_usuario"];
}

$trans = new Translator($idioma_usuario);

//Clase de traduccion
class Translator {
  private $src;
  private $language = 'es';
  private $lang = array();
  private $printStr = true;

  public function __construct($language) {
    $this->src = dirname(__FILE__).'/';
    $this->language = $language;
  }

  private function findString($str, $text = true) {
    if (is_array($this->lang[$this->language]) && array_key_exists($str, $this->lang[$this->language])) {
      if($text){
        if($this->printStr) echo $this->lang[$this->language][$str] === '' ? $str : $this->lang[$this->language][$str];
        else return $this->lang[$this->language][$str] === '' ? $str : $this->lang[$this->language][$str];
        return;
      }else{
        return true;
      }
    }

    if($text){
      if($this->printStr) echo $str;
      else return $str;
    }else{
      return false;
    }
  }

  private function splitStrings($str) {
    return explode('=', trim($str));
  }

  public function __($str, $print = true) {
    $this->printStr = $print;
    $str = trim($str);

    if (!array_key_exists($this->language, $this->lang)) {
      if (file_exists($this->src . $this->language . '.txt')) {
        $this->lang[$this->language] = json_decode(file_get_contents($this->src . $this->language . '.txt'), true);

        return $this->findString($str);
      } else {
        if($this->printStr) echo $str;
        else return $str;
      }
    } else {
      return $this->findString($str);
    }
  }

  public function extract($source, $type){
    //Javascript
    preg_match_all("/trans.__\(([^\)]*)\)/", $source, $match1);
    //Php
    preg_match_all("/trans->__\(([^\)]*)\)/", $source, $match2);

    $matches = array_merge($match1[1], $match2[1]);

    foreach($matches as $val) {
      $str = substr(substr(str_replace(array(', false', ',false'), array('',''), $val), 1), 0, -1);

      if (!array_key_exists($this->language, $this->lang)) {
        if (file_exists($this->src . $this->language . '.txt')) {
          $this->lang[$this->language] = json_decode(file_get_contents($this->src . $this->language . '.txt'), true);

          if(!$this->findString($str, false)){
            $this->printJSON($str);
          }
        } else {
          $this->printJSON($str);
        }
      } else {
        if(!$this->findString($str, false)){
          $this->lang[$this->language][$str] = '';
          $this->printJSON($str);
        }
      }

    }
  }

  private function printJSON($str){
    echo '"' . utf8_decode($str) . '": "",<br/>';
  }

  public function getLanguage(){
    return $this->language;
  }

  public function setLanguage($language){
    return $this->language = $language;
  }
}
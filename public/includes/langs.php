<div class="langs">
	<a href="?lang=es" class="<?php echo $idioma_usuario === 'es' ? 'lang-selected' : '' ?>"><?php $trans->__('ES'); ?></a>
	|
	<a href="?lang=eu" class="<?php echo $idioma_usuario === 'eu' ? 'lang-selected' : '' ?>"><?php $trans->__('EU'); ?></a>
	|
	<a href="?lang=en" class="<?php echo $idioma_usuario === 'en' ? 'lang-selected' : '' ?>"><?php $trans->__('EN'); ?></a>
</div>
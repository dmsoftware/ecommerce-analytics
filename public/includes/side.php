<?php //echo substr( $_SERVER['PHP_SELF'], 8, strlen($_SERVER['PHP_SELF']) ); 
$rutacomparacion = substr( $_SERVER['PHP_SELF'], 8, 13 ); 
//echo $rutacomparacion;
?>

<div class="sidebar-menu">

	<div class="tablet">
		<a id="menu_tablet"><i class="fa fa-caret-right"></i> </a>
		<input type="hidden" value="cerrado" id="modo_menu">
		<script type="text/javascript">
			$(document).ready(function(){

				$("#menu_tablet").on("click",function(){
					var selector = $(".page-container .sidebar-menu");
					var selector_cont = $(".contenedor_slide");
					selector_cont.stop();
					selector.stop();
					//Lo abrimos
					
					if($("#modo_menu").val()=="cerrado"){
						//flecha hacia derecha
						$("#menu_tablet").html('<i class="fa fa-caret-left"></i>')
						selector.animate({
							width: "280px"
							}, 700, function() {
								// Animación completada (calback).
								selector_cont.fadeIn(600);
								$("#modo_menu").val("abierto");
							});

					}else{
						//Flecha hacia izquierda
						$("#menu_tablet").html('<i class="fa fa-caret-right"></i>')
						selector_cont.fadeOut(600,function(){
							selector.animate({
								width: "30px"
								}, 700, function() {
									// Animación completada (calback).
									$("#modo_menu").val("cerrado");
								});

						})
						
					}				

					
				})//click menu

			})//ready
		</script>
	</div>

	<div class="contenedor_slide">

	<br />
	<?php if($rutacomparacion != "configuracion"){ ?>
		<?php

		if(isset($_COOKIE["fechaIni"]) && isset($_COOKIE["fechaFin"])) {
			$fechaIniC_f = $_COOKIE["fechaIni_f"];
			$fechaFinC_f = $_COOKIE["fechaFin_f"];
			$fechaIniC   = $_COOKIE["fechaIni"];
			$fechaFinC   = $_COOKIE["fechaFin"];
		}else{
			$fechaIniC_f = date("F j, Y", strtotime('now - 7 days'));
			$fechaFinC_f = date("F j, Y", strtotime('now - 1 days'));
			$fechaIniC   = date("Y-m-d", strtotime('now - 7 days'));
			$fechaFinC   = date("Y-m-d", strtotime('now - 1 days'));
		}

		?>
		<div class="col-sm-12">
			<p><b><?php $trans->__('Selecciona una fecha'); ?>:</b></p>		
			<div id="calendar_range" class="daterange daterange-inline add-ranges" data-format="MMMM D, YYYY" data-start-date="<?=$fechaIniC_f?>" data-end-date="<?=$fechaFinC_f?>">
				<i class="entypo-calendar"></i>
				<span style="color:black"><?=$fechaIniC_f?> - <?=$fechaFinC_f?></span>
			</div>
		</div>
		<br />
		<script type="text/javascript">
			//VARIABLES GLOVALES PARA LAS FECHAS
			strFechaInicioFiltro = $("#dat_fechaini").val();
			strFechaFinFiltro = $("#dat_fechafin").val();
			var datFechaInicioFiltro = new Date(strFechaInicioFiltro);
			var datFechaFinFiltro = new Date(strFechaFinFiltro);
			//SCRIPTS PARA FECHAS
			$(document).ready(function(){


				if($.isFunction($.fn.daterangepicker)){

					$(".daterange").each(function(i, el)
					{
						// Change the range as you desire
						var ranges = {
							'Hoy': [moment(), moment()],
							'Ayer': [moment().subtract('days', 1), moment().subtract('days', 1)],
							'Últimos 7 días': [moment().subtract('days', 6), moment()],
							'Últimos 30 días': [moment().subtract('days', 29), moment()],
							'Este mes': [moment().startOf('month'), moment().endOf('month')],
							'Mes anterior': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
						};
						
						var $this = $(el),
							opts = {
								format: attrDefault($this, 'format', 'MM/DD/YYYY'),
								timePicker: attrDefault($this, 'timePicker', false),
								timePickerIncrement: attrDefault($this, 'timePickerIncrement', false),
								separator: attrDefault($this, 'separator', ' - '),
							},
							min_date = attrDefault($this, 'minDate', ''),
							max_date = attrDefault($this, 'maxDate', ''),
							start_date = attrDefault($this, 'startDate', ''),
							end_date = attrDefault($this, 'endDate', '');
						
						if($this.hasClass('add-ranges'))
						{
							opts['ranges'] = ranges;
						}	
							
						if(min_date.length)
						{
							opts['minDate'] = min_date;
						}
							
						if(max_date.length)
						{
							opts['maxDate'] = max_date;
						}
							
						if(start_date.length)
						{
							opts['startDate'] = start_date;
						}
							
						if(end_date.length)
						{
							opts['endDate'] = end_date;
						}
						
						
						$this.daterangepicker(opts, function(start, end)
						{
							var drp = $this.data('daterangepicker');

							
							$("#dat_fechaini").val(start.format('YYYY-MM-DD'));
							$("#dat_fechafin").val(end.format('YYYY-MM-DD'));

							$.ajax({
								  type: 'POST',
								  url: "../slide_ajax_fechas.php",
								  data: {
								    fechaini_f: start.format('MMMM D, YYYY'),
									fechafin_f: end.format('MMMM D, YYYY'),   
									fechaini:   start.format('YYYY-MM-DD'),
									fechafin:   end.format('YYYY-MM-DD')  
								    },
								  dataType: 'text',
								  success: function(data){	
									 											
										mostrar_fecha();
										
										cargador();
										cargarPaises(true);
								    },
								  error: function(data){
								  	   
								  }
							})//fin ajax


							if($this.is('[data-callback]'))
							{
								callback_test(start, end);
							}
							
							if($this.hasClass('daterange-inline'))
							{
								$this.find('span').html(start.format(drp.format) + drp.separator + end.format(drp.format));
							}
						});
					});
				}

			mostrar_fecha();


			})//ready	

		</script>
		<div id="contenedor_comparacion">
			<label class="col-sm-12 control-label label-contexto"><b><?php $trans->__('Comparar datos'); ?>:</b></label>
			<div class="col-sm-12">
				<?php
				if(isset($_COOKIE["comparacionC"])) {
					$compcc = $_COOKIE["comparacionC"];
				}else{
					$compcc = 2;
				}
				?>
				<select name="test" id="combo_comparar" class="selectboxit visible" data-allow-clear="false"  data-placeholder="<?=$pais?>">
					<option value="2" <?php if($compcc == 2){echo"selected='selected'";}  ?> ><?php $trans->__('El periodo anterior'); ?></option>
					<option value="1" <?php if($compcc == 1){echo"selected='selected'";}  ?> ><?php $trans->__('El mismo periodo del año anterior'); ?></option>
					<option value="0" <?php if($compcc == 0){echo"selected='selected'";}  ?> ><?php $trans->__('No comparar'); ?></option>
				</select>
				<script type="text/javascript">
				$(document).ready(function(){

					$("#combo_comparar").change(function(){
						var valor = $(this).val();
						$("#dat_comparador").val(valor);

						$.ajax({
							  type: 'POST',
							  url: "../slide_ajax_comparaciones.php",
							  data: {
							    comparacioncc: valor
							    },
							  dataType: 'text',
							  success: function(data){	
								 	cargador();
									mostrar_fecha();
							    },
							  error: function(data){
							  	   
							  }
						})//fin ajax
						
					});

				})//ready
				</script>	
			</div>
		</div>
		<br>
		

<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		$('input.icheck').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});
		
		$('input.icheck-2').iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue'
		});
	});


	jQuery(document).ready(function($)
	{
		var icheck_skins = $(".icheck-skins a");
		
		icheck_skins.click(function(ev)
		{
			ev.preventDefault();
			
			icheck_skins.removeClass('current');
			$(this).addClass('current');
			
			updateiCheckSkinandStyle();
		});
		
		$("#icheck-style").change(updateiCheckSkinandStyle);
	});
		
	function updateiCheckSkinandStyle()
	{
		var skin = $(".icheck-skins a.current").data('color-class'),
			style = $("#icheck-style").val();
		
		var cb_class = 'icheckbox_' + style + (skin.length ? ("-" + skin) : ''),
			rd_class = 'iradio_' + style + (skin.length ? ("-" + skin) : '');
		
		if(style == 'futurico' || style == 'polaris')
		{
			cb_class = cb_class.replace('-' + skin, '');
			rd_class = rd_class.replace('-' + skin, '');
		}
		
		$('input.icheck-2').iCheck('destroy');
		$('input.icheck-2').iCheck({
			checkboxClass: cb_class,
			radioClass: rd_class
		});
}
</script>
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/minimal/_all.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/square/_all.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/flat/_all.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/futurico/futurico.css">
	<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/polaris/polaris.css">
	<script src="<?=RUTA_ABSOLUTA?>assets/js/icheck/icheck.min.js"></script>
<!--Combos de conexto-->

<!--Combo de paises-->
	
<?php


	if(isset($_COOKIE["idpais"])) {
		$idpais = $_COOKIE["idpais"];
		$pais = $_COOKIE["pais"];
	}else{
		$idpais = "todos";
		$pais = "Todos los paises";
	}

	if(isset($_COOKIE["ididioma"])) {
		$ididioma = $_COOKIE["ididioma"];
		$idioma = $_COOKIE["idioma"];
	}else{
		$ididioma = "todos";
		$idioma = "Todos los idiomas";
	}

	if(isset($_COOKIE["filtroper"])) {
		$filtroper = $_COOKIE["filtroper"];
		$filtroperdesc = $_COOKIE["filtroperdesc"];
	}else{
		$filtroper = "no";
		$filtroperdesc = "Sin ningún filtro";
	}


?>

<script type="text/javascript">
function cargarPaises(cargarCombo){

	//alert($("#dat_filtro").val());
	//Usamos cargar para saber si tiene que cargar a la fuerza o tirar se sesion si la tiene
	if(cargarCombo == true){ cargar = 1;}else{ cargar = 0;}

	$("#combo_paises").load("../slide_ajax_pais.php?proyectoasociado=" + $("#dat_proyectoasociado").val() + "&idvista=" + $("#dat_idvista").val() + "&fechaini=" + $("#dat_fechaini").val() + "&fechafin=" + $("#dat_fechafin").val() + "&cargar=" + cargar + "&filtro=" /*+ $("#dat_filtro").val()*/);

}//function

	$(document).ready(function(){
		//setTimeout(cargarPaises(false), 1200);	
		cargarPaises(false);
	})//ready

function cargarIdiomas(cargarCombo){

	//Usamos cargar para saber si tiene que cargar a la fuerza o tirar se sesion si la tiene
	if(cargarCombo == true){ cargar = 1;}else{ cargar = 0;}

	$("#combo_idiomas").load("../slide_ajax_idioma.php?proyectoasociado=" + $("#dat_proyectoasociado").val() + "&idvista=" + $("#dat_idvista").val() + "&fechaini=" + $("#dat_fechaini").val() + "&fechafin=" + $("#dat_fechafin").val() + "&cargar=" + cargar + "&filtro="/* + $("#dat_filtro").val()*/);
	//alert($("#dat_filtro").val());
}//function

	$(document).ready(function(){
		//setTimeout(cargarIdiomas(false), 1200);
		cargarIdiomas(false);
	})//ready

function cargarFiltros(cargarCombo){

	//Usamos cargar para saber si tiene que cargar a la fuerza o tirar se sesion si la tiene
	if(cargarCombo == true){ cargar = 1;}else{ cargar = 0;}

	$("#combo_filtros").load("../slide_ajax_filtro.php?idvista=" + $("#dat_idvista").val());

}//function

	$(document).ready(function(){
		//setTimeout(cargarIdiomas(false), 1200);
		cargarFiltros(false);
	})//ready
</script>


<!--<form role="form" class="form-horizontal form-groups-bordered form-contexto">	
	<div class="form-group">-->
<?php 
	if ($pageuser == "campanas" || $pageuser == "ecommerce") {
		$ocultfil = "style='display:none'";
	}else{
		$ocultfil = "style='display:block'";
	}
?>
<div id="contenedor_paises" <?=$ocultfil?> >
	<label class="col-sm-12 control-label label-contexto"><b><?php $trans->__('Selecciona un país'); ?>:</b></label>
	<div class="col-sm-12">
		<select name="test" id="combo_paises" class="selectboxit visible select2" data-allow-clear="false"  data-placeholder="<?=$trans->__($pais)?>">
			<option></option>
		</select>
		<script type="text/javascript">
		$(document).ready(function(){

			$("#combo_paises").change(function(){
				var valor = $(this).val();
				var idpais= "";
				var pais="";
				var sw=0;
				for (var i = 0; i < valor.length; i++) {
					if(sw==0){
						if (valor[i]!="*"){
							idpais+=valor[i];
						}else{
							sw=1;
						}
					}else{
						pais+=valor[i];
					}
					
				};
			
				//alert(idpais);
				//var signi = $(this).text();
				$("#pais_comparar").val(idpais);
				$("#info_pais").text(trans.__(pais));
				$.ajax({
					  type: 'POST',
					  url: '../slide_ajax_pais.php',
					  data: {
					    idpais: idpais,
					    pais:   pais          
					    },
					  dataType: 'text',
					  success: function(data){
						
						 cargador();
					    
					    },
					  error: function(){
					    $("#txt_propiedad").text("No se pudo mostrar la propiedad");
					  }
				})//fin ajax
	
			});

		})//ready
		</script>	
	</div>
</div>
	<!--</div>
</form>-->
<input type="hidden" id="pais_comparar" value="<?=$idpais ?>">

<div id="contenedor_idiomas" <?=$ocultfil?> >
	<label class="col-sm-12 control-label label-contexto"><b><?php $trans->__('Selecciona un idioma de la web'); ?>:</b></label>
	<div class="col-sm-12">
		<select name="test" id="combo_idiomas" class="selectboxit visible select2" data-allow-clear="false"  data-placeholder="<?=$trans->__($idioma)?>">
			<option></option>
		</select>
		<script type="text/javascript">
		$(document).ready(function(){

			$("#combo_idiomas").change(function(){
				var valor = $(this).val();

				var idpais= "";
				var pais="";
				var sw=0;
				for (var i = 0; i < valor.length; i++) {
					if(sw==0){
						if (valor[i]!="*"){
							idpais+=valor[i];
						}else{
							sw=1;
						}
					}else{
						pais+=valor[i];
					}
					
				};
				//alert(idpais);
				//var signi = $(this).text();
				$("#idioma_comparar").val(idpais);
				$("#info_idioma").text(trans.__(pais));
				$.ajax({
					  type: 'POST',
					  url: '../slide_ajax_idioma.php',
					  data: {
					    ididioma: idpais,
					    idioma:   pais
					    },
					  dataType: 'text',
					  success: function(data){
						
						 cargador();
					    
					    },
					  error: function(){
					    $("#txt_propiedad").text(trans.__("No se pudo mostrar la propiedad"));
					  }
				})//fin ajax
	
			});

		})//ready
		</script>
	</div>
</div>

<input type="hidden" id="idioma_comparar" value="<?=$ididioma?>">



<div id="contenedor_filtro" <?=$ocultfil?> >
	<label class="col-sm-12 control-label label-contexto"><b><?php $trans->__('Filtrado personalizado'); ?>:</b></label>
	<div class="col-sm-12">
		<select name="test" id="combo_filtros" class="selectboxit visible select2" data-allow-clear="false"  data-placeholder="<?=$trans->__($filtroperdesc)?>">
			<option></option>
		</select>
		<style type="text/css">
			.select2-search{ display: none}
		</style>
		<script type="text/javascript">
		$(document).ready(function(){
			
			$("#combo_filtros").change(function(){
				var valor = $(this).val();

				var filtro= "";
				var desc="";
				var sw=0;
				for (var i = 0; i < valor.length; i++) {
					if(sw==0){
						if (valor[i]!="#"){
							filtro+=valor[i];
						}else{
							sw=1;
						}
					}else{
						desc+=valor[i];
					}
					
				};
				//alert(filtro);
				$("#dat_filtro").val(filtro);
				$("#dat_filtr_desc").val(desc);
				$("#info_filtro").text(trans.__(desc));
				$.ajax({
					  type: 'POST',
					  url: '../slide_ajax_filtro.php',
					  data: {
					    filtroper: filtro,
					    filtroperdesc: desc
					    },
					  dataType: 'text',
					  success: function(data){
						
						 cargador();
					    
					    },
					  error: function(){
					    //$("#txt_propiedad").text("No se pudo mostrar la propiedad");
					  }
				})//fin ajax
	
			});

		});
		</script>
		
	</div>
</div>

<?php
	//echo $pageuser;
	if($pageuser == "campanas" || $pageuser == "ecommerce"){
		if($filtroper=="no"){
			$filtroper = $_COOKIE["pagefiltro"];
		}else{
			$filtroper = $_COOKIE["pagefiltro"].";".$filtroper;
		}
	}else{
		$filtroper = $filtroper;
	}
?>

<input type="hidden" id="dat_filtro" value="<?=$filtroper?>">
<input type="hidden" id="dat_filtro_desc" value="<?=$filtroperdesc?>">	

<?php 
$cat_actual = 1;
?>
<?php }else{
$cat_actual = 3;
}//Cierre de comparador de si la ruta es configuracion ?>
<!--Combo de paises-->

<?php

//Script para los resúmenes
?>
<script type="text/javascript">
	$(document).ready(function(){

		$("#ira_6").on("click",function(){

			//Recogemos las variables necesarias
			//vista
			var vista      = $("#dat_idvista").val();
			//fechaini
			var fechaini   = $("#dat_fechaini").val();
			//fechafin
			var fechafin   = $("#dat_fechafin").val();
			//tipo
			var tipo       = "Semana";
			//filtro
			var filtro     = $("#dat_filtro").val();
			//filtrodescripcion
			var filtrodesc = $("#dat_filtro_desc").val();

			self.location = "../admin/Envios/informes_resumen.php?vista="+vista+"&tipo="+tipo+"&fechaini="+fechaini+"&fechafin="+fechafin+"&fechainiant=2015-01-01&fechafinant=2015-01-20&filtro=";

		})

		$("#ira_23").on("click",function(){

			//Recogemos las variables necesarias
			//vista
			var vista      = $("#dat_idvista").val();
			//fechaini
			var fechaini   = $("#dat_fechaini").val();
			//fechafin
			var fechafin   = $("#dat_fechafin").val();
			//tipo
			var tipo       = "Mes";
			//filtro
			var filtro     = $("#dat_filtro").val();
			//filtrodescripcion
			var filtrodesc = $("#dat_filtro_desc").val();

			self.location = "../admin/Envios/informes_resumen.php?vista="+vista+"&tipo="+tipo+"&fechaini="+fechaini+"&fechafin="+fechafin+"&fechainiant=2015-01-01&fechafinant=2015-01-20&filtro=";

		})

	});
</script>

<?php
$thisUrl = $_SERVER['REQUEST_URI'];
?>

<ul id="main-menu" class="">

	<?php 
	if($rutacomparacion != "configuracion"){ 
	?>
	<?php
	$posicion_coincidencia = strpos($thisUrl, "/metricas_habituales.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/informes/metricas_habituales.php"';
	}else{
		$class ="";
		$classb ="";
		$img ="";
		$href = 'href="../public/informes/metricas_habituales.php"';
	}				
	?>
	<li class="opened prim root-level">
		<a <?=$href?> class=" <?=$classb?>">   
			<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">1</span> 
			<span class="desmenulat"><?php $trans->__('Tráfico'); ?></span>
        </a>
    </li>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/caracteristicas_usuarios.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/informes/caracteristicas_usuarios.php"';
	}else{
		$class ="";
		$classb ="";
		$img ="";
		$href = 'href="../public/informes/caracteristicas_usuarios.php"';
	}
	?>
    <li class="opened  root-level">
    	<a <?=$href?> class=" <?=$classb?>">  
    		<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">2</span> 
    		<span class="desmenulat"><?php $trans->__('Público'); ?></span>
        </a>
    </li>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/geo-localizacion.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/informes/geo-localizacion.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/informes/geo-localizacion.php"';
	}
	?>
    <li class="opened  root-level">
    	<a <?=$href?> class=" <?=$classb?>">   
        	<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">3</span> 
        	<span class="desmenulat"><?php $trans->__('Geo-localización'); ?></span>
        </a>
    </li>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/idiomas.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/informes/idiomas.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/informes/idiomas.php"';
	}
	?>
    <li class="opened  root-level">
    	<a <?=$href?> class=" <?=$classb?>">   
	    	<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">4</span> 
	    	<span class="desmenulat"><?php $trans->__('Idiomas'); ?></span>
        </a>
    </li>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/contenido_visto.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/informes/contenido_visto.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/informes/contenido_visto.php"';
	}
	?>
    <li class="opened  root-level">
    	<a <?=$href?> class=" <?=$classb?>">   
        	<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">5</span> 
        	<span class="desmenulat"><?php $trans->__('Contenidos'); ?></span>
        </a>
    </li>
    <?php
    if($primera_app == 0){
		$posicion_coincidencia = strpos($thisUrl, "/navegacion_dispositivos.php");
		if ($posicion_coincidencia != false){ 
			$class ="sel";
			$classb ="sel_lat";
			$img ="_sel";
			$href = 'href="../public/informes/navegacion_dispositivos.php"';
		}else{
			$class ="";
			$classb="";
			$img ="";
			$href = 'href="../public/informes/navegacion_dispositivos.php"';
		}
		?>
	    <li class="opened  root-level">
	    	<a <?=$href?> class=" <?=$classb?>">   
	        	<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">6</span> 
	        	<span class="desmenulat"><?php $trans->__('Dispositivos'); ?></span>
	        </a>
	    </li>
	    <?php
	}else{
		$posicion_coincidencia = strpos($thisUrl, "/aplicacion_dispositivos.php");
		if ($posicion_coincidencia != false){ 
			$class ="sel";
			$classb ="sel_lat";
			$img ="_sel";
			$href = 'href="../public/informes/aplicacion_dispositivos.php"';
		}else{
			$class ="";
			$classb="";
			$img ="";
			$href = 'href="../public/informes/aplicacion_dispositivos.php"';
		}
		?>
	    <li class="opened  root-level">
	    	<a <?=$href?> class=" <?=$classb?>">   
	        	<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">6</span> 
	        	<span class="desmenulat"><?php $trans->__('Aplicación – dispositivos'); ?></span>
	        </a>
	    </li>
	    <?php
	}
    if( $pageuser != "campanas" || $pageuser == "ecommerce"){ 
    ?>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/fuentes-entrada.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/informes/fuentes-entrada.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/informes/fuentes-entrada.php"';
	}
	?>
    <li class="opened  root-level">
    	<a <?=$href?> class=" <?=$classb?>">   
        	<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">7</span> 
        	<span class="desmenulat"><?php $trans->__('Fuentes de entrada'); ?></span>
        </a>
    </li>
    <?php
	}
    

    if( $pageuser == "campanas" || $pageuser == "ecommerce"){ 
    ?>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/terminos.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/informes/terminos.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/informes/terminos.php"';
	}
	?>
    <li class="opened  root-level">
    	<a <?=$href?> class=" <?=$classb?>">   
        	<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">7</span> 
        	<span class="desmenulat"><?php $trans->__('Términos'); ?></span>
        </a>
    </li>
    <?php
	}

    if( $pageuser == "campanas" || $pageuser == "ecommerce"){ $ultimaCampana = "ult"; }else{ $ultimaCampana = ""; }
    
    ?>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/impactos1.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/informes/impactos1.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/informes/impactos1.php"';
	}
	?>
    <li class="opened ult <?=$ultimaCampana?>  root-level">
    	<a <?=$href?> class=" <?=$classb?>">   
        	<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">8</span> 
        	<span class="desmenulat"><?php $trans->__('Conversiones'); ?></span>
        </a>
    </li>
    <?php

	}/*Si es diferente a configuracion*/else{
	?>
	<?php
	$posicion_coincidencia = strpos($thisUrl, "/perfil_usuario.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/configuracion/perfil_usuario.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/configuracion/perfil_usuario.php"';
	}
	?>
	<li class="opened prim root-level">
		<a <?=$href?> class=" <?=$classb?>">   
			<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">1</span> 
			<span class="desmenulat"><?php $trans->__('Perfil de usuario'); ?></span>
        </a>
    </li>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/permisos.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/configuracion/permisos.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/configuracion/permisos.php"';
	}
	?>
    <li class="opened  root-level">
    	<a <?=$href?> class=" <?=$classb?>">   
    		<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">2</span> 
    		<span class="desmenulat"><?php $trans->__('Permisos'); ?></span>
        </a>
    </li>
    <?php
	$posicion_coincidencia = strpos($thisUrl, "/sitioweb.php");
	if ($posicion_coincidencia != false){ 
		$class ="sel";
		$classb ="sel_lat";
		$img ="_sel";
		$href = 'href="../public/configuracion/sitioweb.php"';
	}else{
		$class ="";
		$classb="";
		$img ="";
		$href = 'href="../public/configuracion/sitioweb.php"';
	}
	?>
    <li class="opened ult root-level">
    	<a <?=$href?> class=" <?=$classb?>">   
    		<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">3</span> 
    		<span class="desmenulat"><?php $trans->__('Sitio web'); ?></span>
        </a>
    </li>

	<?php
	}
    ?>


    <?php


	?>		
</ul>
<?php




//Hayamos el id de la propiedad
if($primera_vista != ''){
	$propiedad = propiedadVista($primera_vista);
	foreach ($propiedad["elementos"] as $key => $prop) {
		$primera_propiedad = $prop["idpropiedad"];
	}
}
?>
<input type="hidden" value="<?=$primera_propiedad?>" id="dat_propiedad">
<input type="hidden" value="<?=$primera_proyectoasoc?>" id="dat_proyectoasociado">
<input type="hidden" value="<?=$primera_vista?>" id="dat_idvista">
<?php $strDominio = DominioVista($primera_vista); ?>
<input type="hidden" value="<?=$strDominio?>" id="dat_vistaurl">
<input type="hidden" value="<?=$fechaIniC?>" id="dat_fechaini">
<input type="hidden" value="<?=$fechaFinC?>" id="dat_fechafin">
<input type="hidden" value="Diario" id="dat_perspectiva">
<input type="hidden" value="<?=$compcc?>" id="dat_comparador">
<input type="hidden" value="<?=$primera_app?>" id="dat_app">

<script type="text/javascript">
$(document).ready(function(){
<?php
$ecomerce = ecomercePropiedad($primera_propiedad);
if($ecomerce == NULL || empty($ecomerce) || $ecomerce == 0){
	?>$("#menuecomerce").hide();<?php
}else{
	?>$("#menuecomerce").show();<?php
}
?>
})
</script>

<div class="copyACC">
	&copy; <?php echo date("Y"); ?> <strong>DMIntegra</strong> <?php $trans->__('un producto de'); ?> <a href="http://www.acc.com.es" target="_blank"><img src="<?=RUTA_ABSOLUTA?>images/logo-acc.png" /></a>
</div>

</div><!--contenedorslide-->
</div>	

<?php //echo substr( $_SERVER['PHP_SELF'], 8, strlen($_SERVER['PHP_SELF']) ); 
$rutacomparacion = substr( $_SERVER['PHP_SELF'], 8, 13 ); 
//echo $rutacomparacion;
?>

<div class="sidebar-menu" style="z-index: 4;">

	<div class="tablet">
		<a id="menu_tablet"><i class="fa fa-caret-right"></i> </a>
		<input type="hidden" value="cerrado" id="modo_menu">
		<script type="text/javascript">
			$(document).ready(function(){

				$("#menu_tablet").on("click",function(){
					var selector = $(".page-container .sidebar-menu");
					var selector_cont = $(".contenedor_slide");
					selector_cont.stop();
					selector.stop();
					//Lo abrimos
					
					if($("#modo_menu").val()=="cerrado"){
						//flecha hacia derecha
						$("#menu_tablet").html('<i class="fa fa-caret-left"></i>')
						selector.animate({
							width: "280px"
							}, 700, function() {
								// Animación completada (calback).
								selector_cont.fadeIn(600);
								$("#modo_menu").val("abierto");
							});

					}else{
						//Flecha hacia izquierda
						$("#menu_tablet").html('<i class="fa fa-caret-right"></i>')
						selector_cont.fadeOut(600,function(){
							selector.animate({
								width: "50px"
								}, 700, function() {
									// Animación completada (calback).
									$("#modo_menu").val("cerrado");
								});

						})
						
					}				

					
				})//click menu

			})//ready
		</script>
	</div>

	<div class="contenedor_slide" style="padding:10px;">

		<br />

		<script type="text/javascript">
			function cargarPaises(cargarCombo){

				//alert($("#dat_filtro").val());
				//Usamos cargar para saber si tiene que cargar a la fuerza o tirar se sesion si la tiene
				if(cargarCombo == true){ cargar = 1;}else{ cargar = 0;}

				$("#combo_paises").load("../slide_ajax_pais.php?proyectoasociado=" + $("#dat_proyectoasociado").val() + "&idvista=" + $("#dat_idvista").val() + "&fechaini=" + $("#dat_fechaini").val() + "&fechafin=" + $("#dat_fechafin").val() + "&paisDefault=" + $("#pais_comprar").val() + "&cargar=" + cargar + "&filtro=" + $("#dat_filtro").val());

			}//function
		</script>	

		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				$('input.icheck').iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal'
				});
				
				$('input.icheck-2').iCheck({
					checkboxClass: 'icheckbox_minimal-blue',
					radioClass: 'iradio_minimal-blue'
				});
			});


			jQuery(document).ready(function($)
			{
				var icheck_skins = $(".icheck-skins a");
				
				icheck_skins.click(function(ev)
				{
					ev.preventDefault();
					
					icheck_skins.removeClass('current');
					$(this).addClass('current');
					
					updateiCheckSkinandStyle();
				});
				
				$("#icheck-style").change(updateiCheckSkinandStyle);
			});
				
			function updateiCheckSkinandStyle()
			{
				var skin = $(".icheck-skins a.current").data('color-class'),
					style = $("#icheck-style").val();
				
				var cb_class = 'icheckbox_' + style + (skin.length ? ("-" + skin) : ''),
					rd_class = 'iradio_' + style + (skin.length ? ("-" + skin) : '');
				
				if(style == 'futurico' || style == 'polaris')
				{
					cb_class = cb_class.replace('-' + skin, '');
					rd_class = rd_class.replace('-' + skin, '');
				}
				
				$('input.icheck-2').iCheck('destroy');
				$('input.icheck-2').iCheck({
					checkboxClass: cb_class,
					radioClass: rd_class
				});
		}
		</script>

		<div id="contenedor_paises" style="display:none" >
			<label class="col-sm-12 control-label label-contexto"><b><?php $trans->__('Selecciona un país'); ?>:</b></label>
			<div class="col-sm-12">
				<select name="test" id="combo_paises" class="selectboxit visible select2" data-allow-clear="false"  data-placeholder="<?=$trans->__($pais)?>">
					<option></option>
				</select>
				<script type="text/javascript">
				$(document).ready(function(){

					$("#combo_paises").change(function(){
						var valor = $(this).val();
						var idpais= "";
						var pais="";
						var sw=0;
						for (var i = 0; i < valor.length; i++) {
							if(sw==0){
								if (valor[i]!="*"){
									idpais+=valor[i];
								}else{
									sw=1;
								}
							}else{
								pais+=valor[i];
							}
							
						};
						//alert(idpais);
						//var signi = $(this).text();
						$("#pais_comparar").val(idpais);
						$("#info_pais").text(trans.__(pais));
						$.ajax({
							  type: 'POST',
							  url: '../slide_ajax_pais.php',
							  data: {
							    idpais: idpais,
							    pais:   pais          
							    },
							  dataType: 'text',
							  success: function(data){
								
								 cargador();
							    
							    },
							  error: function(){
							    $("#txt_propiedad").text("No se pudo mostrar la propiedad");
							  }
						})//fin ajax
			
					});

				})//ready
				</script>	
			</div>
		</div>


		<ul id="main-menu" class="">
			<?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=traficoweb");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=traficoweb"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=traficoweb"';
			}				
			?>
			<li class="opened prim root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">1</span> 
					<span class="desmenulat"><?php $trans->__('Tráfico web'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=sexo");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=sexo"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=sexo"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">2</span> 
					<span class="desmenulat"><?php $trans->__('Sexo'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=edad");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=edad"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=edad"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">3</span> 
					<span class="desmenulat"><?php $trans->__('Edad'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=pais");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=pais"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=pais"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">4</span> 
					<span class="desmenulat"><?php $trans->__('País'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=region");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=region"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=region"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">5</span> 
					<span class="desmenulat"><?php $trans->__('Región'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=idiomasweb");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=idiomasweb"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=idiomasweb"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">6</span> 
					<span class="desmenulat"><?php $trans->__('Idiomas de la web'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=idiomasuser");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=idiomasuser"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=idiomasuser"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">7</span> 
					<span class="desmenulat"><?php $trans->__('Idiomas del usuario'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=contenidos");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=contenidos"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=contenidos"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">8</span> 
					<span class="desmenulat"><?php $trans->__('Estructura de contenidos'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=dispositivos");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=dispositivos"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=dispositivos"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">9</span> 
					<span class="desmenulat"><?php $trans->__('Dispositivos'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=fuentesgeneral");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=fuentesgeneral"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=fuentesgeneral"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">10</span> 
					<span class="desmenulat"><?php $trans->__('Fuentes de entrada - General'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=social");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=social"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=social"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">11</span> 
					<span class="desmenulat"><?php $trans->__('Social'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=campanas");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=campanas"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=campanas"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">12</span> 
					<span class="desmenulat"><?php $trans->__('Campañas'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=conversiones");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=conversiones"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=conversiones"';
			}				
			?>
			<li class="opened root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">13</span> 
					<span class="desmenulat"><?php $trans->__('Conversiones'); ?></span>
		        </a>
		    </li>
		    <?php
			$posicion_coincidencia = strpos($thisUrl, "/cuadromando.php?dimension=seo");
			if ($posicion_coincidencia != false){ 
				$class ="sel";
				$classb ="sel_lat";
				$img ="_sel";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=seo"';
			}else{
				$class ="";
				$classb ="";
				$img ="";
				$href = 'href="../public/cuadromando/cuadromando.php?dimension=seo"';
			}				
			?>
			<li class="opened ult root-level">
				<a <?=$href?> class=" <?=$classb?>">   
					<span class="imgmenulateral" style="background-image: url('../public/images/menu_lateral<?=$img?>.png');">14</span> 
					<span class="desmenulat"><?php $trans->__('Seo'); ?></span>
		        </a>
		    </li>
		</ul>

		<input type="hidden" id="pais_comparar" value="<?=$idpais ?>">
		<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/minimal/_all.css">
		<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/square/_all.css">
		<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/flat/_all.css">
		<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/futurico/futurico.css">
		<link rel="stylesheet" href="<?=RUTA_ABSOLUTA?>assets/js/icheck/skins/polaris/polaris.css">
		<script src="<?=RUTA_ABSOLUTA?>assets/js/icheck/icheck.min.js"></script>
		<!--Combos de conexto-->





		<input type="hidden" value="<?=$primera_propiedad?>" id="dat_propiedad">
		<input type="hidden" value="<?=$primera_proyectoasoc?>" id="dat_proyectoasociado">
		<input type="hidden" value="<?=$primera_vista?>" id="dat_idvista">
		<?php $strDominio = DominioVista($primera_vista); ?>
		<input type="hidden" value="<?=$strDominio?>" id="dat_vistaurl">
		<input type="hidden" value="<?=$fechaIniC?>" id="dat_fechaini">
		<input type="hidden" value="<?=$fechaFinC?>" id="dat_fechafin">
		<input type="hidden" value="Diario" id="dat_perspectiva">
		<input type="hidden" value="<?=$compcc?>" id="dat_comparador">
		<input type="hidden" value="<?=$primera_app?>" id="dat_app">

		<script type="text/javascript">
		$(document).ready(function(){
		<?php
		$ecomerce = ecomercePropiedad($primera_propiedad);
		if($ecomerce == NULL || empty($ecomerce) || $ecomerce == 0){
			?>$("#menuecomerce").hide();<?php
		}else{
			?>$("#menuecomerce").show();<?php
		}
		?>
		})
		</script>

		<div class="copyACC">
			&copy; <?php echo date("Y"); ?> <strong>DMIntegra</strong> <?php $trans->__('un producto de'); ?> <a href="http://www.acc.com.es" target="_blank"><img src="<?=RUTA_ABSOLUTA?>images/logo-acc.png" /></a>
		</div>

	</div><!--contenedorslide-->
</div>	

<div class="sidebar-menu">


<div class="contenedor_side_historial">


	
	<div class="contenedor_slide" style="padding: 0px 5px 5px;">
		<!--<ul class="ulcontainer">-->
			<?php
			$historial = historialUser($_COOKIE["usuario"]["email"],$primera_vista);
			//echo var_dump($historial);
			if($historial["cantidad"]==0){
				?>
				  
				  <p>No existen informes</p>
				  <script type="text/javascript">$(document).ready(function(){ $(".contenedor_informse").hide(); })</script>
				<?php
			}else{

				$dataMes  = '';
				$dataAnno = '';
				$primero  = 1;
				
				foreach ($historial["elementos"] as $key => $hist) {

					//Transformamos fecha
					$fecha_e = $hist["fechafin"];
					$fechaf  = date ( 'Y-m-d' , strtotime ($fecha_e) );
					$anno    = date ( 'Y' , strtotime ($fecha_e) );
					$mes     = date ( 'm' , strtotime ($fecha_e) );
					$idmes = intval($mes);
					$idmesb = intval($mes)-1;
					$idmesc = intval($mes)-2;
					$mesf  = traducirMes($idmes);
					$mesfb  = traducirMes($idmesb);
					$mesfc  = traducirMes($idmesc);
					$nmes  = $mesf[$idmes]["nom"];
					$nmesant  = $mesfb[$idmesb]["nom"];
					$nmesantant  = $mesfc[$idmesc]["nom"];
					$dia     = date ( 'd' , strtotime ($fecha_e) );

					$diasigsem = strtotime ( '-6 day' , strtotime ( $fecha_e ) ) ;
					$diasigsem = date ( 'd' , $diasigsem); 

					
					//CREAMOS VARIABLES 
					//Tipo de informe
					switch ($hist["tipo"]) {
						case 'INF_SEM':
							$hist_tipo       = $trans->__('Informes',false);
							$hist_tip        = 'inf';
							$hist_tipo_clase = 'hist_semb';
							//$hist_tipo_ico   = 'entypo-clipboard';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe de la semana del '.$dia;
							$hist_tipo_desc  = '/ '.$diasigsem.'-'.$dia.' / '.''.$trans->__('Semanal',false).'';
							/*'Semana del '.$dia.' de '.$nmes.' del '.$anno;*/ 
							$hist_asunt  	 = vsprintf( $trans->__('Semana del %1$s de %2$s del %3$s', false), array($diasigsem, $trans->__($nmes, false), $anno) );
							$Subcuadrado     = $dia;
							break;
						case 'INF_MEN':
							$hist_tipo = $trans->__('Informes',false);
							$hist_tip        = 'inf';
							$hist_tipo_clase = '';
							//$hist_tipo_ico   = 'entypo-drive';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe del mes de '.$nmes;
							$hist_tipo_desc  = '<b>/ '.$trans->__('Mensual',false).'</b>';
							$hist_asunt  	 = vsprintf( $trans->__('%1s del %2s', false), array($trans->__($nmes, false), $anno) );
							$Subcuadrado     = '<i class="entypo-drive"></i>';
							break;
						case 'INF_TRI':
							$hist_tipo = $trans->__('Informes',false);
							$hist_tip        = 'inf';
							$hist_tipo_clase = '';
							//$hist_tipo_ico   = 'entypo-database';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe trimestral';
							$hist_tipo_desc  = '<b>/ '.$trans->__('Trimestral',false).'</b>';
							$hist_asunt  	 = vsprintf( $trans->__('Trimestral de %1s, %2s y %3s del %4s', false), array($trans->__($nmesantant, false),$trans->__($nmesant, false),$trans->__($nmes, false), $anno) );
							$Subcuadrado     = '<i class="entypo-database"></i>';
							break;
						case 'CAMP_SEM':
							$hist_tipo       = $trans->__('Campañas',false);
							$hist_tip        = 'camp';
							$hist_tipo_clase = 'hist_semb';
							//$hist_tipo_ico   = 'entypo-clipboard';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe de la semana del '.$dia;
							$hist_tipo_desc  = '/ '.$diasigsem.'-'.$dia.'  / '.''.$trans->__('Semanal',false).'';
							$hist_asunt  	 = vsprintf( $trans->__('Semana del %1$s de %2$s del %3$s', false), array($diasigsem, $trans->__($nmes, false), $anno) );
							$Subcuadrado     = $dia;
							break;
						case 'CAMP_MEN':
							$hist_tipo = $trans->__('Campañas',false);
							$hist_tip        = 'camp';
							$hist_tipo_clase = '';
							//$hist_tipo_ico   = 'entypo-drive';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe del mes de '.$nmes;
							$hist_tipo_desc  = '<b>/ '.$trans->__('Mensual',false).'</b>';
							$hist_asunt  	 = vsprintf( $trans->__('%1s del %2s', false), array($trans->__($nmes, false), $anno) );
							$Subcuadrado     = '<i class="entypo-drive"></i>';
							break;
						case 'CAMP_TRI':
							$hist_tipo = $trans->__('Campañas',false);
							$hist_tip        = 'camp';
							$hist_tipo_clase = '';
							//$hist_tipo_ico   = 'entypo-database';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe trimestral';
							$hist_tipo_desc  = '<b>/ '.$trans->__('Trimestral',false).'</b>';
							//'Trimestral de '.$nmes.' del '.$anno;
							$hist_asunt  	 = vsprintf( $trans->__('Trimestral de %1s, %2s y %3s del %4s', false), array($trans->__($nmesantant, false),$trans->__($nmesant, false),$trans->__($nmes, false), $anno) );
							$Subcuadrado     = '<i class="entypo-database"></i>';
							break;
						case 'SEG_SEM':
							$hist_tipo       = $trans->__('Seguimiento',false);
							$hist_tip        = 'camp';
							$hist_tipo_clase = 'hist_semb';
							//$hist_tipo_ico   = 'entypo-clipboard';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe de la semana del '.$dia;
							$hist_tipo_desc  = '/ '.$diasigsem.'-'.$dia.'  / '.''.$trans->__('Semanal',false).'';
							$hist_asunt  	 = vsprintf( $trans->__('Semana del %1$s de %2$s del %3$s', false), array($diasigsem, $trans->__($nmes, false), $anno) );
							$Subcuadrado     = $dia;
							break;
						case 'SEG_MEN':
							$hist_tipo = $trans->__('Seguimiento',false);
							$hist_tip        = 'camp';
							$hist_tipo_clase = '';
							//$hist_tipo_ico   = 'entypo-drive';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe del mes de '.$nmes;
							$hist_tipo_desc  = '<b>/ '.$trans->__('Mensual',false).'</b>';
							$hist_asunt  	 = vsprintf( $trans->__('%1s del %2s', false), array($trans->__($nmes, false), $anno) );
							$Subcuadrado     = '<i class="entypo-drive"></i>';
							break;
						case 'SEG_TRI':
							$hist_tipo = $trans->__('Seguimiento',false);
							$hist_tip        = 'camp';
							$hist_tipo_clase = '';
							//$hist_tipo_ico   = 'entypo-database';
							$hist_tipo_ico   = '';
							//$hist_tipo_desc  = 'Informe trimestral';
							$hist_tipo_desc  = '<b>/ '.$trans->__('Trimestral',false).'</b>';
							//'Trimestral de '.$nmes.' del '.$anno;
							$hist_asunt  	 = vsprintf( $trans->__('Trimestral de %1s, %2s y %3s del %4s', false), array($trans->__($nmesantant, false),$trans->__($nmesant, false),$trans->__($nmes, false), $anno) );
							$Subcuadrado     = '<i class="entypo-database"></i>';
							break;
					}//fin switch tipo
					
					$suproceso =  $hist["proceso"];

					//Comprobamos si el año a cambiado si es así lo pintamos
					if($dataAnno != $anno){
						//Traducimos el mes
						$idmes = $anno;
						?>
						<!--<li class="hist_annob" data-info=""><i class=""></i> <?=$anno?></li>-->
						<p style="font-size:1.5em;color:#fff; background:#6cb5f4;">&nbsp;<i class="fa fa-calendar"></i> <b><?=$anno?></b></p> 
						<?php
						$dataAnno = $anno;
					}

					if($primero==1){
						$select = "informeseleccionado";
						$inica = "<ul>";
						$inicab = "";
						$inicac = "";
						//Partimos las imagenes para mostralas
						$cuerpo = $hist['cuerpo'];
						//Extraemos las tablas del cuerpo

						$imgp = $hist['imagen'];
						$imgp = substr( $imgp , 0, strlen($imgp)-4);
						$imgp1 = $imgp.'-1.png'; 
						$imgp2 = $imgp.'-2.png'; 
						$imgp3 = $imgp.'-3.png'; 
						$imgp4 = $imgp.'-4.png'; 
						$hist_asunt_pri = $hist_asunt;
						$primero = 0;
						$proceso = $hist["proceso"];
						$procesoImagen = $hist["proceso_imagen"];
						$imagen = $hist["pdf"];
						$agrupa = ';display:block';
						$arrow = 'fa-arrow-down';
						if($tipo == "INF_SEM" || $tipo == "INF_MEN" || $tipo == "INF_TRI"){
							$displaya = "block";
							$displayb = "none";
						}else{
							$displaya = "none";
							$displayb = "block";
						}
					}else{
						$inica = "";
						$select = "";
						$inicab = "</li></ul>";
						$inicac = "</ul>";
						$agrupa = ';display:none';
						$arrow = 'fa-arrow-up';
					}

					//Comprobamos si el mes a cambiado si es así lo pintamos
					if($dataMes != $mes){
						//Traducimos el mes
						?>
						<?=$inicab?>
						<?=$inicac?>
						<p class="hist_mesb_grupo" data-selector="<?=$nmes?>" style="border-bottom:solid 1px #cdcdcd; cursor:pointer">&nbsp;&nbsp;<i class="fa fa-calendar-o"></i> <b><?=$trans->__(ucwords($nmes),false)?></b>
			  			<i class="fa <?=$arrow?>" style="font-size:.7em; float:right; margin:3px 7px 0 0; color:#cdcdcd;"></i></p>  
			  			<ul id="informes_<?=$nmes?>" style="margin:0 0 15px 15px; padding:0; list-style:none; line-height:170% <?=$agrupa?>">
						<?php
						$dataMes = $mes;
						$primerob = 1;
						$gurp = $hist_tipo;
					}

					if($primerob == 1){	
						?>
						<li>
							<p class="hist_desc_grupo" data-selector="<?php echo $primerob.$nmes; ?>" style="border-bottom:solid 1px #cdcdcd; cursor:pointer">&nbsp;&nbsp;<i class="fa fa-file-text-o"></i> <b><?=$hist_tipo?></b>
				  			<i class="fa fa-arrow-down" style="font-size:.7em; float:right; margin:3px 7px 0 0; color:#cdcdcd;"></i></p>
				  			<ul id="g<?php echo $primerob.$nmes; ?>"> 
								<li class="<?=$hist_tipo_clase?> opcinfor <?=$select?>" style="margin-left: 14px;" data-proceso="<?=$suproceso?>" data-tipo="<?=$hist_tip?>" data-info="<?=$hist['imagen']?>" data-asunt="<?=$hist_asunt?>" data-proceso="<?=$hist['proceso']?>" data-imagen="<?=$hist['pdf']?>" data-procesoimagen="<?=$hist['proceso_imagen']?>"><?=$hist_tipo_desc?></li>
						
						<?php
						$primerob++;
					}else{
						//echo $hist_tipo .'  -  '. $gurp;
						if($hist_tipo != $gurp){
							?>
								</ul>
							</li>
							<li>
								
								<p class="hist_desc_grupo" data-selector="<?php echo $primerob.$nmes; ?>" style="border-bottom:solid 1px #cdcdcd; cursor:pointer">&nbsp;&nbsp;<i class="fa fa-file-text-o"></i> <b><?=$hist_tipo?></b>
					  			<i class="fa fa-arrow-down" style="font-size:.7em; float:right; margin:3px 7px 0 0; color:#cdcdcd;"></i></p> 
					  			<ul id="g<?php echo $primerob.$nmes; ?>">
									<li class="<?=$hist_tipo_clase?> opcinfor <?=$select?>" style="margin-left: 14px;" data-proceso="<?=$suproceso?>" data-tipo="<?=$hist_tip?>" data-info="<?=$hist['imagen']?>" data-asunt="<?=$hist_asunt?>" data-proceso="<?=$hist['proceso']?>" data-imagen="<?=$hist['pdf']?>" data-procesoimagen="<?=$hist['proceso_imagen']?>"><?=$hist_tipo_desc?></li>
							
							<?php
							$gurp = $hist_tipo ;
						}else{
							?>
							<li class="<?=$hist_tipo_clase?> opcinfor <?=$select?>" style="margin-left: 14px;" data-proceso="<?=$suproceso?>" data-tipo="<?=$hist_tip?>" data-info="<?=$hist['imagen']?>" data-asunt="<?=$hist_asunt?>" data-proceso="<?=$hist['proceso']?>" data-imagen="<?=$hist['pdf']?>" data-procesoimagen="<?=$hist['proceso_imagen']?>"><?=$hist_tipo_desc?></li>
							<?php
						}

					}
					//echo $inica;
					?>
	
					<?php
				}//foeach
				?>
					</li>
				</ul>
				<?php

			}
			
			?>
									
		</ul>
		<?php 

		$propiedad = propiedadVista($primera_vista);
		foreach ($propiedad["elementos"] as $key => $prop) {
			$primera_propiedad = $prop["idpropiedad"];
		}

		?>




		<script type="text/javascript">
		$(document).ready(function(){
		<?php
		$ecomerce = ecomercePropiedad($primera_propiedad);
		if($ecomerce == NULL || empty($ecomerce) || $ecomerce == 0){
			?>$("#menuecomerce").hide();<?php
		}else{
			?>$("#menuecomerce").show();<?php
		}
		?>
		})
		</script>


		<br>
		


	</div>



	<style type="text/css">
		.contenedor_side_historial { display: block; overflow-x: auto; margin-top: 45px;}
		/*.contenedor_side_historial ul{ margin: 0px; padding: 0px; list-style: none;  display: block;overflow: auto; border-left: 3px solid black; margin-left: 10px;}
		.contenedor_side_historial ul li{padding: 5px; margin-bottom: 5px; background-repeat: no-repeat; background-position: left center; cursor: pointer;}*/
		.contenedor_side_historial ul{ margin: 0px; padding: 0px; list-style: none;  display: block;overflow: auto;}
		.contenedor_side_historial ul.ulcontainer{  margin-left: 10px; margin-right: 10px;}
		/*.contenedor_side_historial ul li{padding: 5px; margin-bottom: 5px; cursor: pointer;}*/
		.contenedor_side_historial .hist_mes{ font-size: 20px; background-image: url(../images/linea_p.jpg); padding-left: 12px; font-weight: bold; color: #3B3B3B; background-size: 10px 4px; }
		.contenedor_side_historial .hist_infor_tri{ font-size: 16px; background-image: url(../images/linea_p.jpg); padding-left: 14px; color: #3B3B3B; background-size: 10px 4px; }
		.contenedor_side_historial .hist_infor_men{ font-size: 14px; background-image: url(../images/linea_p.jpg); padding-left: 12px; color: #3B3B3B; background-size: 8px 4px; }
		.contenedor_side_historial .hist_infor_sem{ font-size: 12px; background-image: url(../images/linea_p.jpg); padding-left: 10px; color: #3B3B3B; background-size: 6px 4px; }

		.contenedor_side_historial .hist_infor_tri:hover{ background-image: url(../images/linea_p.jpg); color: #3B3B3B; background-size: 10px 7px; font-weight: bold; }
		.contenedor_side_historial .hist_infor_men:hover{ background-image: url(../images/linea_p.jpg); color: #3B3B3B; background-size: 8px 7px; font-weight: bold;}
		.contenedor_side_historial .hist_infor_sem:hover{ background-image: url(../images/linea_p.jpg); color: #3B3B3B; background-size: 6px 7px; font-weight: bold;}
		.informeseleccionado{color: #3B3B3B; background-size: 10px 7px; font-weight: bold; }


		#menus_tablet{top: 0px !important;}
		.contenedor_side_historial .hist_mesb{ background-color: #3FABE2; color: white; font-weight: bold;font-size: 15px; padding: 4px;border-top: 1px solid #9FC9FF; margin-bottom: 0px;}
		.contenedor_side_historial .hist_annob{ background-color: #3FABE2; color: white; text-align: center; font-weight: bold; font-size: 21px;padding: 4px; border-top: 1px solid #9FC9FF;}
		.mdia{ background-color: #3FABE2;  color: white; width: 30px; height: 30px;display: block; float: left;}
		.hist_mesb i{font-size: 16px;padding-right: 4px; }
		.infsem{ padding: 6px;
				font-size: 13px;
				display: block;
				float: left;}
		.has-sub ul li{ height: 30px;overflow: auto;display: block;}
		.has-sub ul li .mdia { padding: 6px 7px; border-top: 1px solid #9FC9FF;}

		.opcinfor{ cursor: pointer;}
		.opcinfor:hover{ color: #3B3B3B; background-size: 10px 7px; font-weight: bold;}

		.page-container .main-content{ width: 100%; float: right;}
		.page-container .sidebar-menu{ width: 160px; z-index: 9}
		@media (min-width: 765px) and (max-width: 1200px) {
			.page-container .main-content{ width: 85%; float: right;}
			.page-container .sidebar-menu{ width: 160px; z-index: 9} /*280px;*/
			.contenedor_slide {
			    display: block;
			}
		}

	</style>
	<script type="text/javascript">
		$(document).ready(function(){

			estructurando()
		    $( window ).resize(function() {
		        estructurando()
		    }); 
			
			$(".opcinfor").on("click",function(){

				if (!$(this).hasClass('informeseleccionado')){
					$(".conjinformes").stop();
					var asunt   = $(this).attr("data-asunt");
					var proceso = $(this).attr("data-proceso");
					var imagen  = $(this).attr("data-imagen");
					var procesoimagen = $(this).attr("data-procesoimagen");
					var ruta    = $(this).attr("data-info");
					var tipo    = $(this).attr("data-tipo");
					//var proceso    = $(this).attr("data-proceso");
					var cadena  =  ruta.substr(0,ruta.length-4);
					//alert(tipo)
					if(tipo == 'inf'){
						$(".conjcampanas").fadeOut(300);
						$(".conjinformes").fadeOut(300,function(){
							$("#cab_historial_tit").text(asunt);
							$("#btn_pdf").attr("data-proceso",proceso);
							$("#btn_pdf").attr("data-imagen",imagen);
							$("#btn_pdf").attr("data-procesoimagen",procesoimagen);
							$("#parteImg1").attr("src",cadena+'-1.png');
							$("#parteImg2").attr("src",cadena+'-2.png');
							$("#parteImg3").attr("src",cadena+'-3.png');
							$("#parteImg4").attr("src",cadena+'-4.png');
							$(".conjinformes").delay(300);
							$(".conjinformes").fadeIn(600);
						})
					}else{
						$(".conjinformes").fadeOut(300);
						$(".conjcampanas").fadeOut(300,function(){
							$("#cab_historial_tit").text(asunt);
							$("#btn_pdf").attr("data-proceso",proceso);
							$("#btn_pdf").attr("data-imagen",imagen);
							$("#btn_pdf").attr("data-procesoimagen",procesoimagen);
							
							//Ajax
							$.ajax({
						        type: 'POST',
						        url: 'historial_ajax_campanas.php',
						        data: {
						          proc:    proceso 
						          },
						        dataType: 'text',
						        success: function(data){
						        	//alert(data);
						        	$(".conjcampanas_cont").html(data);
						          
						          },
						        error: function(data){
						          	alert('Error: '+data);
						        }
						    })


							$(".conjcampanas").delay(300);
							$(".conjcampanas").fadeIn(600);
						})
					}


					
					
					$(".contenedor_side_historial ul li").removeClass("informeseleccionado");
					$(this).addClass("informeseleccionado");
				}

			})


			$(".hist_mesb_grupo").on("click",function(){
				var mes = $(this).attr("data-selector");
				$("#informes_"+mes).stop();
				if($("#informes_"+mes).css("display")=="none"){
					$("#informes_"+mes).slideDown(600);
					$(this).children("i.fa-arrow-up").removeClass("fa-arrow-up").addClass("fa-arrow-down")
				}else{
					$("#informes_"+mes).slideUp(600);
					$(this).children("i.fa-arrow-down").removeClass("fa-arrow-down").addClass("fa-arrow-up")
				}	
			})

			$(".hist_desc_grupo").on("click",function(){
				var mes = $(this).attr("data-selector");
				$("#g"+mes).stop();
				if($("#g"+mes).css("display")=="none"){
					$("#g"+mes).slideDown(600);
					$(this).children("i.fa-arrow-up").removeClass("fa-arrow-up").addClass("fa-arrow-down")
				}else{
					$("#g"+mes).slideUp(600);
					$(this).children("i.fa-arrow-down").removeClass("fa-arrow-down").addClass("fa-arrow-up")
				}	
			})

		})

		function estructurando(){
			var altura = $(window).height()-165;
			$(".contenedor_side_historial").css({"height": altura+"px"});
		}
	</script>
	</div>

</div>	

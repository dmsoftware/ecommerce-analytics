<?php
////////////////////////////////////////////////////////////////////////////
//                           FUNCIONES DE LOGIN                           //
////////////////////////////////////////////////////////////////////////////

function sec_session_start() {
		//session_save_path(HostingDirURL());    //ESTA FUNCI�N ES SOLO PARA QUE EL CODIGO FUNCIONE EN GOODADY
    $session_name = 'sec_session_id'; // Creamos un nombre para nuestra sesion
    $secure = false; // Lo activamos si usamos https.
    $httponly = true; // Esto detiene javascript poder acceder a la identificaci�n de la sesi�n. 
    //ini_set("session.use_trans_sid","0");
    ini_set('session.use_only_cookies', 1); // Fuerza la sesion a usar solo cookies. 
    $cookieParams = session_get_cookie_params(); // Sesiones que s�lo usen cookies
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
    session_name($session_name); //Establece el nombre de la sesi�n a la establecida anteriormente
    session_start(); // Inicia la sesion start
    //session_regenerate_id(true);  regenera la sesion y elimina la vieja.     
}


//Funcion que te devuelve "correcto" si has conseguido logearte
function login($email, $password, $c, $conectado) {
  
  //Utilizaci�n de sentencias preparadas significa que la inyecci�n de SQL no es posible. 
  if($stmt = $c->prepare("SELECT LOCU_CODIGO, LOCU_EMAIL, LOCU_PASSWORD, LOCU_SALT, LOCU_IDIOMA FROM dbo.LOC_USUARIOS WHERE LOCU_EMAIL = :username")){
    $result = $stmt->execute(array(':username' => $email));
  
    //Si existe
    if ($result && $stmt->rowCount() != 0) {

        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $user_id     = $fila[0];
          $usuario     = $fila[1];
          $db_password = $fila[2];
          $salt        = $fila[3];
          $idiomaU     = $fila[4];
          $count++;
        }

        $password = hash('sha512', $password.$salt);
        $totalfilas = $count;

        if(checkbrute($user_id, $c) == true) { 
            return "Ha hecho demasiados intentos de inicio de sesi�n, int�ntelo mas tarde.";
         }else{
            //return "Pass: ".$db_password."<br>Pass: ".$password;
            if($db_password == $password) { 
                $user_browser = $_SERVER['HTTP_USER_AGENT']; 
                $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                //$usuario = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $usuario); 
                $login_string = hash('sha512', $password.$user_browser);
                if ($conectado==1){
                  setcookie( "usuario[user_id]", $user_id, time() + (86400 * 30), "/"); //86400 es un dia
                  setcookie( "usuario[email]", $usuario, time() + (86400 * 30), "/"); //86400 es un dia
                  setcookie( "usuario[login_string]", $login_string, time() + (86400 * 30), "/"); //86400 es un dia
                  setcookie( "usuario[conect]", 1, time() + (86400 * 30), "/"); //86400 es un dia
                  setcookie( "userpage", 'informes', time() + (86400 * 30), "/"); //86400 es un dia
                  setcookie( "idioma_usuario", $idiomaU, time() + (86400 * 30), "/"); //86400 es un dia
                  
                }else{
                  setcookie( "usuario[user_id]", $user_id, time() + 3600, "/"); //3600 una hora
                  setcookie( "usuario[email]", $usuario, time() + 3600, "/"); //3600 una hora
                  setcookie( "usuario[login_string]", $login_string, time() + 3600, "/"); //3600 una hora
                  setcookie( "usuario[conect]", 0, time() + 3600, "/"); //3600 una hora
                  setcookie( "userpage", 'informes', time() + (3600 * 30), "/"); //3600 es un dia
                  setcookie( "idioma_usuario", $idiomaU, time() + (3600 * 30), "/"); //3600 es un dia
                }
                
                
                /*$_SESSION['user_id'] = $user_id; 
                
                $_SESSION['email'] = $usuario;
                $_SESSION['login_string'] = hash('sha512', $password.$user_browser);  */
                return "correcto";                
              }else{
                $now = time();
                $stmt = $c->prepare("INSERT INTO dbo.LOC_LOGIN_ATTEMPTS (LOCU_CODIGO, LOCU_TIME) VALUES ( :coduser , :fecha )");
                $stmt->execute(array(':coduser' => $user_id, ':fecha' => $now));
                return "La contrase�a es erronea.";
              }

         }

    }else{
      $totalfilas = 0;
      return "El email introducido no est� registrado.";
    }

  }//fin si la sentincia esta preparada
  
}


//Funcion para comprobar las veces que se ha intentado logear
function checkbrute($user_id, $c) {
   // Tremos el tiempo actual
   $now = time();
   // Recoje la fecha de hace 2 horas para compararla
   $valid_attempts = $now - (2 * 60 * 60); 
 
   if($stmt = $c->prepare("SELECT LOCU_TIME FROM dbo.LOC_LOGIN_ATTEMPTS WHERE LOCU_CODIGO = :usuario AND LOCU_TIME > :validacion")){
     $result = $stmt->execute(array(':usuario' => $user_id, ':validacion' => $valid_attempts));
      if ($result && $stmt->rowCount() != 0) {
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $count++;
        }
        if($count > INTENTOS_CONEXION){
          return true;
        }else{
          return false;
        }
      }else{
        return false;
      }
   }

}

//Funcion para comprobar si est� conectado VARIABLES DE SESSION
/*function login_check($c) {
   // chekeamos las variables de sesi�n
   if(isset($_SESSION['user_id'], $_SESSION['email'], $_SESSION['login_string'])) {
     $user_id = $_SESSION['user_id'];
     $login_string = $_SESSION['login_string'];
     $username = $_SESSION['email'];
 
     $user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.

     if($stmt = $c->prepare("SELECT LOCU_PASSWORD FROM dbo.LOC_USUARIOS WHERE LOCU_CODIGO = :iduser")){
      $result = $stmt->execute(array(':iduser' => $user_id));
  
     //Si existe
     if ($result && $stmt->rowCount() != 0) {
 
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {$password = $fila[0];}
          
           $login_check = hash('sha512', $password.$user_browser);
           if($login_check == $login_string) {
              // CONECTADO!!!!
              return true;
           } else {
              // Not conectado
              return false;
           }
        } else {
            // Not conectado
            return false;
        }
     } else {
        // Not conectado
        return false;
     }
   } else {
     // Not conectado
     return false;
   }
}*/

//Funcion para comprobar si est� conectado VARIABLES DE COOKIES
function login_check($c) {
   // chekeamos las variables de sesi�n
   if(isset($_SESSION['user_id'], $_SESSION['email'], $_SESSION['login_string'])) {
     $user_id = $_SESSION['user_id'];
     $login_string = $_SESSION['login_string'];
     $username = $_SESSION['email'];
 
     $user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.

     if($stmt = $c->prepare("SELECT LOCU_PASSWORD FROM dbo.LOC_USUARIOS WHERE LOCU_CODIGO = :iduser")){
      $result = $stmt->execute(array(':iduser' => $user_id));
  
     //Si existe
     if ($result && $stmt->rowCount() != 0) {
 
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {$password = $fila[0];}
          
           $login_check = hash('sha512', $password.$user_browser);
           if($login_check == $login_string) {
              // CONECTADO!!!!
              return true;
           } else {
              // Not conectado
              return false;
           }
        } else {
            // Not conectado
            return false;
        }
     } else {
        // Not conectado
        return false;
     }
   } else {
     // Not conectado
     return false;
   }
}

//Funcion para mostrar las vistas de un usuario
function UsuariosVistas($useremail){
  $devuelve = array();
  $c = Nuevo_PDO();
  if($stmt = $c->prepare("SELECT vu.LOCDVU_ANALYTICS_ID_VISTA, vu.LOCDVU_CODIGO, vu.LOCDVU_ENVIOEMAILSEMANAL, vu.LOCDVU_USUARIO
                          FROM dbo.LOC_USUARIOS as u JOIN  dbo.LOC_DOMINIOS_VISTAS_USUARIOS as vu ON u.LOCU_EMAIL = vu.LOCDVU_USUARIO 
                          WHERE LOCU_EMAIL = :email")){
      $result = $stmt->execute(array(':email' => $useremail));
      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $count++;
          $id = $fila[0];
          $devuelve["elementos"][$id]["id_vista"]     = $fila[0];
          $devuelve["elementos"][$id]["id_codigo"]    = $fila[1];
          $devuelve["elementos"][$id]["emailsemanal"] = $fila[2];
          $devuelve["elementos"][$id]["email_user"]   = $fila[3];
        }
        $devuelve["cantidad"]=$count;
      }else{
        $devuelve["cantidad"]=0;
      }
  }
  return $devuelve;
}

//Funcion para mostrar los datos de un usuario
function UsuariosData($useremail){
  $devuelve = array();
  $c = Nuevo_PDO();
  if($stmt = $c->prepare(" SELECT LOCU_NOMBRE,LOCU_APELLIDOS,LOCU_SEXO,LOCU_IDIOMA,LOCU_CODIGO
                FROM dbo.LOC_USUARIOS
                WHERE LOCU_EMAIL = :email")){
      $result = $stmt->execute(array(':email' => $useremail));
      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $count++;
          $id = $fila[0];
          $devuelve["elementos"]["nombre"]       = $fila[0];
          $devuelve["elementos"]["apellidos"]    = $fila[1];
          $devuelve["elementos"]["sexo"]         = $fila[2];
          $devuelve["elementos"]["idioma"]       = $fila[3];
          $devuelve["elementos"]["codigo"]       = $fila[4];
        }
        $devuelve["cantidad"]=$count;
      }else{
        $devuelve["cantidad"]=0;
      }
  }
  return $devuelve;
}

function editarUser($email,$nombre,$apellidos,$sexo,$idioma){
  $c = Nuevo_PDO();
  $sql = " UPDATE dbo.LOC_USUARIOS SET LOCU_NOMBRE = :nombre, LOCU_APELLIDOS = :apellidos, LOCU_SEXO = :sexo, LOCU_IDIOMA = :idioma
        WHERE LOCU_EMAIL = :email";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':nombre' => $nombre, ':apellidos' => $apellidos, ':sexo' => $sexo, ':idioma' => $idioma, ':email' => $email)); 

  return true;
}

//Funcion para comprobar si una vista es correcta confrme al usuario
function ValidarVista($email,$vista,$com_emails){
  $c = Nuevo_PDO();
  $devuelve = true;
  $maildom = dominioEmail($email,$com_emails);
  if(!comDominiosEmails($maildom,$com_emails)){
    $sql= "SELECT LOCDVU_ANALYTICS_ID_VISTA
           FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS
           WHERE LOCDVU_USUARIO = :user AND LOCDVU_ANALYTICS_ID_VISTA = :vista";
    $stmt = $c->prepare($sql);
    $result = $stmt->execute(array(':user' => $email, ':vista' => $vista)); 
    //Si existe
    if ($result && $stmt->rowCount() != 0) {
      $devuelve = true;
    }else {
      $devuelve = false;
    }
  }

  return $devuelve;
}


//Funcion para mostrar las vistas de un usuario
function UsuariosPropiedadesVistas($useremail,$com_emails,$buscador){

  if(!empty($buscador)){
    $buscar  = " WHERE D.LOCD_ANALYTICS_NAME_PROPIEDAD LIKE '%".$buscador."%' OR V.LOCDV_ANALYTICS_NAME_VISTA LIKE '%".$buscador."%' OR LOCDV_ANALYTICS_URL_VISTA LIKE '%".$buscador."%' ";
    $buscarb = " AND ( D.LOCD_ANALYTICS_NAME_PROPIEDAD LIKE '%".$buscador."%' OR V.LOCDV_ANALYTICS_NAME_VISTA LIKE '%".$buscador."%' OR LOCDV_ANALYTICS_URL_VISTA LIKE '%".$buscador."%')";
  }else{
    $buscar  = "";
    $buscarb = "";
  }

  $devuelve = array();
  $c = Nuevo_PDO();
  $maildom = dominioEmail($useremail);
  if(comDominiosEmails($maildom,$com_emails)){
    $anadido=""; 
    $agrupacion ="GROUP BY VU.LOCDVU_ANALYTICS_ID_VISTA, V.LOCDV_ANALYTICS_NAME_VISTA, 
V.LOCDV_SUDOMINIO, D.LOCD_ANALYTICS_ID_PROPIEDAD, D.LOCD_ANALYTICS_NAME_PROPIEDAD, V.LOCDV_ANALYTICS_DEFAULT_VISTA,
D.LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO";
    $sql = "SELECT V.LOCDV_ANALYTICS_ID_VISTA, V.LOCDV_ANALYTICS_NAME_VISTA, 
                   V.LOCDV_SUDOMINIO, D.LOCD_ANALYTICS_ID_PROPIEDAD, D.LOCD_ANALYTICS_NAME_PROPIEDAD, V.LOCDV_ANALYTICS_DEFAULT_VISTA,
                   D.LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO,LOCDV_ANALYTICS_URL_VISTA, D.LOCD_ANALYTICS_PROPIEDAD_APPMOBILE
            FROM dbo.LOC_DOMINIOS_VISTAS AS V
              JOIN dbo.LOC_DOMINIOS AS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
            ".$buscar."
            ORDER BY D.LOCD_ANALYTICS_NAME_PROPIEDAD ASC, V.LOCDV_ANALYTICS_DEFAULT_VISTA DESC, V.LOCDV_ANALYTICS_NAME_VISTA ASC";
  }else{

    $anadido = "a�adido";
    $d = Nuevo_PDO();
    //Primero comprobamos que sea admin de alguna vista y le mostramos todas sus vistas
    $sql = 'SELECT V.LOCDV_ANALYTICS_ID_VISTA, V.LOCDV_ANALYTICS_NAME_VISTA, 
                   V.LOCDV_SUDOMINIO, D.LOCD_ANALYTICS_ID_PROPIEDAD, D.LOCD_ANALYTICS_NAME_PROPIEDAD, V.LOCDV_ANALYTICS_DEFAULT_VISTA,
                   D.LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO,LOCDV_ANALYTICS_URL_VISTA, D.LOCD_ANALYTICS_PROPIEDAD_APPMOBILE
            FROM dbo.LOC_DOMINIOS_VISTAS AS V
                  JOIN dbo.LOC_DOMINIOS AS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
                  INNER JOIN ISP_DOMINIOS ON D.LOCD_DOMINIO=ISP_DOMINIOS.DOM_CODIGO
                  INNER JOIN CLIENTES as CLI ON ISP_DOMINIOS.DOM_SUCLIENTE=CLI.CLIE_CODIGO 
                  INNER JOIN LOC_USUARIOS_CLIENTES AS CA ON CA.LOCUC_CLIENTE = CLI.CLIE_CODIGO 
            WHERE CA.LOCUC_USUARIO = :email  AND CA.LOCUC_ESADMINISTRADOR = 1  
            ORDER BY D.LOCD_ANALYTICS_NAME_PROPIEDAD ASC, V.LOCDV_ANALYTICS_DEFAULT_VISTA DESC, V.LOCDV_ANALYTICS_NAME_VISTA ASC';
    if($stmtd = $d->prepare($sql)){

      $resultd = $stmtd->execute(array(':email' => $useremail));
      if ($resultd && $stmtd->rowCount() == 0){
        
        /*SI no es admin de nada ejecutamos esta select*/
          $agrupacion ="";
          $anadido="WHERE VU.LOCDVU_USUARIO = :email";
          $sql= "SELECT VU.LOCDVU_ANALYTICS_ID_VISTA, V.LOCDV_ANALYTICS_NAME_VISTA, 
                   V.LOCDV_SUDOMINIO, D.LOCD_ANALYTICS_ID_PROPIEDAD, D.LOCD_ANALYTICS_NAME_PROPIEDAD, V.LOCDV_ANALYTICS_DEFAULT_VISTA,
                   D.LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO,LOCDV_ANALYTICS_URL_VISTA, D.LOCD_ANALYTICS_PROPIEDAD_APPMOBILE
                 FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS AS VU 
                       JOIN dbo.LOC_DOMINIOS_VISTAS AS V ON VU.LOCDVU_ANALYTICS_ID_VISTA = V.LOCDV_ANALYTICS_ID_VISTA
                       JOIN dbo.LOC_DOMINIOS AS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
                 ".$anadido.$buscarb."
                 ".$agrupacion."
                 ORDER BY D.LOCD_ANALYTICS_NAME_PROPIEDAD ASC, V.LOCDV_ANALYTICS_DEFAULT_VISTA DESC, V.LOCDV_ANALYTICS_NAME_VISTA, D.LOCD_ANALYTICS_ID_PROPIEDAD ASC";
                 // D.LOCD_ANALYTICS_NAME_PROPIEDAD ASC,   
        /*************/

      }
      
    }
        



  
  }

  if($stmt = $c->prepare($sql)){
      if($anadido!=""){
        $result = $stmt->execute(array(':email' => $useremail));
      }else{
        $result = $stmt->execute();
      }   
      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $count++;
          $id = $fila[0];
          //$devuelve["elementos"][$id]["id_vista"]                   = $fila[0];
          $devuelve["elementos"][$id]["id_vista_analytics"]         = $fila[0];
          //$devuelve["elementos"][$id]["email_usuario"]              = $fila[2];
          $devuelve["elementos"][$id]["name_vista_analytics"]       = $fila[1];
          $devuelve["elementos"][$id]["id_propiedad"]               = $fila[2];
          $devuelve["elementos"][$id]["id_propiedad_analytics"]     = $fila[3];
          $devuelve["elementos"][$id]["name_propiedad_analytics"]   = $fila[4];
          $devuelve["elementos"][$id]["analytics_default_vista"]    = $fila[5];
          $devuelve["elementos"][$id]["analytics_proyectoasociado"] = $fila[6];
          $devuelve["elementos"][$id]["vista_url"]                  = $fila[7];
          if(empty($fila[8]) OR $fila[8] == "" OR $fila[8] == 0 OR $fila[8] == 0){ $filapp = 0; }else{ $filapp = $fila[8]; }
          $devuelve["elementos"][$id]["app"]                        = $filapp;
        }
        $devuelve["cantidad"]=$count;
      }else{
        $devuelve["cantidad"]=0;
      }
  }
  return $devuelve;
}

//Funcion para mostrar las vistas de un usuario
function UsuariosPropiedadesVistasb($useremail,$com_emails,$buscador){

  if(!empty($buscador)){
    $buscar  = " WHERE D.LOCD_ANALYTICS_NAME_PROPIEDAD LIKE '%".$buscador."%' OR V.LOCDV_ANALYTICS_NAME_VISTA LIKE '%".$buscador."%' OR LOCDV_ANALYTICS_URL_VISTA LIKE '%".$buscador."%' ";
    $buscarb = " AND ( D.LOCD_ANALYTICS_NAME_PROPIEDAD LIKE '%".$buscador."%' OR V.LOCDV_ANALYTICS_NAME_VISTA LIKE '%".$buscador."%' OR LOCDV_ANALYTICS_URL_VISTA LIKE '%".$buscador."%')";
  }else{
    $buscar  = "";
    $buscarb = "";
  }

  $devuelve = array();
  $c = Nuevo_PDO();
  $maildom = dominioEmail($useremail);
  if(comDominiosEmails($maildom,$com_emails)){
    $anadido=""; 
    $agrupacion ="GROUP BY VU.LOCDVU_ANALYTICS_ID_VISTA, V.LOCDV_ANALYTICS_NAME_VISTA, 
V.LOCDV_SUDOMINIO, D.LOCD_ANALYTICS_ID_PROPIEDAD, D.LOCD_ANALYTICS_NAME_PROPIEDAD, V.LOCDV_ANALYTICS_DEFAULT_VISTA,
D.LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO";
    $sql = "SELECT V.LOCDV_ANALYTICS_ID_VISTA, V.LOCDV_ANALYTICS_NAME_VISTA, 
                   V.LOCDV_SUDOMINIO, D.LOCD_ANALYTICS_ID_PROPIEDAD, D.LOCD_ANALYTICS_NAME_PROPIEDAD, V.LOCDV_ANALYTICS_DEFAULT_VISTA,
                   D.LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO,LOCDV_ANALYTICS_URL_VISTA, D.LOCD_ANALYTICS_PROPIEDAD_APPMOBILE
            FROM dbo.LOC_DOMINIOS_VISTAS AS V
              JOIN dbo.LOC_DOMINIOS AS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
            ".$buscar."
            ORDER BY D.LOCD_ANALYTICS_NAME_PROPIEDAD ASC, V.LOCDV_ANALYTICS_DEFAULT_VISTA DESC, V.LOCDV_ANALYTICS_NAME_VISTA ASC";
  }else{

    $agrupacion ="";
    $anadido="WHERE VU.LOCDVU_USUARIO = :email";
   $sql= "SELECT VU.LOCDVU_ANALYTICS_ID_VISTA, V.LOCDV_ANALYTICS_NAME_VISTA, 
             V.LOCDV_SUDOMINIO, D.LOCD_ANALYTICS_ID_PROPIEDAD, D.LOCD_ANALYTICS_NAME_PROPIEDAD, V.LOCDV_ANALYTICS_DEFAULT_VISTA,
             D.LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO,LOCDV_ANALYTICS_URL_VISTA, D.LOCD_ANALYTICS_PROPIEDAD_APPMOBILE
           FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS AS VU 
                 JOIN dbo.LOC_DOMINIOS_VISTAS AS V ON VU.LOCDVU_ANALYTICS_ID_VISTA = V.LOCDV_ANALYTICS_ID_VISTA
                 JOIN dbo.LOC_DOMINIOS AS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
           ".$anadido.$buscarb."
           ".$agrupacion."
           ORDER BY D.LOCD_ANALYTICS_NAME_PROPIEDAD ASC, V.LOCDV_ANALYTICS_DEFAULT_VISTA DESC, V.LOCDV_ANALYTICS_NAME_VISTA, D.LOCD_ANALYTICS_ID_PROPIEDAD ASC";
           // D.LOCD_ANALYTICS_NAME_PROPIEDAD ASC,     
  }

  if($stmt = $c->prepare($sql)){
      if($anadido!=""){
        $result = $stmt->execute(array(':email' => $useremail));
      }else{
        $result = $stmt->execute();
      }   
      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $count++;
          $id = $fila[0];
          //$devuelve["elementos"][$id]["id_vista"]                   = $fila[0];
          $devuelve["elementos"][$id]["id_vista_analytics"]         = $fila[0];
          //$devuelve["elementos"][$id]["email_usuario"]              = $fila[2];
          $devuelve["elementos"][$id]["name_vista_analytics"]       = $fila[1];
          $devuelve["elementos"][$id]["id_propiedad"]               = $fila[2];
          $devuelve["elementos"][$id]["id_propiedad_analytics"]     = $fila[3];
          $devuelve["elementos"][$id]["name_propiedad_analytics"]   = $fila[4];
          $devuelve["elementos"][$id]["analytics_default_vista"]    = $fila[5];
          $devuelve["elementos"][$id]["analytics_proyectoasociado"] = $fila[6];
          $devuelve["elementos"][$id]["vista_url"]                  = $fila[7];
          if(empty($fila[8]) OR $fila[8] == "" OR $fila[8] == 0 OR $fila[8] == 0){ $filapp = 0; }else{ $filapp = $fila[8]; }
          $devuelve["elementos"][$id]["app"]                        = $filapp;
        }
        $devuelve["cantidad"]=$count;
      }else{
        $devuelve["cantidad"]=0;
      }
  }
  return $devuelve;
}

//Funci�n que devuelve true si el usuario corresponde con esa vista
function comprobar_seguridad_vista_usuario($vista,$usuario){
  $c = Nuevo_PDO();
  //Comprobamos que los correos no sea los establecidos como superadmin (ACC Y DMACROWEB)
  $sql="SELECT LOCDVU_ANALYTICS_ID_VISTA, LOCDVU_USUARIO
        FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS
        WHERE LOCDVU_ANALYTICS_ID_VISTA = :vista AND LOCDVU_USUARIO = :usuario";  
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':vista' => $vista, ':usuario' => $usuario)); 
    //Si existe
    if ($result && $stmt->rowCount() != 0) {
      return true;
    }else {
      return false;
    }
}

//Funci�n que devuelve true si el usuario corresponde con esa vista
function comprobar_filtro_usuario($vista,$usuario){
   $devuelve = array();
  $c = Nuevo_PDO();
  //Comprobamos que los correos no sea los establecidos como superadmin (ACC Y DMACROWEB)
  $sql="SELECT LOCDVUF_CODIGO, LOCDVUF_FILTRO, LOCDVUF_FILTRODESCRIPCION
        FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS
        WHERE LOCDVUF_ANALYTICS_ID_VISTA = :vista AND LOCDVUF_USUARIO = :usuario";  
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':vista' => $vista, ':usuario' => $usuario)); 
    //Si existe
    if ($result && $stmt->rowCount() != 0) {
      $count = 0;
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $count++;
        $id = $fila[0];
        $devuelve["elementos"][$id]["filtro"] = $fila[1];
        $devuelve["elementos"][$id]["desc"] = $fila[2];
      }
      $devuelve["cantidad"]=$count;  
    }else {
      $devuelve["cantidad"]=0;
    }
    return $devuelve;
}

//Funci�n que devuelve los filtros de un identificador de filtro
function filtro_personalizado($idfiltro){
   $devuelve = array();
  $c = Nuevo_PDO();
  //Comprobamos que los correos no sea los establecidos como superadmin (ACC Y DMACROWEB)
  $sql="SELECT LOCDVUF_CODIGO, LOCDVUF_FILTRO, LOCDVUF_FILTRODESCRIPCION
        FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS
        WHERE LOCDVUF_CODIGO = :idfiltro";  
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':idfiltro' => $idfiltro)); 
    //Si existe
    if ($result && $stmt->rowCount() != 0) {
      $count = 0;
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $count++;
        $id = $fila[0];
        $devuelve["elementos"][$id]["filtro"] = $fila[1];
        $devuelve["elementos"][$id]["desc"] = $fila[2];
      }
      $devuelve["cantidad"]=$count;  
    }else {
      $devuelve["cantidad"]=0;
    }
    return $devuelve;
}


//comprueba si el email es correcto
function comprobar_email($email){
    $mail_correcto = 0;
    //compruebo unas cosas primeras
    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){
       if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {
          //miro si tiene caracter .
          if (substr_count($email,".")>= 1){
             //obtengo la terminacion del dominio
             $term_dom = substr(strrchr ($email, '.'),1);
             //compruebo que la terminaci�n del dominio sea correcta
             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
                //compruebo que lo de antes del dominio sea correcto
                $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
                if ($caracter_ult != "@" && $caracter_ult != "."){
                   $mail_correcto = 1;
                }
             }
          }
       }
    }
    if ($mail_correcto==1)
       return true;
    else
       return false;
}

//Comprueba si el email existe
function Comprobar_email_existe($email){
  $c = Nuevo_PDO();
  $sql="SELECT LOCU_CODIGO FROM dbo.LOC_USUARIOS WHERE LOCU_EMAIL = :username";  
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':username' => $email)); 
    //Si existe
    if ($result && $stmt->rowCount() != 0) {
      return true;
    }else {
      return false;
    }
}

//Funcion que devuelve info del usuario
function Usuarios($id_user="",$limitaciones=""){
  $c = Nuevo_PDO();
  if(empty($id_user)){
    $sql = "SELECT LOCU_CODIGO, LOCU_EMAIL, LOCU_PASSWORD, LOCU_SALT 
            FROM dbo.LOC_USUARIOS ".$limitaciones;
    $stmt = $c->prepare($sql);
    $result = $stmt->execute();
  }else{
    $sql = "SELECT LOCU_CODIGO, LOCU_EMAIL, LOCU_PASSWORD, LOCU_SALT 
            FROM dbo.LOC_USUARIOS 
            WHERE LOCU_CODIGO = :iduser ".$limitaciones;
    $stmt = $c->prepare($sql);
    $result = $stmt->execute(array(':iduser' => $id_user));
  }

  if ($result && $stmt->rowCount() != 0){
      $count = 0;
      //echo "Filas: ".$stmt->rowCount()."<br>";
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $count++;
        $id = $fila[0];
        $devuelve["elementos"][$id]["id_usuario"] = $fila[0];
        $devuelve["elementos"][$id]["email"]      = $fila[1];
        $devuelve["elementos"][$id]["pass"]       = $fila[2];
        $devuelve["elementos"][$id]["salt"]       = $fila[3];
      }
      $devuelve["cantidad"]=$count;
  }else{
      $devuelve["cantidad"]=0;
  }

  return $devuelve;  

}

//mail con codificacion utfo
function mail_utf8($para, $para_email, $de_email, $titulo = '(Sin t�tulo)', $mensaje = '')
   {
      $para_email = "=?UTF-8?B?".base64_encode($para_email)."?=";
      $titulo = "=?UTF-8?B?".base64_encode($titulo)."?=";

      $cabeceras = "From: $para_email <$de_email>\r\n".
               "MIME-Version: 1.0" . "\r\n" .
               "Content-type: text/html; charset=UTF-8" . "\r\n";

     return mail($para, $titulo, $mensaje, $cabeceras);
}

//Funcion que devuelve true si el salt y el email coinciden
function UserSalt($email,$salt){
  $c = Nuevo_PDO();
  $sql="SELECT LOCU_CODIGO FROM dbo.LOC_USUARIOS WHERE LOCU_EMAIL = :username AND LOCU_SALT = :salt";  
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':username' => $email, ':salt' => $salt)); 
    //Si existe
    if ($result && $stmt->rowCount() != 0) {
      return true;
    }else {
      return false;
    }
}

//funcion que devuelve el nombre de la propiedad de una vista
function propiedadVista($idvista){
  $c = Nuevo_PDO();
  $sql = "SELECT v.LOCDV_ANALYTICS_ID_VISTA, D.LOCD_ANALYTICS_NAME_PROPIEDAD, D.LOCD_ANALYTICS_PROPIEDAD_PROYECTOASOCIADO, D.LOCD_CODIGO, D.LOCD_ANALYTICS_ID_PROPIEDAD,V.LOCDV_ANALYTICS_NAME_VISTA,V.LOCDV_ANALYTICS_URL_VISTA, D.LOCD_ANALYTICS_PROPIEDAD_APPMOBILE
          FROM dbo.LOC_DOMINIOS_VISTAS AS V 
              JOIN dbo.LOC_DOMINIOS AS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
          WHERE v.LOCDV_ANALYTICS_ID_VISTA = :idvista";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':idvista' => $idvista));

  if ($result && $stmt->rowCount() != 0){
      $count = 0;
      //echo "Filas: ".$stmt->rowCount()."<br>";
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $count++;
        $id = $fila[0];
        $devuelve["elementos"][$id]["id_vista"] = $fila[0];
        $devuelve["elementos"][$id]["nombre_propiedad"] = $fila[1];
        $devuelve["elementos"][$id]["proyectoasociado"] = $fila[2];
        $devuelve["elementos"][$id]["propiedad"] = $fila[3];
        $devuelve["elementos"][$id]["idpropiedad"] = $fila[4];
        $devuelve["elementos"][$id]["nombre_vista"] = $fila[5];
        $devuelve["elementos"][$id]["url_vista"] = $fila[6];
        $devuelve["elementos"][$id]["app"] = $fila[7];
      }
      $devuelve["cantidad"]=$count;
  }else{
      $devuelve["cantidad"]=0;
  }

  return $devuelve;  
}

//Transformar fecha de entrada
function fentrada($cambio){
  $uno=substr($cambio, 0, 2);
  $dos=substr($cambio, 3, 2);
  $tres=substr($cambio, 6, 4);
  $resul = ($tres."/".$dos."/".$uno);
  return $resul;
} 

/*****************MENU*****************************/
//Me devuelve los menus del nivel principal
function MenuPrincipal(){
  $c = Nuevo_PDO();
  $sql = "SELECT MEN_CODIGO, MEN_MENU_ES, MEN_ENLACE
          FROM dbo.Men_MENUS
          WHERE MEN_NIVEL = 1
          ORDER BY MEN_ORDEN ASC";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute();

  if ($result && $stmt->rowCount() != 0){
      $count = 0;
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $count++;
        $id = $fila[0];
        $devuelve["elementos"][$id]["id_menu"] = $fila[0];
        $devuelve["elementos"][$id]["menu"]    = $fila[1];
        $devuelve["elementos"][$id]["enlace"]  = $fila[2];
      }
      $devuelve["cantidad"]=$count;
  }else{
      $devuelve["cantidad"]=0;
  }

  return $devuelve;  
}



function hijos($supadre){

  $c = Nuevo_PDO();
  $sql = "SELECT MEN_CODIGO, MEN_MENU_ES, MEN_ENLACE, MEN_NIVEL
          FROM dbo.Men_MENUS
          WHERE MEN_SUPADRE = :supadre";
          //ORDER BY MEN_ORDEN ASC
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':supadre' => $supadre));

  if ($result && $stmt->rowCount() != 0){
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $hijos[$fila[0]]["id"] = $fila[0];
        $hijos[$fila[0]]["name"] = $fila[1];
        $hijos[$fila[0]]["enlace"] = $fila[2];
      }
      return $hijos;
  }else{
      return false;
  }

}

function mostrar_categoria($id_padre = 0, $pos=0, $thisUrl, $topp){
  global $trans;
  
  if ($pos==0){
    echo '<ul id="main-menu" class="">';
  }else{
    //echo '<ul>';
  }
  $count = 1;
  $top = $topp;
  $pos = "prim";
  
  $c = Nuevo_PDO();
  $sql = "SELECT MEN_CODIGO, MEN_MENU_ES, MEN_ENLACE, MEN_NIVEL, MEN_SUPADRE, MEN_ORDEN
          FROM dbo.Men_MENUS
          WHERE MEN_SUPADRE = :supadre
          ORDER BY MEN_ORDEN ASC";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':supadre' => $id_padre));

  while ($categoria = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
    if($pos != "prim"){
      if($pos == "" && $count == $top){ $pos = "ult"; }else{ $pos = ""; } 
    }
    echo '<li class="opened '.$pos.'">'; 
    $pos = ""; 
    $urlvalue = trim($categoria[2]);
    $posicion_coincidencia = strpos($thisUrl, "/$urlvalue");        
    if ($posicion_coincidencia != false) { $class = " sel_lat"; $imgv = "_sel"; $href = "";  }else{ $class = ""; $imgv = ""; $href = RUTA_ABSOLUTA.trim($categoria[2]); }

    if($categoria[0] == 6 || $categoria[0] == 23){
      echo'<a id="ira_'.$categoria[0].'" style="cursor:pointer">';
    }else{
      echo'<a href="'.$href.'" class="'.$class.'" >';
    }

    echo'   <span class="imgmenulateral" style="background-image: url(\''.RUTA_ABSOLUTA.'images/menu_lateral'.$imgv.'.png\');">'.$count.'</span> <span class="desmenulat">'.$trans->__($categoria[1], false).'</span>
         </a>';
    $hijos = hijos($categoria[0]);
    if($hijos){
      echo"<ul>";
      foreach ($hijos as $clave => $valor){
          //echo $clave.' - '.$valor.'<br>';
          echo '<li'.echoActiveClassIfRequestMatches("metricas").'>';
          if($valor["id"] == 6 || $valor["enlace"] == 23){
            echo'<a id="ira_'.$valor["id"].'">';   
          }else{
            echo'<a href="../public/'.trim($valor["enlace"]).'">';   
          }
          echo' <span>'.$valor["name"].'</span>
              </a>
            </li>';
          mostrar_categoria($clave,1,$thisUrl);
      }
      echo"</ul>";
    }
    $count++;
    echo '</li>';

  }//while
  //echo '</ul>';

}

function echoActiveClassIfRequestMatches($requestUri){
  $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");
  if ($current_file_name == $requestUri)
   echo 'class="active"';
}

/*migas de navegacion*/
function Breadcrumd($cat){

  $c = Nuevo_PDO();
  $sql = "SELECT MEN_CODIGO, MEN_MENU_ES, MEN_ENLACE, MEN_NIVEL, MEN_SUPADRE
          FROM dbo.Men_MENUS
          WHERE MEN_SUPADRE = :cod
          order by MEN_ORDEN ASC";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $cat));

  if ($result && $stmt->rowCount() != 0){
    $count=0;
    $breadcrumd = array();
    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $breadcrumd["elementos"][$count]["id"]   = $fila[0];
      $breadcrumd["elementos"][$count]["nom"]   = $fila[1];
      $breadcrumd["elementos"][$count]["url"]   = $fila[2];
      $breadcrumd["elementos"][$count]["padre"] = $fila[4];
      $count++;
    } 
    $breadcrumd["cantidad"] = $count;

    return $breadcrumd;

  }else{
    return false;
  }

}

function BreadAnterior($subcat){

  $c = Nuevo_PDO();
  $sql = "SELECT MEN_CODIGO, MEN_MENU_ES, MEN_ENLACE, MEN_NIVEL, MEN_SUPADRE
          FROM dbo.Men_MENUS
          WHERE MEN_CODIGO = :cod";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $subcat));

  if ($result && $stmt->rowCount() != 0){
    $count=0;
    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $devuelve["cat"] = $fila[1];
      $devuelve["supadre"] = $fila[4];
      return $devuelve;
    }
    
  }else{
    return false;
  }

}


function ContenidoEnlazado($id_vista){

  $c = Nuevo_PDO();
  $sql = "SELECT D.LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO
          FROM dbo.LOC_DOMINIOS_VISTAS V JOIN dbo.LOC_DOMINIOS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
          WHERE V.LOCDV_ANALYTICS_ID_VISTA = :cod";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $id_vista));

  if ($result && $stmt->rowCount() != 0){

    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $enlazado = $fila[0];
    }

    if ($enlazado==true || $enlazado=="NULL" || $enlazado==1 || $enlazado=="") {
      return true;
    }else{
      return false;
    }
    
  }else{
    return false;
  }

}

//Funcion que devuelve el nombre del contenido desenlazado expuesto para envio de email
function ContenidoDesenlazadoDimension($id_vista){
  $c = Nuevo_PDO();
  $sql = "SELECT LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_EMAILSEMANAL
          FROM dbo.LOC_DOMINIOS_VISTAS V JOIN dbo.LOC_DOMINIOS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
          WHERE V.LOCDV_ANALYTICS_ID_VISTA = :cod AND D.LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO = 0";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $id_vista));

  if ($result && $stmt->rowCount() != 0){

      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $dimension_select = $fila[0];
      }
      $fill = "LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D".$dimension_select;
      $c_n = Nuevo_PDO();
      $sql_n = "SELECT ".$fill." 
                FROM dbo.LOC_DOMINIOS_VISTAS V JOIN dbo.LOC_DOMINIOS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
                WHERE V.LOCDV_ANALYTICS_ID_VISTA = :cod AND D.LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO = 0";
      $stmt_n = $c_n->prepare($sql_n);
      $result_n = $stmt_n->execute(array(':cod' => $id_vista));
      if ($result_n && $stmt_n->rowCount() != 0){

        while ($fila_n = $stmt_n->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $dimension_nombre = $fila_n[0];
        }
        return $dimension_nombre;

      }else{
        return false;
      }
     

  }else{
    //No esta enlazado
    return false;
  }

}

//LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO
function ActualizarEnlazado($id_vista,$opc){

  $c = Nuevo_PDO();
  $sql = "SELECT D.LOCD_CODIGO
          FROM dbo.LOC_DOMINIOS_VISTAS V JOIN dbo.LOC_DOMINIOS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
          WHERE V.LOCDV_ANALYTICS_ID_VISTA = :cod";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $id_vista));

  if ($result && $stmt->rowCount() != 0){

    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $cod = $fila[0];
    }

    if($opc == "enlazada"){
      $opc=1;
    }else{
      $opc=0;
    }

    $c = Nuevo_PDO();
    $sql = "UPDATE dbo.LOC_DOMINIOS SET LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO = :opc
            WHERE LOCD_CODIGO = :cod ";
    $stmt = $c->prepare($sql);
    $result = $stmt->execute(array(':opc' => $opc, ':cod' => $cod)); 

    return true;
    
    
  }else{
    return false;
  }

}

//LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO
function ContenidoEnlazado_nombres($id_vista){

  $c = Nuevo_PDO();
  $sql = "SELECT D.LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D4, D.LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D5, D.LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D6, D.LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_D7
          FROM dbo.LOC_DOMINIOS_VISTAS V JOIN dbo.LOC_DOMINIOS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
          WHERE V.LOCDV_ANALYTICS_ID_VISTA = :cod";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $id_vista));

  if ($result && $stmt->rowCount() != 0){

    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $devuelve["d4"] = $fila[0];
      $devuelve["d5"] = $fila[1];
      $devuelve["d6"] = $fila[2];
      $devuelve["d7"] = $fila[3];
      return $devuelve;
    }

  }else{
    return false;
  }

}

//LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO
function ContenidoEnlazado_emailsemanal($id_vista){

  $c = Nuevo_PDO();
  $sql = "SELECT D.LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_EMAILSEMANAL
          FROM dbo.LOC_DOMINIOS_VISTAS V JOIN dbo.LOC_DOMINIOS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
          WHERE V.LOCDV_ANALYTICS_ID_VISTA = :cod";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $id_vista));

  if ($result && $stmt->rowCount() != 0){

    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $devuelve = $fila[0];
      return $devuelve;
    }

  }else{
    return false;
  }

}

function Guardar_Contenido_enlazado_nomrbes($id_vista,$d4,$d5,$d6,$d7,$re){

  $c = Nuevo_PDO();
  $sql = "SELECT D.LOCD_CODIGO
          FROM dbo.LOC_DOMINIOS_VISTAS V JOIN dbo.LOC_DOMINIOS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
          WHERE V.LOCDV_ANALYTICS_ID_VISTA = :cod";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $id_vista));

  if ($result && $stmt->rowCount() != 0){

    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $cod = $fila[0];
    }

    $c = Nuevo_PDO();
    $sql = "UPDATE dbo.LOC_DOMINIOS SET LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_d4 = :da,LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_d5 = :db,LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_d6 = :dc,LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_d7 = :dd,LOCD_ANALYTICS_PROPIEDAD_CONTENIDOENLAZADO_DESENLAZADO_EMAILSEMANAL = :re
            WHERE LOCD_CODIGO = :cod ";
    $stmt = $c->prepare($sql);
    $result = $stmt->execute(array(':da' => $d4, ':db' => $d5, ':dc' => $d6, ':dd' => $d7, ':cod' => $cod, ':re' => $re)); 

    return true;
    
    
  }else{
    return false;
  }

}

function DominioVista($id_vista){

  $c = Nuevo_PDO();

    $sql= "SELECT LOCDV_ANALYTICS_URL_VISTA
           FROM dbo.LOC_DOMINIOS_VISTAS
           WHERE LOCDV_ANALYTICS_ID_VISTA = :cod";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':cod' => $id_vista));

  if ($result && $stmt->rowCount() != 0){

    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $dominio = $fila[0];
    }

    return $dominio;
    
    
  }else{
    return false;
  }

}

function clienteVista($vista){

  $devuelve = "";
  $c = Nuevo_PDO();
  $sql = 'SELECT V.LOCDV_ANALYTICS_ID_VISTA, CLI.CLIE_CODIGO, CLI.CLIE_NOMBRE
          FROM dbo.LOC_DOMINIOS_VISTAS AS V
                JOIN dbo.LOC_DOMINIOS AS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
                INNER JOIN ISP_DOMINIOS ON D.LOCD_DOMINIO=ISP_DOMINIOS.DOM_CODIGO
                INNER JOIN CLIENTES as CLI ON ISP_DOMINIOS.DOM_SUCLIENTE=CLI.CLIE_CODIGO 
          WHERE V.LOCDV_ANALYTICS_ID_VISTA = '.$vista;
  if($stmt = $c->prepare($sql)){
    $result = $stmt->execute();
    if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $devuelve = $fila[1];
        }

    }
  }   

  return $devuelve;   
}



//Funcion que devuelve una arraiy con todos los clientes del usuario enviado
function clientesUsuario($useremail,$com_emails){

  $devuelve = array();
  $c = Nuevo_PDO();
  $maildom = dominioEmail($useremail);
  if(comDominiosEmails($maildom,$com_emails)){
    $anadido=""; 
    $sql = "SELECT CLIE_NOMBRE,CLIE_CODIGO 
            FROM ((((LOC_DOMINIOS INNER JOIN ISP_DOMINIOS ON ISP_DOMINIOS.DOM_CODIGO=LOC_DOMINIOS.LOCD_DOMINIO) 
            LEFT JOIN LOC_DOMINIOS_VISTAS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO) 
            LEFT JOIN LOC_DOMINIOS_VISTAS_USUARIOS ON LOC_DOMINIOS_VISTAS.LOCDV_ANALYTICS_ID_VISTA=LOC_DOMINIOS_VISTAS_USUARIOS.LOCDVU_ANALYTICS_ID_VISTA ) 
            LEFT JOIN CLIENTES ON ISP_DOMINIOS.DOM_SUCLIENTE=CLIENTES.CLIE_CODIGO ) 
            LEFT JOIN LOC_USUARIOS ON LOC_DOMINIOS_VISTAS_USUARIOS.LOCDVU_USUARIO=LOC_USUARIOS.LOCU_EMAIL
            group by CLIE_NOMBRE,CLIE_CODIGO 
            ORDER BY CLIE_NOMBRE,CLIE_CODIGO desc";
  }else{
    $anadido="ok"; 
    $sql= "SELECT CLIE_NOMBRE,CLIE_CODIGO 
           FROM ((((LOC_DOMINIOS INNER JOIN ISP_DOMINIOS ON ISP_DOMINIOS.DOM_CODIGO=LOC_DOMINIOS.LOCD_DOMINIO) 
           LEFT JOIN LOC_DOMINIOS_VISTAS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO) 
           LEFT JOIN LOC_DOMINIOS_VISTAS_USUARIOS ON LOC_DOMINIOS_VISTAS.LOCDV_ANALYTICS_ID_VISTA=LOC_DOMINIOS_VISTAS_USUARIOS.LOCDVU_ANALYTICS_ID_VISTA ) 
           LEFT JOIN CLIENTES ON ISP_DOMINIOS.DOM_SUCLIENTE=CLIENTES.CLIE_CODIGO ) 
           LEFT JOIN LOC_USUARIOS ON LOC_DOMINIOS_VISTAS_USUARIOS.LOCDVU_USUARIO=LOC_USUARIOS.LOCU_EMAIL
           WHERE LOCU_EMAIL = :email
           group by CLIE_NOMBRE,CLIE_CODIGO 
           ORDER BY CLIE_NOMBRE,CLIE_CODIGO desc";  
  }

  if($stmt = $c->prepare($sql)){
      if($anadido!=""){
        $result = $stmt->execute(array(':email' => $useremail));
      }else{
        $result = $stmt->execute();
      }   
      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $count++;
          $id = $fila[1];
          $devuelve["elementos"][$id]["id"]         = $fila[1];
          $devuelve["elementos"][$id]["nombre"]     = $fila[0];
        }
        $devuelve["cantidad"]=$count;
      }else{
        $devuelve["cantidad"]=0;
      }
  }
  return $devuelve;

}

//Funcion que mediante el ccliente te devuelve sus administradores
function clienteAdmins($cliente){

  $devuelve = array();
  $c = Nuevo_PDO();
  $sql= "SELECT LOCUC_CODIGO, LOCUC_USUARIO 
         FROM LOC_USUARIOS_CLIENTES
         WHERE LOCUC_CLIENTE = :client AND LOCUC_ESADMINISTRADOR = 1
         ORDER BY LOCUC_USUARIO ";  

  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':client' => $cliente));

      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $count++;
          $id = $fila[0];
          $devuelve["elementos"][$id]["id"]    = $fila[0];
          $devuelve["elementos"][$id]["admin"] = $fila[1];
        }
        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }
  return $devuelve;

}



//Funcion que mediante el cliente te devuelve el plano de permisos
function clientePermisos($cliente,$admin,$useremail,$com_emails){

  $admin = 0;
  $d = Nuevo_PDO();
  $maildom = dominioEmail($useremail);
  if(comDominiosEmails($maildom,$com_emails)){

      $admin=1;

  }else{

      $sqld= "SELECT LOCUC_ESADMINISTRADOR
             FROM dbo.LOC_USUARIOS_CLIENTES
             WHERE LOCUC_USUARIO = '".$useremail."' AND LOCUC_CLIENTE  = '".$cliente."'";
      $stmtd = $d->prepare($sqld);
      $resultd = $stmtd->execute();
      if ($resultd && $stmtd->rowCount() != 0){
          while ($filad = $stmtd->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
            $admincompr = $filad[0];
          }
          if($admincompr == 1){
            $admin = 1;
          }else{
            $admin = 0;
          }
        
      }else{
        $admin = 0;
      }

  }

  $devuelve = array();
  $c = Nuevo_PDO();
  if($admin==1){
    $ddre = 0;
    $sql= "SELECT CLIE_CODIGO,CLIE_NOMBRE, LOCD_ANALYTICS_NAME_PROPIEDAD,LOCD_ANALYTICS_ID_PROPIEDAD,LOCDV_ANALYTICS_NAME_VISTA,LOCDV_ANALYTICS_URL_VISTA,LOCDV_ANALYTICS_ID_VISTA, LOCDVU_USUARIO,LOCDVU_ENVIOEMAIL_INFORME_SEMANAL,LOCDVU_ENVIOEMAIL_INFORME_MENSUAL,LOCDVU_ENVIOEMAIL_INFORME_TRIMESTRAL,LOCDVU_CODIGO,LOCDVU_ENVIOEMAIL_CAMPANAS_SEMANAL,LOCDVU_ENVIOEMAIL_CAMPANAS_MENSUAL,LOCDVU_ENVIOEMAIL_CAMPANAS_TRIMESTRAL  
           FROM (((LOC_DOMINIOS INNER JOIN ISP_DOMINIOS ON ISP_DOMINIOS.DOM_CODIGO=LOC_DOMINIOS.LOCD_DOMINIO) 
           LEFT JOIN LOC_DOMINIOS_VISTAS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO) 
           LEFT JOIN LOC_DOMINIOS_VISTAS_USUARIOS ON LOC_DOMINIOS_VISTAS.LOCDV_ANALYTICS_ID_VISTA=LOC_DOMINIOS_VISTAS_USUARIOS.LOCDVU_ANALYTICS_ID_VISTA ) 
           LEFT JOIN CLIENTES ON ISP_DOMINIOS.DOM_SUCLIENTE=CLIENTES.CLIE_CODIGO 
           WHERE CLIE_CODIGO=:client
           ORDER BY CLIE_NOMBRE,LOCD_ANALYTICS_ID_PROPIEDAD, LOCDV_ANALYTICS_ID_VISTA,LOCDVU_USUARIO";
    $devuelve["admin"] = 1; 

  }else{
    $ddre = 1;
    $sql= "SELECT CLIE_CODIGO,CLIE_NOMBRE, LOCD_ANALYTICS_NAME_PROPIEDAD,LOCD_ANALYTICS_ID_PROPIEDAD,LOCDV_ANALYTICS_NAME_VISTA,LOCDV_ANALYTICS_URL_VISTA,LOCDV_ANALYTICS_ID_VISTA, LOCDVU_USUARIO,LOCDVU_ENVIOEMAIL_INFORME_SEMANAL,LOCDVU_ENVIOEMAIL_INFORME_MENSUAL,LOCDVU_ENVIOEMAIL_INFORME_TRIMESTRAL,LOCDVU_CODIGO,LOCDVU_ENVIOEMAIL_CAMPANAS_SEMANAL,LOCDVU_ENVIOEMAIL_CAMPANAS_MENSUAL,LOCDVU_ENVIOEMAIL_CAMPANAS_TRIMESTRAL  
           FROM (((LOC_DOMINIOS INNER JOIN ISP_DOMINIOS ON ISP_DOMINIOS.DOM_CODIGO=LOC_DOMINIOS.LOCD_DOMINIO) 
           LEFT JOIN LOC_DOMINIOS_VISTAS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO) 
           LEFT JOIN LOC_DOMINIOS_VISTAS_USUARIOS ON LOC_DOMINIOS_VISTAS.LOCDV_ANALYTICS_ID_VISTA=LOC_DOMINIOS_VISTAS_USUARIOS.LOCDVU_ANALYTICS_ID_VISTA ) 
           LEFT JOIN CLIENTES ON ISP_DOMINIOS.DOM_SUCLIENTE=CLIENTES.CLIE_CODIGO 
           WHERE CLIE_CODIGO=:client AND LOCDVU_USUARIO = :user
           ORDER BY CLIE_NOMBRE,LOCD_ANALYTICS_ID_PROPIEDAD, LOCDV_ANALYTICS_ID_VISTA,LOCDVU_USUARIO"; 
    $devuelve["admin"] = 0;

  }
  if($stmt = $c->prepare($sql)){

      if ($ddre==0){
        $result = $stmt->execute(array(':client' => $cliente));
      }else{
        $result = $stmt->execute(array(':client' => $cliente, ':user' => $useremail));
      }
      

      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $id = $count;
          $count++;
          $devuelve["elementos"][$id]["id_cliente"]     = $fila[0];
          $devuelve["elementos"][$id]["nombre_cliente"] = $fila[1];
          $devuelve["elementos"][$id]["propiedad"]      = $fila[2];
          $devuelve["elementos"][$id]["id_propiedad"]   = $fila[3];
          $devuelve["elementos"][$id]["vista"]          = $fila[4];
          $devuelve["elementos"][$id]["vista_url"]      = $fila[5];
          $devuelve["elementos"][$id]["id_vista"]       = $fila[6];
          $devuelve["elementos"][$id]["user_email"]     = $fila[7];
          $devuelve["elementos"][$id]["informe_sem"]    = $fila[8];
          $devuelve["elementos"][$id]["informe_men"]    = $fila[9];
          $devuelve["elementos"][$id]["informe_tri"]    = $fila[10];
          $devuelve["elementos"][$id]["cod"]            = $fila[11];
          $devuelve["elementos"][$id]["campana_sem"]    = $fila[12];
          $devuelve["elementos"][$id]["campana_men"]    = $fila[13];
          $devuelve["elementos"][$id]["campana_tri"]    = $fila[14];
        }
        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }
  return $devuelve;

}

//Funcion que devuelve los filtros personalizados de un usuario con una vista
function filtrosUsuariosVista($usuario,$vista){

  $devuelve = array();
  $c = Nuevo_PDO();
  $sql= "SELECT LOCDVUF_CODIGO,LOCDVUF_FILTRO,LOCDVUF_FILTRODESCRIPCION
         FROM LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS
         WHERE LOCDVUF_ANALYTICS_ID_VISTA=:vista
         AND LOCDVUF_USUARIO=:email";  

  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':vista' => $vista, ':email' => $usuario));

      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $id = $count;
          $count++;
          $devuelve["elementos"][$id]["id"]          = $fila[0];
          $devuelve["elementos"][$id]["filtro"]      = $fila[1];
          $devuelve["elementos"][$id]["filtro_desc"] = $fila[2];
        }
        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }
  return $devuelve;

}

function CorreosRecomendados($vista, $cliente){
  $devuelve = array();
  $c = Nuevo_PDO();
  $sql= "SELECT CON_EMAIL 
         FROM CONTACTOS 
         WHERE CON_SUCLIENTE=:cliente 
         and CON_EMAIL<>'' and NOT CON_EMAIL IS NULL 
         and CON_EMAIL not in (
               SELECT LOCDVU_USUARIO
               FROM (((LOC_DOMINIOS INNER JOIN ISP_DOMINIOS ON ISP_DOMINIOS.DOM_CODIGO=LOC_DOMINIOS.LOCD_DOMINIO) 
               INNER JOIN LOC_DOMINIOS_VISTAS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO) 
               INNER JOIN LOC_DOMINIOS_VISTAS_USUARIOS ON LOC_DOMINIOS_VISTAS.LOCDV_ANALYTICS_ID_VISTA=LOC_DOMINIOS_VISTAS_USUARIOS.LOCDVU_ANALYTICS_ID_VISTA ) 
               INNER JOIN CLIENTES ON ISP_DOMINIOS.DOM_SUCLIENTE=CLIENTES.CLIE_CODIGO 
               WHERE LOCDV_ANALYTICS_ID_VISTA = :vista 
         )
         ORDER BY CON_EMAIL";  

  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':cliente' => $cliente, ':vista' => $vista));

      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $id = $count;
          $count++;
          $devuelve["elementos"][$id]["email"] = $fila[0];
        }
        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }
  return $devuelve;
}


//Funcion para actualizar el informe semanal
function ActualizarInformeSemanal($vista,$email,$a){

  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.LOC_DOMINIOS_VISTAS_USUARIOS
          SET LOCDVU_ENVIOEMAIL_INFORME_SEMANAL = :is
          WHERE LOCDVU_ANALYTICS_ID_VISTA = :vista AND LOCDVU_USUARIO = :email";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':is' => $a, ':vista' => $vista, ':email' => $email)); 

  return true;

}

//Funcion para actualizar el informe mensual
function ActualizarInformeMensual($vista,$email,$a){

  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.LOC_DOMINIOS_VISTAS_USUARIOS
          SET LOCDVU_ENVIOEMAIL_INFORME_MENSUAL = :is
          WHERE LOCDVU_ANALYTICS_ID_VISTA = :vista AND LOCDVU_USUARIO = :email";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':is' => $a, ':vista' => $vista, ':email' => $email)); 

  return true;

}

//Funcion para actualizar el informe trimestral
function ActualizarInformeTrimestral($vista,$email,$a){

  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.LOC_DOMINIOS_VISTAS_USUARIOS
          SET LOCDVU_ENVIOEMAIL_INFORME_TRIMESTRAL = :is
          WHERE LOCDVU_ANALYTICS_ID_VISTA = :vista AND LOCDVU_USUARIO = :email";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':is' => $a, ':vista' => $vista, ':email' => $email)); 

  return true;

}

//Funcion para actualizar la campana semanal
function ActualizarCampanaSemanal($vista,$email,$a){

  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.LOC_DOMINIOS_VISTAS_USUARIOS
          SET LOCDVU_ENVIOEMAIL_CAMPANAS_SEMANAL = :is
          WHERE LOCDVU_ANALYTICS_ID_VISTA = :vista AND LOCDVU_USUARIO = :email";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':is' => $a, ':vista' => $vista, ':email' => $email)); 

  return true;

}

//Funcion para actualizar la campana mensual
function ActualizarCampanaMensual($vista,$email,$a){

  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.LOC_DOMINIOS_VISTAS_USUARIOS
          SET LOCDVU_ENVIOEMAIL_CAMPANAS_MENSUAL = :is
          WHERE LOCDVU_ANALYTICS_ID_VISTA = :vista AND LOCDVU_USUARIO = :email";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':is' => $a, ':vista' => $vista, ':email' => $email)); 

  return true;

}

//Funcion para actualizar la campana trimestral
function ActualizarCampanaTrimestral($vista,$email,$a){

  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.LOC_DOMINIOS_VISTAS_USUARIOS
          SET LOCDVU_ENVIOEMAIL_CAMPANAS_TRIMESTRAL = :is
          WHERE LOCDVU_ANALYTICS_ID_VISTA = :vista AND LOCDVU_USUARIO = :email";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':is' => $a, ':vista' => $vista, ':email' => $email)); 

  return true;

}

//Funcion para insertar un nuevo usuario
function InsertarUsuario($vista,$email){

  //Primero comprobamos si existe en LOC_USUARIOS
  $c = Nuevo_PDO();
  $sql= "SELECT LOCU_EMAIL
         FROM dbo.LOC_USUARIOS
         WHERE LOCU_EMAIL = :email";  

  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':email' => $email));

      if ($result && $stmt->rowCount() == 0){
        
        //No contiene datos por lo que pasamos a insertar el email
         $d = Nuevo_PDO();
         $sql = "INSERT INTO dbo.LOC_USUARIOS (LOCU_EMAIL)
                 VALUES (:email)";
         $stmt = $d->prepare($sql);
         $resultb = $stmt->execute(array(':email' => $email)); 

      }

  }

  //Segundo comprobamos si existe en LOC_DOINIOS_VISTAS_USUARIOS
  $e = Nuevo_PDO();
  $sql= "SELECT LOCDVU_USUARIO
         FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS
         WHERE LOCDVU_USUARIO = :email AND LOCDVU_ANALYTICS_ID_VISTA = :vista";  

  if($stmt = $e->prepare($sql)){

      $resultc = $stmt->execute(array(':email' => $email,':vista' => $vista));

      if ($resultc && $stmt->rowCount() == 0){
        
        //No contiene datos por lo que pasamos a insertar el email
        $f = Nuevo_PDO();
        $sql = "INSERT INTO dbo.LOC_DOMINIOS_VISTAS_USUARIOS (LOCDVU_ANALYTICS_ID_VISTA,LOCDVU_USUARIO)
                VALUES (:vista, :email)";
        $stmt = $f->prepare($sql);
        $resultd = $stmt->execute(array(':vista' => $vista, ':email' => $email)); 


        //Comprobamos si existe para no volver a insertar
        $cliente = clienteVista($vista); 
        $g = Nuevo_PDO();
        $sqlg = "SELECT LOCUC_USUARIO,LOCUC_CLIENTE
                 FROM dbo.LOC_USUARIOS_CLIENTES
                 WHERE LOCUC_USUARIO = '".$email."' AND LOCUC_CLIENTE = '".$cliente."'";  
        $stmtg = $g->prepare($sqlg);
        $resultg = $stmtg->execute();   
       
        if ($resultg && $stmtg->rowCount() == 0){
          //Y ahora creamos un registro para el administracion 
          $e = Nuevo_PDO();
          $sqle = "INSERT INTO dbo.LOC_USUARIOS_CLIENTES (LOCUC_USUARIO,LOCUC_CLIENTE)
                   VALUES(:email,:cliente)";
          $stmte = $e->prepare($sqle);
          $resulte = $stmte->execute(array(':email' => $email,':cliente' => $cliente)); 
        }
        //************************************************

      }

  }

  return  UsuariosData($email);
}


//Funcion para eliminar un usuario
function EliminarUsuario($vista,$email){

  //Eliminamos los usuarios vistas
  $c = Nuevo_PDO();
  $sql = "DELETE FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS
          WHERE LOCDVU_ANALYTICS_ID_VISTA = :vista AND LOCDVU_USUARIO = :email";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':vista' => $vista, ':email' => $email)); 

  //eliminamos los filtros personalizados
  $b = Nuevo_PDO();
  $sql = "DELETE FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS
          WHERE LOCDVUF_ANALYTICS_ID_VISTA = :vista AND LOCDVUF_USUARIO = :email";
  $stmt = $b->prepare($sql);
  $result = $stmt->execute(array(':vista' => $vista, ':email' => $email)); 

  //AHORA COMPROBAMOS SI TIENE PERMISOS DE VISTA
  $e = Nuevo_PDO();
  $sql = "SELECT LOCDVU_CODIGO
          FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS
          WHERE LOCDVU_USUARIO = :email";
  if($stmt = $e->prepare($sql)){

      $resultc = $stmt->execute(array(':email' => $email));

      if ($resultc && $stmt->rowCount() == 0){
        
        //Si no existen permisos eliminamos el usuario
        $d = Nuevo_PDO();
        $sql = "DELETE FROM dbo.LOC_USUARIOS
                WHERE LOCU_EMAIL = :email";
        $stmt = $d->prepare($sql);
        $result = $stmt->execute(array(':email' => $email)); 

      }

  }

  //Comprobamos si existen mas vistas en el cliente con ese usuario para eliminar la fila
  $cliente = clienteVista($vista); 
  $r = Nuevo_PDO();
  $sqlr = "SELECT VU.LOCDVU_CODIGO
           FROM dbo.LOC_DOMINIOS_VISTAS AS V
                JOIN dbo.LOC_DOMINIOS AS D ON V.LOCDV_SUDOMINIO = D.LOCD_CODIGO
                INNER JOIN ISP_DOMINIOS ON D.LOCD_DOMINIO=ISP_DOMINIOS.DOM_CODIGO
                INNER JOIN CLIENTES as CLI ON ISP_DOMINIOS.DOM_SUCLIENTE=CLI.CLIE_CODIGO 
                INNER JOIN dbo.LOC_DOMINIOS_VISTAS_USUARIOS VU ON VU.LOCDVU_ANALYTICS_ID_VISTA = V.LOCDV_ANALYTICS_ID_VISTA
           WHERE CLI.CLIE_CODIGO = '".$cliente."' AND VU.LOCDVU_USUARIO = '".$email."'";
  $stmtr = $r->prepare($sqlr);
  $resultr = $stmtr->execute(); 
  if ($resultr && $stmtr->rowCount() == 0){

    $d = Nuevo_PDO();
    $sql = "DELETE FROM dbo.LOC_USUARIOS_CLIENTES
            WHERE LOCUC_USUARIO = '".$email."' AND LOCUC_CLIENTE = '".$cliente."'";
    $stmt = $d->prepare($sql);
    $result = $stmt->execute(); 

  }


  return true;

}

//Funcion para a�adir un filtro de usuario
function AnadirFiltro($vista,$email,$filtro,$filtrodesc){

  $c = Nuevo_PDO();
  $sql = "INSERT INTO dbo.LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS (LOCDVUF_USUARIO,LOCDVUF_ANALYTICS_ID_VISTA, LOCDVUF_FILTRO, LOCDVUF_FILTRODESCRIPCION)
          VALUES ( :email, :vista, :filtro, :filtrodesc)";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':email' => $email, ':vista' => $vista, ':filtro' => $filtro, ':filtrodesc' => $filtrodesc)); 

  return true;

}

//Funcion para eliminar un usuario
function EliminarFiltro($id){

  $c = Nuevo_PDO();
  $sql = "DELETE FROM dbo.LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS
          WHERE LOCDVUF_CODIGO = :idf";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':idf' => $id)); 

  return true;

}

//Funcion para actualizar un filtro de usuario
function GuardarFiltro($id,$filtro,$filtrodesc){

  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.LOC_DOMINIOS_VISTAS_USUARIOS_FILTROSPERSONALIZADOS
          SET LOCDVUF_FILTRO = :filtro, LOCDVUF_FILTRODESCRIPCION = :filtrodesc
          WHERE LOCDVUF_CODIGO = :id";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':filtro' => $filtro, ':filtrodesc' => $filtrodesc, ':id' => $id)); 

  return true;

}

//Funcion para aborrar la contrase�a de un usuario
function borrarPass($email){

  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.LOC_USUARIOS
          SET LOCU_PASSWORD = '', LOCU_SALT = ''
          WHERE LOCU_EMAIL = '".$email."'";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(); 

  return true;

}


function pagsIndexadas($vista,$fechaini,$fechafin,$mod){

  $dominio = DominioVista($vista);

  if($mod == "media"){
    $anadido = "avg(LOCR_GOOGLE)";
  }else{
    $anadido = "sum(LOCR_GOOGLE)";
  }

  $c = Nuevo_PDO();
  $sql= "SELECT ".$anadido." as AVG_locr_google 
         FROM LOC_RESULTADOS 
         WHERE LOCR_FECHA >= :fechaini and LOCR_FECHA <= :fechafin 
         and not LOCR_GOOGLE is null 
         and LOCR_NOMBREDOMINIO= :dominio";  

  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':fechaini' => $fechaini.' 00:00:00', ':fechafin' => $fechafin.' 23:59:59', ':dominio' => $dominio));

      if ($result && $stmt->rowCount() != 0){

        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $devuelve = $fila[0];
        }

      }else{
        $devuelve = 0;
      }
  }

  return $devuelve;


  
}

function pagsIndexadasMes($vista,$fechaini,$fechafin){

  $dominio = DominioVista($vista);

  $c = Nuevo_PDO();
  $sql= "SELECT avg(LOCR_GOOGLE) as AVG, MONTH(locr_fecha) as Mes,year(locr_fecha) as Ano
         FROM LOC_RESULTADOS 
         WHERE LOCR_FECHA >= :fechaini and LOCR_FECHA <= :fechafin 
         and not LOCR_GOOGLE is null 
         and LOCR_NOMBREDOMINIO = :dominio
         group by year(locr_fecha),month(locr_fecha) 
         order by year(locr_fecha),MONTH(locr_fecha) ";  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':fechaini' => $fechaini.' 00:00:00', ':fechafin' => $fechafin.' 23:59:59', ':dominio' => $dominio));

      if ($result && $stmt->rowCount() != 0){

        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $devuelve = $fila[0];
        }

      }else{
        $devuelve = 0;
      }
  }

  //return number_format($devuelve,0,',','.')
  return $devuelve;
}

function traducirMes($mes){
  $meses = array();
  switch ($mes) {
    case 1:
        $meses[1]["num"] = 1;
        $meses[1]["nom"] = "enero";
        $meses[1]["res"] = "Ene";
      break;
    case 2:
        $meses[2]["num"] = 2;
        $meses[2]["nom"] = "febrero";
        $meses[2]["res"] = "Feb";
      break;
    case 3:
        $meses[3]["num"] = 3;
        $meses[3]["nom"] = "marzo";
        $meses[3]["res"] = "Mar";
      break;
    case 4:
        $meses[4]["num"] = 4;
        $meses[4]["nom"] = "abril";
        $meses[4]["res"] = "Abr";
      break;
    case 5:
        $meses[5]["num"] = 5;
        $meses[5]["nom"] = "mayo";
        $meses[5]["res"] = "May";
      break;
    case 6:
        $meses[6]["num"] = 6;
        $meses[6]["nom"] = "junio";
        $meses[6]["res"] = "Jun";
      break;
    case 7:
        $meses[7]["num"] = 7;
        $meses[7]["nom"] = "julio";
        $meses[7]["res"] = "Jul";
      break;
    case 8:
        $meses[8]["num"] = 8;
        $meses[8]["nom"] = "agosto";
        $meses[8]["res"] = "Ago";
      break;
    case 9:
        $meses[9]["num"] = 9;
        $meses[9]["nom"] = "septiembre";
        $meses[9]["res"] = "Sep";
      break;
    case 10:
        $meses[10]["num"] = 10;
        $meses[10]["nom"] = "octubre";
        $meses[10]["res"] = "Oct";
      break;
    case 11:
        $meses[11]["num"] = 11;
        $meses[11]["nom"] = "noviembre";
        $meses[11]["res"] = "Nov";
      break;
    case 12:
        $meses[12]["num"] = 12;
        $meses[12]["nom"] = "diciembre";
        $meses[12]["res"] = "Dic";
      break;    
    default:
        $meses[12]["num"] = false;
        $meses[12]["nom"] = "desconocido";
        $meses[12]["res"] = "Null";
      break;
  }

  return $meses;
}

function rangoFechas($fechaIni,$fechaFin){
  global $trans;
  $dias = array("domingo","lunes","martes","mi&eacute;rcoles","jueves","viernes","s&aacute;bado");
  $dias_res = array("dom","lun","mar","mi&eacute;r","jue","vier","s&aacute;b");

  //Separamos la fecha en dia mes y a�o
  $fechaIni_anno    = date("Y", strtotime($fechaIni)); 
  $fechaIni_mes     = date("m", strtotime($fechaIni)); 
  $fechaIni_dia     = date("d", strtotime($fechaIni)); 
  $fechaIni_semanai = date("w", strtotime($fechaIni)); 
  $fechaIni_semana  = $dias[$fechaIni_semanai];

  $fechaFin_anno    = date("Y", strtotime($fechaFin)); 
  $fechaFin_mes     = date("m", strtotime($fechaFin)); 
  $fechaFin_dia     = date("d", strtotime($fechaFin)); 
  $fechaFin_semanai = date("w", strtotime($fechaFin)); 
  $fechaFin_semana  = $dias[$fechaFin_semanai];
  $fechaFin_semana_res  = $dias_res[$fechaFin_semanai];
  //echo "Buenos d&iacute;as, hoy es ".$dias[date("w")];

  //Comprobamos si el a�o es el mismo
  if($fechaIni_anno == $fechaFin_anno){

    //Si el a�o es el mismo comparamos el mes
    if($fechaIni_mes == $fechaFin_mes){

      //Si el mes y a�o es igual
      $inimes = traducirMes($fechaIni_mes);
      //$devuelve = intval($fechaIni_mes);
      $devuelve = vsprintf($trans->__('%1$s %2$d al %3$s %4$d de %5$s del %6$s', false), array($trans->__($fechaIni_semana, false), intval($fechaIni_dia), $trans->__($fechaFin_semana, false), intval($fechaFin_dia), $trans->__($inimes[intval($fechaIni_mes)]["nom"], false), $fechaIni_anno));
      //$devuelve = $fechaIni_semana." ".intval($fechaIni_dia)." al ".$fechaFin_semana." ".intval($fechaFin_dia)." de ".$inimes[intval($fechaIni_mes)]["nom"]." del ".$fechaIni_anno;

    }else{

      //si el mes es diferente pero el a�o igual
      $inimes = traducirMes($fechaIni_mes);
      $finmes = traducirMes($fechaFin_mes);
      $devuelve = vsprintf($trans->__('%1$s %2$d de %3$s al %4$s %5$d de %6$s del %7$s', false), array($trans->__($fechaIni_semana, false),intval($fechaIni_dia),$trans->__($inimes[intval($fechaIni_mes)]["nom"], false), $trans->__($fechaFin_semana, false), intval($fechaFin_dia), $trans->__($finmes[intval($fechaFin_mes)]["nom"], false), $fechaIni_anno));
      //$devuelve = $fechaIni_semana." ".intval($fechaIni_dia)." de ".$inimes[intval($fechaIni_mes)]["nom"]." al ".$fechaFin_semana." ".intval($fechaFin_dia)." de ".$finmes[intval($fechaFin_mes)]["nom"]." del ".$fechaIni_anno;

    }

  }else{

    //Si el a�o es diferente mostramos todo
    $inimes = traducirMes($fechaIni_mes);
    $finmes = traducirMes($fechaFin_mes);
    $devuelve = vsprintf($trans->__('%1$s %2$d de %3$s del %4$s al %5$s %6$d de %7$s del %8$s', false), array($trans->__($fechaIni_semana, false), intval($fechaIni_dia), $trans->__($inimes[intval($fechaIni_mes)]["nom"], false), $fechaIni_anno, $trans->__($fechaFin_semana, false), intval($fechaFin_dia), $trans->__($finmes[intval($fechaFin_mes)]["nom"], false), $fechaFin_anno));
    //$devuelve = $fechaIni_semana." ".intval($fechaIni_dia)." de ".$inimes[intval($fechaIni_mes)]["nom"]." del ".$fechaIni_anno." al ".$fechaFin_semana." ".intval($fechaFin_dia)." de ".$finmes[intval($fechaFin_mes)]["nom"]." del ".$fechaFin_anno;

  }

  return $devuelve;

}

function Mayusculas($cadena){
  return strtr(strtoupper($cadena),"�����������������","�����������������");
}

function encrypt($string, $key) {
   $result = '';
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)+ord($keychar));
      $result.=$char;
   }
   return base64_encode($result);
}

function decrypt($string, $key) {
   $result = '';
   $string = base64_decode($string);
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)-ord($keychar));
      $result.=$char;
   }
   return $result;
}

function comboSeo($urlVista,$tipo){

  $c = Nuevo_PDO();
  if($tipo == "term"){
    $sql= "SELECT distinct(LOCDVS_ANOMES) MES
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         ORDER BY MES DESC";  
  }else{
    $sql= "SELECT distinct(LOCDVP_ANOMES) MES
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         ORDER BY MES DESC";  
  }
  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista));

      if ($result && $stmt->rowCount() != 0){
        $count=0;
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $devuelve[$count] = $fila[0];
          $count++;
        }

      }else{
        $devuelve = 0;
      }
  }

  return $devuelve;

}

function SeoExtras_term($urlVista,$mesSelect,$comp,$control){

  if($control == 1){
    $control = " and LOCDVS_TERMINO IN (Select LOCDVTC_TERMINOCONTROL From LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL WHERE LOCDVTC_NOMBREDOMINIO=LOCDVS_NOMBREDOMINIO)";
    $ctr=1;
  }else{
    $control = "";
    $ctr=0;
  }


  $c = Nuevo_PDO();
  $sql= "SELECT AVG(LOCDVS_W_CTR) AS CTRMEDIO_W, AVG(LOCDVS_W_POSICIONMEDIA) AS POSICIONMEDIA_W, AVG(LOCDVS_M_CTR) AS CTRMEDIO_M, AVG(LOCDVS_M_POSICIONMEDIA) AS POSICIONMEDIA_M
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $ctrmedio_w = $fila[0];
          $posmedia_w = $fila[1];
          $ctrmedio_m = $fila[2];
          $posmedia_m = $fila[3];
        }

      }else{
        $ctrmedio_w = 0;
        $posmedia_w = 0;
        $ctrmedio_m = 0;
        $posmedia_m = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOS
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect ".$control." and not LOCDVS_w_IMPRESIONES IS NULL";  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $terminos_w   = $fila[0];
        }

      }else{
        $terminos_w   = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOS
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect ".$control." and not LOCDVS_m_IMPRESIONES IS NULL";  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $terminos_m   = $fila[0];
        }

      }else{
        $terminos_m   = 0;
      }
  }


  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) URLS
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pagsaterrizaje = $fila[0];
        }

      }else{
        $pagsaterrizaje = 0;
      }
  }

  //PARA ESTE TOTAL CALCULAMOS DESDE SQL SI ES DE CONTROL
  if($ctr == 0){
    $c = Nuevo_PDO();
    $sql= "SELECT SUM(LOCDVT_W_IMPRESIONES) IMPRESIONES_W,SUM(LOCDVT_W_CLICS) CLICS_W,SUM(LOCDVT_M_IMPRESIONES) IMPRESIONES_M,SUM(LOCDVT_M_CLICS) CLICS_M
           FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
           where LOCDVT_NOMBREDOMINIO = :urlVista
           and LOCDVT_ANOMES = :mesSelect ";  
   
    if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $impresiones_w = $fila[0];
          $clicks_w      = $fila[1];
          $impresiones_m = $fila[2];
          $clicks_m      = $fila[3];
        }

      }else{
        $impresiones_w = 0;
        $clicks_w      = 0;
        $impresiones_m = 0;
        $clicks_m      = 0;
      }
    }
  }else{
    $c = Nuevo_PDO();
    //Hallamos las impresiones y clicks s�lo de los t�rminos de control, sumando de la tabla LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS.
    //Este resultado lo multiplicamos por el % estimado de m�s impresiones y clics que resulta de la diferencia entre lo que nos muestra GW en LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS y en LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
    $sql= "SELECT 
          SUM(LOCDVS_W_IMPRESIONES) * 100 /
          ((SELECT SUM(LOCDVS_W_IMPRESIONES)
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS AS A
          where LOCDVS_NOMBREDOMINIO = :urlVistaa
               and LOCDVS_ANOMES = :mesSelecta
          )*100 / (
                SELECT LOCDVT_W_IMPRESIONES
                FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
                where LOCDVT_NOMBREDOMINIO = :urlVistab
                      and LOCDVT_ANOMES = :mesSelectb
          )) AS IMPRESIONES_W_ESTIMADAS
          ,SUM(LOCDVS_W_CLICS) * 100 /
          ((SELECT SUM(LOCDVS_W_CLICS)
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS AS A
          where LOCDVS_NOMBREDOMINIO = :urlVistac
               and LOCDVS_ANOMES = :mesSelectc
          )*100 / (
                SELECT LOCDVT_W_CLICS
                FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
                where LOCDVT_NOMBREDOMINIO = :urlVistad
                      and LOCDVT_ANOMES = :mesSelectd
          )) AS CLICS_W_ESTIMADOS
          ,SUM(LOCDVS_M_IMPRESIONES) * 100 /
          ((SELECT SUM(LOCDVS_M_IMPRESIONES)
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS AS A
          where LOCDVS_NOMBREDOMINIO = :urlVistae
               and LOCDVS_ANOMES = :mesSelecte
          )*100 / (
                SELECT LOCDVT_M_IMPRESIONES
                FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
                where LOCDVT_NOMBREDOMINIO = :urlVistaf
                      and LOCDVT_ANOMES = :mesSelectf
          )) AS IMPRESIONES_M_ESTIMADAS
          ,SUM(LOCDVS_M_CLICS) * 100 /
          ((SELECT SUM(LOCDVS_M_CLICS)
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS AS A
          where LOCDVS_NOMBREDOMINIO = :urlVistag
               and LOCDVS_ANOMES = :mesSelectg
          )*100 / (
                SELECT LOCDVT_M_CLICS
                FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
                where LOCDVT_NOMBREDOMINIO = :urlVistah
                      and LOCDVT_ANOMES = :mesSelecth
          )) AS CLICS_M_ESTIMADOS
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS AS A
          where LOCDVS_NOMBREDOMINIO = :urlVistai
               and LOCDVS_ANOMES = :mesSelecti
               and LOCDVS_TERMINO IN (Select LOCDVTC_TERMINOCONTROL From LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL WHERE LOCDVTC_NOMBREDOMINIO=A.LOCDVS_NOMBREDOMINIO)
          ";  
   
    if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVistaa' => $urlVista,':mesSelecta' => $mesSelect,':urlVistab' => $urlVista,':mesSelectb' => $mesSelect,':urlVistac' => $urlVista,':mesSelectc' => $mesSelect,':urlVistad' => $urlVista,':mesSelectd' => $mesSelect,':urlVistae' => $urlVista,':mesSelecte' => $mesSelect,':urlVistaf' => $urlVista,':mesSelectf' => $mesSelect,':urlVistag' => $urlVista,':mesSelectg' => $mesSelect,':urlVistah' => $urlVista,':mesSelecth' => $mesSelect,':urlVistai' => $urlVista,':mesSelecti' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $impresiones_w = $fila[0];
          $clicks_w      = $fila[1];
          $impresiones_m = $fila[2];
          $clicks_m      = $fila[3];
        }

      }else{
        $impresiones_w = 0;
        $clicks_w      = 0;
        $impresiones_m = 0;
        $clicks_m      = 0;
      }
    }
  }

  /*PAGINAS */
  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect
         and LOCDVS_w_POSICIONMEDIA > 0 and LOCDVS_w_POSICIONMEDIA <= 10 ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag1_w = $fila[0];
        }

      }else{
        $pag1_w = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect
         and LOCDVS_w_POSICIONMEDIA > 10
         and LOCDVS_w_POSICIONMEDIA <= 20 ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag2_w = $fila[0];
        }

      }else{
        $pag2_w = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect
         and LOCDVS_w_POSICIONMEDIA > 20
         and LOCDVS_w_POSICIONMEDIA <= 30 ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag3_w = $fila[0];
        }

      }else{
        $pag3_w = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect
         and (LOCDVS_w_POSICIONMEDIA > 30 OR LOCDVS_W_POSICIONMEDIA = 0) ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag4_w = $fila[0];
        }

      }else{
        $pag4_w = 0;
      }
  }

  /*PAGINAS MOBILE */
  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect
         and LOCDVS_m_POSICIONMEDIA > 0 and LOCDVS_m_POSICIONMEDIA <= 10 ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag1_m = $fila[0];
        }

      }else{
        $pag1_m = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect
         and LOCDVS_m_POSICIONMEDIA > 10
         and LOCDVS_m_POSICIONMEDIA <= 20 ".$control; 
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag2_m = $fila[0];
        }

      }else{
        $pag2_m = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect
         and LOCDVS_m_POSICIONMEDIA > 20
         and LOCDVS_m_POSICIONMEDIA <= 30 ".$control; 
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag3_m = $fila[0];
        }

      }else{
        $pag3_m = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVS_TERMINO) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
         where LOCDVS_NOMBREDOMINIO = :urlVista
         and LOCDVS_ANOMES = :mesSelect
         and (LOCDVS_m_POSICIONMEDIA > 30 OR LOCDVS_m_POSICIONMEDIA = 0 )".$control;   
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag4_m = $fila[0];
        }

      }else{
        $pag4_m = 0;
      }
  }

  //PROCENTAJES
  if ($impresiones_w == 0) {
    $mediacrt_w = 0;
  }else{
    $mediacrt_w = ($clicks_w*100)/$impresiones_w;
  }
  if($impresiones_m == 0){
    $mediacrt_m = 0;
  }else{
    $mediacrt_m = ($clicks_m*100)/$impresiones_m;
  }
  
  if($terminos_w == 0){
    $pag1_por_w   = 0;
    $pag2_por_w   = 0;
    $pag3_por_w   = 0;
    $pag4_por_w   = 0;
    $pag1_por_m   = 0;
    $pag2_por_m   = 0;
    $pag3_por_m   = 0;
    $pag4_por_m   = 0;
  }else{
    $pag1_por_w   = ($pag1_w*100)/$terminos_w;
    $pag2_por_w   = ($pag2_w*100)/$terminos_w;
    $pag3_por_w   = ($pag3_w*100)/$terminos_w;
    $pag4_por_w   = ($pag4_w*100)/$terminos_w;
    if($pag1_m == 0){
      $pag1_por_m   = 0;//($pag1_m*100)/$terminos_m;
    }else{
      $pag1_por_m   = ($pag1_m*100)/$terminos_m;
    }
    if($pag1_m == 0){
      $pag2_por_m   = 0;//($pag1_m*100)/$terminos_m;
    }else{
      $pag2_por_m   = ($pag2_m*100)/$terminos_m;
    }
    if($pag1_m == 0){
      $pag3_por_m   = 0;//($pag1_m*100)/$terminos_m;
    }else{
      $pag3_por_m   = ($pag3_m*100)/$terminos_m;
    }
    if($pag1_m == 0){
      $pag4_por_m   = 0;//($pag1_m*100)/$terminos_m;
    }else{
      $pag4_por_m   = ($pag4_m*100)/$terminos_m;
    }
    //$pag2_por_m   = 0;($pag2_m*100)/$terminos_m;
    //$pag3_por_m   = 0;($pag3_m*100)/$terminos_m;
    // $pag4_por_m   = 0;($pag4_m*100)/$terminos_m;
  }
  

  //Montamos un json
  $strA = '{';
  $strA .= '"comparacion":"'.$comp.'",';
  $strA .= '"seo_terminos_w_f":"'.number_format($terminos_w,0,",",".").'",';
  $strA .= '"seo_terminos_m_f":"'.number_format($terminos_m,0,",",".").'",';
  $strA .= '"seo_ctrmedio_w_f":"'.number_format($mediacrt_w,1,",",".").'",';
  $strA .= '"seo_ctrmedio_m_f":"'.number_format($mediacrt_m,1,",",".").'",';
  $strA .= '"seo_posmedia_w_f":"'.number_format($posmedia_w,1,",",".").'",';
  $strA .= '"seo_posmedia_m_f":"'.number_format($posmedia_m,1,",",".").'",';
  $strA .= '"seo_pagsaterrizaje_f":"'.number_format($pagsaterrizaje,0,",",".").'",';
  $strA .= '"seo_impresiones_w_f":"'.number_format($impresiones_w,0,",",".").'",';
  $strA .= '"seo_impresiones_m_f":"'.number_format($impresiones_m,0,",",".").'",';
  $strA .= '"seo_clicks_w_f":"'.number_format($clicks_w,0,",",".").'",';
  $strA .= '"seo_clicks_m_f":"'.number_format($clicks_m,0,",",".").'",';
  $strA .= '"seo_pag1_w_f":"'.number_format($pag1_w,0,",",".").'",';
  $strA .= '"seo_pag2_w_f":"'.number_format($pag2_w,0,",",".").'",';
  $strA .= '"seo_pag3_w_f":"'.number_format($pag3_w,0,",",".").'",';
  $strA .= '"seo_pag4_w_f":"'.number_format($pag4_w,0,",",".").'",';
  $strA .= '"seo_pag1_w_rate_f":"'.number_format($pag1_por_w,1,",",".").'%",';
  $strA .= '"seo_pag2_w_rate_f":"'.number_format($pag2_por_w,1,",",".").'%",';
  $strA .= '"seo_pag3_w_rate_f":"'.number_format($pag3_por_w,1,",",".").'%",';
  $strA .= '"seo_pag4_w_rate_f":"'.number_format($pag4_por_w,1,",",".").'%",';
  $strA .= '"seo_pag1_m_f":"'.number_format($pag1_m,0,",",".").'",';
  $strA .= '"seo_pag2_m_f":"'.number_format($pag2_m,0,",",".").'",';
  $strA .= '"seo_pag3_m_f":"'.number_format($pag3_m,0,",",".").'",';
  $strA .= '"seo_pag4_m_f":"'.number_format($pag4_m,0,",",".").'",';
  $strA .= '"seo_pag1_m_rate_f":"'.number_format($pag1_por_m,1,",",".").'%",';
  $strA .= '"seo_pag2_m_rate_f":"'.number_format($pag2_por_m,1,",",".").'%",';
  $strA .= '"seo_pag3_m_rate_f":"'.number_format($pag3_por_m,1,",",".").'%",';
  $strA .= '"seo_pag4_m_rate_f":"'.number_format($pag4_por_m,1,",",".").'%",';

  /*Sin formatear*/
  $strA .= '"seo_terminos_w":"'.$terminos_w.'",';
  $strA .= '"seo_terminos_m":"'.$terminos_m.'",';
  $strA .= '"seo_ctrmedio_w":"'.$mediacrt_w.'",';
  $strA .= '"seo_ctrmedio_m":"'.$mediacrt_m.'",';
  $strA .= '"seo_posmedia_w":"'.$posmedia_w.'",';
  $strA .= '"seo_posmedia_m":"'.$posmedia_m.'",';
  $strA .= '"seo_pagsaterrizaje":"'.$pagsaterrizaje.'",';
  $strA .= '"seo_impresiones_w":"'.$impresiones_w.'",';
  $strA .= '"seo_impresiones_m":"'.$impresiones_m.'",';
  $strA .= '"seo_clicks_w":"'.$clicks_w.'",';
  $strA .= '"seo_clicks_m":"'.$clicks_m.'",';
  $strA .= '"seo_pag1_w":"'.$pag1_w.'",';
  $strA .= '"seo_pag2_w":"'.$pag2_w.'",';
  $strA .= '"seo_pag3_w":"'.$pag3_w.'",';
  $strA .= '"seo_pag4_w":"'.$pag4_w.'",';
  $strA .= '"seo_pag1_w_rate":"'.$pag1_por_w.'%",';
  $strA .= '"seo_pag2_w_rate":"'.$pag2_por_w.'%",';
  $strA .= '"seo_pag3_w_rate":"'.$pag3_por_w.'%",';
  $strA .= '"seo_pag4_w_rate":"'.$pag4_por_w.'%",';
  $strA .= '"seo_pag1_m":"'.$pag1_m.'",';
  $strA .= '"seo_pag2_m":"'.$pag2_m.'",';
  $strA .= '"seo_pag3_m":"'.$pag3_m.'",';
  $strA .= '"seo_pag4_m":"'.$pag4_m.'",';
  $strA .= '"seo_pag1_m_rate":"'.$pag1_por_m.'%",';
  $strA .= '"seo_pag2_m_rate":"'.$pag2_por_m.'%",';
  $strA .= '"seo_pag3_m_rate":"'.$pag3_por_m.'%",';
  $strA .= '"seo_pag4_m_rate":"'.$pag4_por_m.'%"';
  $strA .= '}';

  return $strA;

}

function SeoExtras_pags($urlVista,$mesSelect,$comp,$control){

  if($control == 1){
    $control = " and LOCDVP_URL IN (Select LOCDVUC_URLCONTROL From LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL WHERE LOCDVUC_NOMBREDOMINIO=LOCDVP_NOMBREDOMINIO)";
    $ctr=1;
  }else{
    $control = "";
    $ctr=0;
  }


  $c = Nuevo_PDO();
  $sql= "SELECT AVG(LOCDVP_W_CTR) AS CTRMEDIO_W, AVG(LOCDVP_W_POSICIONMEDIA) AS POSICIONMEDIA_W, AVG(LOCDVP_M_CTR) AS CTRMEDIO_M, AVG(LOCDVP_M_POSICIONMEDIA) AS POSICIONMEDIA_M
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $ctrmedio_w = $fila[1];
          $posmedia_w = $fila[2];
          $ctrmedio_m = $fila[3];
          $posmedia_m = $fila[4];
        }

      }else{
        $ctrmedio_w = 0;
        $posmedia_w = 0;
        $ctrmedio_m = 0;
        $posmedia_m = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOS
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect ".$control." and not LOCDVP_w_IMPRESIONES IS NULL";  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $terminos_w   = $fila[0];
        }

      }else{
        $terminos_w   = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOS
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect ".$control." and not LOCDVP_m_IMPRESIONES IS NULL";  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $terminos_m   = $fila[0];
        }

      }else{
        $terminos_m   = 0;
      }
  }


  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) URLS
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pagsaterrizaje = $fila[0];
        }

      }else{
        $pagsaterrizaje = 0;
      }
  }

  if($ctr==0){

    $c = Nuevo_PDO();
    $sql= "SELECT SUM(LOCDVT_W_IMPRESIONES) IMPRESIONES_W,SUM(LOCDVT_W_CLICS) CLICS_W,SUM(LOCDVT_M_IMPRESIONES) IMPRESIONES_M,SUM(LOCDVT_M_CLICS) CLICS_M
           FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
           where LOCDVT_NOMBREDOMINIO = :urlVista
           and LOCDVT_ANOMES = :mesSelect ";  
   
    if($stmt = $c->prepare($sql)){

        $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

        if ($result && $stmt->rowCount() != 0){
          while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
            $impresiones_w = $fila[0];
            $clicks_w      = $fila[1];
            $impresiones_m = $fila[2];
            $clicks_m      = $fila[3];
          }

        }else{
          $impresiones_w = 0;
          $clicks_w      = 0;
          $impresiones_m = 0;
          $clicks_m      = 0;
        }
    }
  }else{
    $c = Nuevo_PDO();
    //Hallamos las impresiones y clicks s�lo de los t�rminos de control, sumando de la tabla LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS.
    //Este resultado lo multiplicamos por el % estimado de m�s impresiones y clics que resulta de la diferencia entre lo que nos muestra GW en LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS y en LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
    $sql= "SELECT 
          SUM(LOCDVP_W_IMPRESIONES) * 100 /
          ((SELECT SUM(LOCDVP_W_IMPRESIONES)
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS AS A
          where LOCDVP_NOMBREDOMINIO = :urlVistaa
               and LOCDVP_ANOMES = :mesSelecta
          )*100 / (
                SELECT LOCDVT_W_IMPRESIONES
                FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
                where LOCDVT_NOMBREDOMINIO = :urlVistab
                      and LOCDVT_ANOMES = :mesSelectb
          )) AS IMPRESIONES_W_ESTIMADAS
          ,SUM(LOCDVP_W_CLICS) * 100 /
          ((SELECT SUM(LOCDVP_W_CLICS)
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS AS A
          where LOCDVP_NOMBREDOMINIO = :urlVistac
               and LOCDVP_ANOMES = :mesSelectc
          )*100 / (
                SELECT LOCDVT_W_CLICS
                FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
                where LOCDVT_NOMBREDOMINIO = :urlVistad
                      and LOCDVT_ANOMES = :mesSelectd
          )) AS CLICS_W_ESTIMADOS
          ,SUM(LOCDVP_M_IMPRESIONES) * 100 /
          ((SELECT SUM(LOCDVP_M_IMPRESIONES)
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS AS A
          where LOCDVP_NOMBREDOMINIO = :urlVistae
               and LOCDVP_ANOMES = :mesSelecte
          )*100 / (
                SELECT LOCDVT_M_IMPRESIONES
                FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
                where LOCDVT_NOMBREDOMINIO = :urlVistaf
                      and LOCDVT_ANOMES = :mesSelectf
          )) AS IMPRESIONES_M_ESTIMADAS
          ,SUM(LOCDVP_M_CLICS) * 100 /
          ((SELECT SUM(LOCDVP_M_CLICS)
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS AS A
          where LOCDVP_NOMBREDOMINIO = :urlVistag
               and LOCDVP_ANOMES = :mesSelectg
          )*100 / (
                SELECT LOCDVT_M_CLICS
                FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TOTALES
                where LOCDVT_NOMBREDOMINIO = :urlVistah
                      and LOCDVT_ANOMES = :mesSelecth
          )) AS CLICS_M_ESTIMADOS
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS AS A
          where LOCDVP_NOMBREDOMINIO = :urlVistai
               and LOCDVP_ANOMES = :mesSelecti
               and LOCDVP_URL IN (Select LOCDVUC_URLCONTROL 
               From LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL 
               WHERE LOCDVUC_NOMBREDOMINIO=A.LOCDVP_NOMBREDOMINIO)";  
   
    if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVistaa' => $urlVista,':mesSelecta' => $mesSelect,':urlVistab' => $urlVista,':mesSelectb' => $mesSelect,':urlVistac' => $urlVista,':mesSelectc' => $mesSelect,':urlVistad' => $urlVista,':mesSelectd' => $mesSelect,':urlVistae' => $urlVista,':mesSelecte' => $mesSelect,':urlVistaf' => $urlVista,':mesSelectf' => $mesSelect,':urlVistag' => $urlVista,':mesSelectg' => $mesSelect,':urlVistah' => $urlVista,':mesSelecth' => $mesSelect,':urlVistai' => $urlVista,':mesSelecti' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $impresiones_w = $fila[0];
          $clicks_w      = $fila[1];
          $impresiones_m = $fila[2];
          $clicks_m      = $fila[3];
        }

      }else{
        $impresiones_w = 0;
        $clicks_w      = 0;
        $impresiones_m = 0;
        $clicks_m      = 0;
      }
    }
  }

  /*PAGINAS */
  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect
         and LOCDVP_w_POSICIONMEDIA > 0 and LOCDVP_w_POSICIONMEDIA <= 10 ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag1_w = $fila[0];
        }

      }else{
        $pag1_w = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect
         and LOCDVP_w_POSICIONMEDIA > 10
         and LOCDVP_w_POSICIONMEDIA <= 20 ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag2_w = $fila[0];
        }

      }else{
        $pag2_w = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect
         and LOCDVP_w_POSICIONMEDIA > 20
         and LOCDVP_w_POSICIONMEDIA <= 30 ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag3_w = $fila[0];
        }

      }else{
        $pag3_w = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect
         and (LOCDVP_w_POSICIONMEDIA > 30 OR LOCDVP_W_POSICIONMEDIA = 0) ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag4_w = $fila[0];
        }

      }else{
        $pag4_w = 0;
      }
  }

  /*PAGINAS MOBILE */
  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect
         and LOCDVP_m_POSICIONMEDIA > 0 and LOCDVP_m_POSICIONMEDIA <= 10 ".$control;  
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag1_m = $fila[0];
        }

      }else{
        $pag1_m = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect
         and LOCDVP_m_POSICIONMEDIA > 10
         and LOCDVP_m_POSICIONMEDIA <= 20 ".$control; 
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag2_m = $fila[0];
        }

      }else{
        $pag2_m = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect
         and LOCDVP_m_POSICIONMEDIA > 20
         and LOCDVP_m_POSICIONMEDIA <= 30 ".$control; 
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag3_m = $fila[0];
        }

      }else{
        $pag3_m = 0;
      }
  }

  $c = Nuevo_PDO();
  $sql= "SELECT COUNT(LOCDVP_URL) TERMINOSPAGINA1
         FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
         where LOCDVP_NOMBREDOMINIO = :urlVista
         and LOCDVP_ANOMES = :mesSelect
         and (LOCDVP_m_POSICIONMEDIA > 30 OR LOCDVP_m_POSICIONMEDIA = 0) ".$control;   
 
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $urlVista,':mesSelect' => $mesSelect));

      if ($result && $stmt->rowCount() != 0){
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $pag4_m = $fila[0];
        }

      }else{
        $pag4_m = 0;
      }
  }

  //PROCENTAJES
  if ($impresiones_w == 0) {
    $mediacrt_w = 0;
  }else{
    $mediacrt_w = ($clicks_w*100)/$impresiones_w;
  }
  if($impresiones_m == 0){
    $mediacrt_m = 0;
  }else{
    $mediacrt_m = ($clicks_m*100)/$impresiones_m;
  }
  
  if($terminos_w == 0){
    $pag1_por_w   = 0;
    $pag2_por_w   = 0;
    $pag3_por_w   = 0;
    $pag4_por_w   = 0;
    $pag1_por_m   = 0;
    $pag2_por_m   = 0;
    $pag3_por_m   = 0;
    $pag4_por_m   = 0;
  }else{
    $pag1_por_w   = ($pag1_w*100)/$terminos_w;
    $pag2_por_w   = ($pag2_w*100)/$terminos_w;
    $pag3_por_w   = ($pag3_w*100)/$terminos_w;
    $pag4_por_w   = ($pag4_w*100)/$terminos_w;
    $pag1_por_m   = ($pag1_m*100)/$terminos_m;
    $pag2_por_m   = ($pag2_m*100)/$terminos_m;
    $pag3_por_m   = ($pag3_m*100)/$terminos_m;
    $pag4_por_m   = ($pag4_m*100)/$terminos_m;
  }

  //Montamos un json
  $strA = '{';
  $strA .= '"comparacion":"'.$comp.'",';
  $strA .= '"seo_terminos_w":"'.number_format($terminos_w,0,",",".").'",';
  $strA .= '"seo_terminos_m":"'.number_format($terminos_m,0,",",".").'",';
  $strA .= '"seo_ctrmedio_w":"'.number_format($mediacrt_w,1,",",".").'",';
  $strA .= '"seo_ctrmedio_m":"'.number_format($mediacrt_m,1,",",".").'",';
  $strA .= '"seo_posmedia_w":"'.number_format($posmedia_w,1,",",".").'",';
  $strA .= '"seo_posmedia_m":"'.number_format($posmedia_m,1,",",".").'",';
  $strA .= '"seo_pagsaterrizaje":"'.number_format($pagsaterrizaje,0,",",".").'",';
  $strA .= '"seo_impresiones_w":"'.number_format($impresiones_w,0,",",".").'",';
  $strA .= '"seo_impresiones_m":"'.number_format($impresiones_m,0,",",".").'",';
  $strA .= '"seo_clicks_w":"'.number_format($clicks_w,0,",",".").'",';
  $strA .= '"seo_clicks_m":"'.number_format($clicks_m,0,",",".").'",';
  $strA .= '"seo_pag1_w":"'.number_format($pag1_w,0,",",".").'",';
  $strA .= '"seo_pag2_w":"'.number_format($pag2_w,0,",",".").'",';
  $strA .= '"seo_pag3_w":"'.number_format($pag3_w,0,",",".").'",';
  $strA .= '"seo_pag4_w":"'.number_format($pag4_w,0,",",".").'",';
  $strA .= '"seo_pag1_w_rate":"'.number_format($pag1_por_w,1,",",".").'%",';
  $strA .= '"seo_pag2_w_rate":"'.number_format($pag2_por_w,1,",",".").'%",';
  $strA .= '"seo_pag3_w_rate":"'.number_format($pag3_por_w,1,",",".").'%",';
  $strA .= '"seo_pag4_w_rate":"'.number_format($pag4_por_w,1,",",".").'%",';
  $strA .= '"seo_pag1_m":"'.number_format($pag1_m,0,",",".").'",';
  $strA .= '"seo_pag2_m":"'.number_format($pag2_m,0,",",".").'",';
  $strA .= '"seo_pag3_m":"'.number_format($pag3_m,0,",",".").'",';
  $strA .= '"seo_pag4_m":"'.number_format($pag4_m,0,",",".").'",';
  $strA .= '"seo_pag1_m_rate":"'.number_format($pag1_por_m,1,",",".").'%",';
  $strA .= '"seo_pag2_m_rate":"'.number_format($pag2_por_m,1,",",".").'%",';
  $strA .= '"seo_pag3_m_rate":"'.number_format($pag3_por_m,1,",",".").'%",';
  $strA .= '"seo_pag4_m_rate":"'.number_format($pag4_por_m,1,",",".").'%"';
  $strA .= '}';

  return $strA;

}


function terminosControl_term($vistaUrl,$mesSelect,$control,$sort,$pag,$buscador){
  $count = 0;

  if($buscador != ""){
    //Primero comprobamos que la palabra no lleve comillas
    //Buscamos el primer y ��timo car�cterer
    //$ultima = substr ($palabra , -1);
    $ultima = $buscador[strlen($buscador)-1];
    $primera = $buscador[0];
    if($ultima == '"' && $primera == '"'){
      $buscador = str_replace('"', '', $buscador);
      $buscadorquery  = "and LOCDVS_TERMINO = '".$buscador."'";
      $buscadorqueryb = "and LOCDVTC_TERMINOCONTROL = '".$buscador."' OR LOCDVTC_TERMINOCONTROL_CATEGORIA = '".$buscador."'";
    }else{
      $buscadorquery  = "and LOCDVS_TERMINO LIKE '%".$buscador."%'";
      $buscadorqueryb = "and LOCDVTC_TERMINOCONTROL LIKE '%".$buscador."%' OR LOCDVTC_TERMINOCONTROL_CATEGORIA LIKE '%".$buscador."%'";
    }


    

  }else{
    $buscadorquery  = "";
    $buscadorqueryb = "";
  }

  //Para buscar los de una pagina
  switch ($pag[1]) {
    case 1:
      $querypag  = "and LOCDVS_".$pag[0]."_POSICIONMEDIA > 0 and LOCDVS_".$pag[0]."_POSICIONMEDIA <= 10";
      $querypagb = "";
      break;
    case 2:
      $querypag  = "and LOCDVS_".$pag[0]."_POSICIONMEDIA > 10 and LOCDVS_".$pag[0]."_POSICIONMEDIA <= 20";
      $querypagb = "";
      break;
    case 3:
      $querypag  = "and LOCDVS_".$pag[0]."_POSICIONMEDIA > 20 and LOCDVS_".$pag[0]."_POSICIONMEDIA <= 30";
      $querypagb = "";
      break;
    case 4:
      $querypag  = "and LOCDVS_".$pag[0]."_POSICIONMEDIA > 30 ";
      $querypagb = "or LOCDVS_".$pag[0]."_POSICIONMEDIA = 0";
      break;
    default:
      $querypag = "";
      $querypagb = "";
      break;
  }

  $devuelve = array();
  $c = Nuevo_PDO();

  if(empty($querypag)){ 
    $tercontrol = "UNION ALL
            SELECT LOCDVTC_TERMINOCONTROL,1 AS TERMINOCONTROL,null,null,null,null,null,null,null,null,LOCDVTC_TERMINOCONTROL_CATEGORIA as CATEGORIA
            FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL
            where LOCDVTC_NOMBREDOMINIO = '".$vistaUrl."'
            ".$buscadorqueryb."
            and LOCDVTC_TERMINOCONTROL NOT IN (SELECT LOCDVS_TERMINO FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS where LOCDVS_NOMBREDOMINIO = '".$vistaUrl."' and LOCDVS_ANOMES = '".$mesSelect."') ";
  }else{
    $tercontrol = "";
  }

  if($control==1){ 
    $sql = "SELECT LOCDVS_TERMINO,1 AS TERMINOCONTROL,LOCDVS_W_IMPRESIONES,LOCDVS_W_CLICS,LOCDVS_W_CTR,LOCDVS_W_POSICIONMEDIA,LOCDVS_M_IMPRESIONES,LOCDVS_M_CLICS,LOCDVS_M_CTR,LOCDVS_M_POSICIONMEDIA,LOCDVTC_TERMINOCONTROL_CATEGORIA as CATEGORIA
            FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS 
            inner join LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL ON LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS.LOCDVS_TERMINO = LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL.LOCDVTC_TERMINOCONTROL AND LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS.LOCDVS_NOMBREDOMINIO = LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL.LOCDVTC_NOMBREDOMINIO
            where LOCDVS_NOMBREDOMINIO = :urlVista
            and LOCDVS_ANOMES = :mesSelect
            ".$buscadorquery."
            ".$querypag."
            and LOCDVS_TERMINO IN (SELECT LOCDVTC_TERMINOCONTROL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL where LOCDVTC_NOMBREDOMINIO = :urlVistab)
            ".$tercontrol.$sort;
  }else{ 
    $sql = "SELECT LOCDVS_TERMINO,1 AS TERMINOCONTROL,LOCDVS_W_IMPRESIONES,LOCDVS_W_CLICS,LOCDVS_W_CTR,LOCDVS_W_POSICIONMEDIA,LOCDVS_M_IMPRESIONES,LOCDVS_M_CLICS,LOCDVS_M_CTR,LOCDVS_M_POSICIONMEDIA,LOCDVTC_TERMINOCONTROL_CATEGORIA as CATEGORIA
            FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS 
            inner join LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL ON LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS.LOCDVS_TERMINO = LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL.LOCDVTC_TERMINOCONTROL AND LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS.LOCDVS_NOMBREDOMINIO = LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL.LOCDVTC_NOMBREDOMINIO
            where LOCDVS_NOMBREDOMINIO = :urlVista
            and LOCDVS_ANOMES = :mesSelect
            ".$buscadorquery."
            ".$querypag."
            and LOCDVS_TERMINO IN (SELECT LOCDVTC_TERMINOCONTROL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL where LOCDVTC_NOMBREDOMINIO = :urlVistab)
            ".$tercontrol."
            UNION ALL
            SELECT LOCDVS_TERMINO,0 AS TERMINOCONTROL,LOCDVS_W_IMPRESIONES,LOCDVS_W_CLICS,LOCDVS_W_CTR,LOCDVS_W_POSICIONMEDIA,LOCDVS_M_IMPRESIONES,LOCDVS_M_CLICS,LOCDVS_M_CTR,LOCDVS_M_POSICIONMEDIA,null as CATEGORIA
            FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
            where LOCDVS_NOMBREDOMINIO = :urlVistae
            and LOCDVS_ANOMES = :mesSelectc
            ".$buscadorquery."
            ".$querypag.$querypagb."
            and LOCDVS_TERMINO NOT IN (SELECT LOCDVTC_TERMINOCONTROL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL where LOCDVTC_NOMBREDOMINIO = :urlVistaf) ".$sort;

  }

  
  if($stmt = $c->prepare($sql)){
      
      if($control==1){
        $result = $stmt->execute(array(':urlVista' => $vistaUrl,':mesSelect' => $mesSelect,':urlVistab' => $vistaUrl));
      }else{
        $result = $stmt->execute(array(':urlVista' => $vistaUrl,':mesSelect' => $mesSelect,':urlVistab' => $vistaUrl,':urlVistae' => $vistaUrl,':mesSelectc' => $mesSelect,':urlVistaf' => $vistaUrl));
      }
  
      if ($result && $stmt->rowCount() != 0){
        
        //Recorremos la primera sql
        while ($fila = $stmt->fetch(PDO::FETCH_BOTH, PDO::FETCH_ORI_NEXT)) {
          $devuelve["elementos"][$count]["termino"]       = $fila["LOCDVS_TERMINO"];
          $devuelve["elementos"][$count]["control"]       = $fila["TERMINOCONTROL"];
          $devuelve["elementos"][$count]["impresiones_w"] = $fila["LOCDVS_W_IMPRESIONES"];
          $devuelve["elementos"][$count]["clics_w"]       = $fila["LOCDVS_W_CLICS"];
          $devuelve["elementos"][$count]["ctr_w"]         = $fila["LOCDVS_W_CTR"];
          $devuelve["elementos"][$count]["posmedia_w"]    = $fila["LOCDVS_W_POSICIONMEDIA"];
          $devuelve["elementos"][$count]["impresiones_m"] = $fila["LOCDVS_M_IMPRESIONES"];
          $devuelve["elementos"][$count]["clics_m"]       = $fila["LOCDVS_M_CLICS"];
          $devuelve["elementos"][$count]["ctr_m"]         = $fila["LOCDVS_M_CTR"];
          $devuelve["elementos"][$count]["posmedia_m"]    = $fila["LOCDVS_M_POSICIONMEDIA"];
          $devuelve["elementos"][$count]["categoria"]     = $fila["CATEGORIA"];
          $count++;
        }

        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }

  return $devuelve;
}

function terminosControl_pags($vistaUrl,$mesSelect,$control,$sort,$pag,$buscador){
  $count = 0;

  if($buscador != ""){
    //Primero comprobamos que la palabra no lleve comillas
    //Buscamos el primer y ��timo car�cterer
    //$ultima = substr ($palabra , -1);
    $ultima = $buscador[strlen($buscador)-1];
    $primera = $buscador[0];
    if($ultima == '"' && $primera == '"'){
      $buscador = str_replace('"', '', $buscador);
      $buscadorquery  = "and LOCDVP_URL = '".$buscador."'";
      $buscadorqueryb = "and LOCDVUC_URLCONTROL = '".$buscador."' OR LOCDVUC_URLCONTROL_CATEGORIA = '".$buscador."'";
    }else{
      $buscadorquery  = "and LOCDVP_URL LIKE '%".$buscador."%'";
      $buscadorqueryb = "and LOCDVUC_URLCONTROL LIKE '%".$buscador."%' OR LOCDVUC_URLCONTROL_CATEGORIA LIKE '%".$buscador."%'";
    }


    

  }else{
    $buscadorquery  = "";
    $buscadorqueryb = "";
  }

  //Para buscar los de una pagina
  switch ($pag[1]) {
    case 1:
      $querypag  = "and LOCDVP_".$pag[0]."_POSICIONMEDIA > 0 and LOCDVP_".$pag[0]."_POSICIONMEDIA <= 10";
      $querypagb = "";
      break;
    case 2:
      $querypag  = "and LOCDVP_".$pag[0]."_POSICIONMEDIA > 10 and LOCDVP_".$pag[0]."_POSICIONMEDIA <= 20";
      $querypagb = "";
      break;
    case 3:
      $querypag  = "and LOCDVP_".$pag[0]."_POSICIONMEDIA > 20 and LOCDVP_".$pag[0]."_POSICIONMEDIA <= 30";
      $querypagb = "";
      break;
    case 4:
      $querypag  = "and LOCDVP_".$pag[0]."_POSICIONMEDIA > 30 ";
      $querypagb = "or LOCDVP_".$pag[0]."_POSICIONMEDIA = 0";
      break;
    default:
      $querypag = "";
      $querypagb = "";
      break;
  }

  $devuelve = array();
  $c = Nuevo_PDO();

  if(empty($querypag)){ 
    $tercontrol = "UNION ALL
            SELECT LOCDVUC_URLCONTROL,1 AS TERMINOCONTROL,null,null,null,null,null,null,null,null,LOCDVUC_URLCONTROL_CATEGORIA as CATEGORIA
            FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL
            where LOCDVUC_NOMBREDOMINIO = '".$vistaUrl."'
            ".$buscadorqueryb."
            and LOCDVUC_URLCONTROL NOT IN (SELECT LOCDVP_URL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS where LOCDVP_NOMBREDOMINIO = '".$vistaUrl."' and LOCDVP_ANOMES = '".$mesSelect."') ";
  }else{
    $tercontrol = "";
  }

  if($control==1){ 
    $sql = "SELECT LOCDVP_URL,1 AS TERMINOCONTROL,LOCDVP_W_IMPRESIONES,LOCDVP_W_CLICS,LOCDVP_W_CTR,LOCDVP_W_POSICIONMEDIA,LOCDVP_M_IMPRESIONES,LOCDVP_M_CLICS,LOCDVP_M_CTR,LOCDVP_M_POSICIONMEDIA,LOCDVUC_URLCONTROL_CATEGORIA as CATEGORIA
            FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS 
            inner join LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL ON LOC_DOMINIOS_VISTAS_WEBMASTER_URLS.LOCDVP_URL = LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL.LOCDVUC_URLCONTROL AND LOC_DOMINIOS_VISTAS_WEBMASTER_URLS.LOCDVP_NOMBREDOMINIO = LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL.LOCDVUC_NOMBREDOMINIO
            where LOCDVP_NOMBREDOMINIO = :urlVista
            and LOCDVP_ANOMES = :mesSelect
            ".$buscadorquery."
            ".$querypag."
            and LOCDVP_URL IN (SELECT LOCDVUC_URLCONTROL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL where LOCDVUC_NOMBREDOMINIO = :urlVistab)
            ".$tercontrol.$sort;
  }else{ 
    $sql = "SELECT LOCDVP_URL,1 AS TERMINOCONTROL,LOCDVP_W_IMPRESIONES,LOCDVP_W_CLICS,LOCDVP_W_CTR,LOCDVP_W_POSICIONMEDIA,LOCDVP_M_IMPRESIONES,LOCDVP_M_CLICS,LOCDVP_M_CTR,LOCDVP_M_POSICIONMEDIA,LOCDVUC_URLCONTROL_CATEGORIA as CATEGORIA
            FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS 
            inner join LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL ON LOC_DOMINIOS_VISTAS_WEBMASTER_URLS.LOCDVP_URL = LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL.LOCDVUC_URLCONTROL AND LOC_DOMINIOS_VISTAS_WEBMASTER_URLS.LOCDVP_NOMBREDOMINIO = LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL.LOCDVUC_NOMBREDOMINIO
            where LOCDVP_NOMBREDOMINIO = :urlVista
            and LOCDVP_ANOMES = :mesSelect
            ".$buscadorquery."
            ".$querypag."
            and LOCDVP_URL IN (SELECT LOCDVUC_URLCONTROL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL where LOCDVUC_NOMBREDOMINIO = :urlVistab)
            ".$tercontrol."
            UNION ALL
            SELECT LOCDVP_URL,0 AS TERMINOCONTROL,LOCDVP_W_IMPRESIONES,LOCDVP_W_CLICS,LOCDVP_W_CTR,LOCDVP_W_POSICIONMEDIA,LOCDVP_M_IMPRESIONES,LOCDVP_M_CLICS,LOCDVP_M_CTR,LOCDVP_M_POSICIONMEDIA,null as CATEGORIA
            FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
            where LOCDVP_NOMBREDOMINIO = :urlVistae
            and LOCDVP_ANOMES = :mesSelectc
            ".$buscadorquery."
            ".$querypag.$querypagb."
            and LOCDVP_URL NOT IN (SELECT LOCDVUC_URLCONTROL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL where LOCDVUC_NOMBREDOMINIO = :urlVistaf) ".$sort;

  }

  
  if($stmt = $c->prepare($sql)){
    
      if($control==1){
        $result = $stmt->execute(array(':urlVista' => $vistaUrl,':mesSelect' => $mesSelect,':urlVistab' => $vistaUrl));
      }else{
        $result = $stmt->execute(array(':urlVista' => $vistaUrl,':mesSelect' => $mesSelect,':urlVistab' => $vistaUrl,':urlVistae' => $vistaUrl,':mesSelectc' => $mesSelect,':urlVistaf' => $vistaUrl));
      }
  
      if ($result && $stmt->rowCount() != 0){
        
        //Recorremos la primera sql
        while ($fila = $stmt->fetch(PDO::FETCH_BOTH, PDO::FETCH_ORI_NEXT)) {
          $devuelve["elementos"][$count]["termino"]       = $fila["LOCDVP_URL"];
          $devuelve["elementos"][$count]["control"]       = $fila["TERMINOCONTROL"];
          $devuelve["elementos"][$count]["impresiones_w"] = $fila["LOCDVP_W_IMPRESIONES"];
          $devuelve["elementos"][$count]["clics_w"]       = $fila["LOCDVP_W_CLICS"];
          $devuelve["elementos"][$count]["ctr_w"]         = $fila["LOCDVP_W_CTR"];
          $devuelve["elementos"][$count]["posmedia_w"]    = $fila["LOCDVP_W_POSICIONMEDIA"];
          $devuelve["elementos"][$count]["impresiones_m"] = $fila["LOCDVP_M_IMPRESIONES"];
          $devuelve["elementos"][$count]["clics_m"]       = $fila["LOCDVP_M_CLICS"];
          $devuelve["elementos"][$count]["ctr_m"]         = $fila["LOCDVP_M_CTR"];
          $devuelve["elementos"][$count]["posmedia_m"]    = $fila["LOCDVP_M_POSICIONMEDIA"];
          $devuelve["elementos"][$count]["categoria"]     = $fila["CATEGORIA"];
          $count++;
        }

        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }

  return $devuelve;
}

//Funcion que devuelve una arraiy con todos los clientes del usuario enviado
function terminosControl_fuera($vistaUrl,$mesSelect,$control,$sort){

  //La siguiente SQL pinta primero los t�rminos de control y finalmente los t�rminos encontrados en GW que no son de control.

  //T�rminos que coinciden con alg�n t�rmino de control 
  $count = 0;

  $devuelve = array();
  $c = Nuevo_PDO();

  $sql = "SELECT LOCDVS_TERMINO AS TERMINO,1 AS TERMINOCONTROL,LOCDVS_IMPRESIONES AS IMPRESIONES,LOCDVS_CLICS AS CLICKS,LOCDVS_CTR AS CTR,LOCDVS_POSICIONMEDIA AS POSICIONMEDIA
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
          where LOCDVS_NOMBREDOMINIO = :urlVista
          and LOCDVS_ANOMES = :mesSelect
          and LOCDVS_TERMINO IN (SELECT LOCDVTC_TERMINOCONTROL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL where LOCDVTC_NOMBREDOMINIO = :urlVistab)";
  $sql .= $sort;        
  if($stmt = $c->prepare($sql)){
      
      $result = $stmt->execute(array(':urlVista' => $vistaUrl,':mesSelect' => $mesSelect,':urlVistab' => $vistaUrl));
  
      if ($result && $stmt->rowCount() != 0){
        
        //Recorremos la primera sql
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $devuelve["elementos"][$count]["termino"]     = $fila[0];
          $devuelve["elementos"][$count]["control"]     = $fila[1];
          $devuelve["elementos"][$count]["impresiones"] = $fila[2];
          $devuelve["elementos"][$count]["clics"]       = $fila[3];
          $devuelve["elementos"][$count]["ctr"]         = $fila[4];
          $devuelve["elementos"][$count]["posmedia"]    = $fila[5];
          $count++;
        }

        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }


  //Segunda SQL
  //T�rminos de control que no aparecen en LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS 
  $cb = Nuevo_PDO();
  $sqlb = "SELECT LOCDVTC_TERMINOCONTROL AS TERMINO,1 AS TERMINOCONTROL,null AS IMPRESIONES,null AS CLICKS,null AS CTR,null AS POSICIONMEDIA
           FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL
           where LOCDVTC_NOMBREDOMINIO = :urlVista
           and LOCDVTC_TERMINOCONTROL NOT IN (SELECT LOCDVS_TERMINO FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS where LOCDVS_NOMBREDOMINIO = :urlVistab and LOCDVS_ANOMES = :mesSelect)";
  $sql .= $sort;   
  $stmtb = $cb->prepare($sqlb);
  $resultb = $stmtb->execute(array(':urlVista' => $vistaUrl,':urlVistab' => $vistaUrl,':mesSelect' => $mesSelect));
  if ($resultb && $stmtb->rowCount() != 0){
          
    //Recorremos la primera sql
    while ($filab = $stmtb->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $devuelve["elementos"][$count]["termino"]     = $filab[0];
      $devuelve["elementos"][$count]["control"]     = $filab[1];
      $devuelve["elementos"][$count]["impresiones"] = $filab[2];
      $devuelve["elementos"][$count]["clics"]       = $filab[3];
      $devuelve["elementos"][$count]["ctr"]         = $filab[4];
      $devuelve["elementos"][$count]["posmedia"]    = $filab[5];
      $count++;
    }

    $devuelve["cantidad"] += $count;
  }else{
    $devuelve["cantidad"] += 0;
  }

  //Si el check de t�rminos de control est� desactivado mostramos tambi�n los que no son t�rminos de control
  if($control==0){

    //Tercera SQL
    //T�rminos que NO coinciden con alg�n t�rmino de control 
    $cc = Nuevo_PDO();
    $sqlc = "SELECT LOCDVS_TERMINO AS TERMINO,0 AS TERMINOCONTROL,LOCDVS_IMPRESIONES AS IMPRESIONES,LOCDVS_CLICS AS CLICKS,LOCDVS_CTR  AS CTR,LOCDVS_POSICIONMEDIA AS POSICIONMEDIA
             FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
             where LOCDVS_NOMBREDOMINIO = :urlVista
             and LOCDVS_ANOMES = :mesSelect
             and LOCDVS_TERMINO NOT IN (SELECT LOCDVTC_TERMINOCONTROL FROM LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL where LOCDVTC_NOMBREDOMINIO = :urlVistab)";
    $sql .= $sort;         
    $stmtc = $cc->prepare($sqlc);
    $resultc = $stmtc->execute(array(':urlVista' => $vistaUrl, ':mesSelect' => $mesSelect, ':urlVistab' => $vistaUrl));
    if ($resultc && $stmtc->rowCount() != 0){
            
      //Recorremos la primera sql
      while ($filac = $stmtc->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $devuelve["elementos"][$count]["termino"]     = $filac[0];
        $devuelve["elementos"][$count]["control"]     = $filac[1];
        $devuelve["elementos"][$count]["impresiones"] = $filac[2];
        $devuelve["elementos"][$count]["clics"]       = $filac[3];
        $devuelve["elementos"][$count]["ctr"]         = $filac[4];
        $devuelve["elementos"][$count]["posmedia"]    = $filac[5];
        $count++;
      }

      $devuelve["cantidad"] += $count;
    }else{
      $devuelve["cantidad"] += 0;
    }

  }



  return $devuelve;


}

//funcion que devuelve false si no es admin de la vista
function IsAdmin($idVista,$user){

  $com_emails["emails"][0] = "";
  $com_emails["emails"][1] = "";

  $maildom = dominioEmail($user);
  if(comDominiosEmails($maildom,$com_emails)){
    return true;
  }else{
    $c = Nuevo_PDO();

    $sql = "SELECT LOCDV_ANALYTICS_ID_VISTA,LOCD_NOMBREDOMINIO,CLIE_CODIGO,LOCUC_USUARIO
            FROM dbo.LOC_DOMINIOS_VISTAS 
               JOIN dbo.LOC_DOMINIOS ON LOCDV_SUDOMINIO = LOCD_CODIGO
               JOIN dbo.ISP_DOMINIOS ON LOCD_DOMINIO = DOM_CODIGO
               JOIN dbo.CLIENTES ON DOM_SUCLIENTE = CLIE_CODIGO
               JOIN dbo.LOC_USUARIOS_CLIENTES ON LOCUC_CLIENTE = CLIE_CODIGO
            WHERE LOCDV_ANALYTICS_ID_VISTA  = :idvista AND LOCUC_ESADMINISTRADOR = 1
            ORDER BY LOCUC_USUARIO ASC";

    if($stmt = $c->prepare($sql)){
        
        $result = $stmt->execute(array(':idvista' => $idVista));
    
        if ($result && $stmt->rowCount() != 0){
          
          //Recorremos la primera sql
          $sw=0;
          while ($fila = $stmt->fetch(PDO::FETCH_BOTH, PDO::FETCH_ORI_NEXT)) {

            if($fila["LOCUC_USUARIO"] == $user){
              $sw=1;
            }

          }

          if($sw==1){
            return true;
          }else{
            return false;
          }

        }else{
          
          return false;

        }
    }
  }
}

function paginasEntrada($vistaUrl,$mesSelect,$sort,$buscador){
  $count = 0;

  

  if($buscador != ""){

    $ultima = $buscador[strlen($buscador)-1];
    $primera = $buscador[0];
    if($ultima == '"' && $primera == '"'){
      $buscador = str_replace('"', '', $buscador);
      $buscadorquery  = "and LOCDVP_URL = '".$buscador."'";
    }else{
      $buscadorquery  = "and LOCDVP_URL LIKE '%".$buscador."%'";
    }


    
  }else{
    $buscadorquery  = "";
  }

  $devuelve = array();
  $c = Nuevo_PDO();

  $sql = "SELECT LOCDVP_URL ,LOCDVP_w_IMPRESIONES ,LOCDVP_w_CLICS ,LOCDVP_w_CTR ,LOCDVP_w_POSICIONMEDIA ,LOCDVP_m_IMPRESIONES ,LOCDVP_m_CLICS ,LOCDVP_m_CTR ,LOCDVP_m_POSICIONMEDIA
          FROM LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
          where LOCDVP_NOMBREDOMINIO = :urlVista
          and LOCDVP_ANOMES = :mesSelect ".$buscadorquery.$sort;

  if($stmt = $c->prepare($sql)){
      
      $result = $stmt->execute(array(':urlVista' => $vistaUrl,':mesSelect' => $mesSelect));
  
      if ($result && $stmt->rowCount() != 0){
        
        //Recorremos la primera sql
        while ($fila = $stmt->fetch(PDO::FETCH_BOTH, PDO::FETCH_ORI_NEXT)) {
          $devuelve["elementos"][$count]["termino"]       = $fila["LOCDVP_URL"];
          $devuelve["elementos"][$count]["impresiones_w"] = $fila["LOCDVP_w_IMPRESIONES"];
          $devuelve["elementos"][$count]["clics_w"]       = $fila["LOCDVP_w_CLICS"];
          $devuelve["elementos"][$count]["ctr_w"]         = $fila["LOCDVP_w_CTR"];
          $devuelve["elementos"][$count]["posmedia_w"]    = $fila["LOCDVP_w_POSICIONMEDIA"];
          $devuelve["elementos"][$count]["impresiones_m"] = $fila["LOCDVP_m_IMPRESIONES"];
          $devuelve["elementos"][$count]["clics_m"]       = $fila["LOCDVP_m_CLICS"];
          $devuelve["elementos"][$count]["ctr_m"]         = $fila["LOCDVP_m_CTR"];
          $devuelve["elementos"][$count]["posmedia_m"]    = $fila["LOCDVP_m_POSICIONMEDIA"];
          $count++;
        }

        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }

  return $devuelve;
}

function categorias_seo($vistaUrl,$tipo){
  $count = 0;

  $devuelve = array();
  $c = Nuevo_PDO();

  if($tipo == "term"){
    $sql = "SELECT LOCDVTC_TERMINOCONTROL_CATEGORIA, COUNT( LOCDVTC_TERMINOCONTROL_CATEGORIA) as CONTADOR
            FROM dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL
            WHERE LOCDVTC_NOMBREDOMINIO = :urlVista AND LOCDVTC_TERMINOCONTROL_CATEGORIA != 'NULL'
            GROUP BY LOCDVTC_TERMINOCONTROL_CATEGORIA";
  }else{
    $sql = "SELECT LOCDVUC_URLCONTROL_CATEGORIA, COUNT( LOCDVUC_URLCONTROL_CATEGORIA) as CONTADOR
            FROM dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL
            WHERE LOCDVUC_NOMBREDOMINIO = :urlVista AND LOCDVUC_URLCONTROL_CATEGORIA != 'NULL'
            GROUP BY LOCDVUC_URLCONTROL_CATEGORIA";
  }

  if($stmt = $c->prepare($sql)){
      
      $result = $stmt->execute(array(':urlVista' => $vistaUrl));
  
      if ($result && $stmt->rowCount() != 0){
        
        //Recorremos la primera sql
        while ($fila = $stmt->fetch(PDO::FETCH_BOTH, PDO::FETCH_ORI_NEXT)) {
          $devuelve["elementos"][$count]["categoria"]  = $fila[0];
          $devuelve["elementos"][$count]["contador"]   = $fila["CONTADOR"];
          $count++;
        }

        $devuelve["cantidad"] = $count;
      }else{
        $devuelve["cantidad"] = 0;
      }
  }

  return $devuelve;
}

//Funcion para insertar un nuevo termino de b�squeda
function guardarTermino($termino,$categoria,$url_vista,$tipo){
  //Fecha actual con formato 101503
  $fecha_m = date("Ym");
  //Primero comprobamos si existe en LOC_USUARIOS
  $c = Nuevo_PDO();

  if($tipo == "term"){
    $sql= "SELECT LOCDVS_CODIGO
           FROM dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOS
           WHERE LOCDVS_NOMBREDOMINIO = :url AND LOCDVS_ANOMES= :fecha AND LOCDVS_TERMINO = :termino";  
  }else{
    $sql= "SELECT LOCDVP_CODIGO
           FROM dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_URLS
           WHERE LOCDVP_NOMBREDOMINIO = :url AND LOCDVP_ANOMES= :fecha AND LOCDVP_TERMINO = :termino";  
  }

  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':url' => $url_vista, ':fecha' => $fecha_m, ':termino' => $termino ));

      if ($result && $stmt->rowCount() != 0){

        return false;
      
      }else{



         $d = Nuevo_PDO();

         if($tipo == "term"){
          $sql = "INSERT INTO dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL (LOCDVTC_NOMBREDOMINIO,LOCDVTC_TERMINOCONTROL,LOCDVTC_TERMINOCONTROL_CATEGORIA)
                 VALUES (:url,:termino,:categoria)";
         }else{
          $sql = "INSERT INTO dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL (LOCDVUC_NOMBREDOMINIO,LOCDVUC_URLCONTROL,LOCDVUC_URLCONTROL_CATEGORIA)
                 VALUES (:url,:termino,:categoria)";
         }
         
         $stmt = $d->prepare($sql);
         $resultb = $stmt->execute(array(':url' => $url_vista, ':termino' => $termino, ':categoria' => $categoria)); 
         return true;
      }

  }
}

function eliminarControl($termino,$url_vista,$tipo){

  $d = Nuevo_PDO();
  if($tipo == "term"){
    $sql = "DELETE FROM dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL 
            WHERE LOCDVTC_NOMBREDOMINIO = :url AND LOCDVTC_TERMINOCONTROL = :termino";
  }else{
    $sql = "DELETE FROM dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL 
            WHERE LOCDVUC_NOMBREDOMINIO = :url AND LOCDVUC_URLCONTROL = :termino";
  }
  
  $stmt = $d->prepare($sql);
  $resultb = $stmt->execute(array(':url' => $url_vista, ':termino' => $termino)); 
  return true;

}

function anadirControl($termino,$url_vista,$tipo){

  $d = Nuevo_PDO();
  if($tipo == "term"){
      $sql = "INSERT INTO dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL (LOCDVTC_NOMBREDOMINIO,LOCDVTC_TERMINOCONTROL)
              VALUES (:url,:termino)";
  }else{
      $sql = "INSERT INTO dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL (LOCDVUC_NOMBREDOMINIO,LOCDVUC_URLCONTROL)
              VALUES (:url,:termino)";
  }
  $stmt = $d->prepare($sql);
  $resultb = $stmt->execute(array(':url' => $url_vista, ':termino' => $termino)); 
  return true;

}

function editarTermino($termino,$terminoant,$url_vista,$mes,$cat,$tipo){


  $c = Nuevo_PDO();

  if($tipo == 'term'){
    $sql= "SELECT LOCDVTC_CODIGO
           FROM dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL
           WHERE LOCDVTC_NOMBREDOMINIO = :urlVista AND LOCDVTC_TERMINOCONTROL = :terminoant";  
  }else{
    $sql= "SELECT LOCDVUC_CODIGO
           FROM dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL
           WHERE LOCDVUC_NOMBREDOMINIO = :urlVista AND LOCDVUC_URLCONTROL = :terminoant";  
  }
  if($stmt = $c->prepare($sql)){

      $result = $stmt->execute(array(':urlVista' => $url_vista, ':terminoant' => $terminoant ));

      if ($result && $stmt->rowCount() != 0){

        $dbb = Nuevo_PDO();
        if($tipo == 'term'){
          $sqlb = "UPDATE dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_TERMINOSCONTROL
                   SET LOCDVTC_TERMINOCONTROL = :termino, LOCDVTC_TERMINOCONTROL_CATEGORIA = :cat
                   WHERE LOCDVTC_NOMBREDOMINIO = :urlVista AND LOCDVTC_TERMINOCONTROL = :terminoant";
        }else{
          $sqlb = "UPDATE dbo.LOC_DOMINIOS_VISTAS_WEBMASTER_URLSCONTROL
                   SET LOCDVUC_URLCONTROL = :termino, LOCDVUC_URLCONTROL_CATEGORIA = :cat
                   WHERE LOCDVUC_NOMBREDOMINIO = :urlVista AND LOCDVUC_URLCONTROL = :terminoant";
        }
        $stmtb = $dbb->prepare($sqlb);
        $resultb = $stmtb->execute(array(':termino' => $termino,':cat' => $cat, ':urlVista' => $url_vista,':terminoant' => $terminoant)); 
      
      }else{
        //No existe por lo que no actualizamos en esta tabla
      }
  }

  return true;

}

  function traductorGT($url) {
      //Procesamos la URL
      $contenido = file_get_contents($url);
      //Procesamos la respuesta
      $contenido = preg_replace('/,,,|,,/', ',"0",', $contenido);
      $contenido = json_decode($contenido);
   
      return trim($contenido[0][0][0]);
  }

  //Traduce las descripciones a otros idiomas
  function traduce_de_espanol($descripcion,$lang) {
      //Codificamos la descripci�n para que Google la entienda
      $descripcion = urlencode($descripcion);
      //Traducimos de Espa�ol a ingl�s
      switch ($lang) {
        case 'es':
          $url = "https://translate.google.es/translate_a/single?client=t&sl=es&tl=en&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&srcrom=0&ssel=3&tsel=6&tk=519297|902558&q=$descripcion";
          $resp = traductorGT($url);
          break;
        case 'en':
          $url = "https://translate.google.es/translate_a/single?client=t&sl=es&tl=en&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&srcrom=0&ssel=3&tsel=6&tk=519297|902558&q=$descripcion";
          $resp = traductorGT($url);
          break;
        case 'es':
          $url = "https://translate.google.es/translate_a/single?client=t&sl=es&tl=en&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&srcrom=0&ssel=3&tsel=6&tk=519297|902558&q=$descripcion";
          $resp = traductorGT($url);
          break;
        default:
          $resp = $descripcion;
          break;
      }


      /*$url = "https://translate.google.es/translate_a/single?client=t&sl=es&tl=en&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&srcrom=0&ssel=3&tsel=6&tk=519297|902558&q=$descripcion";
      $ingles = traductor($url);
      //Traducimos de Espa�ol a alem�n
      $url = "https://translate.google.es/translate_a/single?client=t&sl=es&tl=de&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&srcrom=0&ssel=3&tsel=4&tk=519297|902558&q=$descripcion";
      $aleman = traductor($url);
      //Traducimos de Espa�ol a ruso
      $url = "https://translate.google.es/translate_a/single?client=t&sl=es&tl=ru&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&srcrom=0&ssel=3&tsel=4&tk=519297|902558&q=$descripcion";
      $ruso = traductor($url);
      //Traducimos de Espa�ol a chino
      $url = "https://translate.google.es/translate_a/single?client=t&sl=es&tl=zh-TW&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&rom=1&srcrom=0&ssel=3&tsel=4&tk=519298|749572&q=$descripcion";
      $chino = traductor($url);*/
      //Guardamos el resultado

      return $resp;
  }

  function traduce_de_ingles($descripcion,$lang) {
      //Codificamos la descripci�n para que Google la entienda
      $descripcion = urlencode($descripcion);
      //Traducimos de Espa�ol a ingl�s
      if($descripcion != '%28not+set%29'){

        //$descripcion = str_replace('+', '%20', $descripcion);

        switch ($lang) {
          case 'es':
            $url = "https://translate.google.com/translate_a/single?client=t&sl=en&tl=es&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qc&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&otf=1&ssel=5&tsel=5&kc=1&tk=520791|210518&q=$descripcion";
            $resp = traductorGT($url);
            break;
          case 'en':
            $resp = $descripcion;
            break;
          case 'es':
            //$url = "https://translate.google.es/translate_a/single?client=t&sl=es&tl=en&hl=es&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&srcrom=0&ssel=3&tsel=6&tk=519297|902558&q=$descripcion";
            //$resp = traductor($url);
            break;
          default:
            $resp = $descripcion;
            break;
        }
        if(empty($resp)){
          $resp = $descripcion;
        }
       $resp = str_replace('+', ' ', $resp);
      }else{
        $resp = "(Desconocido)";
      }
      
      return $resp;
  }

//Funcion que devuelve el cuerpo de un informe de campa�a
function historialCampana($proceso){
  $devuelve = "";
  $c = Nuevo_PDO();
  $sql = "SELECT ENVEP_CUERPO
          FROM dbo.ENV_EMAILS_PROCESOS
          WHERE ENVEP_CODIGO = :proceso";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':proceso' => $proceso));

  if ($result && $stmt->rowCount() != 0){
    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
      $devuelve = $fila[0];
    }
  }else{
        $devuelve = "<p>No encuentra la campa�a</p>";
  }

  return $devuelve;

}



//funcion que devuelve el istorial de informes
function historialUser($email,$vista){
  $c = Nuevo_PDO();


  
  $sql = "SELECT ENVE_CODIGO, ENVE_USUARIO, ENVE_TIPO, ENVE_SU_EMAIL_PROCESO, ENVE_FECHA_HORA_ALTA, ENVE_FECHA_HORA_ENVIO, 
          ENVE_FECHA_HORA_LEIDO, ENVEP_PROCESO_IMAGEN, ENVEP_PROCESO_IMAGEN_OK, ENVEP_PROCESO_PDF, ENVEP_ASUNTO, 
          SUBSTRING(envep_identificacion,PATINDEX('%&fechaini=%',envep_identificacion)+10,10) as FECHAINICIO,
          SUBSTRING(envep_identificacion,PATINDEX('%&fechafin=%',envep_identificacion)+10,10) as FECHAFIN,
          SUBSTRING(envep_identificacion,PATINDEX('%&fechafin=%',envep_identificacion)+10,7) as ANOMESFIN,
          SUBSTRING(enve_tipo,1,3) as TIPO, ENVEP_CUERPO
          FROM dbo.ENV_EMAILS JOIN dbo.ENV_EMAILS_PROCESOS EEP ON ENVE_SU_EMAIL_PROCESO = EEP.ENVEP_CODIGO 
          WHERE ENVE_USUARIO = '".$email."' 
                  AND (ENVEP_ID_VISTA = '".$vista."' OR";
                      //Para a�adir los emails de seguimiento de campa�as que tienen alguna campa�a de la vista actual
  $sql.= "              (ENVEP_ID_VISTA IS NULL AND 
                        '".$vista."' IN (
                             SELECT 
                                    (SELECT TOP 1 LOCDV_ANALYTICS_ID_VISTA
                                      FROM LOC_DOMINIOS_VISTAS INNER JOIN LOC_DOMINIOS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO
                                     WHERE LOCD_ANALYTICS_ID_PROPIEDAD=C.LOCC_ANALYTICS_ID_PROPIEDAD
                                               AND LOCDV_ANALYTICS_DEFAULT_VISTA=1) AS VISTA_POR_DEFECTO       
                             FROM LOC_CAMPANAS as C inner join LOC_CAMPANAS_USUARIOS_DISTRIBUCION ON C.LOCC_CODIGO=LOC_CAMPANAS_USUARIOS_DISTRIBUCION.LOCCUD_SUCAMPANA
                             WHERE LOCCUD_ENVIOEMAIL_SEMANAL=1 AND LOCCUD_USUARIO = '".$email."' ";
                             //Comparamos la fecha inicio
  $sql.= "                   AND (LOCC_ACTIVARSEGUIMIENTO=1 AND (LOCC_FECHA_ENVIOS_INICIO IS NULL OR (LOCC_FECHA_ENVIOS_INICIO <=  SUBSTRING(EEP.envep_identificacion,PATINDEX('%&fechaini=%',EEP.envep_identificacion)+10,10)  )) )";
                             //Comparamos la fecha fin
  $sql.="                    AND (LOCC_ACTIVARSEGUIMIENTO=1 AND (LOCC_FECHA_ENVIOS_FIN IS NULL OR (LOCC_FECHA_ENVIOS_FIN >= SUBSTRING(EEP.envep_identificacion,PATINDEX('%&fechafin=%',EEP.envep_identificacion)+10,10) )) )
                             AND (LOCC_HISTORICO=0 OR LOCC_HISTORICO IS NULL)
                             ))          
                  )
          ORDER BY 
          SUBSTRING(envep_identificacion,PATINDEX('%&fechafin=%',envep_identificacion)+10,7) + SUBSTRING(enve_tipo,1,3) DESC,
          SUBSTRING(envep_identificacion,PATINDEX('%&fechafin=%',envep_identificacion)+10,10) DESC,
          DATEDIFF(dd,SUBSTRING(envep_identificacion,PATINDEX('%&fechaini=%',envep_identificacion)+10,10),SUBSTRING(envep_identificacion,PATINDEX('%&fechafin=%',envep_identificacion)+10,10)) desc";


  $stmt = $c->prepare($sql);
  $result = $stmt->execute();
  //echo $sql;
  if ($result && $stmt->rowCount() != 0){
      $count = 0;
      //echo "Filas: ".$stmt->rowCount()."<br>";
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $count++;
        $id = $fila[0];
        $devuelve["elementos"][$id]["cod"]            = $fila[0];
        $devuelve["elementos"][$id]["email"]          = $fila[1];
        $devuelve["elementos"][$id]["tipo"]           = $fila[2];
        $devuelve["elementos"][$id]["proceso"]        = $fila[3];
        $devuelve["elementos"][$id]["f_alta"]         = $fila[4];
        $devuelve["elementos"][$id]["f_envio"]        = $fila[5];
        $devuelve["elementos"][$id]["f_leido"]        = $fila[6];
        $devuelve["elementos"][$id]["proceso_imagen"] = $fila[7];
        $devuelve["elementos"][$id]["imagen"]         = $fila[8];
        $devuelve["elementos"][$id]["pdf"]            = $fila[9];
        $devuelve["elementos"][$id]["asunto"]         = $fila[10];
        $devuelve["elementos"][$id]["fechaini"]       = $fila[11];
        $devuelve["elementos"][$id]["fechafin"]       = $fila[12];
        $devuelve["elementos"][$id]["anomesfin"]      = $fila[13];
        $devuelve["elementos"][$id]["tipodesc"]       = $fila[14];
        $devuelve["elementos"][$id]["cuerpo"]         = $fila[15];
        $id++;
      }
      $devuelve["cantidad"]=$count;
  }else{
      $devuelve["cantidad"]=0;
  }

  return $devuelve;  
}

function guardarPdf($proceso,$pdf_generado){
  $c = Nuevo_PDO();
  $sql = "UPDATE dbo.ENV_EMAILS_PROCESOS
          SET ENVEP_PROCESO_PDF = :pdf
          WHERE ENVEP_CODIGO = :proc";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':pdf' => $pdf_generado,':proc' => $proceso));
};

//funcion que devuelve el istorial de informes
function ecomercePropiedad($propiedad){
  $c = Nuevo_PDO();
  $sql = "    SELECT LOCD_ANALYTICS_PROPIEDAD_COMERCIOELECTRONICO
    FROM dbo.LOC_DOMINIOS
    WHERE LOCD_ANALYTICS_ID_PROPIEDAD = :propiedad";
  $stmt = $c->prepare($sql);
  $result = $stmt->execute(array(':propiedad' => $propiedad));

  if ($result && $stmt->rowCount() != 0){

      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {

        $devuelve= $fila[0];
      }
      
  }else{
      
  }

  return $devuelve;  
}

  //funcion para actualizar o crear campanas de analytics
  function crearActualizarCampana($campana, $medio, $fuente, $propiedad){
    $id = 0;

    $c = Nuevo_PDO();
    $sql = "SELECT TOP 1 LOCC_CODIGO FROM LOC_CAMPANAS WHERE LOCC_CAMPAING = '{$campana}' AND LOCC_MEDIUM = '{$medio}' AND LOCC_SOURCE = '{$fuente}' AND LOCC_ANALYTICS_ID_PROPIEDAD = '{$propiedad}'";
    $stmt = $c->prepare($sql);
    $result = $stmt->execute();

    if ($result && $stmt->rowCount() != 0){
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $id = $fila[0];
      }
    }else{
      $sql = "INSERT INTO LOC_CAMPANAS (LOCC_CAMPAING, LOCC_MEDIUM, LOCC_SOURCE,LOCC_ANALYTICS_ID_PROPIEDAD, LOCC_FECHA_ALTA, LOCC_FECHA_MODIFICACION) 
            VALUES ('{$campana}','{$medio}','{$fuente}','{$propiedad}', '" . date('Y-m-d H:i:s'). ".000', '" . date('Y-m-d H:i:s'). ".000')";
      $stmt = $c->prepare($sql);
      $result = $stmt->execute();

      $sql = "SELECT IDENT_CURRENT('LOC_CAMPANAS')";
      $stmt = $c->prepare($sql);
      $result = $stmt->execute();

      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
        $id = $fila[0];
      }
    }



    return $id;
  }

  function comprobarErrorEnvioEmail($img){
    $error = '';
    $checkStrings = array("Server Error", "Error SummaI'Y", "H1'l'P Error");
    $ruta = 'C:\inetpub\vhosts\dmintegra.acc.com.es\httpdocs\admin\Envios\temp';

    try{
      $nameImg = str_replace('.png', '', substr($img, strrpos($img, '/') + 1));
      $imgBW = $ruta . '\bw_' . $nameImg . '.png';

      $imagen = file_get_contents($img);
      file_put_contents($imgBW, $imagen);

      $im = imagecreatefrompng($imgBW);
      //transformar imagen a escala de grises
      if ($im && imagefilter($im, IMG_FILTER_GRAYSCALE)) {
        imagepng($im, $imgBW);
        imagedestroy($im);

        // ejecutar comando para sacar el texto de la imagen
        $cmd = '"C:\Program Files (x86)\Tesseract-OCR\tesseract" ' . $imgBW . ' ' . $ruta . '\result_' . $nameImg . ' 2>&1';
        exec($cmd, $data);
        //print_r($data);

        if(file_exists($ruta . '\result_' . $nameImg . '.txt')){
          $content = file_get_contents($ruta . '\result_' . $nameImg . '.txt');
          @unlink($ruta . '\result_' . $nameImg . '.txt');
          @unlink($imgBW);

          // comprobar si hay indicio de error en el texto de la imagen
          $checked = 0;
          foreach($checkStrings as $checkString){
            $iError = strpos($content, $checkString);
            if($iError !== false){
              $checked++;
              $iError += strlen($checkString);
            }
          }

          if($checked === count($checkStrings)){
            $error = trim(substr($content, $iError));
          }
        }      
      } 
    }catch(Exception $e){
      
    }

    return $error;
  }

  //Funci�n que guarda los movim
  function pageLogs($email,$url,$propiedad,$vista,$cliente,$idvista){
    $fecha = date("Y-m-d H:i:s");
    $d = Nuevo_PDO();
    $sql = "INSERT INTO dbo.LOC_USUARIOS_LOG_USO (LOCULU_USUARIO,LOCULU_CLIENTE,LOCULU_ID_VISTA,LOCULU_DESC_VISTA,LOCULU_URL,LOCULU_FECHAHORA)
            VALUES (:email,:cliente,:idvista,:descvista,:url,:fecha)";
    $stmt = $d->prepare($sql);
    $resultb = $stmt->execute( array(':email' => $email,':cliente' => $cliente,':idvista' => $idvista,':descvista' => $propiedad." | ".$vista,':url' => $url,':fecha' => $fecha) ); 
    //echo "En funcion: ".$email." -> ".$url." -> ".$fecha." -> ".$propiedad." -> ".$vista." -> ".$cliente;
  }

  //Funcion que muestra las p�ginas vistas segun el a�o mes o dia de un usuario y el cliente
  function pagesViews($cuando,$cliente,$usuario,$vista){

    switch ($cuando) {
      case 'ano':
        $sql = "SELECT COUNT(LOCULU_CODIGO)
                FROM dbo.LOC_USUARIOS_LOG_USO
                WHERE LOCULU_USUARIO = '".$usuario."' AND LOCULU_CLIENTE = '".$cliente."' AND LOCULU_ID_VISTA='".$vista."' AND year(LOCULU_FECHAHORA) = '".date("Y")."'";
        break;
      case 'mes':
        $sql = "SELECT COUNT(LOCULU_CODIGO)
                FROM dbo.LOC_USUARIOS_LOG_USO
                WHERE LOCULU_USUARIO = '".$usuario."' AND LOCULU_CLIENTE = '".$cliente."' AND LOCULU_ID_VISTA='".$vista."' AND year(LOCULU_FECHAHORA) = '".date("Y")."' AND month(LOCULU_FECHAHORA) = '".date("m")."'";
        break;
      case 'dia':
        $sql = "SELECT COUNT(LOCULU_CODIGO)
                FROM dbo.LOC_USUARIOS_LOG_USO
                WHERE LOCULU_USUARIO = '".$usuario."' AND LOCULU_CLIENTE = '".$cliente."' AND LOCULU_ID_VISTA='".$vista."' AND year(LOCULU_FECHAHORA) = '".date("Y")."' AND month(LOCULU_FECHAHORA) = '".date("m")."'  AND day(LOCULU_FECHAHORA) = '".date("d")."'";
        break;
    }
    //echo $sql;

    $d = Nuevo_PDO();
    $stmt = $d->prepare($sql);
    $resultb = $stmt->execute();
    $nfilas = 0;
    if ($resultb && $stmt->rowCount() != 0){
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $nfilas = $fila[0];
      }
    }

  return $nfilas;

  }

  //Funcion para validar la vista del usuario
  function val_vistauser($email,$vista,$com_emails){

    $maildom = dominioEmail($email);
    if(comDominiosEmails($maildom,$com_emails)){
      return true;
    }else{
      $c = Nuevo_PDO();
      $sql = "SELECT LOCU_EMAIL, LOCDVU_ANALYTICS_ID_VISTA
           FROM dbo.LOC_USUARIOS JOIN dbo.LOC_DOMINIOS_VISTAS_USUARIOS ON LOCU_EMAIL = LOCDVU_USUARIO
           WHERE LOCU_EMAIL = '".$email."' AND LOCDVU_ANALYTICS_ID_VISTA = '".$vista."'";

      $stmt = $c->prepare($sql);
      $result = $stmt->execute();
      if ($result && $stmt->rowCount() != 0){
        return true;
      }else{
        //Si no est� ligada a esa vista comprobamos si es administrador de esa vista
        //Primero sacamos el cliente de esa vista
        $cliente = clienteVista($vista);
        $d = Nuevo_PDO();
        $sqlb = "SELECT LOCUC_CODIGO, LOCUC_USUARIO 
         FROM LOC_USUARIOS_CLIENTES
         WHERE LOCUC_CLIENTE = '".$cliente."' AND LOCUC_ESADMINISTRADOR = 1
         ORDER BY LOCUC_USUARIO ";

        $stmtb = $d->prepare($sqlb);
        $resultb = $stmtb->execute();
        if ($resultb && $stmtb->rowCount() != 0){
          //Tiene administradores
          $admin = 0;
          while ($fila = $stmtb->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
            if($fila[1]==$email){
              $admin = 1;
            }
          } 
          if($admin == 0){
            //No es admin
            return false;
          }else{
            //Es admin porlo que le dejamos ver todas las vistas
            return true;
          }
        }else{
          //Esa vista no tiene administradores 
          return true;
        }

      }  
      /*}else{
        return false;
      }*/
    }
    
  }

    //Funcion para validar la vista del usuario
  function permisosUser($email,$vista,$com_emails){

        $devuelve = array();
        $cliente = clienteVista($vista);
        $d = Nuevo_PDO();
        $sqlb = "SELECT LOCUC_USUARIO,LOCUC_CLIENTE,LOCUC_ESADMINISTRADOR, LOCUC_ACCESO_PERFIL, LOCUC_ACCESO_HISTORICO, LOCUC_ACCESO_M_INFORME, LOCUC_ACCESO_M_SEO, LOCUC_ACCESO_M_ECOMMERCE,
                 LOCUC_ACCESO_M_CAMPANAS, LOCUC_ACCESO_M_CUADROMANDO
                 FROM dbo.LOC_USUARIOS_CLIENTES
                 WHERE LOCUC_USUARIO = '".$email."' AND LOCUC_CLIENTE = '".$cliente."'";

        $stmtb = $d->prepare($sqlb);
        $resultb = $stmtb->execute();
        if ($resultb && $stmtb->rowCount() != 0){
          $count = 0;
          //echo "Filas: ".$stmt->rowCount()."<br>";
          while ($fila = $stmtb->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
            $count++;
            $id = $count;
            $devuelve["elementos"][$id]["email"]        = $fila[0];
            $devuelve["elementos"][$id]["cliente"]      = $fila[1];
            $devuelve["elementos"][$id]["admin"]        = $fila[2];
            $devuelve["elementos"][$id]["perfil"]       = $fila[3];
            $devuelve["elementos"][$id]["historico"]    = $fila[4];
            $devuelve["elementos"][$id]["informes"]     = $fila[5];
            $devuelve["elementos"][$id]["seo"]          = $fila[6];
            $devuelve["elementos"][$id]["ecommerce"]    = $fila[7];
            $devuelve["elementos"][$id]["campanas"]     = $fila[8];
            $devuelve["elementos"][$id]["cuadromando"]  = $fila[9];
            $id++;
          }
          $devuelve["cantidad"]=$count;
      }else{
          $devuelve["cantidad"]=0;
      } 

      return $devuelve;
    
  }

  //Devuelve true si el filtro antispam esta activo en una propiedad
  function antiSpam($propiedad){

    $sql = "SELECT TOP 1 LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM
            FROM LOC_DOMINIOS 
            WHERE LOCD_ANALYTICS_ID_PROPIEDAD = '".$propiedad."'";
    $d = Nuevo_PDO();
    $stmt = $d->prepare($sql);
    $resultb = $stmt->execute();
    $nfilas = false;
    if ($resultb && $stmt->rowCount() != 0){
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $nfilas = $fila[0];
      }
    }

    if($nfilas){
      return true;
    }else{
      return false;
    }

  }

  //Funcion para actualizar antispam
  function Guardar_Aptispam($vista,$antispam){

    $sql = "SELECT LOCDV_SUDOMINIO  
            FROM LOC_DOMINIOS_VISTAS
            WHERE LOCDV_ANALYTICS_ID_VISTA = '".$vista."' ";
    $d = Nuevo_PDO();
    $stmt = $d->prepare($sql);
    $resultb = $stmt->execute();
    if ($resultb && $stmt->rowCount() != 0){
      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $id_dominio = $fila[0];
      }

      $c = Nuevo_PDO();
      $sqlf = "UPDATE LOC_DOMINIOS
              SET LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM = ".$antispam."
              WHERE LOCD_CODIGO = '".$id_dominio."'";
      $stmtf = $c->prepare($sqlf);
      $resultf = $stmtf->execute();

    }

  }
  
?>

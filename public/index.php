<?php
	include("includes/conf.php");
	if(isset($_COOKIE["usuario"])) {
		if(isset($_COOKIE["userpage"])) {
			if($_COOKIE["userpage"] == 'campanas'){
				header("Location: ../public/campañas/campañas_resumen.php");
			}else{
				header("Location: ../public/informes/metricas_habituales.php");
			}
			
		}else{
			setcookie( "userpage", 'informes', time() + (86400 * 30), "/");
			header("Location: ../public/informes/metricas_habituales.php");
		}
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>DMIntegra | <?php $trans->__('Inicio Sesión'); ?></title>
	

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">
	<link rel="stylesheet" href="css/estilos.css">

	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<script src="js/translation.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<!--Estilos-->
	
	
</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!--Modal para recuperar la contraseña-->
<div class="modal fade" id="modal_enviamail" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php $trans->__('Cerrar'); ?></span></button>
        <h4 class="modal-title" id="exampleModalLabel"><?php $trans->__('Recuperar contraseña'); ?></h4>
      </div>
      <div class="modal-body">
        <form role="form">
          <div class="form-group">
            <label for="recipient-name" class="control-label"><?php $trans->__('Escriba un correo electrónico donde recibirá un email para el cambio de contraseña.'); ?></label>
            <input type="text" class="form-control" id="txt_email" placeholder="<?php $trans->__('Correo electrónico'); ?>">
          </div>
          <div class="alert alert-danger" role="alert" id="msgerror">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><?php $trans->__('Cerrar'); ?></span></button>
  			<strong><?php $trans->__('Error!'); ?></strong> <span class="cajamsg"></span>
		  </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php $trans->__('Cerrar'); ?></button>
        <button type="button" class="btn btn-primary" id="envmail"><?php $trans->__('Enviar email'); ?></button>
      </div>
    </div>
  </div>
</div>
<!--Modal para recuperar la contraseña-->

<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '';
</script>

<div class="login-container">	
	<div class="login-header login-caret">
		<?php include("includes/langs.php"); ?>
		<div class="login-content">
			
			<a href="index.html" class="logo">
				<img src="images/DMintegra.png" alt="" />
			</a>
			
			<!-- <p class="description">Dear user, log in to access the admin area!</p> -->
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span><?php $trans->__('cargando'); ?>...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
		
		<div class="login-content">
			
			<div class="form-login-error">
				<h3><?php $trans->__('Invalid login'); ?></h3>
				<p><?php $trans->__('Introduce tu usuario y contraseña correctamente.'); ?></p>
			</div>
			<div class="form-login-success">
				<h3><?php $trans->__('Email enviado'); ?></h3>
				<p><?php $trans->__('Se ha enviado una correo electrónico para recuperar su contraseña.'); ?></p>
			</div>
			
			<form method="post" role="form" id="form_login">
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="username" id="username" placeholder="<?php $trans->__('Email'); ?>" autocomplete="off" />

					</div>
					
				</div>
				
				<div class="form-group">
					<p class="msg_newpass"><i class="fa fa-warning"></i>Crea una nueva contraseña para este usuario:</p>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="password" id="password" placeholder="<?php $trans->__('Contraseña');?>" autocomplete="off" />
					</div>
				
				</div>

				<div class="checkbox">
				  <label>
				    <input type="checkbox" id="check_conect"> <?php $trans->__('Seguir conectado'); ?>
				  </label>
				</div>


				<input type="hidden" id="txt_oculto" value="0">
				<div class="form-group">
					<button id="btn_login" type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						<?php $trans->__('Entrar'); ?>
					</button>
				</div>

			
			</form>
			
			
			<div class="login-bottom-links">
				
				<a id="btn_recorpass" class="link"><?php $trans->__('¿Has olvidado la contraseña?'); ?></a>
				
				<br />
				
				<a href="#"><?php $trans->__('ToS'); ?></a>  - <a href="#"><?php $trans->__('Privacy Policy'); ?></a>
				
			</div>
			
		</div>
		
	</div>
	
</div>

	<!-- Bottom Scripts -->
	<script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="js/index_scripts.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>
﻿<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side.php"); 
?>

<div class="main-content">

<?php

include("../includes/breadcrumb.php")
?>
<br>

<div class="col-sm-12">


<div class="row">
	
	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray" id="total_impactos">

			<div class="icon"><i class="entypo-user"></i></div>

			<div id="resumen_dispositivo1" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_dispositivo_nombre1"></h3>
			<p><span id="resumen_pedidos_icono"></span>&nbsp;</p>

			<div id="resumen_dispositivo2" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_dispositivo_nombre2"></h3>
			<p><span id="resumen_pedidos_vendidos_icono"></span>&nbsp;</p>

		</div>
	</div>
	
	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray" id="total_rate">
			<div class="icon"><i class="entypo-monitor"></i></div>

			<div id="resumen_so1" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_so_nombre1"></h3>
			<p><span id="resumen_ingresosb_icono"></span>&nbsp;</p>

			<div id="resumen_so2" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_so_nombre2"></h3>
			<p><span id="resumen_ingresosn_icono"></span>&nbsp;</p>

		</div>
		
	</div>

	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray" id="total_rate">
			<div class="icon"><i class="entypo-monitor"></i></div>

			<div id="resumen_modelo1" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_modelo_nombre1"></h3>
			<p><span id="resumen_ingresosb_icono"></span>&nbsp;</p>

			<div id="resumen_modelo2" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3 id="resumen_modelo_nombre2"></h3>
			<p><span id="resumen_ingresosn_icono"></span>&nbsp;</p>

		</div>
		
	</div>
	
</div>

<div class="row">

	<br>
	<div class="grupo-perspectivas">
		
		<div class="btn-group" data-toggle="buttons">
			<p><?php $trans->__('¿Bajo qué perspectiva deseas ver los datos?'); ?></p>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Aplicación'); ?></p>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="versiones"><?php $trans->__('Versiones de la aplicación'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="nomaplicacion"><?php $trans->__('Nombres de la aplicacion'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="instalador"><?php $trans->__('Instalador'); ?></button>
			</div>

			<div class="caja-perspectiva">
				<p><?php $trans->__('Dispositivo'); ?></p>
				<button type="button" class="btn btn-default radiopers_per active_pri" name="perspectiva_per" value="tipodisp"><?php $trans->__('Tipo dispositivo'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="versionsis"><?php $trans->__('Versión sistema operativo'); ?></button>
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="marca"><?php $trans->__('Marca'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="modelo"><?php $trans->__('Modelo'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="metodo"><?php $trans->__('Método de entrada'); ?></button>	
				<button type="button" class="btn btn-default radiopers_per" name="perspectiva_per" value="resolucion"><?php $trans->__('Resolución pantalla'); ?></button>			
			</div>

		</div>

	</div><!--cierre grupo perspectivas-->

	<input type="hidden" id="perspectiva_camb" value="" />
	<input type="hidden" id="perspectiva" value="tipodisp" />
	<input type="hidden" id="perspectiva_seg" value="" />

	<div class="row">
		
		<table class="table table-bordered datatable" id="table_datatable_productos">
			<thead>
				<tr>
					<th width="20%" id="cab_perspectiva_camb" ><span class="cab_tit" id="cab_perspectiva"><?php $trans->__('Versiones de la aplicación'); ?></span></th>
					<th width="20%" id="cab_perspectiva_seg_camb"><span class="cab_tit" id="cab_perspectiva_seg"></span></th>
					<th width="10%"><?php $trans->__('Sesiones'); ?></th>
					<th width="10%"><?php $trans->__('Pantallas vistas'); ?></th>
					<th width="10%"><?php $trans->__('Duración media'); ?></th>
					<th width="10%"><?php $trans->__('Pant/ses'); ?></th>
				</tr>	
			</thead>
			<tfoot>
			<tr>
				<th><?php $trans->__('TOTALES'); ?></th>
				<th></th>
				<th><span id="total_ses"></span></th>
				<th><span id="total_pant"></span></th>
				<th><span id="total_durm"></span></th>
				<th><span id="total_pantses"></span></th>
			</tr>
			</tfoot>
			<tbody>

		</table>

	</div>

</div>



</div>

<script src="<?=RUTA_ABSOLUTA?>js/aplicacion_dispositivos_scripts.js"></script>	

<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_ecommerce_informes.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



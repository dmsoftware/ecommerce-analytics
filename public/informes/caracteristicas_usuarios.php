<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />

<style type="text/css">
	
	.posinfo{ float: right;}
	.panel-heading > .panel-title { width: 100%; }
	canvas{margin-top: 25px;}
	.periodos{display: block; overflow: auto; width: 400px; margin-bottom: 50px;}
	.periodo{ width: 50%; float: left; text-align: center;}
	.actual{ color: #37bcf8}
	.anterior{ color: #fa8011}
	@media (min-width: 765px) and (max-width: 1330px) {

	.grafcont { padding-left: 0px !important;}
	#table_datatable_sexo .sorting_asc{ background-image: none;font-size: 0em;}
	#table_datatable_edades .sorting_asc{ background-image: none;font-size: 0em;}
	}
</style>
<!--totales-->
<input type="hidden" value="2" id="dat_sestot">
<?php
if($primera_app==0){
	$pageviews = 'Págs/ses';

}else{
	$pageviews = 'Pant/ses';
}
?>
<div class="row">
	
	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray">
			<div class="icon"><i class="entypo-user"></i></div>
			<div id="tilestats_total_sexo" class="num" data-start="0" data-end="5000" data-postfix="%" data-duration="3000" data-delay="0">0</div>
			<h3 id="txt_sexo_nombre"></h3>
			<p><span id="total_sexo_ico"></span>&nbsp;</p>
			<p><span id="total_sesiones_icono"></span>&nbsp;</p>
		</div>
		<div id="canvas-holder">
			<canvas id="chart-area_sexo" width="50" height="50"/>
		</div>

	</div>
	
	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray">
			<div class="icon"><i class="entypo-monitor"></i></div>
			<div id="tilestats_total_edad" class="num" data-start="0" data-end="239" data-postfix="" data-duration="3000" data-delay="600">0</div>
			<h3 id="txt_edad_nombre"></h3>
			<p><span id="total_edad_ico"></span>&nbsp;</p>
			<p><span id="total_paginasvistassesion_icono"></span>&nbsp;</p>
		</div>
		<div id="canvas-holder">
			<canvas id="chart-area_edad" width="50" height="50"/>
		</div>
	</div>

	<div class="col-sm-4">
	
		<div class="tile-stats tile-white-gray">
			<div class="icon"><i class="entypo-user-add"></i></div>
			<div id="tilestats_total_interes" class="num" data-start="0" data-prefix="" data-end="29" data-postfix="" data-duration="1500" data-delay="1800">0</div>
			<h3 id="txt_interes_nombre"></h3>
			<p><span id="total_interes_ico"></span>&nbsp;</p>
			<p><span id="total_usuarios_icono"></span>&nbsp;</p>
		</div>
		
	</div>
	
</div>

<br />
	
</form>	
<script src="../js/Chart.min.js"></script>


<div class="row">
	<div class="col-sm-7">
	<!--******************DATATABLE DE SEXO***************************-->
		<div class="panel minimal minimal-gray" data-collapsed="0">

			<div class="panel-heading">
				<div class="panel-title"><span><?php $trans->__('SEXO'); ?></span><span class="posinfo"><?php echo vsprintf($trans->__('Datos extraidos del %1$s%% del tráfico total.', false), array('<b id="expl_sexo">0</b>')); ?></span></div>				
			</div>

			<table class="table table-bordered datatable" id="table_datatable_sexo">
				<thead>
					<tr>
						<th width="41%"><span class="cab_tit" id="Nombre_Dimension_sexo"></span></th>
						<th width="20%"><?php $trans->__('Sesiones'); ?></th>
						<th width="20%"><?php $trans->__('Tas. rebote'); ?></th>
						<th width="13%"><?php $trans->__('Dur/media'); ?></th>
						<th width="6%"><?php $trans->__($pageviews); ?></th>
					</tr>	
				</thead>		
				<tbody>
			</table>
			
			

		</div>	
		<!--**************************************************************-->
	</div>
	<div class="col-sm-5 grafcont">
		<!--Gráfico de sexo-->
		<canvas id="cvs_sexo" width="400" height="100">[No canvas support]</canvas>
		<p class="periodos"><span class="periodo anterior"><?php $trans->__('Periodo anterior'); ?></span><span class="periodo actual"><?php $trans->__('Periodo actual'); ?></span></p>
	</div>
</div>

<div class="row">
	<div class="col-sm-7">
		<!--******************DATATABLE DE EDADES***************************-->			
		<div class="panel minimal minimal-gray" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title"><span><?php $trans->__('EDADES'); ?></span><span class="posinfo"><?php echo vsprintf($trans->__('Datos extraidos del %1$s%% del tráfico total.', false), array('<b id="expl_edad">0</b>')); ?></span></div>
			</div>

			<table class="table table-bordered datatable" id="table_datatable_edades">
				<thead>
					<tr>
						<th width="41%"><span class="cab_tit" id="Nombre_Dimension_edades"></span></th>
						<th width="20%"><?php $trans->__('Sesiones'); ?></th>
						<th width="20%"><?php $trans->__('Tas. rebote'); ?></th>
						<th width="13%"><?php $trans->__('Dur/media'); ?></th>
						<th width="6%"><?php $trans->__($pageviews); ?></th>
					</tr>	
				</thead>
				
			<tbody>
			</table>
					
		</div>
		<!--**************************************************************-->
	</div>
	<div class="col-sm-5 grafcont">
		<!--Gráfico de edad-->
		<canvas id="cvs_edad" width="400" height="250">[No canvas support]</canvas>
		<p class="periodos"><span class="periodo anterior"><?php $trans->__('Periodo anterior'); ?></span><span class="periodo actual"><?php $trans->__('Periodo actual'); ?></span></p>
	</div>
</div>



<?php
function porcentaje_sesiones($sessions,$sesiones_totales){

	return  number_format((($sessions*100)/$sesiones_totales), 2, ',', '');

}


//echo porcentaje_sesiones(1239,2413);
?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".cab_tit").parent().css({
			"background-color": "transparent"
		})
	})
</script>

<div class="row">
	<div class="col-sm-7">
		<!--******************DATATABLE DE INTERESES***************************-->					
		<div class="panel minimal minimal-gray" data-collapsed="0"> 
			<div class="panel-heading">
				<div class="panel-title"><span><?php $trans->__('INTERESES'); ?></span><span class="posinfo"><?php echo $trans->__('Datos extraidos de un porcentaje del tráfico total.', false); ?></span></div>
			</div>

			<table class="table table-bordered datatable" id="table_datatable_intereses">
				<thead>
					<tr>
						<th width="41%"><span class="cab_tit" id="Nombre_Dimension_intereses"></span></th>
						<th width="20%"><?php $trans->__('Sesiones'); ?></th>
						<th width="20%"><?php $trans->__('Tas. rebote'); ?></th>
						<th width="13%"><?php $trans->__('Dur/media'); ?></th>
						<th width="6%"><?php $trans->__($pageviews); ?></th>
					</tr>	
				</thead>

			<tbody>
			</table>
					
		</div>
		<!--**************************************************************-->
	</div>
	<div class="col-sm-5 grafcont">
		<!--Gráfico de interes-->
		<canvas id="cvs_interes" width="400" height="250">[No canvas support]</canvas>
		<p class="periodos"><span class="periodo anterior"><?php $trans->__('Periodo anterior'); ?></span><span class="periodo actual"><?php $trans->__('Periodo actual'); ?></span></p>
	</div>
</div>

<?php

?>
</div>

<script type="text/javascript" src="<?=RUTA_ABSOLUTA?>js/RGraph/libraries/RGraph.common.core.js" ></script>
<script type="text/javascript" src="<?=RUTA_ABSOLUTA?>js/RGraph/libraries/RGraph.bipolar.js" ></script>
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">
<script src="<?=RUTA_ABSOLUTA?>js/caracteristicas_usuarios_scripts.js"></script>	

<? include("../includes/footer.php"); } ?>



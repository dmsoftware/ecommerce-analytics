<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />

<?php
if($primera_app==0){
	$pageviews = 'Páginas vistas';
	$pageviewsPerSession = 'Páginas vistas por sesion';
	$pantot = 'Páginas vistas por sesión';
	$numtotales = "3";
	$webnom = 'WEB';
}else{
	$pageviews = 'Pantallas vistas';
	$pageviewsPerSession = 'Pantallas vistas por sesion';
	$pantot = 'Pantallas vistas por sesión';
	$numtotales = "4";
	$webnom = 'APP';
}
?>

<div class="row">
	
	<div class="col-sm-6">
	
		<div class="tile-stats tile-white-gray" id="total_contenido_idioma">
			<div class="icon"><i class="entypo-user"></i></div>
			<div id="" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="3000" data-delay="0">0</div>
			<h3 id="txt_nombre_totales"></h3>
			<p><span id="total_sesiones_ico"></span>&nbsp;</p>
			<div id="canvas-holder">
				
			</div>
		</div>
	</div>
	
	<div class="col-sm-6">
	
		<div class="tile-stats tile-white-gray" id="total_contenido_info">
			<div class="fila_total">
				<span class="txt_label"><?php $trans->__($pageviews); ?></span>
				
				<div class="limittam"><div class=" cont_i" id="comparar_pagtotal"></div></div>
				<span class="txt_totinfo" id="txt_pagvistas">0</span>
			</div>
			<div class="fila_total">
				<span class="txt_label"><?php $trans->__('Descargas'); ?></span>
				
				<div class="limittam"><div class=" cont_i" id="comparar_des"></div></div>
				<span class="txt_totinfo" id="txt_des">0</span>	
			</div>
			<div class="fila_total">
				<span class="txt_label"><?php $trans->__('Búsquedas internas'); ?></span>
				
				<div class="limittam"><div class=" cont_i" id="comparar_bus"></div></div>
				<span class="txt_totinfo" id="txt_bus">0</span>
			</div>
			<div class="fila_total">
				<span class="txt_label"><?php $trans->__('Enlaces externos consultados'); ?></span>
				
				<div class="limittam"><div class=" cont_i" id="comparar_enl"></div></div>
				<span class="txt_totinfo" id="txt_enl">0</span>	
			</div>
		</div>
		
	</div>
	
</div>

<br />
<script src="../js/Chart.min.js"></script>
<div class="row">

	<div class="col-sm-12">

		<!--Componente tab-->
		<style type="text/css">

			.fila_total{ width: 100%; display: block;overflow: auto; padding-bottom: 6px;}
			.fila_total .txt_label{ float: left;  font-size: 17px;}
			.fila_total .txt_totinfo{ float: right; font-size: 17px;  text-align: right;}
			.fila_total .cont_i{float: right;}
			.fila_total .limittam{float: right; width: 80px; }
			#caja_arbol{ overflow:auto; padding-bottom: 30px;}
			.datatable_arbol{ margin-top: 40px;}
			.caja_eventos{ display: block; overflow: auto;}
			.caja_div{ width: 50%; float: left;}
			.datatable_eventos{ padding: 15px;}
			.datatable_sustituto_arbol{ width: 25%; padding: 15px; float: left; overflow-x: hidden;}
			#tab_cabeceras li{ cursor: pointer;}
			/**/.google-visualization-orgchart-nodesel { position: relative;}
			.caja_susarbol{ overflow: auto; display: block;}
		</style>
		<div id="tab_cabeceras">
			
		</div>

		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane active" id="home">
		  		
		    <!--En este apartado comprobamos si la propiedad es enlazada o desenlazada-->
		    <?php
			if(ContenidoEnlazado($primera_vista)){
		    ?>
			  <div id="caja_arbol">	
			  	<div id="wrapper">
					<div id="scroller">
			  			<div id="chart_div"></div>
			  		</div>
			  	</div>
			  </div>
			  <style type="text/css">
			  	#caja_arbol{ overflow:hidden;  display: block; width: 100%; position: relative; height: 500px;} 
			  	#wrapper {
				    position: absolute;
				    z-index: 1;
				    top: 0px;
				    bottom: 0px;
				    left: 0px;
				    width: 100%;
				    background: none;
				    overflow: hidden;
				    cursor: move;
				    border: 1px dotted #8A8A8A;
					box-shadow: 5px 5px 8px rgba(183, 183, 183, 0.68) inset;
				}

				#scroller {
					position: absolute;
					z-index: 1;
					-webkit-tap-highlight-color: rgba(0,0,0,0);
					-webkit-transform: translateZ(0);
					-moz-transform: translateZ(0);
					-ms-transform: translateZ(0);
					-o-transform: translateZ(0);
					transform: translateZ(0);
					-webkit-touch-callout: none;
					-webkit-user-select: none;
					-moz-user-select: none;
					-ms-user-select: none;
					user-select: none;
					-webkit-text-size-adjust: none;
					-moz-text-size-adjust: none;
					-ms-text-size-adjust: none;
					-o-text-size-adjust: none;
					text-size-adjust: none;
					background: none;
				}
				.DTTT_container {

				    margin-top: -1px !important;
				}
				.izquierda{ float: left;text-align: left;}
			  </style>
			  <script type="text/javascript">
			  	function inicializar_tables_sustitucion(){} 
			  	function recargar_tables_sustitucion(){}

			  	var myScroll;

				function loaded() {
					myScroll = new IScroll('#wrapper', { scrollX: true, freeScroll: true });
				}

				document.addEventListener('touchmove', function (e) { e.preventDefault(); alert("entra") }, false);

			  </script>
			  <script type="text/javascript" src="../js/scroll/build/iscroll.js"></script>
			<?php
			}else{
				?>
				<div class="col-sm-12 caja_susarbol">
				<?php
				//Si es falso pintamos los datatables
				$nombre_desenlazados = ContenidoEnlazado_nombres($primera_vista);


				$cont = 0;
				for ($i=4; $i <= 7; $i++) { 
					$pos = "d".$i;
					
					if($nombre_desenlazados[$pos] != ""){
						//echo $pos;
						?>
						<!--Datatable de sustitucion la arbol-->
						<div class="datatable_sustituto_arbol">
							<h2><?=$nombre_desenlazados[$pos]?></h2>
							<table class="table table-bordered datatable" id="table_datatable_<?=$pos?>">
								<thead>
									<tr>
										<th><?php $trans->__('Nombre'); ?></th>
										<th><?php $trans->__('Sesiones'); ?></th>
									</tr>
								</thead>
								<tbody>
							</table>
						</div>
						<!--Datatable de sustitucion la arbol-->
						<?php
						$cont++;
					}
				}
				if ($cont==0){
					echo'<p>No ha establecido nombres descriptivos en el panel de configuración para ver la información de esta tabla.</p>
						<script type="text/javascript">function inicializar_tables_sustitucion(){} function recargar_tables_sustitucion(){}</script>';
				}
				

				//Ahora lo volvemos a recorrer para crear la función que inicializa los datatables
				?>
				<script type="text/javascript">
				function inicializar_tables_sustitucion(){
				<?php
				$cont = 0;
				for ($i=4; $i <= 7; $i++) { 
					$pos = "d".$i;
					if($nombre_desenlazados[$pos] != ""){
						?>
						//Datatable del arbol de contenido -----------------------------------------------------------------------------------------
						t_datatable_contenido_<?=$pos?> =  $('#table_datatable_<?=$pos?>').dataTable({
							"language": {
								"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
							},
							"paging": true,
							"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
							"ordering": true,
							"order": [[1,'desc']],
							"searching": false,
							//Exportaciones 
							"dom": 'rtT <filp "fondo">',
							"tableTools": {
				            "aButtons": [
				                {
			                 "sExtends": "pdf",
			                 "sPdfOrientation": "landscape",
			                 //"sPdfMessage": "Your custom message would go here.",
			                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
			             	},
			             	/*{
			                 "sExtends": "csv",
			                 "sPdfOrientation": "landscape",
			                 //"sPdfMessage": "Your custom message would go here.",
			                 "sButtonText": "CSV"
			             	},*/
			             	{
			                 "sExtends": "xls",
			                 "sPdfOrientation": "landscape",
			                 //"sPdfMessage": "Your custom message would go here.",
			                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
			             	},

				            ],
				            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
				        },
							"columns":[
								null,
								null
								],
							"processing": true,			
							"serverSide": true,
							"ajax": {
								"type": "POST",
								"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strApp=" + $("#dat_app").val(), 
								"data": {"strFuncion": 'contenido_datatable_eventos_<?=$pos?>'}
							}		
						});		
						//Datatable del arbol de contenido -----------------------------------------------------------------------------------------

						<?php
						$cont++;
					}
				}

				?>
				}

				</script>

				
				</div>
				<?php
			
			

				//Ahora lo volvemos a recorrer para crear la función que inicializa los datatables
				?>
				<script type="text/javascript">
				function recargar_tables_sustitucion(){
				<?php
				$cont = 0;
				for ($i=4; $i <= 7; $i++) { 
					$pos = "d".$i;
					if($nombre_desenlazados[$pos] != ""){
						?>
						t_datatable_contenido_<?=$pos?>.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strApp=" + $("#dat_app").val()).load();

						<?php
						$cont++;
					}
				}

				?>
				}

				</script>

				
				</div>
				<?php
			}
			?>

		  </div>
		  


		</div>

		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<!--Componente tab-->

		<!--Datatable del arbol de contenido-->

		<div class="datatable_arbol">
			<h2 id="nombre_dimension_dt"><?php $trans->__($webnom); ?></h2>
			<table class="table table-bordered datatable" id="table_datatable_contenido_arbol">
				<thead>
					<tr>
						<th width="33%"><?php $trans->__($pageviews); ?></th>
						<th width="28%"><?php $trans->__('Título'); ?></th>
						<th width="15%"><?php $trans->__('Sesiones'); ?></th>
						<th width="12%"><?php $trans->__('Dur/media'); ?></th>	
						<th width="12%"><?php $trans->__('Tas. rebote'); ?></th>	
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th colspan="2"><?php $trans->__('TOTALES'); ?></th>
						<th><span id="total_visitas"></span></th>
						<th><span id="total_durmedia"></span></th>
						<th><span id="total_tasrebote"></span></th>
					</tr>
				</tfoot>
			<tbody>
			</table>
		</div>
		<!--Datatable del arbol de contenido-->

		<!--Datatable de eventos-->
		<div class="caja_eventos">
	
			<div class="caja_div">	
				<!--Datatable de descargas-->
				<div class="datatable_eventos">
					<h2><?php $trans->__('Descargas'); ?></h2>
					<table class="table table-bordered datatable" id="table_datatable_eventos_descargas">
						<thead>
							<tr>
								<th><?php $trans->__('Archivo'); ?></th>
								<th><?php $trans->__('Sesiones'); ?></th>
							</tr>
						</thead>
						<tbody>
					</table>
				</div>
				<!--Datatable de descargas-->
			</div>
			<div class="caja_div">
				<!--Datatable de búsquedas internas-->
				<div class="datatable_eventos">
					<h2><?php $trans->__('Búsquedas internas'); ?></h2>
					<table class="table table-bordered datatable" id="table_datatable_eventos_busquedas">
						<thead>
							<tr>
								<th><?php $trans->__('Término buscado'); ?></th>
								<th><?php $trans->__('Sesiones'); ?></th>
							</tr>
						</thead>
						<tbody>
					</table>
				</div>
				<!--Datatable de búsquedas internas-->				
			</div>
			<div class="caja_div">
				<!--Datatable de entradas extermas-->
				<div class="datatable_eventos">
					<h2><?php $trans->__('Enlaces externos consultados'); ?></h2>
					<table class="table table-bordered datatable" id="table_datatable_eventos_enlaces">
						<thead>
							<tr>
								<th><?php $trans->__('Enlace (URL)'); ?></th>
								<th><?php $trans->__('Sesiones'); ?></th>
							</tr>
						</thead>
						<tbody>
					</table>
				</div>
				<!--Datatable de entradas extermas-->						
			</div>

		</div>
		<!--Datatable de eventos-->

	</div>

</div>


<script src="<?=RUTA_ABSOLUTA?>js/contenido_scripts.js"></script>
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



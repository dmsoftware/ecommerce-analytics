<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<!--totales-->
<input type="hidden" value="2" id="dat_sestot">

<div class="row">
	
	<div class="col-sm-6">
	
		<div class="tile-stats tile-white-gray">
			<h2><?php $trans->__('Sesiones'); ?></h2>
			<div class="col-sm-6">
				<span id="piechart_actual_tit"><?php $trans->__('Datos del periodo actual'); ?>:</span>
				<div id="piechart_actual" style="width: 200px; height: 200px; margin:auto; margin-top:-20px; margin-bottom:-20px"></div>
			</div>
			<div class="col-sm-6">
				<span id="piechart_anterior_tit"><?php $trans->__('Datos del periodo anterior'); ?>:</span>
				<div id="piechart_anterior" style="width: 200px; height: 200px; margin:auto; margin-top:-20px; margin-bottom:-20px"></div>
			</div>
			<div class="col-sm-12" style="clear:both">
				<img class="legendafuentes" src="../images/legend_trafico_tartas.png">
			</div>
		</div>

	</div>

	<div class="col-sm-6">
		<div class="tile-stats tile-white-gray">
		<h2><?php $trans->__('Conversiones'); ?></h2>
			<div class="col-sm-6">
				<span id="piechart_actual_tit"><?php $trans->__('Datos del periodo actual'); ?>:</span>
				<div id="columchart_valores"></div>
				<img class="l_c_fuentes" src="../images/legend_columnchart_fuentes.png" />
			</div>
			<div class="col-sm-6">
				<span id="piechart_anterior_tit"><?php $trans->__('Datos del periodo anterior'); ?>:</span>
				<div id="columchart_valores_ant"></div>
				<img class="l_c_fuentes" src="../images/legend_columnchart_fuentes.png" />
			</div>

		</div>
		
	</div>
	<style type="text/css">

		.tile-white-gray h2{ margin-top: -10px !important;
							 margin-bottom: 7px !important;
							 color: rgba(128, 128, 128, 0.37);
							 font-weight: bold;
							 margin-left: -10px !important; }
		.l_c_fuentes{ position: absolute;
					bottom: 0;
					left: 65px;}

		.cajachecks{overflow: auto;
					padding-top: 15px;
					display: block;}
		.grafcap{ display: block; float: left; width: 40%;}
		.grafcapb{ display: block; float: left; width: 20%;}
		.legendafuentes{ display: block; margin: auto; margin-top: 50px;}
		.posinfo{ float: right;}
		.panel-heading > .panel-title { width: 100%; }
		.cajadiv_dispo_table{ width: 33.3333%; float:left;}
		.cajadiv_dispo_imgs{ width: 33.3333%; float:left;}
		#cajamokup{ display: block; margin: auto; overflow: auto; position: relative; width: 400px; height: 320px;}
		#fondopri{ position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow-y:hidden; z-index: 5;}
		#imgescritorio{ position: absolute; top: 26px;left: 33px;width: 335px; display: block;margin: auto;z-index: 2;height: 250px; }
		#imgmovil{ position:absolute; top: 172px;left: 65px;width: 64px;display: block;margin: auto;z-index: 3;height: 113px;}
		.mimi{ display: none;}
		.listado_piecharts{ overflow: auto; overflow-y: hidden }
		.caja_tarta{ width:33.333%; float:left; height:200px; }
		.totfuente{ width: 85%; display: block; margin: auto;}
		.totfuente .dat{ float: right;}
		.totfuente span{ font-size: 14px;}
		.totfuente span.sest{ font-size: 20px;}
		.cab_bb{ background-color: transparent !important; color: black !important;}
		
		/*tabla de comparaciones*/
		#tabla_conversiones tfoot{ border-top: 2px solid #D4D4D4;   }
		#tabla_conversiones tfoot tr td{   background-color:transparent !important; text-align: right; }/*F9F9F9*/
		#tabla_conversiones tfoot tr:nth-child(odd){background-color:#F2F2F2 !important; text-align: right;}
		#tabla_conversiones tfoot tr:nth-child(even){background-color:#F9F9F9 !important; text-align: right;}
		#tabla_conversiones td{ position: relative !important;}
		#tc_sesiones{ font-size: 22px;}
		#tc_conversiones{ font-size: 17px;}
		#tabla_conversiones .porcDT{font-size: 12px !important;}
		#tabla_conversiones .porcDTb{font-size: 10px !important; color: gray !important}
		.columconver{ font-size: 12px !important; color: black; padding-left: 25px !important; text-align: left !important;}
		.table-bordered > thead > tr > th, .table-bordered > thead > tr > td {font-weight: bold;}
		#tabla_conversiones{background-color: white;  color: white !important; }
		.sintit{ background-color: white !important; border-top: 1px solid white !important; border-left: 1px solid white !important; }
		.tit_enlaces{ background-color: #01d5fb !important}
		.tit_buscadores{ background-color: #fe6b5b !important}
		.tit_social{ background-color: #fbc63c !important}
		.tit_campanas{ background-color: #01d9b2 !important}
		.tit_directo{ background-color: #7d5fa9 !important}
		.guia_enlaces{background-color: #01d5fb !important; width: 5px; position: absolute;  left: -1px;top: -2px;height: 108%;z-index: 99;}
		.guia_campanas{background-color: #01d9b2 !important; width: 5px; position: absolute;  left: -1px;top: -2px;height: 108%;z-index: 99;}
		.guia_directo{background-color: #7d5fa9 !important; width: 5px; position: absolute;  left: -1px;top: -2px;height: 108%;z-index: 99;}
		.guia_buscadores{background-color: #fe6b5b !important; width: 5px; position: absolute;  left: -1px;top: -2px;height: 108%;z-index: 99;}
		.guia_social{background-color: #fbc63c !important; width: 5px; position: absolute;  left: -1px;top: -2px;height: 108%;z-index: 99;}
		.panel-heading{border:none !important; }
		.panel-heading .panel-title{ margin-bottom: -2px;}
		.panel-heading div{ background-repeat: no-repeat; background-position: left;
						  color: white !important; padding: 3px 0px 3px 6px !important; font-size: 12px;  }
		.cab_enlaces div{ background-image: url("../images/backpr_enlaces.png"); border-bottom: 5px solid #01d5fb; }
		.cab_buscadores div{ background-image: url("../images/backpr_buscadores.png"); border-bottom: 5px solid #fe6b5b; }
		.cab_social div{ background-image: url("../images/backpr_social.png"); border-bottom: 5px solid #fbc63c; }
		.cab_campanas div{ background-image: url("../images/backpr_campanas.png"); border-bottom: 5px solid #01d9b2; }
		.cab_directo div{ background-image: url("../images/backpr_directo.png"); border-bottom: 5px solid #7d5fa9; }
		.form-inline .checkbox{ padding: 5px;}
		.table_enlaces{border-right: 5px solid #01d5fb !important;}
		.table_buscadores{border-right: 5px solid #fe6b5b !important;}
		.table_social{border-right: 5px solid #fbc63c !important;}
		.table_campanas{border-right: 5px solid #01d9b2 !important;}
		.table_directo{border-right: 5px solid #7d5fa9 !important;}
		.tableajax thead th { background-color: #D7D7D7 !important; color: #898989  !important; }
		.izquierda{ display: block; text-align: left;}
	</style>	
	
</div>

<br />
	
<script src="../js/Chart.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<?php
if($primera_app==0){
	$pageviews = 'Págs/ses';

}else{
	$pageviews = 'Pant/ses';
}
?>
<!--******************TABLA COMAPRACIONES***************************-->
<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered datatable " id="tabla_conversiones">
			<thead>
			<tr>
				<th class="sintit"></th>
				<th class="tit_enlaces"><?php $trans->__('Enlaces'); ?></th>
				<th class="tit_buscadores"><?php $trans->__('Buscadores'); ?></th>
				<th class="tit_social"><?php $trans->__('Social'); ?></th>
				<th class="tit_campanas"><?php $trans->__('Campañas'); ?></th>
				<th class="tit_directo"><?php $trans->__('Directo'); ?></th>
			</tr>
			</thead>
			<tbody>
				<tr id="tc_sesiones">
					<td><?php $trans->__('Sesiones'); ?></td>
					<td id="sesiones_enlaces"></td>
					<td id="sesiones_buscadores"></td>
					<td id="sesiones_social"></td>
					<td id="sesiones_campanas"></td>
					<td id="sesiones_directo"></td>
				</tr>
				<tr>
					<td><?php $trans->__('Tasa de rebote'); ?></td>
					<td id="tasarebote_enlaces"></td>
					<td id="tasarebote_buscadores"></td>
					<td id="tasarebote_social"></td>
					<td id="tasarebote_campanas"></td>
					<td id="tasarebote_directo"></td>
				</tr>
				<tr>
					<td><?php $trans->__('Tiempo de sesión'); ?></td>
					<td id="tiemposesion_enlaces"></td>
					<td id="tiemposesion_buscadores"></td>
					<td id="tiemposesion_social"></td>
					<td id="tiemposesion_campanas"></td>
					<td id="tiemposesion_directo"></td>
				</tr>
				<tr>
					<td><?php $trans->__($pageviews); ?></td>
					<td id="pagsesion_enlaces"></td>
					<td id="pagsesion_buscadores"></td>
					<td id="pagsesion_social"></td>
					<td id="pagsesion_campanas"></td>
					<td id="pagsesion_directo"></td>
				</tr>
				<tr id="tc_conversiones">
					<td><?php $trans->__('Conversiones'); ?> </td>
					<td id="conversiones_enlaces"></td>
					<td id="conversiones_buscadores"></td>
					<td id="conversiones_social"></td>
					<td id="conversiones_campanas"></td>
					<td id="conversiones_directo"></td>
				</tr>
			</tbody>
			<tfoot>
				
			</tfoot>

		</table>
	</div>
</div>
<!--******************TABLA COMAPRACIONES***************************-->

<br><br>

<div class="row">

	<div class="col-sm-6">
		<!--******************DATATABLE DE ENLACES***************************-->
		<div class="panel minimal minimal-gray" data-collapsed="0">

			<div class="panel-heading cab_enlaces">
				<div class="panel-title"><span><?php $trans->__('Enlaces'); ?></span></div>				
			</div>

			<div class="table_enlaces">
				<table class="table table-bordered datatable tableajax" id="table_datatable_enlaces">
					<thead>
						<tr>
							<th><span class="cab_tit" id="Nombre_Dimension_enlaces"><?php $trans->__('Dominio'); ?></span></th>
							<th width="30%"><?php $trans->__('Sesiones'); ?></th>
						</tr>	
					</thead>
					<tbody>
				</table>
			</div>

		</div>
		<!--**************************************************************-->
	</div>

	<div class="col-sm-6">
		<!--******************DATATABLE DE BUSCADORES***************************-->
		<div class="panel minimal minimal-gray" data-collapsed="0">
			<div class="panel-heading cab_buscadores">
				<div class="panel-title"><span><?php $trans->__('Buscadores'); ?></span></div>				
			</div>
			
			<div class="table_buscadores">
				<table class="table table-bordered datatable tableajax" id="table_datatable_buscadores">
					<thead>
						<tr>
							<th><span class="cab_tit" id="Nombre_Dimension_buscadores"><?php $trans->__('Término de busqueda'); ?></span></th>
							<th width="30%"><?php $trans->__('Sesiones'); ?></th>
						</tr>	
					</thead>
					<tbody>
				</table>
			</div>
			
		</div>
		<!--**************************************************************-->
	</div>

	<br>

	<div class="col-sm-6">
		<!--******************DATATABLE DE Social***************************-->
		<div class="panel minimal minimal-gray" data-collapsed="0">

			<div class="panel-heading cab_social">
				<div class="panel-title"><span><?php $trans->__('Social'); ?></span></div>				
			</div>

			<div class="table_social">
				<table class="table table-bordered datatable tableajax" id="table_datatable_social">
					<thead>
						<tr>
							<th><span class="cab_tit" id="Nombre_Dimension_social"><?php $trans->__('Red social'); ?></span></th>
							<th width="30%"><?php $trans->__('Sesiones'); ?></th>
						</tr>	
					</thead>
					<tbody>
				</table>
			</div>

		</div>
		<!--**************************************************************-->
	</div>

	<div class="col-sm-6">
		<!--******************DATATABLE DE CAMPAÑAS***************************-->
		<div class="panel minimal minimal-gray" data-collapsed="0">

			<div class="panel-heading cab_campanas">
				<div class="panel-title"><span><?php $trans->__('Campañas'); ?></span></div>				
			</div>		

			<div class="table_campanas">
				<table class="table table-bordered datatable tableajax" id="table_datatable_campanas">
					<thead>
						<tr>
							<th><span class="cab_tit" id="Nombre_Dimension_campanas"><?php $trans->__('Campaña'); ?></span></th>
							<th class="cab_bb"><?php $trans->__('Medio'); ?></th>
							<th class="cab_bb"><?php $trans->__('Fuente'); ?></th>
							<th width="30%"><?php $trans->__('Sesiones'); ?></th>
						</tr>	
					</thead>
					<tbody>
				</table>
			</div>
				
		</div>
		<!--**************************************************************-->
	</div>

</div>

<?php


?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".cab_tit").parent().css({
			"background-color": "transparent"
		})
	})
</script>

</div>

<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">
<script src="<?=RUTA_ABSOLUTA?>js/fuentes-entrada_scripts.js"></script>	

<? include("../includes/footer.php"); } ?>



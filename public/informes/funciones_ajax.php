<?php
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\class_admin_api.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\class_datos_api.php");
require_once ($_SERVER['DOCUMENT_ROOT']."\googleapi\class_mcf_api.php");

function resumen_porcentaje_total($strTipo,$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro){

	switch ($strTipo) {
		case 'sexo':
			$strDimensionsN='ga:userGender';
			break;
		case 'edad':
			$strDimensionsN='ga:userAgeBracket';
			break;
	};

	$filtro="";
	if ($strFiltro!="no"){
		$filtro .= $strFiltro;
	}

	//TRAEMOS EL TOTAL DE SESSIONES REALIZADAS
	$strDimensions='ga:year';
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_totalses = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_totalses -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_totalses -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_totalses -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api_totalses -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api_totalses -> metrics              = 'ga:sessions' ;
    if (empty($filtro)) {
		$obj_Datos_Api_totalses -> optParams        = array(
															'dimensions' => $strDimensions
															);
	}else{
		$obj_Datos_Api_totalses -> optParams        = array(
															'dimensions' => $strDimensions,
															'filters' => $filtro
															);
	}
    

    $obj_Datos_Api_totalses -> Construccion();

    $total_sesiones = $obj_Datos_Api_totalses->Total("sessions");


 	//TRAEMOS EL TOTAL DE SESSIONES REALIZADAS CON GÉNERO
	
 	//Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:sessions' ;
    if (empty($filtro)) {
		$obj_Datos_Api -> optParams        = array(
													'dimensions' => $strDimensionsN
													);
	}else{
		$obj_Datos_Api -> optParams        = array(
													'dimensions' => $strDimensionsN,
													'filters' => $filtro
													);
	}

    $obj_Datos_Api -> Construccion();

    $total_genero = $obj_Datos_Api ->Total("sessions");

    return round(porcentaje_sesiones_res($total_genero,$total_sesiones));
	


}




function resumen_linechart_ses($strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro)
{
	//Linechart de sesiones
	$dimensiones = "ga:day,ga:month,ga:year";	

	$filtro="";
	if ($strFiltro!="no"){
		$filtro .= $strFiltro;
	}

	$DataTables_start  = 1;
	$DataTables_length = 10000;

	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = 'ga:sessions';
	if (empty($filtro)) {
		$obj_Datos_Api -> optParams         = array(
											'dimensions' => $dimensiones,
	                                       	'sort' => 'ga:year,ga:month,ga:day',
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
	}else{
   		$obj_Datos_Api -> optParams         = array(
											'dimensions' => $dimensiones,
											'filters' => $filtro,
	                                       	'sort' => 'ga:year,ga:month,ga:day',
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
   		}
   	$obj_Datos_Api -> Construccion();


   	$obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_ant -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt;
	$obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt;
	$obj_Datos_Api_ant -> metrics              = 'ga:sessions';
	if (empty($filtro)) {
		$obj_Datos_Api_ant -> optParams  = array(
											'dimensions' => $dimensiones,
	                                       	'sort' => 'ga:year,ga:month,ga:day',
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
	}else{
   		$obj_Datos_Api_ant -> optParams  = array(
											'dimensions' => $dimensiones,
											'filters' => $filtro,
	                                       	'sort' => 'ga:year,ga:month,ga:day',
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
   		}
   	
	
	$obj_Datos_Api_ant -> Construccion();
	$strAuxb = "";

	//ANterior
	$arrayant = array();


	for ($x=1; $x<=$obj_Datos_Api_ant->NumValores(); $x++) {

		$strAuxb .= "['".intval($obj_Datos_Api_ant->Valor("day",$x))."',".$obj_Datos_Api_ant->Valor("sessions",$x).",0],";

	}

	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {


		$strAuxb .= "['".intval($obj_Datos_Api->Valor("day",$x))."',0,".$obj_Datos_Api->Valor("sessions",$x)."],";

	}

	$strAuxb = substr($strAuxb, 0, -1);
	return $strAuxb;

}


function resumen_datatable_fuentes ($strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro)
{
	global $trans;
	$dimensiones = "ga:campaign,ga:medium,ga:socialNetwork,ga:source,ga:keyword";	

	$filtro="";
	if ($strFiltro!="no"){
		$filtro .= ''.$strFiltro;
	}

	$DataTables_start  = 1;
	$DataTables_length = 10000;

	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = 'ga:sessions';
	if (empty($filtro)) {
		$obj_Datos_Api -> optParams         = array(
											'dimensions' => $dimensiones,
	                                       	//'sort' => '-ga:sessions',
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
	}else{
   		$obj_Datos_Api -> optParams         = array(
											'dimensions' => $dimensiones,
											'filters' => $filtro,
	                                       	//'sort' => '-ga:sessions',
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
   		}
   	
	
	$obj_Datos_Api -> Construccion();

	//PERIDO ANTERIOR
	$obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_ant -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt;
	$obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt;
	$obj_Datos_Api_ant -> metrics              = 'ga:sessions';
	if (empty($filtro)) {
		$obj_Datos_Api_ant -> optParams        = array(
											'dimensions' => $dimensiones,
	                                       	//'sort' => '-ga:sessions',
											'start-index' => $DataTables_start,
	                                       	'max-results' => $DataTables_length );  
	}else{
   		$obj_Datos_Api_ant -> optParams        = array(
											'dimensions' => $dimensiones,
											'filters' => $filtro,
	                                       	//'sort' => '-ga:sessions',
											'start-index' => $DataTables_start,
	                                       	'max-results' => $DataTables_length );  
   		}
   	
	
	$obj_Datos_Api_ant -> Construccion();


	//2015
    $anno = date('Y');
    $fecha_ini_anno = $anno.'-01-01';
	$fecha_fin_anno = $anno.'-12-31';

	//Del Último año
	$obj_Datos_Api_anno = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_anno -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_anno -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_anno -> startdate            = $fecha_ini_anno;
	$obj_Datos_Api_anno -> enddate              = $fecha_fin_anno;
	$obj_Datos_Api_anno -> metrics              = 'ga:sessions';
	if (empty($filtro)) {
		$obj_Datos_Api_anno -> optParams        = array(
											'dimensions' => $dimensiones,
	                                       	//'sort' => '-ga:sessions',
											'start-index' => $DataTables_start,
	                                       	'max-results' => $DataTables_length );  
	}else{
   		$obj_Datos_Api_anno -> optParams        = array(
											'dimensions' => $dimensiones,
											'filters' => $filtro,
	                                       	//'sort' => '-ga:sessions',
											'start-index' => $DataTables_start,
	                                       	'max-results' => $DataTables_length );  
   		}
   	
	
	$obj_Datos_Api_anno -> Construccion();


	

	$enlaces_sesiones_total = 0;
	$buscadores_sesiones_total = 0;
	$social_sesiones_total = 0;
	$campanas_sesiones_total = 0;
	$directo_sesiones_total = 0;
	$enlaces_sesiones_total_ant = 0;
	$buscadores_sesiones_total_ant = 0;
	$social_sesiones_total_ant = 0;
	$campanas_sesiones_total_ant = 0;
	$directo_sesiones_total_ant = 0;
	$enlaces_sesiones_total_anno = 0;
	$buscadores_sesiones_total_anno = 0;
	$social_sesiones_total_anno = 0;
	$campanas_sesiones_total_anno = 0;
	$directo_sesiones_total_anno = 0;

	//AÑO ENTERO
	for ($x=1; $x<=$obj_Datos_Api_anno->NumValores(); $x++) {

		//Aqui sumamos todas las sesiones para sacar el porcentaje
		$sesiones_total_anno += $obj_Datos_Api_anno->Valor("sessions",$x);
		//****

		//Para enlaces (ga:medium==referral;ga:SocialNetwork==(not set))
		if ($obj_Datos_Api_anno->Valor("medium",$x) == "referral" && $obj_Datos_Api_anno->Valor("SocialNetwork",$x) == "(not set)") {

			$enlaces_sesiones_total_anno += $obj_Datos_Api_anno->Valor("sessions",$x);
			
		}
		//Para busqueda ga:medium==organic;ga:SocialNetwork==(not set)
		if ($obj_Datos_Api_anno->Valor("medium",$x) == "organic" && $obj_Datos_Api_anno->Valor("SocialNetwork",$x) == "(not set)") {
			
			$buscadores_sesiones_total_anno += $obj_Datos_Api_anno->Valor("sessions",$x);
		
		}
		//Para social ga:SocialNetwork==(not set)
		if ($obj_Datos_Api_anno->Valor("SocialNetwork",$x) != "(not set)" && $obj_Datos_Api_anno->Valor("campaign",$x) == "(not set)" ) {
			
			$social_sesiones_total_anno += $obj_Datos_Api_anno->Valor("sessions",$x);
			
		}
		//Para campañas ga:medium!=referral;ga:medium!=organic;ga:medium!=(none); ga:SocialNetwork==(not set)
		if ($obj_Datos_Api_anno->Valor("medium",$x) != "referral" && $obj_Datos_Api_anno->Valor("medium",$x) != "organic" && $obj_Datos_Api_anno->Valor("medium",$x) != "(none)") {
			
			$campanas_sesiones_total_anno += $obj_Datos_Api_anno->Valor("sessions",$x);
			
		}
		//Para directo ga:medium==(none)
		if ($obj_Datos_Api_anno->Valor("medium",$x) == "(none)") {
			
			$directo_sesiones_total_anno += $obj_Datos_Api_anno->Valor("sessions",$x);
			
		}

	}

	//PERIODO ANTERIOR
	for ($x=1; $x<=$obj_Datos_Api_ant->NumValores(); $x++) {

		//Aqui sumamos todas las sesiones para sacar el porcentaje
		//$sesiones_total += $obj_Datos_Api->Valor("sessions",$x);
		//****

		//Para enlaces (ga:medium==referral;ga:SocialNetwork==(not set))
		if ($obj_Datos_Api_ant->Valor("medium",$x) == "referral" && $obj_Datos_Api_ant->Valor("SocialNetwork",$x) == "(not set)") {

			$enlaces_sesiones_total_ant += $obj_Datos_Api_ant->Valor("sessions",$x);
			
		}
		//Para busqueda ga:medium==organic;ga:SocialNetwork==(not set)
		if ($obj_Datos_Api_ant->Valor("medium",$x) == "organic" && $obj_Datos_Api_ant->Valor("SocialNetwork",$x) == "(not set)") {
			
			$buscadores_sesiones_total_ant += $obj_Datos_Api_ant->Valor("sessions",$x);
		
		}
		//Para social ga:SocialNetwork==(not set)
		if ($obj_Datos_Api_ant->Valor("SocialNetwork",$x) != "(not set)" && $obj_Datos_Api_ant->Valor("campaign",$x) == "(not set)") {
			
			$social_sesiones_total_ant += $obj_Datos_Api_ant->Valor("sessions",$x);
			
		}
		//Para campañas ga:medium!=referral;ga:medium!=organic;ga:medium!=(none); ga:SocialNetwork==(not set)
		if ($obj_Datos_Api_ant->Valor("medium",$x) != "referral" && $obj_Datos_Api_ant->Valor("medium",$x) != "organic" && $obj_Datos_Api_ant->Valor("medium",$x) != "(none)" ) {
			
			$campanas_sesiones_total_ant += $obj_Datos_Api_ant->Valor("sessions",$x);
			
		}
		//Para directo ga:medium==(none)
		if ($obj_Datos_Api_ant->Valor("medium",$x) == "(none)") {
			
			$directo_sesiones_total_ant += $obj_Datos_Api_ant->Valor("sessions",$x);
			
		}

	}

	//PERIODO ACTUAL
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		//Aqui sumamos todas las sesiones para sacar el porcentaje
		$sesiones_total += $obj_Datos_Api->Valor("sessions",$x);
		//****

		//Para enlaces (ga:medium==referral;ga:SocialNetwork==(not set))
		if ($obj_Datos_Api->Valor("medium",$x) == "referral" && $obj_Datos_Api->Valor("SocialNetwork",$x) == "(not set)") {

			$enlaces_sesiones_total += $obj_Datos_Api->Valor("sessions",$x);
			
		}
		//Para busqueda ga:medium==organic;ga:SocialNetwork==(not set)
		if ($obj_Datos_Api->Valor("medium",$x) == "organic" && $obj_Datos_Api->Valor("SocialNetwork",$x) == "(not set)") {
			
			$buscadores_sesiones_total += $obj_Datos_Api->Valor("sessions",$x);
		
		}
		//Para social ga:SocialNetwork==(not set)
		if ($obj_Datos_Api->Valor("SocialNetwork",$x) != "(not set)" && $obj_Datos_Api->Valor("campaign",$x) == "(not set)") {
			
			$social_sesiones_total += $obj_Datos_Api->Valor("sessions",$x);
			
		}
		//Para campañas ga:medium!=referral;ga:medium!=organic;ga:medium!=(none); ga:SocialNetwork==(not set)
		if ($obj_Datos_Api->Valor("medium",$x) != "referral" && $obj_Datos_Api->Valor("medium",$x) != "organic" && $obj_Datos_Api->Valor("medium",$x) != "(none)") {
			
			$campanas_sesiones_total += $obj_Datos_Api->Valor("sessions",$x);
			
		}
		//Para directo ga:medium==(none)
		if ($obj_Datos_Api->Valor("medium",$x) == "(none)") {
			
			$directo_sesiones_total += $obj_Datos_Api->Valor("sessions",$x);
			
		}


	}

	//$sesiones_total += $obj_Datos_Api->Total("sessions");
	$strAuxb = "";

	//ENLACES
	if($enlaces_sesiones_total_anno != 0){
		$strAuxb .= "<tr>";
		$porcres = porcentaje_sesiones($enlaces_sesiones_total,$sesiones_total);
		$porcres_anno = porcentaje_sesiones($enlaces_sesiones_total_anno,$sesiones_total_anno);
		$strAuxb .= '<td><span class="negro">' . $trans->__('Enlaces', false) . '</span></td><td>'.DevolverContenido_Total($enlaces_sesiones_total,$enlaces_sesiones_total_ant,$enlaces_sesiones_total,$enlaces_sesiones_total_ant).number_format($enlaces_sesiones_total,0,",",".").'<span class="porcentagest"> ('.round( $porcres ).'%)'.'</td><td>'.number_format($enlaces_sesiones_total_anno,0,",",".").'<span class="porcentagest"> ('.round( $porcres_anno ).'%)'.'</td>';
		$strAuxb .= "</tr>";
	}
	//BUSQUEDA
	if($buscadores_sesiones_total_anno != 0){
		$strAuxb .= "<tr>";
		$porcres = porcentaje_sesiones($buscadores_sesiones_total,$sesiones_total);
		$porcres_anno = porcentaje_sesiones($buscadores_sesiones_total_anno,$sesiones_total_anno);
		$strAuxb .= '<td><span class="negro">' . $trans->__('Buscadores', false) . '</span></td><td>'.DevolverContenido_Total($buscadores_sesiones_total,$buscadores_sesiones_total_ant,$buscadores_sesiones_total,$buscadores_sesiones_total_ant).number_format($buscadores_sesiones_total,0,",",".").'<span class="porcentagest"> ('.round( $porcres ).'%)'.'</span></td><td>'.number_format($buscadores_sesiones_total_anno,0,",",".").'<span class="porcentagest"> ('.round( $porcres_anno ).'%)'.'</td>';
		$strAuxb .= "</tr>";
	}
	//SOCIAL
	if($social_sesiones_total_anno != 0){
		$strAuxb .= "<tr>";
		$porcres = porcentaje_sesiones($social_sesiones_total,$sesiones_total);
		$porcres_anno = porcentaje_sesiones($social_sesiones_total_anno,$sesiones_total_anno);
		$strAuxb .= '<td><span class="negro">' . $trans->__('Social', false) . '</span></td><td>'.DevolverContenido_Total($social_sesiones_total,$social_sesiones_total_ant,$social_sesiones_total,$social_sesiones_total_ant).number_format($social_sesiones_total,0,",",".").'<span class="porcentagest"> ('.round( $porcres ).'%)'.'</span></td><td>'.number_format($social_sesiones_total_anno,0,",",".").'<span class="porcentagest"> ('.round( $porcres_anno ).'%)'.'</td>';
		$strAuxb .= "</tr>";
	}
	//CAMPAÑAS
	if($campanas_sesiones_total_anno != 0){
		$strAuxb .= "<tr>";
		$porcres = porcentaje_sesiones($campanas_sesiones_total,$sesiones_total);
		$porcres_anno = porcentaje_sesiones($campanas_sesiones_total_anno,$sesiones_total_anno);
		$strAuxb .= '<td><span class="negro">' . $trans->__('Campañas', false) . '</span></td><td>'.DevolverContenido_Total($campanas_sesiones_total,$campanas_sesiones_total_ant,$campanas_sesiones_total,$campanas_sesiones_total_ant).number_format($campanas_sesiones_total,0,",",".").'<span class="porcentagest"> ('.round( $porcres ).'%)'.'</span></td><td>'.number_format($campanas_sesiones_total_anno,0,",",".").'<span class="porcentagest"> ('.round( $porcres_anno ).'%)'.'</td>';
		$strAuxb .= "</tr>";
	}
	//DIRECTO
	if($directo_sesiones_total_anno != 0){
		$strAuxb .= "<tr>";
		$porcres = porcentaje_sesiones($directo_sesiones_total,$sesiones_total);
		$porcres_anno = porcentaje_sesiones($directo_sesiones_total_anno,$sesiones_total_anno);
		$strAuxb .= '<td><span class="negro">' . $trans->__('Directo', false) . '</span></td><td>'.DevolverContenido_Total($directo_sesiones_total,$directo_sesiones_total_ant,$directo_sesiones_total,$directo_sesiones_total_ant).number_format($directo_sesiones_total,0,",",".").'<span class="porcentagest"> ('.round( $porcres ).'%)'.'</span></td><td>'.number_format($directo_sesiones_total_anno,0,",",".").'<span class="porcentagest"> ('.round( $porcres_anno ).'%)'.'</td>';
		$strAuxb .= "</tr>";
	}



	return $strAuxb;

}

function resumen_indexacion_com ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{

	$dimensiones = "ga:pagePath,ga:pageTitle";

	$filtro="";
	if ($strFiltro!="no"){
		$filtro .= $strFiltro;
	}

	$DataTables_start  = 1;
	$DataTables_length = 1;

	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = 'ga:pageviews';

	if(empty($filtro)){

   		$obj_Datos_Api -> optParams         = array(
											'dimensions' => $dimensiones,
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
	}else{

		$obj_Datos_Api -> optParams         = array(
											'dimensions' => $dimensiones,
											'filters' => $filtro,
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  

	}

	$obj_Datos_Api -> Construccion();
	
	return $obj_Datos_Api->NumValoresReal();
}

function resumen_datatable_indexacion ($strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro, $dim )
{
	if($dim=="todos"){
		$dimensiones = "ga:dimension1,ga:dimension4";
		$dims = "dimension4";
	}else{
		$dimensiones = "ga:dimension1,ga:dimension".$dim;
		$dims = "dimension".$dim;
	}
	

	$filtro="ga:dimension1!=(not set);ga:".$dims."!=(not set)";
	if ($strFiltro!="no"){
		$filtro .= ';'.$strFiltro;
	}

	$DataTables_start  = 1;
	$DataTables_length = 7;

	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = 'ga:pageviews';
   	$obj_Datos_Api -> optParams         = array(
											'dimensions' => $dimensiones,
											'filters' => $filtro,
	                                       	'sort' => '-ga:pageviews',
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
   	
	
	$obj_Datos_Api -> Construccion();

	//2015
    $anno = date('Y');
    $fecha_ini_anno = $anno.'-01-01';
	$fecha_fin_anno = $anno.'-12-31';

	$DataTables_start  = 1;
	$DataTables_length = 1000;

	$obj_Datos_Api_anno = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_anno -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_anno -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_anno -> startdate            = $fecha_ini_anno;
	$obj_Datos_Api_anno -> enddate              = $fecha_fin_anno;
	$obj_Datos_Api_anno -> metrics              = 'ga:pageviews';
   	$obj_Datos_Api_anno -> optParams         = array(
											'dimensions' => $dimensiones,
											'filters' => $filtro,
	                                       	'sort' => '-ga:pageviews',
											'start-index' => $DataTables_start ,
	                                       	'max-results' => $DataTables_length );  
   	
	
	$obj_Datos_Api_anno -> Construccion();

	$contanno = 0;
	$array_anno = array();
	for ($x=1; $x<=$obj_Datos_Api_anno->NumValores(); $x++) {

		$array_anno[$contanno]["idioma"] = $obj_Datos_Api_anno->Valor("dimension1",$x);
		$array_anno[$contanno]["identidicador"] = $obj_Datos_Api_anno->Valor($dims,$x);
		$array_anno[$contanno]["value"] = $obj_Datos_Api_anno->ValorF('pageviews',$x);
		$porcres = porcentaje_sesiones($obj_Datos_Api_anno->Valor('pageviews',$x),$obj_Datos_Api_anno->Total('pageviews'));
		$array_anno[$contanno]["porcentaje"] = $porcres;
		$contanno++;
	}

	

	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		$dat = 0;
		foreach ($array_anno as $key => $anno) {
			
			if ($anno["identidicador"] == $obj_Datos_Api->Valor($dims,$x) && $anno["idioma"] == $obj_Datos_Api->Valor("dimension1",$x) ) {
				$dat = $anno["value"];
				$datporc = round($anno["porcentaje"]);
			}
		}

		$strAuxb .= "<tr>";
		$porcres = porcentaje_sesiones($obj_Datos_Api->Valor('pageviews',$x),$obj_Datos_Api->Total('pageviews'));
		$strAuxb .= '<td><span class="negro">'.contenido_chart_formatearnombre($obj_Datos_Api->Valor($dims,$x)).'</span></td>'.'<td><p class="letradt">'.traducir_idioma($obj_Datos_Api->Valor("dimension1",$x)).'</p></td>'.'<td><span>'.$obj_Datos_Api->ValorF('pageviews',$x).'</span><span class="porcentagest"> ('.round($porcres).'%)</span></td><td>'.$dat.'<span class="porcentagest"> ('.$datporc.'%)</span></td>';
		$strAuxb .= "</tr>";

	}

	

	return $strAuxb;

}

function resumen_datatable_conversiones_paisregion ($strTipo, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{
  global $trans;
	switch ($strTipo) {
		case 'pais':
			$strDimensions='ga:country,ga:countryIsoCode';
			$strDimensionsN ='country'; 
			$metricas  = 'ga:goalCompletionsAll';
			break;
		case 'region':
			$strDimensions='ga:region';
			$strDimensionsN ='region'; 
			$metricas  = 'ga:goalCompletionsAll';
			break;
	};

	$filtro="ga:goalCompletionsAll>0";	

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
	$DataTables_start  = 0;
	$DataTables_length = 1000;

	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
	                                            'sort' => '-ga:goalCompletionsAll',
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
	                                            'sort' => '-ga:goalCompletionsAll',
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
    $obj_Datos_Api -> Construccion();
	
	$blnComparacion = true;
	//Si hay que comparar creamos el objeto con la fecha anterior
    if ($blnComparacion=="true") {
    	//Creación objeto, parametrización -------------------------------------------------------
	    $obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado;
	    $obj_Datos_Api_ant -> idVista              ='ga:'. $idVistaAnalytics;
	    $obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt;
	    $obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt;
	    $obj_Datos_Api_ant -> metrics              = $metricas;
	    if(empty($filtro)){
	    	$obj_Datos_Api_ant -> optParams        = array(
													'dimensions' => $strDimensions,
		                                            'sort' => '-ga:goalCompletionsAll',
													'start-index' => $DataTables_start + 1,
		                                            'max-results' => $DataTables_length);  
	   	}else{
	   		$obj_Datos_Api_ant -> optParams        = array(
													'dimensions' => $strDimensions,
													'filters' => $filtro,
		                                            'sort' => '-ga:goalCompletionsAll',
													'start-index' => $DataTables_start + 1,
		                                            'max-results' => $DataTables_length);  
	   	}
	   	$obj_Datos_Api_ant -> Construccion();
    }

    //2015
    $anno = date('Y');
    $fecha_ini_anno = $anno.'-01-01';
	$fecha_fin_anno = $anno.'-12-31';

	$obj_Datos_Api_anno = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_anno -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_anno -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_anno -> startdate            = $fecha_ini_anno;
    $obj_Datos_Api_anno -> enddate              = $fecha_fin_anno;
    $obj_Datos_Api_anno -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
	                                            'sort' => '-ga:goalCompletionsAll',
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 1000);  
   	}else{
   		$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
	                                            'sort' => '-ga:goalCompletionsAll',
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 1000);  
   	}
    $obj_Datos_Api_anno -> Construccion();

    $array_anno = array();
    for ($x=1; $x<=$obj_Datos_Api_anno->NumValores(); $x++) {
    	$array_anno[$x]["nombre"] = $obj_Datos_Api_anno->Valor($strDimensionsN,$x);
    	$array_anno[$x]["value"]  = $obj_Datos_Api_anno->ValorF('goalCompletionsAll',$x);
    }


	$top = 5;
	$count = 0;
	$otros = 0;

	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		if($count < $top){
			$strA .= '<tr>';
			//El primer valor va a depender de la perspectiva que estamos pintando
			if($strTipo=="pais"){
				$strA .= "<td><span class='negro'>".Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x))."</span></td>";
			}else{
				$strA .= "<td><span class='negro'>".Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api)."</span></td>";
			}

			$totanno = 0;
			foreach ($array_anno as $key => $annof) {
				if( $annof["nombre"] == $obj_Datos_Api->Valor($strDimensionsN,$x) ){
					$totanno = $annof["value"];
					$array_anno[$key]["value"] = 0;
				}
			}

				
			//Comparamos si tiene comparación
			$strA .= '<td>'.DevolverContenido_Total($obj_Datos_Api->Valor("goalCompletionsAll",$x),$obj_Datos_Api_ant->Valor("goalCompletionsAll",$x),$obj_Datos_Api->ValorF("goalCompletionsAll",$x),$obj_Datos_Api_ant->ValorF("goalCompletionsAll",$x)).$obj_Datos_Api->ValorF("goalCompletionsAll",$x).'</td><td>'.$totanno.'</td>';

			$strA .= '</tr>';	  
			$count++;
		}else{

			//Sumamos otros
			$otros += $obj_Datos_Api->Valor("goalCompletionsAll",$x);
			$otros_ant += $obj_Datos_Api_ant->Valor("goalCompletionsAll",$x);
			$count++;

		}
	}

	if($count >= $top){
		foreach ($array_anno as $key => $annof) {
			$totanno_otros += $annof["value"];
		}

		$strA .= "<td><span class='negro'>" . $trans->__('Otros', false) . "</span></td><td>".DevolverContenido_Total($otros,$otros_ant,$otros,$otros_ant).number_format($otros,0,',','.').'</td><td>'.number_format($totanno_otros,0,',','.').'</td>';
		
	}

	
	//Devolvemos el json
	return $strA;
}

function resumen_dispositivos($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro){

	$obj_Datos_Api_left = new Datos_Api_Informes(); 
    $obj_Datos_Api_left -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_left -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_left -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_left -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_left -> metrics              = 'ga:sessions';
    $obj_Datos_Api_left -> optParams            = array(
												'dimensions' => 'ga:deviceCategory',
												'sort' => '-ga:sessions',
												'filters' => $filtro,
												'start-index' => 1,
	                                            'max-results' => 1000);  

    $obj_Datos_Api_left -> Construccion();

    $devuelve = array();
    for ($x=1; $x<=$obj_Datos_Api_left->NumValores(); $x++) {
    	$devuelve[$x]["nombre"] = $obj_Datos_Api_left->Valor("deviceCategory",$x);
    	$devuelve[$x]["total"]  = $obj_Datos_Api_left->Valor("sessions",$x);
    	if($obj_Datos_Api_left->Valor("sessions",$x) == 0){
    		$devuelve[$x]["rate"] = "0%";	
    	}else{
    		$devuelve[$x]["rate"]   = round( porcentaje_sesiones_res($obj_Datos_Api_left->Valor("sessions",$x),$obj_Datos_Api_left->Total("sessions")) ).'%';
    	}
    }

    return $devuelve;
}

//conversiones 
function resumen_datatable_conversiones_res ($srtipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{
	global $trans;
	$DataTables_start  = 0;
	$DataTables_length = 1000;

	$obj_Datos_Api_admin = new Datos_Api_Admin(); // Instanciamos la clase Datos_Api_Admin
    $obj_Datos_Api_admin -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_admin -> idVista              = $idVistaAnalytics ;
    $obj_Datos_Api_admin -> TipoLlamada          = 'GOALS_VISTA' ;   
    $obj_Datos_Api_admin -> Construccion();

    $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_METRICAS_GOAL();
	$ARRAY_GOALS_VISTA_ORDENADOS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_RESUMEN();

	//Hacemos tantas llamadas al api de datos como elementos tenga el array de métricas.  En cada llamada sacaremos cinco objetivos (10 méticas)
	$array_Datos_Api = array();

	$filtro = "ga:goalCompletionsAll>0";
	if ($strFiltro!="no"){
		$filtro .= ';'.$strFiltro;
	}

	$anno = substr($datFechaFinFiltro, 0, strlen($datFechaFinFiltro)-6);
	$anno = date('Y');

	$fecha_ini_anno = $anno.'-01-01';
	$fecha_fin_anno = $anno.'-12-31';

	//LAMAMOS A LA APII PARA VER EN EL AÑÑO LOS IMPACTOS
	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL); $intI++) {

	    //Creación objeto, parametrización -------------------------------------------------------
	    $array_Datos_Api[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $array_Datos_Api[$intI] -> strProyectoAsociado  = $strProyectoAsociado ;
	    $array_Datos_Api[$intI] -> idVista              ='ga:'. $idVistaAnalytics ;
	    $array_Datos_Api[$intI] -> startdate            = $fecha_ini_anno ;
	    $array_Datos_Api[$intI] -> enddate              = $fecha_fin_anno ;
	    $array_Datos_Api[$intI] -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL[$intI] ;
	    $array_Datos_Api[$intI] -> optParams            = array(
												//'dimensions' => 'ga:sessions',
	                                            //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 3);  
	    $array_Datos_Api[$intI] -> Construccion();

	}//Llamadas a la api


	$array_anual = array();
	//LOS RECORREMOS Y GUARDAMOS EN UNA ARRAY LOS QUE TENGAN MAS DE 0
	for ($x=1; $x<=$array_Datos_Api[0]->NumValores(); $x++) {

		for ($i=1; $i <= count($ARRAY_GOALS_VISTA_ORDENADOS) ; $i++) { 
			
			$array_anual[$i]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
			$array_anual[$i]["idgoal"] = $i;
			$array_anual[$i]["valor"] = $array_Datos_Api[0]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);
			$array_anual[$i]["sum"]  = $array_Datos_Api[0]->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);

		}

	}

	//LAMAMOS A LA APII con la fecha enviada
	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL); $intI++) {

	    //Creación objeto, parametrización -------------------------------------------------------
	    $array_Datos_Api_actu[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $array_Datos_Api_actu[$intI] -> strProyectoAsociado  = $strProyectoAsociado ;
	    $array_Datos_Api_actu[$intI] -> idVista              ='ga:'. $idVistaAnalytics ;
	    $array_Datos_Api_actu[$intI] -> startdate            = $datFechaInicioFiltro ;
	    $array_Datos_Api_actu[$intI] -> enddate              = $datFechaFinFiltro ;
	    $array_Datos_Api_actu[$intI] -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL[$intI] ;
	    $array_Datos_Api_actu[$intI] -> optParams            = array(
												//'dimensions' => 'ga:sessions',
	                                            //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 3);  
	    $array_Datos_Api_actu[$intI] -> Construccion();

	}//Llamadas a la api

	$count = 0;
	$strB="";
	$semanatotal=0;
	$annototal=0;
	//LOS RECORREMOS Y GUARDAMOS EN UNA ARRAY LOS QUE TENGAN MAS DE 0
	for ($x=1; $x<=$array_Datos_Api_actu[0]->NumValores(); $x++) {

		for ($i=1; $i <= count($ARRAY_GOALS_VISTA_ORDENADOS) ; $i++) { 
			
			if($array_anual[$i]["valor"]!=0){

				if ($count < 3) {

					$semanatotal += $array_Datos_Api_actu[0]->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);
					$annototal   += $array_anual[$i]["sum"];

					$strB .= '<tr><td>'.$trans->__($ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"], false).'</td><td>'.$array_Datos_Api_actu[0]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x).'</td><td>'.$array_anual[$i]["valor"].'</td></tr>';
					$count++;
				}

			}
		}

	}


	if($srtipo=="datatable"){
			
		$strA .= $strB;

	}

	return $strA;
	
}

function resumen_datatable_conversiones ($srtipo, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{
	global $trans;
	$DataTables_start  = 0;
	$DataTables_length = 1000;

	$obj_Datos_Api_admin = new Datos_Api_Admin(); // Instanciamos la clase Datos_Api_Admin
    $obj_Datos_Api_admin -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_admin -> idVista              = $idVistaAnalytics ;
    $obj_Datos_Api_admin -> TipoLlamada          = 'GOALS_VISTA' ;   
    $obj_Datos_Api_admin -> Construccion();

    $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_METRICAS_GOAL();
	$ARRAY_GOALS_VISTA_ORDENADOS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_RESUMEN();

	//Hacemos tantas llamadas al api de datos como elementos tenga el array de métricas.  En cada llamada sacaremos cinco objetivos (10 méticas)
	$array_Datos_Api = array();

	$filtro = "ga:goalCompletionsAll>0";
	if ($strFiltro!="no"){
		$filtro .= ';'.$strFiltro;
	}

	$anno = substr($datFechaFinFiltro, 0, strlen($datFechaFinFiltro)-6);
	$anno = date('Y');

	$fecha_ini_anno = $anno.'-01-01';
	$fecha_fin_anno = $anno.'-12-31';

	//LAMAMOS A LA APII PARA VER EN EL AÑO LOS IMPACTOS
	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL); $intI++) {

	    //Creación objeto, parametrización -------------------------------------------------------
	    $array_Datos_Api[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $array_Datos_Api[$intI] -> strProyectoAsociado  = $strProyectoAsociado ;
	    $array_Datos_Api[$intI] -> idVista              ='ga:'. $idVistaAnalytics ;
	    $array_Datos_Api[$intI] -> startdate            = $fecha_ini_anno ;
	    $array_Datos_Api[$intI] -> enddate              = $fecha_fin_anno ;
	    $array_Datos_Api[$intI] -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL[$intI] ;
	    $array_Datos_Api[$intI] -> optParams            = array(
												//'dimensions' => 'ga:sessions',
	                                            //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
	    $array_Datos_Api[$intI] -> Construccion();

	}//Llamadas a la api


	$array_anual = array();
	//LOS RECORREMOS Y GUARDAMOS EN UNA ARRAY LOS QUE TENGAN MAS DE 0
	for ($x=1; $x<=$array_Datos_Api[0]->NumValores(); $x++) {

		for ($i=1; $i <= count($ARRAY_GOALS_VISTA_ORDENADOS) ; $i++) { 
			
			$array_anual[$i]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
			$array_anual[$i]["idgoal"] = $i;
			$array_anual[$i]["valor"] = $array_Datos_Api[0]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);
			$array_anual[$i]["sum"]  = $array_Datos_Api[0]->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);

		}

	}

	//LAMAMOS A LA APII con la fecha enviada
	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL); $intI++) {

	    //Creación objeto, parametrización -------------------------------------------------------
	    $array_Datos_Api_actu[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $array_Datos_Api_actu[$intI] -> strProyectoAsociado  = $strProyectoAsociado ;
	    $array_Datos_Api_actu[$intI] -> idVista              ='ga:'. $idVistaAnalytics ;
	    $array_Datos_Api_actu[$intI] -> startdate            = $datFechaInicioFiltro ;
	    $array_Datos_Api_actu[$intI] -> enddate              = $datFechaFinFiltro ;
	    $array_Datos_Api_actu[$intI] -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL[$intI];
	    $array_Datos_Api_actu[$intI] -> optParams            = array(
												//'dimensions' => 'ga:sessions',
	                                            //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
	    $array_Datos_Api_actu[$intI] -> Construccion();

	}//Llamadas a la api

	$blnComparacion = true;
	//Si hay que comparar creamos el objeto con la fecha anterior
    if ($blnComparacion=="true") {
    	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL); $intI++) {
	    	//Creación objeto, parametrización -------------------------------------------------------
		    $array_Datos_Api_ant[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		    $array_Datos_Api_ant[$intI] -> strProyectoAsociado  = $strProyectoAsociado;
		    $array_Datos_Api_ant[$intI] -> idVista              ='ga:'. $idVistaAnalytics;
		    $array_Datos_Api_ant[$intI] -> startdate            = $strFechaInicioFiltroAnt;
		    $array_Datos_Api_ant[$intI] -> enddate              = $strFechaFinFiltroAnt;
		    $array_Datos_Api_ant[$intI] -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS_GOAL[$intI];

		   	$array_Datos_Api_ant[$intI] -> optParams        = array(
														//'dimensions' => $strDimensions,
														'filters' => $filtro,
														'start-index' => $DataTables_start + 1,
			                                            'max-results' => $DataTables_length);  
		   	
		   	$array_Datos_Api_ant[$intI] -> Construccion();
	   }
    }

	$count = 0;
	$strB="";
	$semanatotal=0;
	$semanatotal_ant=0;
	$annototal=0;
	//LOS RECORREMOS Y GUARDAMOS EN UNA ARRAY LOS QUE TENGAN MAS DE 0
	for ($x=1; $x<=$array_Datos_Api_actu[0]->NumValores(); $x++) {

		for ($i=1; $i <= count($ARRAY_GOALS_VISTA_ORDENADOS) ; $i++) { 
			
			if($array_anual[$i]["valor"]!=0){

				$semanatotal += $array_Datos_Api_actu[0]->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);
				$annototal   += $array_anual[$i]["sum"];
				$semanatotal_ant += $array_Datos_Api_ant[0]->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);

				$imgs = DevolverContenido_Total($array_Datos_Api_actu[0]->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x),$array_Datos_Api_ant[0]->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x),$array_Datos_Api_actu[0]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x),$array_Datos_Api_ant[0]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x));
				$strB .= "<tr><td><span class='negro'>".$trans->__($ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"], false).'</span></td><td>'.$imgs.$array_Datos_Api_actu[0]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x).'</td><td>'.$array_anual[$i]["valor"].'</td></tr>';
				$count++;
			}
		}

	}

	if($srtipo=="datatable"){

		$strA = $strB;

	}else{

		$imgs = DevolverContenido_Total($semanatotal,$semanatotal_ant,number_format($semanatotal,0,',','.'),number_format($semanatotal_ant,0,',','.'));

		//Mostramso los totales
		$strA  = "<th><b><span id='total_conversiones_semana'>".$imgs.number_format($semanatotal,0,',','.')."</span></b></th>";
		$strA .= "<th><b><span id='total_conversiones_mes'>".number_format($annototal,0,',','.')."</span></b></th>";

	}


	return $strA;
	
}

function resumen_datatable_idioma ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{

	$filtro="ga:dimension1!=(not set)";	

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	$filtrob="ga:dimension8!=(not set)";	

	if ($strFiltro!="no"){
		if(!empty($filtrob)){ $filtrob .= ","; }
		$filtrob .= $strFiltro;
	}

	$DataTables_start  = 0;
	$DataTables_length = 1000;
	///LLAMAMOS AL ORDENAR CABECERAS

	//Izquierda
	//Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_left = new Datos_Api_Informes(); 
    $obj_Datos_Api_left -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_left -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_left -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_left -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_left -> metrics              = 'ga:pageviews';
    $obj_Datos_Api_left -> optParams            = array(
												'dimensions' => 'ga:dimension1',
												'sort' => '-ga:pageviews',
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 1000);  

    $obj_Datos_Api_left -> Construccion();

    //Arriba
    $obj_Datos_Api_top = new Datos_Api_Informes(); 
    $obj_Datos_Api_top -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_top -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_top -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_top -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_top -> metrics              = 'ga:pageviews';
    $obj_Datos_Api_top -> optParams            = array(
												'dimensions' => 'ga:dimension8',
												'sort' => '-ga:pageviews',
												'filters' => $filtrob,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 1000);  

    $obj_Datos_Api_top -> Construccion();

    //datos
	$obj_Datos_Api_dat = new Datos_Api_Informes(); 
    $obj_Datos_Api_dat -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_dat -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_dat -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_dat -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_dat -> metrics              = 'ga:pageviews';
    $obj_Datos_Api_dat -> optParams        	   = array(
												'dimensions' => 'ga:dimension1,ga:dimension8',
												'sort' => 'ga:dimension1',
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 1000);  
  
    $obj_Datos_Api_dat -> Construccion();

	
	//TOP
    $top = 10;
    $count = 0;
    $otros = 0;

    $strB='';
    //recorremos las cabeceras
    for ($x=1; $x<=$obj_Datos_Api_left->NumValores(); $x++) {

    	$strB .= '<tr>';

    	//Cabecera
    	$strB .= '<td><div><span class="negro">'.traducir_idioma($obj_Datos_Api_left->Valor("dimension1",$x)).'</span></div></td>';

		$count = 0;
		$otros = 0;
    	for ($i=1; $i<=$obj_Datos_Api_top->NumValores(); $i++) {

    		$sw=0;

    		//Comprobamos si coinciden las dimensiones 1 y 8
	    	if(  $obj_Datos_Api_left->Valor("dimension1",$x) == $obj_Datos_Api_top->Valor("dimension8",$i)  ){
	    		$inidiv = "<div class='destacado'>";
	    	}else{
	    		$inidiv = "<div>";
	    	}
	    	
    		for ($j=1; $j<=$obj_Datos_Api_dat->NumValores(); $j++) {
		

	    		if( ( $obj_Datos_Api_left->Valor("dimension1",$x) == $obj_Datos_Api_dat->Valor("dimension1",$j) ) && ( $obj_Datos_Api_top->Valor("dimension8",$i) == $obj_Datos_Api_dat->Valor("dimension8",$j) ) ){
					
	    			//Comprobamos los tops
	    			if($count < $top){
	    				$strB .= '<td>'.$inidiv.$obj_Datos_Api_dat->ValorF("pageviews",$j).'</div></td>';
	    				$count++;
	    			}else if($count >= $top){
	    				//Sumamos todos los otros
	    				$otros += $obj_Datos_Api_dat->Valor("pageviews",$j);
	    				$count++;
	    			}
					
					$sw=1;

	    		}

    		}  		

    		if($sw==0){
    			if($count < $top){
	    			$strB .= '<td>'.$inidiv.'</div></td>';
	    			$count++;
    			}
    		}	

    	}

    	//si count es mayor que top añadimos el otros
    	if($count >= $top){
    		if($otros == 0){$otros = "";}
    		$strB .= '<td><div>'.$otros.'</div></td>';
    	}


    	$strB .= '</tr>';

    }
    
	//Devolvemos el json
	return $strB;
}

function tablaidioma_idioma ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro, $strApp )
{

	$filtro="ga:dimension1!=(not set)";	

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	$filtrob="";	

	if ($strFiltro!="no"){
		if(!empty($filtrob)){ $filtrob .= ","; }
		$filtrob .= $strFiltro;
	}

	$DataTables_start  = 0;
	$DataTables_length = 1000;
	///LLAMAMOS AL ORDENAR CABECERAS


	if($strApp == 0){
		$metricas  = 'ga:pageviews';
		$metricnom = 'pageviews';
	}else{
		$metricas  = 'ga:screenviews';
		$metricnom = 'screenviews';
	}

	//Izquierda
	//Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_left = new Datos_Api_Informes(); 
    $obj_Datos_Api_left -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_left -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_left -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_left -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_left -> metrics              = $metricas;
    $obj_Datos_Api_left -> optParams            = array(
												'dimensions' => 'ga:dimension1',
												'sort' => '-'.$metricas,
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 1000);  

    $obj_Datos_Api_left -> Construccion();

    //Arriba
    $obj_Datos_Api_top = new Datos_Api_Informes(); 
    $obj_Datos_Api_top -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_top -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_top -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_top -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_top -> metrics              = $metricas;
    if(empty($filtrob)){
	    $obj_Datos_Api_top -> optParams            = array(
													'dimensions' => 'ga:dimension8',
													'sort' => '-'.$metricas,
													'start-index' => $DataTables_start + 1,
	                                            	'max-results' => 1000);  
	}else{
		$obj_Datos_Api_top -> optParams            = array(
													'dimensions' => 'ga:dimension8',
													'sort' => '-'.$metricas,
													'filters' => $filtrob,
													'start-index' => $DataTables_start + 1,
	                                            	'max-results' => 1000);  
	}
    $obj_Datos_Api_top -> Construccion();

    //datos
	$obj_Datos_Api_dat = new Datos_Api_Informes(); 
    $obj_Datos_Api_dat -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_dat -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_dat -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_dat -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_dat -> metrics              = $metricas;
    $obj_Datos_Api_dat -> optParams        	   = array(
												'dimensions' => 'ga:dimension1,ga:dimension8',
												'sort' => 'ga:dimension1',
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => 1000);  
  
    $obj_Datos_Api_dat -> Construccion();
	
	//TOP
    $top = 20;
    $count = 0;
    $otros = 0;

    $strB='';
    //recorremos las cabeceras
    for ($x=1; $x<=$obj_Datos_Api_left->NumValores(); $x++) {

    	$strB .= '<tr>';

    	//Cabecera
    	$strB .= '<td class="tdcans"><div class="padingo"><span class="negro">'.traducir_idioma($obj_Datos_Api_left->Valor("dimension1",$x)).'</span></div></td>';

		$count = 0;
		$otros = 0;
    	for ($i=1; $i<=$obj_Datos_Api_top->NumValores(); $i++) {

    		$sw=0;

    		//Comprobamos si coinciden las dimensiones 1 y 8
	    	if(  $obj_Datos_Api_left->Valor("dimension1",$x) == $obj_Datos_Api_top->Valor("dimension8",$i)  ){
	    		$inidiv = "<div class='destacado padingo'>";
	    	}else{
	    		$inidiv = "<div class='padingo'>";
	    	}
	    	
    		for ($j=1; $j<=$obj_Datos_Api_dat->NumValores(); $j++) {
		

	    		if( ( $obj_Datos_Api_left->Valor("dimension1",$x) == $obj_Datos_Api_dat->Valor("dimension1",$j) ) && ( $obj_Datos_Api_top->Valor("dimension8",$i) == $obj_Datos_Api_dat->Valor("dimension8",$j) ) ){
					
	    			//Comprobamos los tops
	    			if($count < $top){
	    				$strB .= '<td class="tdcans">'.$inidiv.$obj_Datos_Api_dat->ValorF($metricnom,$j).'</div></td>';
	    				$count++;
	    			}else if($count >= $top){
	    				//Sumamos todos los otros
	    				$otros += $obj_Datos_Api_dat->Valor($metricnom,$j);
	    				$count++;
	    			}
					
					$sw=1;

	    		}

    		}  		

    		if($sw==0){
    			if($count < $top){
	    			$strB .= '<td class="tdcans">'.$inidiv.'</div></td>';
	    			$count++;
    			}
    		}	

    	}

    	//si count es mayor que top añadimos el otros
    	if($count >= $top){
    		if($otros == 0){$otros = "";}
    		$strB .= '<td class="tdcans"><div class="padingo">'.$otros.'</div></td>';
    	}


    	$strB .= '</tr>';

    }
    
	//Devolvemos el json
	return $strB;
}  

function resumen_chart_trafico($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro, $tim, $color){
  global $trans;

	$enlaces_sesiones_total = 0;
	$buscadores_sesiones_total = 0;
	$social_sesiones_total = 0;
	$campanas_sesiones_total = 0;
	$directo_sesiones_total = 0;

	if($tim==1){
		//PARA LA LLAMADA ANUAL
	    //Ahora sacamos el año de la fecha para recorrer el año entero
	    $anno = substr($datFechaInicioFiltro, 0, 4);
	    $datFechaInicioFiltro = $anno."-01-01";
	    $datFechaFinFiltro = $anno."-12-31";
	}

	$filtro="";	
	if ($strFiltro!="no"){
		$filtro .= $strFiltro.";";
	}

	//Creación objeto, parametrización -------------------------------------------------------
	$array_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$array_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
	$array_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
	$array_Datos_Api -> startdate            = $datFechaInicioFiltro ;
	$array_Datos_Api -> enddate              = $datFechaFinFiltro ;
	$array_Datos_Api -> metrics              = "ga:sessions";
	$array_Datos_Api -> optParams            = array(
														'dimensions' => 'ga:campaign,ga:medium,ga:socialNetwork,ga:source,ga:keyword',
														'filters' => $filtro."ga:goalCompletionsAll>=0",
														'sort' => '-ga:sessions',
														'start-index' => $DataTables_start + 1,
			                                            'max-results' => $DataTables_length);  
	$array_Datos_Api -> Construccion();
		
	for ($x=1; $x<=$array_Datos_Api->NumValores(); $x++) {

		//Aqui sumamos todas las sesiones para sacar el porcentaje
		$sesiones_total += $array_Datos_Api->Valor("sessions",$x);
		//****

		//Para enlaces (ga:medium==referral;ga:SocialNetwork==(not set))
		if ($array_Datos_Api->Valor("medium",$x) == "referral" && $array_Datos_Api->Valor("SocialNetwork",$x) == "(not set)") {

			$enlaces_sesiones_total += $array_Datos_Api->Valor("sessions",$x);
			
		}
		//Para busqueda ga:medium==organic;ga:SocialNetwork==(not set)
		if ($array_Datos_Api->Valor("medium",$x) == "organic" && $array_Datos_Api->Valor("SocialNetwork",$x) == "(not set)") {
			
			$buscadores_sesiones_total += $array_Datos_Api->Valor("sessions",$x);
		
		}
		//Para social ga:SocialNetwork==(not set)
		if ($array_Datos_Api->Valor("SocialNetwork",$x) != "(not set)") {
			
			$social_sesiones_total += $array_Datos_Api->Valor("sessions",$x);
			
		}
		//Para campañas ga:medium!=referral;ga:medium!=organic;ga:medium!=(none); ga:SocialNetwork==(not set)
		if ($array_Datos_Api->Valor("medium",$x) != "referral" && $array_Datos_Api->Valor("medium",$x) != "organic" && $array_Datos_Api->Valor("medium",$x) != "(none)" && $array_Datos_Api->Valor("SocialNetwork",$x) == "(not set)") {
			
			$campanas_sesiones_total += $array_Datos_Api->Valor("sessions",$x);
			
		}
		//Para directo ga:medium==(none)
		if ($array_Datos_Api->Valor("medium",$x) == "(none)") {
			
			$directo_sesiones_total += $array_Datos_Api->Valor("sessions",$x);
			
		}

	}

	//Componemos los colores
	$strColores = array();
	$strColores[0]["nombre"]  = $trans->__("Hombre", false);
	$strColores[0]["color"]   = "#00b3ed";
	$strColores[1]["nombre"]  = $trans->__("Mujer", false);
	$strColores[1]["color"]   = "#f892e5";
	$strColores[2]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('18', '24'));
	$strColores[2]["color"]   = "#ecf9f2";
	$strColores[3]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('25', '34'));
	$strColores[3]["color"]   = "#c6eed6";
	$strColores[4]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('35', '44'));
	$strColores[4]["color"]   = "#9de1bc";
	$strColores[5]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('45', '54'));
	$strColores[5]["color"]   = "#7ad8a6";	
	$strColores[6]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('55', '64'));
	$strColores[6]["color"]   = "#3fcc8a";
	$strColores[7]["nombre"]  = vsprintf($trans->__('Mayores de %1$s años', false), array('65'));
	$strColores[7]["color"]   = "#00c274";
	$strColores[8]["nombre"]  = $trans->__("Búsquedas", false);
	$strColores[8]["color"]   = "#fe6b5b";
	$strColores[9]["nombre"]  = $trans->__("Social", false);
	$strColores[9]["color"]   = "#fbc63c";
	$strColores[10]["nombre"] = $trans->__("Directo", false);
	$strColores[10]["color"]  = "#7d5fa9";
	$strColores[11]["nombre"] = $trans->__("Enlaces", false);
	$strColores[11]["color"]  = "#01d5fb";
	$strColores[12]["nombre"] = $trans->__("Campañas", false);
	$strColores[12]["color"]  = "#01d9b2";



	

	$cont = 0;
	//Montamos el json los graficos de tarta
	$strB = "[";
	$strB .= "['Fuente', 'Sesiones'],";
	$strB .= "['" . $trans->__('Enlaces', false) . "', ".$enlaces_sesiones_total."],";
	$strB .= "['" . $trans->__('Búsquedas', false) . "', ".$buscadores_sesiones_total."],";
	$strB .= "['" . $trans->__('Social', false) . "', ".$social_sesiones_total."],";
	$strB .= "['" . $trans->__('Campañas', false) . "', ".$campanas_sesiones_total."],";
	$strB .= "['" . $trans->__('Directo', false) . "', ".$directo_sesiones_total."]";
	$strB .= "]";

	if($enlaces_sesiones_total != 0){
		$colores .= $cont.": { color: '".$strColores[11]["color"]."'},";
		$cont++;
	}
	if($buscadores_sesiones_total != 0){
		$colores .= $cont.": { color: '".$strColores[8]["color"]."'},";
		$cont++;
	}
	if($social_sesiones_total != 0){
		$colores .= $cont.": { color: '".$strColores[9]["color"]."'},";
		$cont++;
	}
	if($campanas_sesiones_total != 0){
		$colores .= $cont.": { color: '".$strColores[12]["color"]."'},";
		$cont++;
	}
	if($directo_sesiones_total != 0){
		$colores .= $cont.": { color: '".$strColores[10]["color"]."'},";
		$cont++;
	}
	
	if($color==0){

		$str = "{";
		$str .= '"llamada":"'.$strB.'"';
		$str .= "}";
	

	}else{

		$colores = substr($colores, 0, -1);
		$colores = "{".$colores."}";

		$str = $colores;
	}

    return $str;

}

function resumen_datatable_trafico ($strTipo, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{

	global $trans;
	$metricas = 'ga:sessions,ga:pageviewsPerSession,ga:bounceRate,ga:users';
	$Dimension = 'ga:week';

	$filtro="";	

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
	$DataTables_start  = 0;
	$DataTables_length = 1000;
	
	//PARA LAS LLAMADAS SEMANALES
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
    $obj_Datos_Api -> Construccion();
	
	$blnComparacion = true;
	//Si hay que comparar creamos el objeto con la fecha anterior
    if ($blnComparacion=="true") {
    	//Creación objeto, parametrización -------------------------------------------------------
	    $obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado;
	    $obj_Datos_Api_ant -> idVista              ='ga:'. $idVistaAnalytics;
	    $obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt;
	    $obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt;
	    $obj_Datos_Api_ant -> metrics              = $metricas;
	    if(empty($filtro)){
	    	$obj_Datos_Api_ant -> optParams        = array(
													'dimensions' => $strDimensions,
													'start-index' => $DataTables_start + 1,
		                                            'max-results' => $DataTables_length);  
	   	}else{
	   		$obj_Datos_Api_ant -> optParams        = array(
													'dimensions' => $strDimensions,
													'filters' => $filtro,
													'start-index' => $DataTables_start + 1,
		                                            'max-results' => $DataTables_length);  
	   	}
	   	$obj_Datos_Api_ant -> Construccion();
    }

    //PARA LA LLAMADA ANUAL

    //Ahora sacamos el año de la fecha para recorrer el año entero
    $anno = substr($datFechaInicioFiltro, 0, 4);
    $fechaini = $anno."-01-01";
    $fechafin = $anno."-12-31";

    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_anno = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_anno -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_anno -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_anno -> startdate            = $fechaini;
    $obj_Datos_Api_anno -> enddate              = $fechafin;
    $obj_Datos_Api_anno -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
   	$obj_Datos_Api_anno -> Construccion();

	$strA .= '<tr>';

		$strA .= "<td><span class='negro'>" . $trans->__('Sesiones', false) ."</span></td>";
		$strA .= "<td>".DevolverContenido_Total($obj_Datos_Api->Valor("sessions",1),$obj_Datos_Api_ant->Valor("sessions",1),$obj_Datos_Api->ValorF("sessions",1),$obj_Datos_Api_ant->ValorF("sessions",1))." ".$obj_Datos_Api->ValorF("sessions",1)."</td>";
		$strA .= "<td>".$obj_Datos_Api_anno->ValorF("sessions",1)."</td>";

	$strA .= '</tr>';
	$strA .= '<tr>';

		$strA .= "<td><span class='negro'>" . $trans->__('Usuarios', false) ."</span></td>";
		$strA .= "<td>".DevolverContenido_Total($obj_Datos_Api->Valor("users",1),$obj_Datos_Api_ant->Valor("users",1),$obj_Datos_Api->ValorF("users",1),$obj_Datos_Api_ant->ValorF("users",1))." ".$obj_Datos_Api->ValorF("users",1)."</td>";
		$strA .= "<td>".$obj_Datos_Api_anno->ValorF("users",1)."</td>";

	$strA .= '</tr>';
	$strA .= '<tr>';

		$strA .= "<td><span class='negro'>" . $trans->__('Páginas/Sesión', false) ."</span></td>";
		$strA .= "<td>".DevolverContenido_Total($obj_Datos_Api->Valor("pageviewsPerSession",1),$obj_Datos_Api_ant->Valor("pageviewsPerSession",1),$obj_Datos_Api->ValorF("pageviewsPerSession",1),$obj_Datos_Api_ant->ValorF("pageviewsPerSession",1))." ".$obj_Datos_Api->ValorF("pageviewsPerSession",1)."</td>";
		$strA .= "<td>".$obj_Datos_Api_anno->ValorF("pageviewsPerSession",1)."</td>";

	$strA .= '</tr>';
	$strA .= '<tr>';

		$strA .= "<td><span class='negro'>" . $trans->__('Rebote', false) ."</span></td>";
		$strA .= "<td>".DevolverContenido_Total($obj_Datos_Api_ant->Valor("bounceRate",1),$obj_Datos_Api->Valor("bounceRate",1),$obj_Datos_Api->ValorF("bounceRate",1),$obj_Datos_Api_ant->ValorF("bounceRate",1))." ".round( $obj_Datos_Api->Valor("bounceRate",1) )."%</td>";
		$strA .= "<td>".round( $obj_Datos_Api_anno->Valor("bounceRate",1) )."%</td>";

	$strA .= '</tr>';

	return $strA;
} 

function resumen_datatable_trafico_res ($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{

	global $trans;

	$metricas = 'ga:sessions,ga:pageviewsPerSession,ga:bounceRate,ga:users';
	$Dimension = 'ga:week';

	$filtro="";	

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
	$DataTables_start  = 0;
	$DataTables_length = 1000;

	//PARA LAS LLAMADAS SEMANALES
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
    $obj_Datos_Api -> Construccion();
	
    //PARA LA LLAMADA ANUAL

    //Ahora sacamos el año de la fecha para recorrer el año entero
    $anno = substr($datFechaInicioFiltro, 0, 4);
    $fechaini = $anno."-01-01";
    $fechafin = $anno."-12-31";

    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_anno = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_anno -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_anno -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_anno -> startdate            = $fechaini;
    $obj_Datos_Api_anno -> enddate              = $fechafin;
    $obj_Datos_Api_anno -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
   	$obj_Datos_Api_anno -> Construccion();


		$strA .= '<tr>';
			
			$strA .= '<td>' . $trans->__('Sesiones', false) .'</td>';
			$strA .= '<td>'.$obj_Datos_Api->ValorF("sessions",1).'</td>';
			$strA .= '<td>'.$obj_Datos_Api_anno->ValorF("sessions",1).'</td>';			

		$strA .= '</tr>';
		$strA .= '<tr>';
			
			$strA .= '<td>' . $trans->__('Usuarios', false) .'</td>';
			$strA .= '<td>'.$obj_Datos_Api->ValorF("users",1).'</td>';
			$strA .= '<td>'.$obj_Datos_Api_anno->ValorF("users",1).'</td>';			

		$strA .= '</tr>';
		$strA .= '<tr>';

			$strA .= '<td>' . $trans->__('Rebote', false) .'</td>';
			$strA .= '<td>'.round($obj_Datos_Api->Valor("bounceRate",1)).'%</td>';
			$strA .= '<td>'.round($obj_Datos_Api_anno->Valor("bounceRate",1)).'%</td>';			

		$strA .= '</tr>';


	return $strA;
}  

function resumen_piechart($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro, $color){
  global $trans;

		//$strSort='yearWeek';
	switch ($strTipo) {
		case 'sexo':
			$strDimensions='ga:userGender';
			$strDimensionsN = 'userGender'; 
			$metricas = 'ga:sessions';
			$ordenacion = '-ga:sessions';
			break;
		case 'edad':
			$strDimensions='ga:userAgeBracket';
			$strDimensionsN = 'userAgeBracket'; 
			$metricas = 'ga:sessions';
			$ordenacion = 'ga:userAgeBracket';
			break;
	};

	$filtro="";
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = $metricas;

	if(empty($filtro)){
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
	                                        'sort' => $ordenacion,
											'start-index' => 1,
	                                        'max-results' => 6); 
	}else{
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
											'filters' => $filtro,
	                                        'sort' => $ordenacion,
											'start-index' => 1,
	                                        'max-results' => 6); 
		
	}
	 
	$obj_Datos_Api -> Construccion();

	//Componemos los colores
	$strColores = array();
	$strColores[0]["nombre"]  = $trans->__("Hombre",false);
	$strColores[0]["color"]   = "#00b3ed";
	$strColores[1]["nombre"]  = $trans->__("Mujer",false);
	$strColores[1]["color"]   = "#f892e5";
	$strColores[2]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('18', '24'));
	$strColores[2]["color"]   = "#ecf9f2";
	$strColores[3]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('25', '34'));
	$strColores[3]["color"]   = "#c6eed6";
	$strColores[4]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('35', '44'));
	$strColores[4]["color"]   = "#9de1bc";
	$strColores[5]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('45', '54'));
	$strColores[5]["color"]   = "#7ad8a6";	
	$strColores[6]["nombre"]  = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('55', '64'));
	$strColores[6]["color"]   = "#3fcc8a";
	$strColores[7]["nombre"]  = vsprintf($trans->__('Mayores de %1$s años', false), array('65'));
	$strColores[7]["color"]   = "#00c274";
	$strColores[8]["nombre"]  = $trans->__("Búsquedas",false);
	$strColores[8]["color"]   = "#fe6b5b";
	$strColores[9]["nombre"]  = $trans->__("Social",false);
	$strColores[9]["color"]   = "#fbc63c";
	$strColores[10]["nombre"] = $trans->__("Directo",false);
	$strColores[10]["color"]  = "#7d5fa9";
	$strColores[11]["nombre"] = $trans->__("Enlaces",false);
	$strColores[11]["color"]  = "#01d5fb";
	$strColores[12]["nombre"] = $trans->__("Campañas",false);
	$strColores[12]["color"]  = "#01d9b2";

	

	
		$cont = 0;
		//Recorremos la array que traemos de la api
		$strAux = "[['Idioma', 'Sesiones'],";

		for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
			//$strAuxb .= '[\''.str_replace("'", " ", $obj_Datos_Api->Valor($strDimensionsN,$x)).'\','.$obj_Datos_Api->Valor("pageviews",$x).','.$obj_Datos_Api->Valor("sessions",$x).','.$obj_Datos_Api->Valor("bounceRate",$x).','.$obj_Datos_Api->Valor("avgSessionDuration",$x).','.$obj_Datos_Api->Valor("pageviewsPerSession",$x).'],';
			if ($obj_Datos_Api->Valor($strDimensionsN,$x) != "(not set)") {
				$strAuxb .= '[\''.traducir_idioma($obj_Datos_Api->Valor($strDimensionsN,$x)).'\','.$obj_Datos_Api->Valor("sessions",$x).'],';			
				
				$tr = Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api);

				foreach ($strColores as $key => $col) {
					if( $tr == $col["nombre"]){
						$colores .= $cont.": { color: '".$col['color']."'},";
						$cont++;
					}
				}

			}
		}

		
		$strAuxb = substr($strAuxb, 0, -1);
		//Montando el json
		
		$strAux .= $strAuxb;
		$strAux .= "]";

	if($color == 0){

		$str = "{";
		$str .= '"llamada":"'.$strAux.'"';
		$str .= "}";

	}else{
		
		$colores = substr($colores, 0, -1);
		$colores = "{".$colores."}";

		$str = $colores;

	}
    return $str;

}

function resumen_datatable ($strTipo, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{
	global $trans;
	$filtro_and="";
	//$strSort='yearWeek';
	switch ($strTipo) {
		case 'sexo':
			$strDimensions  ='ga:userGender';
			$strDimensionsN ='userGender'; 
			$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			$array_Ordenacion = "-ga:sessions";
			break;
		case 'edad':
			$strDimensions='ga:userAgeBracket';
			$strDimensionsN ='userAgeBracket'; 
			$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			$array_Ordenacion = "ga:userAgeBracket";
			break;
		case 'pais':
			$strDimensions='ga:country,ga:countryIsoCode';
			$strDimensionsN ='country'; 
			$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			$array_Ordenacion = "-ga:sessions";
			break;
	};

	$filtro="";	

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
	if(!empty($filtro_and)){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $filtro_and;
	}
	
	$DataTables_start  = 0;
	$DataTables_length = 1000;
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
	                                            'sort' => $array_Ordenacion,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
	                                            'sort' => $array_Ordenacion,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
    $obj_Datos_Api -> Construccion();
	
	$blnComparacion = true;
	//Si hay que comparar creamos el objeto con la fecha anterior
    if ($blnComparacion=="true") {
    	//Creación objeto, parametrización -------------------------------------------------------
	    $obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado;
	    $obj_Datos_Api_ant -> idVista              ='ga:'. $idVistaAnalytics;
	    $obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt;
	    $obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt;
	    $obj_Datos_Api_ant -> metrics              = $metricas;
	    if(empty($filtro)){
	    	$obj_Datos_Api_ant -> optParams        = array(
													'dimensions' => $strDimensions,
													'sort' => $array_Ordenacion,
													'start-index' => $DataTables_start + 1,
		                                            'max-results' => $DataTables_length);  
	   	}else{
	   		$obj_Datos_Api_ant -> optParams        = array(
													'dimensions' => $strDimensions,
													'filters' => $filtro,
		                                            'sort' => $array_Ordenacion,
													'start-index' => $DataTables_start + 1,
		                                            'max-results' => $DataTables_length);  
	   	}
	   	$obj_Datos_Api_ant -> Construccion();
    }

    $anno = substr($datFechaInicioFiltro, 0, 4);
    $fechaini = $anno."-01-01";
    $fechafin = $anno."-12-31";
	
	//PARA ANNO
    $obj_Datos_Api_anno = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_anno -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_anno -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_anno -> startdate            = $fechaini;
    $obj_Datos_Api_anno -> enddate              = $fechafin;
    $obj_Datos_Api_anno -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
	                                            'sort' => $array_Ordenacion,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
	                                            'sort' => $array_Ordenacion,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
    $obj_Datos_Api_anno -> Construccion();
    $array_anno = array();
    $cont_anno = 0;
    $otrosanno = 0;

 	//Añadimos un top
	if($strTipo=="pais"){
		$top = 5;
	}else{
		$top = 6;
	}    

	for ($x=1; $x<=$obj_Datos_Api_anno->NumValores(); $x++) {

		
		//El primer valor va a depender de la perspectiva que estamos pintando
		if($strTipo=="pais"){
			$array_anno[$cont_anno]["indetificador"] = $obj_Datos_Api_anno->Valor("countryIsoCode",$x);
			$array_anno[$cont_anno]["value"] = $obj_Datos_Api_anno->ValorF("sessions",$x);	
			$porc = round( porcentaje_sesiones_res($obj_Datos_Api_anno->Valor("sessions",$x),$obj_Datos_Api_anno->Total("sessions")) ); 
			$array_anno[$cont_anno]["porcentaje"] = $porc;

			if($cont_anno < $top){
				$otrosanno += $obj_Datos_Api_anno->Valor("sessions",$x);
			}



		}else{
			$array_anno[$cont_anno]["indetificador"] = $obj_Datos_Api_anno->Valor($strDimensionsN,$x);
			$array_anno[$cont_anno]["value"] = $obj_Datos_Api_anno->Valor("sessions",$x);

			if($cont_anno < $top){
				$otrosanno += $obj_Datos_Api_anno->Valor("sessions",$x);
			}

		}

		$cont_anno++;
		

	}
	//Fin anno
	$total_anno = $obj_Datos_Api_anno->total("sessions");
	$otros_anno = $total_anno - $otrosanno;



	$count = 0;
	$otros = 0;
	//Objeto creado ---------------------------------------------------------------------------	
	$strA = "";
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		if($count < $top){
			$strA .= "<tr>";

			//El primer valor va a depender de la perspectiva que estamos pintando
			if($strTipo=="pais"){
				$strA .= "<td><span class='negro'>".Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x))."</span></td>";
			}else{
				$strA .= "<td><span class='negro'>".Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api)."</span></td>";
			}


			//Comparamos si tiene comparación
			if($strTipo=="pais"){

				//ARRAY EN BUSCA DEL AÑO
				$dat = 0;
				foreach ($array_anno as $key => $anno) {
					if( $anno["indetificador"] == $obj_Datos_Api->Valor("countryIsoCode",$x) ){
						$dat = $anno["value"];
						$dat_porc = $anno["porcentaje"];
					}
				}

				$strA .= "<td>".DevolverContenido_Total($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api_ant->Valor("sessions",$x),$obj_Datos_Api->ValorF("sessions",$x),$obj_Datos_Api_ant->ValorF("sessions",$x)).' '.$obj_Datos_Api->ValorF("sessions",$x).'<span class="porcentagest"> ('.round( porcentaje_sesiones_res($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")) ).'%) </span>'."</td><td>".$dat."<span class='porcentagest'> (".$dat_porc."%) </span></td>";
			}else{

				//ARRAY EN BUSCA DEL AÑO
				$dat = 0;
				foreach ($array_anno as $key => $anno) {
					if( $anno["indetificador"] == $obj_Datos_Api->Valor($strDimensionsN,$x) ){
						$dat = $anno["value"];
					}
				}

				$strA .= "<td>".DevolverContenido_Total($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api_ant->Valor("sessions",$x),$obj_Datos_Api->ValorF("sessions",$x),$obj_Datos_Api_ant->ValorF("sessions",$x)).' '.round( porcentaje_sesiones_res($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")) ).'%'."</td><td>".round( porcentaje_sesiones_res($dat,$total_anno) ).'%'."</td>";
			}

			$strA .= "</tr>"; 	
			$count++;
		}else{

			//Sumamos otros
			$otros += $obj_Datos_Api->Valor("sessions",$x);
			$otros_ant += $obj_Datos_Api_ant->Valor("sessions",$x);
			$count++;

		}
	}

	if($strTipo=="pais"){
		if($count >= $top){
			$strA .= "<td><span class='negro'>" . $trans->__('Otros', false) . "</span></td><td>".DevolverContenido_Total($otros,$otros_ant,$otros,$otros_ant).' '.number_format($otros,0,',','.').'<span class="porcentagest"> ('.round( porcentaje_sesiones_res($otros,$obj_Datos_Api->Total("sessions")) ).'%)'." </span></td><td>".number_format($otros_anno,0,',','.').'<span class="porcentagest"> ('.round( porcentaje_sesiones_res($otros_anno,$obj_Datos_Api_anno->Total("sessions")) ).'%) </span></td>';
		
		}
	}

	//Devolvemos el json
	return $strA;
}  

function resumen_datatable_pais_res ($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro )
{
	$filtro_and="";
	//$strSort='yearWeek';
	$strDimensions='ga:country,ga:countryIsoCode';
	$strDimensionsN ='country'; 
	$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
	$array_Ordenacion = "-ga:sessions";

	$filtro="";	

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
	if(!empty($filtro_and)){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $filtro_and;
	}
	
	$DataTables_start  = 0;
	$DataTables_length = 1000;
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
	                                            'sort' => $array_Ordenacion,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
	                                            'sort' => $array_Ordenacion,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
    $obj_Datos_Api -> Construccion();
	
	$anno = substr($datFechaInicioFiltro, 0, 4);
    $fechaini = $anno."-01-01";
    $fechafin = $anno."-12-31";

    //Creación obj_annoeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_anno = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_anno -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_anno -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_anno -> startdate            = $fechaini;
    $obj_Datos_Api_anno -> enddate              = $fechafin;
    $obj_Datos_Api_anno -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
	                                            'sort' => $array_Ordenacion,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api_anno -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
	                                            'sort' => $array_Ordenacion,
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
    $obj_Datos_Api_anno -> Construccion();


    
    $count = 0;



	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		if ($count < 3) {
			$strA .= '<tr>';
			//El primer valor va a depender de la perspectiva que estamos pintando
			$annodat = 0;
			for ($i=1; $i<=$obj_Datos_Api_anno->NumValores(); $i++) {
				if($obj_Datos_Api_anno->Valor("country",$i) == $obj_Datos_Api->Valor("country",$x)){

					$annodat = porcentaje_sesiones($obj_Datos_Api_anno->Valor("sessions",$i),$obj_Datos_Api_anno->Total("sessions",$i));

				}
			}

			$strA .= "<td>".Formato_Perspectiva('table',"PAIS_SIN",$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x))."</td>";
			$strA .= '<td>'.round( porcentaje_sesiones_res($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")) ).'%</td><td>'.round( $annodat ).'% </td>';

			
			
			$strA .= '</tr>';	  
			$count++;
		}

	}

	
	//Devolvemos el json
	return $strA;
}  

function fuentes_extra($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro, $strApp){

	$DataTables_start = 0;
	$DataTables_length = 20000;
	//Construimos la llamada a la api
	$obj_Datos_Api_admin = new Datos_Api_Admin(); // Instanciamos la clase Datos_Api_Admin
    $obj_Datos_Api_admin -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_admin -> idVista              = $idVistaAnalytics ;
    $obj_Datos_Api_admin -> TipoLlamada          = 'GOALS_VISTA' ;   
    $obj_Datos_Api_admin -> Construccion();

    if($strApp == 0){
    	$goalapp = 1;
    }else{
    	$goalapp = 0;
    }

    $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_METRICAS_SESIONES($goalapp);
	$ARRAY_GOALS_VISTA_ORDENADOS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS();


	//Hacemos tantas llamadas al api de datos como elementos tenga el array de métricas.  En cada llamada sacaremos cinco objetivos (10 méticas)
	$array_Datos_Api = array();

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= $strFiltro;
	}
	//$p="";
	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS); $intI++) {

		$ses = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI];

		//print($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI]);
	    //Creación objeto, parametrización -------------------------------------------------------
	    $array_Datos_Api[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $array_Datos_Api[$intI] -> strProyectoAsociado  = $strProyectoAsociado ;
	    $array_Datos_Api[$intI] -> idVista              = 'ga:'. $idVistaAnalytics ;
	    $array_Datos_Api[$intI] -> startdate            = $datFechaInicioFiltro ;
	    $array_Datos_Api[$intI] -> enddate              = $datFechaFinFiltro ;
	    $array_Datos_Api[$intI] -> metrics              = $ses;

		if(empty($filtro)){
	    	$array_Datos_Api[$intI] -> optParams            = array(
															'dimensions' => 'ga:campaign,ga:medium,ga:socialNetwork,ga:source',
															//'sort' => '-ga:sessions',
															'start-index' => $DataTables_start + 1,
				                                            'max-results' => $DataTables_length);  
	    }else{
	    	$array_Datos_Api[$intI] -> optParams            = array(
															'dimensions' => 'ga:campaign,ga:medium,ga:socialNetwork,ga:source',
															'filters' => $filtro,
															//'sort' => '-ga:sessions',
															'start-index' => $DataTables_start + 1,
				                                            'max-results' => $DataTables_length);  
	    }
	    $array_Datos_Api[$intI] -> Construccion();
		
		

	}//Llamadas a la api
	$p = $ARRAY_GOALS_VISTA_ORDENADOS;

	//Inicializamos las variables contedores
	$enlaces_sesiones_total = 0;
	$cont_enlaces_tr        = 0;
	$cont_buscadores_tr     = 0;
	$cont_social_tr         = 0;
	$cont_campanas_tr       = 0;
	$cont_directo_tr        = 0;
	$fila 			        = 0;
	$cont_enlaces_ts        = 0;
	$cont_buscadores_ts     = 0;
	$cont_social_ts         = 0;
	$cont_campanas_ts       = 0;
	$cont_directo_ts        = 0;
	$cont_enlaces_ps        = 0;
	$cont_buscadores_ps     = 0;
	$cont_social_ps         = 0;
	$cont_campanas_ps       = 0;
	$cont_directo_ps        = 0;
	$objetivos_enlaces    = array();
	$objetivos_buscadores = array();
	$objetivos_social     = array();
	$objetivos_campanas   = array();
	$objetivos_directo    = array();
	//recorremos todos los datos de la api

	for ($x=1; $x<=$array_Datos_Api[0]->NumValores(); $x++) {
		$intI=0;

		//Aqui sumamos todas las sesiones para sacar el porcentaje
		$sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);
		//****

		//Para enlaces (ga:medium==referral;ga:SocialNetwork==(not set))
		if ($array_Datos_Api[$intI]->Valor("medium",$x) == "referral" && $array_Datos_Api[$intI]->Valor("SocialNetwork",$x) == "(not set)") {
			//$p =$array_Datos_Api[$intI]->Valor("source",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$enlaces_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);

			//Tasa rebote
			$enlaces_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_enlaces_tr++;
			}

			//Tiempo sesión
			$enlaces_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_enlaces_ts++;
			}

			//Páginas sesión
			$enlaces_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_enlaces_ps++;
			}
			
		}
		//Para busqueda ga:medium==organic;ga:SocialNetwork==(not set)
		if ($array_Datos_Api[$intI]->Valor("medium",$x) == "organic" && $array_Datos_Api[$intI]->Valor("SocialNetwork",$x) == "(not set)") {
			//$p = $array_Datos_Api[$intI]->Valor("keyword",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$buscadores_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);

			//Tasa rebote
			$buscadores_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_buscadores_tr++;
			}

			//Tiempo sesión
			$buscadores_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_buscadores_ts++;
			}

			//Páginas sesión
			$buscadores_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_buscadores_ps++;
			}
		
		}
		//Para social ga:SocialNetwork==(not set)
		if ($array_Datos_Api[$intI]->Valor("SocialNetwork",$x) != "(not set)" && $array_Datos_Api[$intI]->Valor("campaign",$x) == "(not set)") {
			//$pruebac .=$array_Datos_Api[$intI]->Valor("socialNetwork",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$social_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);

			//Tasa rebote
			$social_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_social_tr++;
			}

			//Tiempo sesión
			$social_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_social_ts++;
			}

			//Páginas sesión
			$social_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_social_ps++;
			}
			
		}
		//Para campañas ga:medium!=referral;ga:medium!=organic;ga:medium!=(none); ga:SocialNetwork==(not set)
		if ($array_Datos_Api[$intI]->Valor("medium",$x) != "referral" && $array_Datos_Api[$intI]->Valor("medium",$x) != "organic" && $array_Datos_Api[$intI]->Valor("medium",$x) != "(none)") {
			//$pruebad .=$array_Datos_Api[$intI]->Valor("socialNetwork",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$campanas_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);
			
			//Tasa rebote
			$campanas_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_campanas_tr++;
			}

			//Tiempo sesión
			$campanas_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_campanas_ts++;
			}

			//Páginas sesión
			$campanas_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_campanass_ps++;
			}
		}
		//Para directo ga:medium==(none)
		if ($array_Datos_Api[$intI]->Valor("medium",$x) == "(none)") {
			//$pruebae .=$array_Datos_Api[$intI]->Valor("source",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$directo_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);
			
			//Tasa rebote
			$directo_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_directo_tr++;
			}

			//Tiempo sesión
			$directo_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_directo_ts++;
			}

			//Páginas sesión
			$directo_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_directo_ps++;
			}
		}

	}

	$tot_ordenados = count($ARRAY_GOALS_VISTA_ORDENADOS);
	$tot_ordenados_m = count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS);

	if($tot_ordenados_m > 2){
		$vt=0;
		while ($tot_ordenados > 5) {
			$tot_ordenados = $tot_ordenados-5;
			$vt++;
		}

	}else if($tot_ordenados_m == 2){
		$vt=2;
		$tot_ordenados = $tot_ordenados;

	}else if($tot_ordenados_m < 2){
		$vt=1;
	}

	$total_total_conversiones = 0;

	for ($x=1; $x<=$array_Datos_Api[0]->NumValores(); $x++) {

		$vuelta=0;
		if($vt==1){

			//Para enlaces
			for ($j=0; $j < 1; $j++) { 		

				for ($i=10; $i < 10+$tot_ordenados; $i++) { 

					$id_obj = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["id"];
					
					//Para enlaces
					if ($array_Datos_Api[0]->Valor("medium",$x) == "referral" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
				
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {

							$objetivos_enlaces[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
							$objetivos_enlaces[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);							
							$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
							$jma += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
						}


					}	

						
					//Buscadores
					if ($array_Datos_Api[0]->Valor("medium",$x) == "organic" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
					
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
	
							$objetivos_buscadores[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
							$objetivos_buscadores[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
							$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
							
						}
					
					}
	
					//Social
					if ($array_Datos_Api[0]->Valor("SocialNetwork",$x) != "(not set)"  && $array_Datos_Api[0]->Valor("campaign",$x) == "(not set)") {
	
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
	
							$objetivos_social[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
							$objetivos_social[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
							$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

						}
					
					}
	
					//Campañas
					if ($array_Datos_Api[0]->Valor("medium",$x) != "referral" && $array_Datos_Api[0]->Valor("medium",$x) != "organic" && $array_Datos_Api[0]->Valor("medium",$x) != "(none)") {
									
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
	
								$objetivos_campanas[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
								$objetivos_campanas[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
								$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

						}
					
					}	
	
					//Directo
					if ($array_Datos_Api[0]->Valor("medium",$x) == "(none)") {
					
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
	
								$objetivos_directo[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
								$objetivos_directo[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
								$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

						}
					
					}

				}//Segundo for

			}//Primer for	

		}else{
			//vt
			//columna 
			for ($j=0; $j < $tot_ordenados_m; $j++) { 
				
				if($j < 2){
					//echo $tot_ordenados."  -  ";
					//mitad una
					if($vuelta==0){
						for ($i=0; $i < 5; $i++) { 


							$id_obj = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["id"];
					
							//Para enlaces
							if ($array_Datos_Api[0]->Valor("medium",$x) == "referral" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
						
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {

									$objetivos_enlaces[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_enlaces[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);							
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$jma += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
								}

							}		

							//Buscadores
							if ($array_Datos_Api[0]->Valor("medium",$x) == "organic" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_buscadores[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_buscadores[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									
								}
							
							}
			
							//Social
							if ($array_Datos_Api[0]->Valor("SocialNetwork",$x) != "(not set)" && $array_Datos_Api[0]->Valor("campaign",$x) == "(not set)") {
			
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_social[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_social[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}
			
							//Campañas
							if ($array_Datos_Api[0]->Valor("medium",$x) != "referral" && $array_Datos_Api[0]->Valor("medium",$x) != "organic" && $array_Datos_Api[0]->Valor("medium",$x) != "(none)") {
											
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_campanas[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_campanas[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}	
			
							//Directo
							if ($array_Datos_Api[0]->Valor("medium",$x) == "(none)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_directo[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_directo[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}

						}
						$vuelta++;
					//mitad 2
					}else{
						$desde = $tot_ordenados/2;
						for ($i=5; $i < $tot_ordenados; $i++) { 

							$id_obj = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["id"];
					
							//Para enlaces
							if ($array_Datos_Api[0]->Valor("medium",$x) == "referral" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
						
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {

									$objetivos_enlaces[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_enlaces[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);							
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$jma += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
								}

							}		

							//Buscadores
							if ($array_Datos_Api[0]->Valor("medium",$x) == "organic" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_buscadores[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_buscadores[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									
								}
							
							}
			
							//Social
							if ($array_Datos_Api[0]->Valor("SocialNetwork",$x) != "(not set)" && $array_Datos_Api[0]->Valor("campaign",$x) == "(not set)") {
			
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_social[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_social[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}
			
							//Campañas
							if ($array_Datos_Api[0]->Valor("medium",$x) != "referral" && $array_Datos_Api[0]->Valor("medium",$x) != "organic" && $array_Datos_Api[0]->Valor("medium",$x) != "(none)") {
											
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_campanas[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_campanas[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}	
			
							//Directo
							if ($array_Datos_Api[0]->Valor("medium",$x) == "(none)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_directo[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_directo[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}

								}
								$vuelta==0;
							}

				}else{

					if($tot_ordenados_m > 2){
						//otros

						for ($i=10; $i < 10+$tot_ordenados; $i++) { 
												
							$id_obj = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["id"];
					
							//Para enlaces
							if ($array_Datos_Api[0]->Valor("medium",$x) == "referral" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
						
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {

									$objetivos_enlaces[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_enlaces[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);							
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$jma += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
								}

							}		

							//Buscadores
							if ($array_Datos_Api[0]->Valor("medium",$x) == "organic" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_buscadores[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_buscadores[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									
								}
							
							}
			
							//Social
							if ($array_Datos_Api[0]->Valor("SocialNetwork",$x) != "(not set)" && $array_Datos_Api[$intI]->Valor("campaign",$x) == "(not set)") {
			
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_social[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_social[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}
			
							//Campañas
							if ($array_Datos_Api[0]->Valor("medium",$x) != "referral" && $array_Datos_Api[0]->Valor("medium",$x) != "organic" && $array_Datos_Api[0]->Valor("medium",$x) != "(none)") {
											
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_campanas[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_campanas[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}	
			
							//Directo
							if ($array_Datos_Api[0]->Valor("medium",$x) == "(none)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_directo[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_directo[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}
						}

					}

				}//else j

		}//elsevt	


	}//for
		}	

	arsort($objetivos_enlaces);
	arsort($objetivos_buscadores);
	arsort($objetivos_social);
	arsort($objetivos_campanas);
	arsort($objetivos_directo);
	foreach ($objetivos_enlaces as $key => $enl) {
		$convertot[$key] += $enl["val"];
	}
	foreach ($objetivos_buscadores as $key => $enl) {
		$convertot[$key] += $enl["val"];
	}
	foreach ($objetivos_social as $key => $enl) {
		$convertot[$key] += $enl["val"];
	}
	foreach ($objetivos_campanas as $key => $enl) {
		$convertot[$key] += $enl["val"];
	}
	foreach ($objetivos_directo as $key => $enl) {
		$convertot[$key] += $enl["val"];
	}
	
	//return $convertot;

	//Calcular sesiones
	if($enlaces_sesiones_total != ""){
		$enlaces_sesiones_total_graf = $enlaces_sesiones_total;
		$enlaces_sesiones_total_img = number_format($enlaces_sesiones_total,0,',','.')."(".porcentaje_sesiones($enlaces_sesiones_total,$sesiones_total)."%)";
		$enlaces_sesiones_total = number_format($enlaces_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($enlaces_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$enlaces_sesiones_total_graf = 0;
		$enlaces_sesiones_total_img = "0 (0,00%)";	
		$enlaces_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";	
	}

	if($buscadores_sesiones_total != ""){
		$buscadores_sesiones_total_graf = $buscadores_sesiones_total;
		$buscadores_sesiones_total_img = number_format($buscadores_sesiones_total,0,',','.')."(".porcentaje_sesiones($buscadores_sesiones_total,$sesiones_total)."%)";
		$buscadores_sesiones_total = number_format($buscadores_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($buscadores_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$buscadores_sesiones_total_graf = 0;
		$buscadores_sesiones_total_img = "0 (0,00%)";	
		$buscadores_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";	
	}

	if($social_sesiones_total != ""){
		$social_sesiones_total_graf = $social_sesiones_total;
		$social_sesiones_total_img = number_format($social_sesiones_total,0,',','.')." (".porcentaje_sesiones($social_sesiones_total,$sesiones_total)."%)";
		$social_sesiones_total = number_format($social_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($social_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$social_sesiones_total_graf = 0;
		$social_sesiones_total_img = "0 (0,00%)>";	
		$social_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";	
	}

	if($campanas_sesiones_total != ""){
		$campanas_sesiones_total_graf = $campanas_sesiones_total;
		$campanas_sesiones_total_img = number_format($campanas_sesiones_total,0,',','.')."(".porcentaje_sesiones($campanas_sesiones_total,$sesiones_total)."%)";
		$campanas_sesiones_total = number_format($campanas_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($campanas_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$campanas_sesiones_total_graf = 0;
		$campanas_sesiones_total_img = "0 (0,00%)";
		$campanas_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";
	}

	if($directo_sesiones_total != ""){
		$directo_sesiones_total_graf = $directo_sesiones_total;
		$directo_sesiones_total_img = number_format($directo_sesiones_total,0,',','.')." (".porcentaje_sesiones($directo_sesiones_total,$sesiones_total)."%)";
		$directo_sesiones_total = number_format($directo_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($directo_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$directo_sesiones_total_graf = 0;
		$directo_sesiones_total_img = "0 (0,00%)";
		$directo_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";
	}


	//Calcular tasarebote
	if($cont_enlaces_tr!=0){
		$enlaces_tasarebote_total_graf = $enlaces_tasarebote_total;
		$enlaces_tasarebote_total = number_format(($enlaces_tasarebote_total/$cont_enlaces_tr),2,',','.');
	}else{
		$$enlaces_tasarebote_total_graf = 0;
		$enlaces_tasarebote_total = "0.0";
	}

	if($cont_buscadores_tr!=0){
		$buscadores_tasarebote_total_graf = $buscadores_tasarebote_total;
		$buscadores_tasarebote_total = number_format(($buscadores_tasarebote_total/$cont_buscadores_tr),2,',','.');
	}else{
		$buscadores_tasarebote_total_graf = 0;
		$buscadores_tasarebote_total = "0.0";
	}

	if($cont_social_tr!=0){
		$social_tasarebote_total_graf = $social_tasarebote_total;
		$social_tasarebote_total = number_format(($social_tasarebote_total/$cont_social_tr),2,',','.');
	}else{
		$social_tasarebote_total_graf = 0;
		$social_tasarebote_total = "0.0";
	}

	if($cont_campanas_tr!=0){
		$campanas_tasarebote_total_graf = $campanas_tasarebote_total;
		$campanas_tasarebote_total = number_format(($campanas_tasarebote_total/$cont_campanas_tr),2,',','.');
	}else{
		$campanas_tasarebote_total_graf = 0;
		$campanas_tasarebote_total = "0.0";
	}

	if($cont_directo_tr!=0){
		$directo_tasarebote_total_graf = $directo_tasarebote_total;
		$directo_tasarebote_total = number_format(($directo_tasarebote_total/$cont_directo_tr),2,',','.');
	}else{
		$directo_tasarebote_total_graf = 0;
		$directo_tasarebote_total = "0.0";
	}

	//Calcular tiemposesion
	if($cont_enlaces_ts!=0){
		$enlaces_tsesion_total_graf = $enlaces_tsesion_total/$cont_enlaces_ts;
		$enlaces_tsesion_total = segundos_tiempo($enlaces_tsesion_total/$cont_enlaces_ts);
	}else{
		$enlaces_tsesion_total_graf = 0;
		$enlaces_tsesion_total = "0.00 min";
	}
	if($cont_buscadores_ts!=0){
		$buscadores_tsesion_total = $buscadores_tsesion_total/$cont_buscadores_ts;
		$buscadores_tsesion_total = segundos_tiempo($buscadores_tsesion_total/$cont_buscadores_ts);
	}else{
		$buscadores_tsesion_total = 0;
		$buscadores_tsesion_total = "0.00 min";
	}
	if($cont_social_ts!=0){
		$social_tsesion_total_graf = $social_tsesion_total/$cont_social_ts;
		$social_tsesion_total = segundos_tiempo($social_tsesion_total/$cont_social_ts);
	}else{
		$social_tsesion_total_graf = 0;
		$social_tsesion_total = "0.00 min";
	}
	if($cont_campanas_ts!=0){
		$campanas_tsesion_total_graf = $campanas_tsesion_total/$cont_campanas_ts;
		$campanas_tsesion_total = segundos_tiempo($campanas_tsesion_total/$cont_campanas_ts);
	}else{
		$campanas_tsesion_total_graf = 0;
		$campanas_tsesion_total = "0.00 min";
	}
	if($cont_directo_ts!=0){
		$directo_tsesion_total_graf = $directo_tsesion_total/$cont_directo_ts;
		$directo_tsesion_total = segundos_tiempo($directo_tsesion_total/$cont_directo_ts);
	}else{
		$directo_tsesion_total_graf = 0;
		$directo_tsesion_total = "0.00 min";
	}

	//Calcular paginas/sesion
	if($cont_enlaces_ps!=0){
		$enlaces_pagsesion_total_graf = $enlaces_pagsesion_total/$cont_enlaces_ps;
		$enlaces_pagsesion_total = number_format(($enlaces_pagsesion_total/$cont_enlaces_ps),2,',','.');
	}else{
		$enlaces_pagsesion_total_graf = 0;
		$enlaces_pagsesion_total = "0";
	}
	if($cont_buscadores_ps!=0){
		$buscadores_pagsesion_total_graf = $buscadores_pagsesion_total/$cont_buscadores_ps;
		$buscadores_pagsesion_total = number_format(($buscadores_pagsesion_total/$cont_buscadores_ps),2,',','.');
	}else{
		$buscadores_pagsesion_total_graf = 0;
		$buscadores_pagsesion_total = "0";
	}
	if($cont_social_ps!=0){
		$social_pagsesion_total_graf = $social_pagsesion_total/$cont_social_ps;
		$social_pagsesion_total = number_format(($social_pagsesion_total/$cont_social_ps),2,',','.');
	}else{
		$social_pagsesion_total_graf = 0;
		$social_pagsesion_total = "0";
	}
	if($cont_campanas_ps!=0){
		$campanas_pagsesion_total_graf = $campanas_pagsesion_total/$cont_campanas_ps;
		$campanas_pagsesion_total = number_format(($campanas_pagsesion_total/$cont_campanas_ps),2,',','.');
	}else{
		$campanas_pagsesion_total_graf = 0;
		$campanas_pagsesion_total = "0";
	}
	if($cont_directo_ps!=0){
		$directo_pagsesion_total_graf = $directo_pagsesion_total/$cont_directo_ps;
		$directo_pagsesion_total = number_format(($directo_pagsesion_total/$cont_directo_ps),2,',','.');
	}else{
		$directo_pagsesion_total_graf = 0;
		$directo_pagsesion_total = "0";
	}

	//Buscamos totales para porcentage de fila de conversiones

	$max_count = count($objetivos_enlaces);
	if(count($objetivos_buscadores)>$max_count){$max_count = count($objetivos_buscadores);}
	if(count($objetivos_social)>$max_count){$max_count = count($objetivos_social);}
	if(count($objetivos_campanas)>$max_count){$max_count = count($objetivos_campanas);}
	if(count($objetivos_directo)>$max_count){$max_count = count($objetivos_directo);}

	
	//Montamos el json para traer la array de objetivos
	$charttits .= "['Conversiones',";
	$titscol = array();
	$ff=0;
	$jm=0;
	//Enlaces
	if(!empty($objetivos_enlaces)){
		$objson_enlaces = "[";

		foreach ($objetivos_enlaces as $key => $obj_enlaces) {
			
			$objson_enlaces .=  "[\"nombre\",\"".$obj_enlaces["nombre"]."\"],";
			if($obj_enlaces["val"] == ""){ $n = 0; }else{ $n = $obj_enlaces["val"];}
			$enlaces_value = $obj_enlaces["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_enlaces["val"],$convertot[$key] )."%)</span>";

			$objson_enlaces .=  "[\"valor\",\"".$enlaces_value."\"],";		

			if (in_array($obj_enlaces["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_enlaces["nombre"]."',";
				$titscol[$ff] = $obj_enlaces["nombre"];
				$ff++;
			}
			$jm++;
		}
		$objson_enlaces = substr($objson_enlaces, 0, -1);
		$objson_enlaces .= "]";

	}else{
		$objson_enlaces .= "\"\"";
	}

	$jm=0;
	//buscadores
	if(!empty($objetivos_buscadores)){
		$objson_buscadores = "[";
		foreach ($objetivos_buscadores as $key => $obj_buscadores) {
			
			$objson_buscadores .=  "[\"nombre\",\"".$obj_buscadores["nombre"]."\"],";
			if($obj_buscadores["val"] == ""){ $n = 0; }else{ $n = $obj_buscadores["val"];}
			$buscadores_value = $obj_buscadores["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_buscadores["val"],$convertot[$key]  )."%)</span>";
			$objson_buscadores .=  "[\"valor\",\"".$buscadores_value."\"],";
			
			if (in_array($obj_buscadores["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_buscadores["nombre"]."',";
				$titscol[$ff] = $obj_buscadores["nombre"];
				$ff++;
			}
			$jm++;
		}
		$objson_buscadores = substr($objson_buscadores, 0, -1);
		$objson_buscadores .= "]";
	}else{
		$objson_buscadores .= "\"\"";
	}


	$jm=0;
	//social
	if(!empty($objetivos_social)){
		$objson_social = "[";
		foreach ($objetivos_social as $key => $obj_social) {
			
			$objson_social .=  "[\"nombre\",\"".$obj_social["nombre"]."\"],";
			if($objson_social["val"] == ""){ $n = 0; }else{ $n = $objson_social["val"];}
			$social_value = $obj_social["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_social["val"],$convertot[$key]  )."%)</span>";
			$objson_social .=  "[\"valor\",\"".$social_value."\"],";
			
			if (in_array($obj_social["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_social["nombre"]."',";
				$titscol[$ff] = $obj_social["nombre"];
				$ff++;
			}
			$jm++;
		}
		$objson_social = substr($objson_social, 0, -1);
		$objson_social .= "]";

	}else{
		$objson_social .= "\"\"";
	}	
	
	$jm=0;
	//Campañas	
	if(!empty($objetivos_campanas)){
		$objson_campanas = "[";
		foreach ($objetivos_campanas as $key => $obj_campanas) {
			
			$objson_campanas .=  "[\"nombre\",\"".$obj_campanas["nombre"]."\"],";
			if($objson_campanas["val"] == ""){ $n = 0; }else{ $n = $objson_campanas["val"];}
			$campanas_value = $obj_campanas["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_campanas["val"],$convertot[$key]  )."%)</span>";
			$objson_campanas .=  "[\"valor\",\"".$campanas_value."\"],";
			
			if (in_array($obj_campanas["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_campanas["nombre"]."',";
				$titscol[$ff] = $obj_campanas["nombre"];
				$ff++;
			}
			$jm++;
		}
		$objson_campanas = substr($objson_campanas, 0, -1);
		$objson_campanas .= "]";
	}else{
		$objson_campanas .= "\"\"";
	}
	
	
	$jm=0;
	//Directo
	if(!empty($objetivos_directo)){
		$objson_directo = "[";
		foreach ($objetivos_directo as $key => $obj_directo) {
			
			$objson_directo .=  "[\"nombre\",\"".$obj_directo["nombre"]."\"],";
			if($objetivos_directo["val"] == ""){ $n = 0; }else{ $n = $objetivos_directo["val"];}
			$directo_value = $obj_directo["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_directo["val"],$convertot[$key]  )."%)</span>";
			$objson_directo .=  "[\"valor\",\"".$directo_value."\"],";
			
			if (in_array($obj_directo["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_directo["nombre"]."',";
				$titscol[$ff] = $obj_directo["nombre"];
				$ff++;
			}
			$jm++;
		}
		$objson_directo = substr($objson_directo, 0, -1);
		$objson_directo .= "]";
	}else{
		$objson_directo .= "\"\"";
	}

	$charttits = substr($charttits, 0, -1);
	$charttits .= "]";



	$conversiones_enlaces_total    = 0;
	$conversiones_buscadores_total = 0;
	$conversiones_social_total     = 0;
	$conversiones_campanas_total   = 0;
	$conversiones_directo_total    = 0;

	//Ahora que sabemos el total de conversiones recorremos los vlores para el gráfico de columnas
	$chartval  .= "";
	$count_col = count($titscol);
	$titscol = array();
	$nvals = 0;

	

	//Enlaces
	//if(!empty($objetivos_enlaces)){

		$chartval .= "['',";
		foreach ($objetivos_enlaces as $key => $obj_enlaces) {
			if (in_array($obj_enlaces["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_enlaces_total += $obj_enlaces["val"];
				$chartval .= "".$obj_enlaces["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;

		//Que pintamos
		$conversiones_enlaces_total = $conversiones_enlaces_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_enlaces_total,$total_total_conversiones )."%)</span>";
	//}

	//Buscadores
	//if(!empty($objetivos_buscadores)){

		$chartval .= "['',";
		foreach ($objetivos_buscadores as $key => $obj_buscadores) {
			if (in_array($obj_buscadores["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_buscadores_total += $obj_buscadores["val"];
				$chartval .= "".$obj_buscadores["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;
		$conversiones_buscadores_total = $conversiones_buscadores_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_buscadores_total,$total_total_conversiones )."%)</span>";

	//}

	//Social
	//if(!empty($objetivos_social)){

		$chartval .= "['',";
		foreach ($objetivos_social as $key => $obj_social) {
			if (in_array($obj_social["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_social_total += $obj_social["val"];
				$chartval .= "".$obj_social["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;
		$conversiones_social_total = $conversiones_social_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_social_total,$total_total_conversiones )."%)</span>";

	//}

	//Campañas
	//if(!empty($objetivos_campanas)){

		$chartval .= "['',";
		foreach ($objetivos_campanas as $key => $obj_campanas) {
			if (in_array($obj_campanas["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_campanas_total += $obj_campanas["val"];
				$chartval .= "".$obj_campanas["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;
		$conversiones_campanas_total = $conversiones_campanas_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_campanas_total,$total_total_conversiones )."%)</span>";
	//}

	//Directo
	//if(!empty($objetivos_directo)){

		$chartval .= "['',";
		foreach ($objetivos_directo as $key => $obj_directo) {
			if (in_array($obj_directo["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_directo_total += $obj_directo["val"];
				$chartval .= "".$obj_directo["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;
		$conversiones_directo_total = $conversiones_directo_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_directo_total,$total_total_conversiones )."%)</span>";
	//}

	$chartval = substr($chartval, 0, -1);

	$chartc = "[";
	$chartc .= $charttits.",";
	$chartc .= $chartval;
	$chartc .= "]";


	//Montamos el json los graficos de tarta
    $strB = "[";
    $strB .= "['Fuente', 'Sesiones'],";
    $strB .= "['Enlaces', ".$enlaces_sesiones_total_graf."],";
    $strB .= "['Búsquedas', ".$buscadores_sesiones_total_graf."],";
    $strB .= "['Social', ".$social_sesiones_total_graf."],";
    $strB .= "['Campañas', ".$campanas_sesiones_total_graf."],";
    $strB .= "['Directo', ".$directo_sesiones_total_graf."]";
    $strB .= "]";


	//Montamos el Json para
	$strA = '{';
	//Sesiones
		//Formateado
		$strA .= '"enlaces_sesiones_total": "' . $enlaces_sesiones_total . '",';	
		$strA .= '"enlaces_sesiones_total_img": "' . $enlaces_sesiones_total_img . '",';	
		$strA .= '"buscadores_sesiones_total": "' . $buscadores_sesiones_total . '",';	
		$strA .= '"buscadores_sesiones_total_img": "' . $buscadores_sesiones_total_img . '",';	
		$strA .= '"social_sesiones_total": "' . $social_sesiones_total . '",';	
		$strA .= '"social_sesiones_total_img": "' . $social_sesiones_total_img . '",';	
		$strA .= '"campanas_sesiones_total": "' . $campanas_sesiones_total . '",';	
		$strA .= '"campanas_sesiones_total_img": "' . $campanas_sesiones_total_img . '",';	
		$strA .= '"directo_sesiones_total": "' . $directo_sesiones_total . '",';
		$strA .= '"directo_sesiones_total_img": "' . $directo_sesiones_total_img . '",';
		//Sin formatear
		$strA .= '"enlaces_sesiones_total_comp": "' . $enlaces_sesiones_total_graf . '",';	
		$strA .= '"buscadores_sesiones_total_comp": "' . $buscadores_sesiones_total_graf . '",';	
		$strA .= '"social_sesiones_total_comp": "' . $social_sesiones_total_graf . '",';	
		$strA .= '"campanas_sesiones_total_comp": "' . $campanas_sesiones_total_graf . '",';	
		$strA .= '"directo_sesiones_total_comp": "' . $directo_sesiones_total_graf . '",';	

	//tasa de rebote
		//Formateado
		$strA .= '"enlaces_tasarebote_total": "' . $enlaces_tasarebote_total . '%",';	
		$strA .= '"buscadores_tasarebote_total": "' . $buscadores_tasarebote_total . '%",';	
		$strA .= '"social_tasarebote_total": "' . $social_tasarebote_total . '%",';	
		$strA .= '"campanas_tasarebote_total": "' . $campanas_tasarebote_total . '%",';	
		$strA .= '"directo_tasarebote_total": "' . $directo_tasarebote_total . '%",';
		//Sin formatear
		$strA .= '"enlaces_tasarebote_total_comp": "' . $enlaces_tasarebote_total_graf . '",';	
		$strA .= '"buscadores_tasarebote_total_comp": "' . $buscadores_tasarebote_total_graf . '",';	
		$strA .= '"social_tasarebote_total_comp": "' . $social_tasarebote_total_graf . '",';	
		$strA .= '"campanas_tasarebote_total_comp": "' . $campanas_tasarebote_total_graf . '",';	
		$strA .= '"directo_tasarebote_total_comp": "' . $directo_tasarebote_total_graf . '",';

	//Tiempo sesion
		//Formateado
		$strA .= '"enlaces_tsesion_total": "' . $enlaces_tsesion_total . '",';	
		$strA .= '"buscadores_tsesion_total": "' . $buscadores_tsesion_total . '",';	
		$strA .= '"social_tsesion_total": "' . $social_tsesion_total . '",';	
		$strA .= '"campanas_tsesion_total": "' . $campanas_tsesion_total . '",';	
		$strA .= '"directo_tsesion_total": "' . $directo_tsesion_total . '",';	
		//Sin formatear
		$strA .= '"enlaces_tsesion_total_comp": "' . $enlaces_tsesion_total_graf . '",';	
		$strA .= '"buscadores_tsesion_total_comp": "' . $buscadores_tsesion_total_graf . '",';	
		$strA .= '"social_tsesion_total_comp": "' . $social_tsesion_total_graf . '",';	
		$strA .= '"campanas_tsesion_total_comp": "' . $campanas_tsesion_total_graf . '",';	
		$strA .= '"directo_tsesion_total_comp": "' . $directo_tsesion_total_graf . '",';	
	
	//Pag/Sesión
		//Formateado
		$strA .= '"enlaces_pagsesion_total": "' . $enlaces_pagsesion_total . '",';	
		$strA .= '"buscadores_pagsesion_total": "' . $buscadores_pagsesion_total . '",';	
		$strA .= '"social_pagsesion_total": "' . $social_pagsesion_total . '",';	
		$strA .= '"campanas_pagsesion_total": "' . $campanas_pagsesion_total . '",';	
		$strA .= '"directo_pagsesion_total": "' . $directo_pagsesion_total . '",';	
		//Sin formatear
		$strA .= '"enlaces_pagsesion_total_comp": "' . $enlaces_pagsesion_total_graf . '",';	
		$strA .= '"buscadores_pagsesion_total_comp": "' . $buscadores_pagsesion_total_graf . '",';	
		$strA .= '"social_pagsesion_total_comp": "' . $social_pagsesion_total_graf . '",';	
		$strA .= '"campanas_pagsesion_total_comp": "' . $campanas_pagsesion_total_graf . '",';	
		$strA .= '"directo_pagsesion_total_comp": "' . $directo_pagsesion_total_graf . '",';

	//Conversiones totales
		$strA .= '"conv_enlaces_total": "' . $conversiones_enlaces_total . '",';	
		$strA .= '"conv_buscadores_total": "' . $conversiones_buscadores_total . '",';	
		$strA .= '"conv_social_total": "' . $conversiones_social_total . '",';	
		$strA .= '"conv_campanas_total": "' . $conversiones_campanas_total . '",';	
		$strA .= '"conv_directo_total": "' . $conversiones_directo_total . '",';	

	$strA .= '"grafico_tarta": "' . $strB . '",';	
	$strA .= '"grafico_colum": "' . $chartc . '",';	

	$strA .= '"objson_enlaces": ' . $objson_enlaces . ',';
	$strA .= '"objson_buscadores": ' . $objson_buscadores . ',';
	$strA .= '"objson_social": ' . $objson_social . ',';
	$strA .= '"objson_campanas": ' . $objson_campanas . ',';
	$strA .= '"objson_directo": ' . $objson_directo . '';
	$strA .= '}';


	
	return $strA;

}//Fuentes extra

function fuentes_extra_prueba($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro){

	$DataTables_start = 0;
	$DataTables_length = 20000;
	//Construimos la llamada a la api
	$obj_Datos_Api_admin = new Datos_Api_Admin(); // Instanciamos la clase Datos_Api_Admin
    $obj_Datos_Api_admin -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_admin -> idVista              = $idVistaAnalytics ;
    $obj_Datos_Api_admin -> TipoLlamada          = 'GOALS_VISTA' ;   
    $obj_Datos_Api_admin -> Construccion();

    $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_METRICAS_SESIONES();
	$ARRAY_GOALS_VISTA_ORDENADOS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS();


	//Hacemos tantas llamadas al api de datos como elementos tenga el array de métricas.  En cada llamada sacaremos cinco objetivos (10 méticas)
	$array_Datos_Api = array();

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= $strFiltro;
	}
	//$p="";
	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS); $intI++) {

		$ses = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI];

		//print($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI]);
	    //Creación objeto, parametrización -------------------------------------------------------
	    $array_Datos_Api[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $array_Datos_Api[$intI] -> strProyectoAsociado  = $strProyectoAsociado ;
	    $array_Datos_Api[$intI] -> idVista              = 'ga:'. $idVistaAnalytics ;
	    $array_Datos_Api[$intI] -> startdate            = $datFechaInicioFiltro ;
	    $array_Datos_Api[$intI] -> enddate              = $datFechaFinFiltro ;
	    $array_Datos_Api[$intI] -> metrics              = $ses;

		if(empty($filtro)){
	    	$array_Datos_Api[$intI] -> optParams            = array(
															'dimensions' => 'ga:campaign,ga:medium,ga:socialNetwork,ga:source',
															//'sort' => '-ga:sessions',
															'start-index' => $DataTables_start + 1,
				                                            'max-results' => $DataTables_length);  
	    }else{
	    	$array_Datos_Api[$intI] -> optParams            = array(
															'dimensions' => 'ga:campaign,ga:medium,ga:socialNetwork,ga:source',
															'filters' => $filtro,
															//'sort' => '-ga:sessions',
															'start-index' => $DataTables_start + 1,
				                                            'max-results' => $DataTables_length);  
	    }
	    $array_Datos_Api[$intI] -> Construccion();
		
		

	}//Llamadas a la api
	$p = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS;

	//Inicializamos las variables contedores
	$enlaces_sesiones_total = 0;
	$cont_enlaces_tr        = 0;
	$cont_buscadores_tr     = 0;
	$cont_social_tr         = 0;
	$cont_campanas_tr       = 0;
	$cont_directo_tr        = 0;
	$fila 			        = 0;
	$cont_enlaces_ts        = 0;
	$cont_buscadores_ts     = 0;
	$cont_social_ts         = 0;
	$cont_campanas_ts       = 0;
	$cont_directo_ts        = 0;
	$cont_enlaces_ps        = 0;
	$cont_buscadores_ps     = 0;
	$cont_social_ps         = 0;
	$cont_campanas_ps       = 0;
	$cont_directo_ps        = 0;
	$objetivos_enlaces    = array();
	$objetivos_buscadores = array();
	$objetivos_social     = array();
	$objetivos_campanas   = array();
	$objetivos_directo    = array();
	//recorremos todos los datos de la api

	for ($x=1; $x<=$array_Datos_Api[0]->NumValores(); $x++) {
		$intI=0;

		//Aqui sumamos todas las sesiones para sacar el porcentaje
		$sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);
		//****

		//Para enlaces (ga:medium==referral;ga:SocialNetwork==(not set))
		if ($array_Datos_Api[$intI]->Valor("medium",$x) == "referral" && $array_Datos_Api[$intI]->Valor("SocialNetwork",$x) == "(not set)") {
			//$p =$array_Datos_Api[$intI]->Valor("source",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$enlaces_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);

			//Tasa rebote
			$enlaces_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_enlaces_tr++;
			}

			//Tiempo sesión
			$enlaces_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_enlaces_ts++;
			}

			//Páginas sesión
			$enlaces_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_enlaces_ps++;
			}
			
		}
		//Para busqueda ga:medium==organic;ga:SocialNetwork==(not set)
		if ($array_Datos_Api[$intI]->Valor("medium",$x) == "organic" && $array_Datos_Api[$intI]->Valor("SocialNetwork",$x) == "(not set)") {
			//$p = $array_Datos_Api[$intI]->Valor("keyword",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$buscadores_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);

			//Tasa rebote
			$buscadores_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_buscadores_tr++;
			}

			//Tiempo sesión
			$buscadores_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_buscadores_ts++;
			}

			//Páginas sesión
			$buscadores_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_buscadores_ps++;
			}
		
		}
		//Para social ga:SocialNetwork==(not set)
		if ($array_Datos_Api[$intI]->Valor("SocialNetwork",$x) != "(not set)") {
			
			$social_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);

			//Tasa rebote
			$social_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_social_tr++;
			}

			//Tiempo sesión
			$social_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_social_ts++;
			}

			//Páginas sesión
			$social_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_social_ps++;
			}
			
		}
		//Para campañas ga:medium!=referral;ga:medium!=organic;ga:medium!=(none); ga:SocialNetwork==(not set)
		if ($array_Datos_Api[$intI]->Valor("medium",$x) != "referral" && $array_Datos_Api[$intI]->Valor("medium",$x) != "organic" && $array_Datos_Api[$intI]->Valor("medium",$x) != "(none)" && $array_Datos_Api[$intI]->Valor("SocialNetwork",$x) == "(not set)") {
			//$pruebad .=$array_Datos_Api[$intI]->Valor("socialNetwork",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$campanas_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);
			
			//Tasa rebote
			$campanas_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_campanas_tr++;
			}

			//Tiempo sesión
			$campanas_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_campanas_ts++;
			}

			//Páginas sesión
			$campanas_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_campanass_ps++;
			}
		}
		//Para directo ga:medium==(none)
		if ($array_Datos_Api[$intI]->Valor("medium",$x) == "(none)") {
			//$pruebae .=$array_Datos_Api[$intI]->Valor("source",$x)."  -- ". $array_Datos_Api[$intI]->Valor("sessions",$x)."<br>";
			$directo_sesiones_total += $array_Datos_Api[$intI]->Valor("sessions",$x);
			
			//Tasa rebote
			$directo_tasarebote_total += $array_Datos_Api[$intI]->Valor("bounceRate",$x);
			if($array_Datos_Api[$intI]->Valor("bounceRate",$x) != "0.0"){
				$cont_directo_tr++;
			}

			//Tiempo sesión
			$directo_tsesion_total += $array_Datos_Api[$intI]->Valor("avgSessionDuration",$x);
			if($array_Datos_Api[$intI]->Valor("avgSessionDuration",$x) != "0.0"){
				$cont_directo_ts++;
			}

			//Páginas sesión
			$directo_pagsesion_total += $array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x);
			if($array_Datos_Api[$intI]->Valor("pageviewsPerSession",$x) != "0.0"){
				$cont_directo_ps++;
			}
		}

	}

	$tot_ordenados = count($ARRAY_GOALS_VISTA_ORDENADOS);
	$tot_ordenados_m = count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS);

	if($tot_ordenados_m > 2){
		$vt=0;
		while ($tot_ordenados > 5) {
			$tot_ordenados = $tot_ordenados-5;
			$vt++;
		}

	}else if($tot_ordenados_m == 2){
		$vt=2;
		$tot_ordenados = $tot_ordenados;

	}else if($tot_ordenados_m < 2){
		$vt=1;
	}

	$p = $tot_ordenados;

	$total_total_conversiones = 0;

	for ($x=1; $x<=$array_Datos_Api[0]->NumValores(); $x++) {

		$vuelta=0;

		if($vt==1){

			//Para enlaces
			for ($j=0; $j < 1; $j++) { 		

				for ($i=10; $i < 10+$tot_ordenados; $i++) { 

					$id_obj = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["id"];
					
					//Para enlaces
					if ($array_Datos_Api[0]->Valor("medium",$x) == "referral" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
				
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {

							$objetivos_enlaces[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
							$objetivos_enlaces[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);							
							$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

						}

					}		

					//Buscadores
					if ($array_Datos_Api[0]->Valor("medium",$x) == "organic" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
					
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
	
							$objetivos_buscadores[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
							$objetivos_buscadores[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
							$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
							
						}
					
					}
	
					//Social
					if ($array_Datos_Api[0]->Valor("SocialNetwork",$x) != "(not set)") {
	
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
	
							$objetivos_social[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
							$objetivos_social[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
							$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

						}
					
					}
	
					//Campañas
					if ($array_Datos_Api[0]->Valor("medium",$x) != "referral" && $array_Datos_Api[0]->Valor("medium",$x) != "organic" && $array_Datos_Api[0]->Valor("medium",$x) != "(none)" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
									
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
	
								$objetivos_campanas[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
								$objetivos_campanas[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
								$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

						}
					
					}	
	
					//Directo
					if ($array_Datos_Api[0]->Valor("medium",$x) == "(none)") {
					
						if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
	
								$objetivos_directo[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
								$objetivos_directo[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
								$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

						}
					
					}

				}//Segundo for

			}//Primer for	

		}else{
			//vt
			//columna 
			for ($j=0; $j < $tot_ordenados_m; $j++) { 
				
				if($j < 2){
					//echo $tot_ordenados."  -  ";
					//mitad una
					if($vuelta==0){
						for ($i=0; $i < 5; $i++) { 


							$id_obj = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["id"];
					
							//Para enlaces
							if ($array_Datos_Api[0]->Valor("medium",$x) == "referral" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
						
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {

									$objetivos_enlaces[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_enlaces[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);							
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}

							}		

							//Buscadores
							if ($array_Datos_Api[0]->Valor("medium",$x) == "organic" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_buscadores[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_buscadores[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									
								}
							
							}
			
							//Social
							if ($array_Datos_Api[0]->Valor("SocialNetwork",$x) != "(not set)") {
			
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_social[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_social[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}
			
							//Campañas
							if ($array_Datos_Api[0]->Valor("medium",$x) != "referral" && $array_Datos_Api[0]->Valor("medium",$x) != "organic" && $array_Datos_Api[0]->Valor("medium",$x) != "(none)" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
											
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_campanas[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_campanas[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}	
			
							//Directo
							if ($array_Datos_Api[0]->Valor("medium",$x) == "(none)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_directo[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_directo[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}

						}
						$vuelta++;
					//mitad 2
					}else{
						$desde = $tot_ordenados/2;
						for ($i=5; $i < 10; $i++) { 

							$id_obj = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["id"];
					
							//Para enlaces
							if ($array_Datos_Api[0]->Valor("medium",$x) == "referral" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
						
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {

									$objetivos_enlaces[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_enlaces[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);							
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}

							}		

							//Buscadores
							if ($array_Datos_Api[0]->Valor("medium",$x) == "organic" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_buscadores[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_buscadores[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									
								}
							
							}
			
							//Social
							if ($array_Datos_Api[0]->Valor("SocialNetwork",$x) != "(not set)") {
			
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_social[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_social[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}
			
							//Campañas
							if ($array_Datos_Api[0]->Valor("medium",$x) != "referral" && $array_Datos_Api[0]->Valor("medium",$x) != "organic" && $array_Datos_Api[0]->Valor("medium",$x) != "(none)" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
											
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_campanas[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_campanas[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}	
			
							//Directo
							if ($array_Datos_Api[0]->Valor("medium",$x) == "(none)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_directo[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_directo[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}

								}
								$vuelta==0;
							}

				}else{

					if($tot_ordenados_m > 2){
						//otros

						for ($i=10; $i < 10+$tot_ordenados; $i++) { 
												
							$id_obj = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["id"];
					
							//Para enlaces
							if ($array_Datos_Api[0]->Valor("medium",$x) == "referral" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
						
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {

									$objetivos_enlaces[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_enlaces[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);							$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}

							}		

							//Buscadores
							if ($array_Datos_Api[0]->Valor("medium",$x) == "organic" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_buscadores[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_buscadores[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									
								}
							
							}
			
							//Social
							if ($array_Datos_Api[0]->Valor("SocialNetwork",$x) != "(not set)") {
			
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
									$objetivos_social[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
									$objetivos_social[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
									$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}
			
							//Campañas
							if ($array_Datos_Api[0]->Valor("medium",$x) != "referral" && $array_Datos_Api[0]->Valor("medium",$x) != "organic" && $array_Datos_Api[0]->Valor("medium",$x) != "(none)" && $array_Datos_Api[0]->Valor("SocialNetwork",$x) == "(not set)") {
											
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_campanas[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_campanas[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}	
			
							//Directo
							if ($array_Datos_Api[0]->Valor("medium",$x) == "(none)") {
							
								if ($array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != "" && $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x) != 0) {
			
										$objetivos_directo[$id_obj]["nombre"] = $ARRAY_GOALS_VISTA_ORDENADOS[$i]["nombre"];
										$objetivos_directo[$id_obj]["val"] += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);
										$total_total_conversiones += $array_Datos_Api[$j]->Valor("goal".$id_obj."Completions",$x);

								}
							
							}
						}

					}

				}//else j

		}//elsevt	


	}//for
		}	
	

	//Calcular sesiones
	if($enlaces_sesiones_total != ""){
		$enlaces_sesiones_total_graf = $enlaces_sesiones_total;
		$enlaces_sesiones_total_img = number_format($enlaces_sesiones_total,0,',','.')."(".porcentaje_sesiones($enlaces_sesiones_total,$sesiones_total)."%)";
		$enlaces_sesiones_total = number_format($enlaces_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($enlaces_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$enlaces_sesiones_total_graf = 0;
		$enlaces_sesiones_total_img = "0 (0,00%)";	
		$enlaces_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";	
	}

	if($buscadores_sesiones_total != ""){
		$buscadores_sesiones_total_graf = $buscadores_sesiones_total;
		$buscadores_sesiones_total_img = number_format($buscadores_sesiones_total,0,',','.')."(".porcentaje_sesiones($buscadores_sesiones_total,$sesiones_total)."%)";
		$buscadores_sesiones_total = number_format($buscadores_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($buscadores_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$buscadores_sesiones_total_graf = 0;
		$buscadores_sesiones_total_img = "0 (0,00%)";	
		$buscadores_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";	
	}

	if($social_sesiones_total != ""){
		$social_sesiones_total_graf = $social_sesiones_total;
		$social_sesiones_total_img = number_format($social_sesiones_total,0,',','.')." (".porcentaje_sesiones($social_sesiones_total,$sesiones_total)."%)";
		$social_sesiones_total = number_format($social_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($social_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$social_sesiones_total_graf = 0;
		$social_sesiones_total_img = "0 (0,00%)>";	
		$social_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";	
	}

	if($campanas_sesiones_total != ""){
		$campanas_sesiones_total_graf = $campanas_sesiones_total;
		$campanas_sesiones_total_img = number_format($campanas_sesiones_total,0,',','.')."(".porcentaje_sesiones($campanas_sesiones_total,$sesiones_total)."%)";
		$campanas_sesiones_total = number_format($campanas_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($campanas_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$campanas_sesiones_total_graf = 0;
		$campanas_sesiones_total_img = "0 (0,00%)";
		$campanas_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";
	}

	if($directo_sesiones_total != ""){
		$directo_sesiones_total_graf = $directo_sesiones_total;
		$directo_sesiones_total_img = number_format($directo_sesiones_total,0,',','.')." (".porcentaje_sesiones($directo_sesiones_total,$sesiones_total)."%)";
		$directo_sesiones_total = number_format($directo_sesiones_total,0,',','.')."<span class='porcDT'> (".porcentaje_sesiones($directo_sesiones_total,$sesiones_total)."%)</span>";
	}else{
		$directo_sesiones_total_graf = 0;
		$directo_sesiones_total_img = "0 (0,00%)";
		$directo_sesiones_total = "0 <span class='porcDT'>(0,00%)</span>";
	}


	//Calcular tasarebote
	if($cont_enlaces_tr!=0){
		$enlaces_tasarebote_total_graf = $enlaces_tasarebote_total;
		$enlaces_tasarebote_total = number_format(($enlaces_tasarebote_total/$cont_enlaces_tr),2,',','.');
	}else{
		$$enlaces_tasarebote_total_graf = 0;
		$enlaces_tasarebote_total = "0.0";
	}

	if($cont_buscadores_tr!=0){
		$buscadores_tasarebote_total_graf = $buscadores_tasarebote_total;
		$buscadores_tasarebote_total = number_format(($buscadores_tasarebote_total/$cont_buscadores_tr),2,',','.');
	}else{
		$buscadores_tasarebote_total_graf = 0;
		$buscadores_tasarebote_total = "0.0";
	}

	if($cont_social_tr!=0){
		$social_tasarebote_total_graf = $social_tasarebote_total;
		$social_tasarebote_total = number_format(($social_tasarebote_total/$cont_social_tr),2,',','.');
	}else{
		$social_tasarebote_total_graf = 0;
		$social_tasarebote_total = "0.0";
	}

	if($cont_campanas_tr!=0){
		$campanas_tasarebote_total_graf = $campanas_tasarebote_total;
		$campanas_tasarebote_total = number_format(($campanas_tasarebote_total/$cont_campanas_tr),2,',','.');
	}else{
		$campanas_tasarebote_total_graf = 0;
		$campanas_tasarebote_total = "0.0";
	}

	if($cont_directo_tr!=0){
		$directo_tasarebote_total_graf = $directo_tasarebote_total;
		$directo_tasarebote_total = number_format(($directo_tasarebote_total/$cont_directo_tr),2,',','.');
	}else{
		$directo_tasarebote_total_graf = 0;
		$directo_tasarebote_total = "0.0";
	}

	//Calcular tiemposesion
	if($cont_enlaces_ts!=0){
		$enlaces_tsesion_total_graf = $enlaces_tsesion_total/$cont_enlaces_ts;
		$enlaces_tsesion_total = segundos_tiempo($enlaces_tsesion_total/$cont_enlaces_ts);
	}else{
		$enlaces_tsesion_total_graf = 0;
		$enlaces_tsesion_total = "0.00 min";
	}
	if($cont_buscadores_ts!=0){
		$buscadores_tsesion_total = $buscadores_tsesion_total/$cont_buscadores_ts;
		$buscadores_tsesion_total = segundos_tiempo($buscadores_tsesion_total/$cont_buscadores_ts);
	}else{
		$buscadores_tsesion_total = 0;
		$buscadores_tsesion_total = "0.00 min";
	}
	if($cont_social_ts!=0){
		$social_tsesion_total_graf = $social_tsesion_total/$cont_social_ts;
		$social_tsesion_total = segundos_tiempo($social_tsesion_total/$cont_social_ts);
	}else{
		$social_tsesion_total_graf = 0;
		$social_tsesion_total = "0.00 min";
	}
	if($cont_campanas_ts!=0){
		$campanas_tsesion_total_graf = $campanas_tsesion_total/$cont_campanas_ts;
		$campanas_tsesion_total = segundos_tiempo($campanas_tsesion_total/$cont_campanas_ts);
	}else{
		$campanas_tsesion_total_graf = 0;
		$campanas_tsesion_total = "0.00 min";
	}
	if($cont_directo_ts!=0){
		$directo_tsesion_total_graf = $directo_tsesion_total/$cont_directo_ts;
		$directo_tsesion_total = segundos_tiempo($directo_tsesion_total/$cont_directo_ts);
	}else{
		$directo_tsesion_total_graf = 0;
		$directo_tsesion_total = "0.00 min";
	}

	//Calcular paginas/sesion
	if($cont_enlaces_ps!=0){
		$enlaces_pagsesion_total_graf = $enlaces_pagsesion_total/$cont_enlaces_ps;
		$enlaces_pagsesion_total = number_format(($enlaces_pagsesion_total/$cont_enlaces_ps),2,',','.');
	}else{
		$enlaces_pagsesion_total_graf = 0;
		$enlaces_pagsesion_total = "0";
	}
	if($cont_buscadores_ps!=0){
		$buscadores_pagsesion_total_graf = $buscadores_pagsesion_total/$cont_buscadores_ps;
		$buscadores_pagsesion_total = number_format(($buscadores_pagsesion_total/$cont_buscadores_ps),2,',','.');
	}else{
		$buscadores_pagsesion_total_graf = 0;
		$buscadores_pagsesion_total = "0";
	}
	if($cont_social_ps!=0){
		$social_pagsesion_total_graf = $social_pagsesion_total/$cont_social_ps;
		$social_pagsesion_total = number_format(($social_pagsesion_total/$cont_social_ps),2,',','.');
	}else{
		$social_pagsesion_total_graf = 0;
		$social_pagsesion_total = "0";
	}
	if($cont_campanas_ps!=0){
		$campanas_pagsesion_total_graf = $campanas_pagsesion_total/$cont_campanas_ps;
		$campanas_pagsesion_total = number_format(($campanas_pagsesion_total/$cont_campanas_ps),2,',','.');
	}else{
		$campanas_pagsesion_total_graf = 0;
		$campanas_pagsesion_total = "0";
	}
	if($cont_directo_ps!=0){
		$directo_pagsesion_total_graf = $directo_pagsesion_total/$cont_directo_ps;
		$directo_pagsesion_total = number_format(($directo_pagsesion_total/$cont_directo_ps),2,',','.');
	}else{
		$directo_pagsesion_total_graf = 0;
		$directo_pagsesion_total = "0";
	}

	//Montamos el json para traer la array de objetivos
	$charttits .= "['Conversiones',";
	$titscol = array();
	$ff=0;
	//Enlaces
	if(!empty($objetivos_enlaces)){
		$objson_enlaces = "[";
		foreach ($objetivos_enlaces as $key => $obj_enlaces) {
			
			$objson_enlaces .=  "[\"nombre\",\"".$obj_enlaces["nombre"]."\"],";
			if($obj_enlaces["val"] == ""){ $n = 0; }else{ $n = $obj_enlaces["val"];}
			$enlaces_value = $obj_enlaces["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_enlaces["val"],$total_total_conversiones )."%)</span>";

			$objson_enlaces .=  "[\"valor\",\"".$enlaces_value."\"],";		

			if (in_array($obj_enlaces["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_enlaces["nombre"]."',";
				$titscol[$ff] = $obj_enlaces["nombre"];
				$ff++;
			}
			//$charttits .= "'".$obj_enlaces["nombre"]."',";
		}
		$objson_enlaces = substr($objson_enlaces, 0, -1);
		$objson_enlaces .= "]";

	}else{
		$objson_enlaces .= "\"\"";
	}

	//buscadores
	if(!empty($objetivos_buscadores)){
		$objson_buscadores = "[";
		foreach ($objetivos_buscadores as $key => $obj_buscadores) {
			
			$objson_buscadores .=  "[\"nombre\",\"".$obj_buscadores["nombre"]."\"],";
			if($obj_buscadores["val"] == ""){ $n = 0; }else{ $n = $obj_buscadores["val"];}
			$buscadores_value = $obj_buscadores["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_buscadores["val"],$total_total_conversiones )."%)</span>";
			$objson_buscadores .=  "[\"valor\",\"".$buscadores_value."\"],";
			
			if (in_array($obj_buscadores["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_buscadores["nombre"]."',";
				$titscol[$ff] = $obj_buscadores["nombre"];
				$ff++;
			}

		}
		$objson_buscadores = substr($objson_buscadores, 0, -1);
		$objson_buscadores .= "]";
	}else{
		$objson_buscadores .= "\"\"";
	}

	//social
	if(!empty($objetivos_social)){
		$objson_social = "[";
		foreach ($objetivos_social as $key => $obj_social) {
			
			$objson_social .=  "[\"nombre\",\"".$obj_social["nombre"]."\"],";
			if($objson_social["val"] == ""){ $n = 0; }else{ $n = $objson_social["val"];}
			$social_value = $obj_social["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_social["val"],$total_total_conversiones )."%)</span>";
			$objson_social .=  "[\"valor\",\"".$social_value."\"],";
			
			if (in_array($obj_social["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_social["nombre"]."',";
				$titscol[$ff] = $obj_social["nombre"];
				$ff++;
			}

		}
		$objson_social = substr($objson_social, 0, -1);
		$objson_social .= "]";

	}else{
		$objson_social .= "\"\"";
	}	
	
	//Campañas
	
	if(!empty($objetivos_campanas)){
		$objson_campanas = "[";
		foreach ($objetivos_campanas as $key => $obj_campanas) {
			
			$objson_campanas .=  "[\"nombre\",\"".$obj_campanas["nombre"]."\"],";
			if($objson_campanas["val"] == ""){ $n = 0; }else{ $n = $objson_campanas["val"];}
			$campanas_value = $obj_campanas["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_campanas["val"],$total_total_conversiones )."%)</span>";
			$objson_campanas .=  "[\"valor\",\"".$campanas_value."\"],";
			
			if (in_array($obj_campanas["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_campanas["nombre"]."',";
				$titscol[$ff] = $obj_campanas["nombre"];
				$ff++;
			}

		}
		$objson_campanas = substr($objson_campanas, 0, -1);
		$objson_campanas .= "]";
	}else{
		$objson_campanas .= "\"\"";
	}
	
	

	//Directo
	if(!empty($objetivos_directo)){
		$objson_directo = "[";
		foreach ($objetivos_directo as $key => $obj_directo) {
			
			$objson_directo .=  "[\"nombre\",\"".$obj_directo["nombre"]."\"],";
			if($objetivos_directo["val"] == ""){ $n = 0; }else{ $n = $objetivos_directo["val"];}
			$directo_value = $obj_directo["val"]." <span class='porcDTb'> (".porcentaje_sesiones($obj_directo["val"],$total_total_conversiones )."%)</span>";
			$objson_directo .=  "[\"valor\",\"".$directo_value."\"],";
			
			if (in_array($obj_directo["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$charttits .= "'".$obj_directo["nombre"]."',";
				$titscol[$ff] = $obj_directo["nombre"];
				$ff++;
			}

		}
		$objson_directo = substr($objson_directo, 0, -1);
		$objson_directo .= "]";
	}else{
		$objson_directo .= "\"\"";
	}

	$charttits = substr($charttits, 0, -1);
	$charttits .= "]";



	$conversiones_enlaces_total    = 0;
	$conversiones_buscadores_total = 0;
	$conversiones_social_total     = 0;
	$conversiones_campanas_total   = 0;
	$conversiones_directo_total    = 0;

	//Ahora que sabemos el total de conversiones recorremos los vlores para el gráfico de columnas
	$chartval  .= "";
	$count_col = count($titscol);
	$titscol = array();
	$nvals = 0;

	//Enlaces
	//if(!empty($objetivos_enlaces)){

		$chartval .= "['',";
		foreach ($objetivos_enlaces as $key => $obj_enlaces) {
			if (in_array($obj_enlaces["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_enlaces_total += $obj_enlaces["val"];
				$chartval .= "".$obj_enlaces["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;

		//Que pintamos
		$conversiones_enlaces_total = $conversiones_enlaces_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_enlaces_total,$total_total_conversiones )."%)</span>";
	//}

	//Buscadores
	//if(!empty($objetivos_buscadores)){

		$chartval .= "['',";
		foreach ($objetivos_buscadores as $key => $obj_buscadores) {
			if (in_array($obj_buscadores["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_buscadores_total += $obj_buscadores["val"];
				$chartval .= "".$obj_buscadores["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;
		$conversiones_buscadores_total = $conversiones_buscadores_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_buscadores_total,$total_total_conversiones )."%)</span>";

	//}

	//Social
	//if(!empty($objetivos_social)){

		$chartval .= "['',";
		foreach ($objetivos_social as $key => $obj_social) {
			if (in_array($obj_social["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_social_total += $obj_social["val"];
				$chartval .= "".$obj_social["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;
		$conversiones_social_total = $conversiones_social_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_social_total,$total_total_conversiones )."%)</span>";

	//}

	//Campañas
	//if(!empty($objetivos_campanas)){

		$chartval .= "['',";
		foreach ($objetivos_campanas as $key => $obj_campanas) {
			if (in_array($obj_campanas["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_campanas_total += $obj_campanas["val"];
				$chartval .= "".$obj_campanas["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;
		$conversiones_campanas_total = $conversiones_campanas_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_campanas_total,$total_total_conversiones )."%)</span>";
	//}

	//Directo
	//if(!empty($objetivos_directo)){

		$chartval .= "['',";
		foreach ($objetivos_directo as $key => $obj_directo) {
			if (in_array($obj_directo["nombre"], $titscol)) {
			    //Existe
			}else{
				//No existe
				$conversiones_directo_total += $obj_directo["val"];
				$chartval .= "".$obj_directo["val"].",";
				$nvals++;
			}
		}
		$nvueltas = $count_col - $nvals;
		if($nvueltas != 0){
			for ($i=0; $i <= $nvueltas-1 ; $i++) { 
				$chartval .= "0,";
			}
		}
		$chartval = substr($chartval, 0, -1);
		$chartval .= "],";
		$nvals=0;
		$conversiones_directo_total = $conversiones_directo_total." <span class='porcDT'> (".porcentaje_sesiones($conversiones_directo_total,$total_total_conversiones )."%)</span>";
	//}

	$chartval = substr($chartval, 0, -1);

	$chartc = "[";
	$chartc .= $charttits.",";
	$chartc .= $chartval;
	$chartc .= "]";


	//Montamos el json los graficos de tarta
    $strB = "[";
    $strB .= "['Fuente', 'Sesiones'],";
    $strB .= "['Enlaces', ".$enlaces_sesiones_total_graf."],";
    $strB .= "['Búsquedas', ".$buscadores_sesiones_total_graf."],";
    $strB .= "['Social', ".$social_sesiones_total_graf."],";
    $strB .= "['Campañas', ".$campanas_sesiones_total_graf."],";
    $strB .= "['Directo', ".$directo_sesiones_total_graf."]";
    $strB .= "]";


	//Montamos el Json para
	$strA = '{';
	//Sesiones
		//Formateado
		$strA .= '"enlaces_sesiones_total": "' . $enlaces_sesiones_total . '",';	
		$strA .= '"enlaces_sesiones_total_img": "' . $enlaces_sesiones_total_img . '",';	
		$strA .= '"buscadores_sesiones_total": "' . $buscadores_sesiones_total . '",';	
		$strA .= '"buscadores_sesiones_total_img": "' . $buscadores_sesiones_total_img . '",';	
		$strA .= '"social_sesiones_total": "' . $social_sesiones_total . '",';	
		$strA .= '"social_sesiones_total_img": "' . $social_sesiones_total_img . '",';	
		$strA .= '"campanas_sesiones_total": "' . $campanas_sesiones_total . '",';	
		$strA .= '"campanas_sesiones_total_img": "' . $campanas_sesiones_total_img . '",';	
		$strA .= '"directo_sesiones_total": "' . $directo_sesiones_total . '",';
		$strA .= '"directo_sesiones_total_img": "' . $directo_sesiones_total_img . '",';
		//Sin formatear
		$strA .= '"enlaces_sesiones_total_comp": "' . $enlaces_sesiones_total_graf . '",';	
		$strA .= '"buscadores_sesiones_total_comp": "' . $buscadores_sesiones_total_graf . '",';	
		$strA .= '"social_sesiones_total_comp": "' . $social_sesiones_total_graf . '",';	
		$strA .= '"campanas_sesiones_total_comp": "' . $campanas_sesiones_total_graf . '",';	
		$strA .= '"directo_sesiones_total_comp": "' . $directo_sesiones_total_graf . '",';	

	//tasa de rebote
		//Formateado
		$strA .= '"enlaces_tasarebote_total": "' . $enlaces_tasarebote_total . '%",';	
		$strA .= '"buscadores_tasarebote_total": "' . $buscadores_tasarebote_total . '%",';	
		$strA .= '"social_tasarebote_total": "' . $social_tasarebote_total . '%",';	
		$strA .= '"campanas_tasarebote_total": "' . $campanas_tasarebote_total . '%",';	
		$strA .= '"directo_tasarebote_total": "' . $directo_tasarebote_total . '%",';
		//Sin formatear
		$strA .= '"enlaces_tasarebote_total_comp": "' . $enlaces_tasarebote_total_graf . '",';	
		$strA .= '"buscadores_tasarebote_total_comp": "' . $buscadores_tasarebote_total_graf . '",';	
		$strA .= '"social_tasarebote_total_comp": "' . $social_tasarebote_total_graf . '",';	
		$strA .= '"campanas_tasarebote_total_comp": "' . $campanas_tasarebote_total_graf . '",';	
		$strA .= '"directo_tasarebote_total_comp": "' . $directo_tasarebote_total_graf . '",';

	//Tiempo sesion
		//Formateado
		$strA .= '"enlaces_tsesion_total": "' . $enlaces_tsesion_total . '",';	
		$strA .= '"buscadores_tsesion_total": "' . $buscadores_tsesion_total . '",';	
		$strA .= '"social_tsesion_total": "' . $social_tsesion_total . '",';	
		$strA .= '"campanas_tsesion_total": "' . $campanas_tsesion_total . '",';	
		$strA .= '"directo_tsesion_total": "' . $directo_tsesion_total . '",';	
		//Sin formatear
		$strA .= '"enlaces_tsesion_total_comp": "' . $enlaces_tsesion_total_graf . '",';	
		$strA .= '"buscadores_tsesion_total_comp": "' . $buscadores_tsesion_total_graf . '",';	
		$strA .= '"social_tsesion_total_comp": "' . $social_tsesion_total_graf . '",';	
		$strA .= '"campanas_tsesion_total_comp": "' . $campanas_tsesion_total_graf . '",';	
		$strA .= '"directo_tsesion_total_comp": "' . $directo_tsesion_total_graf . '",';	
	
	//Pag/Sesión
		//Formateado
		$strA .= '"enlaces_pagsesion_total": "' . $enlaces_pagsesion_total . '",';	
		$strA .= '"buscadores_pagsesion_total": "' . $buscadores_pagsesion_total . '",';	
		$strA .= '"social_pagsesion_total": "' . $social_pagsesion_total . '",';	
		$strA .= '"campanas_pagsesion_total": "' . $campanas_pagsesion_total . '",';	
		$strA .= '"directo_pagsesion_total": "' . $directo_pagsesion_total . '",';	
		//Sin formatear
		$strA .= '"enlaces_pagsesion_total_comp": "' . $enlaces_pagsesion_total_graf . '",';	
		$strA .= '"buscadores_pagsesion_total_comp": "' . $buscadores_pagsesion_total_graf . '",';	
		$strA .= '"social_pagsesion_total_comp": "' . $social_pagsesion_total_graf . '",';	
		$strA .= '"campanas_pagsesion_total_comp": "' . $campanas_pagsesion_total_graf . '",';	
		$strA .= '"directo_pagsesion_total_comp": "' . $directo_pagsesion_total_graf . '",';

	//Conversiones totales
		$strA .= '"conv_enlaces_total": "' . $conversiones_enlaces_total . '",';	
		$strA .= '"conv_buscadores_total": "' . $conversiones_buscadores_total . '",';	
		$strA .= '"conv_social_total": "' . $conversiones_social_total . '",';	
		$strA .= '"conv_campanas_total": "' . $conversiones_campanas_total . '",';	
		$strA .= '"conv_directo_total": "' . $conversiones_directo_total . '",';	

	$strA .= '"grafico_tarta": "' . $strB . '",';	
	$strA .= '"grafico_colum": "' . $chartc . '",';	

	$strA .= '"objson_enlaces": ' . $objson_enlaces . ',';
	$strA .= '"objson_buscadores": ' . $objson_buscadores . ',';
	$strA .= '"objson_social": ' . $objson_social . ',';
	$strA .= '"objson_campanas": ' . $objson_campanas . ',';
	$strA .= '"objson_directo": ' . $objson_directo . '';
	$strA .= '}';

	//return $prueba_obj;
	
	return $p;
	//return $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS;
	//return $prueba . "Total = ". $prueba_total;
}//Fuentes extra PRUEBA


//Funcion para traducir a segundos
function segundos_tiempo($segundos){
	$minutos=$segundos/60;
	$horas=floor($minutos/60);
	$minutos2=$minutos%60;
	$segundos_2=$segundos%60%60%60;
	if($minutos2<10)$minutos2='0'.$minutos2;
	if($segundos_2<10)$segundos_2='0'.$segundos_2;

	if($segundos<60){ /* segundos */
		$resultado= round($segundos).' seg.';
	}elseif($segundos>60 && $segundos<3600){/* minutos */
		$resultado= $minutos2.':'.$segundos_2.' min.';
	}else{/* horas */
	
	$resultado= $horas.':'.$minutos2.':'.$segundos_2.' h.';
	}
	return $resultado;
}


//FuentesTrafico_datos_extra
function fuentestrafico_datos_extra($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strApp){

		//$strSort='yearWeek';
	switch ($strTipo) {
		case 'enlaces':
			$strDimensions='ga:source';
			$strDimensionsN ='source'; 
			$metricas = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			$filtro_and = "ga:medium==referral;ga:SocialNetwork==(not set)";
			break;
		case 'buscadores':
			$strDimensions='ga:source,ga:keyword';
			$strDimensionsN ='source'; 
			$metricas = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			$filtro_and = "ga:medium==organic;ga:SocialNetwork==(not set)";
			break;
		case 'social':
			$strDimensions='ga:SocialNetwork';
			$strDimensionsN ='SocialNetwork'; 
			$metricas = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			$filtro_and = "ga:SocialNetwork!=(not set)";
			break;
		case 'campanas':
			$strDimensions='ga:campaign,ga:medium,ga:source';
			$strDimensionsN ='campaign'; 
			$metricas = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			$filtro_and = "ga:medium!=referral;ga:medium!=organic;ga:medium!=(none);ga:SocialNetwork==(not set)";
			break;


	};

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}

	if(!empty($filtro_and)){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $filtro_and;
	}


	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado = $strProyectoAsociado;
	$obj_Datos_Api -> idVista             = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate           = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate             = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics             = $metricas;

	if(empty($filtro)){
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
											'start-index' => 1,
	                                        'max-results' => 50); 
	}else{
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
											'filters' => $filtro,
											'start-index' => 1,
	                                        'max-results' => 50); 
	}
	 
	$obj_Datos_Api -> Construccion();



	//for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		
	//}

	$strA = '{';
	$strA .= '"sesiones": "' . $obj_Datos_Api->TotalF("sessions",$x) . '",';	
	$strA .= '"tasa_rebote": "' . $obj_Datos_Api->TotalF("bounceRate",$x) . '",';
	$strA .= '"duracion_sesion": "' . $obj_Datos_Api->TotalF("avgSessionDuration",$x) . '",';
	$strA .= '"paginas_sesion": "' . $obj_Datos_Api->TotalF("pageviewsPerSession",$x) . '"';
	$strA .= '}';
	
	//$strAuxb = substr($strAuxb, 0, -1);
	//Montando el json
	
	
    return $strA;

}

//funcion para montar un json para traer todos los idiomas comparados
function comparar_idiomas($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro, $strApp){

	//$strSort='yearWeek';
	$strDimensions='ga:dimension1,ga:dimension8';
	if($strApp == 0){
		$metricas = 'ga:pageviews';
		$mnom = 'pageviews';
	}else{
		$metricas = 'ga:screenviews';
		$mnom = 'screenviews';
	}
	
	


	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = $metricas;

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	if(empty($filtro)){
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
	                                        'sort' => '-'.$metricas,
											'start-index' => 1,
	                                        'max-results' => 10000); 
	}else{
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
											'filters' => $filtro,
	                                        'sort' => '-'.$metricas,
											'start-index' => 1,
	                                        'max-results' => 10000); 
		
	}
	
	$obj_Datos_Api -> Construccion();



	//reordenamos la array
	$strDimensions = "ga:dimension1";
	$obj_Datos_Api_orden = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_orden -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_orden -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_orden -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_orden -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_orden -> metrics              = $metricas;
	if(empty($filtro)){
		$obj_Datos_Api_orden -> optParams     = array(
											'dimensions' => $strDimensions,
	                                        'sort' => '-'.$metricas,
											'start-index' => 1,
	                                        'max-results' => 10000); 
	}else{
		$obj_Datos_Api_orden -> optParams     = array(
											'dimensions' => $strDimensions,
											'filters' => $filtro,
	                                        'sort' => '-'.$metricas,
											'start-index' => 1,
	                                        'max-results' => 10000); 
		
	}
	
	$obj_Datos_Api_orden -> Construccion();


	$total_idiomas = $obj_Datos_Api_orden->NumValores();

	$array_idiomas  = array();

	//Recogemos en un array todo el contenido que trae la api y la ordenamos por dimension8
	/*for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		//Completamos la array con un orden 
		if ($obj_Datos_Api->Valor("dimension8",$x) != "(not set)" && $obj_Datos_Api->Valor("dimension1",$x) != "(not set)") {
			$array_idiomas[$obj_Datos_Api->Valor("dimension8",$x)][$obj_Datos_Api->Valor("dimension1",$x)] = $obj_Datos_Api->Valor("pageviews",$x);
		}
	}*/


	for ($x=1; $x<=$obj_Datos_Api_orden->NumValores(); $x++) {

		for ($i=1; $i<=$obj_Datos_Api->NumValores(); $i++) {

			if($obj_Datos_Api->Valor("dimension8",$i) == $obj_Datos_Api_orden->Valor("dimension1",$x)){
				if ($obj_Datos_Api->Valor("dimension8",$i) != "(not set)" && $obj_Datos_Api->Valor("dimension1",$i) != "(not set)") {
					$array_idiomas[$obj_Datos_Api->Valor("dimension8",$i)][$obj_Datos_Api->Valor("dimension1",$i)] = $obj_Datos_Api->Valor($mnom,$i);
				}
			}

		}

	}

	for ($x=1; $x<=$obj_Datos_Api_orden->NumValores(); $x++) {

		for ($i=1; $i<=$obj_Datos_Api->NumValores(); $i++) {

			if($obj_Datos_Api->Valor("dimension8",$i) != $obj_Datos_Api_orden->Valor("dimension1",$x)){
				if ($obj_Datos_Api->Valor("dimension8",$i) != "(not set)" && $obj_Datos_Api->Valor("dimension1",$i) != "(not set)") {
					$array_idiomas[$obj_Datos_Api->Valor("dimension8",$i)][$obj_Datos_Api->Valor("dimension1",$i)] = $obj_Datos_Api->Valor($mnom,$i);
				}
			}

		}

	}

	/*$tr=0;
	for ($x=1; $x<=$obj_Datos_Api_orden->NumValores(); $x++) {
			

			//Recogemos en un array todo el contenido que trae la api y la ordenamos por dimension8
			for ($i=1; $i<=$obj_Datos_Api->NumValores(); $i++) {

				//Completamos la array con un orden 
				if ($obj_Datos_Api->Valor("dimension8",$i) != "(not set)" && $obj_Datos_Api->Valor("dimension1",$i) != "(not set)") {

					if($obj_Datos_Api_orden->Valor("dimension1",$x) == $obj_Datos_Api->Valor("dimension1",$i)){
						$array_idiomas[$tr][$obj_Datos_Api->Valor("dimension8",$i)][$obj_Datos_Api->Valor("dimension1",$i)] = $obj_Datos_Api->Valor("pageviews",$i);
					}

				}

			}
			$tr++;

	}*/


	//return $echo;
	

	//Recorremos la array que traemos de la api
	
	$strAuxb="";
	
	//$strAuxb .= '[';
	foreach ($array_idiomas as $nom => $ar_idiomas) {
		
		//$strAux_color = "[";
		$cont = 0;
		$strAux_color .= '[';
		$strAuxb .= '"[[\'Idioma\', \'Páginas vistas\'],';
		foreach ($ar_idiomas as $key => $idi) {
			$strAuxb .= '[\''.traducir_idioma($key).'\','.$idi.'],';
			/*
			0: { color: 'yellow' },
			1: { color: 'transparent' }
			*/
			$strAux_color .= '"'.idiomaColor($key).'",';
			$cont++;
		}
		$strAuxb = substr($strAuxb, 0, -1);
		$strAuxb .= ']",';

		$strAux_color = substr($strAux_color, 0, -1);
		$strAux_color .= '],';

		//$strAuxb .= '[[\'Idioma\', \'Páginas vistas\'],[\''.traducir_idioma($obj_Datos_Api->Valor("dimension1",$x)).'\','.$obj_Datos_Api->Valor("pageviews",$x).']"]"';		
	}

	//Recorremos la array que traemos de la api
	$strAuxc="";
	//$strAuxb .= '[';
	foreach ($array_idiomas as $nom => $ar_idiomas) {
		$strAuxc .= '"'.traducir_idioma($nom).'",';
	}
	
	$strAux_color = substr($strAux_color, 0, -1);
	$strAuxb = substr($strAuxb, 0, -1);
	$strAuxc = substr($strAuxc, 0, -1);
	//Montando el json

	$strAux = "{";
	$strAux .= '"total_registros": "'.count($array_idiomas).'",';
	$strAux .= '"registros": ['.$strAuxb.'],';
	$strAux .= '"nombres_registros": ['.$strAuxc.'],';
	$strAux .= '"colores": ['.$strAux_color.']';
	$strAux .= "}";
    return $strAux;

}

function idiomaColor($idioma){
switch ($idioma) {
	case 'es':
		$color = "#ce7474";
		break;
	case 'en':
		$color = "#eed6d6";
		break;
	case 'fr':
		$color = "#70a2d6";
		break;
	case 'pt':
		$color = "#956e12";
		break;
	case 'it':
		$color = "#609415";
		break;
	case 'eu':
		$color = "#79bc50";
		break;
	case 'de':
		$color = "#bcb150";
		break;
	case 'mx':
		$color = "#f2b4ff";
		break;
	default:
		$color = "gray";
		break;
	}
	return $color;
	
}

/*function idiomaColor($idioma){
switch ($idioma) {
	case 'Español':
		$color = "#ce7474";
		break;
	case 'Inglés':
		$color = "#eed6d6";
		break;
	case 'Francés':
		$color = "#70a2d6";
		break;
	case 'Portugués':
		$color = "#956e12";
		break;
	case 'Italiano':
		$color = "#609415";
		break;
	case 'Euskera':
		$color = "#79bc50";
		break;
	case 'Alemán':
		$color = "#bcb150";
		break;
	case 'Mx':
		$color = "#f2b4ff";
		break;
	default:
		$color = "gray";
		break;
	}
	return $color;
	
}*/

//GEOCHART
function contenido_piechart($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro, $strApp){
  global $trans;
		//$strSort='yearWeek';

  	if($strApp == 0){
		$metricas = 'ga:pageviews';
		$nnom = 'Páginas vistas';
		$nmetri = 'pageviews';
	}else{
		$metricas = 'ga:screenviews ';
		$nnom = 'Pantallas vistas';
		$nmetri = 'screenviews';
	}

	switch ($strTipo) {
		case 'idiomauser':
			$strDimensions='ga:dimension8';
			$strDimensionsN ='dimension8'; 
			break;
		case 'idiomanav':
			$strDimensions='ga:dimension1';
			$strDimensionsN ='dimension1'; 
			break;
	};

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = $metricas;

	if(empty($filtro)){
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
	                                        'sort' => '-'.$metricas,
											'start-index' => 1,
	                                        'max-results' => 50); 
	}else{
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
											'filters' => $filtro,
	                                        'sort' => '-'.$metricas,
											'start-index' => 1,
	                                        'max-results' => 50); 
		
	}
	 
	$obj_Datos_Api -> Construccion();


	//Recorremos la array que traemos de la api
	$strAux = "[['".$trans->__('Idioma',false)."', '".$trans->__($nnom,false)."'],";
	$strAux_color .= '[';

	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		//$strAuxb .= '[\''.str_replace("'", " ", $obj_Datos_Api->Valor($strDimensionsN,$x)).'\','.$obj_Datos_Api->Valor("pageviews",$x).','.$obj_Datos_Api->Valor("sessions",$x).','.$obj_Datos_Api->Valor("bounceRate",$x).','.$obj_Datos_Api->Valor("avgSessionDuration",$x).','.$obj_Datos_Api->Valor("pageviewsPerSession",$x).'],';
		if ($obj_Datos_Api->Valor($strDimensionsN,$x) != "(not set)") {
			$strAuxb .= '[\''.$trans->__(traducir_idioma($obj_Datos_Api->Valor($strDimensionsN,$x)),false).'\','.$obj_Datos_Api->Valor($nmetri,$x).'],';			
			$strAux_color .= '"'.idiomaColor($obj_Datos_Api->Valor($strDimensionsN,$x)).'",';
		}
	}

	
	$strAuxb = substr($strAuxb, 0, -1);
	$strAux_color = substr($strAux_color, 0, -1);
	$strAux_color .= ']';
	//Montando el json
	
	$strAux .= $strAuxb;
	$strAux .= "]";


	$strAuxf = "{";
	$strAuxf .= '"registros": "'.$strAux.'",';
	$strAuxf .= '"colores": ['.$strAux_color.']';
	$strAuxf .= "}";
 

    return $strAuxf;

}
//GEOCHART

//GEOCHART
function contenido_geochart($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro){

	global $trans;

	switch ($strTipo) {
		case 'pais':
			$strDimensions='ga:countryIsoCode,ga:country';
			$strDimensionsN ='country'; 
			$metricas = 'ga:pageviews,ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';		
			break;
		case 'region':
			$strDimensions='ga:region';
			$strDimensionsN ='region'; 
			$metricas = 'ga:pageviews,ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			break;
		case 'ciudad':
			$strDimensions='ga:city';
			$strDimensionsN ='city'; 
			$metricas = 'ga:pageviews,ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
			break;
		case 'paisPrin':
			$strDimensions='ga:countryIsoCode,ga:country';
			$strDimensionsN ='country'; 
			$metricas = 'ga:pageviews,ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';		
			break;
	};

	$filtro="";

	if($strTipo!="pais"){
		if($paisGa!="todos"){
			$filtro .= 'ga:country=='.$paisGa;
		}else{
			//Aquí sería el primero de la lista
			//$filtro .= 'ga:country==Spain';
		}
	}else{
		if($paisGa!="todos"){
			$filtro .= 'ga:country=='.$paisGa;
		}
	}
	

	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = $metricas;

	if(empty($filtro)){
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
	                                        'sort' => '-ga:sessions',
											'start-index' => 1,
	                                        'max-results' => 50); 
	}else{
		$obj_Datos_Api -> optParams     = array(
											'dimensions' => $strDimensions,
											'filters' => $filtro,
	                                        'sort' => '-ga:sessions',
											'start-index' => 1,
	                                        'max-results' => 50); 
		
	}
	 
	$obj_Datos_Api -> Construccion();

	$topPais = 'Spain';

	//Recorremos la array que traemos de la api
	$strAux = "[['" . $trans->__('Paises', false) . "', '" . $trans->__('Sesiones', false) . "', '" . $trans->__('Ratio', false) . "'],";

	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		//$strAuxb .= '[\''.str_replace("'", " ", $obj_Datos_Api->Valor($strDimensionsN,$x)).'\','.$obj_Datos_Api->Valor("pageviews",$x).','.$obj_Datos_Api->Valor("sessions",$x).','.$obj_Datos_Api->Valor("bounceRate",$x).','.$obj_Datos_Api->Valor("avgSessionDuration",$x).','.$obj_Datos_Api->Valor("pageviewsPerSession",$x).'],';
		if ($obj_Datos_Api->Valor("country",$x) != "(not set)") {
			$strAuxb .= '[\''.str_replace("'", " ", $obj_Datos_Api->Valor($strDimensionsN,$x)).'\','.$obj_Datos_Api->Valor("sessions",$x).', '.number_format(($obj_Datos_Api->Valor("sessions",$x)*100)/$obj_Datos_Api->Total("sessions",$x), 2,'.','').' ],';			

		}
	}

	$strAuxb .= '[\' \',0, 0 ]';			
	//$strAuxb = substr($strAuxb, 0, -1);
	//Montando el json
	
	$strAux .= $strAuxb;
	$strAux .= "]";

	if($strTipo == "paisPrin"){
		$strAux = $obj_Datos_Api->Valor('country',1);
	}

    return $strAux;

}
//GEOCHART

//Funcion para traer las iso del user
function contener_iso($pais){
  $devuelve = array();
  $c = Nuevo_PDO();
  if($stmt = $c->prepare("SELECT LOCP_CODIGO
                          FROM dbo.LOC_PAISES 
                          WHERE LOCP_COUNTRY = :pais")){
      $result = $stmt->execute(array(':pais' => $pais));
      if ($result && $stmt->rowCount() != 0){
        $count = 0;
        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
          $count++;
          $id = $fila[0];
          $devuelve["elementos"][$id]["iso"] = $fila[0];
          $iso = $fila[0];
        }
        $devuelve["cantidad"]=$count;
      }else{
        $devuelve["cantidad"]=0;
      }
  }
  return trim($iso);
}

//CONTENIDO_visto

function contenido_datatable_sustitucion_arbol($straux, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idioma, $strFiltro, $strApp){

	$filtro = "ga:dimension1==".$idioma;
	if($paisGa!="todos"){
		$filtro .= ";ga:country==".$paisGa;
	}
	/*if ($strFiltro!="no"){
		$filtro .= ";".$strFiltro;
	}*/

	if($strApp == 0){
		$metricas = 'ga:pageviews';
		$pagesnom  = 'pageviews';
	}else{
		$metricas = 'ga:screenviews';
		$pagesnom  = 'screenviews';
	}

	$array_Ordenacion = array($straux,$pagesnom);
	$DirOrdenacion='';         
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = $metricas;		
	$obj_Datos_Api -> optParams    = array(
										'dimensions' => 'ga:'.$straux,
										'filters' => $filtro,
		                     			'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
										'start-index' => $DataTables_start+1,
		                                'max-results' => $DataTables_length);  		
	$obj_Datos_Api -> Construccion();

	$ndimension = substr ($straux, strlen($straux) - 1, strlen($straux) - 1);
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"data":[';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		$strA .= '[';
		//El primer valor va a depender de la perspectiva que estamos pintando
		if($obj_Datos_Api->Valor($straux,$x)=="(not set)"){
			$nombremostrar = "Otros";
			$nom = $nombremostrar;
		}else{
			$nombremostrar = $obj_Datos_Api->Valor($straux,$x);
			$nom = "";
			$ft = 0;
			for ($i=0; $i < strlen($nombremostrar) ; $i++) { 
			 	
				if($ft==1){
					$nom .= $nombremostrar[$i];
				}
				if($nombremostrar[$i] == "|"){
					$ft=1;
				}
			}
		}
		$strA .= '"<a class=\'linkdim\' name=\''.$obj_Datos_Api->Valor($straux,$x).'\' onClick=\'cargar_datatable_arbol('.$ndimension.',this.name)\' >'.$nom.'</a>",';
		$strA .= '"'.$obj_Datos_Api->Valor($pagesnom,$x).'"';
		$strA .= ']';	  
		//Añadimos una "," si no es el último registro a pintar
		if ($x < $obj_Datos_Api->NumValores()) {
			$strA .= ','; 
		}
	}
	$strA .= ']}';


    return $strA;

}


//Cabeceras Idioma
function contenido_datos_extra($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro, $strApp){


	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	if($strApp == 0){
		$metricas = 'ga:pageviews';
		$metricnom = 'pageviews';
	}else{
		$metricas = 'ga:screenviews';
		$metricnom = 'screenviews';
	}

	//TOTALES PARA sesiones
	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = $metricas;
	if(empty($filtro)){
		$obj_Datos_Api -> optParams    = array(
										'dimensions' => 'ga:dimension1',
		                                'sort' => '-'.$metricas,
										'start-index' => 1,
		                                'max-results' => 50);  
	}else{
		$obj_Datos_Api -> optParams    = array(
										'dimensions' => 'ga:dimension1',
										'filters' => $filtro,
		                                'sort' => '-'.$metricas,
										'start-index' => 1,
		                                'max-results' => 50);  
	}
	$obj_Datos_Api -> Construccion();


	//TOTALES PARA EVENTOS
	$obj_Datos_Api_eventos = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_eventos -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_eventos -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_eventos -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_eventos -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_eventos -> metrics              = 'ga:totalEvents';		
	if(empty($filtro)){
		$obj_Datos_Api_eventos -> optParams    = array(
										'dimensions' => 'ga:eventCategory',
		                                'sort' => '-ga:totalEvents',
										'start-index' => 1,
		                                'max-results' => 1000); 
	}else{
		$obj_Datos_Api_eventos -> optParams    = array(
										'dimensions' => 'ga:eventCategory',
										'filters' => $filtro,
		                                'sort' => '-ga:totalEvents',
										'start-index' => 1,
		                                'max-results' => 1000); 
	}
	
	$obj_Datos_Api_eventos -> Construccion();
	//De Aquí hay que coger Descarga, Buscador y Externo
	$tot_descarga = 0;
	$tot_externo  = 0;
	$tot_busqueda = 0;

	$tarta_ajax = "\"[";
	$pags_idioma = 0;
	$idioma_mayor = "";
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		if($obj_Datos_Api->Valor("dimension1",$x)!="(not set)"){

			//$tarta_ajax .= "{";

			$c = Nuevo_PDO();
			if($stmt = $c->prepare("SELECT LOCI_IDIOMA
			                        FROM dbo.LOC_IDIOMASNAVEGADOR
			                        WHERE LOCI_CODIGO LIKE :idioma")){
			    $result = $stmt->execute(array(':idioma' => $obj_Datos_Api->Valor("dimension1",$x)));
			    if ($result && $stmt->rowCount() != 0){
			      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
			      	$idioma = $fila[0];
			      }
			    }else{
			    	$idioma = $obj_Datos_Api->Valor("dimension1",$x);

			    }
			}

			if($obj_Datos_Api->Valor($metricnom,$x)>$pags_idioma){
				$pags_idioma = $obj_Datos_Api->Valor($metricnom,$x);
				$idioma_mayor = ucwords($idioma);
			}

			$tarta_ajax  .= "{"."value: ".$obj_Datos_Api->Valor($metricnom,$x).", color: '#6d7578', highlight: '#a2a2a2', label:'".ucwords($idioma)."'"."},";

		}	
		
	}


	$tarta_ajax = substr($tarta_ajax, 0, -1);
	$tarta_ajax .= "]\"";


	for ($x=1; $x<=$obj_Datos_Api_eventos->NumValores(); $x++) {

		//eventos
		if($obj_Datos_Api_eventos->valor("eventCategory",$x)=="descarga"){
			$tot_descarga = $obj_Datos_Api_eventos->valor("totalEvents",$x);
		}
		if($obj_Datos_Api_eventos->valor("eventCategory",$x)=="buscador"){
			$tot_busqueda = $obj_Datos_Api_eventos->valor("totalEvents",$x);
		}
		if($obj_Datos_Api_eventos->valor("eventCategory",$x)=="externo"){
			$tot_externo  = $obj_Datos_Api_eventos->valor("totalEvents",$x);
		}

	}


	$strA = '{';
	
	$strA .= '"total_pagvistas": "' . $obj_Datos_Api->Total($metricnom) . '",';
	$strA .= '"pags_idioma": "'     . $pags_idioma . '",';
	$strA .= '"idioma_mayor": "'    . $idioma_mayor. '",';
	$strA .= '"tot_descarga": "'    . $tot_descarga. '",';
	$strA .= '"tot_busqueda": "'    . $tot_busqueda. '",';
	$strA .= '"tot_externo": "'     . $tot_externo. '"';
	//$strA .= '"tarta_ajax": '      . $tarta_ajax . '';
	 
	
	$strA .= '}';
	return $strA;
}


function contenido_cabeceras_idiomas($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro, $strApp){
	global $trans;

	if($strApp == 0){
		$metricas = 'ga:pageviews';
		$metricnom = 'pageviews';
	}else{
		$metricas = 'ga:screenviews';
		$metricnom = 'screenviews';
	}
	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = $metricas;

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	if(empty($filtro)){
		$obj_Datos_Api -> optParams    = array(
										'dimensions' => 'ga:dimension1',
		                                'sort' => '-'.$metricas,
										'start-index' => 1,
		                                'max-results' => 50);  
	}else{
		$obj_Datos_Api -> optParams    = array(
										'dimensions' => 'ga:dimension1',
										'filters' => $filtro,
		                                'sort' => '-'.$metricas,
										'start-index' => 1,
		                                'max-results' => 50);  
	}
	$obj_Datos_Api -> Construccion();

	$strA = '<ul class="nav nav-tabs" role="tablist" >';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		if($obj_Datos_Api->Valor("dimension1",$x)!="(not set)"){
			if ($x==1) { $activo = 'class="active"'; $mostrar = $obj_Datos_Api->Valor("dimension1",$x);}else{$activo="";}	

			$c = Nuevo_PDO();
			if($stmt = $c->prepare("SELECT LOCI_IDIOMA
			                        FROM dbo.LOC_IDIOMASNAVEGADOR
			                        WHERE LOCI_CODIGO LIKE :idioma")){
			    $result = $stmt->execute(array(':idioma' => $obj_Datos_Api->Valor("dimension1",$x)));
			    if ($result && $stmt->rowCount() != 0){
			      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
			      	$idioma = $fila[0];
			      }
			    }else{
			    	$idioma = $obj_Datos_Api->Valor("dimension1",$x);
			    }
			}

			$strA .= '<li role="presentation" '.$activo.'><a class="seleccionar_idioma" role="tab" data-toggle="tab" name="'.$obj_Datos_Api->Valor("dimension1",$x).'">'.$trans->__(ucwords($idioma), false).' ('.porcentaje_sesiones($obj_Datos_Api->Valor($metricnom,$x),$obj_Datos_Api->Total($metricnom,$x)).'%)</a></li>';
		}
	}
	$strA .= '</ul>';
	$strA .= '<input type="hidden" id="txt_idioma" value="'.$mostrar.'" />';
	$strA .= '<script>$(".seleccionar_idioma").on("click",function(){$("#txt_idioma").val($(this).attr("name"));cargador_desdecab();})</script>';
	return $strA;

}
//Para crear el json del orgchart
function contenido_datatable_eventos($straux, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idioma, $strFiltro, $strApp){


		$filtro = "ga:eventAction==".$idioma.";ga:eventCategory==".$straux;
		if($paisGa!="todos"){
			$filtro .= ";ga:country==".$paisGa;
		}

		if ($strFiltro!="no"){
			$filtro .= ";".$strFiltro;
		}

		$array_Ordenacion = array('eventLabel','totalEvents');
		$DirOrdenacion='';         
		if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}


		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
		$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
		$obj_Datos_Api -> metrics              = 'ga:totalEvents';		
		$obj_Datos_Api -> optParams    = array(
											'dimensions' => 'ga:eventCategory,ga:eventAction,ga:eventLabel',
											'filters' => $filtro,
			                     			'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											'start-index' => $DataTables_start+1,
			                                'max-results' => $DataTables_length);  		
		$obj_Datos_Api -> Construccion();

		
		$strA = '{"draw":' . $DataTables_draw .',';
		$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
		$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .',';
		$strA .= '"data":[';
		for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
			$strA .= '[';
			//El primer valor va a depender de la perspectiva que estamos pintando
			$strA .= '"'.str_replace('"', ' ', $obj_Datos_Api->Valor("eventLabel",$x)).'",';
			$strA .= '"'.$obj_Datos_Api->Valor("totalEvents",$x).'"';
			$strA .= ']';	  
			//Añadimos una "," si no es el último registro a pintar
			if ($x < $obj_Datos_Api->NumValores()) {
				$strA .= ','; 
			}
		}
		$strA .= ']}';


        return $strA;

}

//Para crear el json del orgchart
function contenido_datatable_chart($filtrochart,$filtrodimension, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idioma,$strFiltro,$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt,$strApp){


		if($paisGa=="todos"){
	    	$filtro = "ga:dimension1==".$idioma;
	    }else{
	    	$filtro = "ga:dimension1==".$idioma.";ga:country==".$paisGa;
	    } 
	    if ($strFiltro!="no"){
			$filtro .= ";".$strFiltro;
		}

	    if($filtrochart!=""){
	    	$filtro .= ";ga:dimension".$filtrodimension."==".$filtrochart;
	    }

	    if($strApp == 0){
	    	$metricas  = 'ga:pageviews,ga:avgTimeOnPage,ga:bounceRate';
	    	$metricnom = 'pageviews';
	    	$dimension = 'ga:pagePath,ga:pageTitle';
	    	$dimname   = 'pagePath';
	    	$avgname   = 'avgTimeOnPage';
	    }else{
	    	$metricas  = 'ga:screenviews,ga:timeOnScreen,ga:bounceRate';
	    	$metricnom = 'screenviews';
	    	$dimension = 'ga:screenName,ga:pageTitle';
	    	$dimname   = 'screenName';
	    	$avgname   = 'timeOnScreen';
	    }

		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
		$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
		$obj_Datos_Api -> metrics              = $metricas;
		$obj_Datos_Api -> optParams    = array(
											'dimensions' => $dimension,
											'filters' => $filtro,
			                                'sort' => '-ga:'.$metricnom,
											'start-index' => $DataTables_start+1,
			                                'max-results' => $DataTables_length); 
		$obj_Datos_Api -> Construccion();

		//Si existe comparación
		if($blnComparacion == 'true'){

			$obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
			$obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado;
			$obj_Datos_Api_ant -> idVista              = "ga:".$idVistaAnalytics;
			$obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt;
			$obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt;
			$obj_Datos_Api_ant -> metrics              = $metricas;
			$obj_Datos_Api_ant -> optParams    = array(
												'dimensions' => $dimension,
												'filters' => $filtro,
				                                'sort' => '-ga:'.$metricnom,
												'start-index' => $DataTables_start+1,
				                                'max-results' => $DataTables_length); 
			$obj_Datos_Api_ant -> Construccion();

			//Cargamos una array para luego comparar con ella
			$comparray = array();
			for ($x=1; $x<=$obj_Datos_Api_ant->NumValores(); $x++) {
				$comparray[$x][$dimname]         = $obj_Datos_Api_ant->Valor($dimname,$x); 
				$comparray[$x][$avgname]    = $obj_Datos_Api_ant->Valor($avgname,$x); 
				$comparray[$x][$metricnom]         = $obj_Datos_Api_ant->Valor($metricnom,$x); 
				$comparray[$x]["bounceRate"]       = $obj_Datos_Api_ant->Valor("bounceRate",$x); 
				$comparray[$x][$avgname."_f"]  = $obj_Datos_Api_ant->ValorF($avgname,$x); 
				$comparray[$x][$metricnom."_f"]    = $obj_Datos_Api_ant->ValorF($metricnom,$x); 
				$comparray[$x]["bounceRate_f"]     = $obj_Datos_Api_ant->ValorF("bounceRate",$x); 
			}	

			$strA = '{"draw":' . $DataTables_draw .',';
			$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
			$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .','	;
			$strA .= '"data":[';
			for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
				$strA .= '[';
				//recorremos la array en busca de su comparación
				$sw=0;
				foreach ($comparray as $key => $comp) {
					if($comp[$dimname] == $obj_Datos_Api->Valor($dimname,$x) ){
						$durme_ant     = $comp[$avgname]; 
						$visitas_ant   = $comp[$metricnom]; 
						$ratio_ant     = $comp["bounceRate"];
						$durme_ant_f   = $comp[$avgname."_f"]; 
						$visitas_ant_f = $comp[$metricnom."_f"]; 
						$ratio_ant_f   = $comp["bounceRate_f"];
						$sw=1;
					}
				}
				if($sw==0){
					$durme_ant     = 0;
					$visitas_ant   = 0;
					$ratio_ant     = 0;
					$durme_ant_f   = 0;
					$visitas_ant_f = 0;
					$ratio_ant_f   = 0;
				}

				$strA .= '"'.$obj_Datos_Api->Valor($dimname,$x).'",';
				$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->Valor("pageTitle",$x).'</span>",';
				$strA .= '"'.contenido_comparador($obj_Datos_Api->Valor($metricnom,$x),$visitas_ant,$obj_Datos_Api->ValorF($metricnom,$x),$visitas_ant_f).$obj_Datos_Api->ValorF($metricnom,$x).'<span class=\'porcDT\'> ('.porcentaje_sesiones($obj_Datos_Api->Valor($metricnom,$x),$obj_Datos_Api->Total($metricnom)).'%)</span> ",';
				$strA .= '"'.contenido_comparador($obj_Datos_Api->Valor($avgname,$x),$durme_ant,$obj_Datos_Api->ValorF($avgname,$x),$durme_ant_f).$obj_Datos_Api->ValorF($avgname,$x).'",';
				$strA .= '"'.contenido_comparador($ratio_ant,$obj_Datos_Api->Valor("bounceRate",$x),$ratio_ant_f,$obj_Datos_Api->ValorF("bounceRate",$x),1).$obj_Datos_Api->ValorF("bounceRate",$x).'"';
				$strA .= ']';	  
				//Añadimos una "," si no es el último registro a pintar
				if ($x < $obj_Datos_Api->NumValores()) {
					$strA .= ','; 
				}
			}
			$strA .= ']}';

		}else{

			$strA = '{"draw":' . $DataTables_draw .',';
			$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
			$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .','	;
			$strA .= '"data":[';
			for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
				$strA .= '[';
				//El primer valor va a depender de la perspectiva que estamos pintando
				$strA .= '"'.$obj_Datos_Api->Valor("pagePath",$x).'",';
				$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->Valor("pageTitle",$x).'</span>",';
				$strA .= '"'.$obj_Datos_Api->ValorF($metricnom,$x).'<span class=\'porcDT\'> ('.porcentaje_sesiones($obj_Datos_Api->Valor($metricnom,$x),$obj_Datos_Api->Total($metricnom)).'%)</span> ",';
				$strA .= '"'.$obj_Datos_Api->ValorF($avgname,$x).'",';
				$strA .= '"'.$obj_Datos_Api->ValorF("bounceRate",$x).'"';
				$strA .= ']';	  
				//Añadimos una "," si no es el último registro a pintar
				if ($x < $obj_Datos_Api->NumValores()) {
					$strA .= ','; 
				}
			}
			$strA .= ']}';

		}

		
		


        return $strA;

}

function contenido_comparador($valor,$valor_ant,$valor_f,$valor_ant_f,$reves=0){
	$post="";
	if($reves==1){
		$aux = $valor_f;
		$valor_f = $valor_ant_f;
		$valor_ant_f = $aux;
	}
	if($valor > $valor_ant){
		$devuelve = "<img class='imgdrip' src='../public/images/flecha_arriba.gif' title='Actual: ".$valor_f.$post." | Anterior: ".$valor_ant_f.$post."'>";
	}elseif($valor< $valor_ant){
		$devuelve = "<img class='imgdrip' src='../public/images/flecha_abajo.gif' title='Actual: ".$valor_f.$post." | Anterior: ".$valor_ant_f.$post."'>";
	}else{
		$devuelve = "<img class='imgdrip' src='../public/images/flecha_igual.gif' title='Actual: ".$valor_f.$post." | Anterior: ".$valor_ant_f.$post."'>";
	}
	return $devuelve;
}

//Para crear el json del orgchart
function contenido_datatable_chart_totales($filtrochart,$filtrodimension, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idioma,$strFiltro, $strApp){


		if($paisGa=="todos"){
	    	$filtro = "ga:dimension1==".$idioma;
	    }else{
	    	$filtro = "ga:dimension1==".$idioma.";ga:country==".$paisGa;
	    } 
	    if ($strFiltro!="no"){
			$filtro .= ";".$strFiltro;
		}

	    if($filtrochart!=""){
	    	$filtro .= ";ga:dimension".$filtrodimension."==".$filtrochart;
	    }

	    if($strApp == 0){
	    	$metricas = 'ga:pageviews,ga:avgTimeOnPage,ga:bounceRate';
	    	$metricnom = 'pageviews';
	    	$dimension = 'ga:pagePath,ga:pageTitle';
	    	$dimname   = 'pagePath';
	    	$avgpage   = 'avgTimeOnPage';
	    }else{
	    	$metricas = 'ga:screenviews,ga:timeOnScreen,ga:bounceRate';
	    	$metricnom = 'screenviews';
	    	$dimension = 'ga:screenName,ga:pageTitle';
	    	$dimname   = 'screenName';
	    	$avgpage   = 'timeOnScreen';
	    }


		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
		$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
		$obj_Datos_Api -> metrics              = $metricas;
		$obj_Datos_Api -> optParams    = array(
											'dimensions' => $dimension,
											'filters' => $filtro,
			                                'sort' => '-ga:'.$metricnom,
											'start-index' => 1,
			                                'max-results' => 10000); 
		$obj_Datos_Api -> Construccion();

		/*$mediaDur = $obj_Datos_Api->Total($avgpage,$x) / $obj_Datos_Api->NumValores();
		
		if($strApp == 1){
			$mediareb = 0;
		}else{
			
			$mediareb = $obj_Datos_Api->Total("bounceRate",$x) / $obj_Datos_Api->NumValores();
		}*/
		

		$strA = '{';
		$strA .= '"total_visitas":"'.$obj_Datos_Api->Total($metricnom,$x).'",';
		$strA .= '"total_durmedia":"'.$obj_Datos_Api->Media($avgpage,$x).'",';
		$strA .= '"total_tasrebote":"'.$obj_Datos_Api->Total("bounceRate",$x) .'",';
		$strA .= '"total_visitas_f":"'.$obj_Datos_Api->TotalF($metricnom,$x).'",';
		$strA .= '"total_durmedia_f":"'.$obj_Datos_Api->MediaF($avgpage,$x).'",';
		$strA .= '"total_tasrebote_f":"'.$obj_Datos_Api->TotalF("bounceRate",$x) .'"';
		$strA .= '}';

        return $strA;

}


//Para crear el json del orgchart
/*function Contenido_chart($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idioma ){

		$dimensiones = "ga:dimension4,ga:dimension5,ga:dimension6,ga:dimension7";

		if($paisGa=="todos"){
	    	$filtro = "ga:dimension1==".$idioma;
	    }else{
	    	$filtro = "ga:dimension1==".$idioma.";ga:country==".$paisGa;
	    } 

		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
		$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
		$obj_Datos_Api -> metrics              = 'ga:pageviews';
		$obj_Datos_Api -> optParams            = array(
												'dimensions' => $dimensiones,
												'filters' => $filtro,
		                                        'sort' => $dimensiones,
												'start-index' => 1,
		                                        'max-results' => 50);  
		$obj_Datos_Api -> Construccion();

		$strAuxb ="";
		$strAuxb .= '[{v:\'web\',f:\'WEB<div style="color:black;"><p>'.$obj_Datos_Api->TotalF('pageviews').' pag.</p>100%</p></div>\'}, \'\', \'Tooltip\'],';     
		for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
			//Dimension 4
			if ($obj_Datos_Api->Valor('dimension4',$x) != "(not set)"){
				$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension4',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension4',$x)).'<div style="color:black;"><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \'web\', \'Tooltip\'],';     
			}else{
				$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension4',$x).'\',f:\'<p style="color:gray; font-style: italic; ">Otros</p><div style="color:black; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \'web\', \'Tooltip\'],';     
			}
			//Dimension 5
			if ($obj_Datos_Api->Valor('dimension5',$x) != "(not set)"){
				$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension5',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension5',$x)).'<div" style="color:black;"><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension5',$obj_Datos_Api->Valor('dimension5',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension5',$obj_Datos_Api->Valor('dimension5',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension4',$x).'\', \'Tooltip\'],';     
			}
			//Dimension 6
			if ($obj_Datos_Api->Valor('dimension6',$x) != "(not set)"){
				$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension6',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension6',$x)).'<div" style="color:black;"><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension6',$obj_Datos_Api->Valor('dimension6',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension6',$obj_Datos_Api->Valor('dimension6',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension5',$x).'\', \'Tooltip\'],';     
			}
			//Dimension 7
			if ($obj_Datos_Api->Valor('dimension7',$x) != "(not set)"){
				$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension7',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension7',$x)).'<div" style="color:black;"><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension7',$obj_Datos_Api->Valor('dimension7',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension7',$obj_Datos_Api->Valor('dimension7',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension6',$x).'\', \'Tooltip\'],';     
			}
		}
		$strAuxb = substr($strAuxb, 0, -1);
		//Montando el json
		$strAux = "[";
		$strAux .= $strAuxb;
		$strAux .= "]";
        return $strAux;

}*/

//Para crear el json del orgchart
/*function Contenido_chart($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idioma ){

		$dimensiones = "ga:dimension4,ga:dimension5,ga:dimension6,ga:dimension7";

		if($paisGa=="todos"){
	    	$filtro = "ga:dimension1==".$idioma;
	    }else{
	    	$filtro = "ga:dimension1==".$idioma.";ga:country==".$paisGa;
	    } 

		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
		$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
		$obj_Datos_Api -> metrics              = 'ga:pageviews';
		$obj_Datos_Api -> optParams            = array(
												'dimensions' => $dimensiones,
												'filters' => $filtro,
		                                        'sort' => $dimensiones,
												'start-index' => 1,
		                                        'max-results' => 50);  
		$obj_Datos_Api -> Construccion();

		$otros = 0;
		$strAuxb ="";
		$strAuxb .= '[{v:\'web\',f:\'WEB<div onClick="cargar_datatable_arbol(0,0)" style="color:black; cursor:pointer; "><p>'.$obj_Datos_Api->TotalF('pageviews').' pag.</p>100%</p></div>\'}, \'\', \'Tooltip\'],';     
		for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
			//Dimension 4
			if ($obj_Datos_Api->Valor('dimension4',$x) != "(not set)"){

				if(contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)) <= 1){
					$otros++;
				}else{
					$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension4',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension4',$x)).'<div id="'.$obj_Datos_Api->Valor('dimension4',$x).'" onClick="cargar_datatable_arbol(4,this.id)" style="color:black; cursor:pointer;"><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \'web\', \'Tooltip\'],';     
				}

			}else{
				//otros
				$otros += contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x));
				$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension4',$x).'\',f:\'<p style="color:gray; font-style: italic; ">Otros</p><div style="color:black; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \'web\', \'Tooltip\'],';     
			}
			//Dimension 5
			if ($obj_Datos_Api->Valor('dimension5',$x) != "(not set)"){

				//if(contenido_chart_calcularpags($obj_Datos_Api,'dimension5',$obj_Datos_Api->Valor('dimension5',$x)) <= 1){
					//$otros++;
				//}else{
					$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension5',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension5',$x)).'<div id="'.$obj_Datos_Api->Valor('dimension5',$x).'" onClick="cargar_datatable_arbol(5,this.id)" style="color:black; cursor:pointer; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension5',$obj_Datos_Api->Valor('dimension5',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension5',$obj_Datos_Api->Valor('dimension5',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension4',$x).'\', \'Tooltip\'],';    
				//}

			}
			//Dimension 6
			if ($obj_Datos_Api->Valor('dimension6',$x) != "(not set)"){

				//if(contenido_chart_calcularpags($obj_Datos_Api,'dimension6',$obj_Datos_Api->Valor('dimension6',$x)) <= 1){
					//$otros++;
				//}else{
					$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension6',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension6',$x)).'<div id="'.$obj_Datos_Api->Valor('dimension6',$x).'" onClick="cargar_datatable_arbol(6,this.id)" style="color:black; cursor:pointer; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension6',$obj_Datos_Api->Valor('dimension6',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension6',$obj_Datos_Api->Valor('dimension6',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension5',$x).'\', \'Tooltip\'],';     
				//}

			}
			//Dimension 7
			if ($obj_Datos_Api->Valor('dimension7',$x) != "(not set)"){

				//if(contenido_chart_calcularpags($obj_Datos_Api,'dimension7',$obj_Datos_Api->Valor('dimension7',$x)) <= 1){
					//$otros++;
				//}else{
					$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension7',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension7',$x)).'<div id="'.$obj_Datos_Api->Valor('dimension7',$x).'" onClick="cargar_datatable_arbol(7,this.id)" style="color:black; cursor:pointer; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension7',$obj_Datos_Api->Valor('dimension7',$x)).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension7',$obj_Datos_Api->Valor('dimension7',$x)),$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension6',$x).'\', \'Tooltip\'],';     
				//}

			}
		}

		//y ahora añadimos el otros acumulado
		$strAuxb .= '[{v:\'otros\',f:\'<p style="color:gray; font-style: italic; ">Otros</p><div style="color:black; "><p>'.$otros.' pag.</p>'.porcentaje_sesiones($otros,$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \'web\', \'Tooltip\'],';     

		$strAuxb = substr($strAuxb, 0, -1);
		//Montando el json
		$strAux = "[";
		$strAux .= $strAuxb;
		$strAux .= "]";
        return $strAux;

}*/

function Contenido_chart($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idioma, $strFiltro, $strApp){

		$dimensiones = "ga:dimension4,ga:dimension5,ga:dimension6,ga:dimension7";

		if($paisGa=="todos"){
	    	$filtro = "ga:dimension1==".$idioma;
	    }else{
	    	$filtro = "ga:dimension1==".$idioma.";ga:country==".$paisGa;
	    } 
	    if ($strFiltro!="no"){
			$filtro .= ';'.$strFiltro;
		}

		if($strApp == 0){
			$metricas = 'ga:pageviews';
			$metricnom = 'pageviews';
		}else{
			$metricas = 'ga:screenviews';
			$metricnom = 'screenviews';
		}

		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
		$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
		$obj_Datos_Api -> metrics              = $metricas;
		$obj_Datos_Api -> optParams            = array(
												'dimensions' => $dimensiones,
												'filters' => $filtro,
		                                        'sort' => $dimensiones,
												'start-index' => 1,
		                                        'max-results' => 200);  
		$obj_Datos_Api -> Construccion();

		$min = 1;

		$otros = 0;
		$strAuxb ="";
		//echo $metricas.'  -  '. $dimensiones.'  -  '.$filtro;
		//$strAuxb .= '[{v:\'web\',f:\'WEB<div onClick="cargar_datatable_arbol(0,0)" style="color:black; cursor:pointer; "><p>'.$obj_Datos_Api->TotalF($metricnom).' pag.</p>100%</p></div>\'}, \'\', \'Tooltip\'],';     
		for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
			//Dimension 4
			if ($obj_Datos_Api->Valor('dimension4',$x) != "(not set)"){

				if(contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x),$metricnom) <= $min){
					$otros += contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x),$metricnom);
				}else{
					$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension4',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension4',$x)).'<div id="'.$obj_Datos_Api->Valor('dimension4',$x).'" onClick="cargar_datatable_arbol(4,this.id)" style="color:black; cursor:pointer;"><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x),$metricnom).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x),$metricnom),$obj_Datos_Api->Total($metricnom)).'%</p></div>\'}, \'web\', \'Tooltip\'],';     
				
					//Dimension 5
					if ($obj_Datos_Api->Valor('dimension5',$x) != "(not set)"){

						if(contenido_chart_calcularpags($obj_Datos_Api,'dimension5',$obj_Datos_Api->Valor('dimension5',$x),$metricnom) <= $min){
							//$otros++;
						}else{
							$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension5',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension5',$x)).'<div id="'.$obj_Datos_Api->Valor('dimension5',$x).'" onClick="cargar_datatable_arbol(5,this.id)" style="color:black; cursor:pointer; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension5',$obj_Datos_Api->Valor('dimension5',$x),$metricnom).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension5',$obj_Datos_Api->Valor('dimension5',$x),$metricnom),$obj_Datos_Api->Total($metricnom)).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension4',$x).'\', \'Tooltip\'],';    
						}

					}
					//Dimension 6
					if ($obj_Datos_Api->Valor('dimension6',$x) != "(not set)"){

						if(contenido_chart_calcularpags($obj_Datos_Api,'dimension6',$obj_Datos_Api->Valor('dimension6',$x),$metricnom) <= $min){
							//$otros++;
						}else{
							$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension6',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension6',$x)).'<div id="'.$obj_Datos_Api->Valor('dimension6',$x).'" onClick="cargar_datatable_arbol(6,this.id)" style="color:black; cursor:pointer; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension6',$obj_Datos_Api->Valor('dimension6',$x),$metricnom).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension6',$obj_Datos_Api->Valor('dimension6',$x),$metricnom),$obj_Datos_Api->Total($metricnom)).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension5',$x).'\', \'Tooltip\'],';     
						}

					}
					//Dimension 7
					if ($obj_Datos_Api->Valor('dimension7',$x) != "(not set)"){

						if(contenido_chart_calcularpags($obj_Datos_Api,'dimension7',$obj_Datos_Api->Valor('dimension7',$x),$metricnom) <= $min){
							//$otros++;
						}else{
							$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension7',$x).'\',f:\''.contenido_chart_formatearnombre($obj_Datos_Api->Valor('dimension7',$x)).'<div id="'.$obj_Datos_Api->Valor('dimension7',$x).'" onClick="cargar_datatable_arbol(7,this.id)" style="color:black; cursor:pointer; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension7',$obj_Datos_Api->Valor('dimension7',$x),$metricnom).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension7',$obj_Datos_Api->Valor('dimension7',$x),$metricnom),$obj_Datos_Api->Total($metricnom)).'%</p></div>\'}, \''.$obj_Datos_Api->Valor('dimension6',$x).'\', \'Tooltip\'],';     
						}

					}


				}

			}else{
				//otros
				$otros += contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x),$metricnom);
				$strAuxb .= '[{v:\''.$obj_Datos_Api->Valor('dimension4',$x).'\',f:\'<p style="color:gray; font-style: italic; ">Otros</p><div style="color:black; "><p>'.contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x),$metricnom).' pag.</p>'.porcentaje_sesiones(contenido_chart_calcularpags($obj_Datos_Api,'dimension4',$obj_Datos_Api->Valor('dimension4',$x),$metricnom),$obj_Datos_Api->Total($metricnom)).'%</p></div>\'}, \'web\', \'Tooltip\'],';     
			}
			
		}

		//y ahora añadimos el otros acumulado
		//$strAuxb .= '[{v:\'otros\',f:\'<p style="color:gray; font-style: italic; ">Otros</p><div style="color:black; "><p>'.$otros.' pag.</p>'.porcentaje_sesiones($otros,$obj_Datos_Api->Total('pageviews')).'%</p></div>\'}, \'web\', \'Tooltip\'],';     

		$strAuxb = substr($strAuxb, 0, -1);
		//Montando el json
		$strAux = "[";
		$strAux .= $strAuxb;
		$strAux .= "]";
        return $strAux;

}


//Calculo de total de páginas de un elemento del gráfico
function contenido_chart_calcularpags($obj_Datos_Api,$dimensionX,$elemento,$metrica){
	//Recorremos toda la estructura de resultados y sumamos las páginas vistas de aquellos registros cuyo valor en la dimensión x coincida con el valor del elemento
	//echo $metrica."<br>";
	$intSumatorio =0;
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		if ($obj_Datos_Api->Valor($dimensionX,$x) == $elemento) {
			$intSumatorio +=  $obj_Datos_Api->Valor($metrica,$x);
		}
		
	}
	return $intSumatorio;
}

function contenido_chart_formatearnombre($nombre){
	$result="";
	$sw=0;

	for ($i = 0; $i < strlen($nombre); $i++) {

		if($sw==1){
			$result .= $nombre[$i];
		}

		if ($nombre[$i]=="|"){
			$sw=1;
		}

		
	};

	return $result;
}


//CONTENIDO_visto


function traducir_pais($pais){

  global $trans;

	if($trans->getLanguage() == 'en'){
		return $pais;
	}elseif ($trans->getLanguage() == 'es') {
		$devuelve = array();
		$c = Nuevo_PDO();
		if($stmt = $c->prepare("SELECT LOCP_COUNTRY, LOCP_PAIS
		                        FROM dbo.LOC_PAISES
		                        WHERE LOCP_COUNTRY LIKE :pais")){
		    $result = $stmt->execute(array(':pais' => $pais));
		    if ($result && $stmt->rowCount() != 0){

		      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
		      	$def = $fila[1];
		      }

		      return $def;

		    }else{

		    	return $pais;

		    }
		}else{

			return $pais;

			}
	}else{
		return $trans->__($pais, false);
	}
		
}


function traducir_paises($array_paises){

	foreach ($array_paises as $key => $pais_traducir) {
		
		$devuelve = array();
		$c = Nuevo_PDO();
		if($stmt = $c->prepare("SELECT LOCP_COUNTRY, LOCP_PAIS
		                        FROM dbo.LOC_PAISES
		                        WHERE LOCP_COUNTRY LIKE :pais")){
		    $result = $stmt->execute(array(':pais' => $pais_traducir["idpais"]));
		    if ($result && $stmt->rowCount() != 0){
		      $count = 0;
		      //echo "Filas: ".$stmt->rowCount()."<br>";
		      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
		      	$array_paises[$key]["pais"] = $fila[1];
		      }
		    }
		}
		

	}

	return $array_paises;

}



function paises($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro){
	
	//if(!isset($_SESSION["paises"])){
		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
		$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
		$obj_Datos_Api -> metrics              = 'ga:sessions';
		//if ($strFiltro=="no") {
			$obj_Datos_Api -> optParams      = array(
												'dimensions' => 'ga:country',
		                                        'sort' => '-ga:sessions',
												'start-index' => 1,
		                                        'max-results' => 50); 
		/*}else{
			$strFiltro = str_replace("**","==",$strFiltro);
			$obj_Datos_Api -> optParams      = array(
												'dimensions' => 'ga:country',
												'filters' => $strFiltro,
		                                        'sort' => '-ga:sessions',
												'start-index' => 1,
		                                        'max-results' => 50); 
		}*/

		 
		$obj_Datos_Api -> Construccion();
		$array_paises = array();
		for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
			$array_paises[$x]["idpais"] = $obj_Datos_Api->Valor('country',$x);
			$array_paises[$x]["sesiones"] = $obj_Datos_Api->Valor('sessions',$x);
			$array_paises[$x]["pais"] = $obj_Datos_Api->Valor('country',$x);
			$array_paises[$x]["totales"] = $obj_Datos_Api->Total('sessions');
		}
		$array_paises = traducir_paises($array_paises);
		$_SESSION["paises"] = $array_paises;
	/*}else{
		$array_paises = $_SESSION["paises"];
	}*/
	return $array_paises;
}

function idiomas($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $strFiltro){
	
	//if(!isset($_SESSION["idiomas"])){
		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
		$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
		$obj_Datos_Api -> metrics              = 'ga:pageviews';
		//if ($strFiltro=="no" || $strFiltro=="" ) {
			$obj_Datos_Api -> optParams            = array(
													'dimensions' => 'ga:dimension1',
			                                        'sort' => '-ga:pageviews',
													'start-index' => 1,
			                                        'max-results' => 50); 
		/*}else{
			$strFiltro = str_replace("**","==",$strFiltro);
			$obj_Datos_Api -> optParams            = array(
													'dimensions' => 'ga:dimension1',
													'filters' => $strFiltro,
			                                        'sort' => '-ga:pageviews',
													'start-index' => 1,
			                                        'max-results' => 50); 
		} */

		$obj_Datos_Api -> Construccion();
		$array_idiomas = array();
		for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
			if ($obj_Datos_Api->Valor('dimension1',$x)!= "(not set)") {
				$array_idiomas[$x]["ididioma"] = $obj_Datos_Api->Valor('dimension1',$x);
				$array_idiomas[$x]["sesiones"] = $obj_Datos_Api->Valor('pageviews',$x);
				$array_idiomas[$x]["idioma"] = traducir_idioma($obj_Datos_Api->Valor('dimension1',$x));
				$array_idiomas[$x]["totales"] = $obj_Datos_Api->Total('pageviews');
			}
		}
		//$array_idiomas = traducir_idiomas($array_paises);
		$_SESSION["idiomas"] = $array_idiomas;
	/*}else{
		$array_idiomas = $_SESSION["idiomas"];
	}*/
	return $array_idiomas;
}


function Impactos1_datos_extra ( $strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro)
{
	global $trans;

	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($strPerspectiva)) {

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions='ga:source';
			$strSort='source';
			$strNombre_Dimension = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions='ga:medium';
			$strSort='medium';
			$strNombre_Dimension = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions='ga:campaign';
			$strSort='campaign';
			$strNombre_Dimension = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions='ga:deviceCategory';
			$strSort='deviceCategory';
			$strNombre_Dimension = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions='ga:browser';
			$strSort='browser';
			$strNombre_Dimension = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions='ga:userGender';
			$strSort='userGender';
			$strNombre_Dimension = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions='ga:userAgeBracket';
			$strSort='userAgeBracket';
			$strNombre_Dimension = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions='ga:country';
			$strSort='country';
			$strNombre_Dimension = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions='ga:region';
			$strSort='region';
			$strNombre_Dimension = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions='ga:city';
			$strSort='city';
			$strNombre_Dimension = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			$strNombre_Dimension = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			$strNombre_Dimension = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			$strNombre_Dimension = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			$strNombre_Dimension = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			$strNombre_Dimension = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			$strNombre_Dimension = $trans->__('Día', false);
			break;  
	}//Cierre switch 		
	
	//Construimos la llamada a la api
	$obj_Datos_Api_admin = new Datos_Api_Admin(); // Instanciamos la clase Datos_Api_Admin
    $obj_Datos_Api_admin -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_admin -> idVista              = $idVistaAnalytics ;
    $obj_Datos_Api_admin -> TipoLlamada          = 'GOALS_VISTA' ;   
    $obj_Datos_Api_admin -> Construccion();

    $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_METRICAS();
	$ARRAY_GOALS_VISTA_ORDENADOS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS();


	//Hacemos tantas llamadas al api de datos como elementos tenga el array de métricas.  En cada llamada sacaremos cinco objetivos (10 méticas)
	$array_Datos_Api = array();

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa.';';
	}
	if($idIdioma!="todos"){
		//if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma.';';
	}
	if ($strFiltro!="no"){
		//if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro.';';
	}

	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS); $intI++) {

		//print($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI]);
	    //Creación objeto, parametrización -------------------------------------------------------
	    $array_Datos_Api[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $array_Datos_Api[$intI] -> strProyectoAsociado  = $strProyectoAsociado ;
	    $array_Datos_Api[$intI] -> idVista              ='ga:'. $idVistaAnalytics ;
	    $array_Datos_Api[$intI] -> startdate            = $datFechaInicioFiltro ;
	    $array_Datos_Api[$intI] -> enddate              = $datFechaFinFiltro ;
	    $array_Datos_Api[$intI] -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI] ;
	    $array_Datos_Api[$intI] -> optParams            = array(
												'dimensions' => '',
	                                            //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'filters' => $filtro."ga:goalCompletionsAll>0",
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
	    $array_Datos_Api[$intI] -> Construccion();
		//print_r($array_Datos_Api[$intI] -> Google_Service_Analytics_GaData)	.'<br><br>'  ;
	    //$obj_Datos_Api -> Construccion();

	}//Llamadas a la api

	$obj_Datos_Api_totales = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_totales -> strProyectoAsociado  = $strProyectoAsociado ;
	$obj_Datos_Api_totales -> idVista              ='ga:'. $idVistaAnalytics ;
	$obj_Datos_Api_totales -> startdate            = $datFechaInicioFiltro ;
	$obj_Datos_Api_totales -> enddate              = $datFechaFinFiltro ;
	$obj_Datos_Api_totales -> metrics              = "ga:goalConversionRateAll,ga:goalCompletionsAll";
	$obj_Datos_Api_totales -> optParams            = array(
												'dimensions' => '',
	                                            //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'filters' => $filtro."ga:goalCompletionsAll>0",
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);
	$obj_Datos_Api_totales -> Construccion();

	$strTotales_datatable='';
	$sw=0;
	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS); $intI++) {
		for ($x=1; $x<=$array_Datos_Api[$intI]->NumMetricas(); $x++) {
			//$strTotales_datatable .= $array_Datos_Api[$intI]->Total($x) . ',';	
			if ($sw==0) {
				$strTotales_datatable .= $array_Datos_Api[$intI]->Total($x) . '<span class=\'porcDT\'> (';	
				$sw=1;
			}else{
				$strTotales_datatable .= number_format($array_Datos_Api[$intI]->Total($x) , 2, '.', '') . '%) </span>,';	
				$sw=0;
			}
			
		}
	}

	$strTotales_datatable = substr($strTotales_datatable, 0, -1);
	
	//Construimos el json
	$strA = '{';	
	$strA .= '"nombre_dimension": "' . $strNombre_Dimension . '",';	
	$strA .= '"total_conversionF": "'. $obj_Datos_Api_totales->totalF("goalConversionRateAll") . '",';
	$strA .= '"total_conversion": "'. $obj_Datos_Api_totales->total("goalConversionRateAll") . '",';
	$strA .= '"total_completionF": "'. $obj_Datos_Api_totales->totalF("goalCompletionsAll") . '",';
	$strA .= '"total_completion": "'. $obj_Datos_Api_totales->total("goalCompletionsAll") . '",';
	$strA .= '"totales_datatable": "'.$strTotales_datatable.'"';

	
	$strA .= '}';
	
	//Devolvemos el json
	return $strA;
} 


function Formato_Perspectiva($tipo,$strPerspectiva,$x,$obj_Datos_Api,$iso="") {
	global $trans;

	switch (strtoupper($strPerspectiva)) {
		/*ECOMMERCE*/
		case "CODPEDIDO":

			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("transactionId",$x);

				$strAux = '"'.$cab.'"';

			}else{
				$cab = $obj_Datos_Api->Valor("transactionId",$x);

				$strAux = ''.$cab.'';
			}

			break; 
		case "DIASTRANSACCION":

			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("daysToTransaction",$x);

				$strAux = '"'.$cab.'"';

			}else{
				$cab = $obj_Datos_Api->Valor("daysToTransaction",$x);

				$strAux = ''.$cab.'';
			}
			break;
		case "SESIONESTRANSACCION":

			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("sessionsToTransaction",$x);

				$strAux = '"'.$cab.'"';

			}else{
				$cab = $obj_Datos_Api->Valor("sessionsToTransaction",$x);

				$strAux = ''.$cab.'';
			}
			break;

		case "PRODUCTO":

			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("ProductName",$x);

				$strAux = '"'.$cab.'"';

			}else{
				$cab = $obj_Datos_Api->Valor("ProductName",$x);

				$strAux = ''.$cab.'';
			}

			break; 
		case "CATEGORIA1":

			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("productCategoryLevel1",$x);

				$strAux = '"'.$cab.'"';

			}else{
				$cab = $obj_Datos_Api->Valor("productCategoryLevel1",$x);

				$strAux = ''.$cab.'';
			}
			break;
		case "CATEGORIA2":

			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("productCategoryLevel2",$x);

				$strAux = '"'.$cab.'"';

			}else{
				$cab = $obj_Datos_Api->Valor("productCategoryLevel2",$x);

				$strAux = ''.$cab.'';
			}
			break;
		case "MARCA":

			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("productBrand",$x);

				$strAux = '"'.$cab.'"';

			}else{
				$cab = $obj_Datos_Api->Valor("productBrand",$x);

				$strAux = ''.$cab.'';
			}
			break;
		case "LISTAPRODUCTO":

			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("productListName",$x);

				$strAux = '"'.$cab.'"';

			}else{
				$cab = $obj_Datos_Api->Valor("productListName",$x);

				$strAux = ''.$cab.'';
			}
			break;


		/*ADQUISICIÓN*/
		case "FUENTE":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("source",$x);

				$strAux = '"'.$cab.'"';

			}
			break; 
		case "MEDIO":
			if ($tipo=='datatable') {
				
				$cab = $obj_Datos_Api->Valor("medium",$x);

				$strAux = '"'.$cab.'"';

			}
			break;
		case "CAMPANAS":
			if ($tipo=='datatable') {
				
				$cab = $obj_Datos_Api->Valor("campaign",$x);

				$strAux = '"'.$cab.'"';

			}
			break;

		/*TECNOLÓGICO*/	
		case "DISPOSITIVOS":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("deviceCategory",$x);
				switch ($cab) {
					case "desktop":
						$cabecera = 'Escritorio';
					break;
				
					case "mobile":
						$cabecera = 'Móvil';
					break;
					case "tablet":
						$cabecera = 'Tablet';
					break;
					default:
						$cabecera = $cab;
						break;
				}
				$strAux = '"'.$trans->__($cabecera, false).'"';

			}else if ($tipo=='table') {

				$cab = $obj_Datos_Api->Valor("deviceCategory",$x);
				switch ($cab) {
					case "desktop":
						$cabecera = 'Escritorio';
					break;
				
					case "mobile":
						$cabecera = 'Móvil';
					break;
					case "tablet":
						$cabecera = 'Tablet';
					break;
					default:
						$cabecera = $cab;
						break;
				}
				
				$strAux = ''.$trans->__($cabecera, false).'';

			}
			break;
		case "TIPODISP":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("deviceCategory",$x);
				switch ($cab) {
					case "desktop":
						$cabecera = 'Escritorio';
					break;
				
					case "mobile":
						$cabecera = 'Móvil';
					break;
					case "tablet":
						$cabecera = 'Tablet';
					break;
					default:
						$cabecera = $cab;
						break;
				}
				$strAux = '"'.$trans->__($cabecera, false).'"';

			}else if ($tipo=='table') {

				$cab = $obj_Datos_Api->Valor("deviceCategory",$x);
				switch ($cab) {
					case "desktop":
						$cabecera = 'Escritorio';
					break;
				
					case "mobile":
						$cabecera = 'Móvil';
					break;
					case "tablet":
						$cabecera = 'Tablet';
					break;
					default:
						$cabecera = $cab;
						break;
				}
				
				$strAux = ''.$trans->__($cabecera, false).'';

			}
		break; 
		case "NAVEGADORES":
			if ($tipo=='datatable') {
				
				$cab = $obj_Datos_Api->Valor("browser",$x);
				$strAux = '"'.$trans->__($cab, false).'"';

			}else{

				$cab = $obj_Datos_Api->Valor("browser",$x);
				$strAux = ''.$trans->__($cab, false).'';
			}
			break;

		case "INTERES":
			if ($tipo=='datatable') {
				
				$cab = $obj_Datos_Api->Valor("interestOtherCategory",$x);

				$strAux = '"'.$cab.'"';

			}
			break;

		/*USUARIOS*/
		case "SEXO":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("userGender",$x);
				switch ($cab) {
					case "female":
						$cabecera = $trans->__('Mujer', false);
					break;
				
					case "male":
						$cabecera = $trans->__('Hombre', false);
						break;
					default:
						$cabecera = $cab;
						break;
				}
				
				$strAux = '"'.$cabecera.'"';

			}else if ($tipo=='table') {

				$cab = $obj_Datos_Api->Valor("userGender",$x);
				switch ($cab) {
					case "female":
						$cabecera = $trans->__('Mujer', false);
					break;
				
					case "male":
						$cabecera = $trans->__('Hombre', false);
						break;
					default:
						$cabecera = $cab;
						break;
				}
				
				$strAux = ''.$cabecera.'';

			}
			break; 
		case "EDAD":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("userAgeBracket",$x);
				switch ($cab) {

					case "65+":
						$cabecera = vsprintf($trans->__('Mayores de %1$s años', false), array('65'));
						break;

					case "55-64":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('55', '64'));
						break;

					case "45-54":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('45', '54'));
						break;

					case "35-44": 
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('35', '44'));
						break;

					case "25-34":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('25', '34'));
						break;

					case "18-24":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('18', '24'));
						break;
					default:
						$cabecera = $cab;
						break;
					}

				$strAux = '"'.$cabecera.'"';
			}elseif ($tipo=='table') {

				$cab = $obj_Datos_Api->Valor("userAgeBracket",$x);
				switch ($cab) {

					case "65+":
						$cabecera = vsprintf($trans->__('Mayores de %1$s años', false), array('65'));
						break;

					case "55-64":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('55', '64'));
						break;

					case "45-54":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('45', '54'));
						break;

					case "35-44":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('35', '44'));
						break;

					case "25-34":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('25', '34'));
						break;

					case "18-24":
						$cabecera = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('18', '24'));
						break;
					default:
						$cabecera = $cab;
						break;
					}

				$strAux = ''.$cabecera.'';
			}
			break;

		/*GEOGRÁFICO*/
		case "PAIS":

			$nombre_fichero = '../public/images/flags/flags-iso/flat/16/'.$iso.'.png';
			$file_headers = @get_headers($nombre_fichero);
			if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
			    $imgp = '../public/images/bandera_desconocida.png';
			}
			else {
			    $imgp = $nombre_fichero;
			}

			if ($tipo=='datatable') {

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
					$strAux = '"<img src=\''.$imgp.'\' /> '.$cab.'"';
				}else{
					$cab = $obj_Datos_Api->Valor("country",$x);
					$strAux = '"<img src=\''.$imgp.'\' /> '.traducir_pais($cab).'"';
				}
				
			}else if ($tipo=='table') {

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
					$strAux = '<img src=\''.$imgp.'\' /> '.$cab;
				}else{
					$cab = $obj_Datos_Api->Valor("country",$x);
					$strAux = '<img src=\''.$imgp.'\' /> '.traducir_pais($cab);
				}
				
			}else if ($tipo=='grafico') {

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
					$strAux = $cab;
				}else{
					$cab = $obj_Datos_Api->Valor("country",$x);
					$strAux = traducir_pais($cab);
				}
				
			}
			break; 
		case "PAIS_SIN":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("country",$x);


				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = '"'.traducir_pais($cab).'"';
			}else if ($tipo=='table') {

				$cab = $obj_Datos_Api->Valor("country",$x);


				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = ''.traducir_pais($cab).'';
			}
			break; 
		case "REGION":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("region",$x);

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = '"'.$cab.'"';
			}else if ($tipo=='table') {

				$cab = $obj_Datos_Api->Valor("region",$x);

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = ''.$cab.'';
			}
			break;
		case "CIUDAD":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("city",$x);

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = '"'.$cab.'"';
			}
			break; 
		case "ENLACES":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("source",$x);

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = '"'.$cab.'"';
			}
			break; 
		case "BUSCADORES":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("keyword",$x);

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = '"'.$cab.'"';
			}
			break; 
		case "SOCIAL":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("SocialNetwork",$x);

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = '"'.$cab.'"';
			}
			break; 
		case "CAMPANAS":
			if ($tipo=='datatable') {

				$cab = $obj_Datos_Api->Valor("campaign",$x);

				if ($cab == "(not set)"){
					$cab = "(Desconocido)";
				}

				$strAux = '"'.$cab.'"';
			}
			break; 

		/*DIMENSIONES*/
		case "IDIOMAUSER":
			if ($tipo=='datatable') {
				$strAux = '"'.$obj_Datos_Api->ValorF("dimension1",$x).'"';
			}
			break; 
		case "IDIOMANAV":
			if ($tipo=='datatable') {
				$strAux= '"'.$obj_Datos_Api->ValorF("dimension8",$x).'"';
			} 			
			break;

		/*TEMPORAL*/
		case "SEMANAL":
			if ($tipo=='datatable') {
				$strAux = '"'.$obj_Datos_Api->ValorF("yearWeek",$x).'"';
			}else if($tipo=='table'){
				$strAux= ''.$obj_Datos_Api->ValorF("yearWeek",$x).'';
			}else {
				$strAux = ($x).': \''.$obj_Datos_Api->ValorF("yearWeek",$x).'\'';			
			}
			break; 
		case "MENSUAL":
			if ($tipo=='datatable') {
				$strAux= '"'.$obj_Datos_Api->ValorF("yearMonth",$x).'"';
			}else if($tipo=='table'){
				$strAux= ''.$obj_Datos_Api->ValorF("yearMonth",$x).'';
			}else {
				$strAux = ($x).': \''.$obj_Datos_Api->ValorF("yearMonth",$x).'\'';			
			}			
			break;
		case "ANUAL":
			if ($tipo=='datatable') {
				$strAux= '"'.$obj_Datos_Api->ValorF("year",$x).'"';
			}else if($tipo=='table'){
				$strAux= ''.$obj_Datos_Api->ValorF("year",$x).'';
			}else{
				$strAux = ($x).': \''.$obj_Datos_Api->ValorF("year",$x).'\'';			
			}			
			break; 
		case "HORAS":
			if ($tipo=='datatable') {
				$strAux= '"'.$obj_Datos_Api->ValorF("hour",$x).' h."';
			}else if($tipo=='table'){
				$strAux= ''.$obj_Datos_Api->ValorF("hour",$x).'';
			}else {
				$strAux = ($x).': \''.$obj_Datos_Api->ValorF("hour",$x).' h.\'';			
			}			
			break; 	
		case "DIASSEMANA":
			if ($tipo=='datatable') {
				$strAux= '"'.$obj_Datos_Api->ValorF("dayOfWeek",$x).'"';
			}else if($tipo=='table'){
				$strAux= ''.$obj_Datos_Api->ValorF("dayOfWeek",$x).'';
			}else {
				$strAux = ($x).': \''.$obj_Datos_Api->ValorF("dayOfWeek",$x).'\'';			
			}			
			break;
		case "DIARIO": 			
			if ($tipo=='datatable') {
				$strAux = '"'.dia_semana($obj_Datos_Api->Valor("year",$x).'-'.$obj_Datos_Api->Valor("Month",$x).'-'.$obj_Datos_Api->Valor("day",$x)).' '.$obj_Datos_Api->Valor("Day",$x) .'/'.$obj_Datos_Api->Valor("Month",$x).'"';
			}else if($tipo=='table'){
				$strAux = ''.dia_semana($obj_Datos_Api->Valor("year",$x).'-'.$obj_Datos_Api->Valor("Month",$x).'-'.$obj_Datos_Api->Valor("day",$x)).' '.$obj_Datos_Api->Valor("Day",$x) .'/'.$obj_Datos_Api->Valor("Month",$x).'';
			}else {
				$strAux = ($x).': \''.dia_semana($obj_Datos_Api->Valor("year",$x).'-'.$obj_Datos_Api->Valor("Month",$x).'-'.$obj_Datos_Api->Valor("day",$x)).' '.$obj_Datos_Api->Valor("Day",$x) .'/'.$obj_Datos_Api->Valor("Month",$x).'\'';
			}				
			break;  
		default:
			if ($tipo=='datatable') {
				$strAux = '"'.$obj_Datos_Api->Valor($strPerspectiva,$x).'"';
			}else{
				$strAux = ''.$obj_Datos_Api->Valor($strPerspectiva,$x).'';
			}
			break;
	}//Cierre switch 
	return $strAux;
}

function Impactos1_datatable ( $strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro)
{
	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($strPerspectiva)) {

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions='ga:source';
			$strSort='source';
			break; 
		case "MEDIO":
			$strDimensions='ga:medium';
			$strSort='medium';
			break;
		case "CAMPANAS":
			$strDimensions='ga:campaign';
			$strSort='campaign';
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions='ga:deviceCategory';
			$strSort='deviceCategory';
			break; 
		case "NAVEGADORES":
			$strDimensions='ga:browser';
			$strSort='browser';
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions='ga:userGender';
			$strSort='userGender';
			break; 
		case "EDAD":
			$strDimensions='ga:userAgeBracket';
			$strSort='userAgeBracket';
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions='ga:country,ga:countryIsoCode';
			$strSort='country';
			break; 
		case "REGION":
			$strDimensions='ga:region';
			$strSort='region';
			break;
		case "CIUDAD":
			$strDimensions='ga:city';
			$strSort='city';
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			break;  
	}//Cierre switch 	

	$obj_Datos_Api_admin = new Datos_Api_Admin(); // Instanciamos la clase Datos_Api_Admin
    $obj_Datos_Api_admin -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_admin -> idVista              = $idVistaAnalytics ;
    $obj_Datos_Api_admin -> TipoLlamada          = 'GOALS_VISTA' ;   
    $obj_Datos_Api_admin -> Construccion();

    $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_METRICAS();
	$ARRAY_GOALS_VISTA_ORDENADOS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS();

	//Hacemos tantas llamadas al api de datos como elementos tenga el array de métricas.  En cada llamada sacaremos cinco objetivos (10 méticas)
	$array_Datos_Api = array();

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa.';';
	}
	if($idIdioma!="todos"){
		//if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma.';';
	}
	if ($strFiltro!="no"){
		//if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro.';';
	}

	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS); $intI++) {

	    //Creación objeto, parametrización -------------------------------------------------------
	    $array_Datos_Api[$intI] = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $array_Datos_Api[$intI] -> strProyectoAsociado  = $strProyectoAsociado ;
	    $array_Datos_Api[$intI] -> idVista              ='ga:'. $idVistaAnalytics ;
	    $array_Datos_Api[$intI] -> startdate            = $datFechaInicioFiltro ;
	    $array_Datos_Api[$intI] -> enddate              = $datFechaFinFiltro ;
	    $array_Datos_Api[$intI] -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI] ;
	    $array_Datos_Api[$intI] -> optParams            = array(
												'dimensions' => $strDimensions,
	                                            //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'filters' => $filtro."ga:goalCompletionsAll>0",
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
	    $array_Datos_Api[$intI] -> Construccion();

	}//Llamadas a la api

	$obj_Datos_Api_totales = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_totales -> strProyectoAsociado  = $strProyectoAsociado ;
	$obj_Datos_Api_totales -> idVista              ='ga:'. $idVistaAnalytics ;
	$obj_Datos_Api_totales -> startdate            = $datFechaInicioFiltro ;
	$obj_Datos_Api_totales -> enddate              = $datFechaFinFiltro ;
	$obj_Datos_Api_totales -> metrics              = "ga:goalConversionRateAll,ga:goalCompletionsAll";
	$obj_Datos_Api_totales -> optParams            = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro."ga:goalCompletionsAll>0",
	                                            //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);
	$obj_Datos_Api_totales -> Construccion();

	//print_r(var_dump($ARRAY_GOALS_VISTA_ORDENADOS)."  -  ".var_dump($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS));
	//Realizamos una cuenta de filas de 5 por recorrer mas los sobrante
	$tot_ordenados = count($ARRAY_GOALS_VISTA_ORDENADOS);
	$tot_ordenados_m = count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS);

	

	if($tot_ordenados_m > 2){
		$vt=0;
		while ($tot_ordenados > 5) {
			$tot_ordenados = $tot_ordenados-5;
			$vt++;
		}

	}else if($tot_ordenados_m == 2){
		$vt=2;
		$tot_ordenados = $tot_ordenados;

	}else if($tot_ordenados_m < 2){
		$vt=1;
	}

	//print_r(count($ARRAY_GOALS_VISTA_ORDENADOS)."  -  ".count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS));

	//echo $tot_ordenados. " - ";

	//filas
	for ($x=1; $x<=$array_Datos_Api[0]->NumValores(); $x++) {
		

		$strB .= '[';
		//El primer valor va a depender de la perspectiva que estamos pintando
		if($strPerspectiva == "Pais"){
			$strB .= Formato_Perspectiva('datatable',$strPerspectiva,$x,$array_Datos_Api[0],$array_Datos_Api[0]->Valorf('countryIsoCode',$x)).',';
		}else{
			$strB .= Formato_Perspectiva('datatable',$strPerspectiva,$x,$array_Datos_Api[0]).',';
		}
		//$strB .= '"'.$array_Datos_Api[0]->Valor($strSort,$x).'",';
		$totalrate=0;
		$vuelta=0;
		//colummnas

		//print_r( var_dump($array_Datos_Api[0]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[10]['id'].'Completions',$x)));
		
		//print_r($vt);
		if($vt==1){

			//si es uno
			for ($j=0; $j < 1; $j++) { 
				for ($i=10; $i < 10+$tot_ordenados; $i++) { 
					if($array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x)=="0"){
						$text_format = "-";
					}else{
						$text_format = $array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);
					}
					if($array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'ConversionRate',$x)=="0,00%"){
						$text_formatb = "";
					}else{
						$text_formatb = '<span class=\'porcDT\'> ('.$array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'ConversionRate',$x).') </span>';
					}
					$strB .= '"'.$text_format.$text_formatb.'",';
				}
			}
			

		}else{
			

		
			//columna 
			for ($j=0; $j < $tot_ordenados_m; $j++) { 
				
				if($j < 2){
					//echo $tot_ordenados."  -  ";
					//mitad una
					if($vuelta==0){
						for ($i=0; $i < 5; $i++) { 
							if($array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x)=="0"){
								$text_format = "-";
							}else{
								$text_format = $array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);
							}
							if($array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'ConversionRate',$x)=="0,00%"){
								$text_formatb = "";
							}else{
								$text_formatb = '<span class=\'porcDT\'> ('.$array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'ConversionRate',$x).') </span>';
							}

							$strB .= '"'.$text_format.$text_formatb.'",';
						}
						$vuelta++;
					//mitad 2
					}else{
						$desde = $tot_ordenados/2;
						for ($i=5; $i < 10; $i++) { 
							if($array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x)=="0"){
								$text_format = "-";
							}else{
								$text_format = $array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);
							}
							if($array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'ConversionRate',$x)=="0,00%"){
								$text_formatb = "";
							}else{
								$text_formatb = '<span class=\'porcDT\'> ('.$array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'ConversionRate',$x).') </span>';
							}

							$strB .= '"'.$text_format.$text_formatb.'",';
						}
						$vuelta==0;
					}

				}else{
					

					if($tot_ordenados_m > 2){
						//otros

						for ($i=10; $i < 10+$tot_ordenados; $i++) { 
							if($array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x)=="0"){
								$text_format = "";
							}else{
								$text_format = $array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'Completions',$x);
							}
							if($array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'ConversionRate',$x)=="0,00%"){
								$text_formatb = "";
							}else{
								$text_formatb = '<span class=\'porcDT\'> ('.$array_Datos_Api[$j]->ValorF('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$i]['id'].'ConversionRate',$x).') </span>';
							}

							$strB .= '"'.$text_format.$text_formatb.'",';
						}
					}

				}
			
			
			}//for


		}

	    //TOTALES
	    $strB .= '"'.$obj_Datos_Api_totales->ValorF("goalCompletionsAll",$x).'<span class=\'porcDT\'> ('.$obj_Datos_Api_totales->ValorF("goalConversionRateAll",$x).') </span>",';	    


		$strB = substr($strB, 0, -1);
		$strB .= '],';	 


	}

	$numValoresTotal += $array_Datos_Api[0]->NumValoresReal();

	
	$strB = substr($strB, 0, -1);
	//CREACION dE JSON PARA DATATABLE
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $numValoresTotal .',';
	$strA .= '"recordsFiltered":' . $numValoresTotal .','	;
	$strA .= '"data":[';
	$strA .= $strB;
	$strA .= ']}';
	
	//Devolvemos el json
	return $strA;
}

function Resumen_emailsemanal_impactos ( $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $datFechaInicioFiltroAno, $datFechaFinFiltroAno, $intMesActual )
{
	//HALLAMOS EL ARRAY CON LAS MÉTRICAS DE OBJETIVOS DE ESTA VISTA
	$obj_Datos_Api_admin = new Datos_Api_Admin(); // Instanciamos la clase Datos_Api_Admin
    $obj_Datos_Api_admin -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_admin -> idVista              = $idVistaAnalytics ;
    $obj_Datos_Api_admin -> TipoLlamada          = 'GOALS_VISTA' ;   
    $obj_Datos_Api_admin -> Construccion();
    $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_METRICAS();
	$ARRAY_GOALS_VISTA_ORDENADOS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS();

print("array ordenado:<pre>".print_r($ARRAY_GOALS_VISTA_ORDENADOS,true)."</pre><br><br>");
print("array metricas:<pre>".print_r($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS,true)."</pre><br><br>");
print (Count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS).' NUM LLAMADAS <br><br>');	


	//Hacemos tantas llamadas al api de datos como elementos tenga el array de métricas.  En cada llamada sacaremos cinco objetivos (10 méticas)
	for ($intI = 0; $intI < count($ARRAY_GOALS_VISTA_ORDENADOS_METRICAS); $intI++) {
		
		//SEMANA ACTUAL. Llamamos al api de datos de google, parametrización --------------------------------------
		$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
		$obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics ;
		$obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
		$obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
		$obj_Datos_Api -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI] ;
		$obj_Datos_Api -> optParams            = array(
												'start-index' => 1,
												'max-results' => 100);  
		$obj_Datos_Api -> Construccion();
		//Llamada al api realizada ---------------------------------------------------------------------------------
 
 		//MES ACTUAL / AÑO ACTUAL. Llamamos al api de datos de google, parametrización -----------------------------
		$obj_Datos_Api_Mes = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		$obj_Datos_Api_Mes -> strProyectoAsociado  = $strProyectoAsociado ;
		$obj_Datos_Api_Mes -> idVista              ='ga:'. $idVistaAnalytics ;
		$obj_Datos_Api_Mes -> startdate            = $datFechaInicioFiltroAno ;
		$obj_Datos_Api_Mes -> enddate              = $datFechaFinFiltroAno ;
		$obj_Datos_Api_Mes -> metrics              = $ARRAY_GOALS_VISTA_ORDENADOS_METRICAS[$intI] ;
		$obj_Datos_Api_Mes -> optParams            = array(
													'dimensions'  => 'ga:month',
													'sort' 		  => 'ga:month',
													'start-index' => 1,
													'max-results' => 100);  
		$obj_Datos_Api_Mes 	-> Construccion();
//print_r($obj_Datos_Api_Mes->Google_Service_Analytics_GaData);		
		//Llamada al api realizada ---------------------------------------------------------------------------------
 
		//Pintamos las filas de los objetivos recogidos ------------------------------------------------------------
		for ($intR = 0; $intR <= 4; $intR++) {
			$intIndice = ($intI * 5 ) + $intR;
			//Comprobamos que pintamos sólo las filas que corresponden al número de objetivos de esta vista
			if ($intIndice < count($ARRAY_GOALS_VISTA_ORDENADOS)) {
			
				//Ponemos cabeceras agrupando los objetivos
				if ($intIndice == 0) {$strRegistros .= '<tr><td colspan=3>OBTENCIÓN DE VISITAS DE CALIDAD</td></tr>';}
				if ($intIndice == 4) {$strRegistros .= '<tr><td colspan=3>OBTENCIÓN DE CONTACTOS</td></tr>';}
				if ($intIndice == 7) {$strRegistros .= '<tr><td colspan=3>DIFUSIÓN DEL CONTENIDO WEB</td></tr>';}
				if ($intIndice == 10) {$strRegistros .= '<tr><td colspan=3>OTROS</td></tr>';}
				$strRegistros .= '<tr>';
				$strRegistros .= '<td>'.$ARRAY_GOALS_VISTA_ORDENADOS[$intIndice]["nombre"].'</td>';
				//AÑO
				$strRegistros .= '<td>'.$obj_Datos_Api_Mes->Total('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$intIndice]["id"].'Completions').'</td>';
				$strRegistros .= '<td>'.$obj_Datos_Api_Mes->Total('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$intIndice]["id"].'ConversionRate').'</td>';
				//MES
				//Movemos los datos hasta encontrar el mes $intMesActual
				for ($x=1; $x<=$obj_Datos_Api_Mes->NumValores(); $x++) {			
					if ($x == $intMesActual) {
						$strRegistros .= '<td>'.$obj_Datos_Api_Mes->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$intIndice]["id"].'Completions',$x).'</td>';
						$strRegistros .= '<td>'.$obj_Datos_Api_Mes->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$intIndice]["id"].'ConversionRate',$x).'</td>';
					}
				}				
				//SEMANA ACTUAL
				$strRegistros .= '<td>'.$obj_Datos_Api->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$intIndice]["id"].'Completions',1).'</td>';
				$strRegistros .= '<td>'.$obj_Datos_Api->Valor('goal'.$ARRAY_GOALS_VISTA_ORDENADOS[$intIndice]["id"].'ConversionRate',1).'</td>';
				$strRegistros .= '</tr>';
				
			}
		}
		//Pintamos las filas de los objetivos recogidos ------------------------------------------------------------
		

	}
	
	//Construimos una tabla con los resultados
	$strA = '<table border=1>';	
	$strA .= '<tr>';	
	$strA .= '<td>&nbsp;</td>';	
	$strA .= '<td colspan=2>2014</td>';
	$strA .= '<td colspan=2>Diciembre</td>';
	$strA .= '<td colspan=2>Semana</td>';
	$strA .= '</tr>';	
	$strA .= '<tr>';	
	$strA .= '<td>Impacto</td>';	
	$strA .= '<td>#</td><td>%</td>';
	$strA .= '<td>#</td><td>%</td>';
	$strA .= '<td>#</td><td>%</td>';
	$strA .= '</tr>';
	$strA .= $strRegistros;
	$strA .= '</table>';
 
	//Pintamos el resultado
	return $strA;

}


function Impactos1_resultados($strProyectoAsociado,$idVistaAnalytics){
	$obj_Datos_Api_admin = new Datos_Api_Admin(); // Instanciamos la clase Datos_Api_Admin
    $obj_Datos_Api_admin -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_admin -> idVista              = $idVistaAnalytics ;
    $obj_Datos_Api_admin -> TipoLlamada          = 'GOALS_VISTA' ;   
    $obj_Datos_Api_admin -> Construccion();

    $devuelve = array();
	$ARRAY_GOALS_VISTA_ORDENADOS_METRICAS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS_METRICAS();
	$ARRAY_GOALS_VISTA_ORDENADOS = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS();
	//$ARRAY_GOALS_VISTA = $obj_Datos_Api_admin -> GOALS_VISTA_ORDENADOS();
	//print("array metricas:<pre>".print_r($ARRAY_GOALS_VISTA,true)."</pre><br><br>");
	//print (Count($ARRAY_GOALS_VISTA_ORDENADOS).' NUM LLAMADAS <br><br>');
	$devuelve["cabeceras"] = $ARRAY_GOALS_VISTA_ORDENADOS;
	return $devuelve;
}

//renombrar metricas habituales
function metricas_habituales_datatable ( $strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir,$paisGa, $idIdioma, $strFiltro, $strApp)
{
	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($strPerspectiva)) {
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			break;  
	}//Cierre switch 	

	//Preparación de la ordenación
	$array_Ordenacion = array($strSort,'Sessions','PageViews','pageviewsPerSession','Users','newUsers','','bounceRate','avgSessionDuration');
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	if($strApp == 0){
		$metricas = 'ga:sessions,ga:pageviews,ga:pageviewsPerSession,ga:users,ga:newUsers,ga:bounceRate,ga:avgSessionDuration';
		$pageviews = 'pageviews';
		$pageviewsPerSession = 'pageviewsPerSession';
	}else{
		$metricas = 'ga:sessions,ga:screenviews,ga:screenviewsPerSession,ga:users,ga:newUsers,ga:bounceRate,ga:avgSessionDuration';
		$pageviews = 'screenviews';
		$pageviewsPerSession = 'screenviewsPerSession';
	}
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------
 
	// Construimos un json con los datos
	// -------------------------------------
	// Día
	// Sesiones
	// Páginas vistas
	// Páginas / sesión
	// Usuarios
	// Usuarios nuevos
	// Usuarios recurrentes
	// Tasa de rebote
	// Duración media de la sesión
	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .','	;
	$strA .= '"data":[';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		$strA .= '[';
		//El primer valor va a depender de la perspectiva que estamos pintando
		$strA .= Formato_Perspectiva('datatable',$strPerspectiva,$x,$obj_Datos_Api).',';
		$strA .= '"'.$obj_Datos_Api->ValorF("Sessions",$x).'",';
		$strA .= '"'.$obj_Datos_Api->ValorF($pageviews ,$x).'",';
		$strA .= '"'.$obj_Datos_Api->ValorF($pageviewsPerSession ,$x).'",';
		$strA .= '"'.$obj_Datos_Api->ValorF("Users",$x).'",';
		$strA .= '"'.$obj_Datos_Api->ValorF("newUsers",$x).'",';
		$strA .= '"'. number_format($obj_Datos_Api->Valor("Users",$x) - $obj_Datos_Api->Valor("newUsers",$x),0,',','.') .'",';
		$strA .= '"'.$obj_Datos_Api->ValorF("bounceRate",$x).'",';
		$strA .= '"'.$obj_Datos_Api->ValorF("avgSessionDuration",$x).'"';
		$strA .= ']';	  
		//Añadimos una "," si no es el último registro a pintar
		if ($x < $obj_Datos_Api->NumValores()) {
			$strA .= ','; 
		}
	}
	$strA .= ']}';
	
	//Devolvemos el json
	return $strA;
}


//USAR la misma para sexo y edades
function caracteristicas_datatable ($strTipo, $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro, $strApp )
{
	$filtro_and="";
	//$strSort='yearWeek';
	switch ($strTipo) {
		case 'sexo':
			$strDimensions  ='ga:userGender';
			$strDimensionsN ='userGender';
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('sessions','bounceRate','avgSessionDuration','pageviewsPerSession');	
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}	
			break;
		case 'edad':
			$strDimensions='ga:userAgeBracket';
			$strDimensionsN ='userAgeBracket'; 
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('sessions','bounceRate','avgSessionDuration','pageviewsPerSession');	
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}	
			break;
		case 'interes':
			$strDimensions='ga:interestInMarketCategory';
			$strDimensionsN ='interestInMarketCategory'; 
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('interestInMarketCategory','sessions','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('interestInMarketCategory','sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
		case 'dispositivos':
			$strDimensions='ga:deviceCategory';
			$strDimensionsN ='deviceCategory'; 
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('deviceCategory','sessions','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('deviceCategory','sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
		case 'navegadores':
			$strDimensions='ga:browser';
			$strDimensionsN ='browser'; 
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('browser','sessions','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('browser','sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
		case 'pais':
			$strDimensions='ga:country,ga:countryIsoCode';
			$strDimensionsN ='country'; 
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('country','sessions','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('country','sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
		case 'region':
			$strDimensions='ga:region';
			$strDimensionsN ='region'; 
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('region','sessions','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('region','sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
		case 'ciudad':
			$strDimensions='ga:city';
			$strDimensionsN ='city'; 
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('city','sessions','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('city','sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
		case 'idiomauser':
			$strDimensions='ga:dimension8';
			$strDimensionsN ='dimension8'; 
			if($strApp == 0){
				$metricas  = 'ga:pageviews,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('dimension8','pageviews','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:screenviews ,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('dimension8','screenviews','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
		case 'idiomanav':
			$strDimensions='ga:dimension1';
			$strDimensionsN ='dimension1'; 
			if($strApp == 0){
				$metricas  = 'ga:pageviews,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('dimension1','pageviews','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:screenviews ,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('dimension1','screenviews','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
		case 'enlaces':
			$strDimensions='ga:source';
			$strDimensionsN ='source'; 
			$metricas  = 'ga:sessions';
			$array_Ordenacion = array('source','sessions');
			$filtro_and = "ga:medium==referral;ga:SocialNetwork==(not set)";
			break;
		case 'buscadores':
			$strDimensions='ga:keyword';
			$strDimensionsN ='keyword'; 
			$metricas  = 'ga:sessions';
			$array_Ordenacion = array('keyword','sessions');
			$filtro_and = "ga:medium==organic;ga:SocialNetwork==(not set)";
			break;
		case 'social':
			$strDimensions='ga:SocialNetwork';
			$strDimensionsN ='SocialNetwork'; 
			$metricas  = 'ga:sessions';
			$array_Ordenacion = array('SocialNetwork','sessions');
			$filtro_and = "ga:SocialNetwork!=(not set)";
			break;
		case 'campanas':
			$strDimensions='ga:campaign,ga:medium,ga:source';
			$strDimensionsN ='campaign'; 
			$metricas  = 'ga:sessions';
			$array_Ordenacion = array('campaign','medium','source','sessions');
			$filtro_and = "ga:medium!=referral;ga:medium!=organic;ga:medium!=(none);ga:SocialNetwork==(not set)";
			break;
		case 'terminos':
			$strDimensions='ga:keyword';
			$strDimensionsN ='keyword'; 
			if($strApp == 0){
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession';
				$array_Ordenacion = array('keyword','sessions','bounceRate','avgSessionDuration','pageviewsPerSession');
			}else{
				$metricas  = 'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:screenviewsPerSession';
				$array_Ordenacion = array('keyword','sessions','bounceRate','avgSessionDuration','screenviewsPerSession');	
			}
			break;
	};

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= $strFiltro;
	}
	
	if(!empty($filtro_and)){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= $filtro_and;
	}
	
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='asc') {$DirOrdenacion='-';}
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
    	$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
	                                            'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}else{
   		$obj_Datos_Api -> optParams        = array(
												'dimensions' => $strDimensions,
												'filters' => $filtro,
	                                            'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
												'start-index' => $DataTables_start + 1,
	                                            'max-results' => $DataTables_length);  
   	}
    $obj_Datos_Api -> Construccion();
	
	//Si hay que comparar creamos el objeto con la fecha anterior
    if ($blnComparacion=="true") {
    	//Creación objeto, parametrización -------------------------------------------------------
	    $obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado;
	    $obj_Datos_Api_ant -> idVista              ='ga:'. $idVistaAnalytics;
	    $obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt;
	    $obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt;
	    $obj_Datos_Api_ant -> metrics              = $metricas;
	    if(empty($filtro)){
	    	$obj_Datos_Api_ant -> optParams        = array(
													'dimensions' => $strDimensions,
		                                            'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
													'start-index' => 1,
		                                            'max-results' => 10000);  
	   	}else{
	   		$obj_Datos_Api_ant -> optParams        = array(
													'dimensions' => $strDimensions,
													'filters' => $filtro,
		                                            'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
													'start-index' => 1,
		                                            'max-results' => 10000);  
	   	}
	   	$obj_Datos_Api_ant -> Construccion();
    }
    if($strApp == 0){
    	$pagsesion = 'pageviewsPerSession';
    	$pagvistas = 'pageviews';
    }else{
    	$pagsesion = 'screenviewsPerSession';
    	$pagvistas = 'screenviews ';
    }

    //Objeto creado ---------------------------------------------------------------------------	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"data":[';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		$strA .= '[';
		//El primer valor va a depender de la perspectiva que estamos pintando
		if($strTipo=="pais"){
			$strA .= Formato_Perspectiva('datatable',$strTipo,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x));
		}else if($strTipo=="idiomauser" || $strTipo=="idiomanav"){
			$strA .= '"'.traducir_idioma($obj_Datos_Api->Valor($strDimensionsN,$x)).'"';
		}else if($strTipo=="interes" || $strTipo=="region" || $strTipo=="ciudad"/**/){
			
			$strA .= '"'.traduce_de_ingles($obj_Datos_Api->Valor($strDimensionsN,$x),$_COOKIE["idioma_usuario"]).'"';
		}else if($strTipo=="terminos"){
			$strA .= '"'.$obj_Datos_Api->Valor($strDimensionsN,$x).'"';
		}else{
			$strA .= Formato_Perspectiva('datatable',$strTipo,$x,$obj_Datos_Api);
		}

		//PARA CAMAPAÑAS METEMSO ANTES LA FUENTE Y EL MEDIO
		if($strTipo == "campanas"){
			$strA .= ',"<span class=\'izquierda\'>'.$obj_Datos_Api->ValorF("medium",$x).'</span>",';
			$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->ValorF("source",$x).'</span>"';
		}
			
		//Comparamos si tiene comparación
		if($strTipo == "dispositivos" || $strTipo == "navegadores"  || $strTipo == "enlaces" || $strTipo == "buscadores" || $strTipo == "social" || $strTipo == "campanas" || $strTipo == "terminos"){

			if ($blnComparacion=="true") {
				$strA .= ',"'.DevolverContenido_Total($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api_ant->Valor("sessions",$x),$obj_Datos_Api->ValorF("sessions",$x),$obj_Datos_Api_ant->ValorF("sessions",$x)).' '.$obj_Datos_Api->ValorF("sessions",$x).' <span class=\'porcDT\'>('.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'%) </span>",';
			}else{
				$strA .= ',"'.$obj_Datos_Api->ValorF("sessions",$x).' <span class=\'porcDT\'> ('.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'%) </span>",';
			}
		}else if($strTipo == "pais" || $strTipo == "region" || $strTipo == "ciudad"){

			if ($blnComparacion=="true") {
				$swp=0;
				for ($i=1; $i<=$obj_Datos_Api_ant->NumValores(); $i++) {

					if( trim( $obj_Datos_Api->Valor($strDimensionsN,$x) ) == trim( $obj_Datos_Api_ant->Valor($strDimensionsN,$i) ) ){

						$strA .= ',"'.DevolverContenido_Total($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api_ant->Valor("sessions",$i),$obj_Datos_Api->ValorF("sessions",$x),$obj_Datos_Api_ant->ValorF("sessions",$i)).' '.$obj_Datos_Api->ValorF("sessions",$x).' <span class=\'porcDT\'>('.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'%) </span>",';
						$swp=1;

					}
				}
				if($swp!=1){
					$strA .= ',"'.$obj_Datos_Api->ValorF("sessions",$x).'|'.$obj_Datos_Api->Valor($strDimensionsN,$x).'| <span class=\'porcDT\'> ('.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'%) </span>",';
				}
			}else{
				$strA .= ',"'.$obj_Datos_Api->ValorF("sessions",$x).' <span class=\'porcDT\'> ('.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'%) </span>",';
			}

		}else if($strTipo=="idiomauser" || $strTipo=="idiomanav"){

			if ($blnComparacion=="true") {
				$strA .= ',"'.DevolverContenido_Total($obj_Datos_Api->Valor($pagvistas,$x),$obj_Datos_Api_ant->Valor($pagvistas,$x),$obj_Datos_Api->ValorF($pagvistas,$x),$obj_Datos_Api_ant->ValorF($pagvistas,$x)).' '.$obj_Datos_Api->ValorF($pagvistas,$x).' <span class=\'porcDT\'> ('.porcentaje_sesiones($obj_Datos_Api->Valor($pagvistas,$x),$obj_Datos_Api->Total($pagvistas)).'%) </span>",';
			}else{
				$strA .= ',"'.$obj_Datos_Api->ValorF($pagvistas,$x).' <span class=\'porcDT\'>('.porcentaje_sesiones($obj_Datos_Api->Valor($pagvistas,$x),$obj_Datos_Api->Total($pagvistas)).'%) </span>",';
			}

		}else if($strTipo=="sexo" || $strTipo=="edad" || $strTipo=="interes"){

			if ($blnComparacion=="true") {
				$porcentage_sei = porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions"));
				$porcentage_sei_ant = porcentaje_sesiones($obj_Datos_Api_ant->Valor("sessions",$x),$obj_Datos_Api_ant->Total("sessions"));
				$strA .= ',"'.DevolverContenido_Total($porcentage_sei,$porcentage_sei_ant,$porcentage_sei,$porcentage_sei_ant,'%').'  '.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'%  ",';
			}else{
				$strA .= ',"  '.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'%  ",';
			}

		}else{

			if ($blnComparacion=="true") {
				$strA .= ',"'.DevolverContenido_Total($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api_ant->Valor("sessions",$x),$obj_Datos_Api->ValorF("sessions",$x),$obj_Datos_Api_ant->ValorF("sessions",$x)).' <span class=\'porcDT\'> '.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'% </span> ",';
			}else{
				$strA .= '," <span class=\'porcDT\'> '.porcentaje_sesiones($obj_Datos_Api->Valor("sessions",$x),$obj_Datos_Api->Total("sessions")).'% </span> ",';
			}

		}
		//Solo mostramos sesiones
		if ($strTipo != "enlaces" || $strTipo != "buscadores" || $strTipo != "social" || $strTipo != "campanas") {
			$strA .= '"'.$obj_Datos_Api->ValorF("bounceRate",$x).'",';
			$strA .= '"'.$obj_Datos_Api->ValorF("avgSessionDuration",$x).'",';
			$strA .= '"'.$obj_Datos_Api->ValorF($pagsesion,$x).'"';
		}
		if($strTipo!="interes"){
			$strA .= ',""';
		}
		$strA .= ']';	  
		//Añadimos una "," si no es el último registro a pintar
		if ($x < $obj_Datos_Api->NumValores()) {
			$strA .= ','; 
		}
	}
	$strA .= ']}';
	
	//Devolvemos el json
	return $strA;
}  

function DevolverContenido_Total($Valor,$ValorAnt,$ValorF,$ValorAntF,$post=""){

	if($ValorAnt != ''){
		if ($Valor > $ValorAnt) {
			$strAux = '<img class=\'imgdrip\' src=\'../public/images/flecha_arriba.gif\' Title=\'Actual: ' . $ValorF .$post.' | Anterior: ' . $ValorAntF .$post. '\'/>';
		} else {
			if ($Valor == $ValorAnt) {
				$strAux = '<img class=\'imgdrip\' src=\'../public/images/flecha_igual.gif\' Title=\'Actual: ' . $ValorF .$post.' | Anterior: ' . $ValorAntF .$post. '\'/>';
			} else {
				$strAux = '<img class=\'imgdrip\' src=\'../public/images/flecha_abajo.gif\' Title=\'Actual: ' . $ValorF .$post.' | Anterior: ' . $ValorAntF .$post. '\'/>';
			}
		}
	}
	return $strAux;

}


function traducir_idioma($idioma){
	global $trans;
	$c = Nuevo_PDO();

	$sql = "SELECT LOCI_IDIOMA FROM dbo.LOC_IDIOMASNAVEGADOR WHERE LOCI_CODIGO LIKE :idioma";

	if($trans->getLanguage() == 'en'){
		$sql = "SELECT LOCI_IDIOMA_EN FROM dbo.LOC_IDIOMASNAVEGADOR WHERE LOCI_CODIGO LIKE :idioma";
	}

	if($stmt = $c->prepare($sql)){
	    $result = $stmt->execute(array(':idioma' => $idioma));
	    if ($result && $stmt->rowCount() != 0){
	      while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
	      	$idioma = $fila[0];
	      }
	    }else{
	    	$idioma = $idioma;

	    }
	}

	return $trans->__(ucwords($idioma), false);

}

//Función para formatear los nombres
function caracteristicas_datatable_formatear_nombre($nombre){

	switch (strtolower($nombre)) {
		case "female":
			$strAux = $trans->__('Mujer', false);
			break;
		
		case "male":
			$strAux = $trans->__('Hombre', false);
			break;

		case "65+":
			$strAux = vsprintf($trans->__('Mayores de %1$s años', false), array('65'));
			break;

		case "55-64":
			$strAux = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('55', '64'));
			break;

		case "45-54":
			$strAux = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('45', '54'));
			break;

		case "35-44":
			$strAux = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('35', '44'));
			break;

		case "25-34":
			$strAux = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('25', '34'));
			break;

		case "18-24":
			$strAux = vsprintf($trans->__('Entre %1$s y %2$s años', false), array('18', '24'));
			break;
			
		default:
			$strAux = $nombre;				
			break; 
	}

	return $strAux;

}

//Función para haya el porcentaje de acierto de genero
function porcentaje_sesiones($sessions,$sesiones_totales){

	if($sesiones_totales != 0){
		return  number_format((($sessions*100)/$sesiones_totales), 2, ',', '');
	}else{
		return false;
	}
	

}
function porcentaje_sesiones_res($sessions,$sesiones_totales){

	if($sesiones_totales != 0){
		return  ($sessions*100)/$sesiones_totales;
	}else{
		return false;
	}

}
  
function metricas_habituales_graficos ( $strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro, $strApp )
{
	global $trans;

	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($strPerspectiva)) {
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			$strNombre_Dimension = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			$strNombre_Dimension = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			$strNombre_Dimension =  $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			$strNombre_Dimension = $trans->__('Hora del día', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			$strNombre_Dimension = $trans->__('Día semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			$strNombre_Dimension = $trans->__('Día', false);
			break;  
	}//Cierre switch 	

	
	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
	if($strApp == 0){
		$metricas = 'ga:sessions,ga:pageviews,ga:pageviewsPerSession,ga:users,ga:newUsers,ga:bounceRate,ga:avgSessionDuration';
		$pageviews = 'pageviews';
		$pageviewsPerSession = 'pageviewsPerSession';
	}else{
		$metricas = 'ga:sessions,ga:screenviews,ga:screenviewsPerSession,ga:users,ga:newUsers,ga:bounceRate,ga:avgSessionDuration';
		$pageviews = 'screenviews';
		$pageviewsPerSession = 'screenviewsPerSession';
	}

	//Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = $metricas ;
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams            = array(
	                                            'dimensions' => $strDimensions,
	                                            'sort' => 'ga:'.$strSort,
												'start-index' => 1,
	                                            'max-results' => 10000); 
    }else{
    	$obj_Datos_Api -> optParams            = array(
	                                            'dimensions' => $strDimensions,
	                                            'filters' => $filtro,
	                                            'sort' => 'ga:'.$strSort,
												'start-index' => 1,
	                                            'max-results' => 10000); 
    } 
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------
 
	// Construimos un json con los datos
	// -------------------------------------
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		$rickshaw_dimensiones .= Formato_Perspectiva('grafico',$strPerspectiva,$x,$obj_Datos_Api);
		$rickshaw_sesiones .= '{x:'.$x.',y:'.$obj_Datos_Api->Valor("sessions",$x).'}';
		$rickshaw_sesiones_legend .= '\''.$obj_Datos_Api->ValorF("sessions",$x).'\'';
		$rickshaw_paginasvistas .= '{x:'.$x.',y:'.$obj_Datos_Api->Valor($pageviews,$x).'}';
		$rickshaw_paginasvistas_legend .= '\''.$obj_Datos_Api->ValorF($pageviews,$x).'\'';		
		$rickshaw_paginasvistassesion .= '{x:'.$x.',y:'.$obj_Datos_Api->Valor($pageviewsPerSession,$x).'}';
		$rickshaw_paginasvistassesion_legend .= '\''.$obj_Datos_Api->ValorF($pageviewsPerSession,$x).'\'';
		$rickshaw_usuarios .= '{x:'.$x.',y:'.$obj_Datos_Api->Valor("users",$x).'}';
		$rickshaw_usuarios_legend .= '\''.$obj_Datos_Api->ValorF("users",$x).'\'';

		$rickshaw_tasarebote .= '{x:'.$x.',y:'.$obj_Datos_Api->Valor("bounceRate",$x).'}';
		$rickshaw_tasarebote_legend .= '\''.$obj_Datos_Api->ValorF("bounceRate",$x).'\'';
		
		$rickshaw_duracionmediasesion .= '{x:'.$x.',y:'.$obj_Datos_Api->Valor("avgSessionDuration",$x).'}';
		$rickshaw_duracionmediasesion_legend .= '\''.($obj_Datos_Api->Valor("avgSessionDuration",$x)/60).'\'';


		if($obj_Datos_Api->Valor("Users",$x) != 0){		
			$rickshaw_usuarios_nuevos .= '{x:'.$x.',y:'.$obj_Datos_Api->Valor("NewUsers",$x)*100/$obj_Datos_Api->Valor("Users",$x).'}';
			$rickshaw_usuarios_recurrentes .= '{x:'.$x.',y:'.(100-($obj_Datos_Api->Valor("NewUsers",$x)*100/$obj_Datos_Api->Valor("Users",$x))).'}';
			$rickshaw_usuarios_nuevos_legend .= $obj_Datos_Api->Valor("NewUsers",$x)*100/$obj_Datos_Api->Valor("Users",$x);
			$rickshaw_usuarios_recurrentes_legend .= (100-($obj_Datos_Api->Valor("NewUsers",$x)*100/$obj_Datos_Api->Valor("Users",$x)));
		}else{
			$rickshaw_usuarios_nuevos .= '{x:'.$x.',y:0}';
			$rickshaw_usuarios_recurrentes .= '{x:'.$x.',y:0}';
			$rickshaw_usuarios_nuevos_legend .= 0;
			$rickshaw_usuarios_recurrentes_legend .= 0;
		}
		//Añadimos una "," si no es el último registro a pintar
		if ($x < $obj_Datos_Api->NumValores()) {
			$rickshaw_dimensiones .= ','; 
			$rickshaw_sesiones .= ','; 
			$rickshaw_sesiones_legend .= ','; 
			$rickshaw_paginasvistas .= ','; 
			$rickshaw_paginasvistas_legend .= ','; 
			$rickshaw_paginasvistassesion .= ','; 
			$rickshaw_paginasvistassesion_legend .= ','; 
			$rickshaw_usuarios .= ','; 
			$rickshaw_usuarios_legend .= ','; 
			$rickshaw_tasarebote .= ',';
			$rickshaw_tasarebote_legend .= ',';
			$rickshaw_duracionmediasesion .= ',';
			$rickshaw_duracionmediasesion_legend .= ',';
			$rickshaw_usuarios_nuevos .= ',';
			$rickshaw_usuarios_recurrentes .= ',';
			$rickshaw_usuarios_nuevos_legend .= ',';
			$rickshaw_usuarios_recurrentes_legend .= ',';
		}
	}



	//PARA TOTALES SIN DIMENSIO
	//Creación objeto, parametrización -------------------------------------------------------
	$strDimensions = "";
    $obj_Datos_Api_total = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_total -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_total -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_total -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api_total -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api_total -> metrics              = $metricas ;
    if(empty($filtro)){
	    $obj_Datos_Api_total -> optParams            = array(
	    										'dimensions' => $strDimensions,
												'start-index' => 1,
	                                            'max-results' => 10000); 
    }else{
    	$obj_Datos_Api_total -> optParams            = array(
    											'dimensions' => $strDimensions,
	                                            'filters' => $filtro,
												'start-index' => 1,
	                                            'max-results' => 10000); 
    } 
    $obj_Datos_Api_total -> Construccion();

    //echo var_dump($metricas);

    //Objeto creado ---------------------------------------------------------------------------
	$strA = '{';
	
	$strA .= '"nombre_dimension": "' . $strNombre_Dimension . '",';	
	$strA .= '"rickshaw_dimensiones": "' . $rickshaw_dimensiones . '",';
	
	$strA .= '"rickshaw_sesiones": "' . $rickshaw_sesiones . '",';
	$strA .= '"rickshaw_sesiones_legend": "' . $rickshaw_sesiones_legend . '",';
	$strA .= '"total_sesionesF": "' . $obj_Datos_Api_total->TotalF("sessions") . '",';	
	$strA .= '"total_sesiones": "' . $obj_Datos_Api_total->Total("sessions") . '",';	
	$strA .= '"media_sesionesF": "' . $obj_Datos_Api->MediaF("sessions") . '",';	
	$strA .= '"media_sesiones": "' . $obj_Datos_Api->Media("sessions") . '",';	
	$strA .= '"tilestats_total_sesiones": "' . $obj_Datos_Api_total->Total("sessions") . '",';	
	
	$strA .= '"rickshaw_paginasvistas": "' . $rickshaw_paginasvistas . '",';
	$strA .= '"rickshaw_paginasvistas_legend": "' . $rickshaw_paginasvistas_legend . '",';
	$strA .= '"total_paginasvistasF": "' . $obj_Datos_Api_total->TotalF($pageviews) . '",';
	$strA .= '"total_paginasvistas": "' . $obj_Datos_Api_total->Total($pageviews) . '",';
	$strA .= '"media_paginasvistasF": "' . $obj_Datos_Api->MediaF($pageviews) . '",';
	$strA .= '"media_paginasvistas": "' . $obj_Datos_Api->Media($pageviews) . '",';

	$strA .= '"rickshaw_paginasvistassesion": "' . $rickshaw_paginasvistassesion . '",';
	$strA .= '"rickshaw_paginasvistassesion_legend": "' . $rickshaw_paginasvistassesion_legend . '",';
	$strA .= '"total_paginasvistassesionF": "' . $obj_Datos_Api_total->TotalF($pageviewsPerSession) . '",';
	$strA .= '"total_paginasvistassesion": "' . $obj_Datos_Api_total->Total($pageviewsPerSession) . '",';	
	$strA .= '"media_paginasvistassesionF": "' . $obj_Datos_Api->MediaF($pageviewsPerSession) . '",';	
	$strA .= '"media_paginasvistassesion": "' . $obj_Datos_Api->Media($pageviewsPerSession) . '",';	
	$strA .= '"tilestats_total_paginasvistassesion": "' . $obj_Datos_Api_total->Total($pageviewsPerSession) . '",';

	$strA .= '"rickshaw_usuarios": "' . $rickshaw_usuarios . '",';
	$strA .= '"rickshaw_usuarios_legend": "' . $rickshaw_usuarios_legend . '",';
	$strA .= '"total_usuariosF": "' . $obj_Datos_Api_total->TotalF("users") . '",';
	$strA .= '"total_usuarios": "' . $obj_Datos_Api_total->Total("users") . '",';	
	$strA .= '"media_usuariosF": "' . $obj_Datos_Api->MediaF("users") . '",';	
	$strA .= '"media_usuarios": "' . $obj_Datos_Api->Media("users") . '",';	
	$strA .= '"tilestats_total_usuarios": "' . $obj_Datos_Api_total->Total("users") . '",';
	
	$strA .= '"rickshaw_usuarios_nuevos": "' . $rickshaw_usuarios_nuevos . '",';
	$strA .= '"rickshaw_usuarios_recurrentes": "' . $rickshaw_usuarios_recurrentes . '",';
	$strA .= '"rickshaw_usuarios_nuevos_legend": "' . $rickshaw_usuarios_nuevos_legend . '",';
	$strA .= '"rickshaw_usuarios_recurrentes_legend": "' . $rickshaw_usuarios_recurrentes_legend . '",';	
	$strA .= '"total_usuariosnuevosF": "' . $obj_Datos_Api_total->TotalF("newUsers") . '",';	
	$strA .= '"total_usuariosnuevos": "' . $obj_Datos_Api_total->Total("newUsers") . '",';	
	$strA .= '"media_usuariosnuevosF": "' . $obj_Datos_Api->MediaF("newUsers") . '",';	
	$strA .= '"media_usuariosnuevos": "' . $obj_Datos_Api->Media("newUsers") . '",';	
	$strA .= '"total_usuariosrecurrentesF": "' . number_format($obj_Datos_Api_total->Total("Users",$x) - $obj_Datos_Api_total->Total("newUsers",$x),0,',','.') . '",';	
	$strA .= '"total_usuariosrecurrentes": "' . ($obj_Datos_Api_total->Total("Users",$x) - $obj_Datos_Api_total->Total("newUsers",$x)) . '",';	
	$strA .= '"media_usuariosrecurrentesF": "' . number_format($obj_Datos_Api->Media("Users",$x) - $obj_Datos_Api->Media("newUsers",$x),0,',','.') . '",';	
	$strA .= '"total_usuariosrecurrentes": "' . ($obj_Datos_Api->Total("Users",$x) - $obj_Datos_Api->Total("newUsers",$x)) . '",';	
	
	$strA .= '"rickshaw_tasarebote": "' . $rickshaw_tasarebote . '",';
	$strA .= '"rickshaw_tasarebote_legend": "' . $rickshaw_tasarebote_legend . '",';
	$strA .= '"total_tasareboteF": "' . $obj_Datos_Api_total->TotalF("bounceRate") . '",';
	$strA .= '"total_tasarebote": "' . $obj_Datos_Api_total->Total("bounceRate") . '",';	
	$strA .= '"media_tasareboteF": "' . $obj_Datos_Api->MediaF("bounceRate") . '",';	
	$strA .= '"media_tasarebote": "' . $obj_Datos_Api->Media("bounceRate") . '",';	
	$strA .= '"tilestats_total_rebote": "' . $obj_Datos_Api_total->Total("bounceRate") . '",';

	$strA .= '"rickshaw_duracionmediasesion": "' . $rickshaw_duracionmediasesion . '",';
	$strA .= '"rickshaw_duracionmediasesion_legend": "' .$rickshaw_duracionmediasesion_legend . '",';
	$strA .= '"total_duracionmediasesionF": "' . $obj_Datos_Api_total->TotalF("avgSessionDuration") . '",';	
	$strA .= '"total_duracionmediasesion": "' . $obj_Datos_Api_total->Total("avgSessionDuration") . '",';	
	$strA .= '"media_duracionmediasesionF": "' . $obj_Datos_Api->MediaF("avgSessionDuration") . '",';	
	$strA .= '"media_duracionmediasesion": "' . $obj_Datos_Api->Media("avgSessionDuration") . '"';	
	
	$strA .= '}';
	
	//Devolvemos el json
	return $strA;
} 

function caracteristicas_graficos_bipolar ($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro ){

   //**********************************************************
	$strNombre_Dimension ='Año';
	switch ($strTipo) {
		case 'sexo':
			$strDimensions='ga:userGender';
			$strDimensionsN ='userGender'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
		case 'edad':
			$strDimensions='ga:userAgeBracket';
			$strDimensionsN ='userAgeBracket'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
		case 'interes':
			$strDimensions='ga:interestOtherCategory';
			$strDimensionsN ='interestOtherCategory'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
		case 'dispositivos':
			$strDimensions='ga:deviceCategory';
			$strDimensionsN ='deviceCategory'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
		case 'navegadores':
			$strDimensions='ga:browser';
			$strDimensionsN ='browser'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
	};	


	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= $strFiltro;
	}

	//Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams            = array(
	                                            'dimensions' => $strDimensions,
												'start-index' => 1,
												'sort' => $sort,
	                                            'max-results' => 9);
    }else{
    	$obj_Datos_Api -> optParams            = array(
	                                            'dimensions' => $strDimensions,
	                                            'filters' => $filtro,
												'start-index' => 1,
												'sort' => $sort,
	                                            'max-results' => 9);
    }  
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------

    /*
	left:  [100,95],
	right: [80,101],
    */
    $cont =0;

    if($strTipo == 'dispositivos' || $strTipo == 'sexo' || $strTipo == 'interes' || $strTipo == 'edad' || $strTipo == 'navegadores'){

	 	$strB_dat = "[";
	 	$strB_nom = "[";
	    for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
	    	//porcetange
	    	//$strB_dat .= $obj_Datos_Api->Valor("sessions",$x).",";
	    	if($strTipo == 'sexo' || $strTipo == 'interes' || $strTipo == 'edad'){
		    	$data_port = ($obj_Datos_Api->Valor("sessions",$x)*100)/$obj_Datos_Api->Total("sessions");
		    	$strB_dat .= $data_port.",";
		    }else{
		    	$strB_dat .= $obj_Datos_Api->Valor("sessions",$x).",";
		    }
	    	if( $strTipo == 'sexo' || $strTipo == 'edad' || $strTipo == 'dispositivos' ){
	    		$strB_nom .= "'".Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api)."',";
	    	}else{
	    		$strB_nom .= "'".$obj_Datos_Api->Valor($strDimensionsN,$x)."',";
	    	}
	    	
	    	$cont++;
		}
		$strB_dat = substr($strB_dat, 0, -1);
		$strB_nom = substr($strB_nom, 0, -1);
		$strB_dat .= "]";
	 	$strB_nom .= "]";
	
	}

	$strA = "{";
	$strA .= '"col_dat":"'.$strB_dat.'",';
	$strA .= '"col_nom":"'.$strB_nom.'",';
	$strA .= '"ndata":"'.$cont.'"';
	$strA .= "}";

	return $strA;

}

function caracteristicas_graficos ($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro )
{
	switch ($strTipo) {
		case 'sexo':
			$filas = array();
		    $filas[0] = "male";
		    $filas[1] = "female"; 
			break;
			
		case 'edad':
			$filas = array();
			$filas[0] = "18-24"; 
			$filas[1] = "25-34";
			$filas[2] = "35-44"; 
			$filas[3] = "45-54";
			$filas[4] = "55-64";
			$filas[5] = "65+";
			break;

		case 'dispositivos':
			$filas = array();
			$filas[0] = "desktoc"; 
			$filas[1] = "mobile";
			$filas[2] = "tablet";
			break;
	}

	    	

    //**********************************************************
	$strNombre_Dimension ='Año';
	switch ($strTipo) {
		case 'sexo':
			$strDimensions='ga:userGender';
			$strDimensionsN ='userGender'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
		case 'edad':
			$strDimensions='ga:userAgeBracket';
			$strDimensionsN ='userAgeBracket'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
		case 'interes':
			$strDimensions='ga:interestOtherCategory';
			$strDimensionsN ='interestOtherCategory'; 
			/*$strDimensions='ga:interestInMarketCategory';
			$strDimensionsN ='interestInMarketCategory';
			$strDimensions='ga:interestAffinityCategory';
			$strDimensionsN ='interestAffinityCategory';*/  
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
		case 'dispositivos':
			$strDimensions='ga:deviceCategory';
			$strDimensionsN ='deviceCategory'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
		case 'navegadores':
			$strDimensions='ga:browser';
			$strDimensionsN ='browser'; 
			$metricas  = 'ga:sessions';
			$sort = "-ga:sessions";
			break;
	};	


	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= $strFiltro;
	}

	//Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = $metricas;
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams            = array(
	                                            'dimensions' => $strDimensions,
												'start-index' => 1,
												'sort' => $sort,
	                                            'max-results' => 1000);
    }else{
    	$obj_Datos_Api -> optParams            = array(
	                                            'dimensions' => $strDimensions,
	                                            'filters' => $filtro,
												'start-index' => 1,
												'sort' => $sort,
	                                            'max-results' => 1000);
    }  
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------

    //TRAEMOS EL TOTAL DE SESSIONES REALIZADAS
	$strDimensions_tot='ga:year';
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_totalses = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_totalses -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_totalses -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_totalses -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api_totalses -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api_totalses -> metrics              = 'ga:sessions' ;
    if (empty($filtro)) {
		$obj_Datos_Api_totalses -> optParams        = array(
															'dimensions' => $strDimensions_tot
															);
	}else{
		$obj_Datos_Api_totalses -> optParams        = array(
															'dimensions' => $strDimensions_tot,
															'filters' => $filtro
															);
	}
    

    $obj_Datos_Api_totalses -> Construccion();

    $total_sesiones = $obj_Datos_Api_totalses->Total("sessions");

    //DISPOSITIVOS
    if($strTipo == 'dispositivos' || $strTipo == 'sexo' || $strTipo == 'interes' || $strTipo == 'edad'){
	 
	    for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

	    	if($x==1){
	    		$mayor = $obj_Datos_Api->Valor("sessions",$x);
	    		$mayor_nombre =  Formato_Perspectiva('datatable',$strTipo,$x,$obj_Datos_Api);
	    	}

		}

		$strA = "{";

		if($mayor_nombre != ''){

			if($strTipo=='interes'){
				$strA .= '"nombre_mayor": "'.traduce_de_ingles($mayor_nombre,$_COOKIE["idioma_usuario"])	.'",';	
			}else{		
					$strA .= '"nombre_mayor":'.$mayor_nombre.',';	
			}

		}else{
				$strA .= '"nombre_mayor": " ",';
			}

		$strA .= '"data_mayor_formateado":"'.number_format(porcentaje_sesiones_res($mayor,$obj_Datos_Api->Total("sessions")),2,",",".").'",';	
		$strA .= '"data_mayor":"'.porcentaje_sesiones_res($mayor,$obj_Datos_Api->Total("sessions")).'",';
		$strA .= '"data_total":"'.number_format(porcentaje_sesiones_res($obj_Datos_Api->Total("sessions"),$total_sesiones),0,",",".").'"';
		$strA .= "}";
	}

	//DISPOSITIVOS
    if($strTipo == 'navegadores'){
	    
	    //Inicializamos variables
	    $primero_data   = "";
		$primero_nombre = 0;
		$segundo_data   = "";
		$segundo_nombre = 0;
		$tercero_data   = "";
		$tercero_nombre = 0;
	 	$cont=0;
	 	

	    for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

			switch ($cont) {
				case 0:
					$primero_data   = $obj_Datos_Api->Valor("sessions",$x);
					$primero_nombre = $obj_Datos_Api->Valor($strDimensionsN,$x);
					break;
				case 1:
					$segundo_data   = $obj_Datos_Api->Valor("sessions",$x);
					$segundo_nombre = $obj_Datos_Api->Valor($strDimensionsN,$x);
					break;
				case 2:
					$tercero_data   = $obj_Datos_Api->Valor("sessions",$x);
					$tercero_nombre = $obj_Datos_Api->Valor($strDimensionsN,$x);
					break;

			}
			$cont++;

		}

		$strA = "{";

		$strA .= '"primero_nombre":"'.$primero_nombre.'",';
		$strA .= '"primero_data":"'.porcentaje_sesiones_res($primero_data,$obj_Datos_Api->Total("sessions")).'",';
		$strA .= '"primero_data_f":"'.number_format(porcentaje_sesiones_res($primero_data,$obj_Datos_Api->Total("sessions")),2,",",".").'",';
		$strA .= '"primero_img":"<img src=\''.suImagen($primero_nombre).'\' />",';

		$strA .= '"segundo_nombre":"'.$segundo_nombre.'",';
		$strA .= '"segundo_data":"'.porcentaje_sesiones_res($segundo_data,$obj_Datos_Api->Total("sessions")).'",';
		$strA .= '"segundo_data_f":"'.number_format(porcentaje_sesiones_res($segundo_data,$obj_Datos_Api->Total("sessions")),2,",",".").'",';
		$strA .= '"segundo_img":"<img src=\''.suImagen($segundo_nombre).'\' />",';

		$strA .= '"tercero_nombre":"'.$tercero_nombre.'",';
		$strA .= '"tercero_data":"'.porcentaje_sesiones_res($tercero_data,$obj_Datos_Api->Total("sessions")).'",';
		$strA .= '"tercero_data_f":"'.number_format(porcentaje_sesiones_res($tercero_data,$obj_Datos_Api->Total("sessions")),2,",",".").'",';
		$strA .= '"tercero_img":"<img src=\''.suImagen($tercero_nombre).'\' />"';

		$strA .= "}";
	}


	//Devolvemos el json
	return $strA;
} 

//Funcion para los que tengan su nombre como imagen
function suImagen($nombre){
	switch (strtolower($nombre)) {
		case 'chrome':
			$imgruta = "../images/navegadores/chrome.png";
			break;
		case 'safari':
			$imgruta = "../images/navegadores/safari.png";
			break;
		case 'firefox':
			$imgruta = "../images/navegadores/firefox.png";
			break;
		case 'internet explorer':
			$imgruta = "../images/navegadores/internet-explorer.png";
			break;
		case 'android browser':
			$imgruta = "../images/navegadores/android-browser.png";
			break;
		case 'opera':
			$imgruta = "../images/navegadores/opera.png";
			break;
		default:
			$imgruta = "../images/navegadores/none.png";
			break;
	}
	return $imgruta;
}



function dia_semana($fecha) {
	global $trans;

	$dias = array("LUN","MAR","MIE","JUE","VIE","SAB","DOM");
	return $trans->__($dias[date('N', strtotime($fecha))-1], false);
}


function sesiones_totales( $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro,$filtrado=0)
{

	$strDimensions='ga:year';
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:sessions' ;
    $obj_Datos_Api -> optParams            = array(
											'dimensions' => $strDimensions
												);

    $obj_Datos_Api -> Construccion();
 
	
	//Devolvemos el total de sesiones
	if($filtrado == 0){
		return $obj_Datos_Api->TotalF("sessions");
	}else{
		return $obj_Datos_Api->Total("sessions");
	}
}

function navegacion_mokup_imgs($idVistaAnalytics)
{
		$APIKEY = "AIzaSyBY6QNL3HBkDm2h7CMbFXKeYoE0SQb5hA4";
		//$strEstrategia = "mobile";
		$strEstrategia = "desktop";
		//Buscamos la url de la vista

		$strDominio = DominioVista($idVistaAnalytics);
	    $strUrl = "http://www.". $strDominio;
	    $strCampos ="fields=score%2Cscreenshot";
	    $strLlamada = "https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=".$strUrl."&locale=ES&filter_third_party_resources=false&screenshot=true&strategy=".$strEstrategia."&key=".$APIKEY ."&".$strCampos;

		//Iniciamos cURL junto con la URL
		$gInfo = curl_init($strLlamada);

		curl_setopt ($gInfo ,CURLOPT_SSL_VERIFYPEER,FALSE);
		//Agregamos opciones necesarias para leer
		curl_setopt($gInfo,CURLOPT_RETURNTRANSFER,TRUE);
		// Capturamos la URL
		$gInfo = curl_exec($gInfo);
		//Descodificamos para leer
		$imginfo = json_decode($gInfo,true);


		$imgData = $imginfo["screenshot"]["data"];

	    $imgData  = str_replace("_", "/", $imgData);
	    $imgData  = str_replace("-", "+", $imgData);

	    $strEstrategia = "mobile";
	    $strLlamada = "https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=".$strUrl."&locale=ES&filter_third_party_resources=false&screenshot=true&strategy=".$strEstrategia."&key=".$APIKEY ."&".$strCampos;
		//Iniciamos cURL junto con la URL
		$gInfo = curl_init($strLlamada);

		curl_setopt ($gInfo ,CURLOPT_SSL_VERIFYPEER,FALSE);
		//Agregamos opciones necesarias para leer
		curl_setopt($gInfo,CURLOPT_RETURNTRANSFER,TRUE);
		// Capturamos la URL
		$gInfo = curl_exec($gInfo);
		//Descodificamos para leer
		$imginfo = json_decode($gInfo,true);


		$imgDatab = $imginfo["screenshot"]["data"];

	    $imgDatab  = str_replace("_", "/", $imgDatab);
	    $imgDatab  = str_replace("-", "+", $imgDatab);

	    $devuelve = '<img id="imgescritorio" class="mimi" src="data:image/jpeg;base64,'.$imgData.'" /><br><img id="imgmovil" class="mimi" src="data:image/jpeg;base64,'.$imgDatab.'" /><br>';

		return $devuelve;
}


//SEO
function seo_enlaces_externos($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro){

	//Para haya el año anterior
	$fechaCom = date("Y-m-d"); //Fecha final;
	$fechaCom_manno = strtotime ( '-1 year' , strtotime ( $fechaCom ) ) ; 
	$fechaCom_manno = date ( 'Y-m-d' , $fechaCom_manno );//fecha Inicial

	//Dominios
    $obj_Datos_Api_dominios = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_dominios -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_dominios -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_dominios -> startdate            = $fechaCom_manno ;
    $obj_Datos_Api_dominios -> enddate              = $fechaCom ;
    $obj_Datos_Api_dominios -> metrics              = 'ga:sessions' ;
    $obj_Datos_Api_dominios -> optParams            = array(
														'dimensions' => 'ga:source',
														'filters' => 'ga:medium==referral;ga:SocialNetwork==(not set)',
														'start-index' => 1,
			                                            'max-results' => 1
														);


    $obj_Datos_Api_dominios -> Construccion();
    //Páginas
    $obj_Datos_Api_paginas = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_paginas -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_paginas -> idVista              ='ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_paginas -> startdate            = $fechaCom_manno ;
    $obj_Datos_Api_paginas -> enddate              = $fechaCom ;
    $obj_Datos_Api_paginas -> metrics              = 'ga:sessions' ;
    $obj_Datos_Api_paginas -> optParams            = array(
														'dimensions' => 'ga:fullReferrer',
														'filters' => 'ga:medium==referral;ga:SocialNetwork==(not set)',
														'start-index' => 1,
			                                            'max-results' => 1
														);

    $obj_Datos_Api_paginas -> Construccion();

    $strA = '{';
	
	$strA .= '"total_enlaces_dominios": "' . number_format($obj_Datos_Api_dominios->NumValoresReal(),0,',','.')  . '",';	
	$strA .= '"total_enlaces_paginas": "' . number_format($obj_Datos_Api_paginas->NumValoresReal(),0,',','.')  . '"';

	$strA .= '}';

	return $strA;
}

function table_datatable_enlaces_seo($strTipo, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro,$DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir){

	switch ($strTipo) {
		case 'dominiostrafico':
			$strDimensions  ='ga:source';
			$strDimensionsN ='source'; 
			$metricas  = 'ga:sessions';
			$print  = 'sessions';
			$filtro = 'ga:medium==referral;ga:SocialNetwork==(not set)';
			$sort = '-ga:sessions';
			$array_Ordenacion = array('sessions');
			break;
		case 'enlacestrafico':
			$strDimensions  ='ga:fullReferrer';
			$strDimensionsN ='fullReferrer'; 
			$metricas  = 'ga:sessions';
			$print  = 'sessions';
			$filtro = 'ga:medium==referral;ga:SocialNetwork==(not set)';
			$sort = '-ga:sessions';
			$array_Ordenacion = array('sessions');
			break;
		case 'dominiosconversiones':
			$strDimensions  ='ga:source';
			$strDimensionsN ='source'; 
			$metricas  = 'ga:goalCompletionsAll';
			$print  = 'goalCompletionsAll';
			$filtro = 'ga:medium==referral;ga:SocialNetwork==(not set)';
			$sort = '-ga:goalCompletionsAll';
			$array_Ordenacion = array('goalCompletionsAll');
			break;
		case 'enlacesconversiones':
			$strDimensions  ='ga:fullReferrer';
			$strDimensionsN ='fullReferrer'; 
			$metricas  = 'ga:goalCompletionsAll';
			$print  = 'goalCompletionsAll';
			$filtro = 'ga:medium==referral;ga:SocialNetwork==(not set)';
			$sort = '-ga:goalCompletionsAll';
			$array_Ordenacion = array('goalCompletionsAll');
			break;
	}

	$DirOrdenacion='-';         
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	//Para haya el año anterior
	$fechaCom = date("Y-m-d"); //Fecha final;
	$fechaCom_manno = strtotime ( '-1 year' , strtotime ( $fechaCom ) ) ; 
	$fechaCom_manno = date ( 'Y-m-d' , $fechaCom_manno );//fecha Inicial

	//Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $fechaCom_manno;
    $obj_Datos_Api -> enddate              = $fechaCom;
    $obj_Datos_Api -> metrics              = $metricas;
   	$obj_Datos_Api -> optParams        = array(
											'dimensions' => $strDimensions,
											'filters' => $filtro,
	                    'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											'start-index' => $DataTables_start + 1,
	                    'max-results' => $DataTables_length);	                                          
   	$obj_Datos_Api -> Construccion();
 //Objeto creado ---------------------------------------------------------------------------
 	if($obj_Datos_Api->NumValores()==0){$DataTables_draw =0;}	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"data":[';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		$strA .= '[';
		$strA .= '"'.$obj_Datos_Api->Valor($strDimensionsN,$x).'",';
		$strA .= '"'.$obj_Datos_Api->Valor($print,$x).'"';
		$strA .= '],';
	}
	$strA = substr($strA, 0, -1);
	$strA .= "]";
	$strA .= "}";

	return $strA;
}

function table_datatable_terminoscontrol($idvista,$vistaUrl,$mesSelect,$control,$DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir,$pag,$buscador,$tipo){

	//Para la ordenacion
	if ($DataTables_OrderDir=='asc') {$DirOrdenacion='ASC';}else{$DirOrdenacion='DESC';}
	if($tipo == "term"){
		$array_Ordenacion = array('LOCDVS_TERMINO','CATEGORIA','TERMINOCONTROL','LOCDVS_W_IMPRESIONES','LOCDVS_M_IMPRESIONES','LOCDVS_W_CLICS','LOCDVS_M_CLICS','LOCDVS_W_CTR','LOCDVS_M_CTR','LOCDVS_W_POSICIONMEDIA','LOCDVS_M_POSICIONMEDIA');
	}else{
		$array_Ordenacion = array('LOCDVP_TERMINO','CATEGORIA','TERMINOCONTROL','LOCDVP_W_IMPRESIONES','LOCDVP_M_IMPRESIONES','LOCDVP_W_CLICS','LOCDVP_M_CLICS','LOCDVP_W_CTR','LOCDVP_M_CTR','LOCDVP_W_POSICIONMEDIA','LOCDVP_M_POSICIONMEDIA');
	}
	$sort = "ORDER BY ".$array_Ordenacion[$DataTables_OrderColumn]." ".$DirOrdenacion;


	//Comparaciones
	$mesSelect_anno = $mesSelect[0].$mesSelect[1].$mesSelect[2].$mesSelect[3];
	$mesSelect_mes  = $mesSelect[4].$mesSelect[5];
	//Restamos un mes
	//Primero comprobamos si es 01 si es asi restamos un año y el mes 12
	if($mesSelect_mes == 01){
	  $mesSelect_anno_ant = intval($mesSelect_anno) -1;
	  $mesSelect_mes_ant = 12;
	}else{
	  $mesSelect_anno_ant = $mesSelect_anno;
	  $mesSelect_mes_ant = intval($mesSelect_mes) -1;
	  if($mesSelect_mes_ant < 10){
	    $mesSelect_mes_ant = '0'.$mesSelect_mes_ant;
	  }
	}
	$mesSelect_ant = $mesSelect_anno_ant.$mesSelect_mes_ant;
	//Comprobamos si existe ese mes en los terminos
	$sw=0;
	$meses = comboSeo($vistaUrl,$tipo);
	if($meses != 0){
		foreach ($meses as $key => $mes) {
		  if($mes == $mesSelect_ant){
		    $sw=1;
		  }
		}
	}
	if($sw == 1){
		if($tipo == "term"){
			$teminosControl_ant = terminosControl_term($vistaUrl,$mesSelect_ant,$control,$sort,$pag,$buscador);
		}else{
			$teminosControl_ant = terminosControl_pags($vistaUrl,$mesSelect_ant,$control,$sort,$pag,$buscador);
		}
		
	}
	if($tipo == "term"){
		$teminosControl = terminosControl_term($vistaUrl,$mesSelect,$control,$sort,$pag,$buscador);
	}else{
		$teminosControl = terminosControl_pags($vistaUrl,$mesSelect,$control,$sort,$pag,$buscador);
	}
	


	if($teminosControl["cantidad"] == 0){
		$strA = '{"draw":'.$DataTables_draw.',';
		$strA .= '"recordsTotal":0,';
		$strA .= '"recordsFiltered":0,';
		$strA .= '"data":[';
		$strA .= "]";
		$strA .= "}";
	}else{
		
		//Para el paginador
		$desde = $DataTables_start;
		$hasta = $DataTables_length+$DataTables_start;
		$esta_en = 0;

		$strB="";
		foreach ($teminosControl["elementos"] as $key => $tc) {
			if($esta_en >= $desde && $esta_en < $hasta){
				$strB .= "[";

				if( IsAdmin($idvista,$_COOKIE["usuario"]["email"]) ){

					if($tc["control"] == 1 ){

						//Completamos con categorias
						$straCat  = '<div class=\'input-group contcatsc\'><input id=\'select'.$key.'\' class=\'form-control inputre\' value=\''.$tc["categoria"].'\' style=\' padding: 7px !important; height: 33px;\' aria-label=\'Text input with segmented button dropdown\' type=\'text\' /><div class=\'input-group-btn\'><button type=\'button\' class=\'btn btn-default dropdown-toggle\' data-toggle=\'dropdown\' aria-expanded=\'false\'><span class=\'caret\'></span><span class=\'sr-only\'>Toggle Dropdown</span></button><ul class=\'dropdown-menu dropdown-menu-right combocatseo\' role=\'menu\'>';
					    //Devuelvo un ajax con los totales 
						$cat_seo = categorias_seo($vistaUrl,$tipo);
						if ($cat_seo["cantidad"] == 0) {
							$straCat .= '<li>No hay ninguna categoría</li>';
						}else{
							foreach ($cat_seo["elementos"] as $clv => $cs) {
							
							 $straCat .= '<li><a name=\''.$cs['categoria'].'\' class=\'linkselectcat\' onClick=\'anadiralselect('.$key.',\"'.$cs['categoria'].'\")\' >'.$cs['categoria'].'('.$cs['contador'].')</a></li>';
							
							}
						}
					    $straCat .= '</ul></div></div>';
					    //******************************


						$strB .= '" <div id=\'caja'.$key.'\' class=\'termc\' >'. str_replace('"', ' ', $tc["termino"]).'</div> <div id=\'input'.$key.'\' class=\'termcb oculto\' > <input type=\'text\' id=\'inputdata'.$key.'\' class=\'trium\' value=\''.str_replace('"', ' ', $tc["termino"]).'\' /> '.$straCat.' <button type=\'button\' onClick=\'cancelareditar('.$key.')\' class=\'btn btn-default eliminar_termino controlterm\' aria-label=\'Left Align\'><span class=\'glyphicon glyphicon-remove\' aria-hidden=\'true\'></span></button> <button type=\'button\' onClick=\'editarGuardar_'.$tipo.'('.$key.',\"'.$vistaUrl.'\",\"'.str_replace('"', ' ', $tc["termino"]).'\",\"'.$tc["categoria"].'\")\' class=\'btn btn-default eliminar_termino controlterm\' aria-label=\'Left Align\'><span class=\'glyphicon glyphicon-floppy-disk\' aria-hidden=\'true\'></span></button> </div> <button type=\'button\' onClick=\'editarpencil('.$key.')\' class=\'btn btn-default eliminar_termino controlterm\' id=\'pencil'.$key.'\' aria-label=\'Left Align\'><span class=\'glyphicon glyphicon-pencil\' aria-hidden=\'true\'></span></button>",';
					}else{
						$strB .= '" <div id=\'caja'.$key.'\' class=\'termc\' >'. str_replace('"', ' ', $tc["termino"]).'</div>",';

					}

					if($tc["control"] == 1 ){
						$cntr = '<a name=\''.str_replace('"', ' ', $tc["termino"]).'\' class=\'btncontrolsn\' onClick=\'estrellaDown_'.$tipo.'(\"'.$vistaUrl.'\",\"'.str_replace('"', ' ', $tc["termino"]).'\")\'><img name=\''.$vistaUrl.'\' class=\'imgwidcontrol\' src=\'../images/control_si.png\'></a>'; 
					}else{ 
						$cntr = '<a name=\''.str_replace('"', ' ', $tc["termino"]).'\' class=\'btncontrolsn\' onClick=\'estrellaUp_'.$tipo.'(\"'.$vistaUrl.'\",\"'.str_replace('"', ' ', $tc["termino"]).'\")\'><img name=\''.$vistaUrl.'\' class=\'imgwidcontrol\' src=\'../images/control_no.png\'></a>'; 
					} 	
				

				}else{
					$strB .= '"'. str_replace('"', ' ', $tc["termino"])       .'",';
					if($tc["control"] == 1 ){
						$cntr = '<img class=\'imgwidcontrol\' src=\'../images/control_si.png\'>'; 
					}else{ 
						$cntr = '<img class=\'imgwidcontrol\' src=\'../images/control_no.png\'>'; 
					} 		                              
				
				}
				$strB .= '"<span class=\'catizq\'>'. $tc["categoria"].'</span>",';
				$strB .= '"'. $cntr	.'",';

				if($sw==1){
					$comp_impresiones   = DevolverContenido_Total($tc["impresiones_w"],$teminosControl_ant["elementos"][$key]["impresiones_w"],number_format($tc["impresiones_w"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["impresiones_w"],0,',','.'));
					$comp_clics         = DevolverContenido_Total($tc["clics_w"],$teminosControl_ant["elementos"][$key]["clics_w"],number_format($tc["clics_w"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["clics_w"],0,',','.'));
					$comp_ctr           = DevolverContenido_Total($tc["ctr_w"],$teminosControl_ant["elementos"][$key]["ctr_w"],number_format($tc["ctr_w"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["ctr_w"],0,',','.'));
					$comp_posmedia      = DevolverContenido_Total($tc["posmedia_m"],$teminosControl_ant["elementos"][$key]["posmedia_w"],number_format($tc["posmedia_w"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["posmedia_w"],0,',','.'));
					$comp_impresiones_m = DevolverContenido_Total($tc["impresiones_m"],$teminosControl_ant["elementos"][$key]["impresiones_m"],number_format($tc["impresiones_m"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["impresiones_m"],0,',','.'));
					$comp_clics_m       = DevolverContenido_Total($tc["clics_m"],$teminosControl_ant["elementos"][$key]["clics_m"],number_format($tc["clics_m"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["clics_m"],0,',','.'));
					$comp_ctr_m         = DevolverContenido_Total($tc["ctr_m"],$teminosControl_ant["elementos"][$key]["ctr_m"],number_format($tc["ctr_m"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["ctr_m"],0,',','.'));
					$comp_posmedia_m    = DevolverContenido_Total($tc["posmedia_m"],$teminosControl_ant["elementos"][$key]["posmedia_m"],number_format($tc["posmedia_m"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["posmedia_m"],0,',','.'));

				}else{
					$comp_impresiones   = "";
					$comp_clics         = "";
					$comp_ctr           = "";
					$comp_posmedia      = "";
					$comp_impresiones_m = "";
					$comp_clics_m       = "";
					$comp_ctr_m         = "";
					$comp_posmedia_m    = "";
				}
				//DevolverContenido_Total($Valor,$ValorAnt,$ValorF,$ValorAntF)
				
				$strB .= '"'.$comp_impresiones  . number_format($tc["impresiones_w"],0,',','.') .'",';
				$strB .= '"'.$comp_impresiones_m. number_format($tc["impresiones_m"],0,',','.') .'",';
				$strB .= '"'.$comp_clics        . number_format($tc["clics_w"],0,',','.')       .'",';
				$strB .= '"'.$comp_clics_m      . number_format($tc["clics_m"],0,',','.')       .'",';
				$strB .= '"'.$comp_ctr          . number_format($tc["ctr_w"],1,',','.')         .'%",';
				$strB .= '"'.$comp_ctr_m        . number_format($tc["ctr_m"],1,',','.')         .'%",';
				$strB .= '"'.$comp_posmedia     . number_format($tc["posmedia_w"],1,',','.')    .'",';
				$strB .= '"'.$comp_posmedia_m   . number_format($tc["posmedia_m"],1,',','.')    .'"';
				$strB .= "],";
			}
			$esta_en++;
		}
		$strB = substr($strB, 0, -1);

		$strA = '{"draw":'.$DataTables_draw.',';
		$strA .= '"recordsTotal":' . $teminosControl["cantidad"] .',';
		$strA .= '"recordsFiltered":' . $teminosControl["cantidad"] .',';
		$strA .= '"data":[';
		$strA .= $strB;
		$strA .= "]";
		$strA .= "}";
	}

	return $strA;

	/*
	<button id="btn_abrirmodaltermino" type="button" class="btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
	<button id="btn_abrirmodaltermino" type="button" class="btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
	*/

}
function table_datatable_pagsentradal($vistaUrl,$mesSelect,$control,$DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir,$buscador){

	if ($DataTables_OrderDir=='asc') {$DirOrdenacion='ASC';}else{$DirOrdenacion='DESC';}
	$array_Ordenacion = array('LOCDVP_URL','LOCDVP_w_IMPRESIONES','LOCDVP_m_IMPRESIONES','LOCDVP_w_CLICS','LOCDVP_m_CLICS','LOCDVP_w_CTR','LOCDVP_m_CTR','LOCDVP_w_POSICIONMEDIA','LOCDVP_m_POSICIONMEDIA');
	$sort = "ORDER BY ".$array_Ordenacion[$DataTables_OrderColumn]." ".$DirOrdenacion;

	//Comparaciones
	$mesSelect_anno = $mesSelect[0].$mesSelect[1].$mesSelect[2].$mesSelect[3];
	$mesSelect_mes  = $mesSelect[4].$mesSelect[5];
	//Restamos un mes
	//Primero comprobamos si es 01 si es asi restamos un año y el mes 12
	if($mesSelect_mes == 01){
	  $mesSelect_anno_ant = intval($mesSelect_anno) -1;
	  $mesSelect_mes_ant = 12;
	}else{
	  $mesSelect_anno_ant = $mesSelect_anno;
	  $mesSelect_mes_ant = intval($mesSelect_mes) -1;
	  if($mesSelect_mes_ant < 10){
	    $mesSelect_mes_ant = '0'.$mesSelect_mes_ant;
	  }
	}
	$mesSelect_ant = $mesSelect_anno_ant.$mesSelect_mes_ant;
	//Comprobamos si existe ese mes en los terminos
	$sw=0;
	$meses = comboSeo($vistaUrl);
	if($meses != 0){
		foreach ($meses as $key => $mes) {
		  if($mes == $mesSelect_ant){
		    $sw=1;
		  }
		}
	}
	if($sw == 1){
		$teminosControl_ant = paginasEntrada($vistaUrl,$mesSelect_ant,$sort,$buscador);
	}
	$teminosControl = paginasEntrada($vistaUrl,$mesSelect,$sort,$buscador);


	if($teminosControl["cantidad"] == 0){
		$strA = '{"draw":'.$DataTables_draw.',';
		$strA .= '"recordsTotal":0,';
		$strA .= '"recordsFiltered":0,';
		$strA .= '"data":[';
		$strA .= "]";
		$strA .= "}";
	}else{
		
		//Para el paginador
		$desde = $DataTables_start;
		$hasta = $DataTables_length+$DataTables_start;
		$esta_en = 0;

		$strB="";
		foreach ($teminosControl["elementos"] as $key => $tc) {
			if($esta_en >= $desde && $esta_en < $hasta){

				if($sw==1){
					$comp_impresiones_w = DevolverContenido_Total($tc["impresiones_w"],$teminosControl_ant["elementos"][$key]["impresiones_w"],number_format($tc["impresiones_w"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["impresiones_w"],0,',','.'));
					$comp_clics_w       = DevolverContenido_Total($tc["clics_w"],$teminosControl_ant["elementos"][$key]["clics_w"],number_format($tc["clics_w"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["clics_w"],0,',','.'));
					$comp_ctr_w         = DevolverContenido_Total($tc["ctr_w"],$teminosControl_ant["elementos"][$key]["ctr_w"],number_format($tc["ctr_w"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["ctr_w"],0,',','.'));
					$comp_posmedia_w    = DevolverContenido_Total($tc["posmedia_w"],$teminosControl_ant["elementos"][$key]["posmedia_w"],number_format($tc["posmedia_w"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["posmedia_w"],0,',','.'));
					$comp_impresiones_m = DevolverContenido_Total($tc["impresiones_m"],$teminosControl_ant["elementos"][$key]["impresiones_m"],number_format($tc["impresiones_m"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["impresiones_m"],0,',','.'));
					$comp_clics_m       = DevolverContenido_Total($tc["clics_m"],$teminosControl_ant["elementos"][$key]["clics_m"],number_format($tc["clics_m"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["clics_m"],0,',','.'));
					$comp_ctr_m         = DevolverContenido_Total($tc["ctr_m"],$teminosControl_ant["elementos"][$key]["ctr_m"],number_format($tc["ctr_m"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["ctr_m"],0,',','.'));
					$comp_posmedia_m    = DevolverContenido_Total($tc["posmedia_m"],$teminosControl_ant["elementos"][$key]["posmedia_m"],number_format($tc["posmedia_m"],0,',','.'),number_format($teminosControl_ant["elementos"][$key]["posmedia_m"],0,',','.'));
				}else{
					$comp_impresiones_w = "";
					$comp_clics_w       = "";
					$comp_ctr_w         = "";
					$comp_posmedia_w    = "";
					$comp_impresiones_m = "";
					$comp_clics_m       = "";
					$comp_ctr_m         = "";
					$comp_posmedia_m    = "";
				}

				$strB .= "[";
				$strB .= '"'. str_replace('"', ' ', $tc["termino"])       .'",';						                              
				$strB .= '"'.$comp_impresiones_w. number_format($tc["impresiones_w"],0,',','.') .'",';
				$strB .= '"'.$comp_impresiones_m. number_format($tc["impresiones_m"],0,',','.') .'",';
				$strB .= '"'.$comp_clics_w      . number_format($tc["clics_w"],0,',','.')       .'",';
				$strB .= '"'.$comp_clics_m      . number_format($tc["clics_m"],0,',','.')       .'",';
				$strB .= '"'.$comp_ctr_w        . number_format($tc["ctr_w"],2,',','.')         .'%",';
				$strB .= '"'.$comp_ctr_m        . number_format($tc["ctr_m"],2,',','.')         .'%",';
				$strB .= '"'.$comp_posmedia_w   . number_format($tc["posmedia_w"],2,',','.')    .'",';
				$strB .= '"'.$comp_posmedia_m   . number_format($tc["posmedia_m"],2,',','.')    .'"';
				$strB .= "],";
			}
			$esta_en++;
		}
		$strB = substr($strB, 0, -1);

		$strA = '{"draw":'.$DataTables_draw.',';
		$strA .= '"recordsTotal":' . $teminosControl["cantidad"] .',';
		$strA .= '"recordsFiltered":' . $teminosControl["cantidad"] .',';
		$strA .= '"data":[';
		$strA .= $strB;
		$strA .= "]";
		$strA .= "}";
	}

	return $strA;

	/*
	<button id="btn_abrirmodaltermino" type="button" class="btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
	<button id="btn_abrirmodaltermino" type="button" class="btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
	*/

}


function campanas_datatable ($strTipo,$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro, $idPropiedad )
{

	switch ($strTipo) {
		case 'campanas':
			$trans = "Goal";
			$resumenif = "no";
			break;
		case 'campanas_resumen':
			$trans = "Goal";
			$resumenif = "si";
			break;
		case 'ecomerce':
			$trans = "Transaction";
			$resumenif = "no";
			break;
		case 'ecomerce_resumen':
			$trans = "Transaction";
			$resumenif = "si";
			break;
		
	}
	//echo $datFechaInicioFiltro;

	$array_completo = array_campanas($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPropiedad, $trans);
	/*if ($blnComparacion) {
		$array_completo_ant = array_campanas($strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPropiedad);
	}*/
	/*echo "<pre>";
	 echo var_dump($array_completo);
	 echo "</pre>";*/
	$an 					   = 0;
	$an_nombre				   = "";
    $tot_cont 				   = 0; 
	$tot_sesiones 		  	   = "";
	$tot_rebote 		  	   = "";
	$tot_pagsesion        	   = "";
	$tot_usuarios         	   = "";
	$tot_convpinteraccion 	   = "";
	$tot_convasistidas    	   = "";
	$tot_convuinteraccion 	   = "";
	$tot_ga 			  	   = "";
	$tot_convtotal        	   = "";
	$tot_transtotal        	   = "";
	$tot_assistedValue	  	   = "";
	$tot_firstInteractionValue = "";
	$tot_lastInteractionValue  = "";
	$tot_trans  			   = "";
	$tot_transactionRevenue    = "";
	$tot_transactionShipping   = "";
	$tot_transactionTax    	   = "";
	$agr 					   = 0;
	//Devolvemos el json
	//Pintamos la tabla por php
	$campana_anterior = "";
	$tabla = "";
	$tablaclass = "par";
	$where = "";
	if(!empty($array_completo)){
		foreach ($array_completo as $key => $filas) {
			$idCampana = crearActualizarCampana($filas["campana"], $filas["medium"], $filas["source"], $idPropiedad);

			//Creamos la where para luego añadir las campañas de la base de datos
			$where .= "AND NOT ([LOCC_CAMPAING]='".$filas["campana"]."' AND [LOCC_MEDIUM]='".$filas["medium"]."' AND [LOCC_SOURCE]= '".$filas["source"]."') "; 

			if($campana_anterior != $filas["campana"]){


				if($agr == 1){
					//A la hora de pintar el total de una campaña traemos los totales de ella
					$tot_campana = totalCampanas($array_completo[$key-1]["campana"],$strProyectoAsociado,$idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro,$trans);
					$tot_sesiones 		  	  = $tot_campana["tot_sesiones"];
					$tot_rebote 		  	  = $tot_campana["tot_rebote"];
					$tot_pagsesion        	  = $tot_campana["tot_pagsesion"];
					$tot_usuarios         	  = $tot_campana["tot_usuarios"];
					$tot_ga        		  	  = $tot_campana["tot_usuarios"];
					$tot_trans        		  = $tot_campana["tot_trans"];
					$tot_transactionRevenue   = $tot_campana["tot_transactionRevenue"];
					$tot_transactionShipping  = $tot_campana["tot_transactionShipping"];
					$tot_transactionTax       = $tot_campana["tot_transactionTax"];
					if($resumenif != "si"){$mostrartotal = $an_nombre; }else{ $mostrartotal = 'Totales'; }
					$tabla .= "<tr name=\"grupo".$an."\" class=\"filadesplegable ".$tablaclass."\" id=\"".$an_nombre."\">";
					$tabla .= "<td class=\"total tittot primera\"><span class=\"subtotal_tit\">".$mostrartotal."</span></td>";
					$tabla .= "<td class=\"total\"></td>";
					$tabla .= "<td class=\"total\"></td>";
					$tabla .= "<td class=\"total\">".$tot_sesiones."</td>";
					$tabla .= "<td class=\"total\">".$tot_usuarios."</td>";
					$tabla .= "<td class=\"total\">".$tot_rebote."</td>";
					$tabla .= "<td class=\"total\">".$tot_pagsesion."</td>";
					
					if($strTipo=="campanas"){
						//$tabla .= "<td class='total'>".number_format($tot_convpinteraccion,0,',','.')."</td>";
						//$tabla .= "<td class='total'>".number_format($tot_convasistidas,0,',','.')."</td>";
						//$tabla .= "<td class='total'>".number_format($tot_convuinteraccion,0,',','.')."</td>";
						$tabla .= "<td class=\"total\">".number_format($tot_convuinteraccion,0,',','.')."</td>";
						$tabla .= "<td class=\"total\">0</td>";
						//$tabla .= "<td class='total'>".number_format($tot_ga,0,',','.')."</td>";
					}else{
						//$tabla .= "<td class='total'>".number_format($tot_convpinteraccion,0,',','.')." (".$tot_firstInteractionValue.")</td>";
						//$tabla .= "<td class='total'>".number_format($tot_convasistidas,0,',','.')." (".$tot_assistedValue.")</td>";
						//$tabla .= "<td class='total'>".number_format($tot_convuinteraccion,0,',','.')." (".$tot_lastInteractionValue.")</td>";
						$tabla .= "<td class=\"total\">".number_format($tot_convtotal,0,',','.')."</td>";
						$tabla .= "<td class=\"total\">".number_format($tot_transtotal,0,',','.')."€</td>";
						//$tabla .= "<td class='total'>".number_format($tot_trans,0,',','.')."<br>Neto:".$tot_transactionRevenue."<br>Envío:".$tot_transactionShipping."<br>Iva:".$tot_transactionTax."</td>";
					}

					if($resumenif == "no"){
						$tabla .= "<td class=\"total\"><a onClick='verCampana(\"".$filas["campana"]."\",\"\",\"\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a></td>";
					}else{
						$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;;
						$tabla .= "<td class=\"total\"><a class='btn_vercampana' href='".$href."'><img class='imagensearch' src='../admin/Envios/estructura_email/images/search.png'/></a></td>";
					}

					$tabla .= "</tr>";
					$tot_cont =0;
					$an++;

					//En totales le
					$tot_sesiones 		       = "";
					$tot_rebote 		       = "";
					$tot_pagsesion             = "";
					$tot_usuarios              = "";
					$tot_convasistidas         = "";
					$tot_convpinteraccion      = "";
					$tot_convuinteraccion      = "";
					$tot_convtotal        	   = "";
					$tot_transtotal        	   = "";
					$tot_assistedValue	  	   = "";
					$tot_firstInteractionValue = "";
					$tot_lastInteractionValue  = "";
					$tot_ga 			       = "";
					$tot_trans  			   = "";
					$tot_transactionRevenue    = "";
					$tot_transactionShipping   = "";
					$tot_transactionTax    	   = "";
					$agr = 0;
				}
				
				if($tablaclass == "par"){
					$tablaclass = "impar";
				}else{
					$tablaclass = "par";
				}



				if($array_completo[$key+1]["campana"] == $filas["campana"]){ 
					if($resumenif == "no"){
						$agrupacion = 'grupo'.$an.' agrupacion '; 
						$agrupacionres = 'grupo'.$an.' agrupacion'; 
					}else{
						$agrupacion = 'agrupacion'; 
						$agrupacionres = 'agrupacion'; 
					}
				}else{ 
					$agrupacion = ''; 
					$agrupacionres = ''; 
				}

				$totales = 0;
				
				//Si el siguiente es vacio o diferente comprobamso si el anterior era igual
				if($array_completo[$key-1]["campana"] == $filas["campana"] ){
					if($array_completo[$key]["campana"] != $filas["campana"] ){
						$totales=1;
					}
				}
				
				$an_nombre = $filas["campana"];
				
				$tabla .= "<tr class=\"".$tablaclass."\">";	
				$tabla .= "<td class=\"".$agrupacion."primera\">".$filas["campana"]." <span class=\"porcDT\">(".$filas["id_campana"].")</span></td>";
				$tabla .= "<td class=\"".$agrupacionres."\"><span class=\"izquierda\">".$filas["medium"]."</span></td>";
				$tabla .= "<td class=\"".$agrupacionres."\"><span class=\"izquierda\">".$filas["source"]."</span></td>";
				$tabla .= "<td class=\"".$agrupacionres."\">".$filas["sesiones_f"]."</td>";
				$tabla .= "<td class=\"".$agrupacionres."\">".$filas["usuarios_f"]."</td>";
				$tabla .= "<td class=\"".$agrupacionres."\">".$filas["rebote_f"]."</td>";
				$tabla .= "<td class=\"".$agrupacionres."\">".$filas["pagsesion_f"]."</td>";
				
				if($strTipo=="campanas"){
					//$tabla .= "<td class='".$agrupacion."'>".$filas["convpinteraccion_f"]."</td>";
					//$tabla .= "<td class='".$agrupacion."'>".$filas["convasistidas_f"]."</td>";
					//$tabla .= "<td class='".$agrupacion."'>".$filas["convuinteraccion_f"]."</td>";
					$tabla .= "<td class=\"".$agrupacionres."\">".$filas["convtotal"]."</td>";
					$tabla .= "<td class=\"".$agrupacionres."\">0</td>";
					//$tabla .= "<td class='".$agrupacion."'>".$filas["goalcompletionsall_f"]."</td>";
				}else{
					//$tabla .= "<td class='".$agrupacion."'>".$filas["convpinteraccion_f"]." (".$filas["firstInteractionValue"].")</td>";
					//$tabla .= "<td class='".$agrupacion."'>".$filas["convasistidas_f"]." (".$filas["assistedValue"].")</td>";
					//$tabla .= "<td class='".$agrupacion."'>".$filas["convuinteraccion_f"]." (".$filas["lastInteractionValue"].")</td>";
					$tabla .= "<td class=\"".$agrupacionres."\">".$filas["convtotal"]."</td>";
					$tabla .= "<td class=\"".$agrupacionres."\">".$filas["transtotal"]."€</td>";
					//$tabla .= "<td class='".$agrupacion."'>".$filas["transactions_f"]."<br>Neto:".$filas["transactionRevenue_f"]."<br>Envío:".$filas["transactionShipping_f"]."<br>Iva:".$filas["transactionTax_f"]."</td>";
				}
				
				if($resumenif == "no"){
					$tabla .= "<td class=\"".$agrupacionres."\"><a onClick='verCampana(\"".$filas["campana"]."\",\"".$filas["medium"]."\",\"".$filas["source"]."\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a><a onClick='editarCampana(".$idCampana.", \"".$idPropiedad."\")' class='btn_vercampana'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a></td>";
				}else{
					$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=".$filas["medium"]."&fuente=".$filas["source"]."&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;
					$tabla .= "<td class=\"".$agrupacionres."\"><a class=\"btn_vercampana\" href='".$href."'><img class='imagensearch' src='../admin/Envios/estructura_email/images/search.png'/></a></td>";
				}
				$tabla .= "</tr>";

				$campana_anterior = $filas["campana"];
				
				//Crearmos totales
				if($totales == 1){
					//A la hora de pintar el total de una campaña traemos los totales de ella
					$tot_campana = totalCampanas($array_completo[$key-1]["campana"],$strProyectoAsociado,$idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro,$trans);
					$tot_sesiones 			  = $tot_campana["tot_sesiones"];
					$tot_rebote 			  = $tot_campana["tot_rebote"];
					$tot_pagsesion  		  = $tot_campana["tot_pagsesion"];
					$tot_usuarios   		  = $tot_campana["tot_usuarios"];
					$tot_ga         		  = $tot_campana["tot_usuarios"];
					$tot_trans        		  = $tot_campana["tot_trans"];
					$tot_transactionRevenue   = $tot_campana["tot_transactionRevenue"];
					$tot_transactionShipping  = $tot_campana["tot_transactionShipping"];
					$tot_transactionTax       = $tot_campana["tot_transactionTax"];
					if($resumenif != ""){$mostrartotal = $an_nombre; }else{ $mostrartotal = 'Totales'; }
					$tabla .= "<tr name=\"grupo".$an."\" class=\"filadesplegable ".$tablaclass."\" id=\"".$an_nombre."\" >";
					$tabla .= "<td class=\"total tittot primera\"><span class=\"subtotal_tit\">".$mostrartotal."</span></td>";
					$tabla .= "<td class=\"total\"></td>";
					$tabla .= "<td class=\"total\"></td>";
					$tabla .= "<td class=\"total\">".$tot_sesiones."</td>";
					$tabla .= "<td class=\"total\">".$tot_usuarios."</td>";
					$tabla .= "<td class=\"total\">".$tot_rebote."</td>";
					$tabla .= "<td class=\"total\">".$tot_pagsesion."</td>";
					
					if($strTipo=="campanas"){
						//$tabla .= "<td class='total'>".number_format($tot_convpinteraccion,0,',','.')."</td>";
						//$tabla .= "<td class='total'>".number_format($tot_convasistidas,0,',','.')."</td>";
						//$tabla .= "<td class='total'>".number_format($tot_convuinteraccion,0,',','.')."</td>";
						$tabla .= "<td class=\"total\">".number_format($tot_convuinteraccion,0,',','.')."</td>";
						$tabla .= "<td class=\"total\">0</td>";
						//$tabla .= "<td class='total'>".number_format($tot_ga,0,',','.')."</td>";
					}else{
						//$tabla .= "<td class='total'>".number_format($tot_convpinteraccion,0,',','.')." (".$tot_firstInteractionValue.")</td>";
						//$tabla .= "<td class='total'>".number_format($tot_convasistidas,0,',','.')." (".$tot_assistedValue.")</td>";
						//$tabla .= "<td class='total'>".number_format($tot_convuinteraccion,0,',','.')." (".$tot_lastInteractionValue.")</td>";
						$tabla .= "<td class=\"total\">".number_format($tot_convtotal,0,',','.')."</td>";
						$tabla .= "<td class=\"total\">".number_format($tot_transtotal,0,',','.')."€</td>";
						//$tabla .= "<td class='total'>".number_format($tot_trans,0,',','.')."<br>Neto:".$tot_transactionRevenue."<br>Envío:".$tot_transactionShipping."<br>Iva:".$tot_transactionTax."</td>";
					}
					if($resumenif == "no"){
						$tabla .= "<td class=\"total\"><a onClick='verCampana(\"".$filas["campana"]."\",\"\",\"\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a></td>";
					}else{
						$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;;
						$tabla .= "<td class=\"total\"><a class='btn_vercampana' href='".$href."'><img class='imagensearch' src='../admin/Envios/estructura_email/images/search.png'/></a></td>";
					}

					$tabla .= "</tr>";
					$tot_cont =0;

					//En totales le
					$tot_sesiones 		       = "";
					$tot_rebote 		       = "";
					$tot_pagsesion             = "";
					$tot_usuarios              = "";
					$tot_convasistidas         = "";
					$tot_convpinteraccion      = "";
					$tot_convuinteraccion      = "";
					$tot_convtotal        	   = "";
					$tot_transtotal        	   = "";
					$tot_assistedValue	  	   = "";
					$tot_firstInteractionValue = "";
					$tot_lastInteractionValue  = "";
					$tot_ga 			       = "";
					$tot_trans  			   = "";
					$tot_transactionRevenue    = "";
					$tot_transactionShipping   = "";
					$tot_transactionTax    	   = "";

				}

				if($array_completo[$key+1]["campana"] == $filas["campana"]){ 
					//Si es que si empezamos a sumar los totales
					$tot_cont ++; 
					$tot_convasistidas    += $filas["convasistidas"];
					$tot_convpinteraccion += $filas["convpinteraccion"];
					$tot_convuinteraccion += $filas["convuinteraccion"];
					$tot_convtotal        += $filas["convtotal"];
					$tot_transtotal		  += $filas["transtotal"];
					$tot_assistedValue	  	  	+= $filas["assistedValue"];
					$tot_firstInteractionValue  += $filas["firstInteractionValue"];
					$tot_lastInteractionValue   += $filas["lastInteractionValue"];
				}

			}else{

				//Si se esta agrupando comprobamos si hay que medium anterior igual

				if($array_completo[$key-1]["medium"] != $filas["medium"]){ $agrubacionb = "agrupacionb"; $mediumm = $filas["medium"]; }else{ $agrubacionb = ""; $mediumm=""; }
				$agrupacion = 'grupo'.$an.' agrupacion ';
				if($resumenif == "no"){
					$agrucompleto = $agrupacion.$agrubacionb;
				}else{
					$agrucompleto = 'agrupacion';
				}
				
				$agr = 1;
				$tabla .= "<tr class=\"".$tablaclass."\" >";
				$tabla .= "<td class=\"".$agrucompleto." primera\"></td>";
				$tabla .= "<td class=\"".$agrucompleto."\"><span class=\"izquierda\">".$mediumm."</span></td>";
				$tabla .= "<td class=\"".$agrucompleto."\"><span class=\"izquierda\">".$filas["source"]."</span></td>";
				$tabla .= "<td class=\"".$agrucompleto."\">".$filas["sesiones_f"]."</td>";
				$tabla .= "<td class=\"".$agrucompleto."\">".$filas["usuarios_f"]."</td>";
				$tabla .= "<td class=\"".$agrucompleto."\">".$filas["rebote_f"]."</td>";
				$tabla .= "<td class=\"".$agrucompleto."\">".$filas["pagsesion_f"]."</td>";
				
				if($strTipo=="campanas"){
					//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'>".$filas["convpinteraccion_f"]."</td>";
					//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'>".$filas["convasistidas_f"]."</td>";
					//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'>".$filas["convuinteraccion_f"]."</td>";	
					$tabla .= "<td class=\"".$agrucompleto."\">".$filas["convtotal"]."</td>";	
					$tabla .= "<td class=\"".$agrucompleto."\">0</td>";
					//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'>".$filas["goalcompletionsall_f"]."</td>";
				}else{
					//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'>".$filas["convpinteraccion_f"]." (".$filas["firstInteractionValue"].")</td>";
					//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'>".$filas["convasistidas_f"]." (".$filas["assistedValue"].")</td>";
					//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'>".$filas["convuinteraccion_f"]." (".$filas["lastInteractionValue"].")</td>";
					$tabla .= "<td class=\"".$agrucompleto."\">".$filas["convtotal"]."</td>";
					$tabla .= "<td class=\"".$agrucompleto."\">".$filas["transtotal"]."€</td>";
					//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'>".$filas["transactions_f"]."<br>Neto:".$filas["transactionRevenue_f"]."<br>Envío:".$filas["transactionShipping_f"]."<br>Iva:".$filas["transactionTax_f"]."</td>";
				}
						
				if($resumenif == "no"){
					$tabla .= "<td class=\"".$agrucompleto."\"><a onClick='verCampana(\"".$filas["campana"]."\",\"".$filas["medium"]."\",\"".$filas["source"]."\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a><a onClick='editarCampana(".$idCampana.", \"".$idPropiedad."\")' class='btn_vercampana'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a></td>";
				}else{
					$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=".$filas["medium"]."&fuente=".$filas["source"]."&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;;
					$tabla .= "<td class=\"".$agrucompleto."\"><a class='btn_vercampana' href='".$href."'><img class='imagensearch' src='../admin/Envios/estructura_email/images/search.png'/></a></td>";
				}
				//$tabla .= "<td class='".$agrupacion." ".$agurbacionb."'><a onClick='verCampana(\"".$filas["campana"]."\",\"".$filas["medium"]."\",\"".$filas["source"]."\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a><a onClick='editarCampana(".$idCampana.", \"".$idPropiedad."\")' class='btn_vercampana'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a></td>";
				$tabla .= "</tr>";
				$campana_anterior = $filas["campana"];
				$tot_cont ++; 
				$tot_convasistidas    += $filas["convasistidas"];
				$tot_convpinteraccion += $filas["convpinteraccion"];
				$tot_convuinteraccion += $filas["convuinteraccion"];
				$tot_convtotal        += $filas["convtotal"];
				$tot_transtotal		  += $filas["transtotal"];
				$tot_assistedValue	  	  	+= $filas["assistedValue"];
				$tot_firstInteractionValue  += $filas["firstInteractionValue"];
				$tot_lastInteractionValue   += $filas["lastInteractionValue"];

	


			}
			
				

		}

		//al terminar comprobamos tmb si hay totales
		if($array_completo[$key-1]["campana"] == $filas["campana"] ){

			//A la hora de pintar el total de una campaña traemos los totales de ella
			$tot_campana = totalCampanas($array_completo[$key-1]["campana"],$strProyectoAsociado,$idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro,$trans);
			$tot_sesiones 		  	  = $tot_campana["tot_sesiones"];
			$tot_rebote 		  	  = $tot_campana["tot_rebote"];
			$tot_pagsesion        	  = $tot_campana["tot_pagsesion"];
			$tot_usuarios         	  = $tot_campana["tot_usuarios"];
			$tot_ga        		      = $tot_campana["tot_ga"];
			$tot_trans        		  = $tot_campana["tot_trans"];
			$tot_transactionRevenue   = $tot_campana["tot_transactionRevenue"];
			$tot_transactionShipping  = $tot_campana["tot_transactionShipping"];
			$tot_transactionTax       = $tot_campana["tot_transactionTax"];

			if($resumenif != ""){$mostrartotal = $an_nombre; }else{ $mostrartotal = 'Totales'; }
			$tabla .= "<tr name=\"grupo".$an."\" class=\"filadesplegable ".$tablaclass."\" id=\"".$an_nombre."\" >";
			$tabla .= "<td class=\"total tittot primera\"><span class=\"subtotal_tit\">".$mostrartotal."</span></td>";
			$tabla .= "<td class=\"total\"></td>";
			$tabla .= "<td class=\"total\"></td>";
			$tabla .= "<td class=\"total\">".$tot_sesiones."</td>";
			$tabla .= "<td class=\"total\">".$tot_usuarios."</td>";
			$tabla .= "<td class=\"total\">".$tot_rebote."</td>";
			$tabla .= "<td class=\"total\">".$tot_pagsesion."</td>";
			
			if($strTipo=="campanas"){
				//$tabla .= "<td class='total'>".number_format($tot_convpinteraccion,0,',','.')."</td>";
				//$tabla .= "<td class='total'>".number_format($tot_convasistidas,0,',','.')."</td>";
				//$tabla .= "<td class='total'>".number_format($tot_convuinteraccion,0,',','.')."</td>";
				$tabla .= "<td class=\"total\">".number_format($tot_convuinteraccion,0,',','.')."</td>";
				$tabla .= "<td class=\"total\">0</td>";
				//$tabla .= "<td class='total'>".number_format($tot_ga,0,',','.')."</td>";
			}else{
				//$tabla .= "<td class='total'>".number_format($tot_convpinteraccion,0,',','.')." (".$tot_firstInteractionValue.")</td>";
				//$tabla .= "<td class='total'>".number_format($tot_convasistidas,0,',','.')." (".$tot_assistedValue.")</td>";
				//$tabla .= "<td class='total'>".number_format($tot_convuinteraccion,0,',','.')." (".$tot_lastInteractionValue.")</td>";
				$tabla .= "<td class=\"total\">".number_format($tot_convtotal,0,',','.')."</td>";
				$tabla .= "<td class=\"total\">".number_format($tot_transtotal,0,',','.')."€</td>";
				//$tabla .= "<td class='total'>".number_format($tot_trans,0,',','.')."<br>Neto:".$tot_transactionRevenue."<br>Envío:".$tot_transactionShipping."<br>Iva:".$tot_transactionTax."</td>";
			}
			if($resumenif == "no"){
				$tabla .= "<td class=\"total\"><a onClick='verCampana(\"".$filas["campana"]."\",\"\",\"\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a></td>";
			}else{
				$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;;
				$tabla .= "<td class=\"total\"><a class='btn_vercampana' href='".$href."'><img class='imagensearch' src='../admin/Envios/estructura_email/images/search.png'/></a></td>";
			}
			$tabla .= "</tr>";
			$an++;
		}


		/**********************************************TRAYENDO DE LA BASE DE DATOS************************************************/
		$tabla .= campanas_no_detectadas($idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro,$where,$resumenif);
		/*************************************************************************************************************************/

	}else{
		//Si no hay campañas activas primero comprobamos si existe en la db
		$tablam = campanas_no_detectadas($idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro,$where,$resumenif);
		if(!empty($tablam)){
			$tabla .= $tablam;
		}else{
			$tabla .= "<tr>";
				$tabla .= "<td colspan='14'>No hay campañas activas.</td>";
			$tabla .= "</tr>";
		}
	}

		

		//SEO V2************************************************************************************************************
		$strDimensions='ga:campaign,ga:medium,ga:source,ga:adwordsCampaignID';
		$metricas  = 'ga:sessions,ga:users,ga:bounceRate,ga:pageviewsPerSession,ga:goalCompletionsAll,ga:transactions,ga:transactionRevenue,ga:transactionShipping,ga:transactionTax';
		$strDimensions='';

		$dataFil = FiltrosSocial(2,'ga');
		$filtro_and = "ga:medium==organic;".$dataFil;
		$filtro_and = substr($filtro_and, 0, -1);

	    //Creación objeto, parametrización -------------------------------------------------------
	    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
	    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	    $obj_Datos_Api -> metrics              = $metricas;  
	    $obj_Datos_Api -> optParams       	   = array(
		                                            'filters' => $filtro_and,
													'start-index' => 1,
		                                            'max-results' => 1);  
	   	
	    $obj_Datos_Api -> Construccion();

	    //for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
	    	$tabla .= "<tr class='filaSeo'>";
	    		$tabla .= "<td colspan='3' class=\"primera\">SEO</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("sessions")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("users")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("bounceRate")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("pageviewsPerSession")."</td>";
				

				$metricasMcf = 'mcf:firstInteractionConversions,mcf:assistedConversions,mcf:lastInteractionConversions,mcf:assistedValue,mcf:firstInteractionValue,mcf:lastInteractionValue';

				$dataFil = FiltrosSocial(2,'mcf');
				$filtro_mcf = "mcf:conversionType==".$trans.";mcf:medium==organic;".$dataFil;
				$filtro_mcf = substr($filtro_mcf, 0, -1);

				//$filtro_mcf = "mcf:medium==organic";
				//API MCF
				$obj_Datos_Api_mcf = new Datos_Api_Mcf(); // Instanciamos la clase Datos_Api_Mcf
			    $obj_Datos_Api_mcf -> strProyectoAsociado  = $strProyectoAsociado;
			    $obj_Datos_Api_mcf -> idVista              ='ga:'. $idVistaAnalytics;
			    $obj_Datos_Api_mcf -> startdate            = $datFechaInicioFiltro;
			    $obj_Datos_Api_mcf -> enddate              = $datFechaFinFiltro;
			    $obj_Datos_Api_mcf -> metrics              = $metricasMcf;
			    $obj_Datos_Api_mcf -> optParams            = array(
				                                            'filters' => $filtro_mcf,
															'start-index' => 1,
				                                            'max-results' => 1000
				                                            );  
			    $obj_Datos_Api_mcf -> Construccion();

			    if($strTipo=="campanas"){
			    	//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(1)."</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(2)."</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(3)."</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(2)+$obj_Datos_Api_mcf->Total(3),0,',','.')."</td>";
					$tabla .= "<td>0</td>";
					//$tabla .= "<td>".$obj_Datos_Api->TotalF("goalCompletionsAll")."</td>";
				}else{
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(1)."(".$obj_Datos_Api_mcf->TotalF(5).")</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(2)."(".$obj_Datos_Api_mcf->TotalF(4).")</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(3)."(".$obj_Datos_Api_mcf->TotalF(6).")</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(2)+$obj_Datos_Api_mcf->Total(3),0,',','.')."</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(4)+$obj_Datos_Api_mcf->Total(6),0,',','.')."€</td>";
					//$tabla .= "<td>".$obj_Datos_Api->TotalF("transactions")."<br>Neto:".$obj_Datos_Api->TotalF("transactionRevenue")."<br>Envío:".$obj_Datos_Api->TotalF("transactionShipping")."<br>Iva:".$obj_Datos_Api->TotalF("transactionTax")."</td>";
				}
				
			if($resumenif == "no"){
				$tabla .= "<td><a onClick='verFiltro(\"seo\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a></td>";
			}else{
				$href = "../public/comprobando.php?campana=seo&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;
				$tabla .= "<td><a class='btn_vercampana' href='".$href."'><img class='imagensearch' src='../admin/Envios/estructura_email/images/search.png'/></a></td>";
			}
			$tabla .= "</tr>";
	   // }

	

	//ENLACES V2************************************************************************************************************
		$strDimensions='ga:campaign,ga:medium,ga:source,ga:adwordsCampaignID';
		$metricas  = 'ga:sessions,ga:users,ga:bounceRate,ga:pageviewsPerSession,ga:goalCompletionsAll,ga:transactions,ga:transactionRevenue,ga:transactionShipping,ga:transactionTax';
		$strDimensions='';

		$dataFil = FiltrosSocial(2,'ga');
		$filtro_and = "ga:medium==referral;".$dataFil;
		$filtro_and = substr($filtro_and, 0, -1);

	    //Creación objeto, parametrización -------------------------------------------------------
	    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
	    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	    $obj_Datos_Api -> metrics              = $metricas;  
	    $obj_Datos_Api -> optParams       	   = array(
		                                            'filters' => $filtro_and,
													'start-index' => 1,
		                                            'max-results' => 1);  
	   	
	    $obj_Datos_Api -> Construccion();

	    //for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
	    	$tabla .= "<tr class='filaEnlaces'>";
	    		$tabla .= "<td colspan='3' class=\"primera\">Enlaces</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("sessions")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("users")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("bounceRate")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("pageviewsPerSession")."</td>";
				

				$metricasMcf = 'mcf:firstInteractionConversions,mcf:assistedConversions,mcf:lastInteractionConversions,mcf:assistedValue,mcf:firstInteractionValue,mcf:lastInteractionValue';

				$dataFil = FiltrosSocial(2,'mcf');
				$filtro_mcf = "mcf:conversionType==".$trans.";mcf:medium==referral;".$dataFil;
				$filtro_mcf = substr($filtro_mcf, 0, -1);

				//$filtro_mcf = "mcf:medium==referral";
				//API MCF
				$obj_Datos_Api_mcf = new Datos_Api_Mcf(); // Instanciamos la clase Datos_Api_Mcf
			    $obj_Datos_Api_mcf -> strProyectoAsociado  = $strProyectoAsociado;
			    $obj_Datos_Api_mcf -> idVista              ='ga:'. $idVistaAnalytics;
			    $obj_Datos_Api_mcf -> startdate            = $datFechaInicioFiltro;
			    $obj_Datos_Api_mcf -> enddate              = $datFechaFinFiltro;
			    $obj_Datos_Api_mcf -> metrics              = $metricasMcf;
			    $obj_Datos_Api_mcf -> optParams            = array(
				                                            'filters' => $filtro_mcf,
															'start-index' => 1,
				                                            'max-results' => 1000
				                                            );  
			    $obj_Datos_Api_mcf -> Construccion();

				if($strTipo=="campanas"){
			    	//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(1)."</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(2)."</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(3)."</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(2)+$obj_Datos_Api_mcf->Total(3),0,',','.')."</td>";
					$tabla .= "<td>0</td>";
					//$tabla .= "<td>".$obj_Datos_Api->TotalF("goalCompletionsAll")."</td>";
				}else{
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(1)."(".$obj_Datos_Api_mcf->TotalF(5).")</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(2)."(".$obj_Datos_Api_mcf->TotalF(4).")</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(3)."(".$obj_Datos_Api_mcf->TotalF(6).")</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(2)+$obj_Datos_Api_mcf->Total(3),0,',','.')."</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(4)+$obj_Datos_Api_mcf->Total(6),0,',','.')."€</td>";
					//$tabla .= "<td>".$obj_Datos_Api->TotalF("transactions")."<br>Neto:".$obj_Datos_Api->TotalF("transactionRevenue")."<br>Envío:".$obj_Datos_Api->TotalF("transactionShipping")."<br>Iva:".$obj_Datos_Api->TotalF("transactionTax")."</td>";
				}	

				if($resumenif == "no"){
					$tabla .= "<td><a onClick='verFiltro(\"enlaces\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a></td>";
				}else{
					$href = "../public/comprobando.php?campana=enlaces&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;
					$tabla .= "<td><a class='btn_vercampana' href='".$href."'><img class='imagensearch' src='../admin/Envios/estructura_email/images/search.png'/></a></td>";
				}
			$tabla .= "</tr>";
	   // }

	

		//Social V2************************************************************************************************************
		$strDimensions='ga:campaign,ga:medium,ga:source,ga:adwordsCampaignID';
		$metricas  = 'ga:sessions,ga:users,ga:bounceRate,ga:pageviewsPerSession,ga:goalCompletionsAll,ga:transactions,ga:transactionRevenue,ga:transactionShipping,ga:transactionTax';
		$strDimensions='';

		$dataFil = FiltrosSocial(1,'ga');
		$filtro_and = "ga:campaign==(not set);".$dataFil;
		$filtro_and = substr($filtro_and, 0, -1);
		
		//$filtro_and = "ga:SocialNetwork!=(not set);ga:campaign==(not set)";

	    //Creación objeto, parametrización -------------------------------------------------------
	    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
	    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	    $obj_Datos_Api -> metrics              = $metricas;  
	    $obj_Datos_Api -> optParams       	   = array(
		                                            'filters' => $filtro_and,
													'start-index' => 1,
		                                            'max-results' => 1);  
	   	
	    $obj_Datos_Api -> Construccion();

	    //for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
	    	$tabla .= "<tr class='filaSocial' >";
	    		$tabla .= "<td colspan='3' class=\"primera\">Social</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("sessions")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("users")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("bounceRate")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("pageviewsPerSession")."</td>";
				

				$metricasMcf = 'mcf:firstInteractionConversions,mcf:assistedConversions,mcf:lastInteractionConversions,mcf:assistedValue,mcf:firstInteractionValue,mcf:lastInteractionValue';

				$dataFil = FiltrosSocial(1,'mcf');
				$filtro_mcf = "mcf:conversionType==".$trans.";mcf:campaignName==(not set);".$dataFil;
				$filtro_mcf = substr($filtro_mcf, 0, -1);

				//$filtro_mcf = "mcf:campaignName==(not set)";
				//API MCF
				$obj_Datos_Api_mcf = new Datos_Api_Mcf(); // Instanciamos la clase Datos_Api_Mcf
			    $obj_Datos_Api_mcf -> strProyectoAsociado  = $strProyectoAsociado;
			    $obj_Datos_Api_mcf -> idVista              ='ga:'. $idVistaAnalytics;
			    $obj_Datos_Api_mcf -> startdate            = $datFechaInicioFiltro;
			    $obj_Datos_Api_mcf -> enddate              = $datFechaFinFiltro;
			    $obj_Datos_Api_mcf -> metrics              = $metricasMcf;
			    $obj_Datos_Api_mcf -> optParams            = array(
				                                            'filters' => $filtro_mcf,
															'start-index' => 1,
				                                            'max-results' => 1000
				                                            );  
			    $obj_Datos_Api_mcf -> Construccion();

				if($strTipo=="campanas"){
			    	//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(1)."</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(2)."</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(3)."</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(2)+$obj_Datos_Api_mcf->Total(3),0,',','.')."</td>";
					$tabla .= "<td>0</td>";
					//$tabla .= "<td>".$obj_Datos_Api->TotalF("goalCompletionsAll")."</td>";
				}else{
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(1)."(".$obj_Datos_Api_mcf->TotalF(5).")</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(2)."(".$obj_Datos_Api_mcf->TotalF(4).")</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(3)."(".$obj_Datos_Api_mcf->TotalF(6).")</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(2)+$obj_Datos_Api_mcf->Total(3),0,',','.')."</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(4)+$obj_Datos_Api_mcf->Total(6),0,',','.')."€</td>";
					//$tabla .= "<td>".$obj_Datos_Api->TotalF("transactions")."<br>Neto:".$obj_Datos_Api->TotalF("transactionRevenue")."<br>Envío:".$obj_Datos_Api->TotalF("transactionShipping")."<br>Iva:".$obj_Datos_Api->TotalF("transactionTax")."</td>";
				}

				if($resumenif == "no"){
					$tabla .= "<td><a onClick='verFiltro(\"social\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a></td>";
				}else{
					$href = "../public/comprobando.php?campana=social&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;
					$tabla .= "<td><a class='btn_vercampana' href='".$href."'><img class='imagensearch' src='../admin/Envios/estructura_email/images/search.png'/></a></td>";
				}
			$tabla .= "</tr>";
	   // }

		//TOTAL************************************************************************************************************
		$strDimensions='ga:campaign,ga:medium,ga:source,ga:adwordsCampaignID';
		$metricas  = 'ga:sessions,ga:users,ga:bounceRate,ga:pageviewsPerSession,ga:goalCompletionsAll,ga:transactions,ga:transactionRevenue,ga:transactionShipping,ga:transactionTax';
		$strDimensions='';

	    //Creación objeto, parametrización -------------------------------------------------------
	    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
	    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	    $obj_Datos_Api -> metrics              = $metricas;  
	    $obj_Datos_Api -> optParams       	   = array(
													'start-index' => 1,
		                                            'max-results' => 1);  
	   	
	    $obj_Datos_Api -> Construccion();

	    //for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
	    	$tabla .= "<tr class='filaTotal'>";
	    		$tabla .= "<td colspan='3' class=\"primera\">Total</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("sessions")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("users")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("bounceRate")."</td>";
				$tabla .= "<td>".$obj_Datos_Api->TotalF("pageviewsPerSession")."</td>";
				

				$metricasMcf = 'mcf:firstInteractionConversions,mcf:assistedConversions,mcf:lastInteractionConversions,mcf:assistedValue,mcf:firstInteractionValue,mcf:lastInteractionValue';
				$filtro_mcf = "mcf:conversionType==".$trans;
				//API MCF
				$obj_Datos_Api_mcf = new Datos_Api_Mcf(); // Instanciamos la clase Datos_Api_Mcf
			    $obj_Datos_Api_mcf -> strProyectoAsociado  = $strProyectoAsociado;
			    $obj_Datos_Api_mcf -> idVista              ='ga:'. $idVistaAnalytics;
			    $obj_Datos_Api_mcf -> startdate            = $datFechaInicioFiltro;
			    $obj_Datos_Api_mcf -> enddate              = $datFechaFinFiltro;
			    $obj_Datos_Api_mcf -> metrics              = $metricasMcf;
			    $obj_Datos_Api_mcf -> optParams            = array(
			    											'filters' => $filtro_mcf,
															'start-index' => 1,
				                                            'max-results' => 1000
				                                            );  
			    $obj_Datos_Api_mcf -> Construccion();

				if($strTipo=="campanas"){
			    	//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(1)."</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(2)."</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(3)."</td>";
					$tabla .= "<td>".number_format($obj_Datos_Api_mcf->Total(3),0,',','.')."</td>";
					$tabla .= "<td>0</td>";
					//$tabla .= "<td>".$obj_Datos_Api->TotalF("goalCompletionsAll")."</td>";
				}else{
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(1)."(".$obj_Datos_Api_mcf->TotalF(5).")</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(2)."(".$obj_Datos_Api_mcf->TotalF(4).")</td>";
					//$tabla .= "<td>".$obj_Datos_Api_mcf->TotalF(3)."(".$obj_Datos_Api_mcf->TotalF(6).")</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(2)+$obj_Datos_Api_mcf->Total(3),0,',','.')."</td>";
					$tabla .= "<td>".number_format( $obj_Datos_Api_mcf->Total(4)+$obj_Datos_Api_mcf->Total(6),0,',','.')."€</td>";
					//$tabla .= "<td>".$obj_Datos_Api->TotalF("transactions")."<br>Neto:".$obj_Datos_Api->TotalF("transactionRevenue")."<br>Envío:".$obj_Datos_Api->TotalF("transactionShipping")."<br>Iva:".$obj_Datos_Api->TotalF("transactionTax")."</td>";
				}

				if($resumenif == "no"){
					$tabla .= "<td><a onClick='verFiltro(\"total\")' class='btn_vercampana'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></a></td>";
				}else{
					$href = "../public/comprobando.php?campana=total&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$strFechaInicioFiltroAnt."&fechafin_ant=".$strFechaFinFiltroAnt."&idVista=".$idVistaAnalytics;
					$tabla .= "<td></td>";
				}
			$tabla .= "</tr>";
	   // }





	return $tabla;
}  

function campanas_no_detectadas($idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro,$where,$resumenif){

	$tabla ="";
	$propiedad = propiedadVista($idVistaAnalytics);
	foreach ($propiedad["elementos"] as $key => $prop) {
		$primera_propiedad = $prop["idpropiedad"];
	}

	$devuelve = array();
	
	$sql = "SELECT [LOCC_CAMPAING],[LOCC_MEDIUM],[LOCC_SOURCE]";
	$sql .= "FROM [DMI_DMINTEGRA].[dbo].[LOC_CAMPANAS]";
	$sql .= "WHERE [LOCC_ANALYTICS_ID_PROPIEDAD]= '".$primera_propiedad."'";
	//-- Condición de fecha inicio de campaña
	$sql .= "AND (LOCC_ACTIVARSEGUIMIENTO=1 AND (LOCC_FECHA_ENVIOS_INICIO IS NULL OR (LOCC_FECHA_ENVIOS_INICIO <= '".$datFechaFinFiltro."')) )";
	//-- Condición de fecha inicio de campaña
	$sql .= "AND (LOCC_ACTIVARSEGUIMIENTO=1 AND (LOCC_FECHA_ENVIOS_FIN IS NULL OR (LOCC_FECHA_ENVIOS_FIN >= '".$datFechaInicioFiltro."' )) )";
	//-- Condición de que no sea histórica la campaña
	$sql .= "AND (LOCC_HISTORICO=0 OR LOCC_HISTORICO IS NULL)";
	//-- Quitamos aquellas campañas detectadas en Analytics
	$sql .= "AND ( 1=1 ".$where." )";
	$sql .= "ORDER BY LOCC_CAMPAING DESC ,LOCC_MEDIUM DESC ,LOCC_SOURCE DESC";
	$c = Nuevo_PDO();
	$stmt = $c->prepare($sql);
	$result = $stmt->execute();
	if ($result && $stmt->rowCount() != 0){
	  $count = 0;
	  //echo "Filas: ".$stmt->rowCount()."<br>";
	  while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
	    
	    $devuelve[$count]["campana"] = $fila[0];
	    $devuelve[$count]["medium"]   = $fila[1];
	    $devuelve[$count]["source"]  = $fila[2];
	    $count++;
	  }
	}else{

	 }
	
	$count 			  = 0;
	$campana_anterior = "";
	$an 			  = 0;
	$an_nombre		  = "";
	$tot_cont 		  = 0; 
	$agr 			  = 0;

	if(!empty($devuelve)){

		foreach ($devuelve as $key => $filas) {
			
			
	

				if($campana_anterior != $filas["campana"]){

					if($agr == 1){
						//A la hora de pintar el total de una campaña traemos los totales de ella
						if($resumenif != "si"){$mostrartotal = $an_nombre; }else{ $mostrartotal = 'Totales'; }
						$tabla .= '<tr name="grupo'.$an.'" class="filadesplegable nodetectadas" id="'.$an_nombre.'">
				   		<td class="total tittot primera"><span class="subtotal_tit">'.$mostrartotal.'</span></td>
				   		<td class="total"><span class="izquierda"></span></td>
				   		<td class="total"><span class="izquierda"></span></td>
				   		<td class="total">0</td>
				   		<td class="total">0</td>
				   		<td class="total">0%</td>
				   		<td class="total">0</td>
				   		<td class="total">0</td>
				   		<td class="total">0</td>';
				   		if($resumenif == "no"){
				   			$tabla .= '<td class="total"><a onclick="verCampana(\''.$filas["campana"].'\',\'\',\'\')" class="btn_vercampana"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a><a onclick="editarCampana(\'Kukimba Google Shoping\', \'UA-27910505-5\')" class="btn_vercampana"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
						}else{
							$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$datFechaInicioFiltro."&fechafin_ant=".$datFechaFinFiltro."&idVista=".$idVistaAnalytics;
				   			$tabla .= '<td class="total"><a href="'.$href.'" class="btn_vercampana"><img class="imagensearch" src="../admin/Envios/estructura_email/images/search.png"/></a></td>';
						}
				   		$tabla .= '</tr>';
				   		$tot_cont =0;
						$an++;
					}

					
	
					if($devuelve[$key+1]["campana"] == $filas["campana"]){ 
						$agrupacion = 'agrupacion grupo'.$an.' ';
						$agrupacionres = 'agrupacion grupo'.$an;  
					}else{ 
						$agrupacion = ''; 
						$agrupacionres = '';
					}

					$totales = 0;
				
					//Si el siguiente es vacio o diferente comprobamso si el anterior era igual
					if($devuelve[$key-1]["campana"] == $filas["campana"] ){
						if($devuelve[$key]["campana"] != $filas["campana"] ){
							$totales=1;
						}
					}
					

					$an_nombre = $filas["campana"];

					$tabla .= '<tr class="nodetectadas">
						<td class="'.$agrupacion.'primera">'.$filas["campana"].'</td>
						<td class="'.$agrupacionres.'"><span class="izquierda">'.$filas["medium"].'</span></td>
						<td class="'.$agrupacionres.'"><span class="izquierda">'.$filas["source"].'</span></td>
						<td class="'.$agrupacionres.'">0</td>
						<td class="'.$agrupacionres.'">0</td>
						<td class="'.$agrupacionres.'">0%</td>
						<td class="'.$agrupacionres.'">0</td>
						<td class="'.$agrupacionres.'">0</td>
						<td class="'.$agrupacionres.'">0</td>';
						if($resumenif == "no"){
				   			$tabla .= '<td class="'.$agrupacionres.'"><a onclick="verCampana(\''.$filas["campana"].'\',\''.$filas["medium"].'\',\''.$filas["source"].'\')" class="btn_vercampana"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a><a onclick="editarCampana(\'Kukimba Google Shoping\', \'UA-27910505-5\')" class="btn_vercampana"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
						}else{
							$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=".$filas["medium"]."&fuente=".$filas["source"]."&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$datFechaInicioFiltro."&fechafin_ant=".$datFechaFinFiltro."&idVista=".$idVistaAnalytics;
				   			$tabla .= '<td class="'.$agrupacionres.'"><a href="'.$href.'" class="btn_vercampana"><img class="imagensearch" src="../admin/Envios/estructura_email/images/search.png"/></a></td>';
						}
				   		$tabla .= '</tr>';


					
			
					$campana_anterior = $filas["campana"];

					//Crearmos totales
					if($totales == 1){
						//A la hora de pintar el total de una campaña traemos los totales de ella
						if($resumenif != "si"){$mostrartotal = $an_nombre; }else{ $mostrartotal = 'Totales'; }
						$tabla .= '<tr name="grupo'.$an.'" class="filadesplegable nodetectadas" id="'.$an_nombre.'">
						<td class="total tittot primera"><span class="subtotal_tit">'.$mostrartotal.'</span></td>
						<td class="total"><span class="izquierda"></span></td>
						<td class="total"><span class="izquierda"></span></td>
						<td class="total">0</td>
						<td class="total">0</td>
						<td class="total">0%</td>
						<td class="total">0</td>
						<td class="total">0</td>
						<td class="total">0</td>';
						if($resumenif == "no"){
				   			$tabla .= '<td class="total"><a onclick="verCampana(\''.$filas["campana"].'\',\'\',\'\')" class="btn_vercampana"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a><a onclick="editarCampana(\'Kukimba Google Shoping\', \'UA-27910505-5\')" class="btn_vercampana"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
						}else{
							$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$datFechaInicioFiltro."&fechafin_ant=".$datFechaFinFiltro."&idVista=".$idVistaAnalytics;
				   			$tabla .= '<td class="total"><a href="'.$href.'" class="btn_vercampana"><img class="imagensearch" src="../admin/Envios/estructura_email/images/search.png"/></a></td>';
						}
						$tabla .= '</tr>';
						$tot_cont =0;
					}

				}else{

					if($devuelve[$key-1]["medium"] != $filas["medium"]){ $agrubacionb = " agrupacionb"; $mediumm = $filas["medium"]; }else{ $agrubacionb = ""; $mediumm=""; }
					$agrupacion = 'agrupacion grupo'.$an;
					$agr = 1;
					$tabla .= '<tr class="nodetectadas">
						<td class="'.$agrupacion.''.$agrubacionb.' primera"></td>
						<td class="'.$agrupacion.''.$agrubacionb.'"><span class="izquierda">'.$mediumm.'</span></td>
						<td class="'.$agrupacion.''.$agrubacionb.'"><span class="izquierda">'.$filas["source"].'</span></td>
						<td class="'.$agrupacion.''.$agrubacionb.'">0</td>
						<td class="'.$agrupacion.''.$agrubacionb.'">0</td>
						<td class="'.$agrupacion.''.$agrubacionb.'">0%</td>
						<td class="'.$agrupacion.''.$agrubacionb.'">0</td>
						<td class="'.$agrupacion.''.$agrubacionb.'">0</td>
						<td class="'.$agrupacion.''.$agrubacionb.'">0</td>';

					if($resumenif == "no"){
				   			$tabla .= '<td class="'.$agrupacion.''.$agrubacionb.'"><a onclick="verCampana(\''.$filas["campana"].'\',\''.$filas["medium"].'\',\''.$filas["source"].'\')" class="btn_vercampana"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a><a onclick="editarCampana(\'Kukimba Google Shoping\', \'UA-27910505-5\')" class="btn_vercampana"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
					}else{
						$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=".$filas["medium"]."&fuente=".$filas["source"]."&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$datFechaInicioFiltro."&fechafin_ant=".$datFechaFinFiltro."&idVista=".$idVistaAnalytics;
				   		$tabla .= '<td class="'.$agrupacion.''.$agrubacionb.'"><a href="'.$href.'" class="btn_vercampana"><img class="imagensearch" src="../admin/Envios/estructura_email/images/search.png"/></a></td>';
					}
					$tabla .= '</tr>';
					$campana_anterior = $filas["campana"];
					$tot_cont ++; 
				}//fin campana_anterior


				
			//}

			//al terminar comprobamos tmb si hay totales
			if($devuelve[$key-1]["campana"] == $filas["campana"] ){
				//A la hora de pintar el total de una campaña traemos los totales de ella
				if($resumenif != "si"){$mostrartotal = $an_nombre; }else{ $mostrartotal = 'Totales'; }
				$tabla .= '<tr name="grupo'.$an.'" class="filadesplegable nodetectadas" id="'.$an_nombre.'">
				<td class="total tittot primera"><span class="subtotal_tit">'.$mostrartotal.'</span></td>
				<td class="total"><span class="izquierda"></span></td>
				<td class="total"><span class="izquierda"></span></td>
				<td class="total">0</td>
				<td class="total">0</td>
				<td class="total">0%</td>
				<td class="total">0</td>
				<td class="total">0</td>
				<td class="total">0</td>';
				if($resumenif == "no"){
				   	$tabla .= '<td class="total"><a onclick="verCampana(\''.$filas["campana"].'\',\'\',\'\')" class="btn_vercampana"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a><a onclick="editarCampana(\'Kukimba Google Shoping\', \'UA-27910505-5\')" class="btn_vercampana"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
				}else{
					$href = "../public/comprobando.php?campana=".$filas["campana"]."&medio=&fuente=&fechaini=".$datFechaInicioFiltro."&fechafin=".$datFechaFinFiltro."&fechaini_ant=".$datFechaInicioFiltro."&fechafin_ant=".$datFechaFinFiltro."&idVista=".$idVistaAnalytics;
					$tabla .= '<td class="total"><a href="'.$href.'" class="btn_vercampana"><img class="imagensearch" src="../admin/Envios/estructura_email/images/search.png"/></a></td>';
				}
				$tabla .= '</tr>';

				$tot_cont =0;
			}
			
		}//fin for

		
	}// fin si es o la cantidad
	

	return $tabla;

}

function array_campanas_seguimiento($datFechaInicioFiltro, $datFechaFinFiltro, $emailus){

	$array_completo = array();	
	$fecha_actual = date("Y-m-d");
	$id_array = 0;
	$c = Nuevo_PDO();
	$sql = "SELECT LOCC_CAMPAING,LOCC_MEDIUM,LOCC_SOURCE,LOCCUD_USUARIO,LOCC_ANALYTICS_ID_PROPIEDAD, 
			(SELECT TOP 1 LOCDV_ANALYTICS_ID_VISTA
			 FROM LOC_DOMINIOS_VISTAS INNER JOIN LOC_DOMINIOS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO
			 WHERE LOCD_ANALYTICS_ID_PROPIEDAD=C.LOCC_ANALYTICS_ID_PROPIEDAD
			      AND LOCDV_ANALYTICS_DEFAULT_VISTA=1) AS VISTA_POR_DEFECTO,
			(SELECT TOP 1  LOCD_ANALYTICS_NAME_PROPIEDAD + ' | ' + LOCDV_ANALYTICS_URL_VISTA + ' | ' + LOCDV_ANALYTICS_NAME_VISTA 
			 FROM LOC_DOMINIOS_VISTAS INNER JOIN LOC_DOMINIOS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO
			 WHERE LOCD_ANALYTICS_ID_PROPIEDAD=C.LOCC_ANALYTICS_ID_PROPIEDAD
			      AND LOCDV_ANALYTICS_DEFAULT_VISTA=1) AS PROPIEDAD_VISTA_DESCRIPCION         
				FROM LOC_CAMPANAS as C inner join LOC_CAMPANAS_USUARIOS_DISTRIBUCION ON C.LOCC_CODIGO=LOC_CAMPANAS_USUARIOS_DISTRIBUCION.LOCCUD_SUCAMPANA
				WHERE 
				LOCCUD_ENVIOEMAIL_SEMANAL=1
				AND (LOCC_ACTIVARSEGUIMIENTO=1 AND (LOCC_FECHA_ENVIOS_INICIO IS NULL OR (LOCC_FECHA_ENVIOS_INICIO <= :fecha)) )
				AND (LOCC_ACTIVARSEGUIMIENTO=1 AND (LOCC_FECHA_ENVIOS_FIN IS NULL OR (LOCC_FECHA_ENVIOS_FIN >= :fechab )) )
				AND (LOCC_HISTORICO=0 OR LOCC_HISTORICO IS NULL)
				AND LOCCUD_USUARIO = '".$emailus."'";
	
	if($stmt = $c->prepare($sql)){
	    
	    $result = $stmt->execute(array(':fecha' => $fecha_actual,':fechab' => $fecha_actual));
	
	    if ($result && $stmt->rowCount() != 0){
	      
	      //Recorremos la primera sql
	      while ($fila = $stmt->fetch(PDO::FETCH_BOTH, PDO::FETCH_ORI_NEXT)) {

	        $array_completo[$id_array]["campana"] 		   	   = $fila[0];
	        $array_completo[$id_array]["medium"] 		   	   = $fila[1]; 
	        $array_completo[$id_array]["source"] 		   	   = $fila[2]; 
	        $array_completo[$id_array]["idvista"]   	       = $fila[5]; 
	        $array_completo[$id_array]["propiedad"]		   	   = $fila[4]; 
	        $array_completo[$id_array]["desc"]		   	       = $fila[6]; 
	        $asoc = propiedadVista($fila[5]);
	        foreach ($asoc["elementos"] as $key => $ac) {
	        	$asoct = $ac["proyectoasociado"];
	        }
	        $array_completo[$id_array]["proasoc"]		   	      = $asoct; 
	        $array_completo[$id_array]["id_campana"]       	      = '';	
	        $array_completo[$id_array]["sesiones_f"]  	          = 0;
		    $array_completo[$id_array]["rebote_f"]  		      = 0;
		    $array_completo[$id_array]["pagsesion_f"]  	          = 0;
		    $array_completo[$id_array]["usuarios_f"] 		      = 0;
		    $array_completo[$id_array]["goalcompletionsall_f"]    = 0;
			$array_completo[$id_array]["transactionRevenue_f"]    = 0;
	        $array_completo[$id_array]["transactionShipping_f"]   = 0;
	       	$array_completo[$id_array]["transactionTax_f"]        = 0;
	       	$array_completo[$id_array]["transactions_f"]          = 0;
		    
		    $array_completo[$id_array]["sesiones"]  	   	      = 0;
		    $array_completo[$id_array]["rebote"]  		   	      = 0;
		    $array_completo[$id_array]["pagsesion"]  	   	      = 0;
		    $array_completo[$id_array]["usuarios"] 		   	      = 0;
		    $array_completo[$id_array]["goalcompletionsall"]      = 0;	        
	       	$array_completo[$id_array]["transactionRevenue"]      = 0;
	       	$array_completo[$id_array]["transactionShipping"]     = 0;
	       	$array_completo[$id_array]["transactionTax"]          = 0;
	       	$array_completo[$id_array]["transactions"]            = 0;

	       	$array_completo[$id_array]["firstInteractionValue_f"] = 0;
		    $array_completo[$id_array]["lastInteractionValue_f"]  = 0;   
		    $array_completo[$id_array]["convasistidas_f"]         = 0;
			$array_completo[$id_array]["convpinteraccion_f"]      = 0;
			$array_completo[$id_array]["convuinteraccion_f"]      = 0;
			$array_completo[$id_array]["assistedValuel_f"]	      = 0;
			
		    $array_completo[$id_array]["firstInteractionValue"]   = 0;
		    $array_completo[$id_array]["lastInteractionValue"]    = 0;
		    $array_completo[$id_array]["convasistidas"]    	      = 0;
			$array_completo[$id_array]["convpinteraccion"] 	      = 0;
			$array_completo[$id_array]["convuinteraccion"] 	      = 0;
			$array_completo[$id_array]["assistedValuel"]	      = 0;


	        //Vamso a api de google por campaña
	        $strDimensions='';
			$strDimensionsN =''; 
			$metricas  = 'ga:sessions,ga:users,ga:bounceRate,ga:pageviewsPerSession,ga:goalCompletionsAll,ga:transactions,ga:transactionRevenue,ga:transactionShipping,ga:transactionTax';
			$array_Ordenacion = array('sessions');

			$filtro_and = "ga:campaign==".$fila[0].";ga:medium==".$fila[1].";ga:source==".$fila[2];
			/

			//Ahora llamamos a la Api
			$DirOrdenacion='';
			if ($DataTables_OrderDir=='asc') {$DirOrdenacion='-';}

			
			
		    //Creación objeto, parametrización -------------------------------------------------------
		    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
		    $obj_Datos_Api -> strProyectoAsociado  = $asoct;
		    $obj_Datos_Api -> idVista              ='ga:'. $fila[5];
		    $obj_Datos_Api -> startdate            = $fecha_actual;
		    $obj_Datos_Api -> enddate              = $fecha_actual;
		    $obj_Datos_Api -> metrics              = $metricas;  
		    $obj_Datos_Api -> optParams       	   = array(
														'dimensions' => $strDimensions,
			                                            'sort' => '-ga:sessions',
			                                            'filters' => $filtro_and,
														'start-index' => 1,
			                                            'max-results' => 1);  
		   	
		    $obj_Datos_Api -> Construccion();

		    for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		        $array_completo[$id_array]["sesiones_f"]  	          = $obj_Datos_Api->ValorF("sessions",$x);
		        $array_completo[$id_array]["rebote_f"]  		      = $obj_Datos_Api->ValorF("bounceRate",$x);
		        $array_completo[$id_array]["pagsesion_f"]  	          = $obj_Datos_Api->ValorF("pageviewsPerSession",$x);
		        $array_completo[$id_array]["usuarios_f"] 		      = $obj_Datos_Api->ValorF("users",$x);         
		        $array_completo[$id_array]["goalcompletionsall_f"]    = $obj_Datos_Api->ValorF("goalcompletionsall",$x);
				$array_completo[$id_array]["transactionRevenue_f"]    = $obj_Datos_Api->ValorF("transactionRevenue",$x);
	        	$array_completo[$id_array]["transactionShipping_f"]   = $obj_Datos_Api->ValorF("transactionShipping",$x);
	       		$array_completo[$id_array]["transactionTax_f"]        = $obj_Datos_Api->ValorF("transactionTax",$x);
	       		$array_completo[$id_array]["transactions_f"]          = $obj_Datos_Api->ValorF("sessions",$x);	
		        
		        $array_completo[$id_array]["sesiones"]  	   	      = $obj_Datos_Api->Valor("sessions",$x);
		        $array_completo[$id_array]["rebote"]  		   	      = $obj_Datos_Api->Valor("bounceRate",$x);
		        $array_completo[$id_array]["pagsesion"]  	   	      = $obj_Datos_Api->Valor("pageviewsPerSession",$x); 
		        $array_completo[$id_array]["usuarios"] 		   	      = $obj_Datos_Api->Valor("users",$x);		        
		        $array_completo[$id_array]["goalcompletionsall"]      = $obj_Datos_Api->Valor("goalcompletionsall",$x);     	        
	       		$array_completo[$id_array]["transactionRevenue"]      = $obj_Datos_Api->Valor("transactionRevenue",$x);
	       		$array_completo[$id_array]["transactionShipping"]     = $obj_Datos_Api->Valor("transactionShipping",$x);
	       		$array_completo[$id_array]["transactionTax"]          = $obj_Datos_Api->Valor("transactionTax",$x);
	       		$array_completo[$id_array]["transactions"]            = $obj_Datos_Api->Valor("transactions",$x);
		        
	        
	        }

	        //Y ahora completamso con mdf
	        $metricas = 'mcf:firstInteractionConversions,mcf:assistedConversions,mcf:lastInteractionConversions,mcf:assistedValue,mcf:firstInteractionValue,mcf:lastInteractionValue';
		    $strDimensions = '';
		    $filtro = 'mcf:conversionType==Goal;mcf:campaignName!='.$fila[0].'';

		    $obj_Datos_Api_mcf = new Datos_Api_Mcf(); // Instanciamos la clase Datos_Api_Mcf
		    $obj_Datos_Api_mcf -> strProyectoAsociado  = $asoct;
		    $obj_Datos_Api_mcf -> idVista              ='ga:'. $fila[5];
		    $obj_Datos_Api_mcf -> startdate            = $fecha_actual;
		    $obj_Datos_Api_mcf -> enddate              = $fecha_actual;
		    $obj_Datos_Api_mcf -> metrics              = $metricas;
		    $obj_Datos_Api_mcf -> optParams            = array(
														'dimensions' => $strDimensions,
			                                            'filters' => $filtro,
														'start-index' => 1,
			                                            'max-results' => 1
			                                            );  
		    $obj_Datos_Api_mcf -> Construccion();

			for ($x=1; $x<=$obj_Datos_Api_mcf->NumValores(); $x++) {

		        $array_completo[$id_array]["firstInteractionValue_f"] = $obj_Datos_Api_mcf->ValorF(0,$x);
		        $array_completo[$id_array]["lastInteractionValue_f"]  = $obj_Datos_Api_mcf->ValorF(2,$x);    
		        $array_completo[$id_array]["convasistidas_f"]         = $obj_Datos_Api_mcf->ValorF(1,$x); 
			    $array_completo[$id_array]["convpinteraccion_f"]      = $obj_Datos_Api_mcf->ValorF(4,$x); 
			    $array_completo[$id_array]["convuinteraccion_f"]      = $obj_Datos_Api_mcf->ValorF(5,$x);
			    $array_completo[$id_array]["assistedValuel_f"]	      = $obj_Datos_Api_mcf->ValorF(3,$x);
				
		        $array_completo[$id_array]["firstInteractionValue"]   = $obj_Datos_Api_mcf->Valor(0,$x);
		        $array_completo[$id_array]["lastInteractionValue"]    = $obj_Datos_Api_mcf->Valor(2,$x);
		        $array_completo[$id_array]["convasistidas"]    	      = $obj_Datos_Api_mcf->Valor(1,$x); 
			    $array_completo[$id_array]["convpinteraccion"] 	      = $obj_Datos_Api_mcf->Valor(4,$x); 
			    $array_completo[$id_array]["convuinteraccion"] 	      = $obj_Datos_Api_mcf->Valor(5,$x);
			    $array_completo[$id_array]["assistedValuel"]	      = $obj_Datos_Api_mcf->Valor(3,$x);

			}

	        $id_array++;
	      }

	      
	    }else{
	      //No hay campañas en la tabla
	    }
	}


	return $array_completo;
}


function array_campanas($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPropiedad, $ctype){

	//PRIMERO LLAMAMOS A LA BASE DE DATOS
	
	$array_completo = array();	
	$id_array = 0;
	
	$fecha_actual =date("Y-m-d");
	$c = Nuevo_PDO();

	$sql = "SELECT LOCC_CAMPAING,LOCC_MEDIUM,LOCC_SOURCE
			FROM LOC_CAMPANAS
			WHERE LOCC_ANALYTICS_ID_PROPIEDAD = ".$idPropiedad."
				AND LOCC_FECHA_ENVIOS_INICIO >= :fecha
				AND LOCC_FECHA_ENVIOS_FIN <= :fechab
			ORDER BY LOCC_CAMPAING DESC";	

	if($stmt = $c->prepare($sql)){
	    
	    $result = $stmt->execute(array(':fecha' => $fecha_actual,':fechab' => $fecha_actual));
	
	    if ($result && $stmt->rowCount() != 0){
	      
	      //Recorremos la primera sql
	      while ($fila = $stmt->fetch(PDO::FETCH_BOTH, PDO::FETCH_ORI_NEXT)) {

	        $array_completo[$id_array]["campana"] 		   	   = $fila[0];
	        $array_completo[$id_array]["medium"] 		   	   = $fila[1]; 
	        $array_completo[$id_array]["source"] 		   	   = $fila[2]; 
	        $array_completo[$id_array]["id_campana"]       	   = '';	        


	        $array_completo[$id_array]["sesiones_f"]  	          = 0;
	        $array_completo[$id_array]["rebote_f"]  		      = 0;
	        $array_completo[$id_array]["pagsesion_f"]  	          = 0; 
	        $array_completo[$id_array]["usuarios_f"] 		      = 0; 
	        $array_completo[$id_array]["convasistidas_f"]         = 0; 
	        $array_completo[$id_array]["convpinteraccion_f"]      = 0; 
	        $array_completo[$id_array]["convuinteraccion_f"]      = 0;
	        $array_completo[$id_array]["goalcompletionsall_f"]    = 0;
	        $array_completo[$id_array]["assistedValuel_f"]	      = 0;
	        $array_completo[$id_array]["firstInteractionValue_f"] = 0;
	        $array_completo[$id_array]["lastInteractionValue_f"]  = 0;
	        $array_completo[$id_array]["transactionRevenue_f"]    = 0;
	        $array_completo[$id_array]["transactionShipping_f"]   = 0;
	        $array_completo[$id_array]["transactionTax_f"]        = 0;
	        $array_completo[$id_array]["transactions_f"]          = 0;


	        $array_completo[$id_array]["sesiones"]  	   	      = 0;
	        $array_completo[$id_array]["rebote"]  		   	      = 0;
	        $array_completo[$id_array]["pagsesion"]  	   	      = 0; 
	        $array_completo[$id_array]["usuarios"] 		   	      = 0; 
	        $array_completo[$id_array]["convasistidas"]    	      = 0; 
	        $array_completo[$id_array]["convpinteraccion"] 	      = 0; 
	        $array_completo[$id_array]["convuinteraccion"] 	      = 0;
	        $array_completo[$id_array]["goalcompletionsall"]      = 0;
	        $array_completo[$id_array]["assistedValuel"]	      = 0;
	        $array_completo[$id_array]["firstInteractionValue"]   = 0;
	        $array_completo[$id_array]["lastInteractionValue"]    = 0;
	        $array_completo[$id_array]["transactionRevenue"]      = 0;
	        $array_completo[$id_array]["transactionShipping"]     = 0;
	        $array_completo[$id_array]["transactionTax"]          = 0;
	        $array_completo[$id_array]["transactions"]            = 0;


	        $id_array++;
	      }

	      
	    }else{
	      //No hay campañas en la tabla
	    }
	}
	  
	$strDimensions='ga:campaign,ga:medium,ga:source,ga:adwordsCampaignID';
	$strDimensionsN ='campaign'; 
	$metricas  = 'ga:sessions,ga:users,ga:bounceRate,ga:pageviewsPerSession,ga:goalCompletionsAll,ga:transactions,ga:transactionRevenue,ga:transactionShipping,ga:transactionTax';
	$array_Ordenacion = array('sessions');

	$filtro_and = "ga:medium!=referral;ga:medium!=organic;ga:medium!=(none)";
	//$filtro_and = "ga:campaign!=(not set)";

	//Ahora llamamos a la Api
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='asc') {$DirOrdenacion='-';}

	//$filtro = 'ga:campaign!=(not set)';
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;  
    $obj_Datos_Api -> optParams       	   = array(
												'dimensions' => $strDimensions,
	                                            'sort' => '-ga:campaign,-ga:medium,-ga:sessions',
	                                            'filters' => $filtro_and,
												'start-index' => 1,
	                                            'max-results' => 1000);  
   	
    $obj_Datos_Api -> Construccion();

	//Si existe array
	if($id_array!=0){

    	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

			$sw=0;
			foreach ($array_completo as $key => $ac) {
				if( ($ac["campana"] == $obj_Datos_Api->Valor("campaign",$x)) && ($ac["medium"] == $obj_Datos_Api->Valor("medium",$x)) && ($ac["source"] == $obj_Datos_Api->Valor("source",$x))){
					$array_completo[$key]["sesiones_f"]   		  		  = $obj_Datos_Api->ValorF("sessions",$x);
					$array_completo[$key]["rebote_f"]     		  		  = $obj_Datos_Api->ValorF("bounceRate",$x);
					$array_completo[$key]["pagsesion_f"]  		  		  = $obj_Datos_Api->ValorF("pageviewsPerSession",$x); 
					$array_completo[$key]["usuarios_f"]   		  		  = $obj_Datos_Api->ValorF("users",$x); 
					$array_completo[$key]["id_campana_f"] 		  		  = $obj_Datos_Api->ValorF("adwordsCampaignID",$x);
					$array_completo[$key]["goalcompletionsall_f"] 		  = $obj_Datos_Api->ValorF("goalCompletionsAll",$x);
	        		$array_completo[$key]["transactionRevenue_f"]    	  = $obj_Datos_Api->ValorF("transactionRevenue",$x);
	        		$array_completo[$key]["transactionShipping_f"]        = $obj_Datos_Api->ValorF("transactionShipping",$x);
	        		$array_completo[$key]["transactionTax_f"]             = $obj_Datos_Api->ValorF("transactionTax",$x);
	        		$array_completo[$key]["transactions_f"]               = $obj_Datos_Api->ValorF("transactions",$x);
		  
					$array_completo[$key]["sesiones"]     		  = $obj_Datos_Api->Valor("sessions",$x);
					$array_completo[$key]["rebote"]       		  = $obj_Datos_Api->Valor("bounceRate",$x);
					$array_completo[$key]["pagsesion"]    		  = $obj_Datos_Api->Valor("pageviewsPerSession",$x); 
					$array_completo[$key]["usuarios"]     		  = $obj_Datos_Api->Valor("users",$x); 
					$array_completo[$key]["id_campana"]   		  = $obj_Datos_Api->Valor("adwordsCampaignID",$x);
					$array_completo[$key]["goalcompletionsall"]   = $obj_Datos_Api->Valor("goalCompletionsAll",$x);
					$array_completo[$key]["transactionRevenue"]   = $obj_Datos_Api->Valor("transactionRevenue",$x);
	        		$array_completo[$key]["transactionShipping"]  = $obj_Datos_Api->Valor("transactionShipping",$x);
	        		$array_completo[$key]["transactionTax"]       = $obj_Datos_Api->Valor("transactionTax",$x);
	        		$array_completo[$key]["transactions"]         = $obj_Datos_Api->Valor("transactions",$x);
					$sw=1;
				}
			}
			if($sw==0){
				$array_completo[$id_array]["id_campana"] 	          = $obj_Datos_Api->ValorF("adwordsCampaignID",$x);
				$array_completo[$id_array]["campana"] 		  	      = $obj_Datos_Api->ValorF("campaign",$x);
				$array_completo[$id_array]["medium"] 		          = $obj_Datos_Api->ValorF("medium",$x);
				$array_completo[$id_array]["source"] 		          = $obj_Datos_Api->ValorF("source",$x);
				     
				$array_completo[$id_array]["sesiones_f"]  	          = $obj_Datos_Api->ValorF("sessions",$x);
				$array_completo[$id_array]["rebote_f"]  		      = $obj_Datos_Api->ValorF("bounceRate",$x);
				$array_completo[$id_array]["pagsesion_f"]  	          = $obj_Datos_Api->ValorF("pageviewsPerSession",$x); 
				$array_completo[$id_array]["usuarios_f"] 		      = $obj_Datos_Api->ValorF("users",$x); 
				$array_completo[$id_array]["goalcompletionsall_f"]    = $obj_Datos_Api->ValorF("goalCompletionsAll",$x);
				$array_completo[$id_array]["transactionRevenue_f"]    = $obj_Datos_Api->ValorF("transactionRevenue",$x);
	        	$array_completo[$id_array]["transactionShipping_f"]   = $obj_Datos_Api->ValorF("transactionShipping",$x);
	        	$array_completo[$id_array]["transactionTax_f"]        = $obj_Datos_Api->ValorF("transactionTax",$x);
	        	$array_completo[$id_array]["transactions_f"]          = $obj_Datos_Api->ValorF("transactions",$x);
	        	$array_completo[$id_array]["assistedValuel_f"]	      = 0;
	            $array_completo[$id_array]["firstInteractionValue_f"] = 0;
	            $array_completo[$id_array]["lastInteractionValue_f"]  = 0;
				$array_completo[$id_array]["convasistidas_f"]         = 0; 
				$array_completo[$id_array]["convpinteraccion_f"]      = 0; 
				$array_completo[$id_array]["convuinteraccion_f"]      = 0;
  
				$array_completo[$id_array]["sesiones"]  	          = $obj_Datos_Api->Valor("sessions",$x);
				$array_completo[$id_array]["rebote"]  		          = $obj_Datos_Api->Valor("bounceRate",$x);
				$array_completo[$id_array]["pagsesion"]  	          = $obj_Datos_Api->Valor("pageviewsPerSession",$x); 
				$array_completo[$id_array]["usuarios"] 		          = $obj_Datos_Api->Valor("users",$x); 
				$array_completo[$id_array]["goalcompletionsall"]      = $obj_Datos_Api->Valor("goalCompletionsAll",$x);
				$array_completo[$id_array]["transactionRevenue"]      = $obj_Datos_Api->Valor("transactionRevenue",$x);
	        	$array_completo[$id_array]["transactionShipping"]     = $obj_Datos_Api->Valor("transactionShipping",$x);
	        	$array_completo[$id_array]["transactionTax"]          = $obj_Datos_Api->Valor("transactionTax",$x);
	        	$array_completo[$id_array]["transactions"]            = $obj_Datos_Api->Valor("transactions",$x);
				$array_completo[$id_array]["assistedValuel"]	      = 0;
	            $array_completo[$id_array]["firstInteractionValue"]   = 0;
	            $array_completo[$id_array]["lastInteractionValue"]    = 0;
				$array_completo[$id_array]["convasistidas"]           = 0; 
				$array_completo[$id_array]["convpinteraccion"]        = 0; 
				$array_completo[$id_array]["convuinteraccion"]        = 0;
   
				$array_completo[$id_array]["convtotal"]  		      = 0;
				$array_completo[$id_array]["transtotal"]              = 0;
				$id_array++;
			}
		}

	}else{

		for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

			$array_completo[$id_array]["id_campana"] 	          = $obj_Datos_Api->ValorF("adwordsCampaignID",$x);
			$array_completo[$id_array]["campana"] 		          = $obj_Datos_Api->ValorF("campaign",$x);
			$array_completo[$id_array]["medium"] 		          = $obj_Datos_Api->ValorF("medium",$x);
			$array_completo[$id_array]["source"] 		          = $obj_Datos_Api->ValorF("source",$x);
			
			$array_completo[$id_array]["sesiones_f"]  	          = $obj_Datos_Api->ValorF("sessions",$x);
			$array_completo[$id_array]["rebote_f"]  		      = $obj_Datos_Api->ValorF("bounceRate",$x);
			$array_completo[$id_array]["pagsesion_f"]  	          = $obj_Datos_Api->ValorF("pageviewsPerSession",$x); 
			$array_completo[$id_array]["usuarios_f"] 		      = $obj_Datos_Api->ValorF("users",$x); 
			$array_completo[$id_array]["goalcompletionsall_f"]    = $obj_Datos_Api->ValorF("goalCompletionsAll",$x);
			$array_completo[$id_array]["transactionRevenue_f"]    = $obj_Datos_Api->ValorF("transactionRevenue",$x);
	        $array_completo[$id_array]["transactionShipping_f"]   = $obj_Datos_Api->ValorF("transactionShipping",$x);
	        $array_completo[$id_array]["transactionTax_f"]        = $obj_Datos_Api->ValorF("transactionTax",$x);
	        $array_completo[$id_array]["transactions_f"]          = $obj_Datos_Api->ValorF("transactions",$x);
			$array_completo[$id_array]["assistedValuel"]	      = 0;
	        $array_completo[$id_array]["firstInteractionValue_f"] = 0;
	        $array_completo[$id_array]["lastInteractionValue_f"]  = 0;
			$array_completo[$id_array]["convasistidas_f"]         = 0; 
			$array_completo[$id_array]["convpinteraccion_f"]      = 0; 
			$array_completo[$id_array]["convuinteraccion_f"]      = 0;
  
			$array_completo[$id_array]["sesiones"]  	          = $obj_Datos_Api->Valor("sessions",$x);
			$array_completo[$id_array]["rebote"]  		          = $obj_Datos_Api->Valor("bounceRate",$x);
			$array_completo[$id_array]["pagsesion"]  	          = $obj_Datos_Api->Valor("pageviewsPerSession",$x); 
			$array_completo[$id_array]["usuarios"] 		          = $obj_Datos_Api->Valor("users",$x); 
			$array_completo[$id_array]["goalcompletionsall"]      = $obj_Datos_Api->Valor("goalCompletionsAll",$x);
			$array_completo[$id_array]["transactionRevenue"]      = $obj_Datos_Api->Valor("transactionRevenue",$x);
	        $array_completo[$id_array]["transactionShipping"]     = $obj_Datos_Api->Valor("transactionShipping",$x);
	        $array_completo[$id_array]["transactionTax"]          = $obj_Datos_Api->Valor("transactionTax",$x);
	        $array_completo[$id_array]["transactions"]            = $obj_Datos_Api->Valor("transactions",$x);
			$array_completo[$id_array]["assistedValue"]	          = 0;
	        $array_completo[$id_array]["firstInteractionValue"]   = 0;
	        $array_completo[$id_array]["lastInteractionValue"]    = 0;
			$array_completo[$id_array]["convasistidas"]           = 0; 
			$array_completo[$id_array]["convpinteraccion"]        = 0; 
			$array_completo[$id_array]["convuinteraccion"]        = 0;

			$array_completo[$id_array]["convtotal"]               = 0;
			$array_completo[$id_array]["transtotal"]              = 0;
			$id_array++;
			
		}

	}

    //AQUI LLAMAMOS A LA NUEVA API PARA SAVER LAS CONVERSIONES	mcf:assistedValue

    $metricas = 'mcf:firstInteractionConversions,mcf:assistedConversions,mcf:lastInteractionConversions,mcf:assistedValue,mcf:firstInteractionValue,mcf:lastInteractionValue';
    $strDimensions = 'mcf:campaignName,mcf:medium,mcf:source,mcf:adwordsCampaignID';
    $filtro = 'mcf:conversionType=='.$ctype.';mcf:campaignName!=(not set)';

    $obj_Datos_Api_mcf = new Datos_Api_Mcf(); // Instanciamos la clase Datos_Api_Mcf
    $obj_Datos_Api_mcf -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_mcf -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_mcf -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_mcf -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_mcf -> metrics              = $metricas;
    $obj_Datos_Api_mcf -> optParams            = array(
												'dimensions' => $strDimensions,
	                                            'filters' => $filtro,
												'start-index' => 1,
	                                            'max-results' => 1000
	                                            );  
    $obj_Datos_Api_mcf -> Construccion();


    //Si existe array
	if($id_array!=0){

		//$pruebadatos = $obj_Datos_Api_mcf->Valor(1,1); 

    	for ($x=1; $x<=$obj_Datos_Api_mcf->NumValores(); $x++) {
    		//$pruebadatos .= $obj_Datos_Api_mcf->Valor(1,$x)." - ";

			foreach ($array_completo as $key => $ac) {

				if($ac["id_campana"] != "(not set)" && $ac["id_campana"] ==  $obj_Datos_Api_mcf->Valor(4,$x)){
					
					$array_completo[$key]["id_campana"]       = $obj_Datos_Api_mcf->ValorF(4,$x);
					
					$array_completo[$key]["convpinteraccion_f"]      = $obj_Datos_Api_mcf->ValorF(5,$x);
					$array_completo[$key]["convasistidas_f"]         = $obj_Datos_Api_mcf->ValorF(6,$x);					
					$array_completo[$key]["convuinteraccion_f"]      = $obj_Datos_Api_mcf->ValorF(7,$x);
					$array_completo[$key]["assistedValue_f"]         = $obj_Datos_Api_mcf->ValorF(8,$x);
					$array_completo[$key]["firstInteractionValue_f"] = $obj_Datos_Api_mcf->ValorF(9,$x);					
					$array_completo[$key]["lastInteractionValue_f"]  = $obj_Datos_Api_mcf->ValorF(10,$x);

					$array_completo[$key]["convpinteraccion"]        = $obj_Datos_Api_mcf->Valor(5,$x);
					$array_completo[$key]["convasistidas"]           = $obj_Datos_Api_mcf->Valor(6,$x);	
					$array_completo[$key]["convuinteraccion"]        = $obj_Datos_Api_mcf->Valor(7,$x);
					$array_completo[$key]["assistedValue"]           = $obj_Datos_Api_mcf->Valor(8,$x);
					$array_completo[$key]["firstInteractionValue"]   = $obj_Datos_Api_mcf->Valor(9,$x);					
					$array_completo[$key]["lastInteractionValue"]    = $obj_Datos_Api_mcf->Valor(10,$x);

					$array_completo[$key]["convtotal"]   		     = $obj_Datos_Api_mcf->Valor(6,$x) + $obj_Datos_Api_mcf->Valor(7,$x);
					$array_completo[$key]["transtotal"]   		     = $obj_Datos_Api_mcf->Valor(8,$x) + $obj_Datos_Api_mcf->Valor(10,$x);
					

				}else{

					if( ($ac["campana"] == $obj_Datos_Api_mcf->Valor(1,$x)) && ($ac["medium"] == $obj_Datos_Api_mcf->Valor(2,$x)) && ($ac["source"] == $obj_Datos_Api_mcf->Valor(3,$x))){
						
						$array_completo[$key]["id_campana"]              = $obj_Datos_Api_mcf->ValorF(4,$x);
					     
						$array_completo[$key]["convpinteraccion_f"]      = $obj_Datos_Api_mcf->ValorF(5,$x);
						$array_completo[$key]["convasistidas_f"]         = $obj_Datos_Api_mcf->ValorF(6,$x);						
						$array_completo[$key]["convuinteraccion_f"]      = $obj_Datos_Api_mcf->ValorF(7,$x);
						$array_completo[$key]["assistedValue_f"]         = $obj_Datos_Api_mcf->ValorF(8,$x);
					    $array_completo[$key]["firstInteractionValue_f"] = $obj_Datos_Api_mcf->ValorF(9,$x);					
					    $array_completo[$key]["lastInteractionValue_f"]  = $obj_Datos_Api_mcf->ValorF(10,$x);

						$array_completo[$key]["convpinteraccion"]        = $obj_Datos_Api_mcf->Valor(5,$x);
						$array_completo[$key]["convasistidas"]           = $obj_Datos_Api_mcf->Valor(6,$x);					
						$array_completo[$key]["convuinteraccion"]        = $obj_Datos_Api_mcf->Valor(7,$x);
						$array_completo[$key]["assistedValue"]           = $obj_Datos_Api_mcf->Valor(8,$x);
					    $array_completo[$key]["firstInteractionValue"]   = $obj_Datos_Api_mcf->Valor(9,$x);					
					    $array_completo[$key]["lastInteractionValue"]    = $obj_Datos_Api_mcf->Valor(10,$x);

						$array_completo[$key]["convtotal"]   		     = $obj_Datos_Api_mcf->Valor(6,$x) + $obj_Datos_Api_mcf->Valor(7,$x);
						$array_completo[$key]["transtotal"]   		     = $obj_Datos_Api_mcf->Valor(8,$x) + $obj_Datos_Api_mcf->Valor(10,$x);


					}
				}

				
			}

		}

	}

	return $array_completo;
}



function totalCampanas($campana,$strProyectoAsociado,$idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro,$trans){


	$strDimensions='ga:campaign,ga:medium,ga:source,ga:adwordsCampaignID';
	$strDimensionsN ='campaign'; 
	$metricas  = 'ga:sessions,ga:users,ga:bounceRate,ga:pageviewsPerSession,ga:goalCompletionsAll,ga:transactions,ga:transactionRevenue,ga:transactionShipping,ga:transactionTax';
	$array_Ordenacion = array('sessions');

	$filtro_and = "ga:medium!=referral;ga:medium!=organic;ga:medium!=(none);ga:campaign==".$campana;
	//$filtro_and = "ga:campaign!=(not set)";

	//Ahora llamamos a la Api
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='asc') {$DirOrdenacion='-';}

	//$filtro = 'ga:campaign!=(not set)';
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;  
    $obj_Datos_Api -> optParams       	   = array(
												'dimensions' => $strDimensions,
	                                            'sort' => '-ga:campaign,-ga:medium,-ga:sessions',
	                                            'filters' => $filtro_and,
												'start-index' => 1,
	                                            'max-results' => 1000);  
   	
    $obj_Datos_Api -> Construccion();

    $array_totales = array();
 	$array_totales["nombre_campana"]          = $campana;
    $array_totales["tot_sesiones"]            = $obj_Datos_Api->TotalF("sessions",$x);
	$array_totales["tot_rebote"]              = $obj_Datos_Api->TotalF("bounceRate",$x);
	$array_totales["tot_pagsesion"]           = $obj_Datos_Api->TotalF("pageviewsPerSession",$x);
	$array_totales["tot_usuarios"]            = $obj_Datos_Api->TotalF("users",$x);
	$array_totales["tot_ga"]   		          = $obj_Datos_Api->TotalF("goalCompletionsAll",$x);
	$array_totales["tot_trans"]   		      = $obj_Datos_Api->TotalF("transactions",$x);
	$array_totales["tot_transactionRevenue"]  = $obj_Datos_Api->TotalF("transactionRevenue",$x);
	$array_totales["tot_transactionShipping"] = $obj_Datos_Api->TotalF("transactionShipping",$x);
	$array_totales["tot_transactionTax"]   	  = $obj_Datos_Api->TotalF("transactionTax",$x);
	return $array_totales;
}

function totalSubCampanas($campana,$medium,$strProyectoAsociado,$idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro){

	$strDimensions='ga:campaign,ga:medium,ga:source,ga:adwordsCampaignID';
	$strDimensionsN ='campaign'; 
	$metricas  = 'ga:sessions,ga:users,ga:bounceRate,ga:pageviewsPerSession,ga:goalCompletionsAll,ga:transactions,ga:transactionRevenue,ga:transactionShipping,ga:transactionTax';
	$array_Ordenacion = array('sessions');

	$filtro_and = "ga:medium!=referral;ga:medium!=organic;ga:medium!=(none);ga:campaign==".$campana.";ga:medium==".$medium;
	//$filtro_and = "ga:campaign!=(not set)";

	//Ahora llamamos a la Api
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='asc') {$DirOrdenacion='-';}

	//$filtro = 'ga:campaign!=(not set)';
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api -> metrics              = $metricas;  
    $obj_Datos_Api -> optParams       	   = array(
												'dimensions' => $strDimensions,
	                                            'sort' => '-ga:campaign,-ga:medium,-ga:sessions',
	                                            'filters' => $filtro_and,
												'start-index' => 1,
	                                            'max-results' => 1000);  
   	
    $obj_Datos_Api -> Construccion();

    $array_totales = array();
 	$array_totales["nombre_campana"] 		  = $campana;
    $array_totales["tot_sesiones"]   		  = $obj_Datos_Api->TotalF("sessions",$x);
	$array_totales["tot_rebote"]     		  = $obj_Datos_Api->TotalF("bounceRate",$x);
	$array_totales["tot_pagsesion"]  		  = $obj_Datos_Api->TotalF("pageviewsPerSession",$x);
	$array_totales["tot_usuarios"]   		  = $obj_Datos_Api->TotalF("users",$x);
	$array_totales["tot_ga"]   		 		  = $obj_Datos_Api->TotalF("goalCompletionsAll",$x);
	$array_totales["tot_trans"]   		      = $obj_Datos_Api->TotalF("transactions",$x);
	$array_totales["tot_transactionRevenue"]  = $obj_Datos_Api->TotalF("transactionRevenue",$x);
	$array_totales["tot_transactionShipping"] = $obj_Datos_Api->TotalF("transactionShipping",$x);
	$array_totales["tot_transactionTax"]   	  = $obj_Datos_Api->TotalF("transactionTax",$x);
	return $array_totales;
}



function FiltrosSocial($mod,$api){
	switch ($mod) {
		case 1:
			$expr = '=~';
			$coma = ",";
			break;		
		case 2:
			$expr = '!~';
			$coma = ";";
			break;
	}

	$filtro = "";
	$c = Nuevo_PDO();
	$sql = "SELECT LOCGS_SOCIALNETWORK, LOCGS_FILTRO_EXPREG
			FROM dbo.LOC_GA_SOCIALNETWORKS";
	$stmt = $c->prepare($sql);
	$result = $stmt->execute();

	if ($result && $stmt->rowCount() != 0){
	    $count = 0;
	    //echo "Filas: ".$stmt->rowCount()."<br>";
	    while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
	      $count++;
	      $id = $fila[0];
	      $filtro .= $api.":source".$expr.$fila[1].$coma ; 
	    }

	}else{
	    $filtro = false;
	}

	return $filtro;  

}

function terminos_convierten ($blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro )
{

	$metricas = 'mcf:assistedConversions, mcf:lastInteractionConversions';
    $strDimensions = 'mcf:keyword';

	$filtro="";
	/*if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}*/

	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ";"; }
		$filtro .= $strFiltro;
	}

	$filtro = str_replace("ga:", "mcf:", $filtro);
	$filtro = str_replace("campaign", "campaignName", $filtro);
	
    $obj_Datos_Api_mcf = new Datos_Api_Mcf(); // Instanciamos la clase Datos_Api_Mcf
    $obj_Datos_Api_mcf -> strProyectoAsociado  = $strProyectoAsociado;
    $obj_Datos_Api_mcf -> idVista              ='ga:'. $idVistaAnalytics;
    $obj_Datos_Api_mcf -> startdate            = $datFechaInicioFiltro;
    $obj_Datos_Api_mcf -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Api_mcf -> metrics              = $metricas;
    $obj_Datos_Api_mcf -> optParams            = array(
												'dimensions' => $strDimensions,
	                                            'filters' => $filtro,
												'start-index' =>$DataTables_start+1,
	                                            'max-results' => $DataTables_length
	                                            );  
    $obj_Datos_Api_mcf -> Construccion();
	
	
	//Objeto creado ---------------------------------------------------------------------------	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api_mcf->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api_mcf->NumValoresReal() .',';
	$strA .= '"data":[';
	if($obj_Datos_Api_mcf->NumValores()!=0){
	    for ($x=1; $x<=$obj_Datos_Api_mcf->NumValores(); $x++) {
			$strA .= '[';
			$strA .= '"'.$obj_Datos_Api_mcf->Valor(1,$x).'",';
			$strA .= '"'.($obj_Datos_Api_mcf->Valor(2,$x)+$obj_Datos_Api_mcf->Valor(3,$x)).'",';
			$strA .= '"'.$obj_Datos_Api_mcf->ValorF(2,$x).'",';
			$strA .= '"'.$obj_Datos_Api_mcf->ValorF(3,$x).'"';
			$strA .= '],';
	    }
	    $strA = substr($strA, 0, -1);
	}

    $strA .= ']}';
    

    return $strA;
}

function FiltrosSocial_script($fuente){

    $coincide = 0;
    $c = Nuevo_PDO();
    $sql = "SELECT LOCGS_SOCIALNETWORK, LOCGS_FILTRO_EXPREG
            FROM dbo.LOC_GA_SOCIALNETWORKS";
    $stmt = $c->prepare($sql);
    $result = $stmt->execute();

    if ($result && $stmt->rowCount() != 0){

        //echo "Filas: ".$stmt->rowCount()."<br>";
        while ($fila = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {

            //Evaluamos la expresion regular de la tabla con la fuente
            if(preg_match("/".$fila[1]."/",$fuente)){
                $coincide = 1;
                //echo $fila[0]."   -   /".$fila[1]."/   -   ".$fuente."<br>";
            }

        }
        if($coincide==0){
            $filtro = false;
        }else{
            $filtro = true;
        }

    }else{
        $filtro = false;
    }

    return $filtro;  

}

function comprobarTipo($medio,$fuente,$campana){



    $tipo = "";
    //Con la fuente hayamos en la db si existe
    if(FiltrosSocial_script($fuente)){ $fu =1; }else{ $fu=0; }

    //ENLACES
    if($medio == "referral" && $campana == "(not set)" && $fu==0 ){
        $tipo="enlaces";
    }
    //SEO
    if($medio == "organic" && $campana == "(not set)" && $fu==0 ){
        $tipo="seo";
    }
    //SOCIAL
    if($fu==1  && $campana == "(not set)" ){
        $tipo="social";
    }
    //CAMPAÑAS
    if( $campana != "(not set)"  ){
        $tipo="campanas";
    }
    //DIRECTO
    if($medio != "referral" && $medio != "organic" && $campana == "(not set)" && $fu==0){
        $tipo="directo";
    }

    return $tipo;
} 

function datatable_informes_pedidos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro, $perspectiva, $perspectiva_seg)
{

	global $trans;
	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($perspectiva)) {

		/*ECOMMERCE*/
		case "CODPEDIDO":
			$strDimensions='ga:transactionId ';
			$strSort='transactionId';
			$strNombre_Dimension = $trans->__('Código pedido', false);
			break; 
		case "DIASTRANSACCION":
			$strDimensions='ga:daysToTransaction ';
			$strSort='daysToTransaction ';
			$strNombre_Dimension = $trans->__('Días para la transacción', false);
			break;
		case "SESIONESTRANSACCION":
			$strDimensions='ga:sessionsToTransaction';
			$strSort='sessionsToTransaction';
			$strNombre_Dimension = $trans->__('Sesiones para la transacción', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions='ga:source';
			$strSort='source';
			$strNombre_Dimension = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions='ga:medium';
			$strSort='medium';
			$strNombre_Dimension = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions='ga:campaign';
			$strSort='campaign';
			$strNombre_Dimension = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions='ga:deviceCategory';
			$strSort='deviceCategory';
			$strNombre_Dimension = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions='ga:browser';
			$strSort='browser';
			$strNombre_Dimension = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions='ga:userGender';
			$strSort='userGender';
			$strNombre_Dimension = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions='ga:userAgeBracket';
			$strSort='userAgeBracket';
			$strNombre_Dimension = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions='ga:country,ga:countryIsoCode';
			$strSort='country';
			$strNombre_Dimension = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions='ga:region';
			$strSort='region';
			$strNombre_Dimension = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions='ga:city';
			$strSort='city';
			$strNombre_Dimension = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			$strNombre_Dimension = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			$strNombre_Dimension = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			$strNombre_Dimension = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			$strNombre_Dimension = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			$strNombre_Dimension = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			$strNombre_Dimension = $trans->__('Día', false);
			break;  
	}//Cierre switch 	

		//Preparación de la dimensión a utilizar (Perspectiva)
	if($perspectiva_seg!=""){
	switch (strtoupper($perspectiva_seg)) {

		/*ECOMMERCE*/
		case "CODPEDIDO":
			$strDimensions_seg='ga:transactionId ';
			$strSort_seg='transactionId';
			$strNombre_Dimension_seg = $trans->__('Código pedido', false);
			break; 
		case "DIASTRANSACCION":
			$strDimensions_seg='ga:daysToTransaction ';
			$strSort_seg='daysToTransaction ';
			$strNombre_Dimension_seg = $trans->__('Días para la transacción', false);
			break;
		case "SESIONESTRANSACCION":
			$strDimensions_seg='ga:sessionsToTransaction';
			$strSort_seg='sessionsToTransaction';
			$strNombre_Dimension_seg = $trans->__('Sesiones para la transacción', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions_seg='ga:source';
			$strSort_seg='source';
			$strNombre_Dimension_seg = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions_seg='ga:medium';
			$strSort_seg='medium';
			$strNombre_Dimension_seg = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions_seg='ga:campaign';
			$strSort_seg='campaign';
			$strNombre_Dimension_seg = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions_seg='ga:deviceCategory';
			$strSort_seg='deviceCategory';
			$strNombre_Dimension_seg = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions_seg='ga:browser';
			$strSort_seg='browser';
			$strNombre_Dimension_seg = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions_seg='ga:userGender';
			$strSort_seg='userGender';
			$strNombre_Dimension_seg = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions_seg='ga:userAgeBracket';
			$strSort_seg='userAgeBracket';
			$strNombre_Dimension_seg = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions_seg='ga:country,ga:countryIsoCode';
			$strSort_seg='country';
			$strNombre_Dimension_seg = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions_seg='ga:region';
			$strSort_seg='region';
			$strNombre_Dimension_seg = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions_seg='ga:city';
			$strSort_seg='city';
			$strNombre_Dimension_seg = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions_seg='ga:yearWeek';
			$strSort_seg='yearWeek';
			$strNombre_Dimension_seg = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions_seg='ga:yearMonth';
			$strSort_seg='yearMonth';
			$strNombre_Dimension_seg = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions_seg='ga:year';
			$strSort_seg='year';
			$strNombre_Dimension_seg = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions_seg='ga:hour';
			$strSort_seg='hour';
			$strNombre_Dimension_seg = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions_seg='ga:dayOfWeek';
			$strSort_seg='dayOfWeek';
			$strNombre_Dimension_seg = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions_seg='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort_seg='nthDay';
			$strNombre_Dimension_seg = $trans->__('Día', false);
			break;  
	}//Cierre switch 	
	}else{
		$strDimensions_seg='';
		$strSort_seg='';
		$strNombre_Dimension_seg = '';
	}

	if($perspectiva_seg != ""){
		$strDimensions .=  ",".$strDimensions_seg;
	}

	//return $strDimensions .=  ",".$perspectiva_seg;
	$strTipo = strtolower($perspectiva);
	$strTipo_seg = strtolower($perspectiva_seg);

	//Preparación de la ordenación
	$array_Ordenacion = array($strSort,$strSort_seg,'transactionRevenue','','transactions','itemQuantity','','');
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:transactionRevenue, ga:transactions, ga:itemQuantity, ga:sessions';
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------


    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_ant -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt ;
    $obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt ;
    $obj_Datos_Api_ant -> metrics              = 'ga:transactionRevenue, ga:transactions, ga:itemQuantity, ga:sessions';
    if(empty($filtro)){
	    $obj_Datos_Api_ant -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api_ant -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api_ant -> Construccion();

    $array_ant = array();
 	for ($i=1; $i<=$obj_Datos_Api_ant->NumValores(); $i++) {
 
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["perspectiva"]     = $obj_Datos_Api_ant->Valor($strSort,$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["perspectiva_seg"] = $obj_Datos_Api_ant->Valor($strSort_seg,$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["ingresobruto"]    = $obj_Datos_Api_ant->Valor("transactionRevenue",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["ingresoneto"]     = $obj_Datos_Api_ant->Valor("transactionRevenue",$i)/100;
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["transactions"]    = $obj_Datos_Api_ant->Valor("transactions",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["itemQuantity"]    = $obj_Datos_Api_ant->Valor("itemQuantity",$i);
 		if($obj_Datos_Api->Valor("sessions",$i) != 0){
 			$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["tasaconversion"]  = ($obj_Datos_Api_ant->Valor("transactions",$i)*100)/$obj_Datos_Api->Valor("sessions",$i);
 		}else{
 			$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["tasaconversion"]  = "0";
 		}	
 		if($obj_Datos_Api->Valor("transactions",$i) != 0){
 			$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["pedidomedio"]     = $obj_Datos_Api_ant->Valor("transactionRevenue",$i)/$obj_Datos_Api->Valor("transactions",$i);
 		}else{
 			$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["pedidomedio"]     = "0";
 		}
 		

 	}

	// Construimos un json con los datos

	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .','	;
	$strA .= '"data":[';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		$strA .= '[';
		//El primer valor va a depender de la perspectiva que estamos pintando
		if($strTipo=="pais"){
			$strA .= '"<span class=\'izquierda_pri\'>'.Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x)).'</span>"';
		}else if($strTipo=="idiomauser" || $strTipo=="idiomanav"){
			$strA .= '"<span class=\'izquierda_pri\'>'.traducir_idioma($obj_Datos_Api->Valor($strSort,$x)).'</span>"';
		}else if($strTipo=="interes" || $strTipo=="region" || $strTipo=="ciudad"/**/){
			
			$strA .= '"<span class=\'izquierda_pri\'>'.traduce_de_ingles($obj_Datos_Api->Valor($strSort,$x),$_COOKIE["idioma_usuario"]).'</span>"';
		}else if($strTipo=="terminos"){
			$strA .= '"<span class=\'izquierda_pri\'>'.$obj_Datos_Api->Valor($strSort,$x).'</span>"';
		}else{
			$strA .= '"<span class=\'izquierda_pri\'>'.Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api).'</span>"';
		}
		$strA .= ",";

		if($perspectiva_seg != ""){
			if($strTipo_seg=="pais"){
				$strA .= '"<span class=\'izquierda\'>'.Formato_Perspectiva('table',$strTipo_seg,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x)).'</span>"';
			}else if($strTipo_seg=="idiomauser" || $strTipo_seg=="idiomanav"){
				$strA .= '"<span class=\'izquierda\'>'.traducir_idioma($obj_Datos_Api->Valor($strSort_seg,$x)).'</span>"';
			}else if($strTipo_seg=="interes" || $strTipo_seg=="region" || $strTipo_seg=="ciudad"/**/){
				
				$strA .= '"<span class=\'izquierda\'>'.traduce_de_ingles($obj_Datos_Api->Valor($strSort_seg,$x),$_COOKIE["idioma_usuario"]).'</span>"';
			}else if($strTipo_seg=="terminos"){
				$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->Valor($strSort_seg,$x).'</span>"';
			}else{
				$strA .= '"<span class=\'izquierda\'>'.Formato_Perspectiva('table',$strTipo_seg,$x,$obj_Datos_Api).'</span>"';
			}
			$strA .= ",";
		}else{
			$strA .= '"",';
		}

		/*Buscamos los anteriores*/
		$ingresobruto_ant     = "0";
		$ingresoneto_ant  = "0";
		$transactions_ant      = "0";
		$itemQuantity_ant   = "0";
		$tasaconversion_ant = "0";
		$pedidomedio_ant = "0";

		if(isset($array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["perspectiva"])){
			$ingresobruto_ant     = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["ingresobruto"];
			$ingresoneto_ant      = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["ingresoneto"];
			$transactions_ant     = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["transactions"];
			$itemQuantity_ant     = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["itemQuantity"];
			$tasaconversion_ant   = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["tasaconversion"];
			$pedidomedio_ant 	  = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["pedidomedio"];
		}

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("transactionRevenue",$x),$ingresobruto_ant ,number_format( $obj_Datos_Api->Valor("transactionRevenue",$x)  ,2,',','.'),number_format( $ingresobruto_ant ,2,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("transactionRevenue",$x)  ,2,',','.').'€</span>",';

		$img_transactionRevenue = DevolverContenido_Total(($obj_Datos_Api->Valor("transactionRevenue",$x)/100), $ingresoneto_ant, number_format( ($obj_Datos_Api->Valor("transactionRevenue",$x)/100) ,2,',','.'),number_format( $ingresoneto_ant ,2,',','.'));		
		$strA .= '"'.$img_transactionRevenue .number_format( ($obj_Datos_Api->Valor("transactionRevenue",$x)/100) ,2,',','.').'€",';//falta multiplicar por margenes
		
		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("transactions",$x),$transactions_ant,number_format( $obj_Datos_Api->Valor("transactions",$x)  ,2,',','.'),number_format( $transactions_ant ,2,',','.'));
		$strA .= '"'.$img_transactionRevenue.$obj_Datos_Api->ValorF("transactions",$x).'",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("itemQuantity",$x),$itemQuantity_ant ,number_format( $obj_Datos_Api->Valor("itemQuantity",$x)  ,2,',','.'),number_format( $itemQuantity_ant   ,2,',','.'));
		$strA .= '"'.$img_transactionRevenue.$obj_Datos_Api->ValorF("itemQuantity",$x).'",';

		if($obj_Datos_Api->Valor("transactions",$x)==0 || $obj_Datos_Api->Valor("sessions",$x) == 0){
			$img_transactionRevenue = DevolverContenido_Total(0, $tasaconversion_ant, 0, number_format( $tasaconversion_ant ,2,',','.') );						
			$strA .= '"'.$img_transactionRevenue.'0%",';
		}else{
			$img_transactionRevenue = DevolverContenido_Total((($obj_Datos_Api->Valor("transactions",$x)*100)/$obj_Datos_Api->Valor("sessions",$x)), $tasaconversion_ant, number_format( (($obj_Datos_Api->Valor("transactions",$x)*100)/$obj_Datos_Api->Valor("sessions",$x)) ,2,',','.'),number_format( $tasaconversion_ant ,2,',','.'));						
			$strA .= '"'.$img_transactionRevenue. number_format( (($obj_Datos_Api->Valor("transactions",$x)*100)/$obj_Datos_Api->Valor("sessions",$x)) ,2,',','.') .'%",';
		}

		//PEDIDO MEDIO
		if($obj_Datos_Api->Valor("transactionRevenue",$x) == 0 || $obj_Datos_Api->Valor("sessions",$x) == 0 ){
			$img_transactionRevenue = DevolverContenido_Total(0, $pedidomedio_ant , 0, number_format( $pedidomedio_ant ,2,',','.') );						
			$strA .= '"'.$img_transactionRevenue.'0€"';
		}else{
			$img_transactionRevenue = DevolverContenido_Total(($obj_Datos_Api->Valor("transactionRevenue",$x)/$obj_Datos_Api->Valor("transactions",$x)), $pedidomedio_ant , number_format( ($obj_Datos_Api->Valor("transactionRevenue",$x)/$obj_Datos_Api->Valor("transactions",$x)) ,2,',','.'),number_format( $pedidomedio_ant ,2,',','.'));		
			$strA .= '"'.$img_transactionRevenue. number_format( ($obj_Datos_Api->Valor("transactionRevenue",$x)/$obj_Datos_Api->Valor("transactions",$x)) ,2,',','.') .'€"';
		}
		$strA .= ']';	  
		//Añadimos una "," si no es el último registro a pintar
		if ($x < $obj_Datos_Api->NumValores()) {
			$strA .= ','; 
		}
	}
	$strA .= ']}';
	
	//Devolvemos el json
	return $strA;
}

function campanas_informes_pedidos_extra($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro){

	global $trans;

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	//TOTALES PARA sesiones
	$obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api -> metrics              = 'ga:transactionRevenue, ga:transactions, ga:itemQuantity, ga:sessions';
	if(empty($filtro)){
		$obj_Datos_Api -> optParams    = array(
										'dimensions' => '',
										'start-index' => 1,
		                                'max-results' => 1);  
	}else{
		$obj_Datos_Api -> optParams    = array(
										'dimensions' => '',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 1);  
	}
	$obj_Datos_Api -> Construccion();


	
	//De Aquí hay que coger Descarga, Buscador y Externo
	$total_ibruto 	   = 0;
	$total_ineto  	   = 0;
	$total_npedidos    = 0;
	$total_nproductos  = 0;
	$total_tconversion = 0;
	$total_pmedio      = 0;

	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {

		$total_ibruto      = $obj_Datos_Api->Valor("transactionRevenue",$x);
		$total_ineto       = $obj_Datos_Api->Valor("transactionRevenue",$x)/100;
		$total_npedidos    = $obj_Datos_Api->Valor("transactions",$x);
		$total_nproductos  = $obj_Datos_Api->Valor("itemQuantity",$x);
		$total_tconversion = ($obj_Datos_Api->Valor("transactions",$x)*100)/$obj_Datos_Api->Valor("sessions",$x);
		$total_pmedio      = $obj_Datos_Api->Valor("transactionRevenue",$x)/$obj_Datos_Api->Valor("transactions",$x);

	}

	$strA = '{';
	
	$strA .= '"total_ibruto": "' 	  . $total_ibruto	   . '",';
	$strA .= '"total_ineto": "' 	  . $total_ineto 	   . '",';
	$strA .= '"total_npedidos": "' 	  . $total_npedidos	   . '",';
	$strA .= '"total_nproductos": "'  . $total_nproductos  . '",';
	$strA .= '"total_tconversion": "' . $total_tconversion . '",';
	$strA .= '"total_pmedio": "' 	  . $total_pmedio	   . '",';	
	$strA .= '"total_ibrutoF": "' 	  . number_format( $total_ibruto ,2,',','.')	   . '",';
	$strA .= '"total_inetoF": "' 	  . number_format( $total_ineto ,2,',','.')	  	   . '",';
	$strA .= '"total_npedidosF": "'   . number_format( $total_npedidos ,0,',','.')	   . '",';
	$strA .= '"total_nproductosF": "' . number_format( $total_nproductos ,0,',','.')   . '",';
	$strA .= '"total_tconversionF": "'. number_format( $total_tconversion ,2,',','.')  . '",';
	$strA .= '"total_pmedioF": "' 	  . number_format( $total_pmedio ,2,',','.')	   . '"';	 
	
	$strA .= '}';
	return $strA;
}

function datatable_informes_productos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro, $perspectiva, $perspectiva_seg)
{

	global $trans;
	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($perspectiva)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions='ga:ProductName  ';
			$strSort='ProductName ';
			$strNombre_Dimension = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions='ga:productCategoryLevel1';
			$strSort='productCategoryLevel1';
			$strNombre_Dimension = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions='ga:productCategoryLevel2';
			$strSort='productCategoryLevel2';
			$strNombre_Dimension = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions='ga:productBrand';
			$strSort='productBrand';
			$strNombre_Dimension = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions='ga:productListName';
			$strSort='productListName';
			$strNombre_Dimension = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions='ga:source';
			$strSort='source';
			$strNombre_Dimension = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions='ga:medium';
			$strSort='medium';
			$strNombre_Dimension = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions='ga:campaign';
			$strSort='campaign';
			$strNombre_Dimension = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions='ga:deviceCategory';
			$strSort='deviceCategory';
			$strNombre_Dimension = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions='ga:browser';
			$strSort='browser';
			$strNombre_Dimension = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions='ga:userGender';
			$strSort='userGender';
			$strNombre_Dimension = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions='ga:userAgeBracket';
			$strSort='userAgeBracket';
			$strNombre_Dimension = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions='ga:country,ga:countryIsoCode';
			$strSort='country';
			$strNombre_Dimension = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions='ga:region';
			$strSort='region';
			$strNombre_Dimension = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions='ga:city';
			$strSort='city';
			$strNombre_Dimension = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			$strNombre_Dimension = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			$strNombre_Dimension = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			$strNombre_Dimension = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			$strNombre_Dimension = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			$strNombre_Dimension = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			$strNombre_Dimension = $trans->__('Día', false);
			break;  
	}//Cierre switch 	

		//Preparación de la dimensión a utilizar (Perspectiva)
	if($perspectiva_seg!=""){
	switch (strtoupper($perspectiva_seg)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions_seg='ga:ProductName  ';
			$strSort_seg='ProductName';
			$strNombre_Dimension_seg = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions_seg='ga:productCategoryLevel1  ';
			$strSort_seg='productCategoryLevel1';
			$strNombre_Dimension_seg = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions_seg='ga:productCategoryLevel2';
			$strSort_seg='productCategoryLevel2';
			$strNombre_Dimension_seg = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions_seg='ga:productBrand';
			$strSort_seg='productBrand';
			$strNombre_Dimension_seg = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions_seg='ga:productListName';
			$strSort_seg='productListName';
			$strNombre_Dimension_seg = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions_seg='ga:source';
			$strSort_seg='source';
			$strNombre_Dimension_seg = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions_seg='ga:medium';
			$strSort_seg='medium';
			$strNombre_Dimension_seg = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions_seg='ga:campaign';
			$strSort_seg='campaign';
			$strNombre_Dimension_seg = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions_seg='ga:deviceCategory';
			$strSort_seg='deviceCategory';
			$strNombre_Dimension_seg = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions_seg='ga:browser';
			$strSort_seg='browser';
			$strNombre_Dimension_seg = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions_seg='ga:userGender';
			$strSort_seg='userGender';
			$strNombre_Dimension_seg = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions_seg='ga:userAgeBracket';
			$strSort_seg='userAgeBracket';
			$strNombre_Dimension_seg = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions_seg='ga:country,ga:countryIsoCode';
			$strSort_seg='country';
			$strNombre_Dimension_seg = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions_seg='ga:region';
			$strSort_seg='region';
			$strNombre_Dimension_seg = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions_seg='ga:city';
			$strSort_seg='city';
			$strNombre_Dimension_seg = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions_seg='ga:yearWeek';
			$strSort_seg='yearWeek';
			$strNombre_Dimension_seg = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions_seg='ga:yearMonth';
			$strSort_seg='yearMonth';
			$strNombre_Dimension_seg = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions_seg='ga:year';
			$strSort_seg='year';
			$strNombre_Dimension_seg = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions_seg='ga:hour';
			$strSort_seg='hour';
			$strNombre_Dimension_seg = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions_seg='ga:dayOfWeek';
			$strSort_seg='dayOfWeek';
			$strNombre_Dimension_seg = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions_seg='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort_seg='nthDay';
			$strNombre_Dimension_seg = $trans->__('Día', false);
			break;  
	}//Cierre switch 	
	}else{
		$strDimensions_seg='';
		$strSort_seg='';
		$strNombre_Dimension_seg = '';
	}

	if($perspectiva_seg != ""){
		$strDimensions .=  ",".$strDimensions_seg;
	}

	//return $strDimensions .=  ",".$perspectiva_seg;
	$strTipo = strtolower($perspectiva);
	$strTipo_seg = strtolower($perspectiva_seg);

	//Preparación de la ordenación
	$array_Ordenacion = array($strSort,$strSort_seg,'itemQuantity','uniquePurchases ','itemRevenue','revenuePerItem','itemsPerPurchase ');
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$filtro="ga:itemQuantity>0";
	if($paisGa!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:itemQuantity, ga:uniquePurchases, ga:itemRevenue, ga:revenuePerItem, ga:itemsPerPurchase';
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------


    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_ant -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt ;
    $obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt ;
    $obj_Datos_Api_ant -> metrics              = 'ga:itemQuantity, ga:uniquePurchases, ga:itemRevenue, ga:revenuePerItem, ga:itemsPerPurchase';
    if(empty($filtro)){
	    $obj_Datos_Api_ant -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api_ant -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api_ant -> Construccion();

    $array_ant = array();
 	for ($i=1; $i<=$obj_Datos_Api_ant->NumValores(); $i++) {
 
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["perspectiva"]      = $obj_Datos_Api_ant->Valor($strSort,$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["perspectiva_seg"]  = $obj_Datos_Api_ant->Valor($strSort_seg,$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["itemQuantity"]     = $obj_Datos_Api_ant->Valor("itemQuantity",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["itemRevenue"]      = $obj_Datos_Api_ant->Valor("itemRevenue",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["revenuePerItem"]   = $obj_Datos_Api_ant->Valor("revenuePerItem",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["itemsPerPurchase"] = $obj_Datos_Api_ant->Valor("itemsPerPurchase",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["uniquePurchases"]  = $obj_Datos_Api_ant->Valor("uniquePurchases",$i);

 	}

	// Construimos un json con los datos

	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .','	;
	$strA .= '"data":[';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		$strA .= '[';
		//El primer valor va a depender de la perspectiva que estamos pintando
		if($strTipo=="pais"){
			$strA .= '"<span class=\'izquierda_pri\'>'.Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x)).'</span>"';
		}else if($strTipo=="idiomauser" || $strTipo=="idiomanav"){
			$strA .= '"<span class=\'izquierda_pri\'>'.traducir_idioma($obj_Datos_Api->Valor($strSort,$x)).'</span>"';
		}else if($strTipo=="interes" || $strTipo=="region" || $strTipo=="ciudad"/**/){
			
			$strA .= '"<span class=\'izquierda_pri\'>'.traduce_de_ingles($obj_Datos_Api->Valor($strSort,$x),$_COOKIE["idioma_usuario"]).'</span>"';
		}else if($strTipo=="terminos"){
			$strA .= '"<span class=\'izquierda_pri\'>'.$obj_Datos_Api->Valor($strSort,$x).'</span>"';
		}else{
			$strA .= '"<span class=\'izquierda_pri\'>'.Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api).'</span>"';
		}
		$strA .= ",";

		if($perspectiva_seg != ""){
			if($strTipo_seg=="pais"){
				$strA .= '"<span class=\'izquierda\'>'.Formato_Perspectiva('table',$strTipo_seg,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x)).'</span>"';
			}else if($strTipo_seg=="idiomauser" || $strTipo_seg=="idiomanav"){
				$strA .= '"<span class=\'izquierda\'>'.traducir_idioma($obj_Datos_Api->Valor($strSort_seg,$x)).'</span>"';
			}else if($strTipo_seg=="interes" || $strTipo_seg=="region" || $strTipo_seg=="ciudad"/**/){
				
				$strA .= '"<span class=\'izquierda\'>'.traduce_de_ingles($obj_Datos_Api->Valor($strSort_seg,$x),$_COOKIE["idioma_usuario"]).'</span>"';
			}else if($strTipo_seg=="terminos"){
				$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->Valor($strSort_seg,$x).'</span>"';
			}else{
				$strA .= '"<span class=\'izquierda\'>'.Formato_Perspectiva('table',$strTipo_seg,$x,$obj_Datos_Api).'</span>"';
			}
			$strA .= ",";
		}else{
			$strA .= '"",';
		}

		/*Buscamos los anteriores*/
		$itemQuantity_ant     = "0";
		$uniquePurchases_ant  = "0";
		$itemRevenue_ant      = "0";
		$revenuePerItem_ant   = "0";
		$itemsPerPurchase_ant = "0";

		if(isset($array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["perspectiva"])){
			$itemQuantity_ant     = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["itemQuantity"];
			$uniquePurchases_ant  = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["uniquePurchases"];
			$itemRevenue_ant      = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["itemRevenue"];
			$revenuePerItem_ant   = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["revenuePerItem"];
			$itemsPerPurchase_ant = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["itemsPerPurchase"];
		}


		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("itemQuantity",$x),$itemQuantity_ant ,number_format( $obj_Datos_Api->Valor("itemQuantity",$x)  ,0,',','.'),number_format( $itemQuantity_ant   ,0,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("itemQuantity",$x)  ,0,',','.').'</span>",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("uniquePurchases",$x),$uniquePurchases_ant ,number_format( $obj_Datos_Api->Valor("uniquePurchases",$x)  ,0,',','.'),number_format( $uniquePurchases_ant   ,0,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("uniquePurchases",$x)  ,0,',','.').'</span>",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("itemRevenue",$x),$itemRevenue_ant,number_format( $obj_Datos_Api->Valor("itemRevenue",$x)  ,2,',','.'),number_format( $itemRevenue_ant ,2,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("itemRevenue",$x)  ,2,',','.').'€</span>",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("revenuePerItem",$x),$revenuePerItem_ant,number_format( $obj_Datos_Api->Valor("revenuePerItem",$x)  ,2,',','.'),number_format( $revenuePerItem_ant  ,2,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("revenuePerItem",$x)  ,2,',','.').'€</span>",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("itemsPerPurchase",$x),$itemsPerPurchase_ant,number_format( $obj_Datos_Api->Valor("itemsPerPurchase",$x)  ,0,',','.'),number_format( $itemsPerPurchase_ant  ,0,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("itemsPerPurchase",$x)  ,0,',','.').'</span>"';


		$strA .= ']';	  
		//Añadimos una "," si no es el último registro a pintar
		if ($x < $obj_Datos_Api->NumValores()) {
			$strA .= ','; 
		}
	}
	$strA .= ']}';
	
	//Devolvemos el json
	return $strA;
}

function campanas_informes_productos_extra($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro){

	global $trans;

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	//Sacamos los dos mayores productos 
	$obj_Datos_Api_productos = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_productos -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_productos -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_productos -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_productos -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_productos -> metrics              = 'ga:itemRevenue';
	if(empty($filtro)){
		$obj_Datos_Api_productos -> optParams    = array(
										'dimensions' => 'ga:ProductName',
										'sort' => '-ga:itemRevenue',
										'start-index' => 1,
		                                'max-results' => 2);  
	}else{
		$obj_Datos_Api_productos -> optParams    = array(
										'dimensions' => 'ga:ProductName',
										'sort' => '-ga:itemRevenue',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 2);  
	}
	$obj_Datos_Api_productos -> Construccion();

	$pri = 0;

	for ($x=1; $x<=$obj_Datos_Api_productos->NumValores(); $x++) {

		if($pri==0){
			$resumen_nombre_producto1 = $obj_Datos_Api_productos->Valor("ProductName",$x);
			$resumen_producto1 		  = $obj_Datos_Api_productos->Valor("itemRevenue",$x);
			$pri++;
		}else{
			$resumen_nombre_producto2 = $obj_Datos_Api_productos->Valor("ProductName",$x);
			$resumen_producto2 		  = $obj_Datos_Api_productos->Valor("itemRevenue",$x);
		}
	}

    //Sacamos las dos mayores marcas 
	$obj_Datos_Api_marcas = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_marcas -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_marcas -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_marcas -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_marcas -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_marcas -> metrics              = 'ga:itemRevenue';
	if(empty($filtro)){
		$obj_Datos_Api_marcas -> optParams    = array(
										'dimensions' => 'ga:ProductBrand',
										'sort' => '-ga:itemRevenue',
										'start-index' => 1,
		                                'max-results' => 2);  
	}else{
		$obj_Datos_Api_marcas -> optParams    = array(
										'dimensions' => 'ga:ProductBrand',
										'sort' => '-ga:itemRevenue',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 2);  
	}
	$obj_Datos_Api_marcas -> Construccion();

	$pri = 0;

	for ($x=1; $x<=$obj_Datos_Api_marcas->NumValores(); $x++) {

		if($pri==0){
			$resumen_nombre_marca1 = $obj_Datos_Api_marcas->Valor("ProductBrand",$x);
			$resumen_marca1 	   = $obj_Datos_Api_marcas->Valor("itemRevenue",$x);
			$pri++;
		}else{
			$resumen_nombre_marca2 = $obj_Datos_Api_marcas->Valor("ProductBrand",$x);
			$resumen_marca2 	   = $obj_Datos_Api_marcas->Valor("itemRevenue",$x);
		}
	}

	
	//Sacamos las dos mayores listas 
	$filtro_lista = $filtro;
	if(empty($filtro_lista)){
		$filtro_lista = 'ga:productListName!=(not set)';
	}else{
		$filtro_lista .= ',ga:productListName!=(not set)';
	}

	$obj_Datos_Api_listas = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_listas -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_listas -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_listas -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_listas -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_listas -> metrics              = 'ga:itemRevenue';
	$obj_Datos_Api_listas -> optParams    = array(
												'dimensions' => 'ga:ProductListName',
												'sort' => '-ga:itemRevenue',
												'filters' => $filtro_lista,
												'start-index' => 1,
		                                		'max-results' => 2);  

	$obj_Datos_Api_listas -> Construccion();

	$pri = 0;

	for ($x=1; $x<=$obj_Datos_Api_listas->NumValores(); $x++) {

		if($pri==0){
			$resumen_nombre_listas1 = $obj_Datos_Api_listas->Valor("ProductListName",$x);
			$resumen_listas1 	    = $obj_Datos_Api_listas->Valor("itemRevenue",$x);
			$pri++;
		}else{
			$resumen_nombre_listas2 = $obj_Datos_Api_listas->Valor("ProductListName",$x);
			$resumen_listas2 	    = $obj_Datos_Api_listas->Valor("itemRevenue",$x);
		}
	}


	//Sacamos las dos mayores categorias 
	$obj_Datos_Api_categoria1 = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_categoria1 -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_categoria1 -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_categoria1 -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_categoria1 -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_categoria1 -> metrics              = 'ga:itemRevenue';
	if(empty($filtro)){
		$obj_Datos_Api_categoria1 -> optParams    = array(
										'dimensions' => 'ga:productCategoryLevel1',
										'sort' => '-ga:itemRevenue',
										'start-index' => 1,
		                                'max-results' => 1);  
	}else{
		$obj_Datos_Api_categoria1 -> optParams    = array(
										'dimensions' => 'ga:productCategoryLevel1',
										'sort' => '-ga:itemRevenue',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 1);  
	}
	$obj_Datos_Api_categoria1 -> Construccion();



	for ($x=1; $x<=$obj_Datos_Api_categoria1->NumValores(); $x++) {
			$resumen_nombre_categoria1 = $obj_Datos_Api_categoria1->Valor("productCategoryLevel1",$x);
			$resumen_categoria1 	   = $obj_Datos_Api_categoria1->Valor("itemRevenue",$x);
	}

	$obj_Datos_Api_categoria2 = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_categoria2 -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_categoria2 -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_categoria2 -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_categoria2 -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_categoria2 -> metrics              = 'ga:itemRevenue';
	if(empty($filtro)){
		$obj_Datos_Api_categoria2 -> optParams    = array(
												 'dimensions' => 'ga:productCategoryLevel2',
												 'sort' => '-ga:itemRevenue',
												 'start-index' => 1,
		                                		 'max-results' => 1);  
	}else{
		$obj_Datos_Api_categoria2 -> optParams    = array(
												 'dimensions' => 'ga:productCategoryLevel2',
												 'sort' => '-ga:itemRevenue',
												 'filters' => $filtro,
												 'start-index' => 1,
		                                		 'max-results' => 1);  
	}
	$obj_Datos_Api_categoria2 -> Construccion();



	for ($x=1; $x<=$obj_Datos_Api_categoria2->NumValores(); $x++) {
			$resumen_nombre_categoria2 = $obj_Datos_Api_categoria2->Valor("productCategoryLevel2",$x);
			$resumen_categoria2 	   = $obj_Datos_Api_categoria2->Valor("itemRevenue",$x);
	}

	//TOTALES PARA sesiones
	$obj_Datos_Api_total = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_total -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_total -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_total -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_total -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_total -> metrics              = 'ga:itemQuantity, ga:uniquePurchases, ga:itemRevenue, ga:revenuePerItem, ga:itemsPerPurchase';
	if(empty($filtro)){
		$obj_Datos_Api_total -> optParams    = array(
										'dimensions' => '',
										'start-index' => 1,
		                                'max-results' => 1);  
	}else{
		$obj_Datos_Api_total -> optParams    = array(
										'dimensions' => '',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 1);  
	}
	$obj_Datos_Api_total -> Construccion();


	
	//De Aquí hay que coger Descarga, Buscador y Externo
	$total_ibruto 	   = 0;
	$total_ineto  	   = 0;
	$total_npedidos    = 0;
	$total_nproductos  = 0;
	$total_tconversion = 0;
	$total_pmedio      = 0;

	for ($x=1; $x<=$obj_Datos_Api_total->NumValores(); $x++) {

		$total_nv   = $obj_Datos_Api_total->Valor("itemQuantity",$x);
		$total_nuv  = $obj_Datos_Api_total->Valor("uniquePurchases",$x);
		$total_tv   = $obj_Datos_Api_total->Valor("itemRevenue",$x);
		$total_pmpv = $obj_Datos_Api_total->Valor("revenuePerItem",$x);
		$total_mpp  = $obj_Datos_Api_total->Valor("itemsPerPurchase",$x);

	}


	$strA = '{';
	
	$strA .= '"total_nv": "' 	  . $total_nv	. '",';
	$strA .= '"total_nuv": "' 	  . $total_nuv 	. '",';
	$strA .= '"total_tv": "' 	  . $total_tv	. '",';
	$strA .= '"total_pmpv": "'    . $total_pmpv . '",';
	$strA .= '"total_mpp": "'     . $total_mpp  . '",';

	$strA .= '"total_nvF": "' 	  . number_format( $total_nv ,0,',','.')   . '",';
	$strA .= '"total_nuvF": "' 	  . number_format( $total_nuv ,0,',','.')  . '",';
	$strA .= '"total_tvF": "'     . number_format( $total_tv ,2,',','.')   . '",';
	$strA .= '"total_pmpvF": "'   . number_format( $total_pmpv ,2,',','.') . '",';
	$strA .= '"total_mppF": "'    . number_format( $total_mpp ,0,',','.')  . '",';

	$strA .= '"resumen_producto1": "'  . $resumen_producto1	 . '",';
	$strA .= '"resumen_producto2": "'  . $resumen_producto2  . '",';
	$strA .= '"resumen_marca1": "' 	   . $resumen_marca1	 . '",';
	$strA .= '"resumen_marca2": "'     . $resumen_marca2     . '",';
	$strA .= '"resumen_categoria1": "' . $resumen_categoria1 . '",';
	$strA .= '"resumen_categoria2": "' . $resumen_categoria2 . '",';
	$strA .= '"resumen_listas1": "'    . $resumen_listas1    . '",';
	$strA .= '"resumen_listas2": "'    . $resumen_listas2    . '",';

	$strA .= '"resumen_producto1F": "'  . number_format( $resumen_producto1 ,2,',','.')	 . '",';
	$strA .= '"resumen_producto2F": "' 	. number_format( $resumen_producto2 ,2,',','.')	 . '",';
	$strA .= '"resumen_marca1F": "'     . number_format( $resumen_marca1 ,0,',','.')	 . '",';
	$strA .= '"resumen_marca2F": "'     . number_format( $resumen_marca2 ,0,',','.')     . '",';
	$strA .= '"resumen_categoria1F": "' . number_format( $resumen_categoria1 ,2,',','.') . '",';
	$strA .= '"resumen_categoria2F": "' . number_format( $resumen_categoria2 ,0,',','.') . '",';
	$strA .= '"resumen_listas1F": "'    . number_format( $resumen_listas1 ,0,',','.')    . '",';
	$strA .= '"resumen_listas2F": "'    . number_format( $resumen_listas2 ,2,',','.')    . '",';

	$strA .= '"resumen_nombre_producto1": "'  . $resumen_nombre_producto1  . '",';
	$strA .= '"resumen_nombre_producto2": "'  . $resumen_nombre_producto2  . '",';
	$strA .= '"resumen_nombre_marca1": "'     . $resumen_nombre_marca1     . '",';
	$strA .= '"resumen_nombre_marca2": "'     . $resumen_nombre_marca2     . '",';
	$strA .= '"resumen_nombre_categoria1": "' . $resumen_nombre_categoria1 . '",';
	$strA .= '"resumen_nombre_categoria2": "' . $resumen_nombre_categoria2 . '",';
	$strA .= '"resumen_nombre_listas1": "'    . $resumen_nombre_listas1    . '",';
	$strA .= '"resumen_nombre_listas2": "'    . $resumen_nombre_listas2    . '"';



	
	$strA .= '}';
	return $strA;
}

function datatable_informes_embudos_pedidos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro, $perspectiva, $perspectiva_seg)
{

	global $trans;
	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($perspectiva)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions='ga:ProductName  ';
			$strSort='ProductName ';
			$strNombre_Dimension = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions='ga:productCategoryLevel1';
			$strSort='productCategoryLevel1';
			$strNombre_Dimension = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions='ga:productCategoryLevel2';
			$strSort='productCategoryLevel2';
			$strNombre_Dimension = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions='ga:productBrand';
			$strSort='productBrand';
			$strNombre_Dimension = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions='ga:productListName';
			$strSort='productListName';
			$strNombre_Dimension = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions='ga:source';
			$strSort='source';
			$strNombre_Dimension = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions='ga:medium';
			$strSort='medium';
			$strNombre_Dimension = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions='ga:campaign';
			$strSort='campaign';
			$strNombre_Dimension = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions='ga:deviceCategory';
			$strSort='deviceCategory';
			$strNombre_Dimension = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions='ga:browser';
			$strSort='browser';
			$strNombre_Dimension = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions='ga:userGender';
			$strSort='userGender';
			$strNombre_Dimension = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions='ga:userAgeBracket';
			$strSort='userAgeBracket';
			$strNombre_Dimension = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions='ga:country, ga:countryIsoCode';
			$strSort='country';
			$strNombre_Dimension = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions='ga:region';
			$strSort='region';
			$strNombre_Dimension = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions='ga:city';
			$strSort='city';
			$strNombre_Dimension = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			$strNombre_Dimension = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			$strNombre_Dimension = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			$strNombre_Dimension = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			$strNombre_Dimension = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			$strNombre_Dimension = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			$strNombre_Dimension = $trans->__('Día', false);
			break;  
	}//Cierre switch 	

		//Preparación de la dimensión a utilizar (Perspectiva)
	if($perspectiva_seg!=""){
	switch (strtoupper($perspectiva_seg)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions_seg='ga:ProductName  ';
			$strSort_seg='ProductName';
			$strNombre_Dimension_seg = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions_seg='ga:productCategoryLevel1  ';
			$strSort_seg='productCategoryLevel1';
			$strNombre_Dimension_seg = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions_seg='ga:productCategoryLevel2';
			$strSort_seg='productCategoryLevel2';
			$strNombre_Dimension_seg = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions_seg='ga:productBrand';
			$strSort_seg='productBrand';
			$strNombre_Dimension_seg = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions_seg='ga:productListName';
			$strSort_seg='productListName';
			$strNombre_Dimension_seg = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions_seg='ga:source';
			$strSort_seg='source';
			$strNombre_Dimension_seg = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions_seg='ga:medium';
			$strSort_seg='medium';
			$strNombre_Dimension_seg = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions_seg='ga:campaign';
			$strSort_seg='campaign';
			$strNombre_Dimension_seg = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions_seg='ga:deviceCategory';
			$strSort_seg='deviceCategory';
			$strNombre_Dimension_seg = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions_seg='ga:browser';
			$strSort_seg='browser';
			$strNombre_Dimension_seg = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions_seg='ga:userGender';
			$strSort_seg='userGender';
			$strNombre_Dimension_seg = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions_seg='ga:userAgeBracket';
			$strSort_seg='userAgeBracket';
			$strNombre_Dimension_seg = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions_seg='ga:country,ga:countryIsoCode';
			$strSort_seg='country';
			$strNombre_Dimension_seg = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions_seg='ga:region';
			$strSort_seg='region';
			$strNombre_Dimension_seg = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions_seg='ga:city';
			$strSort_seg='city';
			$strNombre_Dimension_seg = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions_seg='ga:yearWeek';
			$strSort_seg='yearWeek';
			$strNombre_Dimension_seg = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions_seg='ga:yearMonth';
			$strSort_seg='yearMonth';
			$strNombre_Dimension_seg = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions_seg='ga:year';
			$strSort_seg='year';
			$strNombre_Dimension_seg = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions_seg='ga:hour';
			$strSort_seg='hour';
			$strNombre_Dimension_seg = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions_seg='ga:dayOfWeek';
			$strSort_seg='dayOfWeek';
			$strNombre_Dimension_seg = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions_seg='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort_seg='nthDay';
			$strNombre_Dimension_seg = $trans->__('Día', false);
			break;  
	}//Cierre switch 	
	}else{
		$strDimensions_seg='';
		$strSort_seg='';
		$strNombre_Dimension_seg = '';
	}

	if($perspectiva_seg != ""){
		$strDimensions .=  ",".$strDimensions_seg;
	}

	//Creamos una array con llas 7 llamadas para ir descartando las que se van realizando
	$llamadas = array();
	$llamadas[0] = 'ALL_VISITS';
	$llamadas[1] = 'PRODUCT_VIEW';
	$llamadas[2] = 'ADD_TO_CART';
	$llamadas[3] = 'CHECKOUT_1';
	$llamadas[4] = 'CHECKOUT_2';
	$llamadas[5] = 'CHECKOUT_3';
	$llamadas[6] = 'TRANSACTION';

	//return $strDimensions .=  ",".$perspectiva_seg;
	$strTipo = strtolower($perspectiva);
	$strTipo_seg = strtolower($perspectiva_seg);

	//Preparación de la ordenación
	$array_Ordenacion = array($strSort,$strSort_seg,"","ALL_VISITS","PRODUCT_VIEW","ADD_TO_CART","CHECKOUT_1","CHECKOUT_2","CHECKOUT_3","TRANSACTION");
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa.',';
	}
	if($idIdioma!="todos"){
		$filtro .= 'ga:dimension1=='.$idIdioma.',';
	}
	if ($strFiltro!="no"){
		$filtro .= $strFiltro.',';
	}

	//Primera ordenacion
	if($DataTables_OrderColumn == 0 OR $DataTables_OrderColumn == 1 OR $DataTables_OrderColumn == 2){
		$primera_ordenacion = 'ALL_VISITS';
	}else{
		$primera_ordenacion = $array_Ordenacion[$DataTables_OrderColumn];
	}
	

	$filtro_pri = substr($filtro, 0, -1);
	
    //PRIMERA ARRAY PARA LA ORDENACIÓN
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:sessions';
	$obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => 'ga:shoppingStage=='.$primera_ordenacion.$filtro_pri,
	                                         'sort' => $DirOrdenacion.'ga:sessions',
	                                         'sort' => '-ga:sessions',
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------

    //echo $DataTables_length;
    $filtroanadido = "";

    $perspectiva = strtolower($perspectiva);
    $perspectiva_seg = strtolower($perspectiva_seg);

    $array_pri = array();
	for ($i=1; $i<=$obj_Datos_Api->NumValores(); $i++) {

		//Guardamos en una array la información
		if($perspectiva=="pais"){
			//echo Formato_Perspectiva('table',$perspectiva,$i,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$i))." - ";
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = Formato_Perspectiva('table',$perspectiva,$i,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$i));
		}else if($perspectiva=="idiomauser" || $perspectiva=="idiomanav"){
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = traducir_idioma($obj_Datos_Api->Valor($strSort,$i));
		}else if($perspectiva=="interes" || $perspectiva=="region" || $perspectiva=="ciudad"/**/){
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = traduce_de_ingles($obj_Datos_Api->Valor($strSort,$i),$_COOKIE["idioma_usuario"]);
		}else if($perspectiva=="terminos"){
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = $obj_Datos_Api->Valor($strSort,$i);
		}else{
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = Formato_Perspectiva('table',$perspectiva,$i,$obj_Datos_Api);
		}

		if($perspectiva_seg != ""){
			if($perspectiva_seg=="pais"){
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = Formato_Perspectiva('table',$perspectiva_seg,$i,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$i));
			}else if($perspectiva_seg=="idiomauser" || $perspectiva_seg=="idiomanav"){
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = traducir_idioma($obj_Datos_Api->Valor($strSort_seg,$i));
			}else if($perspectiva_seg=="interes" || $perspectiva_seg=="region" || $perspectiva_seg=="ciudad"/**/){
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = traduce_de_ingles($obj_Datos_Api->Valor($strSort_seg,$i),$_COOKIE["idioma_usuario"]);
			}else if($perspectiva_seg=="terminos"){
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = $obj_Datos_Api->Valor($strSort_seg,$i);
			}else{
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = Formato_Perspectiva('table',$perspectiva_seg,$i,$obj_Datos_Api);
			}
		}
	 	
 		$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)][$array_Ordenacion[$DataTables_OrderColumn]] = $obj_Datos_Api->Valor("sessions",$i);
 		$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)][$array_Ordenacion[$DataTables_OrderColumn].'F'] = $obj_Datos_Api->ValorF("sessions",$i);
 		//Creamos un filtro para la segunda array
 		$filtroanadido .= "ga:".$strSort."==".$obj_Datos_Api->Valor($strSort,$i).'';
 		if( !empty( $strDimensions_seg ) ){
 			$filtroanadido .= ','."ga:".$strSort_seg."==".$obj_Datos_Api->Valor($strSort_seg,$i).',';
 		}else{
 			$filtroanadido .= ',';
 		}

	}

	//eliminamos la llamada realizada
	foreach ($llamadas as $key => $ll) {
		if($ll == $primera_ordenacion){
			unset($llamadas[$key]);
		}
	}
	$llamadas = array_values($llamadas);






	//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";

	$filtro_seg .= $filtro;
	$filtro_seg .= $filtroanadido;

	$filtro_seg = substr($filtro_seg, 0, -1);

	
    $obj_Datos_Apib = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apib -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apib -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apib -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apib -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apib -> metrics              = 'ga:sessions';
	$obj_Datos_Apib -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
    $obj_Datos_Apib -> Construccion();
   
	for ($j=1; $j<=$obj_Datos_Apib->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apib->Valor($strSort,$j).$obj_Datos_Apib->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apib->Valor("sessions",$j);
		$array_pri[$obj_Datos_Apib->Valor($strSort,$j).$obj_Datos_Apib->Valor($strSort_seg,$j)][$llamadas[0].'F'] = $obj_Datos_Apib->ValorF("sessions",$j);

 		

	}
	
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);

	//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";



    $obj_Datos_Apic = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apic -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apic -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apic -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apic -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apic -> metrics              = 'ga:sessions';
	$obj_Datos_Apic -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
    $obj_Datos_Apic -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apic->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apic->Valor($strSort,$j).$obj_Datos_Apic->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apic->Valor("sessions",$j);
		$array_pri[$obj_Datos_Apic->Valor($strSort,$j).$obj_Datos_Apic->Valor($strSort_seg,$j)][$llamadas[0].'F'] = $obj_Datos_Apic->ValorF("sessions",$j);
 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]

	unset($llamadas[0]);
	$llamadas = array_values($llamadas);




	//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";


    $obj_Datos_Apid = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apid -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apid -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apid -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apid -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apid -> metrics              = 'ga:sessions';
	$obj_Datos_Apid -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
    $obj_Datos_Apid -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apid->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apid->Valor($strSort,$j).$obj_Datos_Apid->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apid->Valor("sessions",$j);
		$array_pri[$obj_Datos_Apid->Valor($strSort,$j).$obj_Datos_Apid->Valor($strSort_seg,$j)][$llamadas[0].'F'] = $obj_Datos_Apid->ValorF("sessions",$j);
 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);



		//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";



    $obj_Datos_Apie = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apie -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apie -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apie -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apie -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apie -> metrics              = 'ga:sessions';
	$obj_Datos_Apie -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
    $obj_Datos_Apie -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apie->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apie->Valor($strSort,$j).$obj_Datos_Apie->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apie->Valor("sessions",$j);
		$array_pri[$obj_Datos_Apie->Valor($strSort,$j).$obj_Datos_Apie->Valor($strSort_seg,$j)][$llamadas[0].'F'] = $obj_Datos_Apie->ValorF("sessions",$j);
 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);




		//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";



    $obj_Datos_Apif = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apif -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apif -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apif -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apif -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apif -> metrics              = 'ga:sessions';
	$obj_Datos_Apif -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
    $obj_Datos_Apif -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apif->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apif->Valor($strSort,$j).$obj_Datos_Apif->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apif->Valor("sessions",$j);
		$array_pri[$obj_Datos_Apif->Valor($strSort,$j).$obj_Datos_Apif->Valor($strSort_seg,$j)][$llamadas[0].'F'] = $obj_Datos_Apif->ValorF("sessions",$j);
 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);


		//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";


    $obj_Datos_Apig = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apig -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apig -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apig -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apig -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apig -> metrics              = 'ga:sessions';
	$obj_Datos_Apig -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
    $obj_Datos_Apig -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apig->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apig->Valor($strSort,$j).$obj_Datos_Apig->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apig->Valor("sessions",$j);
		$array_pri[$obj_Datos_Apig->Valor($strSort,$j).$obj_Datos_Apig->Valor($strSort_seg,$j)][$llamadas[0].'F'] = $obj_Datos_Apig->ValorF("sessions",$j);
 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);

	//echo var_dump($array_pri);

	$records = $obj_Datos_Api->NumValoresReal();
	if(empty($array_pri)){
		$DataTables_draw = 0;
		$records = 0;
	}


	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $records .',';
	$strA .= '"recordsFiltered":' . $records .','	;
	$strA .= '"data":[';
	//echo var_dump($array_pri);
	$sw = 1;
	foreach ($array_pri as $key => $data) {
		if($sw < $DataTables_length+1){
			$strA .= '[';
			$strA .= '"<span class=\'izquierda_pri\'>'.$data["perspectiva"].'</span>",';
			$strA .= '"<span class=\'izquierda\'>'.$data["perspectiva_seg"].'</span>",';
			if($data["ALL_VISITS"] != 0){
				$strA .= '"'.number_format( (($data["TRANSACTION"]*100)/$data["ALL_VISITS"]) ,2,',','.').'%",';
			}else{
				$strA .= '"0%",';
			}

			//Eventlavel
			$datadat_av = 0;
			if(!empty($data["ALL_VISITS"])){
				$datadat_av = $data["ALL_VISITS"];
				$datadat_avf = $data["ALL_VISITSF"];
			}
			$strA .= '"'.$datadat_avf.' <span class=\'porcpri_dt\'>100%<span>",';

			$datadat = 0;
			if(!empty($data["PRODUCT_VIEW"])){
				$datadat = $data["PRODUCT_VIEW"];
			}
			if($datadat_av == 0){ $porca = 0;}else{$porca = number_format( ($datadat*100)/$datadat_av ,2,',','.');}
			$strA .= '"'.$datadat.' <span class=\'porcpri_dt\'>'.$porca.'%<span>",';

			$datadat_ac = 0;
			if(!empty($data["ADD_TO_CART"])){
				$datadat_ac = $data["ADD_TO_CART"];
			}
			if($datadat_av == 0){ $porca = 0;}else{$porca = number_format( ($datadat_ac*100)/$datadat_av ,2,',','.');}
			$strA .= '"'.$datadat_ac.' <span class=\'porcpri_dt\'>'.$porca.'%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg_dt\'>100%</span>",';

			$datadat = 0;
			if(!empty($data["CHECKOUT_1"])){
				$datadat = $data["CHECKOUT_1"];
			}
			if($datadat_av == 0){ $porca = 0;}else{$porca = number_format( ($datadat*100)/$datadat_av ,2,',','.');}
			if($datadat_ac == 0){ $porcab = 0;}else{$porcab = number_format( ($datadat*100)/$datadat_ac ,2,',','.');}
			$strA .= '"'.$datadat.' <span class=\'porcpri_dt\'>'.$porca.'%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg_dt\'>'.$porcab.'%</span>",';

			$datadat = 0;
			if(!empty($data["CHECKOUT_2"])){
				$datadat = $data["CHECKOUT_2"];
			}
			if($datadat_av == 0){ $porca = 0;}else{$porca = number_format( ($datadat*100)/$datadat_av ,2,',','.');}
			if($datadat_ac == 0){ $porcab = 0;}else{$porcab = number_format( ($datadat*100)/$datadat_ac ,2,',','.');}
			$strA .= '"'.$datadat.' <span class=\'porcpri_dt\'>'.$porca.'%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg_dt\'>'.$porcab.'%</span>",';

			$datadat = 0;
			if(!empty($data["CHECKOUT_3"])){
				$datadat = $data["CHECKOUT_3"];
			}
			if($datadat_av == 0){ $porca = 0;}else{$porca = number_format( ($datadat*100)/$datadat_av ,2,',','.');}
			if($datadat_ac == 0){ $porcab = 0;}else{$porcab = number_format( ($datadat*100)/$datadat_ac ,2,',','.');}
			$strA .= '"'.number_format( $datadat ,0,',','.').' <span class=\'porcpri_dt\'>'.$porca.'%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg_dt\'>'.$porcab.'%</span>",';

			$datadat = 0;
			if(!empty($data["TRANSACTION"])){
				$datadat = $data["TRANSACTION"];
			}
			if($datadat_av == 0){ $porca = 0;}else{$porca = number_format( ($datadat*100)/$datadat_av ,2,',','.');}
			if($datadat_ac == 0){ $porcab = 0;}else{$porcab = number_format( ($datadat*100)/$datadat_ac ,2,',','.');}
			$strA .= '"'.number_format( $datadat ,0,',','.').' <span class=\'porcpri_dt\'>'.$porca.'%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg_dt\'>'.$porcab.'%</span>"';

			$strA .= '],';   
		}
		$sw++;
	}

	if(!empty($array_pri)){
		$strA = substr($strA, 0, -1);
	}
	

	$strA .= ']}';
	
	return $strA; //var_dump($array_pri);$strA; 
}

function campanas_informes_embudos_pedidos_extra($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro, $perspectiva, $perspectiva_seg){

	global $trans;

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	//Sacamos los dos mayores productos 
	$obj_Datos_Api1 = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api1 -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api1 -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api1 -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api1 -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api1 -> metrics              = 'ga:sessions';
	if(empty($filtro)){
		$obj_Datos_Api1 -> optParams    = array(
										'dimensions' => 'ga:shoppingStage',
										'start-index' => 1,
		                                'max-results' => 100);  
	}else{
		$obj_Datos_Api1 -> optParams    = array(
										'dimensions' => 'ga:shoppingStage',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 100);  
	}
	$obj_Datos_Api1 -> Construccion();


	for ($x=1; $x<=$obj_Datos_Api1->NumValores(); $x++) {


		switch ($obj_Datos_Api1->Valor("shoppingStage",$x)) {
			case 'ALL_VISITS':
				$sesiones = $obj_Datos_Api1->Valor("sessions",$x);
				break;
			case 'ADD_TO_CART':
				$añadir_carrito = $obj_Datos_Api1->Valor("sessions",$x);
				break;
			case 'CHECKOUT_1':
				$login_registro = $obj_Datos_Api1->Valor("sessions",$x);
				break;
			case 'CHECKOUT_2':
				$revisar_pedido = $obj_Datos_Api1->Valor("sessions",$x);
				break;
			case 'CHECKOUT_3':
				$finalizar_pedido = $obj_Datos_Api1->Valor("sessions",$x);
				break;
			case 'PRODUCT_VIEW':
				$vista_producto = $obj_Datos_Api1->Valor("sessions",$x);
				break;
			case 'TRANSACTION':
				$pago_realizado = $obj_Datos_Api1->Valor("sessions",$x);
				break;
		}
		
	}

$tasa_conversion = ($pago_realizado*100)/$sesiones;


	if(empty($tasa_conversion)){ $tasa_conversion = 0;}
	if(empty($sesiones)){ $sesiones = 0;}
	if(empty($pago_realizado)){ $pago_realizado = 0;}
	if(empty($vista_producto)){ $vista_producto = 0;}
	if(empty($añadir_carrito)){ $añadir_carrito = 0;}
	if(empty($login_registro)){ $login_registro = 0;}
	if(empty($revisar_pedido)){ $revisar_pedido = 0;}
	if(empty($finalizar_pedido)){ $finalizar_pedido = 0;}



	/**MONSTAMOS GRAFICO**/

	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($perspectiva)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions='ga:ProductName  ';
			$strSort='ProductName ';
			$strNombre_Dimension = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions='ga:productCategoryLevel1';
			$strSort='productCategoryLevel1';
			$strNombre_Dimension = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions='ga:productCategoryLevel2';
			$strSort='productCategoryLevel2';
			$strNombre_Dimension = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions='ga:productBrand';
			$strSort='productBrand';
			$strNombre_Dimension = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions='ga:productListName';
			$strSort='productListName';
			$strNombre_Dimension = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions='ga:source';
			$strSort='source';
			$strNombre_Dimension = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions='ga:medium';
			$strSort='medium';
			$strNombre_Dimension = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions='ga:campaign';
			$strSort='campaign';
			$strNombre_Dimension = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions='ga:deviceCategory';
			$strSort='deviceCategory';
			$strNombre_Dimension = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions='ga:browser';
			$strSort='browser';
			$strNombre_Dimension = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions='ga:userGender';
			$strSort='userGender';
			$strNombre_Dimension = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions='ga:userAgeBracket';
			$strSort='userAgeBracket';
			$strNombre_Dimension = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions='ga:country,ga:countryIsoCode';
			$strSort='country';
			$strNombre_Dimension = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions='ga:region';
			$strSort='region';
			$strNombre_Dimension = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions='ga:city';
			$strSort='city';
			$strNombre_Dimension = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			$strNombre_Dimension = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			$strNombre_Dimension = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			$strNombre_Dimension = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			$strNombre_Dimension = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			$strNombre_Dimension = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			$strNombre_Dimension = $trans->__('Día', false);
			break;  
	}//Cierre switch 	

		//Preparación de la dimensión a utilizar (Perspectiva)
	if($perspectiva_seg!=""){
	switch (strtoupper($perspectiva_seg)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions_seg='ga:ProductName  ';
			$strSort_seg='ProductName';
			$strNombre_Dimension_seg = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions_seg='ga:productCategoryLevel1  ';
			$strSort_seg='productCategoryLevel1';
			$strNombre_Dimension_seg = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions_seg='ga:productCategoryLevel2';
			$strSort_seg='productCategoryLevel2';
			$strNombre_Dimension_seg = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions_seg='ga:productBrand';
			$strSort_seg='productBrand';
			$strNombre_Dimension_seg = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions_seg='ga:productListName';
			$strSort_seg='productListName';
			$strNombre_Dimension_seg = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions_seg='ga:source';
			$strSort_seg='source';
			$strNombre_Dimension_seg = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions_seg='ga:medium';
			$strSort_seg='medium';
			$strNombre_Dimension_seg = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions_seg='ga:campaign';
			$strSort_seg='campaign';
			$strNombre_Dimension_seg = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions_seg='ga:deviceCategory';
			$strSort_seg='deviceCategory';
			$strNombre_Dimension_seg = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions_seg='ga:browser';
			$strSort_seg='browser';
			$strNombre_Dimension_seg = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions_seg='ga:userGender';
			$strSort_seg='userGender';
			$strNombre_Dimension_seg = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions_seg='ga:userAgeBracket';
			$strSort_seg='userAgeBracket';
			$strNombre_Dimension_seg = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions_seg='ga:country,ga:countryIsoCode';
			$strSort_seg='country';
			$strNombre_Dimension_seg = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions_seg='ga:region';
			$strSort_seg='region';
			$strNombre_Dimension_seg = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions_seg='ga:city';
			$strSort_seg='city';
			$strNombre_Dimension_seg = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions_seg='ga:yearWeek';
			$strSort_seg='yearWeek';
			$strNombre_Dimension_seg = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions_seg='ga:yearMonth';
			$strSort_seg='yearMonth';
			$strNombre_Dimension_seg = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions_seg='ga:year';
			$strSort_seg='year';
			$strNombre_Dimension_seg = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions_seg='ga:hour';
			$strSort_seg='hour';
			$strNombre_Dimension_seg = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions_seg='ga:dayOfWeek';
			$strSort_seg='dayOfWeek';
			$strNombre_Dimension_seg = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions_seg='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort_seg='nthDay';
			$strNombre_Dimension_seg = $trans->__('Día', false);
			break;  
	}//Cierre switch 	
	}else{
		$strDimensions_seg='';
		$strSort_seg='';
		$strNombre_Dimension_seg = '';
	}

	if($perspectiva_seg != ""){
		$strDimensions .=  ",".$strDimensions_seg;
	}


//Creamos una array con llas 7 llamadas para ir descartando las que se van realizando
	$llamadas = array();
	$llamadas[0] = 'ALL_VISITS';
	$llamadas[1] = 'PRODUCT_VIEW';
	$llamadas[2] = 'ADD_TO_CART';
	$llamadas[3] = 'CHECKOUT_1';
	$llamadas[4] = 'CHECKOUT_2';
	$llamadas[5] = 'CHECKOUT_3';
	$llamadas[6] = 'TRANSACTION';

	//return $strDimensions .=  ",".$perspectiva_seg;
	$strTipo = strtolower($perspectiva);
	$strTipo_seg = strtolower($perspectiva_seg);

	//Preparación de la ordenación
	$array_Ordenacion = array($strSort,$strSort_seg,"","ALL_VISITS","PRODUCT_VIEW","ADD_TO_CART","CHECKOUT_1","CHECKOUT_2","CHECKOUT_3","TRANSACTION");
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa.',';
	}
	if($idIdioma!="todos"){
		$filtro .= 'ga:dimension1=='.$idIdioma.',';
	}
	if ($strFiltro!="no"){
		$filtro .= $strFiltro.',';
	}

	//Primera ordenacion
	$primera_ordenacion = 'ALL_VISITS';
	
	

	$filtro_pri = substr($filtro, 0, -1);
	
    //PRIMERA ARRAY PARA LA ORDENACIÓN
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:sessions';
	$obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => 'ga:shoppingStage=='.$primera_ordenacion.$filtro_pri,
	                                         'sort' => $DirOrdenacion.'ga:sessions',
	                                         'sort' => '-ga:sessions',
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => 5); 
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------

    $perspectiva = strtolower($perspectiva);
	$perspectiva_seg = strtolower($perspectiva_seg);

    $filtroanadido = "";
    $array_pri = array();
	for ($i=1; $i<=$obj_Datos_Api->NumValores(); $i++) {
		//Guardamos en una array la información
		if($perspectiva=="pais"){
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = Formato_Perspectiva('grafico',$perspectiva,$i,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$i));
		}else if($perspectiva=="idiomauser" || $perspectiva=="idiomanav"){
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = traducir_idioma($obj_Datos_Api->Valor($strSort,$i));
		}else if($perspectiva=="interes" || $perspectiva=="region" || $perspectiva=="ciudad"/**/){
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = traduce_de_ingles($obj_Datos_Api->Valor($strSort,$i),$_COOKIE["idioma_usuario"]);
		}else if($perspectiva=="terminos"){
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = $obj_Datos_Api->Valor($strSort,$i);
		}else{
			$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva"]  = Formato_Perspectiva('table',$perspectiva,$i,$obj_Datos_Api);
		}

		if($perspectiva_seg != ""){
			if($perspectiva_seg=="pais"){
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = Formato_Perspectiva('grafico',$perspectiva_seg,$i,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$i));
			}else if($perspectiva_seg=="idiomauser" || $perspectiva_seg=="idiomanav"){
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = traducir_idioma($obj_Datos_Api->Valor($strSort_seg,$i));
			}else if($perspectiva_seg=="interes" || $perspectiva_seg=="region" || $perspectiva_seg=="ciudad"/**/){
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = traduce_de_ingles($obj_Datos_Api->Valor($strSort_seg,$i),$_COOKIE["idioma_usuario"]);
			}else if($perspectiva_seg=="terminos"){
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = $obj_Datos_Api->Valor($strSort_seg,$i);
			}else{
				$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)]["perspectiva_seg"]  = Formato_Perspectiva('table',$perspectiva_seg,$i,$obj_Datos_Api);
			}
		}
	 	
 		$array_pri[$obj_Datos_Api->Valor($strSort,$i).$obj_Datos_Api->Valor($strSort_seg,$i)][$array_Ordenacion[$DataTables_OrderColumn]] = $obj_Datos_Api->Valor("sessions",$i);
 		//Creamos un filtro para la segunda array
 		$filtroanadido .= "ga:".$strSort."==".$obj_Datos_Api->Valor($strSort,$i).',';
 		if( !empty( $strDimensions_seg ) ){
 			$filtroanadido .= "ga:".$strSort_seg."==".$obj_Datos_Api->Valor($strSort_seg,$i).',';
 		}

	}

	//eliminamos la llamada realizada
	foreach ($llamadas as $key => $ll) {
		if($ll == $primera_ordenacion){
			unset($llamadas[$key]);
		}
	}
	$llamadas = array_values($llamadas);

	//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";

	$filtro_seg .= $filtro;
	$filtro_seg .= $filtroanadido;

	$filtro_seg = substr($filtro_seg, 0, -1);

    $obj_Datos_Apib = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apib -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apib -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apib -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apib -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apib -> metrics              = 'ga:sessions';
	$obj_Datos_Apib -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => 1000); 
    $obj_Datos_Apib -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apib->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apib->Valor($strSort,$j).$obj_Datos_Apib->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apib->Valor("sessions",$j);

 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);

	//echo var_dump($llamadas);


		//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";


    $obj_Datos_Apic = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apic -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apic -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apic -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apic -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apic -> metrics              = 'ga:sessions';
	$obj_Datos_Apic -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => 1000); 
    $obj_Datos_Apic -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apic->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apic->Valor($strSort,$j).$obj_Datos_Apic->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apic->Valor("sessions",$j);

 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);




		//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";


    $obj_Datos_Apid = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apid -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apid -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apid -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apid -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apid -> metrics              = 'ga:sessions';
	$obj_Datos_Apid -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => 1000); 
    $obj_Datos_Apid -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apid->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apid->Valor($strSort,$j).$obj_Datos_Apid->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apid->Valor("sessions",$j);

 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);



		//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";


    $obj_Datos_Apie = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apie -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apie -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apie -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apie -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apie -> metrics              = 'ga:sessions';
	$obj_Datos_Apie -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => 1000); 
    $obj_Datos_Apie -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apie->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apie->Valor($strSort,$j).$obj_Datos_Apie->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apie->Valor("sessions",$j);

 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);




		//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";


    $obj_Datos_Apif = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apif -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apif -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apif -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apif -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apif -> metrics              = 'ga:sessions';
	$obj_Datos_Apif -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => 1000); 
    $obj_Datos_Apif -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apif->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apif->Valor($strSort,$j).$obj_Datos_Apif->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apif->Valor("sessions",$j);

 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);


		//Ahora con una segunda llamada rellenamos los datos que faltan
	$strDimensions .= "";

    $obj_Datos_Apig = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Apig -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Apig -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Apig -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Apig -> enddate              = $datFechaFinFiltro;
    $obj_Datos_Apig -> metrics              = 'ga:sessions';
	$obj_Datos_Apig -> optParams        = array(
											 'dimensions' => $strDimensions.',ga:shoppingStage',
											 'filters' => $filtro_seg.';ga:shoppingStage=='.$llamadas[0],
	                                         //'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => 1000); 
    $obj_Datos_Apig -> Construccion();

	for ($j=1; $j<=$obj_Datos_Apig->NumValores(); $j++) {

		//Guardamos en una array la información
		$array_pri[$obj_Datos_Apig->Valor($strSort,$j).$obj_Datos_Apig->Valor($strSort_seg,$j)][$llamadas[0]] = $obj_Datos_Apig->Valor("sessions",$j);

 		//Creamos un filtro para la segunda array	

	}
	//Eliminamos llamadas[0]
	unset($llamadas[0]);
	$llamadas = array_values($llamadas);

	
	$sw = 1;
	$graf_tit = "['embudo',";
	$graf_tot_ac = "['Añadir al parrito',";
	$graf_tot_lr = "['Login/registro',";
	$graf_tot_rp = "['Revisar pedido',";
	$graf_tot_fp = "['Finalizar pedido',";
	$graf_tot_pr = "['Pedido pagado',";

	foreach ($array_pri as $key => $data) {
		if($sw < 6){
			
			$graf_tit .= "'".$data["perspectiva"]."/".$data["perspectiva_seg"]."',";

			//Añadir al carrito
			$n = $data["ADD_TO_CART"];
			if($n == ""){ 
				$n = 0; 
			}else{
				$n = 100;
			}	
			$graf_tot_ac .= "".$n.",";

			//Login/registro
			$n = $data["ADD_TO_CART"];
			if($n == ""){ 
				$n = 0; 
			}else{
				$n = number_format( ($data["CHECKOUT_1"]*100)/$data["ADD_TO_CART"] ,2,'.',',');
			}	
			$graf_tot_lr .= "".$n.",";

			//Revisar pedido
			$n = $data["ADD_TO_CART"];
			if($n == ""){ 
				$n = 0; 
			}else{
				$n = number_format( ($data["CHECKOUT_2"]*100)/$data["ADD_TO_CART"] ,2,'.',',');
			}
			$graf_tot_rp .= "".$n.",";

			//finalizar pedido
			$n = $data["ADD_TO_CART"];
			if($n == ""){ 
				$n = 0; 
			}else{
				$n = number_format( ($data["CHECKOUT_3"]*100)/$data["ADD_TO_CART"] ,2,'.',',');
			}	
			$graf_tot_fp .= "".$n.",";

			//Pedido pagado
			$n = $data["ADD_TO_CART"];
			if($n == ""){ 
				$n = 0; 
			}else{
				$n = number_format( ($data["TRANSACTION"]*100)/$data["ADD_TO_CART"] ,2,'.',',');
			}	
			$graf_tot_pr .= "".$n.",";
			

			$sw++;

		}
	}

	$añadir_carrito_graf   = 100;
	$login_registro_graf   = number_format(($login_registro*100)/$añadir_carrito,2,'.',',');
	$revisar_pedido_graf   = number_format(($revisar_pedido*100)/$añadir_carrito ,2,'.',',');
	$finalizar_pedido_graf = number_format(($finalizar_pedido*100)/$añadir_carrito,2,'.',',');
	$pago_realizado_graf   = number_format(($pago_realizado*100)/$añadir_carrito,2,'.',',');

	$graf_tit    .= "'".$trans->__('Total', false)."'";
	$graf_tot_ac .= "".$añadir_carrito_graf."";
	$graf_tot_lr .= "".$login_registro_graf."";
	$graf_tot_rp .= "".$revisar_pedido_graf."";
	$graf_tot_fp .= "".$finalizar_pedido_graf."";
	$graf_tot_pr .= "".$pago_realizado_graf."";

	$graf_tit    .= "],";
	$graf_tot_ac .= "],";
	$graf_tot_lr .= "],";
	$graf_tot_rp .= "],";
	$graf_tot_fp .= "],";
	$graf_tot_pr .= "]";

	$graf = "[";
	$graf .= $graf_tit;
	$graf .= $graf_tot_ac;
	$graf .= $graf_tot_lr;
	$graf .= $graf_tot_rp;
	$graf .= $graf_tot_fp;
	$graf .= $graf_tot_pr;
	$graf .= "]";


	/*********************/

	$strA = '{';
	
	$strA .= '"total_tasa_conversion": "'  . $tasa_conversion  . '",';
	$strA .= '"total_sesiones": "' 	       . $sesiones 	       . '",';
	$strA .= '"total_pago_realizado": "'   . $pago_realizado   . '",';
	$strA .= '"total_vista_producto": "'   . $vista_producto   . '",';
	$strA .= '"total_añadir_carrito": "'   . $añadir_carrito   . '",';
	$strA .= '"total_login_registro": "'   . $login_registro   . '",';
	$strA .= '"total_revisar_pedido": "'   . $revisar_pedido   . '",';
	$strA .= '"total_finalizar_pedido": "' . $finalizar_pedido . '",';

	$strA .= '"total_tasa_conversion_porcentages": "",';
	$strA .= '"total_sesiones_porcentages": "<span class=\'porcpri\'>100%</span>",';
	$strA .= '"total_pago_realizado_porcentages": "<span class=\'porcpri\'>'  . number_format( ($pago_realizado*100)/$sesiones ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg\'>' . number_format( ($pago_realizado*100)/$añadir_carrito ,2,',','.') . '%</span>",';
	$strA .= '"total_vista_producto_porcentages": "<span class=\'porcpri\'>'  . number_format( ($vista_producto*100)/$sesiones ,2,',','.') . '%</span>",';
	$strA .= '"total_añadir_carrito_porcentages": "<span class=\'porcpri\'>'  . number_format( ($añadir_carrito*100)/$sesiones ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg\'> 100%</span>",';
	$strA .= '"total_login_registro_porcentages": "<span class=\'porcpri\'>'  . number_format( ($login_registro*100)/$sesiones ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg\'> ' . number_format( ($login_registro*100)/$añadir_carrito ,2,',','.') . '%</span>",';
	$strA .= '"total_revisar_pedido_porcentages": "<span class=\'porcpri\'>'  . number_format( ($revisar_pedido*100)/$sesiones ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg\'> ' . number_format( ($revisar_pedido*100)/$añadir_carrito ,2,',','.') . '%</span>",';
	$strA .= '"total_finalizar_pedido_porcentages": "<span class=\'porcpri\'>'  . number_format( ($finalizar_pedido*100)/$sesiones ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg\'> ' . number_format( ($finalizar_pedido*100)/$añadir_carrito ,2,',','.') . '%</span>",';
	
	$strA .= '"total_tasa_conversionf": "'  . number_format( $tasa_conversion ,2,',','.')  . '",';
	$strA .= '"total_sesionesf": "' 	    . number_format( $sesiones ,0,',','.') 	       . '",';
	$strA .= '"total_pago_realizadof": "'   . number_format( $pago_realizado ,0,',','.')   . '",';
	$strA .= '"total_vista_productof": "'   . number_format( $vista_producto ,0,',','.')   . '",';
	$strA .= '"total_añadir_carritof": "'   . number_format( $añadir_carrito ,0,',','.')   . '",';
	$strA .= '"total_login_registrof": "'   . number_format( $login_registro ,0,',','.')   . '",';
	$strA .= '"total_revisar_pedidof": "'   . number_format( $revisar_pedido ,0,',','.')   . '",';
	$strA .= '"total_finalizar_pedidof": "' . number_format( $finalizar_pedido ,0,',','.') . '",';

	$strA .= '"grafico": "' . $graf . '",';
	$strA .= '"grafico_count": "' . ($sw-1) . '"';

	$strA .= '}';
	return $strA;
}


function datatable_informes_embudos_productos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro, $perspectiva, $perspectiva_seg)
{

	global $trans;
	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($perspectiva)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions='ga:ProductName  ';
			$strSort='ProductName ';
			$strNombre_Dimension = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions='ga:productCategoryLevel1';
			$strSort='productCategoryLevel1';
			$strNombre_Dimension = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions='ga:productCategoryLevel2';
			$strSort='productCategoryLevel2';
			$strNombre_Dimension = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions='ga:productBrand';
			$strSort='productBrand';
			$strNombre_Dimension = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions='ga:productListName';
			$strSort='productListName';
			$strNombre_Dimension = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions='ga:source';
			$strSort='source';
			$strNombre_Dimension = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions='ga:medium';
			$strSort='medium';
			$strNombre_Dimension = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions='ga:campaign';
			$strSort='campaign';
			$strNombre_Dimension = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions='ga:deviceCategory';
			$strSort='deviceCategory';
			$strNombre_Dimension = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions='ga:browser';
			$strSort='browser';
			$strNombre_Dimension = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions='ga:userGender';
			$strSort='userGender';
			$strNombre_Dimension = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions='ga:userAgeBracket';
			$strSort='userAgeBracket';
			$strNombre_Dimension = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions='ga:country,ga:countryIsoCode';
			$strSort='country';
			$strNombre_Dimension = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions='ga:region';
			$strSort='region';
			$strNombre_Dimension = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions='ga:city';
			$strSort='city';
			$strNombre_Dimension = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			$strNombre_Dimension = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			$strNombre_Dimension = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			$strNombre_Dimension = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			$strNombre_Dimension = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			$strNombre_Dimension = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			$strNombre_Dimension = $trans->__('Día', false);
			break;  
	}//Cierre switch 	

	//Preparación de la dimensión a utilizar (Perspectiva)
	if($perspectiva_seg!=""){
	switch (strtoupper($perspectiva_seg)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions_seg='ga:ProductName  ';
			$strSort_seg='ProductName';
			$strNombre_Dimension_seg = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions_seg='ga:productCategoryLevel1  ';
			$strSort_seg='productCategoryLevel1';
			$strNombre_Dimension_seg = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions_seg='ga:productCategoryLevel2';
			$strSort_seg='productCategoryLevel2';
			$strNombre_Dimension_seg = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions_seg='ga:productBrand';
			$strSort_seg='productBrand';
			$strNombre_Dimension_seg = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions_seg='ga:productListName';
			$strSort_seg='productListName';
			$strNombre_Dimension_seg = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions_seg='ga:source';
			$strSort_seg='source';
			$strNombre_Dimension_seg = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions_seg='ga:medium';
			$strSort_seg='medium';
			$strNombre_Dimension_seg = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions_seg='ga:campaign';
			$strSort_seg='campaign';
			$strNombre_Dimension_seg = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions_seg='ga:deviceCategory';
			$strSort_seg='deviceCategory';
			$strNombre_Dimension_seg = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions_seg='ga:browser';
			$strSort_seg='browser';
			$strNombre_Dimension_seg = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions_seg='ga:userGender';
			$strSort_seg='userGender';
			$strNombre_Dimension_seg = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions_seg='ga:userAgeBracket';
			$strSort_seg='userAgeBracket';
			$strNombre_Dimension_seg = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions_seg='ga:country,ga:countryIsoCode';
			$strSort_seg='country';
			$strNombre_Dimension_seg = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions_seg='ga:region';
			$strSort_seg='region';
			$strNombre_Dimension_seg = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions_seg='ga:city';
			$strSort_seg='city';
			$strNombre_Dimension_seg = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions_seg='ga:yearWeek';
			$strSort_seg='yearWeek';
			$strNombre_Dimension_seg = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions_seg='ga:yearMonth';
			$strSort_seg='yearMonth';
			$strNombre_Dimension_seg = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions_seg='ga:year';
			$strSort_seg='year';
			$strNombre_Dimension_seg = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions_seg='ga:hour';
			$strSort_seg='hour';
			$strNombre_Dimension_seg = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions_seg='ga:dayOfWeek';
			$strSort_seg='dayOfWeek';
			$strNombre_Dimension_seg = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions_seg='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort_seg='nthDay';
			$strNombre_Dimension_seg = $trans->__('Día', false);
			break;  
	}//Cierre switch 	
	}else{
		$strDimensions_seg='';
		$strSort_seg='';
		$strNombre_Dimension_seg = '';
	}

	if($perspectiva_seg != ""){
		$strDimensions .=  ",".$strDimensions_seg;
	}

	
	$strTipo = strtolower($perspectiva);
	$strTipo_seg = strtolower($perspectiva_seg);

	//Preparación de la ordenación
	$array_Ordenacion = array($strSort,$strSort_seg,'productDetailViews','productAddsToCart ','itemQuantity');
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$filtro="ga:itemQuantity>0";
	if($paisGa!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:productDetailViews, ga:productAddsToCart, ga:itemQuantity';
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------


    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_ant -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt ;
    $obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt ;
    $obj_Datos_Api_ant -> metrics              = 'ga:productDetailViews, ga:productAddsToCart, ga:itemQuantity';
    if(empty($filtro)){
	    $obj_Datos_Api_ant -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api_ant -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api_ant -> Construccion();

    $array_ant = array();
 	for ($i=1; $i<=$obj_Datos_Api_ant->NumValores(); $i++) {
 
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["perspectiva"]        = $obj_Datos_Api_ant->Valor($strSort,$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["perspectiva_seg"]    = $obj_Datos_Api_ant->Valor($strSort_seg,$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["itemQuantity"]       = $obj_Datos_Api_ant->Valor("itemQuantity",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["productAddsToCart"]  = $obj_Datos_Api_ant->Valor("productAddsToCart",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["productDetailViews"] = $obj_Datos_Api_ant->Valor("productDetailViews",$i);

 	}

	// Construimos un json con los datos

	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .','	;
	$strA .= '"data":[';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		$strA .= '[';
		//El primer valor va a depender de la perspectiva que estamos pintando
		if($strTipo=="pais"){
			$strA .= '"<span class=\'izquierda_pri\'>'.Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x)).'</span>"';
		}else if($strTipo=="idiomauser" || $strTipo=="idiomanav"){
			$strA .= '"<span class=\'izquierda_pri\'>'.traducir_idioma($obj_Datos_Api->Valor($strSort,$x)).'</span>"';
		}else if($strTipo=="interes" || $strTipo=="region" || $strTipo=="ciudad"/**/){
			
			$strA .= '"<span class=\'izquierda_pri\'>'.traduce_de_ingles($obj_Datos_Api->Valor($strSort,$x),$_COOKIE["idioma_usuario"]).'</span>"';
		}else if($strTipo=="terminos"){
			$strA .= '"<span class=\'izquierda_pri\'>'.$obj_Datos_Api->Valor($strSort,$x).'</span>"';
		}else{
			$strA .= '"<span class=\'izquierda_pri\'>'.Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api).'</span>"';
		}
		$strA .= ",";

		if($perspectiva_seg != ""){
			if($strTipo_seg=="pais"){
				$strA .= '"<span class=\'izquierda\'>'.Formato_Perspectiva('table',$strTipo_seg,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x)).'</span>"';
			}else if($strTipo_seg=="idiomauser" || $strTipo_seg=="idiomanav"){
				$strA .= '"<span class=\'izquierda\'>'.traducir_idioma($obj_Datos_Api->Valor($strSort_seg,$x)).'</span>"';
			}else if($strTipo_seg=="interes" || $strTipo_seg=="region" || $strTipo_seg=="ciudad"/**/){
				
				$strA .= '"<span class=\'izquierda\'>'.traduce_de_ingles($obj_Datos_Api->Valor($strSort_seg,$x),$_COOKIE["idioma_usuario"]).'</span>"';
			}else if($strTipo_seg=="terminos"){
				$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->Valor($strSort_seg,$x).'</span>"';
			}else{
				$strA .= '"<span class=\'izquierda\'>'.Formato_Perspectiva('table',$strTipo_seg,$x,$obj_Datos_Api).'</span>"';
			}
			$strA .= ",";
		}else{
			$strA .= '"",';
		}

		/*Buscamos los anteriores*/
		$itemQuantity_ant       = "0";
		$productAddsToCart_ant  = "0";
		$productDetailViews_ant = "0";


		if(isset($array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["perspectiva"])){
			$itemQuantity_ant       = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["itemQuantity"];
			$productAddsToCart_ant  = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["productAddsToCart"];
			$productDetailViews_ant = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["productDetailViews"];
		}
	

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("productDetailViews",$x),$productDetailViews_ant,number_format( $obj_Datos_Api->Valor("productDetailViews",$x)  ,0,',','.'),number_format( $productDetailViews_ant ,0,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("productDetailViews",$x)  ,0,',','.').' <span class=\'porcpri_dt\'>100%</span></span>",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("productAddsToCart",$x),$productAddsToCart_ant ,number_format( $obj_Datos_Api->Valor("productAddsToCart",$x)  ,0,',','.'),number_format( $productAddsToCart_ant   ,0,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("productAddsToCart",$x)  ,0,',','.').' <span class=\'porcpri_dt\'>'  . number_format( ($obj_Datos_Api->Valor("productAddsToCart",$x)*100)/$obj_Datos_Api->Valor("productDetailViews",$x) ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg_dt\'>100%</span></span>",';	

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("itemQuantity",$x),$itemQuantity_ant ,number_format( $obj_Datos_Api->Valor("itemQuantity",$x)  ,0,',','.'),number_format( $itemQuantity_ant   ,0,',','.'));
		$trep = 0;
		if($obj_Datos_Api->Valor("productAddsToCart",$x) != 0 || $obj_Datos_Api->Valor("productAddsToCart",$x) != ""){
			$trep = number_format( ($obj_Datos_Api->Valor("itemQuantity",$x)*100)/$obj_Datos_Api->Valor("productAddsToCart",$x) ,2,',','.');
		}
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("itemQuantity",$x)  ,0,',','.').' <span class=\'porcpri_dt\'>'  . number_format( ($obj_Datos_Api->Valor("itemQuantity",$x)*100)/$obj_Datos_Api->Valor("productDetailViews",$x) ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg_dt\'>'.$trep.'%</span></span>"';

		
		

		$strA .= ']';	  
		//Añadimos una "," si no es el último registro a pintar
		if ($x < $obj_Datos_Api->NumValores()) {
			$strA .= ','; 
		}
	}
	$strA .= ']}';
	
	//Devolvemos el json
	return $strA;
}

function campanas_informes_embudos_productos_extra($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro, $perspectiva, $perspectiva_seg)
{

	global $trans;

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	//Sacamos los dos mayores productos 
	$obj_Datos_Api_productos = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_productos -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_productos -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_productos -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_productos -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_productos -> metrics              = 'ga:productDetailViews,ga:productAddsToCart,ga:itemQuantity';
	if(empty($filtro)){
		$obj_Datos_Api_productos -> optParams    = array(
										'dimensions' => '',
										'start-index' => 1,
		                                'max-results' => 1);  
	}else{
		$obj_Datos_Api_productos -> optParams    = array(
										'dimensions' => '',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 1);  
	}
	$obj_Datos_Api_productos -> Construccion();

	for ($x=1; $x<=$obj_Datos_Api_productos->NumValores(); $x++) {

		$resumen_productovisto     = $obj_Datos_Api_productos->Valor("productDetailViews",$x);
		$resumen_productovistof    = $obj_Datos_Api_productos->ValorF("productDetailViews",$x);
		$resumen_productocarrito   = $obj_Datos_Api_productos->Valor("productAddsToCart",$x);
		$resumen_productocarritof  = $obj_Datos_Api_productos->ValorF("productAddsToCart",$x);
		$resumen_productocomprado  = $obj_Datos_Api_productos->Valor("itemQuantity",$x);
		$resumen_productocompradof = $obj_Datos_Api_productos->ValorF("itemQuantity",$x);

	}



	//GRÁFICO

		global $trans;
	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($perspectiva)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions='ga:ProductName  ';
			$strSort='ProductName ';
			$strNombre_Dimension = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions='ga:productCategoryLevel1';
			$strSort='productCategoryLevel1';
			$strNombre_Dimension = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions='ga:productCategoryLevel2';
			$strSort='productCategoryLevel2';
			$strNombre_Dimension = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions='ga:productBrand';
			$strSort='productBrand';
			$strNombre_Dimension = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions='ga:productListName';
			$strSort='productListName';
			$strNombre_Dimension = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions='ga:source';
			$strSort='source';
			$strNombre_Dimension = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions='ga:medium';
			$strSort='medium';
			$strNombre_Dimension = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions='ga:campaign';
			$strSort='campaign';
			$strNombre_Dimension = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions='ga:deviceCategory';
			$strSort='deviceCategory';
			$strNombre_Dimension = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions='ga:browser';
			$strSort='browser';
			$strNombre_Dimension = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions='ga:userGender';
			$strSort='userGender';
			$strNombre_Dimension = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions='ga:userAgeBracket';
			$strSort='userAgeBracket';
			$strNombre_Dimension = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions='ga:country,ga:countryIsoCode';
			$strSort='country';
			$strNombre_Dimension = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions='ga:region';
			$strSort='region';
			$strNombre_Dimension = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions='ga:city';
			$strSort='city';
			$strNombre_Dimension = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions='ga:yearWeek';
			$strSort='yearWeek';
			$strNombre_Dimension = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions='ga:yearMonth';
			$strSort='yearMonth';
			$strNombre_Dimension = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions='ga:year';
			$strSort='year';
			$strNombre_Dimension = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions='ga:hour';
			$strSort='hour';
			$strNombre_Dimension = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions='ga:dayOfWeek';
			$strSort='dayOfWeek';
			$strNombre_Dimension = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort='nthDay';
			$strNombre_Dimension = $trans->__('Día', false);
			break;  
	}//Cierre switch 	

		//Preparación de la dimensión a utilizar (Perspectiva)
	if($perspectiva_seg!=""){
	switch (strtoupper($perspectiva_seg)) {

		/*ECOMMERCE*/
		case "PRODUCTO":
			$strDimensions_seg='ga:ProductName  ';
			$strSort_seg='ProductName';
			$strNombre_Dimension_seg = $trans->__('Producto', false);
			break; 
		case "CATEGORIA1":
			$strDimensions_seg='ga:productCategoryLevel1  ';
			$strSort_seg='productCategoryLevel1';
			$strNombre_Dimension_seg = $trans->__('Categoría1', false);
			break;
		case "CATEGORIA2":
			$strDimensions_seg='ga:productCategoryLevel2';
			$strSort_seg='productCategoryLevel2';
			$strNombre_Dimension_seg = $trans->__('Categoría2', false);
			break;
		case "MARCA":
			$strDimensions_seg='ga:productBrand';
			$strSort_seg='productBrand';
			$strNombre_Dimension_seg = $trans->__('Marca', false);
			break;
		case "LISTAPRODUCTO":
			$strDimensions_seg='ga:productListName';
			$strSort_seg='productListName';
			$strNombre_Dimension_seg = $trans->__('Lista de productos', false);
			break;

		/*ADQUISICIÓN*/
		case "FUENTE":
			$strDimensions_seg='ga:source';
			$strSort_seg='source';
			$strNombre_Dimension_seg = $trans->__('Fuente', false);
			break; 
		case "MEDIO":
			$strDimensions_seg='ga:medium';
			$strSort_seg='medium';
			$strNombre_Dimension_seg = $trans->__('Medio', false);
			break;
		case "CAMPANAS":
			$strDimensions_seg='ga:campaign';
			$strSort_seg='campaign';
			$strNombre_Dimension_seg = $trans->__('Campañas', false);
			break;

		/*TECNOLÓGICO*/
		case "DISPOSITIVOS":
			$strDimensions_seg='ga:deviceCategory';
			$strSort_seg='deviceCategory';
			$strNombre_Dimension_seg = $trans->__('Dispositivos', false);
			break; 
		case "NAVEGADORES":
			$strDimensions_seg='ga:browser';
			$strSort_seg='browser';
			$strNombre_Dimension_seg = $trans->__('Navegadores', false);
			break;

		/*USUARIOS*/
		case "SEXO":
			$strDimensions_seg='ga:userGender';
			$strSort_seg='userGender';
			$strNombre_Dimension_seg = $trans->__('Sexo', false);
			break; 
		case "EDAD":
			$strDimensions_seg='ga:userAgeBracket';
			$strSort_seg='userAgeBracket';
			$strNombre_Dimension_seg = $trans->__('Edad', false);
			break;

		/*GEOGRÁFICO*/
		case "PAIS":
			$strDimensions_seg='ga:country,ga:countryIsoCode';
			$strSort_seg='country';
			$strNombre_Dimension_seg = $trans->__('Pais', false);
			break; 
		case "REGION":
			$strDimensions_seg='ga:region';
			$strSort_seg='region';
			$strNombre_Dimension_seg = $trans->__('Región', false);
			break;
		case "CIUDAD":
			$strDimensions_seg='ga:city';
			$strSort_seg='city';
			$strNombre_Dimension_seg = $trans->__('Ciudad', false);
			break; 

		/*TEMPORAL*/
		case "SEMANAL":
			$strDimensions_seg='ga:yearWeek';
			$strSort_seg='yearWeek';
			$strNombre_Dimension_seg = $trans->__('Semana', false);
			break; 
		case "MENSUAL":
			$strDimensions_seg='ga:yearMonth';
			$strSort_seg='yearMonth';
			$strNombre_Dimension_seg = $trans->__('Mes', false);
			break;
		case "ANUAL":
			$strDimensions_seg='ga:year';
			$strSort_seg='year';
			$strNombre_Dimension_seg = $trans->__('Año', false);
			break; 
		case "HORAS":
			$strDimensions_seg='ga:hour';
			$strSort_seg='hour';
			$strNombre_Dimension_seg = $trans->__('Hora', false);
			break; 	
		case "DIASSEMANA":
			$strDimensions_seg='ga:dayOfWeek';
			$strSort_seg='dayOfWeek';
			$strNombre_Dimension_seg = $trans->__('Días semana', false);
			break;
		case "DIARIO": 			
		default:
			$strDimensions_seg='ga:nthDay,ga:year,ga:month,ga:day';
			$strSort_seg='nthDay';
			$strNombre_Dimension_seg = $trans->__('Día', false);
			break;  
	}//Cierre switch 	
	}else{
		$strDimensions_seg='';
		$strSort_seg='';
		$strNombre_Dimension_seg = '';
	}

	if($perspectiva_seg != ""){
		$strDimensions .=  ",".$strDimensions_seg;
	}

	//return $strDimensions .=  ",".$perspectiva_seg;
	$strTipo = strtolower($perspectiva);
	$strTipo_seg = strtolower($perspectiva_seg);

	//Preparación de la ordenación
	$array_Ordenacion = array($strSort,$strSort_seg,'productDetailViews','productAddsToCart ','itemQuantity');
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$filtro="ga:itemQuantity>0";
	if($paisGa!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:productDetailViews, ga:productAddsToCart, ga:itemQuantity';
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'start-index' => 1,
	                                         'max-results' => 5); 
	}else{
		$obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
											 'start-index' => 1,
	                                         'max-results' => 5); 
	}
    $obj_Datos_Api -> Construccion();

	$sw = 1;
	$graf_tit = "['embudo',";
	$graf_tot_pv = "['Producto visto',";
	$graf_tot_pa = "['Producto añadido al carrito',";
	$graf_tot_pc = "['Producto comprado',";

	for ($i=1; $i<=$obj_Datos_Api->NumValores(); $i++) {

		//Guardamos en una array la información
		if($perspectiva=="pais"){
			$graf_perspectiva = Formato_Perspectiva('grafico',$perspectiva,$i,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$i));
		}else if($perspectiva=="idiomauser" || $perspectiva=="idiomanav"){
			$graf_perspectiva = traducir_idioma($obj_Datos_Api->Valor($strSort,$i));
		}else if($perspectiva=="interes" || $perspectiva=="region" || $perspectiva=="ciudad"/**/){
			$graf_perspectiva = traduce_de_ingles($obj_Datos_Api->Valor($strSort,$i),$_COOKIE["idioma_usuario"]);
		}else if($perspectiva=="terminos"){
			$graf_perspectiva = $obj_Datos_Api->Valor($strSort,$i);
		}else{
			$graf_perspectiva = Formato_Perspectiva('table',$perspectiva,$i,$obj_Datos_Api);
		}

		if($perspectiva_seg != ""){
			if($perspectiva_seg=="pais"){
				$graf_perspectiva_seg = Formato_Perspectiva('grafico',$perspectiva_seg,$i,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$i));
			}else if($perspectiva_seg=="idiomauser" || $perspectiva_seg=="idiomanav"){
				$graf_perspectiva_seg = traducir_idioma($obj_Datos_Api->Valor($strSort_seg,$i));
			}else if($perspectiva_seg=="interes" || $perspectiva_seg=="region" || $perspectiva_seg=="ciudad"/**/){
				$graf_perspectiva_seg = traduce_de_ingles($obj_Datos_Api->Valor($strSort_seg,$i),$_COOKIE["idioma_usuario"]);
			}else if($perspectiva_seg=="terminos"){
				$graf_perspectiva_seg = $obj_Datos_Api->Valor($strSort_seg,$i);
			}else{
				$graf_perspectiva_seg = Formato_Perspectiva('table',$perspectiva_seg,$i,$obj_Datos_Api);
			}
		}

		$graf_productovisto     = $obj_Datos_Api->Valor("productDetailViews",$i);
		$graf_productocarrito   = $obj_Datos_Api->Valor("productAddsToCart",$i);
		$graf_productocomprado  = $obj_Datos_Api->Valor("itemQuantity",$i);

		$graf_tit .= "'".$graf_perspectiva."/".$graf_perspectiva_seg."',";

		//Producto añadir al carrito
		$n = $graf_productocarrito;
		if($n == "" || $n == 0){ 
			$n = 0; 
		}else{
			$n = number_format( ($graf_productocarrito*100)/$graf_productocarrito ,2,'.',',');
		}	
		$graf_tot_pa .= "".$n.",";

		//Producto comprado
		$n = $graf_productocarrito;
		if($n == "" || $n == 0){ 
			$n = 0; 
		}else{
			$n = number_format( ($graf_productocomprado*100)/$graf_productocarrito ,2,'.',',');
		}	
		$graf_tot_pc .= "".$n.",";
		$sw++;

	}


	
	$resumen_productocarrito_graf  = 100;
	$resumen_productocomprado_graf = ($resumen_productocomprado*100)/$resumen_productocarrito;

	$graf_tit    .= "'".$trans->__('Total', false)."'";
	$graf_tot_pa .= "".number_format($resumen_productocarrito_graf,2,'.',',')."";
	$graf_tot_pc .= "".number_format($resumen_productocomprado_graf,2,'.',',')."";

	$graf_tit    .= "],";
	$graf_tot_pa .= "],";
	$graf_tot_pc .= "]";


	$graf = "[";
	$graf .= $graf_tit;
	$graf .= $graf_tot_pa;
	$graf .= $graf_tot_pc;

	$graf .= "]";



	$strA = '{';
	
	$strA .= '"total_pv": "' . $resumen_productovisto	 . '",';
	$strA .= '"total_pa": "' . $resumen_productocarrito  . '",';
	$strA .= '"total_pc": "' . $resumen_productocomprado . '",';

	$strA .= '"total_pvF": "' . $resumen_productovistof	   . '",';
	$strA .= '"total_paF": "' . $resumen_productocarritof  . '",';
	$strA .= '"total_pcF": "' . $resumen_productocompradof . '",';

	$strA .= '"total_pv_porcentajes": "<span class=\'porcpri\'>100%</span>",';
	$strA .= '"total_pa_porcentajes": "<span class=\'porcpri\'>'  . number_format( ($resumen_productocarrito*100)/$resumen_productovisto ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg\'>100%</span>",';
	$strA .= '"total_pc_porcentajes": "<span class=\'porcpri\'>'  . number_format( ($resumen_productocomprado*100)/$resumen_productovisto ,2,',','.') . '%</span> <span class=\'barrita\'>|</span> <span class=\'porcseg\'>' . number_format( ($resumen_productocomprado*100)/$resumen_productocarrito ,2,',','.') . '%</span>",';

	$strA .= '"resumen_productovisto": "'    . $resumen_productovisto    . '",';
	$strA .= '"resumen_productocarrito": "'  . $resumen_productocarrito  . '",';
	$strA .= '"resumen_productocomprado": "' . $resumen_productocomprado . '",';

	$strA .= '"resumen_productovistoF": "'    . $resumen_productovistof    . '",';
	$strA .= '"resumen_productocarritoF": "'  . $resumen_productocarritof  . '",';
	$strA .= '"resumen_productocompradoF": "' . $resumen_productocompradof . '",';

	$strA .= '"grafico": "' . $graf . '",';
	$strA .= '"grafico_count": "' . ($sw-1) . '"';
	
	$strA .= '}';
	return $strA;
}

function datatable_aplicaciondispositivos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $paisGa, $idIdioma, $strFiltro, $perspectiva, $perspectiva_seg)
{

	global $trans;
	//Preparación de la dimensión a utilizar (Perspectiva)
	switch (strtoupper($perspectiva)) {

		/*APLICACIÓN*/
		case "VERSIONES":
			$strDimensions='ga:appVersion ';
			$strSort='appVersion ';
			$strNombre_Dimension = $trans->__('Versiones de la aplicación', false);
			break; 
		case "NOMAPLICACION":
			$strDimensions='ga:appName ';
			$strSort='appName ';
			$strNombre_Dimension = $trans->__('Nombres de la aplicación', false);
			break;
		case "INSTALADOR":
			$strDimensions='ga:appInstallerId';
			$strSort='appInstallerId';
			$strNombre_Dimension = $trans->__('Instalador', false);
			break;

		/*DISPOSITIVO*/
		case "TIPODISP":
			$strDimensions='ga:deviceCategory ';
			$strSort='deviceCategory ';
			$strNombre_Dimension = $trans->__('Tipo dispositivo', false);
			break; 
		case "VERSIONSIS":
			$strDimensions='ga:operatingSystem,ga:operatingSystemVersion';
			$strSort='operatingSystem';
			$strNombre_Dimension = $trans->__('Versión sistema operativo', false);
			break;
		case "MARCA":
			$strDimensions='ga:mobileDeviceBranding ';
			$strSort='mobileDeviceBranding ';
			$strNombre_Dimension = $trans->__('Marca', false);
			break;
		case "MODELO":
			$strDimensions='ga:MobileDeviceInfo ';
			$strSort='MobileDeviceInfo ';
			$strNombre_Dimension = $trans->__('Modelo', false);
			break;
		case "METODO":
			$strDimensions='ga:mobileInputSelector ';
			$strSort='mobileInputSelector ';
			$strNombre_Dimension = $trans->__('Método de entrada', false);
			break;
		case "RESOLUCION":
			$strDimensions='ga:screenResolution';
			$strSort='screenResolution';
			$strNombre_Dimension = $trans->__('Resolución pantalla', false);
			break;

		 
	}//Cierre switch 	

		//Preparación de la dimensión a utilizar (Perspectiva)
	if($perspectiva_seg!=""){
		switch (strtoupper($perspectiva_seg)) {

			case "VERSIONES":
				$strDimensions_seg ='ga:appVersion ';
				$strSort_seg ='appVersion ';
				$strNombre_Dimension_seg = $trans->__('Versiones de la aplicación', false);
				break; 
			case "NOMAPLICACION":
				$strDimensions_seg ='ga:appName ';
				$strSort_seg ='appName ';
				$strNombre_Dimension_seg = $trans->__('Nombres de la aplicación', false);
				break;
			case "INSTALADOR":
				$strDimensions_seg ='ga:appInstallerId';
				$strSort_seg ='appInstallerId';
				$strNombre_Dimension_seg = $trans->__('Instalador', false);
				break;

			/*DISPOSITIVO*/
			case "TIPODISP":
				$strDimensions_seg ='ga:deviceCategory ';
				$strSort_seg ='deviceCategory ';
				$strNombre_Dimension_seg = $trans->__('Tipo dispositivo', false);
				break; 
			case "VERSIONSIS":
				$strDimensions_seg ='ga:operatingSystem,ga:operatingSystemVersion';
				$strSort_seg ='operatingSystem';
				$strNombre_Dimension_seg = $trans->__('Versión sistema operativo', false);
				break;
			case "MARCA":
				$strDimensions_seg ='ga:mobileDeviceBranding ';
				$strSort_seg ='mobileDeviceBranding ';
				$strNombre_Dimension_seg = $trans->__('Marca', false);
				break;
			case "MODELO":
				$strDimensions_seg ='ga:MobileDeviceInfo ';
				$strSort_seg ='MobileDeviceInfo ';
				$strNombre_Dimension_seg = $trans->__('Modelo', false);
				break;
			case "METODO":
				$strDimensions_seg ='ga:mobileInputSelector ';
				$strSort_seg ='mobileInputSelector ';
				$strNombre_Dimension_seg = $trans->__('Método de entrada', false);
				break;
			case "RESOLUCION":
				$strDimensions_seg ='ga:screenResolution';
				$strSort_seg ='screenResolution';
				$strNombre_Dimension_seg = $trans->__('Resolución pantalla', false);
				break;

		}//Cierre switch 	
	}else{
		$strDimensions_seg='';
		$strSort_seg='';
		$strNombre_Dimension_seg = '';
	}

	if($perspectiva_seg != ""){
		$strDimensions .=  ",".$strDimensions_seg;
	}

	//return $strDimensions .=  ",".$perspectiva_seg;
	$strTipo = strtolower($perspectiva);
	$strTipo_seg = strtolower($perspectiva_seg);

	//Preparación de la ordenación
	$array_Ordenacion = array($strSort,$strSort_seg,'sessions','screenViews ','avgSessionDuration','screenViewsPerSession');
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	$filtro="";
	if($paisGa!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}
	
    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api -> startdate            = $datFechaInicioFiltro ;
    $obj_Datos_Api -> enddate              = $datFechaFinFiltro ;
    $obj_Datos_Api -> metrics              = 'ga:sessions, ga:screenViews, ga:avgSessionDuration, ga:screenViewsPerSession';
    if(empty($filtro)){
	    $obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api -> Construccion();
    //Objeto creado ---------------------------------------------------------------------------


    //Creación objeto, parametrización -------------------------------------------------------
    $obj_Datos_Api_ant = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
    $obj_Datos_Api_ant -> strProyectoAsociado  = $strProyectoAsociado ;
    $obj_Datos_Api_ant -> idVista              = 'ga:'. $idVistaAnalytics ;
    $obj_Datos_Api_ant -> startdate            = $strFechaInicioFiltroAnt ;
    $obj_Datos_Api_ant -> enddate              = $strFechaFinFiltroAnt ;
    $obj_Datos_Api_ant -> metrics              = 'ga:sessions, ga:screenViews, ga:avgSessionDuration, ga:screenViewsPerSession';
    if(empty($filtro)){
	    $obj_Datos_Api_ant -> optParams        = array(
											 'dimensions' => $strDimensions,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}else{
		$obj_Datos_Api_ant -> optParams        = array(
											 'dimensions' => $strDimensions,
											 'filters' => $filtro,
	                                         'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
											 'start-index' => $DataTables_start + 1,
	                                         'max-results' => $DataTables_length); 
	}
    $obj_Datos_Api_ant -> Construccion();

    $array_ant = array();
 	for ($i=1; $i<=$obj_Datos_Api_ant->NumValores(); $i++) {
 
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["perspectiva"]           = $obj_Datos_Api_ant->Valor($strSort,$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["perspectiva_seg"]       = $obj_Datos_Api_ant->Valor($strSort_seg,$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["sessions"]              = $obj_Datos_Api_ant->Valor("sessions",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["screenViews"]           = $obj_Datos_Api_ant->Valor("screenViews",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["avgSessionDuration"]    = $obj_Datos_Api_ant->Valor("avgSessionDuration",$i);
 		$array_ant[$obj_Datos_Api_ant->Valor($strSort,$i).$obj_Datos_Api_ant->Valor($strSort_seg,$i)]["screenViewsPerSession"] = $obj_Datos_Api_ant->Valor("screenViewsPerSession",$i);

 	}

	// Construimos un json con los datos

	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api->NumValoresReal() .','	;
	$strA .= '"data":[';
	for ($x=1; $x<=$obj_Datos_Api->NumValores(); $x++) {
		$strA .= '[';
		//El primer valor va a depender de la perspectiva que estamos pintando
		if($strTipo=="pais"){
			$strA .= '"<span class=\'izquierda_pri\'>'.Formato_Perspectiva('table',$strTipo,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x)).'</span>"';
		}else if($strTipo=="idiomauser" || $strTipo=="idiomanav"){
			$strA .= '"<span class=\'izquierda_pri\'>'.traducir_idioma($obj_Datos_Api->Valor($strSort,$x)).'</span>"';
		}else if($strTipo=="interes" || $strTipo=="region" || $strTipo=="ciudad"/**/){
			
			$strA .= '"<span class=\'izquierda_pri\'>'.traduce_de_ingles($obj_Datos_Api->Valor($strSort,$x),$_COOKIE["idioma_usuario"]).'</span>"';
		}else if($strTipo=="terminos"){
			$strA .= '"<span class=\'izquierda_pri\'>'.$obj_Datos_Api->Valor($strSort,$x).'</span>"';
		}else if($strTipo=="versionsis"){
			$strA .= '"<span class=\'izquierda_pri\'>'.$obj_Datos_Api->Valor('operatingSystem',$x).' / '.$obj_Datos_Api->Valor('operatingSystemVersion',$x).'</span>"';
		}else  if($strTipo=="tipodisp"){
			$strA .= '"<span class=\'izquierda_pri\'>'.Formato_Perspectiva('table','dispositivos',$x,$obj_Datos_Api).'</span>"';
		}else{
			$strA .= '"<span class=\'izquierda_pri\'>'.$obj_Datos_Api->Valor($strSort,$x).'</span>"';
		}
		$strA .= ",";

		if($perspectiva_seg != ""){
			if($strTipo_seg=="pais"){
				$strA .= '"<span class=\'izquierda\'>'.Formato_Perspectiva('table',$strTipo_seg,$x,$obj_Datos_Api,$obj_Datos_Api->ValorF("countryIsoCode",$x)).'</span>"';
			}else if($strTipo_seg=="idiomauser" || $strTipo_seg=="idiomanav"){
				$strA .= '"<span class=\'izquierda\'>'.traducir_idioma($obj_Datos_Api->Valor($strSort_seg,$x)).'</span>"';
			}else if($strTipo_seg=="interes" || $strTipo_seg=="region" || $strTipo_seg=="ciudad"/**/){
				
				$strA .= '"<span class=\'izquierda\'>'.traduce_de_ingles($obj_Datos_Api->Valor($strSort_seg,$x),$_COOKIE["idioma_usuario"]).'</span>"';
			}else if($strTipo_seg=="terminos"){
				$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->Valor($strSort_seg,$x).'</span>"';
			}else if($strTipo_seg=="versionsis"){
				$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->Valor('operatingSystem',$x).' / '.$obj_Datos_Api->Valor('operatingSystemVersion',$x).'</span>"';
			}else if($strTipo_seg=="tipodisp"){
				$strA .= '"<span class=\'izquierda\'>'.Formato_Perspectiva('table','dispositivos',$x,$obj_Datos_Api).'</span>"';
			}else{
				$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api->Valor($strSort_seg,$x).'</span>"';
			}
			$strA .= ",";
		}else{
			$strA .= '"",';
		}

		/*Buscamos los anteriores*/
		$itemQuantity_ant     = "0";
		$uniquePurchases_ant  = "0";
		$itemRevenue_ant      = "0";
		$revenuePerItem_ant   = "0";
		$itemsPerPurchase_ant = "0";

		if(isset($array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["perspectiva"])){
			$itemQuantity_ant     = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["sessions"];
			$uniquePurchases_ant  = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["screenViews"];
			$itemRevenue_ant      = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["avgSessionDuration"];
			$revenuePerItem_ant   = $array_ant[$obj_Datos_Api->Valor($strSort,$x).$obj_Datos_Api->Valor($strSort_seg,$x)]["screenViewsPerSession"];
		}


		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("sessions",$x),$itemQuantity_ant ,number_format( $obj_Datos_Api->Valor("sessions",$x)  ,0,',','.'),number_format( $itemQuantity_ant   ,0,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("sessions",$x)  ,0,',','.').'</span>",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("screenViews",$x),$uniquePurchases_ant ,number_format( $obj_Datos_Api->Valor("screenViews",$x)  ,0,',','.'),number_format( $uniquePurchases_ant   ,0,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("screenViews",$x)  ,0,',','.').'</span>",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("avgSessionDuration",$x),$itemRevenue_ant,number_format( $obj_Datos_Api->Valor("avgSessionDuration",$x)  ,2,',','.'),number_format( $itemRevenue_ant ,2,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("avgSessionDuration",$x)  ,2,',','.').'</span>",';

		$img_transactionRevenue = DevolverContenido_Total($obj_Datos_Api->Valor("screenViewsPerSession",$x),$revenuePerItem_ant,number_format( $obj_Datos_Api->Valor("screenViewsPerSession",$x)  ,2,',','.'),number_format( $revenuePerItem_ant  ,2,',','.'));
		$strA .= '"<span class=\'siempreizq\'>'.$img_transactionRevenue.number_format( $obj_Datos_Api->Valor("screenViewsPerSession",$x)  ,2,',','.').'</span>"';

		$strA .= ']';	  
		//Añadimos una "," si no es el último registro a pintar
		if ($x < $obj_Datos_Api->NumValores()) {
			$strA .= ','; 
		}
	}
	$strA .= ']}';
	
	//Devolvemos el json
	return $strA;
}

function campanas_aplicacion_escritorio_extra($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa, $idIdioma, $strFiltro){

	global $trans;

	$filtro="";
	if($paisGa!="todos"){
		$filtro .= 'ga:country=='.$paisGa;
	}
	if($idIdioma!="todos"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= 'ga:dimension1=='.$idIdioma;
	}
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	//Sacamos los dos mayorestipos de dispositivos
	$obj_Datos_Api_productos = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_productos -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_productos -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_productos -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_productos -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_productos -> metrics              = 'ga:screenviews';
	if(empty($filtro)){
		$obj_Datos_Api_productos -> optParams    = array(
										'dimensions' => 'ga:deviceCategory',
										'sort' => '-ga:screenviews',
										'start-index' => 1,
		                                'max-results' => 2);  
	}else{
		$obj_Datos_Api_productos -> optParams    = array(
										'dimensions' => 'ga:deviceCategory',
										'sort' => '-ga:screenviews',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 2);  
	}
	$obj_Datos_Api_productos -> Construccion();

	$pri = 0;

	for ($x=1; $x<=$obj_Datos_Api_productos->NumValores(); $x++) {

		if($pri==0){
			$resumen_nombre_producto1 = Formato_Perspectiva('table','dispositivos',$x,$obj_Datos_Api_productos);//$obj_Datos_Api_productos->Valor("deviceCategory",$x);
			$resumen_producto1 		  = $obj_Datos_Api_productos->Valor("screenviews",$x);
			$pri++;
		}else{
			$resumen_nombre_producto2 = Formato_Perspectiva('table','dispositivos',$x,$obj_Datos_Api_productos);//$obj_Datos_Api_productos->Valor("deviceCategory",$x);
			$resumen_producto2 		  = $obj_Datos_Api_productos->Valor("screenviews",$x);
		}
	}

    //Sacamos las dos mayores sistemas operativos 
	$obj_Datos_Api_marcas = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_marcas -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_marcas -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_marcas -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_marcas -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_marcas -> metrics              = 'ga:screenviews';
	if(empty($filtro)){
		$obj_Datos_Api_marcas -> optParams    = array(
										'dimensions' => 'ga:operatingSystem,ga:operatingSystemVersion',
										'sort' => '-ga:screenviews',
										'start-index' => 1,
		                                'max-results' => 2);  
	}else{
		$obj_Datos_Api_marcas -> optParams    = array(
										'dimensions' => 'ga:operatingSystem,ga:operatingSystemVersion',
										'sort' => '-ga:screenviews',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 2);  
	}
	$obj_Datos_Api_marcas -> Construccion();

	$pri = 0;

	for ($x=1; $x<=$obj_Datos_Api_marcas->NumValores(); $x++) {

		if($pri==0){
			$resumen_nombre_marca1 = $obj_Datos_Api_marcas->Valor("operatingSystem",$x).' / '.$obj_Datos_Api_marcas->Valor("operatingSystemVersion",$x);
			$resumen_marca1 	   = $obj_Datos_Api_marcas->Valor("screenviews",$x);
			$pri++;
		}else{
			$resumen_nombre_marca2 = $obj_Datos_Api_marcas->Valor("operatingSystem",$x).' / '.$obj_Datos_Api_marcas->Valor("operatingSystemVersion",$x);
			$resumen_marca2 	   = $obj_Datos_Api_marcas->Valor("screenviews",$x);
		}
	}

	


	$obj_Datos_Api_listas = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_listas -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_listas -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_listas -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_listas -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_listas -> metrics              = 'ga:screenviews';
	if(empty($filtro)){
		$obj_Datos_Api_listas -> optParams    = array(
												'dimensions' => 'ga:MobileDeviceInfo ',
												'sort' => '-ga:screenviews',
												'start-index' => 1,
		                                		'max-results' => 2);   
	}else{
		$obj_Datos_Api_listas -> optParams    = array(
												'dimensions' => 'ga:MobileDeviceInfo ',
												'sort' => '-ga:screenviews',
												'filters' => $filtro,
												'start-index' => 1,
		                                		'max-results' => 2);   
	}
	

	$obj_Datos_Api_listas -> Construccion();

	$pri = 0;

	for ($x=1; $x<=$obj_Datos_Api_listas->NumValores(); $x++) {

		if($pri==0){
			$resumen_nombre_listas1 = $obj_Datos_Api_listas->Valor("MobileDeviceInfo ",$x);
			$resumen_listas1 	    = $obj_Datos_Api_listas->Valor("screenviews",$x);
			$pri++;
		}else{
			$resumen_nombre_listas2 = $obj_Datos_Api_listas->Valor("MobileDeviceInfo ",$x);
			$resumen_listas2 	    = $obj_Datos_Api_listas->Valor("screenviews",$x);
		}
	}


	//TOTALES PARA sesiones
	$obj_Datos_Api_total = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_total -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_total -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_total -> startdate            = $datFechaInicioFiltro;
	$obj_Datos_Api_total -> enddate              = $datFechaFinFiltro;
	$obj_Datos_Api_total -> metrics              = 'ga:sessions, ga:screenviews, ga:avgSessionDuration, ga:screenviewsPerSession';
	if(empty($filtro)){
		$obj_Datos_Api_total -> optParams    = array(
										           'dimensions' => '',
										           'start-index' => 1,
		                                           'max-results' => 1);  
	}else{
		$obj_Datos_Api_total -> optParams    = array(
										           'dimensions' => '',
										           'filters' => $filtro,
										           'start-index' => 1,
		                                           'max-results' => 1);  
	}
	$obj_Datos_Api_total -> Construccion();


	for ($x=1; $x<=$obj_Datos_Api_total->NumValores(); $x++) {

		$total_nv   = $obj_Datos_Api_total->Valor("sessions",$x);
		$total_nuv  = $obj_Datos_Api_total->Valor("screenviews",$x);
		$total_tv   = $obj_Datos_Api_total->Valor("avgSessionDuration",$x);
		$total_pmpv = $obj_Datos_Api_total->Valor("screenviewsPerSession",$x);

	}


	$strA = '{';
	
	$strA .= '"total_nv": "' 	  . $total_nv	. '",';
	$strA .= '"total_nuv": "' 	  . $total_nuv 	. '",';
	$strA .= '"total_tv": "' 	  . $total_tv	. '",';
	$strA .= '"total_pmpv": "'    . $total_pmpv . '",';

	$strA .= '"total_nvF": "' 	  . number_format( $total_nv ,0,',','.')   . '",';
	$strA .= '"total_nuvF": "' 	  . number_format( $total_nuv ,0,',','.')  . '",';
	$strA .= '"total_tvF": "'     . number_format( $total_tv ,2,',','.')   . '",';
	$strA .= '"total_pmpvF": "'   . number_format( $total_pmpv ,2,',','.') . '",';

	$strA .= '"resumen_producto1": "'  . $resumen_producto1	 . '",';
	$strA .= '"resumen_producto2": "'  . $resumen_producto2  . '",';
	$strA .= '"resumen_marca1": "' 	   . $resumen_marca1	 . '",';
	$strA .= '"resumen_marca2": "'     . $resumen_marca2     . '",';
	$strA .= '"resumen_listas1": "' . $resumen_listas1    . '",';
	$strA .= '"resumen_listas2": "' . $resumen_listas2    . '",';

	$strA .= '"resumen_producto1F": "'  . number_format( $resumen_producto1 ,0,',','.')	 . '",';
	$strA .= '"resumen_producto2F": "' 	. number_format( $resumen_producto2 ,0,',','.')	 . '",';
	$strA .= '"resumen_marca1F": "'     . number_format( $resumen_marca1 ,0,',','.')	 . '",';
	$strA .= '"resumen_marca2F": "'     . number_format( $resumen_marca2 ,0,',','.')     . '",';
	$strA .= '"resumen_listas1F": "' . number_format( $resumen_listas1 ,0,',','.')    . '",';
	$strA .= '"resumen_listas2F": "' . number_format( $resumen_listas2 ,0,',','.')    . '",';

	$strA .= '"resumen_nombre_producto1": "'  . $resumen_nombre_producto1  . '",';
	$strA .= '"resumen_nombre_producto2": "'  . $resumen_nombre_producto2  . '",';
	$strA .= '"resumen_nombre_marca1": "'     . $resumen_nombre_marca1     . '",';
	$strA .= '"resumen_nombre_marca2": "'     . $resumen_nombre_marca2     . '",';
	$strA .= '"resumen_nombre_listas1": "' . $resumen_nombre_listas1    . '",';
	$strA .= '"resumen_nombre_listas2": "' . $resumen_nombre_listas2    . '"';
	
	$strA .= '}';
	return $strA;
}

function caracteristicas_datatable_sitioweb ( $strProyectoAsociado, $idVistaAnalytics, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir){

	global $trans;

	$filtro="";
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	//Preparación de la ordenación
	$array_Ordenacion = array('hostname','medium ','source','sessions','pageViewsPerSession','bounceRate');
	$DirOrdenacion='';
	if ($DataTables_OrderDir=='desc') {$DirOrdenacion='-';}

	//Calculamos las fecha actual y la de un año atras
	$hoy = date("Y-m-d");
	$haceanno = strtotime ( '-1 year' , strtotime ( $hoy ) ) ;
	$haceanno = date ( 'Y-m-d' , $haceanno );

	//BUSCAMOS EL FILTRO ANTIESPAN Y SI HAY CONCATENAMOS FILTRO
	$f = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);			
	$sqlf = " SELECT LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM, ";
	$sqlf .= " STUFF(( SELECT ';ga:hostname!@' + LOCDV_ANALYTICS_URL_VISTA ";
	$sqlf .= "		  FROM LOC_DOMINIOS_VISTAS INNER JOIN LOC_DOMINIOS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO ";
	$sqlf .= "		  WHERE LOCD_ANALYTICS_ID_PROPIEDAD=LD.LOCD_ANALYTICS_ID_PROPIEDAD   ";
	$sqlf .= "		  GROUP BY LOCDV_ANALYTICS_URL_VISTA FOR XML PATH('')) ,1,1,'') AS FILTRO_SPAM_HOST, ";
	$sqlf .= "  STUFF(( SELECT ',ga:source=@' + LOCSR_REFERER  ";
	$sqlf .= "		  FROM LOC_SPAM_REFERER FOR XML PATH('')) ,1,1,'') AS FILTRO_SPAM_REFERER ";
	$sqlf .= "  FROM LOC_DOMINIOS LD INNER JOIN LOC_DOMINIOS_VISTAS ON LD.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO ";
	$sqlf .= "  WHERE LOCDV_ANALYTICS_ID_VISTA='".$idVistaAnalytics."' ";
	$sqlf .= "  GROUP BY LOCD_ANALYTICS_ID_PROPIEDAD,LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM	";	

	
	$stmtf = $f->prepare($sqlf);
	$resultf = $stmtf->execute();
	
	if ($resultf && $stmtf->rowCount() != 0) {
	  	while ($filaf = $stmtf->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
			$antiespam = $filaf[0];
			$filtroa   = $filaf[1];
			$filtrob   = $filaf[2];
	  	}
	  	$filtro = "";

	  	    
		if(!empty($filtro)){
			$filtro = ";".$filtrob.",".$filtroa;
		}else{
			$filtro = $filtrob.",".$filtroa;
		}
		
	}

	//Sacamos los dos mayorestipos de dispositivos
	$obj_Datos_Api_spam = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_spam -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_spam -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_spam -> startdate            = $haceanno;
	$obj_Datos_Api_spam -> enddate              = $hoy;
	$obj_Datos_Api_spam -> metrics              = 'ga:sessions,ga:pageViewsPerSession,ga:bounceRate';
	$obj_Datos_Api_spam -> optForzarNousarFiltroSpam = 1;

	$obj_Datos_Api_spam -> optParams    = array(
										'dimensions' => 'ga:source,ga:medium,ga:hostname',
										'sort' => $DirOrdenacion.'ga:'.$array_Ordenacion[$DataTables_OrderColumn],
										'filters' => $filtro,
										'start-index' => $DataTables_start + 1,
		                                'max-results' => $DataTables_length);  
	
	$obj_Datos_Api_spam -> Construccion();


	//Objeto creado ---------------------------------------------------------------------------	
	$strA = '{"draw":' . $DataTables_draw .',';
	$strA .= '"recordsTotal":' . $obj_Datos_Api_spam->NumValoresReal() .',';
	$strA .= '"recordsFiltered":' . $obj_Datos_Api_spam->NumValoresReal() .',';
	$strA .= '"data":[';
	if($obj_Datos_Api_spam->NumValores()!=0){
	    for ($x=1; $x<=$obj_Datos_Api_spam->NumValores(); $x++) {
			$strA .= '[';
			$strA .= '"'.$obj_Datos_Api_spam->Valor("hostname",$x).'",';
			$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api_spam->Valor("medium",$x).'</span>",';
			$strA .= '"<span class=\'izquierda\'>'.$obj_Datos_Api_spam->Valor("source",$x).'</span>",';
			$strA .= '"'.$obj_Datos_Api_spam->ValorF("sessions",$x).'",';
			$strA .= '"'.$obj_Datos_Api_spam->ValorF("pageViewsPerSession",$x).'",';
			$strA .= '"'.$obj_Datos_Api_spam->ValorF("bounceRate",$x).'"';
			$strA .= '],';
	    }
	    $strA = substr($strA, 0, -1);
	}

    $strA .= ']}';


    return $strA;

}

function antispam_datosextra ($strProyectoAsociado, $idVistaAnalytics){

	global $trans;

	$filtro="";
	if ($strFiltro!="no"){
		if(!empty($filtro)){ $filtro .= ","; }
		$filtro .= $strFiltro;
	}

	//Calculamos las fecha actual y la de un año atras
	$hoy = date("Y-m-d");
	$haceanno = strtotime ( '-1 year' , strtotime ( $hoy ) ) ;
	$haceanno = date ( 'Y-m-d' , $haceanno );

	//BUSCAMOS EL FILTRO ANTIESPAN Y SI HAY CONCATENAMOS FILTRO
	$f = new PDO("sqlsrv:Server=".HOST.";Database=".DATABASE, USER, PASSWORD);			
	$sqlf = " SELECT LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM, ";
	$sqlf .= " STUFF(( SELECT ';ga:hostname!@' + LOCDV_ANALYTICS_URL_VISTA ";
	$sqlf .= "		  FROM LOC_DOMINIOS_VISTAS INNER JOIN LOC_DOMINIOS ON LOC_DOMINIOS.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO ";
	$sqlf .= "		  WHERE LOCD_ANALYTICS_ID_PROPIEDAD=LD.LOCD_ANALYTICS_ID_PROPIEDAD   ";
	$sqlf .= "		  GROUP BY LOCDV_ANALYTICS_URL_VISTA FOR XML PATH('')) ,1,1,'') AS FILTRO_SPAM_HOST, ";
	$sqlf .= "  STUFF(( SELECT ',ga:source=@' + LOCSR_REFERER  ";
	$sqlf .= "		  FROM LOC_SPAM_REFERER FOR XML PATH('')) ,1,1,'') AS FILTRO_SPAM_REFERER ";
	$sqlf .= "  FROM LOC_DOMINIOS LD INNER JOIN LOC_DOMINIOS_VISTAS ON LD.LOCD_CODIGO=LOC_DOMINIOS_VISTAS.LOCDV_SUDOMINIO ";
	$sqlf .= "  WHERE LOCDV_ANALYTICS_ID_VISTA='".$idVistaAnalytics."' ";
	$sqlf .= "  GROUP BY LOCD_ANALYTICS_ID_PROPIEDAD,LOCD_ANALYTICS_PROPIEDAD_FILTRO_ANTISPAM	";	

	//ga:source=@buttons-for-website.com,ga:source=@dailyrank.net,ga:hostname!@enkoa.com;ga:hostname!@enkoa.es	
	
	$stmtf = $f->prepare($sqlf);
	$resultf = $stmtf->execute();
	
	if ($resultf && $stmtf->rowCount() != 0) {
	  	while ($filaf = $stmtf->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
			$antiespam = $filaf[0];
			$filtroa   = $filaf[1];
			$filtrob   = $filaf[2];
	  	}
	  	$filtro = "";
	  	//if($antiespam == 1){
	  		//$filtro = $filtroa.";".$filtrob;
	  	//}
	  	    
		if(!empty($filtro)){
			$filtro = ";".$filtrob.",".$filtroa;
		}else{
			$filtro = $filtrob.",".$filtroa;
		}
		
	}

	//Sacamos los dos mayorestipos de dispositivos
	$obj_Datos_Api_spam = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_spam -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_spam -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_spam -> startdate            = $haceanno;
	$obj_Datos_Api_spam -> enddate              = $hoy;
	$obj_Datos_Api_spam -> metrics              = 'ga:sessions,ga:pageViewsPerSession,ga:bounceRate';
	$obj_Datos_Api_spam -> optForzarNousarFiltroSpam = 1;

	$obj_Datos_Api_spam -> optParams    = array(
										'dimensions' => 'ga:source,ga:medium,ga:hostname',
										'sort' => '-ga:sessions',
										'filters' => $filtro,
										'start-index' => 1,
		                                'max-results' => 1000);  
	
	$obj_Datos_Api_spam -> Construccion();
	$tot_sesiones 			  = 0;
	$tot_paginasvistassesion  = 0;
	$tot_tasarebote  		  = 0;
	$tot_sesionesF 			  = 0;
	$tot_paginasvistassesionF = 0;
	$tot_tasareboteF 		  = 0;
	if($obj_Datos_Api_spam->NumValores()!=0){
		$tot_sesiones 		      = $obj_Datos_Api_spam->total("sessions",$x);	
		$tot_paginasvistassesion  = $obj_Datos_Api_spam->total("pageViewsPerSession",$x);	
		$tot_tasarebote  		  = $obj_Datos_Api_spam->total("bounceRate",$x);	
		$tot_sesionesF 		      = $obj_Datos_Api_spam->totalF("sessions",$x);	
		$tot_paginasvistassesionF = $obj_Datos_Api_spam->totalF("pageViewsPerSession",$x);	
		$tot_tasareboteF  		  = $obj_Datos_Api_spam->totalF("bounceRate",$x);	
	}

	//Sacamos los dos mayorestipos de dispositivos
	$obj_Datos_Api_todo = new Datos_Api_Informes(); // Instanciamos la clase Datos_Api_Informes
	$obj_Datos_Api_todo -> strProyectoAsociado  = $strProyectoAsociado;
	$obj_Datos_Api_todo -> idVista              = "ga:".$idVistaAnalytics;
	$obj_Datos_Api_todo -> startdate            = $haceanno;
	$obj_Datos_Api_todo -> enddate              = $hoy;
	$obj_Datos_Api_todo -> metrics              = 'ga:sessions,ga:pageViewsPerSession,ga:bounceRate';
	$obj_Datos_Api_todo -> optForzarNousarFiltroSpam = 1;

	$obj_Datos_Api_todo -> optParams    = array(
										'dimensions' => 'ga:source,ga:medium,ga:hostname',
										'sort' => '-ga:sessions',
										'start-index' => 1,
		                                'max-results' => 1000);  
	
	$obj_Datos_Api_todo -> Construccion();
	$tot_tot_sesiones 			  = 0;
	$tot_tot_paginasvistassesion  = 0;
	$tot_tot_tasarebote  		  = 0;
	$tot_tot_sesionesF 			  = 0;
	$tot_tot_paginasvistassesionF = 0;
	$tot_tot_tasareboteF 		  = 0;
	if($obj_Datos_Api_todo->NumValores()!=0){
		$tot_tot_sesiones 		      = $obj_Datos_Api_todo->total("sessions",$x);	
		$tot_tot_paginasvistassesion  = $obj_Datos_Api_todo->total("pageViewsPerSession",$x);	
		$tot_tot_tasarebote  		  = $obj_Datos_Api_todo->total("bounceRate",$x);	
		$tot_tot_sesionesF 		      = $obj_Datos_Api_todo->totalF("sessions",$x);	
		$tot_tot_paginasvistassesionF = $obj_Datos_Api_todo->totalF("pageViewsPerSession",$x);	
		$tot_tot_tasareboteF  		  = $obj_Datos_Api_todo->totalF("bounceRate",$x);	
	}

	$porcspam = ($tot_sesiones * 100) / $tot_tot_sesiones;
	$strA = '{';
	
	$strA .= '"tot_sesiones": "' . $tot_sesiones	 . '",';
	$strA .= '"tot_paginasvistassesion": "' . $tot_paginasvistassesion  . '",';
	$strA .= '"tot_tasarebote": "' . $tot_tasarebote . '",';

	$strA .= '"tot_sesionesF": "' . $tot_sesionesF	 . ' ('.number_format($porcspam,2,',','.').'%)",';
	$strA .= '"tot_paginasvistassesionF": "' . $tot_paginasvistassesionF  . '",';
	$strA .= '"tot_tasareboteF": "' . $tot_tasareboteF . '",';

	$strA .= '"tot_tot_sesiones": "' . $tot_tot_sesiones	 . '",';
	$strA .= '"tot_tot_paginasvistassesion": "' . $tot_tot_paginasvistassesion  . '",';
	$strA .= '"tot_tot_tasarebote": "' . $tot_tot_tasarebote . '",';

	$strA .= '"tot_tot_sesionesF": "' . $tot_tot_sesionesF	 . '",';
	$strA .= '"tot_tot_paginasvistassesionF": "' . $tot_tot_paginasvistassesionF  . '",';
	$strA .= '"tot_tot_tasareboteF": "' . $tot_tot_tasareboteF . '"';
	
	$strA .= '}';

	return $strA;
}
?>


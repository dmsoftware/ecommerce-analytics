<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{


	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />

<!--totales-->
<input type="hidden" value="2" id="dat_sestot">

<div class="row">
	

	<style type="text/css">
		.posinfo{ float: right;}
		.panel-heading > .panel-title { width: 100%; }
		.cajadiv_dispo_table{ width: 50%; float:left;}
		.cajadiv_dispo_imgs{ width: 50%; float:left;}
		#cajamokup{ display: block; margin: auto; overflow: auto; position: relative; width: 400px; height: 320px;}
		#fondopri{ position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow-y:hidden; z-index: 5;}
		#imgescritorio{ position: absolute; top: 26px;left: 33px;width: 335px; display: block;margin: auto;z-index: 2;height: 250px; }
		#imgmovil{ position:absolute; top: 172px;left: 65px;width: 64px;display: block;margin: auto;z-index: 3;height: 113px;}
		.mimi{ display: none;}
	</style>	
	
</div>

<br />

<?php
if($primera_app==0){
	$pageviewsPerSession = 'Págs/ses';
}else{
	$pageviewsPerSession = 'Pant/ses';
}
?>
	
</form>	
<script src="../js/Chart.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!--******************DATATABLE DE PAÍS***************************-->
<div class="panel minimal minimal-gray" data-collapsed="0">
<div class="panel-heading">
	<div class="panel-title"><span><?php $trans->__('País'); ?></span></div>				
</div>

	<div class="cajadiv_dispo_table">

		<table class="table table-bordered datatable" id="table_datatable_pais">
			<thead>
				<tr>
					<th width="25%"><span class="cab_tit" id="Nombre_Dimension_pais"></span></th>
					<th width="36%"><?php $trans->__('Sesiones'); ?></th>
					<th width="27%"><?php $trans->__('Tas. rebote'); ?></th>
					<th width="8%%"><?php $trans->__('Dur/media'); ?></th>
					<th width="4%"><?php $trans->__($pageviewsPerSession); ?></th>
				</tr>	
			</thead>
		<tbody>
		</table>

	</div>
	<div class="cajadiv_dispo_imgs">
		<div id="pais_div" style="display:block; width: 80%; margin:auto"></div>
	</div>
</div>	
<!--**************************************************************-->


<?php

function porcentaje_sesiones($sessions,$sesiones_totales){

	return  number_format((($sessions*100)/$sesiones_totales), 2, ',', '');

}


?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".cab_tit").parent().css({
			"background-color": "transparent"
		})
	})
</script>
<!--******************DATATABLE DE REGIÓN***************************-->					
<div class="panel minimal minimal-gray" data-collapsed="0">
<div class="panel-heading">
	<div class="panel-title"><span><?php $trans->__('Región'); ?></span></div>				
</div>

	<div class="cajadiv_dispo_table">

		<table class="table table-bordered datatable" id="table_datatable_region">
			<thead>
				<tr>
					<th width="25%"><span class="cab_tit" id="Nombre_Dimension_region"></span></th>
					<th width="36%"><?php $trans->__('Sesiones'); ?></th>
					<th width="27%"><?php $trans->__('Tas. rebote'); ?></th>
					<th width="8%%"><?php $trans->__('Dur/media'); ?></th>
					<th width="4%"><?php $trans->__($pageviewsPerSession); ?></th>
				</tr>	
			</thead>
		<tbody>
		</table>

	</div>
	<div class="cajadiv_dispo_imgs">
		<div id="region_div" style="display:block; width: 80%; margin:auto"></div>
	</div>
</div>	
<!--**************************************************************-->
<!--******************DATATABLE DE CIUDAD***************************-->					
<div class="panel minimal minimal-gray" data-collapsed="0">
<div class="panel-heading">
	<div class="panel-title"><span><?php $trans->__('Ciudad'); ?></span></div>				
</div>

	<div class="cajadiv_dispo_table">

		<table class="table table-bordered datatable" id="table_datatable_ciudad">
			<thead>
				<tr>
					<th width="25%"><span class="cab_tit" id="Nombre_Dimension_ciudad"></span></th>
					<th width="36%"><?php $trans->__('Sesiones'); ?></th>
					<th width="27%"><?php $trans->__('Tas. rebote'); ?></th>
					<th width="8%"><?php $trans->__('Dur/media'); ?></th>
					<th width="4%"><?php $trans->__($pageviewsPerSession); ?></th>
				</tr>	
			</thead>
		<tbody>
		</table>

	</div>
	<div class="cajadiv_dispo_imgs">
		<div id="ciudad_div" style="display:block; width: 80%; margin:auto"></div>
	</div>
</div>	
<!--**************************************************************-->

</div>

<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">
<script src="<?=RUTA_ABSOLUTA?>js/geo-localizacion_scripts.js"></script>	

<? include("../includes/footer.php"); } ?>



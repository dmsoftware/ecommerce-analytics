<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{


	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />

<!--totales-->
<input type="hidden" value="2" id="dat_sestot">

<div class="row">
	

	<style type="text/css">
		.posinfo{ float: right;}
		.panel-heading > .panel-title { width: 100%; }
		.cajadiv_dispo_table{ width: 50%; float:left;}
		.cajadiv_dispo_tableb{ width: 100%; float:left;}
		.cajadiv_dispo_imgs{ width: 50%; float:left;}
		#cajamokup{ display: block; margin: auto; overflow: auto; position: relative; width: 400px; height: 320px;}
		#fondopri{ position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow-y:hidden; z-index: 5;}
		#imgescritorio{ position: absolute; top: 26px;left: 33px;width: 335px; display: block;margin: auto;z-index: 2;height: 250px; }
		#imgmovil{ position:absolute; top: 172px;left: 65px;width: 64px;display: block;margin: auto;z-index: 3;height: 113px;}
		.mimi{ display: none;}
		.listado_piecharts{ overflow: auto; overflow-y: hidden }
		.caja_tarta{ width:33.333%; float:left; height:auto; }
		.tartatit{ display: block; margin-top: 20px; text-align: center;}
		@media (min-width: 765px) and (max-width: 1200px) {

			.cajaconttarta { margin-left: -40px;}

		}
	</style>	
	
</div>

<br />
	
</form>	
<script src="../js/Chart.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<?php
if($primera_app==0){
	$pageviews = 'Págs. vistas';
	$pageviewss = 'Págs/ses';
	$idweb = 'Idiomas del sitio web';
	$idiweb2 = 'Idiomas de los usuarios e idiomas del sitio web';
	$idiweb3 = 'Idiomas visitados en el sitio web según el idioma que hablan los usuarios';
}else{
	$pageviews = 'Pant. vistas';
	$pageviewss = 'Pant/ses';
	$idiweb = 'Idiomas de la app';
	$idiweb2 = 'Idiomas de los usuarios e idiomas de la app';
	$idiweb3 = 'Idiomas visitados en la app según el idioma que hablan los usuarios';
}
?>
<!--******************DATATABLE DE PAÍS***************************-->
<div class="panel minimal minimal-gray" data-collapsed="0">
<div class="panel-heading">
	<div class="panel-title"><span><?php $trans->__('Idiomas que hablan los usuarios'); ?></span></div>				
</div>

	<div class="cajadiv_dispo_table">

		<table class="table table-bordered datatable" id="table_datatable_idiomauser">
			<thead>
				<tr>
					<th width="26%"><span class="cab_tit" id="Nombre_Dimension_idiomauser"></span></th>
					<th width="36%"><?php $trans->__($pageviews); ?></th>
					<th width="24%"><?php $trans->__('Tas. rebote'); ?></th>
					<th width="10%"><?php $trans->__('Dur/media'); ?></th>
					<th width="4%"><?php $trans->__($pageviewss); ?></th>
				</tr>	
			</thead>
		<tbody>
		</table>

	</div>
	<div class="cajadiv_dispo_imgs">
		<div id="idiomauser_div" style="display:block; width: 100%; margin:auto; padding-top:35px;"></div>
	</div>
</div>	
<!--**************************************************************-->


<?php

function porcentaje_sesiones($sessions,$sesiones_totales){

	return  number_format((($sessions*100)/$sesiones_totales), 2, ',', '');

}


//echo porcentaje_sesiones(1239,2413);
?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".cab_tit").parent().css({
			"background-color": "transparent"
		})
	})
</script>
<!--******************DATATABLE DE REGIÓN***************************-->					
<div class="panel minimal minimal-gray" data-collapsed="0">

	<div class="panel-heading">
		<div class="panel-title"><span><?php $trans->__($idiweb); ?></span></div>				
	</div>

	<div class="cajadiv_dispo_table">

		<table class="table table-bordered datatable" id="table_datatable_idiomanav">
			<thead>
				<tr>
					<th width="26%"><span class="cab_tit" id="Nombre_Dimension_idiomanav"></span></th>
					<th width="36%"><?php $trans->__($pageviews); ?></th>
					<th width="24%"><?php $trans->__('Tas. rebote'); ?></th>
					<th width="10%"><?php $trans->__('Dur/media'); ?></th>
					<th width="4%"><?php $trans->__($pageviewss); ?></th>
				</tr>	
			</thead>
		<tbody>
		</table>

	</div>
	<div class="cajadiv_dispo_imgs">
		<div id="idiomanav_div" style="display:block; width: 100%; margin:auto; padding-top:35px;"></div>
	</div>

</div>	
<!--**************************************************************-->

<!--******************DATATABLE DE REGIÓN***************************-->					
<div class="panel minimal minimal-gray" data-collapsed="0">

	<div class="panel-heading">
		<div class="panel-title"><span><?php $trans->__($idiweb2); ?></span></div>				
	</div>

	<div class="cajadiv_dispo_tableb" id="tabla_datatable_idioma">

		

	</div>
	<div class="cajadiv_dispo_imgs">
		<div id="idiomanav_div" style="display:block; width: 100%; margin:auto; padding-top:35px;"></div>
	</div>

</div>	
<!--**************************************************************-->

<!--******************LISTADO DE TARTAS***************************-->					
<div class="panel minimal minimal-gray" data-collapsed="0">

	<div class="panel-heading">
		<div class="panel-title"><span><?php $trans->__($idiweb3); ?></span></div>				
	</div>

	<div class="listado_piecharts">
		


	</div>

</div>	
<!--**************************************************************-->
</div>

<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">
<script src="<?=RUTA_ABSOLUTA?>js/idiomas_scripts.js"></script>	

<? include("../includes/footer.php"); } ?>



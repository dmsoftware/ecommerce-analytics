<?php include("../informes/funciones_ajax.php"); ?>

<table class="table table-bordered datatable table_datatable_idiomab" id="tablaidiomas"  style="margin-top:5px;">
	<thead>
		<?php
		$top = 20;
		$cab = $top+1;
		?>
		<tr>
			<th id="libre_caja"></th>
			<th colspan="<?=$cab?>" class="centrartexto"><?php $trans->__('Idioma de los usuarios'); ?></th>
		</tr>
		<tr>
		<th><?php $trans->__('Idioma web'); ?></th>

		<?php
		$filtro      = $_GET["strFiltro"];
		$proyectasco = $_GET["strProyectoAsociado"];
		$idVista     = $_GET["idVistaAnalytics"];
		$fechaIni    = $_GET["strFechaInicioFiltro"];
		$fechaFin    = $_GET["strFechaFinFiltro"];
		$strApp      = $_GET["strApp"];
		$strPais     = $_GET["strPais"];
		$strIdioma   = $_GET["strIdioma"];

		$filtrob="";	

		if ($filtro!="no"){
			if(!empty($filtrob)){ $filtrob .= ","; }
			$filtrob .= $filtro;
		}
		if($strPais!="todos"){
			if(!empty($filtrob)){ $filtrob .= ","; }
			$filtrob .= 'ga:country=='.$strPais;
		}
		if($strIdioma!="todos"){
			if(!empty($filtrob)){ $filtrob .= ","; }
			$filtrob .= 'ga:dimension1=='.$strIdioma;
		}
		//Montamos la table trayendo cabeceras
		$filtro = $filtrob;
		if( empty($filtro) ){
			$filtro = "no";
		}

		if($strApp == 0){
			$metricas = 'ga:pageviews';
		}else{
			$metricas = 'ga:screenviews';
		}

		//Arriba
		$obj_Datos_Api_top = new Datos_Api_Informes(); 
		$obj_Datos_Api_top -> strProyectoAsociado  = $proyectasco;
		$obj_Datos_Api_top -> idVista              ='ga:'. $idVista;
		$obj_Datos_Api_top -> startdate            = $fechaIni;
		$obj_Datos_Api_top -> enddate              = $fechaFin;
		$obj_Datos_Api_top -> metrics              = $metricas;
		if(empty($filtrob)){
			$obj_Datos_Api_top -> optParams            = array(
													'dimensions' => 'ga:dimension8',
													'sort' => '-'.$metricas,
													'start-index' => 1,
		                                            'max-results' => 1000); 
		}else{
			$obj_Datos_Api_top -> optParams            = array(
													'dimensions' => 'ga:dimension8',
													'sort' => '-'.$metricas,
													'filters' => $filtrob,
													'start-index' => 1,
		                                            'max-results' => 1000); 
		}
		 

		$obj_Datos_Api_top -> Construccion();

		
		$count = 0;

		for ($x=1; $x<=$obj_Datos_Api_top->NumValores(); $x++) {

			if($count < $top){
				echo "<th><span class='centrar_dch'>".$obj_Datos_Api_top->Valor("dimension8",$x)."</span></th>";
				$count++;
			}else if($count == $top){
				echo "<th>".$trans->__('Otros', false)."</th>";
				$count++;
			}
			
		}
		$count=0;
		?>

		</tr>	
	</thead>	
	<tbody>
		<?php
		$idiomas = tablaidioma_idioma ($proyectasco, $idVista, $fechaIni, $fechaFin, $filtro, $strApp);
		echo $idiomas;
		?>
	</tbody>
</table>



<style>
.datatableres{ margin-bottom: 2px; border:none;}
.datatableres th{ padding: 3px !important; font-size: 11px !important; color: white; text-align: center;} 
.datatableres td{ padding: 1px !important; font-size: 12px !important; text-align: center !important; border:none !important; color: white !important;}
.datatableres td:last-child{ text-align: left !important; font-size: 9px !important;}
.destacado{ background-color: #c5e5f6; min-height: 33px;}
.table_datatable_idioma tr td{ padding: 0px !important; }
.table_datatable_idioma tr td div{ padding: 8px !important; }
.cabimg{ padding: 0px !important;}
#libre_caja{ background-color: transparent !important; border-top: 1px solid #FFF !important; border-left: 1px solid #FFF !important;}
.centrartexto{text-align: center !important;}
#tablaidiomas tbody tr th { padding: 0px !important;}
.tdcans{ padding: 0px !important;}
.padingo{ padding: 9px;}
</style>
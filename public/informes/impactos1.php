<?php

if(!isset($_COOKIE["usuario"])) {

	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />


<div class="row">
	
	<div class="col-sm-6">
	
		<div class="tile-stats tile-white-gray" id="total_impactos">
			<div class="icon"><i class="entypo-user"></i></div>
			<div id="tilestats_total_sesiones" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="3000" data-delay="0">0</div>
			<h3><?php $trans->__('Impactos'); ?></h3>
			<p><span id="total_impactos_ico"></span>&nbsp;</p>
		</div>
	</div>
	
	<div class="col-sm-6">
	
		<div class="tile-stats tile-white-gray" id="total_rate">
			<div class="icon"><i class="entypo-monitor"></i></div>
			<div id="tilestats_total_paginasvistassesion" class="num" data-start="0" data-end="239" data-postfix="" data-duration="3000" data-delay="600">0</div>
			<h3><?php $trans->__('Ratio de conversiones'); ?></h3>
			<p><span id="total_rate_ico"></span>&nbsp;</p>
		</div>
		
	</div>
	
</div>

<br />


<div class="row">

	<div class="col-sm-12">

		<script src="../js/Chart.min.js"></script>
		<style type="text/css">

			input[type="radio"]{display: none;} 
			.grupo-perspectivas{ display: block; width: 100%; overflow: auto; clear: both; margin-bottom: 50px; }
			.caja-perspectiva{ float: left; width: auto;padding: 12px; }
			.totclass{ width: 150px !important;}
			.totclass img{ float: left;}
		</style>
		<div class="grupo-perspectivas">
			

			<div class="btn-group" data-toggle="buttons">
				<p><?php $trans->__('¿Bajo qué perspectiva deseas ver los datos?'); ?></p>
				
				<div class="caja-perspectiva">
					<p><?php $trans->__('Temporal'); ?></p>
					<label class="btn btn-white selecpers  active">
						<input type="radio" class="radiopers" name="perspectiva" value="Diario"><?php $trans->__('Diario'); ?>
					</label>
					<label class="btn btn-white selecpers " >
						<input type="radio" class="radiopers" name="perspectiva" value="Semanal"><?php $trans->__('Semanal'); ?>
					</label>
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Mensual"><?php $trans->__('Mensual'); ?>
					</label>
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Anual"><?php $trans->__('Anual'); ?>
					</label>
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Horas"><?php $trans->__('Horas del día'); ?>
					</label>

					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="DiasSemana"><?php $trans->__('Días de la semana'); ?>
					</label>
				</div>

				<div class="caja-perspectiva">
					<p><?php $trans->__('Geográfico'); ?></p>
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Pais" checked="checked"><?php $trans->__('País'); ?>
					</label>
					<label class="btn btn-white selecpers " >
						<input type="radio" class="radiopers" name="perspectiva" value="Region"><?php $trans->__('Región'); ?>
					</label>
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Ciudad"><?php $trans->__('Ciudad'); ?>
					</label>
				</div>

				<div class="caja-perspectiva">

					<p><?php $trans->__('Usuarios'); ?></p>
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Sexo"><?php $trans->__('Sexo'); ?>
					</label>
					<label class="btn btn-white selecpers " >
						<input type="radio" class="radiopers" name="perspectiva" value="Edad"><?php $trans->__('Edad'); ?>
					</label>

				</div>

				<div class="caja-perspectiva">

					<p><?php $trans->__('Tecnología'); ?></p>		
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Dispositivos"><?php $trans->__('Dispositivos'); ?>
					</label>
					<label class="btn btn-white selecpers " >
						<input type="radio" class="radiopers" name="perspectiva" value="Navegadores"><?php $trans->__('Navegadores'); ?>
					</label>
					

				</div>

				<div class="caja-perspectiva">

					<p><?php $trans->__('Adquisición'); ?></p>
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Fuente"><?php $trans->__('Fuente'); ?>
					</label>
					<label class="btn btn-white selecpers " >
						<input type="radio" class="radiopers" name="perspectiva" value="Medio"><?php $trans->__('Medio'); ?>
					</label>
					<label class="btn btn-white selecpers">
						<input type="radio" class="radiopers" name="perspectiva" value="Campanas"><?php $trans->__('Campañas'); ?>
					</label>

				</div>

			</div>

		</div>


	<script type="text/javascript">
	var perspectiva = "Diario";
		$(document).ready(function(){

			$(".selecpers").on("click",function(){

				pers = $(this).children().val();
				if(perspectiva!=pers){
					$("#dat_perspectiva").val(pers);
					perspectiva = pers;
					cargador();
				}
			
			})

		})	

	</script>

	<table class="table table-bordered datatable" id="table_datatable">
		<thead>
		<?php

		//BUSCAMOS LAS CABECERAS DEL DATATABLE

			include("funciones_ajax.php") ;

			$strProyectoAsociado    = $primera_proyectoasoc;
			$idVistaAnalytics		= $primera_vista;

			$elementos = Impactos1_resultados($strProyectoAsociado,$idVistaAnalytics);
			
			

			$devuelve = count($elementos["cabeceras"])*2;

			//Montamos una array para luego mostrarla
			$cabeceras = array();
			$cabeceras["cabeceras"]["visitas"]["desc"]    = $trans->__("Obtener visitas de calidad", false);
			$cabeceras["cabeceras"]["visitas"]["tam"]     = 0;
			$cabeceras["cabeceras"]["visitas"]["activo"]  = 0;
			$cabeceras["cabeceras"]["contactos"]["desc"]    = $trans->__("Obtener contactos", false);
			$cabeceras["cabeceras"]["contactos"]["tam"]     = 0;
			$cabeceras["cabeceras"]["contactos"]["activo"]  = 0;
			$cabeceras["cabeceras"]["contenido"]["desc"]    = $trans->__("Difundir contenido", false);
			$cabeceras["cabeceras"]["contenido"]["tam"]     = 0;
			$cabeceras["cabeceras"]["contenido"]["activo"]  = 0;
			$cabeceras["cabeceras"]["otros"]["desc"]    = $trans->__("Otros objetivos", false);
			$cabeceras["cabeceras"]["otros"]["tam"]     = 0;
			$cabeceras["cabeceras"]["otros"]["activo"]  = 0;
			//Primero debemos averiguar cuantas cabeceras recibimos y su tamaño
			foreach ($elementos["cabeceras"] as $key => $cabs) {
				if($cabs["nombre_corto"] == "VL" || $cabs["nombre_corto"] == "F" ||$cabs["nombre_corto"] == "BI" ||$cabs["nombre_corto"] == "D" ){
					$cabeceras["cabeceras"]["visitas"]["tam"] += 1;
					$cabeceras["cabeceras"]["visitas"]["activo"]  = 1;
				}else if($cabs["nombre_corto"] == "FC" || $cabs["nombre_corto"] == "CM" ||$cabs["nombre_corto"] == "TF"){
					$cabeceras["cabeceras"]["contactos"]["tam"] += 1;
					$cabeceras["cabeceras"]["contactos"]["activo"]  = 1;
				}else if($cabs["nombre_corto"] == "CI" || $cabs["nombre_corto"] == "CR" ||$cabs["nombre_corto"] == "FN"){
					$cabeceras["cabeceras"]["contenido"]["tam"] += 1;
					$cabeceras["cabeceras"]["contenido"]["activo"]  = 1;
				}else{
					//AQUI IRIAN LOS OTROS CONTEIDOS
					$cabeceras["cabeceras"]["otros"]["tam"] += 1;
					$cabeceras["cabeceras"]["otros"]["activo"]  = 1;
				}
				
			}


		?>
		<tr>
			<th rowspan="2"><span id="cab_perspectiva"></span></th>
			<?php
			//PINTAMOS LAS CABECERAS
			if($cabeceras["cabeceras"]["visitas"]["activo"]==1){
				?><th colspan="<?=$cabeceras["cabeceras"]["visitas"]["tam"]?>"><?=$cabeceras["cabeceras"]["visitas"]["desc"]?></th><?php
			}
			if($cabeceras["cabeceras"]["contactos"]["activo"]==1){
				?><th colspan="<?=$cabeceras["cabeceras"]["contactos"]["tam"]?>"><?=$cabeceras["cabeceras"]["contactos"]["desc"]?></th><?php
			}
			if($cabeceras["cabeceras"]["contenido"]["activo"]==1){
				?><th colspan="<?=$cabeceras["cabeceras"]["contenido"]["tam"]?>"><?=$cabeceras["cabeceras"]["contenido"]["desc"]?></th><?php
			}
			if($cabeceras["cabeceras"]["otros"]["activo"]==1){
				?><th colspan="<?=$cabeceras["cabeceras"]["otros"]["tam"]?>"><?=$cabeceras["cabeceras"]["otros"]["desc"]?></th><?php
			}
			?>				
			<th colspan="1" rowspan="2"><?php $trans->__('Totales'); ?></th>
		</tr>
		<tr>
			<?php
			//Recorriendo de nuevo la array deberian coincidir cabeceras con subcabeceras
			foreach ($elementos["cabeceras"] as $key => $cabs) {
				?><th colspan="1"><?=$cabs["nombre"]?></th><?php
			}
			?>


		</tr>		

	</thead>

	<tfoot>
		<tr class="filaTotales">
			<th><?php $trans->__('TOTALES'); ?></th>

			<?php
			$totales = count($elementos["cabeceras"]);
			for ($i=0; $i < $totales; $i++) { 
				?>
				<th id="totales<?=$i?>" class="totclass"></th>
				<?php
			}
			?>

			<!--OTROS Y TOTAL-->
			<th id="total_completion" class="totclass"></th>
		</tr>
	</tfoot>
	<tbody>
</table>

<script type="text/javascript">
	
	$(document).ready(function(){		

		t_datatable = $('#table_datatable').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[0,'asc']],
			"searching": false,
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				null,
			<?php
			$totcolumns = count($elementos["cabeceras"]);
			for ($i=0; $i <$totcolumns ; $i++) { 
				?>
				{"orderable": false },
				<?php
			}
			?>
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strPerspectiva=" + $("#dat_perspectiva").val() + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val(),
				"data": {"strFuncion": 'Impactos1_datatable'}
			},				
		});	

		

	})
</script>

</div>
<script type="text/javascript" src="<?=RUTA_ABSOLUTA?>js/jquery-number-master/jquery.number.min.js"></script>
<script src="<?=RUTA_ABSOLUTA?>js/impactos1_scripts.js"></script>
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



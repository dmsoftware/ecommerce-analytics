<?php 
//Seguridad. Sólo atendemos llamadas desde nuestro dominio ---------
$arrDominioReferer = preg_split('/\//',$_SERVER['HTTP_REFERER']);
$strDominioReferer = str_replace('www.', '', $arrDominioReferer[2]);
$strDominioMio = str_replace('www.', '', $_SERVER['SERVER_NAME']);
if ($strDominioReferer!=$strDominioMio) {print '>';exit();}
//------------------------------------------------------------------

include($_SERVER['DOCUMENT_ROOT']."/public/informes/funciones_ajax.php");

//Recogemos los parámetros comunes
$strFuncion				=  $_POST['strFuncion'];
$strPerspectiva			=  $_GET['strPerspectiva'];
$strProyectoAsociado	=  $_GET['strProyectoAsociado'];
$idVistaAnalytics		=  $_GET['idVistaAnalytics'];
$datFechaInicioFiltro	=  $_GET['strFechaInicioFiltro'];
$datFechaFinFiltro		=  $_GET['strFechaFinFiltro'];
$blnComparacion			=  $_GET['blnComparacion'];
$strFechaInicioFiltroAnt=  $_GET['strFechaInicioFiltroAnt'];
$strFechaFinFiltroAnt	=  $_GET['strFechaFinFiltroAnt'];
$idPaisga				=  $_GET['strPais'];
$idIdioma				=  $_GET['strIdioma'];
$filtro				    =  $_GET['strFiltro'];
$filtro = str_replace("**","==",$filtro);
if (isset($_GET['filtrochart'])) {
	$filtrochart = $_GET['filtrochart'];
	$filtrodimension = $_GET['filtrodimension'];
}else{
	$filtrochart = "";
	$filtrodimension = "";
}
$idIdioma				=  $_GET['strIdioma'];

//Recogemos los parámetros DATATABLES, si existen
$DataTables_draw 		= $_POST['draw'];
$DataTables_length 		= $_POST['length'];
//Si length es -1 ponemos 10.000 (máximo de google)
if ($DataTables_length==-1) {$DataTables_length=10000;}
$DataTables_start		= $_POST['start'];
$DataTables_OrderColumn	= $_POST['order'][0][column];
$DataTables_OrderDir	= $_POST['order'][0][dir];

//Para buscador
$buscador = $_POST['search'][value];

if(isset($_GET["strApp"])){
	$strApp = $_GET["strApp"];
}else{
	$strApp = 0;
}


//Comprobamos a qué función debemos llamar
switch ($strFuncion) {
	case "metricas_habituales_datatable":
		$resultado = metricas_habituales_datatable ($strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $_GET["strApp"]);		
		print ($resultado);
		break; 
	case "metricas_habituales_graficos":
		$resultadoFinal = metricas_habituales_graficos ($strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $_GET["strApp"]);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= metricas_habituales_graficos ($strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro, $_GET["strApp"]);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	
	//CAracteristicas
	case "caracteristicas_datatable_sexo":
		$resultado = caracteristicas_datatable ( "sexo",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_edades":
		$resultado = caracteristicas_datatable ( "edad",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "sesiones_totales":
		$resultado = sesiones_totales ( $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro);
		print ($resultado);
		break;  
	case "caracteristicas_graficos_sexo":
		$resultadoFinal = caracteristicas_graficos ("sexo", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos ("sexo", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	case "caracteristicas_graficos_edades":
		$resultadoFinal = caracteristicas_graficos ("edad", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos ("edad", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break;
	case "caracteristicas_graficos_interes":
		$resultadoFinal = caracteristicas_graficos ("interes", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos ("interes", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break;
	case "caracteristicas_datatable_intereses":
		$resultado = caracteristicas_datatable ( "interes",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break; 
	case "caracteristicas_graficos_bipolar_sexo":
		$resultadoFinal = caracteristicas_graficos_bipolar ("sexo", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos_bipolar ("sexo", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	case "caracteristicas_graficos_bipolar_edad":
		$resultadoFinal = caracteristicas_graficos_bipolar ("edad", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos_bipolar ("edad", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	case "caracteristicas_graficos_bipolar_interes":
		$resultadoFinal = caracteristicas_graficos_bipolar ("interes", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos_bipolar ("interes", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	case "caracteristicas_graficos_bipolar_dispositivos":
		$resultadoFinal = caracteristicas_graficos_bipolar ("dispositivos", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos_bipolar ("dispositivos", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	case "caracteristicas_graficos_bipolar_navegadores":
		$resultadoFinal = caracteristicas_graficos_bipolar ("navegadores", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos_bipolar ("navegadores", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 

	//dispositivos
	case "caracteristicas_datatable_dispositivos":
		$resultado = caracteristicas_datatable ( "dispositivos", $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_navegadores":
		$resultado = caracteristicas_datatable ( "navegadores", $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break; 
	case "caracteristicas_graficos_dispositivos":
		$resultadoFinal = caracteristicas_graficos ("dispositivos", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos ("dispositivos", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break;
	case "caracteristicas_graficos_navegadores":
		$resultadoFinal = caracteristicas_graficos ("navegadores", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= caracteristicas_graficos ("navegadores", $strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break;
	case "navegacion_mokup_imgs":
		$resultado = navegacion_mokup_imgs ($idVistaAnalytics);
		print ($resultado);
		break;

	//geo-localizacion
	case "caracteristicas_datatable_pais":
		$resultado = caracteristicas_datatable ( "pais",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_region":
		$resultado = caracteristicas_datatable ( "region",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_ciudad":
		$resultado = caracteristicas_datatable ( "ciudad",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "contenido_chart_pais":
		$resultado = contenido_geochart ( "pais", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);
		print ($resultado);
		break;
	case "contenido_chart_region":
		$resultado = contenido_geochart ( "region", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);
		print ($resultado);
		break;
	case "contenido_chart_ciudad":
		$resultado = contenido_geochart ( "ciudad", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);
		print ($resultado);
		break;
	case "contenido_chart_paisPrin":
		$resultado = contenido_geochart ( "paisPrin", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);
		print ($resultado);
		break;
	case "contener_iso":
		$resultado = contener_iso ($idPaisga);
		print ($resultado);
		break;

	//idiomas
	case "caracteristicas_datatable_idiomauser":
		$resultado = caracteristicas_datatable ( "idiomauser",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_idiomanav":
		$resultado = caracteristicas_datatable ( "idiomanav",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "contenido_chart_idiomauser":
		$resultado = contenido_piechart ( "idiomauser", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "contenido_chart_idiomanav":
		$resultado = contenido_piechart ( "idiomanav", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "comparar_idiomas":
		$resultado = comparar_idiomas ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;

	//fuentes de entrada
	case "caracteristicas_datatable_enlaces":
		$resultado = caracteristicas_datatable ( "enlaces", $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_buscadores":
		$resultado = caracteristicas_datatable ( "buscadores", $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_social":
		$resultado = caracteristicas_datatable ( "social", $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_campanas":
		$resultado = caracteristicas_datatable ( "campanas", $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "fuentestrafico_datos_extra_enlaces":
		$resultado = fuentestrafico_datos_extra ( "enlaces", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "fuentestrafico_datos_extra_social":
		$resultado = fuentestrafico_datos_extra ( "social", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "fuentestrafico_datos_extra_buscadores":
		$resultado = fuentestrafico_datos_extra ( "buscadores", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "fuentestrafico_datos_extra_campanas":
		$resultado = fuentestrafico_datos_extra ( "campanas", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "fuentestrafico_datos_extra":
		$resultadoFinal = fuentes_extra ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= fuentes_extra ($strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro, $strApp);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break;

	//Impactos
	case "Impactos1_datatable":
		$resultado = Impactos1_datatable ($strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro);		
		print ($resultado);
		break;
	case "Impactos1_datos_extra":
		$resultadoFinal = Impactos1_datos_extra ($strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= Impactos1_datos_extra ($strProyectoAsociado, $strPerspectiva, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	

	//Contenido_visto
	case "contenido_chart":
		$resultado = Contenido_chart ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);	
		print ($resultado);
		break; 	
	case "contenido_datatable_chart":
		$resultado = contenido_datatable_chart ($filtrochart,$filtrodimension,$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $blnComparacion,$strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strApp);	
		print ($resultado);
		break; 	
	case "contenido_datatable_chart_totales":
		$resultadoFinal .= contenido_datatable_chart_totales ($filtrochart,$filtrodimension,$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);	
		if ($blnComparacion=='true') {
		    $resultadoAuxiliar .= contenido_datatable_chart_totales ($filtrochart,$filtrodimension,$strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro, $strApp);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 	
	case "contenido_datatable_eventos_descargas":
		$resultado = contenido_datatable_eventos ("descarga",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro ,$strApp);	
		print ($resultado);
		break; 	
	case "contenido_datatable_eventos_busquedas":
		$resultado = contenido_datatable_eventos ("buscador",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro ,$strApp);	
		print ($resultado);
		break; 
	case "contenido_datatable_eventos_enlaces":
		$resultado = contenido_datatable_eventos ("externo",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro ,$strApp);	
		print ($resultado);
		break;
	case "contenido_cabeceras_idiomas":
		$resultado = contenido_cabeceras_idiomas ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);	
		print ($resultado);
		break;	
	case "contenido_datos_extra":
		$resultadoFinal = contenido_datos_extra ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $strApp);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= contenido_datos_extra ($strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro, $strApp);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	case "contenido_datatable_eventos_d4":
		$resultado = contenido_datatable_sustitucion_arbol("dimension4",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);	
		print ($resultado);
		break; 
	case "contenido_datatable_eventos_d5":
		$resultado = contenido_datatable_sustitucion_arbol("dimension5",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);	
		print ($resultado);
		break; 
	case "contenido_datatable_eventos_d6":
		$resultado = contenido_datatable_sustitucion_arbol("dimension6",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);	
		print ($resultado);
		break; 
	case "contenido_datatable_eventos_d7":
		$resultado = contenido_datatable_sustitucion_arbol("dimension7",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);	
		print ($resultado);
		break; 	

	//SEO
	case "seo_enlaces_externos":
		$resultado = seo_enlaces_externos($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro);	
		print ($resultado);
		break; 	
	case "table_datatable_enlaces_dominiostrafico":
		$resultado = table_datatable_enlaces_seo("dominiostrafico",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir);	
		print ($resultado);
		break;
	case "table_datatable_enlaces_enlacestrafico":
		$resultado = table_datatable_enlaces_seo("enlacestrafico",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir);	
		print ($resultado);
		break;
	case "table_datatable_enlaces_dominiosconversiones":
		$resultado = table_datatable_enlaces_seo("dominiosconversiones",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir);	
		print ($resultado);
		break;
	case "table_datatable_enlaces_enlacesconversiones":
		$resultado = table_datatable_enlaces_seo("enlacesconversiones",$strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir);	
		print ($resultado);
		break;
	case "table_datatable_terminoscontrol":
		$resultado = table_datatable_terminoscontrol($idVistaAnalytics,$_GET["vistaUrl"],$_GET["mesSelect"],$_GET["control"],$DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir,$_GET["pag"],$buscador,$_GET["tipo"]);	
		print ($resultado);
		break;
	case "table_datatable_pagsentradal":
		$resultado = table_datatable_pagsentradal($_GET["vistaUrl"],$_GET["mesSelect"],$_GET["control"],$DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir,$buscador);	
		print ($resultado);
		break;	

	//RESUMEN SEMANAL
	case "resumen_datatable_pais":
		$resultado = resumen_datatable ( "pais", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $filtro);
		print ($resultado);
		break;
	case "resumen_datatable_genero":
		$resultado = resumen_datatable ( "sexo", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $filtro);
		print ($resultado);
		break;
	case "resumen_datatable_edad":
		$resultado = resumen_datatable ( "edad", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $filtro);
		print ($resultado);
		break;
	case "resumen_piechart_sexo":
		$resultado = resumen_piechart ( "sexo", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $filtro, 0);
		print ($resultado);
		break;
	case "resumen_piechart_edad":
		$resultado = resumen_piechart ( "edad", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $filtro, 0);
		print ($resultado);
		break;
	case "resumen_datatable_trafico":
		$resultado = resumen_datatable_trafico ( "trafico", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $filtro);
		print ($resultado);
		break;
	case "resumen_piechart_trafico_sem":
		$resultado = resumen_chart_trafico ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $filtro, 0, 0);
		print ($resultado);
		break;
	case "resumen_piechart_trafico_anno":
		$resultado = resumen_chart_trafico ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $filtro, 1, 0);
		print ($resultado);
		break;
	case "resumen_datatable_idiomas":
		$resultado = resumen_datatable_idioma ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_length, $DataTables_start, $filtro );
		print ($resultado);
		break;
	case "resumen_datatable_conversiones":
		$resultado = resumen_datatable_conversiones ("datatable", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_length, $DataTables_start, $filtro );
		print ($resultado);
		break;
	case "resumen_datatable_conversiones_totales":
		$resultado = resumen_datatable_conversiones ("totales", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_length, $DataTables_start, $filtro );
		print ($resultado);
		break;
	case "resumen_datatable_conversiones_pais":
		$resultado = resumen_datatable_conversiones_paisregion ( "pais", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $filtro);
		print ($resultado);
		break;
	case "resumen_datatable_conversiones_region":
		$resultado = resumen_datatable_conversiones_paisregion ( "region", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $filtro);
		print ($resultado);
		break;
	//Totales del resumen
	case "resumen_datatable_conversiones_res":
		$resultado = resumen_datatable_conversiones_res ("datatable", $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_length, $DataTables_start, $filtro );
		print ($resultado);
		break;
	case "resumen_datatable_trafico_res":
		$resultado = resumen_datatable_trafico_res ( "trafico", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $filtro);
		print ($resultado);
		break;
	case "resumen_datatable_pais_res":
		$resultado = resumen_datatable_pais_res ( "pais", $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $filtro);
		print ($resultado);
		break;
	//campañas
	case "campanas_resumen_datatable":
		$resultado = campanas_datatable ("campanas",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $_GET["idPropiedad"]);
		print ($resultado);
		break;
	case "campanas_resumen_datatable_ecomerce":
		$resultado = campanas_datatable ("ecomerce",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $_GET["idPropiedad"]);
		print ($resultado);
		break;
	case "caracteristicas_datatable_terminos":
		$resultado = caracteristicas_datatable ( "terminos",$blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $strApp);
		print ($resultado);
		break;
	case "caracteristicas_datatable_terminos_conversiones":
		$resultado = terminos_convierten ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro);
		print ($resultado);
		break;
	//Campanas pedidos informes
	case "datatable_informes_pedidos":
		$resultado = datatable_informes_pedidos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);
		print ($resultado);
		break;
	case "campanas_informes_pedidos_extra":
		$resultadoFinal = campanas_informes_pedidos_extra ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= campanas_informes_pedidos_extra ($strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	//Campanas productos informes
	case "datatable_informes_productos":
		$resultado = datatable_informes_productos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);
		print ($resultado);
		break;
	case "campanas_informes_productos_extra":
		$resultadoFinal = campanas_informes_productos_extra ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= campanas_informes_productos_extra ($strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	//Campanas productos informes
	case "datatable_informes_embudos_pedidos":
		$resultado = datatable_informes_embudos_pedidos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);
		print ($resultado);
		break;
	case "datatable_informes_embudos_productos":
		$resultado = datatable_informes_embudos_productos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);
		print ($resultado);
		break;
	case "campanas_informes_embudos_pedidos_extra":
		$resultadoFinal = campanas_informes_embudos_pedidos_extra ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= campanas_informes_embudos_pedidos_extra ($strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	case "campanas_informes_embudos_productos_extra":
		$resultadoFinal = campanas_informes_embudos_productos_extra ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= campanas_informes_embudos_productos_extra ($strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	//APPS
	//aplicacion-dispositivos
	case "datatable_aplicaciondispositivos":
		$resultado = datatable_aplicaciondispositivos ( $blnComparacion, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);
		print ($resultado);
		break;
	case "campanas_aplicacion_escritorio_extra":
		$resultadoFinal = campanas_aplicacion_escritorio_extra ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);	
		//Si tenemos que comparar llamamos 2 veces a la función
		if ($blnComparacion=='true') {
			$resultadoAuxiliar .= campanas_aplicacion_escritorio_extra ($strProyectoAsociado, $idVistaAnalytics, $strFechaInicioFiltroAnt, $strFechaFinFiltroAnt, $idPaisga, $idIdioma, $filtro, $_GET["perspectiva"], $_GET["perspectiva_seg"]);	
			$resultadoFinal .= ',' . $resultadoAuxiliar;
		}
		print ('{"datos":['.$resultadoFinal.']}');
		break; 
	//Tabla antispam de sitioweb
	case "caracteristicas_datatable_sitioweb":
		$resultado = caracteristicas_datatable_sitioweb ( $strProyectoAsociado, $idVistaAnalytics, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir);
		print ($resultado);
		break;
	case "antispam_datosextra":
		$resultadoFinal = antispam_datosextra ($strProyectoAsociado, $idVistaAnalytics);	
		//Si tenemos que comparar llamamos 2 veces a la función
		print ('{"datos":['.$resultadoFinal.']}');
		break; 

	//Cuadro de mando
	case "cuadromando_datatable_sesiones":
		$resultado = cuadromando_datatable ('sesiones', $strProyectoAsociado, $idVistaAnalytics, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $idPaisga, $DataTables_draw, $DataTables_length, $DataTables_start, $DataTables_OrderColumn, $DataTables_OrderDir, $strApp);
		print ($resultado);
		break;
}
//contenido_datos_extra 
?>


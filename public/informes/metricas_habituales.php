<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<style type="text/css">
	

	div.DTTT_container {
			margin-top: -1px;
		}

</style>
<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />

<?php
if($primera_app==0){
	$pageviews = 'Páginas vistas';
	$pageviewsPerSession = 'Páginas vistas por sesión';
	$pantot = 'Páginas vistas por sesión';
	$numtotales = "3";
}else{
	$pageviews = 'Pantallas vistas';
	$pageviewsPerSession = 'Pantallas vistas por sesión';
	$pantot = 'Pantallas vistas por sesión';
	$numtotales = "4";
}
?>
<div class="row">
	
	<div class="col-sm-<?=$numtotales?>">
	
		<div class="tile-stats tile-white-gray">
			<div class="icon"><i class="entypo-user"></i></div>
			<div id="tilestats_total_sesiones" class="num" data-start="0" data-end="5000" data-postfix="" data-duration="1000" data-delay="0">0</div>
			<h3><?php $trans->__('Sesiones'); ?></h3>
			<p><span id="total_sesiones_icono"></span>&nbsp;</p>
		</div>
		
	</div>
	
	<div class="col-sm-<?=$numtotales?>">
	
		<div class="tile-stats tile-white-gray">
			<div class="icon"><i class="entypo-monitor"></i></div>
			<div id="tilestats_total_paginasvistassesion" class="num" data-start="0" data-end="239" data-postfix="" data-duration="1000" data-delay="600">0</div>
			<h3><?php $trans->__($pantot); ?></h3>
			<p><span id="total_paginasvistassesion_icono"></span>&nbsp;</p>
		</div>
		
	</div>

	<div class="col-sm-<?=$numtotales?>">
	
		<div class="tile-stats tile-white-gray" id="usercaja">
			
			<div id="tilestats_total_usuarios" class="num" data-start="0" data-prefix="" data-end="29" data-postfix="" data-duration="1000" data-delay="1200">0</div>
			<h3><?php $trans->__('Usuarios'); ?></h3>
			<p><span id="total_usuarios_icono"></span>&nbsp;</p>
			<div id="canvas-holder">
				<canvas id="chart-area" width="50" height="50"/>
			</div>
		</div>
		
	</div>
	<?php
	if($primera_app==0){
	?>
	<div class="col-sm-<?=$numtotales?>">
	
		<div class="tile-stats tile-white-gray">
			<div class="icon"><i class="entypo-flow-branch"></i></div>
			<div id="tilestats_total_rebote" class="num" data-start="0" data-end="57.6" data-postfix="%" data-duration="1000" data-delay="1800">0</div>
			<h3><?php $trans->__('Tasa de rebote'); ?></h3>
			<p><span id="total_tasarebote_icono"></span>&nbsp;</p>
		</div>
		
	</div>
	<?php
	}
	?>
	
</div>

<br /> <br /> <br />




<div class="row">

	<div class="col-sm-12">

	<script src="<?=RUTA_ABSOLUTA?>js/Chart.min.js"></script>
	<p><?php $trans->__('¿Bajo qué perspectiva deseas ver los datos?'); ?></p>

	<div class="btn-group" data-toggle="buttons">
		<label class="btn btn-white selecpers  active">
			<input type="radio" class="radiopers" name="perspectiva_option" value="Diario" checked="checked"><?php $trans->__('Diario'); ?>
		</label>
		<label class="btn btn-white selecpers " >
			<input type="radio" class="radiopers" name="perspectiva_option" value="Semanal"><?php $trans->__('Semanal'); ?>
		</label>
		<label class="btn btn-white selecpers">
			<input type="radio" class="radiopers" name="perspectiva_option" value="Mensual"><?php $trans->__('Mensual'); ?>
		</label>
		<label class="btn btn-white selecpers">
			<input type="radio" class="radiopers" name="perspectiva_option" value="Anual"><?php $trans->__('Anual'); ?>
		</label>
		<label class="btn btn-white selecpers">
			<input type="radio" class="radiopers" name="perspectiva_option" value="Horas"><?php $trans->__('Horas del día'); ?>
		</label>

		<label class="btn btn-white selecpers">
			<input type="radio" class="radiopers" name="perspectiva_option" value="DiasSemana"><?php $trans->__('Días de la semana'); ?>
		</label>
	</div>
	<script type="text/javascript">
	var perspectiva = "Diario";
		$(document).ready(function(){

			$(".selecpers").on("click",function(){

				pers = $(this).children().val();
				if(perspectiva!=pers){
					$("#dat_perspectiva").val(pers);
					perspectiva = pers;
					cargador();
				}
			
			})

		})	

	</script>


	<table class="table table-bordered datatable" id="table_datatable">
		<thead>
			<tr>
				<th rowspan="2"><span id="Nombre_Dimension"><?php $trans->__('Día'); ?></span></th>
				<th colspan="3"><?php $trans->__('Tráfico'); ?></th>
				<th colspan="3"><?php $trans->__('Usuarios'); ?></th>
				<th colspan="2"><?php $trans->__('Calidad'); ?></th>
			</tr>	
			<tr>
				<th><?php $trans->__('Sesiones'); ?></th>
				<th><?php $trans->__($pageviews); ?></th>
				<th><?php $trans->__($pageviewsPerSession); ?></th>
				<th><?php $trans->__('Usuarios'); ?></th>
				<th><?php $trans->__('Nuevos'); ?></th>
				<th><?php $trans->__('Recurrentes'); ?></th>
				<th><?php $trans->__('Tasa de rebote'); ?></th>
				<th><?php $trans->__('Duración media de la sesión'); ?></th>	
			</tr>
		</thead>
		
		<tfoot>
			<tr>
				<th><?php $trans->__('TOTALES'); ?></th>
				<th><span id="total_sesiones"></span></th>
				<th><span id="total_paginasvistas"></th>
				<th><span id="total_paginasvistassesion"></th>
				<th><span id="total_usuarios"></span></th>
				<th><span id="total_usuariosnuevos"></span></th>
				<th><span id="total_usuariosrecurrentes"></span></th>
				<th><span id="total_tasarebote"></span></th>
				<th><span id="total_duracionmediasesion"></span></th>
			</tr>
			<tr>
				<th><?php $trans->__('MEDIAS'); ?></th>
				<th><span id="media_sesiones"></span></th>
				<th><span id="media_paginasvistas"></th>
				<th><span id="media_paginasvistassesion"></th>
				<th><span id="media_usuarios"></span></th>
				<th><span id="media_usuariosnuevos"></span></th>
				<th><span id="media_usuariosrecurrentes"></span></th>
				<th><span id="media_tasarebote"></span></th>
				<th><span id="media_duracionmediasesion"></span></th>
			</tr>		
			<tr>
				<td><?php $trans->__('EVOLUCIÓN'); ?></td>
				<td>
					<div id="rickshaw_sesiones"></div>	
				</td>
				<td>
					<div id="rickshaw_paginasvistas"></div>			
				</td>
				<td>
					<div id="rickshaw_paginasvistassesion"></div>			
				</td>			
				<td>
					<div id="rickshaw_usuarios"></div>			
				</td>
				<td>
					<div id="rickshaw_usuariosnuevos"></div>				
				</td>
				<td>
					<div id="rickshaw_usuariosrecurrentes"></div>				
				</td>
				<td>
					<div id="rickshaw_tasarebote"></div>
				</td>
				<td>
					<div id="rickshaw_duracionmediasesion"></div>
				</td>
			</tr>
		</tfoot>
	<tbody>
	</table>


	</div>
</div>

<script src="<?=RUTA_ABSOLUTA?>js/metricas_habituales_scripts.js"></script>	
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php");  }?>



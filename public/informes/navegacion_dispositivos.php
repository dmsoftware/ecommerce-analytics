<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side.php"); 
	
?>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />
<style type="text/css">

	.cajaimgtot{ display: block;}
	.cajaimgtot img{ display: block;margin: auto; width: 70px; margin-top: 15px;}
	.nomtot{ text-align: center;}
</style>
<!--totales-->
<input type="hidden" value="2" id="dat_sestot">

<div class="row">
	
	<div class="col-sm-6">
		<div class="col-sm-12">
		
			<div class="tile-stats tile-white-gray">
				<!--<div class="icon"><i class="entypo-user"></i></div>-->
				<div id="tilestats_total_dispositivos" class="num" data-start="0" data-end="5000" data-postfix="%" data-duration="3000" data-delay="0">0</div>
				<h3 id="txt_dispositivos_nombre"></h3>
				<p><span id="total_dispositivos_ico"></span>&nbsp;</p>
				<p><span id="total_dispositivos_icono"></span>&nbsp;</p>
			</div>

		</div>

		<div class="col-sm-12">
		
			<div class="tile-stats tile-white-gray">
				<!--<div class="icon"><i class="entypo-user-add"></i></div>-->
				<div class="col-sm-4">
					<div id="tilestats_total_navegadores_primero" class="num" data-start="0" data-prefix="" data-end="29" data-postfix="" data-duration="1500" data-delay="1800">0</div>
					<h3 id="txt_navegadores_nombre_primero" class="nomtot"></h3>
					<div id="img_primero" class="cajaimgtot"></div>
					<p><span id="total_navegadores_ico_primero" class="pinfs"></span>&nbsp;</p>
					<p><span id="total_navegadores_icono_primero"></span>&nbsp;</p>
				</div>
				<div class="col-sm-4">
					<div id="tilestats_total_navegadores_segundo" class="num" data-start="0" data-prefix="" data-end="29" data-postfix="" data-duration="1500" data-delay="1800">0</div>
					<h3 id="txt_navegadores_nombre_segundo" class="nomtot"></h3>
					<div id="img_segundo" class="cajaimgtot"></div>
					<p><span id="total_navegadores_ico_segundo" class="pinfs"></span>&nbsp;</p>
					<p><span id="total_navegadores_icono_segundo"></span>&nbsp;</p>
				</div>
				<div class="col-sm-4">
					<div id="tilestats_total_navegadores_tercero" class="num" data-start="0" data-prefix="" data-end="29" data-postfix="" data-duration="1500" data-delay="1800">0</div>
					<h3 id="txt_navegadores_nombre_tercero" class="nomtot"></h3>
					<div id="img_tercero" class="cajaimgtot"></div>
					<p><span id="total_navegadores_ico_tercero" class="pinfs"></span>&nbsp;</p>
					<p><span id="total_navegadores_icono_tercero"></span>&nbsp;</p>
				</div>
				
			</div>
			
		</div>
	</div>
	<div class="col-sm-6">
		<!--Traer de la api imgs-->
		<div id="cajamokup">
		    <img id="fondopri" src="../images/mokup.png" /><br>
		    <div id="capaimgs_carga">
				<img id="imgescritorio"  src="../images/cargando_escritorio.png" /><br>
				<img id="imgmovil" src="../images/cargando_movil.png" /><br>
			</div>
		</div>
	</div>
	<style type="text/css">
		.pinfs{margin-top: -21px;display: block;}
		.posinfo{ float: right;}
		.panel-heading > .panel-title { width: 100%; }
		.cajadiv_dispo_table{ width: 60%; float:left;}
		.cajadiv_dispo_imgs{ width: 40%; float:left;}
		#cajamokup{ display: block; margin: auto; overflow: auto; position: relative; width: 400px; height: 320px;}
		#fondopri{ position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow-y:hidden; z-index: 5;}
		#imgescritorio{ position: absolute; top: 26px;left: 33px;width: 335px; display: block;margin: auto;z-index: 2;height: 250px; }
		#imgmovil{ position:absolute; top: 172px;left: 65px;width: 64px;display: block;margin: auto;z-index: 3;height: 113px;}
		.mimi{ display: none;}
		.tile-stats .num {
    font-size: 25px;
    font-weight: bold;
}

	@media (min-width: 765px) and (max-width: 1330px) {

		.grafcont { padding-left: 0px !important;}
		.tile-stats .num { margin-top: 10px;}
		.pinfs {
			    margin-top: -28px;
			    display: block;
			    margin-right: -17px;
			}
		#txt_navegadores_nombre_tercero{ font-size: 13px;}

	}
	</style>	
	
</div>

<br />
	
</form>	
<script src="../js/Chart.min.js"></script>

<!--******************DATATABLE DE Dispositivos***************************-->
<div class="panel minimal minimal-gray" data-collapsed="0">
	<div class="panel-heading">
		<div class="panel-title"><span><?php $trans->__('Dispositivos de navegación'); ?></span></div>				
	</div>
	
	<div class="col-sm-7">

		<table class="table table-bordered datatable" id="table_datatable_dispositivos">
			<thead>
				<tr>
					<th width="31%"><span class="cab_tit" id="Nombre_Dimension_dispositivo"></span></th>
					<th width="30%"><?php $trans->__('Sesiones'); ?></th>
					<th width="20%"><?php $trans->__('Tas. rebote'); ?></th>
					<th width="13%"><?php $trans->__('Dur/media'); ?></th>
					<th width="6%"><?php $trans->__('Págs/ses'); ?></th>
				</tr>	
			</thead>
		<tbody>
		</table>

	</div>
	<div class="col-sm-5 grafcont">
		
		<div class="caja_gráfico">
			<!--Gráfico de dispositivos-->
			<canvas id="cvs_dispositivos" width="400" height="130"><?php $trans->__('[No canvas support]'); ?></canvas>
			<p class="periodos"><span class="periodo anterior"><?php $trans->__('Periodo anterior'); ?></span><span class="periodo actual"><?php $trans->__('Periodo actual'); ?></span></p>
		</div>

	</div>
	
</div>	
<!--**************************************************************-->
<style type="text/css">

	.periodos{display: block; overflow: auto; width: 400px; margin-bottom: 50px;}
	.periodo{ width: 50%; float: left; text-align: center;}
	.actual{ color: #37bcf8}
	.anterior{ color: #fa8011}	

</style>

<?php

function porcentaje_sesiones($sessions,$sesiones_totales){

	return  number_format((($sessions*100)/$sesiones_totales), 2, ',', '');

}


//echo porcentaje_sesiones(1239,2413);
?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".cab_tit").parent().css({
			"background-color": "transparent"
		})
	})
</script>
<!--******************DATATABLE DE INTERESES***************************-->					
<div class="panel minimal minimal-gray" data-collapsed="0">
	<div class="panel-heading">
			<div class="panel-title"><span><?php $trans->__('Navegador'); ?></span></div>				
	</div>
	<div class="col-sm-7">
		<table class="table table-bordered datatable" id="table_datatable_navegadores">
			<thead>
				<tr>
					<th width="31%"><span class="cab_tit" id="Nombre_Dimension_navegadores"></span></th>
					<th width="30%"><?php $trans->__('Sesiones'); ?></th>
					<th width="20%"><?php $trans->__('Tas. rebote'); ?></th>
					<th width="13%"><?php $trans->__('Dur/media'); ?></th>
					<th width="6%"><?php $trans->__('Págs/ses'); ?></th>
				</tr>	
			</thead>

		<tbody>
		</table>
	</div>
	<div class="col-sm-5 grafcont">
	<!--Gráfico de navegadores-->
		<canvas id="cvs_navegadores" width="400" height="250" style="margin-top: -20px;"><?php $trans->__('[No canvas support]'); ?></canvas>
		<p class="periodos"><span class="periodo anterior"><?php $trans->__('Periodo anterior'); ?></span><span class="periodo actual"><?php $trans->__('Periodo actual'); ?></span></p>
	</div>	
</div>
<!--**************************************************************-->

</div>

<script type="text/javascript" src="<?=RUTA_ABSOLUTA?>js/RGraph/libraries/RGraph.common.core.js" ></script>
<script type="text/javascript" src="<?=RUTA_ABSOLUTA?>js/RGraph/libraries/RGraph.bipolar.js" ></script>
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">
<script src="<?=RUTA_ABSOLUTA?>js/navegacion_dispositivos_scripts.js"></script>	

<? include("../includes/footer.php"); } ?>



	var table_datatable_embudos;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	var dataBakRelleno=false;
	var dataBak = [];
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion();			
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	})//.trigger('change');		
	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();
		//alert($("#dat_comparador").val())
		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	function Recargar_Datatable() {
		//alert(blnComparacion)
		$('#table_datatable_embudos').dataTable().api().ajax.url('../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&perspectiva=" + $("#perspectiva").val() + "&perspectiva_seg=" + $("#perspectiva_seg").val() + "&idpropiedad="+$("#dat_propiedad").val() ).load();
	}
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------

	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico,sub){
		if(ValorAnt != ''){
			if (parseFloat(Valor) > parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + sub+'</div>';
			} else {
				if (parseFloat(Valor) == parseFloat(ValorAnt)) {
					// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
					strAux = '<div class="gris comparador"> ' + ValorAntF + sub+'</div>';
				} else {
					// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
					strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + sub+'</div>';
				}
			}
			if (blnSoloGrafico) {
				return strAux
			} else {
				return strAux + ' ' + ValorF;
			}
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}




	function formato_numero(numero, decimales, separador_decimal, separador_miles){     
		numero=parseFloat(numero);     
		if(isNaN(numero)){         
			return "";}     
		if(decimales!==undefined){        
		 	// Redondeamos         
			numero=numero.toFixed(decimales);     
		}     
		// Convertimos el punto en separador_decimal     
		numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
		if(separador_miles){         
			// Añadimos los separadores de miles         
			var miles=new RegExp("(-?[0-9]+)([0-9]{3})");         
			while(miles.test(numero)) {             
				numero=numero.replace(miles, "$1" + separador_miles + "$2");         
			}     
		}     
		return numero; 
	}

			

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		Leer_Opcion_Comparacion();	
		Recargar_Datatable();
		cargar_datos_extras()
	}

	function segundaperspectiva(){
		if( $("#perspectiva_seg").val() != "" ){

			table_datatable_embudos.column( 1 ).visible( true );
			cargador();

		}else{

			table_datatable_embudos.column( 1 ).visible( false );
			cargador();			

		}

		
	}


	function DevolverContenido_Total_img(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			strAux = '<img src="../images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				strAux = '<img src="../images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			} else {
				strAux = '<img src="../images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}


	function cargar_datos_extras(){

		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&idpropiedad="+$("#dat_propiedad").val() + "&perspectiva=" + $("#perspectiva").val() + "&perspectiva_seg=" + $("#perspectiva_seg").val(),
			data: { strFuncion: 'campanas_informes_embudos_pedidos_extra'}
		})
		.done(function (respuesta) {
			var objJson = jQuery.parseJSON(respuesta);
			$("#resumen_conversion").html( DevolverContenido_Total( parseFloat(objJson.datos[0].total_tasa_conversion).toFixed(2),parseFloat(objJson.datos[1].total_tasa_conversion).toFixed(2),objJson.datos[0].total_tasa_conversionf,objJson.datos[1].total_tasa_conversionf,false,'%')+'%' );
			$("#resumen_sesiones").html( DevolverContenido_Total( objJson.datos[0].total_sesiones,objJson.datos[1].total_sesiones,objJson.datos[0].total_sesionesf,objJson.datos[1].total_sesionesf,false,'')+' '+objJson.datos[0].total_sesiones_porcentages );
			$("#resumen_vistasproducto").html( DevolverContenido_Total( parseFloat(objJson.datos[0].total_vista_producto).toFixed(2),parseFloat(objJson.datos[1].total_vista_producto).toFixed(2),objJson.datos[0].total_vista_productof,objJson.datos[1].total_vista_productof,false,'')+' '+objJson.datos[0].total_vista_producto_porcentages );
			$("#resumen_anadircarrito").html( DevolverContenido_Total( parseFloat(objJson.datos[0].total_añadir_carrito).toFixed(2),parseFloat(objJson.datos[1].total_añadir_carrito).toFixed(2),objJson.datos[0].total_añadir_carritof,objJson.datos[1].total_añadir_carritof,false,'')+' '+objJson.datos[0].total_añadir_carrito_porcentages );
			$("#resumen_loginregistro").html( DevolverContenido_Total( parseFloat(objJson.datos[0].total_login_registro).toFixed(2),parseFloat(objJson.datos[1].total_login_registro).toFixed(2),objJson.datos[0].total_login_registrof,objJson.datos[1].total_login_registrof,false,'')+' '+objJson.datos[0].total_login_registro_porcentages );
			$("#resumen_revisarpedido").html( DevolverContenido_Total( objJson.datos[0].total_revisar_pedido,objJson.datos[1].total_revisar_pedido,objJson.datos[0].total_revisar_pedidof,objJson.datos[1].total_revisar_pedidof,false,'')+' '+objJson.datos[0].total_revisar_pedido_porcentages );
			$("#resumen_finalizarpedido").html( DevolverContenido_Total( parseFloat(objJson.datos[0].total_finalizar_pedido).toFixed(2),parseFloat(objJson.datos[1].total_finalizar_pedido).toFixed(2),objJson.datos[0].total_finalizar_pedidof,objJson.datos[1].total_finalizar_pedidof,false,'')+' '+objJson.datos[0].total_finalizar_pedido_porcentages );
			$("#resumen_pagorealizado").html( DevolverContenido_Total( parseFloat(objJson.datos[0].total_pago_realizado).toFixed(2),parseFloat(objJson.datos[1].total_pago_realizado).toFixed(2),objJson.datos[0].total_pago_realizadof,objJson.datos[1].total_pago_realizadof,false,'')+' '+objJson.datos[0].total_pago_realizado_porcentages );

		

			$("#total_ts").html( DevolverContenido_Total_img( parseFloat(objJson.datos[0].total_tasa_conversion).toFixed(2),parseFloat(objJson.datos[1].total_tasa_conversion).toFixed(2),objJson.datos[0].total_tasa_conversionf,objJson.datos[1].total_tasa_conversionf,false,'%')+'%' );
			$("#total_s").html( DevolverContenido_Total_img( objJson.datos[0].total_sesiones,objJson.datos[1].total_sesiones,objJson.datos[0].total_sesionesf,objJson.datos[1].total_sesionesf,false,'')+' '+objJson.datos[0].total_sesiones_porcentages );
			$("#total_vp").html( DevolverContenido_Total_img( parseFloat(objJson.datos[0].total_vista_producto).toFixed(2),parseFloat(objJson.datos[1].total_vista_producto).toFixed(2),objJson.datos[0].total_vista_productof,objJson.datos[1].total_vista_productof,false,'')+' '+objJson.datos[0].total_vista_producto_porcentages );
			$("#total_ac").html( DevolverContenido_Total_img( parseFloat(objJson.datos[0].total_añadir_carrito).toFixed(2),parseFloat(objJson.datos[1].total_añadir_carrito).toFixed(2),objJson.datos[0].total_añadir_carritof,objJson.datos[1].total_añadir_carritof,false,'')+' '+objJson.datos[0].total_añadir_carrito_porcentages );
			$("#total_lr").html( DevolverContenido_Total_img( parseFloat(objJson.datos[0].total_login_registro).toFixed(2),parseFloat(objJson.datos[1].total_login_registro).toFixed(2),objJson.datos[0].total_login_registrof,objJson.datos[1].total_login_registrof,false,'')+' '+objJson.datos[0].total_login_registro_porcentages );
			$("#total_rp").html( DevolverContenido_Total_img( objJson.datos[0].total_revisar_pedido,objJson.datos[1].total_revisar_pedido,objJson.datos[0].total_revisar_pedidof,objJson.datos[1].total_revisar_pedidof,false,'')+' '+objJson.datos[0].total_revisar_pedido_porcentages );
			$("#total_fp").html( DevolverContenido_Total_img( parseFloat(objJson.datos[0].total_finalizar_pedido).toFixed(2),parseFloat(objJson.datos[1].total_finalizar_pedido).toFixed(2),objJson.datos[0].total_finalizar_pedidof,objJson.datos[1].total_finalizar_pedidof,false,'')+' '+objJson.datos[0].total_finalizar_pedido_porcentages );
			$("#total_pp").html( DevolverContenido_Total_img( parseFloat(objJson.datos[0].total_pago_realizado).toFixed(2),parseFloat(objJson.datos[1].total_pago_realizado).toFixed(2),objJson.datos[0].total_pago_realizadof,objJson.datos[1].total_pago_realizadof,false,'')+' '+objJson.datos[0].total_pago_realizado_porcentages );

		
			data_graficos  = google.visualization.arrayToDataTable(eval(objJson.datos[0].grafico));
			datavAxis      = '';	
			var nseries = parseInt(eval(objJson.datos[0].grafico_count));

			switch(nseries) {
			    case 5:
			        var options = {
				    	//title: dataTitulo[current],
				    	//vAxis: { title: datavAxis },
				    	hAxis: { _title: "Total",
				    	textStyle: {
					      opacity: 0
					    }
					 },
				    	backgroundColor: 'transparent',
						bar: { groupWidth: '40px' },	
				    	
							
						
				    	legend: { textStyle: {fontSize: 12 }},
				    	seriesType: "line",
				    	//"#3366cc","#dc3912","#ff9900","#109618","#990099","#0099c6","#dd4477","#66aa00","#b82e2e","#316395","#994499","#22aa99","#8fabe3","#b96c70","#dc8338","#6c8d26","#7b853d","#615d6a","#56a5ff","#db25ff","#1447ff","#ffda0a","#ff9b0a"],
				    	//seriesType: "bars",

				    	
				    	    series: { 5: { type: "steppedArea", color: '#cdcdcd', } },
				    	
				    	animation: {
				    	    duration: 1000,
				    	    easing: 'out'
				            },
				    };
			        break;
			    case 4:
			        var options = {
				    	//title: dataTitulo[current],
				    	//vAxis: { title: datavAxis },
				    	hAxis: { _title: "Total",
				    	textStyle: {
					      opacity: 0
					    }
					 },
				    	backgroundColor: 'transparent',
						bar: { groupWidth: '40px' },	
				    	
							
						
				    	legend: { textStyle: {fontSize: 12 }},
				    	seriesType: "line",
				    	//"#3366cc","#dc3912","#ff9900","#109618","#990099","#0099c6","#dd4477","#66aa00","#b82e2e","#316395","#994499","#22aa99","#8fabe3","#b96c70","#dc8338","#6c8d26","#7b853d","#615d6a","#56a5ff","#db25ff","#1447ff","#ffda0a","#ff9b0a"],
				    	//seriesType: "bars",

				    	
				    	    series: { 4: { type: "steppedArea", color: '#cdcdcd', } },
				    	
				    	animation: {
				    	    duration: 1000,
				    	    easing: 'out'
				            },
				    };
			        break;
			     case 3:
			        var options = {
				    	//title: dataTitulo[current],
				    	//vAxis: { title: datavAxis },
				    	hAxis: { _title: "Total",
				    	textStyle: {
					      opacity: 0
					    }
					 },
				    	backgroundColor: 'transparent',
						bar: { groupWidth: '40px' },	
				    	
							
						
				    	legend: { textStyle: {fontSize: 12 }},
				    	seriesType: "line",
				    	//"#3366cc","#dc3912","#ff9900","#109618","#990099","#0099c6","#dd4477","#66aa00","#b82e2e","#316395","#994499","#22aa99","#8fabe3","#b96c70","#dc8338","#6c8d26","#7b853d","#615d6a","#56a5ff","#db25ff","#1447ff","#ffda0a","#ff9b0a"],
				    	//seriesType: "bars",

				    	
				    	    series: { 3: { type: "steppedArea", color: '#cdcdcd', } },
				    	
				    	animation: {
				    	    duration: 1000,
				    	    easing: 'out'
				            },
				    };
			        break;
			    case 2:
			        var options = {
				    	//title: dataTitulo[current],
				    	//vAxis: { title: datavAxis },
				    	hAxis: { _title: "Total",
				    	textStyle: {
					      opacity: 0
					    }
					 },
				    	backgroundColor: 'transparent',
						bar: { groupWidth: '40px' },	
				    	
							
						
				    	legend: { textStyle: {fontSize: 12 }},
				    	seriesType: "line",
				    	//"#3366cc","#dc3912","#ff9900","#109618","#990099","#0099c6","#dd4477","#66aa00","#b82e2e","#316395","#994499","#22aa99","#8fabe3","#b96c70","#dc8338","#6c8d26","#7b853d","#615d6a","#56a5ff","#db25ff","#1447ff","#ffda0a","#ff9b0a"],
				    	//seriesType: "bars",

				    	
				    	    series: { 2: { type: "steppedArea", color: '#cdcdcd', } },
				    	
				    	animation: {
				    	    duration: 1000,
				    	    easing: 'out'
				            },
				    };
			        break;

			    case 1:
			        var options = {
				    	//title: dataTitulo[current],
				    	//vAxis: { title: datavAxis },
				    	hAxis: { _title: "Total",
				    	textStyle: {
					      opacity: 0
					    }
					 },
				    	backgroundColor: 'transparent',
						bar: { groupWidth: '40px' },	
				    	
							
						
				    	legend: { textStyle: {fontSize: 12 }},
				    	seriesType: "line",
				    	//"#3366cc","#dc3912","#ff9900","#109618","#990099","#0099c6","#dd4477","#66aa00","#b82e2e","#316395","#994499","#22aa99","#8fabe3","#b96c70","#dc8338","#6c8d26","#7b853d","#615d6a","#56a5ff","#db25ff","#1447ff","#ffda0a","#ff9b0a"],
				    	//seriesType: "bars",

				    	
				    	    series: { 1: { type: "steppedArea", color: '#cdcdcd', } },
				    	
				    	animation: {
				    	    duration: 1000,
				    	    easing: 'out'
				            },
				    };
			        break;
			} 

			

		    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));

		    chart.draw(data_graficos, options);

    	})

	}


	google.load('visualization', '1.1', { packages: ['corechart','table'] });
    google.setOnLoadCallback(inicializar);

	function inicializar(){

		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion(1);	
		//Opciones de comparación de datos -------------------------------------------


		//DATATABLE DE PEDIDOS -----------------------------------------------------------------------------------------
		table_datatable_embudos = $('#table_datatable_embudos').DataTable({
			"language": {
				"url": "/public/assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[3,'desc']],
			"searching": false,
			//Exportaciones 
			//"dom": 'Trt <filp "fondo">',
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
		        },
		     "columnDefs": [
	        {
	            "targets": [ 1 ],
	            "visible": false
	        }
	        ],
			"columns":[
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false }
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&perspectiva=Diario&perspectiva_seg=" + "&idpropiedad="+$("#dat_propiedad").val(), 
				"data": {"strFuncion": 'datatable_informes_embudos_pedidos'}
			}	
		});	

	cargar_datos_extras();

}


$(document).ready(function() {

	$(".radiopers_per").on("click",function(){



		var data  = $(this).attr("value");
		var pers  = $("#perspectiva").val();
		var persb = $("#perspectiva_seg").val();

		if($("#perspectiva_camb").val() == ""){

			if(data != pers){
				//Si es diferente la primera perspectiva a la pinchada comprobamos si la segunda tiene perspectiva
				if(persb == ""){
					$(".radiopers_per").removeClass("active_seg");
					$(this).addClass("active_seg");
					$("#perspectiva_seg").val(data);
					segundaperspectiva()
					$("#cab_perspectiva_seg").text($(this).text());
					cargador();
				}else{
					//Si persb == data eliminamos la segunda perspectiva
					if(persb == data){
						$(".radiopers_per").removeClass("active_seg");
						$("#perspectiva_seg").val("");	
						segundaperspectiva()
						$("#cab_perspectiva_seg").text("");
					}else{
						$(".radiopers_per").removeClass("active_seg");
						$(this).addClass("active_seg");
						$("#perspectiva_seg").val(data);	
						segundaperspectiva()
						$("#cab_perspectiva_seg").text($(this).text());
						cargador();
					}						
				}
							
			}else{
				//Si es igual significa que se pone en modo cambio de perspectiva 1
				$("#perspectiva_camb").val(data);
				$(".radiopers_per").removeClass("active_pri");
				$(this).addClass("active_select");
			}		
		}else{

			$(".radiopers_per").removeClass("active_select");
			$(this).addClass("active_pri");
			$("#perspectiva_camb").val("");
			$("#perspectiva").val(data);
			segundaperspectiva()
			$("#cab_perspectiva").text($(this).text());
			cargador();
		}

	});


});	



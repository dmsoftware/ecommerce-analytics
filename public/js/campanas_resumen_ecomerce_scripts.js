	var nombre_dimension = [];

	var cab_sesion = [];
	var cab_nombre_sesion = [];
	var cab_porcentaje = [];
	var cab_sesiones = [];

	var table_datatable_enlaces;
	var table_datatable_buscadores;
	var table_datatable_social;
	var table_datatable_campanas;
	var t_datatable_edades;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	var data_column;
	var data_column_ant;
	var data_column_original;
	var data_column_original_ant;

	var chart_column;
	var chart_column_ant;

	var chart_colum_origin_ant;
	var chart_colum_origin;
	var array_colums_off = [];

	var data_columnchart;
	var data_columnchart_ant;

	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();

		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}
	
	function DevolverContenido_Total_img(Valor,ValorAnt,ValorF,ValorAntF){

		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			strAux = '<img src="../images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				strAux = '<img src="../images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			} else {
				strAux = '<img src="../images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			}
		}

		return strAux;

	}
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}


	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX

	function trim(cadena){
	       cadena=cadena.replace(/^\s+/,'').replace(/\s+$/,'');
	       return(cadena);
	} 


 	function cargador(){
		Leer_Opcion_Comparacion();
		cargar_cabeceras();
	}

	function cargar_cabeceras_ini(){
		Leer_Opcion_Comparacion();

		Inicializar_datatables();

		//cargar_datos_extra();

	}

	function cargar_cabeceras(){
		Leer_Opcion_Comparacion();
		Recargar_Datatables();
		//cargar_datos_extra();
	}


    $(document).ready(function(){
    	cargar_cabeceras_ini();
    })

	function Recargar_Datatables() {
		//table_datatable_campanas.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
	}

	function Inicializar_datatables(){


		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&idPropiedad=" + $("#dat_propiedad").val(),
			data: { strFuncion: 'campanas_resumen_datatable_ecomerce'}
		})
		.done(function (respuesta) {

			//var objJson = jQuery.parseJSON(respuesta);
			$("#table_datatable_campanas_ecomerce tbody").html(respuesta);

			$(".agrupacion").parent("tr").hide();
			$(".subtotal").parent("tr").hide();
			//$(".subtotal_tit").html($(".subtotal_tit").attr('id'));


			$(".filadesplegable").on("click",function(){
				var grupo = $(this).attr("name");
				$("."+grupo).parent("tr").stop();
				//alert(grupo);
				if($("."+grupo).parent("tr").css("display") == "none"){
					$("."+grupo).parent("tr").fadeIn(300);
					//alert($(this).children(".tittot").children(".subtotal").html())
					$(this).children(".tittot").children(".subtotal_tit").html("Totales:")
				}else{
					$("."+grupo).parent("tr").hide();
					$(this).children(".tittot").children(".subtotal_tit").html($(this).attr("id"))
				}
				
			})
		})//done
	
		//DATATABLE DE CAMPAÑAS -----------------------------------------------------------------------------------------
		/*table_datatable_campanas = $('#table_datatable_campanas').dataTable({
			"language": {
				"url": "/public/assets/js/datatables/spanish.txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[0,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&idPropiedad" + $("#dat_propiedad").val(), 
				"data": {"strFuncion": 'campanas_resumen_datatable'}
			}		
		});	*/	
		//DATATABLE CAMPAÑAS ----------------------------------------------------------------------------------------- 		


	}


function verCampana(campana,medium,source){

	$.ajax({
		  type: 'POST',
		  url: "campanas_resumen_ajax_pagecampana.php",
		  data: {
		    camp: campana, 
		    medi: medium, 
		    sour: source       
		    },
		  dataType: 'text',
		  success: function(data){	
		  	//alert(data);
			 	self.location = "../informes/metricas_habituales.php"
		    },
		  error: function(data){
		  	   
		  }
	})//fin ajax

}

function verFiltro(filtro){

	$.ajax({
	  type: 'POST',
	  url: "campanas_resumen_ajax_pagecampana.php",
	  data: {
	    filt: filtro      
	    },
	  dataType: 'text',
	  success: function(data){	
	  	//alert(data);
		 	self.location = "../informes/metricas_habituales.php"
	    },
	  error: function(data){
	  	   
	  }
	})//fin ajax	
	
}

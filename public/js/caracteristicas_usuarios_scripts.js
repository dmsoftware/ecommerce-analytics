	var sexo_rickshaw_duracionmediasesion = [];
	var sexo_rickshaw_duracionmediasesion_legend=[];
	var sexo_rickshaw_sesiones=[];
	var sexo_rickshaw_sesiones_legend=[];
	var sexo_rickshaw_paginasvistassesion=[];
	var sexo_rickshaw_paginasvistassesion_legend=[];

	var sexo_rickshaw_tasarebote=[];
	var sexo_rickshaw_tasarebote_legend=[];
	var sexo_rickshaw_dimensiones=[];
	var sexo_rickshaw_dimensiones_legend=[];

	var edad_rickshaw_duracionmediasesion = [];
	var edad_rickshaw_duracionmediasesion_legend=[];
	var edad_rickshaw_sesiones=[];
	var edad_rickshaw_sesiones_legend=[];
	var edad_rickshaw_paginasvistassesion=[];
	var edad_rickshaw_paginasvistassesion_legend=[];

	var edad_rickshaw_tasarebote=[];
	var edad_rickshaw_tasarebote_legend=[];
	var edad_rickshaw_dimensiones=[];
	var edad_rickshaw_dimensiones_legend=[];

	var edad_nombre_dimension = [];
	var sexo_nombre_dimension = [];


	var rickshaw_sesiones=[];
	var rickshaw_sesiones_legend=[];
	var nombre_dimension = [];

	var cab_sesion = [];
	var cab_nombre_sesion = [];
	var cab_porcentaje = [];
	var cab_sesiones = [];

	var t_datatable_sexo;
	var t_datatable_edades;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion();			
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	})//.trigger('change');		
	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();
		//alert($("#dat_comparador").val())
		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	function Recargar_Datatable() {
		//alert(blnComparacion)
		t_datatable_sexo.api().ajax.url('../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
		t_datatable_edades.api().ajax.url('../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
		t_datatable_intereses.api().ajax.url('../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
	}
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------

	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if(ValorAnt != ''){
			if (parseFloat(Valor) > parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '%</div>';
			} else {
				if (parseFloat(Valor) == parseFloat(ValorAnt)) {
					// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
					strAux = '<div class="gris comparador"> ' + ValorAntF + '%</div>';
				} else {
					// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
					strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '%</div>';
				}
			}
			if (blnSoloGrafico) {
				return strAux
			} else {
				return strAux + ' ' + ValorF;
			}
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}

	function Cargar_datos_sexo(){
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'caracteristicas_graficos_sexo'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);	
			

			if(blnComparacion==true){
				$("#total_sexo_ico").html(DevolverContenido_Total(objJson.datos[0].data_mayor,objJson.datos[1].data_mayor,objJson.datos[0].data_mayor_formateado,objJson.datos[1].data_mayor_formateado,true));
			}else{
				$("#total_sexo_ico").html("");
			}

			$("#expl_sexo").text(objJson.datos[0].data_total);	


			$("#txt_sexo_nombre").text(trans.__(objJson.datos[0].nombre_mayor));
			//4. DESTACADOS. Tile Stats
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('#tilestats_total_sexo'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval(objJson.datos[0].data_mayor),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix);
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

		});		
			
	
	}//cargar datos sexo

	function Cargar_datos_edad(){
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'caracteristicas_graficos_edades'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);	
			

			if(blnComparacion==true){
				$("#total_edad_ico").html(DevolverContenido_Total(objJson.datos[0].data_mayor,objJson.datos[1].data_mayor,objJson.datos[0].data_mayor_formateado,objJson.datos[1].data_mayor_formateado,true));
			}else{
				$("#total_edad_ico").html("");
			}

			$("#expl_edad").text(objJson.datos[0].data_total);	

			$("#txt_edad_nombre").text(objJson.datos[0].nombre_mayor);
			//4. DESTACADOS. Tile Stats
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('#tilestats_total_edad'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval(objJson.datos[0].data_mayor),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix+'%');
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

		});		
			
	
	}
	function Cargar_datos_interes(){
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'caracteristicas_graficos_interes'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);	
			

			if(blnComparacion==true){
				$("#total_interes_ico").html(DevolverContenido_Total(objJson.datos[0].data_mayor,objJson.datos[1].data_mayor,objJson.datos[0].data_mayor_formateado,objJson.datos[1].data_mayor_formateado,true));
			}else{
				$("#total_interes_ico").html("");
			}

			$("#expl_interes").text(objJson.datos[0].data_total);	

			$("#txt_interes_nombre").text(objJson.datos[0].nombre_mayor);
			//4. DESTACADOS. Tile Stats
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('#tilestats_total_interes'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval(objJson.datos[0].data_mayor),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix + '%');
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

		});		
			
	
	}//cargar datos sexo

/*function Cargar_datos_sexo(){
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val(),
			data: { strFuncion: 'caracteristicas_graficos_sexo'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);			
			
	//Comprobamos que sse compara o no para poner el doble a la array o no
	if (blnComparacion==true) {
		//si se compara se trae el doble de arrays por lo que recorremos la mitad
		numfilas = objJson.datos.length/2;
	}else{
		numfilas = objJson.datos.length;
	}

	for (var i = 0 ; i < numfilas ; i++) {

		sexo_nombre_dimension[i] = objJson.datos[i].nombre_dimension;
		eval('sexo_rickshaw_dimensiones={' + objJson.datos[i].rickshaw_dimensiones +'}');
		sexo_rickshaw_sesiones[i] = '['+objJson.datos[i].rickshaw_sesiones+']';
		sexo_rickshaw_sesiones_legend[i] = '['+objJson.datos[i].rickshaw_sesiones_legend+']';

	}	

	if (blnComparacion==true) {

		for (var i = numfilas ; i < numfilas*2 ; i++) {

			sexo_nombre_dimension[i] = objJson.datos[i].nombre_dimension;
			eval('sexo_rickshaw_dimensiones={' + objJson.datos[i].rickshaw_dimensiones +'}');
			sexo_rickshaw_sesiones[i] = '['+objJson.datos[i].rickshaw_sesiones+']';
			sexo_rickshaw_sesiones_legend[i] = '['+objJson.datos[i].rickshaw_sesiones_legend+']';


		}

	}
	
setTimeout(function(){ Cargar_Graficos_Evolucion("sexo") }, 1200); 
			

		tarta_data = "[";
		var n = 0;
		var total_mayor = 0;
		var nombre_mayor = "";
		var total = 0;
		for (var i = 0; i < numfilas; i++) {
		
			if(total_mayor < objJson.datos[i].total_sesiones){
				total_mayor = objJson.datos[i].total_sesiones;
				nombre_mayor = objJson.datos[i].nombre_fila;
			}

			total += parseInt(objJson.datos[i].total_sesiones);
			total_total_sesiones = objJson.datos[i].total_total_sesiones
			//tarta_data = objJson.datos[i].total_sesiones;
			if(n!=0){ tarta_data += ","; }else{ n = 1;}
			tarta_data += "{"+"value: "+objJson.datos[i].total_sesiones+", color: '#6d7578', highlight: '#a2a2a2', label:'"+objJson.datos[i].nombre_fila+"'"+"}";
		}
		tarta_data += "]";
		
		porcentaje = (total_mayor*100)/total;
		porcentaje_total = (total*100/total_total_sesiones);

		totalb = 0;
		if (blnComparacion==true) {

			for (var i = numfilas ; i < numfilas*2 ; i++) {

				totalb += parseInt(objJson.datos[i].total_sesiones);


			}

		}
		if(blnComparacion==true){
			$("#total_sexo_ico").html(DevolverContenido_Total(total,totalb,total,totalb,true));
		}else{
			$("#total_sexo_ico").html("");
		}

		var pieData = eval(tarta_data)

		$("#expl_sexo").text(formato_numero(porcentaje_total, 2, ',', '.'))	

		
			var ctx = document.getElementById("chart-area_sexo").getContext("2d");
			chart_sexo = new Chart(ctx).Pie(pieData);
		

			$("#txt_sexo_nombre").text(nombre_mayor);
			//4. DESTACADOS. Tile Stats
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('#tilestats_total_sexo'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval(porcentaje),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix);
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

		});
	}//cargar datos sexo*/





function formato_numero(numero, decimales, separador_decimal, separador_miles){     
	numero=parseFloat(numero);     
	if(isNaN(numero)){         
		return "";}     
	if(decimales!==undefined){        
	 	// Redondeamos         
		numero=numero.toFixed(decimales);     
	}     
	// Convertimos el punto en separador_decimal     
	numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
	if(separador_miles){         
		// Añadimos los separadores de miles         
		var miles=new RegExp("(-?[0-9]+)([0-9]{3})");         
		while(miles.test(numero)) {             
			numero=numero.replace(miles, "$1" + separador_miles + "$2");         
		}     
	}     
	return numero; 
}



			
	function Cargar_Datos_Extra(strtipo) {
		switch(strtipo){
			case "sexo":
				Cargar_datos_sexo();
				break;
			case "edad":
				Cargar_datos_edad();
				break;
			case "interes":
				Cargar_datos_interes();
				break;
		
		}
	}//funcion

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		Leer_Opcion_Comparacion();	
		Recargar_Datatable();
		Cargar_Datos_Extra("sexo");
		Cargar_Datos_Extra("edad");
		Cargar_Datos_Extra("interes");
		cargarGraficos();
	}

	function Sesiones_Totales(){

		var ruta = "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strApp=" + $("#dat_app").val();
		//alert(ruta);
		var resp = 0;
		$.ajax({
		  type: 'POST',
		  url: ruta,
		  data: {
		    strFuncion: 'sesiones_totales'         
		    },
		  dataType: 'text',
		  success: function(data){
		  	//alert(parseInt(data.replace(" ","")));
		    resp = parseInt(data.replace(" ",""));
		    $("#dat_sestot").val(resp);
		    },
		  error: function(data){
		    //alert("Error: "+data);
		    resp = data;		  }
		})//fin ajax

		
		return resp;

	}


	$(document).ready(function() {

		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion(1);	
		//Opciones de comparación de datos -------------------------------------------

		if( $("#dat_app").val() == 0 ){	
			appdata = true;
		}else{
			appdata = false;
		}

		//DATATABLE DE SEXO -----------------------------------------------------------------------------------------
		t_datatable_sexo = $('#table_datatable_sexo').dataTable({
			"language": {
				"url": "/public/assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[0,'asc']],
			"searching": false,
			//Exportaciones 
			//"dom": 'Trt <filp "fondo">',
			"dom": 'rt',
			"tableTools": {
	            "aButtons": [
	                {
                    "sExtends": "xls",
                    "sPdfOrientation": "landscape",
                    //"sPdfMessage": "Your custom message would go here.",
                    "sButtonText": "Excel"
                	},
                	{
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    //"sPdfMessage": "Your custom message would go here.",
                    "sButtonText": "PDF"
                	},
                	{
                    "sExtends": "csv",
                    "sPdfOrientation": "landscape",
                    //"sPdfMessage": "Your custom message would go here.",
                    "sButtonText": "CSV"
                	}

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		      {
		          "targets": [ 2 ],
		          "visible": appdata
		      }
		      ],
			"columns":[
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false }
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_sexo'}
			}		
		});		

		//DATATABLE de SEXO ----------------------------------------------------------------------------------------- 		
		
		//DATATABLE DE EDADES -----------------------------------------------------------------------------------------
		t_datatable_edades = $('#table_datatable_edades').dataTable({
			"language": {
				"url": "/public/assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [6, 10, 25, 50, -1], [6, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[0,'asc']],
			"searching": false,
			//Exportaciones 
			//"dom": 'Trt <filp "fondo">',
			"dom": 'rt',
			"tableTools": {
	            "aButtons": [
	                {
                    "sExtends": "xls",
                    "sPdfOrientation": "landscape",
                    //"sPdfMessage": "Your custom message would go here.",
                    "sButtonText": "Excel"
                	},
                	{
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    //"sPdfMessage": "Your custom message would go here.",
                    "sButtonText": "PDF"
                	},
                	{
                    "sExtends": "csv",
                    "sPdfOrientation": "landscape",
                    //"sPdfMessage": "Your custom message would go here.",
                    "sButtonText": "CSV"
                	}

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		      {
		          "targets": [ 2 ],
		          "visible": appdata
		      }
		      ],
			"columns":[
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false }
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_edades'}
			}		
		});		

		//DATATABLE EDADES ----------------------------------------------------------------------------------------- 		

		//DATATABLE DE Intereses -----------------------------------------------------------------------------------------
		t_datatable_intereses = $('#table_datatable_intereses').dataTable({
			"language": {
				"url": "/public/assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		      {
		          "targets": [ 2 ],
		          "visible": appdata
		      }
		      ],
			"columns":[
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_intereses'}
			}		
		});		

		//DATATABLE EDADES ----------------------------------------------------------------------------------------- 		


		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		Cargar_Datos_Extra("sexo");
		Cargar_Datos_Extra("edad");
		Cargar_Datos_Extra("interes");
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		
		cargarGraficos();
		
});	

function limpiarCamvas(idcanvas){
	var oCanvas = document.getElementById(idcanvas);
	var oContext = oCanvas.getContext("2d");
	oContext.clearRect(0, 0, oCanvas.width, oCanvas.height);
}

function cargarGraficos(){
	graficoSexo();
	graficoEdad();
	graficoInteres();
}

function graficoSexo(){

	$.ajax({
		type: 'POST',
		url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
		data: { strFuncion: 'caracteristicas_graficos_bipolar_sexo'}
	})
	.done(function (respuesta) {

		limpiarCamvas("cvs_sexo");

		var objJson = jQuery.parseJSON(respuesta);	

		if(blnComparacion == 1){

			if(objJson.datos[1].ndata == 0){
				var ant = "[";
				for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
					if(i!=0){ ant +=",";}
					ant += "0";
				};
				ant += "]";
			}else{
				var ant = objJson.datos[1].col_dat;
			}
			
		}else{
			var ant = "[";
			for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
				if(i!=0){ ant +=",";}
				ant += "0";
			};
			ant += "]";
		}

		var noms;
		if(objJson.datos[1].ndata > objJson.datos[0].ndata){
			noms = objJson.datos[1].col_nom;
		}else{
			noms = objJson.datos[0].col_nom;
		}

		//alert(ant);
		var bipolar = new RGraph.Bipolar({
			id: 'cvs_sexo',
			left:  eval(ant),
			right: eval(objJson.datos[0].col_dat),
			options: {
			    shadow: {
			        color: '#bbb'
			    },
			    gutter: {
			        left: 15,
			        right: 15,
			        center: 0
			    },
			    text:{
			    	color: 'black',
			    	size: 8
			    },
			    axis:{
			    	color: 'rgba(179,179,179,0.5)'
			    },
			    labels: eval(noms),
				colors: [
			        'Gradient(#fa8011:#fa8011:#37bcf8:#37bcf8)'
			    ],	            
			    strokestyle: 'white',
			    linewidth: 1
			}
	    }).draw()

	});
}

function graficoEdad(){

	$.ajax({
		type: 'POST',
		url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
		data: { strFuncion: 'caracteristicas_graficos_bipolar_edad'}
	})
	.done(function (respuesta) {

		limpiarCamvas("cvs_edad");

		var objJson = jQuery.parseJSON(respuesta);	

		if(blnComparacion == 1){

			if(objJson.datos[1].ndata == 0){
				var ant = "[";
				for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
					if(i!=0){ ant +=",";}
					ant += "0";
				};
				ant += "]";
			}else{
				var ant = objJson.datos[1].col_dat;
			}
			
		}else{
			var ant = "[";
			for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
				if(i!=0){ ant +=",";}
				ant += "0";
			};
			ant += "]";
		}

		var noms;
		if(objJson.datos[1].ndata > objJson.datos[0].ndata){
			noms = objJson.datos[1].col_nom;
		}else{
			noms = objJson.datos[0].col_nom;
		}
		
		//alert(noms);
		//alert(ant);
		var bipolar = new RGraph.Bipolar({
			id: 'cvs_edad',
			left:  eval(ant),
			right: eval(objJson.datos[0].col_dat),
			options: {
			    shadow: {
			        color: '#bbb'
			    },
			    gutter: {
			        left: 15,
			        right: 15,
			        center: 0
			    },
			    text:{
			    	color: 'black',
			    	size: 8
			    },
			    axis:{
			    	color: 'rgba(179,179,179,0.5)'
			    },
			    labels: eval(noms),
				colors: [
			        'Gradient(#fa8011:#fa8011:#37bcf8:#37bcf8)'
			    ],	            
			    strokestyle: 'white',
			    linewidth: 1
			}
	    }).draw()

	});
}

function graficoInteres(){

	$.ajax({
		type: 'POST',
		url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
		data: { strFuncion: 'caracteristicas_graficos_bipolar_interes'}
	})
	.done(function (respuesta) {

		limpiarCamvas("cvs_interes");

		var objJson = jQuery.parseJSON(respuesta);	

		if(blnComparacion == 1){

			if(objJson.datos[1].ndata == 0){
				var ant = "[";
				for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
					if(i!=0){ ant +=",";}
					ant += "0";
				};
				ant += "]";
			}else{
				var ant = objJson.datos[1].col_dat;
			}
			
		}else{
			var ant = "[";
			for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
				if(i!=0){ ant +=",";}
				ant += "0";
			};
			ant += "]";
		}

		var noms;
		if(objJson.datos[1].ndata > objJson.datos[0].ndata){
			noms = objJson.datos[1].col_nom;
		}else{
			noms = objJson.datos[0].col_nom;
		}

		//alert(ant);
		var bipolar = new RGraph.Bipolar({
			id: 'cvs_interes',
			left:  eval(ant),
			right: eval(objJson.datos[0].col_dat),
			options: {
			    shadow: {
			        color: '#bbb'
			    },
			    gutter: {
			        left: 15,
			        right: 15,
			        center: 0
			    },
			    text:{
			    	color: 'black',
			    	size: 8
			    },
			    axis:{
			    	color: 'rgba(179,179,179,0.5)'
			    },
			    labels: eval(noms),
			    //strokestyle : '#aaa',
			    //colors.
				colors: {
					  
			        sequential : true
			    	
			    }, 	 
			    colors:[
			    		//'#00f', '#f00', '#0f0', '#ff0', '#0ff', '#f0f', '#ff6101', '#b401ff', '#e4ff01', '#fb8195', '#ccc'
			        	'Gradient(#fa8011:#fa8011:#37bcf8:#37bcf8)',   
			    ],      
			    strokestyle: 'white',
			    linewidth: 1
			}
	    }).draw()

	});
}
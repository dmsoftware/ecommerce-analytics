//VARIABLES GLOBALES
var blnComparacion;
var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
var chart;
var t_datatable_contenido_arbol;
var t_datatable_contenido_eventos_descargas;
var t_datatable_contenido_eventos_busquedas;
var t_datatable_contenido_eventos_enlaces;
	


/*Funciones externas*/	
function treatAsUTC(date) {
	var result = new Date(date);
	result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
	return result;
}

function daysBetween(startDate, endDate) {
	var millisecondsPerDay = 24 * 60 * 60 * 1000;
	return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
}

function DateToString (datDate) {
	return datDate.toISOString().slice(0,10);
}
/*Funciones externas*/	

function Leer_Opcion_Comparacion() {
	//comparacion = 0
	datFechaInicioFiltro = new Date($("#dat_fechaini").val());
	datFechaFinFiltro = new Date($("#dat_fechafin").val());

	valComparacion = $("#dat_comparador").val();
	switch (valComparacion) {
		case "0":
			blnComparacion = false;
			break;
		case "1": //Mismo periodo del año pasado
			blnComparacion = true;
			datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
			datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
			datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
			datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
			strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
			strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
			break;
		case "2": //Periodo anterior
			blnComparacion = true;
			//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
			datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
			datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
			intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
			datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
			datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
			strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
			strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
			break;
	}
}

//Funcion para pintar el arbol web
function drawChart(){

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Name');
    data.addColumn('string', 'Manager');
    data.addColumn('string', 'ToolTip');
	
    //Llamamos al ajax para traer el json del char
    $.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'contenido_chart'}
		})
		.done(function (respuesta) {
			//alert(respuesta);
			//var prueba =  "[[{v:'web', f:'WEB<div style=\"color:black; width:100px;\"><p>700 pag.</p>100%</p></div>'}, '', 'tooltip'],[{v:'Jim', f:'Jim<div style=\"color:red; font-style:italic\">Vice President<div>'}, 'web', 'tooltip'],['Alice', 'web', ''],['Bob', 'Jim', 'Bob Sponge'],['Carol', 'Bob', '']]";
    		data.addRows(eval(respuesta));

			chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
			chart.draw(data, {allowHtml:true});

			var altcaj = $(".google-visualization-orgchart-table").width();
			$("#scroller").css("width",altcaj+"px")

			loaded()
		})

}

function Recargar_Datatables() {
	recargar_tables_sustitucion();
	t_datatable_contenido_arbol.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + '&blnComparacion=' + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strApp=" + $("#dat_app").val()).load();
	t_datatable_contenido_eventos_descargas.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
	t_datatable_contenido_eventos_busquedas.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
	t_datatable_contenido_eventos_enlaces.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
}
//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
function cargador(){
	Leer_Opcion_Comparacion();
	cargar_cabeceras();
	cargar_totales('','')
}

function cargador_desdecab(){
	Leer_Opcion_Comparacion();
	drawChart();
	Recargar_Datatables();
	//cargar_datos_extra();
	cargar_totales('','')
	
	if($("#dat_app").val() == 1){
		$("#nombre_dimension_dt").text("APP");
	}else{
		$("#nombre_dimension_dt").text("WEB");
	}

}

function cargador_vista(){
	cargar_cabeceras();	

	if($("#dat_app").val() == 1){
		$("#nombre_dimension_dt").text("APP");
	}else{
		$("#nombre_dimension_dt").text("WEB");
	}
}

function cargar_cabeceras_ini(){
	$.ajax({
		  type: 'POST',
		  url: "/public/informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
		  data: {
		    strFuncion: 'contenido_cabeceras_idiomas'       
		    },
		  dataType: 'text',
		  success: function(data){	
			 	$("#tab_cabeceras").html(data).promise().done(function(){
					drawChart();
					Inicializar_datatables();
					cargar_datos_extra();
					cargar_totales('','')
			    });
		    },
		  error: function(data){
		  	   $("#tab_cabeceras").html(data)
		  }
	})//fin ajax
}

function cargar_cabeceras(){
	$.ajax({
		  type: 'POST',
		  url: "/public/informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
		  data: {
		    strFuncion: 'contenido_cabeceras_idiomas'       
		    },
		  dataType: 'text',
		  success: function(data){	
			 	$("#tab_cabeceras").html(data).promise().done(function(){
					drawChart();
					Recargar_Datatables();
					//cargar_datos_extra();
			    });
		    },
		  error: function(data){
		  	   $("#tab_cabeceras").html(data)
		  }
	})//fin ajax
}

//Cargar el char
google.load("visualization", "1", {packages:["orgchart"]});
google.setOnLoadCallback(cargar_cabeceras_ini);


function Inicializar_datatables(){
	
	inicializar_tables_sustitucion();

	if( $("#dat_app").val() == 0 ){	
		appdata = true;
	}else{
		appdata = false;
	}
	
	//Datatable del arbol de contenido -----------------------------------------------------------------------------------------
	t_datatable_contenido_arbol = $('#table_datatable_contenido_arbol').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[0,'asc']],
		"searching": false,
		//Exportaciones 
		"dom": 'rtT <filp "fondo">',
		"tableTools": {
	        "aButtons": [
	            {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	        ],
	        "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
	     "columnDefs": [
		 {
		     "targets": [ 1,4 ],
		     "visible": appdata
		 }
		 ],
		"columns":[
			{"orderable": false },
			{"orderable": false },
			{"orderable": false },
			{"orderable": false },
			{"orderable": false }
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + '&blnComparacion=' + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strApp=" + $("#dat_app").val(), 
			"data": {"strFuncion": 'contenido_datatable_chart'}
		}		
	});		
	//Datatable del arbol de contenido -----------------------------------------------------------------------------------------

	//Datatable de eventos descargas -----------------------------------------------------------------------------------------
	t_datatable_contenido_eventos_descargas = $('#table_datatable_eventos_descargas').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[1,'desc']],
		"searching": false,
		//Exportaciones 
		"dom": 'rtT <filp "fondo">',
		"tableTools": {
	        "aButtons": [
	            {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	        ],
	        "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			null,
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
			"data": {"strFuncion": 'contenido_datatable_eventos_descargas'}
		}		
	});		
	//Datatable de eventos descargas -----------------------------------------------------------------------------------------

	//Datatable de eventos busquedas -----------------------------------------------------------------------------------------
	t_datatable_contenido_eventos_busquedas = $('#table_datatable_eventos_busquedas').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[1,'desc']],
		"searching": false,
		//Exportaciones 
		"dom": 'rtT <filp "fondo">',
		"tableTools": {
	        "aButtons": [
	            {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	        ],
	        "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			null,
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
			"data": {"strFuncion": 'contenido_datatable_eventos_busquedas'}
		}		
	});		
	//Datatable de eventos busquedas -----------------------------------------------------------------------------------------

	//Datatable de eventos enlaces -----------------------------------------------------------------------------------------
	t_datatable_contenido_eventos_enlaces = $('#table_datatable_eventos_enlaces').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[1,'desc']],
		"searching": false,
		//Exportaciones 
		"dom": 'rtT <filp "fondo">',
		"tableTools": {
	        "aButtons": [
	            {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	        ],
	        "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			null,
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
			"data": {"strFuncion": 'contenido_datatable_eventos_enlaces'}
		}		
	});		
	//Datatable de eventos enlaces -----------------------------------------------------------------------------------------

	

}

function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}

function cargar_datos_extra(){
	$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + /*$("#txt_idioma").val()*/"todos" + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'contenido_datos_extra'}
		})
		.done(function (respuesta) {
			//alert(respuesta);
			var objJson = jQuery.parseJSON(respuesta);
			
			nombre_idioma_total = trans.__(objJson.datos[0].idioma_mayor);
			pags_idioma_total = objJson.datos[0].pags_idioma;
			pags_total = objJson.datos[0].total_pagvistas;

			porcentaje = (pags_idioma_total*100)/pags_total;

			$("#txt_nombre_totales").text(nombre_idioma_total);
			$("#txt_pagvistas").text(formato_numero(pags_total, 0, ',', '.'));
			$("#txt_des").text(formato_numero(objJson.datos[0].tot_descarga, 0, ',', '.'));
			$("#txt_bus").text(formato_numero(objJson.datos[0].tot_busqueda, 0, ',', '.'));
			$("#txt_enl").text(formato_numero(objJson.datos[0].tot_externo, 0, ',', '.'));
			//4. idioma. Tile Stats
			$("#total_contenido_idioma").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('.num'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					//end = eval('objJson.datos[0].'+ $(el).find('.num').attr('id')),
					end = eval(porcentaje),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', ''),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix + "%");
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

		
		//Comparador
		if (blnComparacion) {
			pags_idioma_total_ant = objJson.datos[1].pags_idioma;
			pags_total_ant = objJson.datos[1].total_pagvistas;
			porcentaje_ant = (pags_idioma_total_ant*100)/pags_total_ant;
			porcentaje_f = formato_numero(porcentaje, 2, ',', '.')+"%";
			porcentaje_ant_f = formato_numero(porcentaje_ant, 2, ',', '.')+"%";

			$("#total_sesiones_ico").html(DevolverContenido_Total(porcentaje,porcentaje_ant,porcentaje_f,porcentaje_ant_f,2))
			$("#comparar_pagtotal").html(DevolverContenido_Total(objJson.datos[0].total_pagvistas, objJson.datos[1].total_pagvistas, formato_numero(objJson.datos[0].total_pagvistas, 0, ',', '.'), formato_numero(objJson.datos[1].total_pagvistas, 0, ',', '.'), 1 ))
			$("#comparar_des").html(DevolverContenido_Total(objJson.datos[0].tot_descarga, objJson.datos[1].tot_descarga, formato_numero(objJson.datos[0].tot_descarga, 0, ',', '.'), formato_numero(objJson.datos[1].tot_descarga, 0, ',', '.'), 1 ))
			$("#comparar_bus").html(DevolverContenido_Total(objJson.datos[0].tot_busqueda, objJson.datos[1].tot_busqueda, formato_numero(objJson.datos[0].tot_busqueda, 0, ',', '.'), formato_numero(objJson.datos[1].tot_busqueda, 0, ',', '.'), 1 ))
			$("#comparar_enl").html(DevolverContenido_Total(objJson.datos[0].tot_externo, objJson.datos[1].tot_externo, formato_numero(objJson.datos[0].tot_externo, 0, ',', '.'), formato_numero(objJson.datos[1].tot_externo, 0, ',', '.'), 1 ))
		
		}else{
			$("#total_sesiones_ico").html("");
			$("#comparar_pagtotal").html("");
			$("#comparar_des").html("");
			$("#comparar_bus").html("");
			$("#comparar_enl").html("");
		}

		/*var pieData = eval(objJson.datos[0].tarta_ajax);

		var ctx = document.getElementById("chart-contenido_idioma").getContext("2d");
		chart_contenido_idioma = new Chart(ctx).Pie(pieData);*/



		})
}

function DevolverContenido_Total_totales(Valor,ValorAnt,ValorF,ValorAntF){

	if (Valor > ValorAnt) {
		strAux = '<img class="imgdrip" src="../public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
	} else {
		if (Valor == ValorAnt) {
			strAux = '<img class="imgdrip" src="../public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		} else {
			strAux = '<img class="imgdrip" src="../public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		}
	}
	return strAux;

}
function cargar_datatable_arbol(dimension, filtro){
	//alert("ga:dimension"+dimension+" == "+filtro);
	//filtro = "ga:dimension"+dimension+" == "+filtro;
	if(dimension == 0){
			if($("#dat_app").val() == 1){
				$("#nombre_dimension_dt").text("APP");
			}else{
				$("#nombre_dimension_dt").text("WEB");
			}
			
			t_datatable_contenido_arbol.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + '&blnComparacion=' + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strApp=" + $("#dat_app").val()).load();
			cargar_totales(dimension, filtro)
		}else{
			sv = 0;
			bfiltro = "";
			for (var i = 0; i < filtro.length; i++) {
				if(sv == 1){
					bfiltro += filtro[i];
				}
				if(filtro[i]=="|"){
					sv=1
				}
				
			};
			$("#nombre_dimension_dt").text(bfiltro);
			t_datatable_contenido_arbol.api().ajax.url("../informes/informes_ajax.php?filtrochart=" + filtro +"&filtrodimension=" + dimension + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + '&blnComparacion=' + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strApp=" + $("#dat_app").val()).load();
			cargar_totales(dimension, filtro)
		}

}

function cargar_totales(dimension,filtro){

	$("#total_visitas").html("");
	$("#total_durmedia").html("");
	$("#total_tasrebote").html("");

	if(dimension==0){

		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'contenido_datatable_chart_totales'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);
			//Comparador
			if (blnComparacion) {
				$("#total_visitas").html(DevolverContenido_Total_totales(objJson.datos[0].total_visitas,objJson.datos[1].total_visitas,objJson.datos[0].total_visitas_f,objJson.datos[1].total_visitas_f,false))
				$("#total_durmedia").html(DevolverContenido_Total_totales(objJson.datos[0].total_durmedia,objJson.datos[1].total_durmedia,objJson.datos[0].total_durmedia_f,objJson.datos[1].total_durmedia_f,false))
				$("#total_tasrebote").html(DevolverContenido_Total_totales(objJson.datos[1].total_tasrebote,objJson.datos[0].total_tasrebote,objJson.datos[1].total_tasrebote_f,objJson.datos[0].total_tasrebote_f,false))	
			}else{
				$("#total_visitas").html("");
				$("#total_durmedia").html("");
				$("#total_tasrebote").html("");
			}

			$("#total_visitas").html($("#total_visitas").html()+objJson.datos[0].total_visitas_f);
			$("#total_durmedia").html($("#total_durmedia").html()+objJson.datos[0].total_durmedia_f);
			$("#total_tasrebote").html($("#total_tasrebote").html()+objJson.datos[0].total_tasrebote_f);

			})

	}else{
		
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?filtrochart=' + filtro +"&filtrodimension=" + dimension +'&blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#txt_idioma").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'contenido_datatable_chart_totales'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);
			if (blnComparacion) {
				$("#total_visitas").html(DevolverContenido_Total_totales(objJson.datos[0].total_visitas,objJson.datos[1].total_visitas,objJson.datos[0].total_visitas_f,objJson.datos[1].total_visitas_f,false))
				$("#total_durmedia").html(DevolverContenido_Total_totales(objJson.datos[0].total_durmedia,objJson.datos[1].total_durmedia,objJson.datos[0].total_durmedia_f,objJson.datos[1].total_durmedia_f,false))
				$("#total_tasrebote").html(DevolverContenido_Total_totales(objJson.datos[1].total_tasrebote,objJson.datos[0].total_tasrebote,objJson.datos[1].total_tasrebote_f,objJson.datos[0].total_tasrebote_f,false))	
			}else{
				$("#total_visitas").html("");
				$("#total_durmedia").html("");
				$("#total_tasrebote").html("");
			}

			$("#total_visitas").html($("#total_visitas").html()+objJson.datos[0].total_visitas_f);
			$("#total_durmedia").html($("#total_durmedia").html()+objJson.datos[0].total_durmedia_f);
			$("#total_tasrebote").html($("#total_tasrebote").html()+objJson.datos[0].total_tasrebote_f);			

		})
	}
}

function formato_numero(numero, decimales, separador_decimal, separador_miles){     
	numero=parseFloat(numero);     
	if(isNaN(numero)){         
		return "";}     
	if(decimales!==undefined){        
	 	// Redondeamos         
		numero=numero.toFixed(decimales);     
	}     
	// Convertimos el punto en separador_decimal     
	numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
	if(separador_miles){         
		// Añadimos los separadores de miles         
		var miles=new RegExp("(-?[0-9]+)([0-9]{3})");         
		while(miles.test(numero)) {             
			numero=numero.replace(miles, "$1" + separador_miles + "$2");         
		}     
	}     
	return numero; 
}



$(document).ready(function() {

	//A la hora de cargar
	//cargar_cabeceras();
	


});	

	var nombre_dimension = [];

	var cab_sesion = [];
	var cab_nombre_sesion = [];
	var cab_porcentaje = [];
	var cab_sesiones = [];

	var table_datatable_enlaces;
	var table_datatable_buscadores;
	var table_datatable_social;
	var table_datatable_campanas;
	var t_datatable_edades;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	var data_column;
	var data_column_ant;
	var data_column_original;
	var data_column_original_ant;

	var chart_column;
	var chart_column_ant;

	var chart_colum_origin_ant;
	var chart_colum_origin;
	var array_colums_off = [];

	var data_columnchart;
	var data_columnchart_ant;

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion();			
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	})//.trigger('change');		
	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();

		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}
	
	function DevolverContenido_Total_img(Valor,ValorAnt,ValorF,ValorAntF){

		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			strAux = '<img src="../images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				strAux = '<img src="../images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			} else {
				strAux = '<img src="../images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			}
		}

		return strAux;

	}
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}

			

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX

	function trim(cadena){
	       cadena=cadena.replace(/^\s+/,'').replace(/\s+$/,'');
	       return(cadena);
	} 

	function drawChart(straux) {

		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val(),
			data: { strFuncion: 'contenido_chart_'+straux}
		})
		.done(function (respuesta) {

			var data = google.visualization.arrayToDataTable(eval(respuesta));

	        if (straux=="idiomauser") {
	        	var options = {
	          		title: 'Idiomas del usuario',
	          		is3D: true
	          		//pieHole: 0.4


	        	};
	        }else{
				var options = {
		          	title: 'Idiomas del navegador',
		          	is3D: true
		        };
	        }
	        

	        var chart = new google.visualization.PieChart(document.getElementById(straux+'_div'));

	        chart.draw(data, options);

    	})//done
    }

	function cargar_datos_extra() {

		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + '&blnComparacion=' + blnComparacion + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'fuentestrafico_datos_extra'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);

			if(blnComparacion){

				$("#sesiones_enlaces").html("<div class='guia_enlaces'></div>"+DevolverContenido_Total_img(objJson.datos[0].enlaces_sesiones_total_comp,objJson.datos[1].enlaces_sesiones_total_comp,objJson.datos[0].enlaces_sesiones_total_img,objJson.datos[1].enlaces_sesiones_total_img) + ' ' + objJson.datos[0].enlaces_sesiones_total);
				$("#sesiones_buscadores").html("<div class='guia_buscadores'></div>"+DevolverContenido_Total_img(objJson.datos[0].buscadores_sesiones_total_comp,objJson.datos[1].buscadores_sesiones_total_comp,objJson.datos[0].buscadores_sesiones_total_img,objJson.datos[1].buscadores_sesiones_total_img) + ' ' + objJson.datos[0].buscadores_sesiones_total);
				$("#sesiones_social").html("<div class='guia_social'></div>"+DevolverContenido_Total_img(objJson.datos[0].social_sesiones_total_comp,objJson.datos[1].social_sesiones_total_comp,objJson.datos[0].social_sesiones_total_img,objJson.datos[1].social_sesiones_total_img) + ' ' + objJson.datos[0].social_sesiones_total);
				$("#sesiones_campanas").html("<div class='guia_campanas'></div>"+DevolverContenido_Total_img(objJson.datos[0].campanas_sesiones_total_comp,objJson.datos[1].campanas_sesiones_total_comp,objJson.datos[0].campanas_sesiones_total_img,objJson.datos[1].campanas_sesiones_total_img) + ' ' + objJson.datos[0].campanas_sesiones_total);
				$("#sesiones_directo").html("<div class='guia_directo'></div>"+DevolverContenido_Total_img(objJson.datos[0].directo_sesiones_total_comp,objJson.datos[1].directo_sesiones_total_comp,objJson.datos[0].directo_sesiones_total_img,objJson.datos[1].directo_sesiones_total_img) + ' ' + objJson.datos[0].directo_sesiones_total);
				
				$("#tasarebote_enlaces").html("<div class='guia_enlaces'></div>"+DevolverContenido_Total_img(objJson.datos[1].enlaces_tasarebote_total_comp,objJson.datos[0].enlaces_tasarebote_total_comp,objJson.datos[0].enlaces_tasarebote_total,objJson.datos[1].enlaces_tasarebote_total) + ' ' + objJson.datos[0].enlaces_tasarebote_total);
				$("#tasarebote_buscadores").html("<div class='guia_buscadores'></div>"+DevolverContenido_Total_img(objJson.datos[1].buscadores_tasarebote_total_comp,objJson.datos[0].buscadores_tasarebote_total_comp,objJson.datos[0].buscadores_tasarebote_total,objJson.datos[1].buscadores_tasarebote_total) + ' ' + objJson.datos[0].buscadores_tasarebote_total);
				$("#tasarebote_social").html("<div class='guia_social'></div>"+DevolverContenido_Total_img(objJson.datos[1].social_tasarebote_total_comp,objJson.datos[0].social_tasarebote_total_comp,objJson.datos[0].social_tasarebote_total,objJson.datos[1].social_tasarebote_total) + ' ' + objJson.datos[0].social_tasarebote_total);
				$("#tasarebote_campanas").html("<div class='guia_campanas'></div>"+DevolverContenido_Total_img(objJson.datos[1].campanas_tasarebote_total_comp,objJson.datos[0].campanas_tasarebote_total_comp,objJson.datos[0].campanas_tasarebote_total,objJson.datos[1].campanas_tasarebote_total) + ' ' + objJson.datos[0].campanas_tasarebote_total);
				$("#tasarebote_directo").html("<div class='guia_directo'></div>"+DevolverContenido_Total_img(objJson.datos[1].directo_tasarebote_total_comp,objJson.datos[0].directo_tasarebote_total_comp,objJson.datos[0].directo_tasarebote_total,objJson.datos[1].directo_tasarebote_total) + ' ' + objJson.datos[0].directo_tasarebote_total);

				$("#tiemposesion_enlaces").html("<div class='guia_enlaces'></div>"+DevolverContenido_Total_img(objJson.datos[0].enlaces_tsesion_total_comp,objJson.datos[1].enlaces_tsesion_total_comp,objJson.datos[0].enlaces_tsesion_total,objJson.datos[1].enlaces_tsesion_total) + ' ' + objJson.datos[0].enlaces_tsesion_total);
				$("#tiemposesion_buscadores").html("<div class='guia_buscadores'></div>"+DevolverContenido_Total_img(objJson.datos[1].buscadores_tsesion_total_comp,objJson.datos[0].buscadores_tsesion_total_comp,objJson.datos[0].buscadores_tsesion_total,objJson.datos[1].buscadores_tsesion_total) + ' ' + objJson.datos[0].buscadores_tsesion_total);
				$("#tiemposesion_social").html("<div class='guia_social'></div>"+DevolverContenido_Total_img(objJson.datos[1].social_tsesion_total_comp,objJson.datos[0].social_tsesion_total_comp,objJson.datos[0].social_tsesion_total,objJson.datos[1].social_tsesion_total) + ' ' + objJson.datos[0].social_tsesion_total);
				$("#tiemposesion_campanas").html("<div class='guia_campanas'></div>"+DevolverContenido_Total_img(objJson.datos[0].campanas_tsesion_total_comp,objJson.datos[1].campanas_tsesion_total_comp,objJson.datos[0].campanas_tsesion_total,objJson.datos[1].campanas_tsesion_total) + ' ' + objJson.datos[0].campanas_tsesion_total);
				$("#tiemposesion_directo").html("<div class='guia_directo'></div>"+DevolverContenido_Total_img(objJson.datos[0].directo_tsesion_total_comp,objJson.datos[1].directo_tsesion_total_comp,objJson.datos[0].directo_tsesion_total,objJson.datos[1].directo_tsesion_total) + ' ' + objJson.datos[0].directo_tsesion_total);

				$("#pagsesion_enlaces").html("<div class='guia_enlaces'></div>"+DevolverContenido_Total_img(objJson.datos[0].enlaces_pagsesion_total_comp,objJson.datos[1].enlaces_pagsesion_total_comp,objJson.datos[0].enlaces_pagsesion_total,objJson.datos[1].enlaces_pagsesion_total) + ' ' + objJson.datos[0].enlaces_pagsesion_total);
				$("#pagsesion_buscadores").html("<div class='guia_buscadores'></div>"+DevolverContenido_Total_img(objJson.datos[0].buscadores_pagsesion_total_comp,objJson.datos[1].buscadores_pagsesion_total_comp,objJson.datos[0].buscadores_pagsesion_total,objJson.datos[1].buscadores_pagsesion_total)+ ' ' + objJson.datos[0].buscadores_pagsesion_total);
				$("#pagsesion_social").html("<div class='guia_social'></div>"+DevolverContenido_Total_img(objJson.datos[0].social_pagsesion_total_comp,objJson.datos[1].social_pagsesion_total_comp,objJson.datos[0].social_pagsesion_total,objJson.datos[1].social_pagsesion_total) + ' ' + objJson.datos[0].social_pagsesion_total);
				$("#pagsesion_campanas").html("<div class='guia_campanas'></div>"+DevolverContenido_Total_img(objJson.datos[0].campanas_pagsesion_total_comp,objJson.datos[1].campanas_pagsesion_total_comp,objJson.datos[0].campanas_pagsesion_total,objJson.datos[1].campanas_pagsesion_total) + ' ' + objJson.datos[0].campanas_pagsesion_total);
				$("#pagsesion_directo").html("<div class='guia_directo'></div>"+DevolverContenido_Total_img(objJson.datos[0].directo_pagsesion_total_comp,objJson.datos[1].directo_pagsesion_total_comp,objJson.datos[0].directo_pagsesion_total,objJson.datos[1].directo_pagsesion_total) + ' ' + objJson.datos[0].directo_pagsesion_total);

				$("#conversiones_enlaces").html("<div class='guia_enlaces'></div>"+DevolverContenido_Total_img(objJson.datos[0].conv_enlaces_total,objJson.datos[1].conv_enlaces_total,objJson.datos[0].conv_enlaces_total,objJson.datos[1].conv_enlaces_total) + ' ' + objJson.datos[0].conv_enlaces_total);
				$("#conversiones_buscadores").html("<div class='guia_buscadores'></div>"+DevolverContenido_Total_img(objJson.datos[0].conv_buscadores_total,objJson.datos[1].conv_buscadores_total,objJson.datos[0].conv_buscadores_total,objJson.datos[1].conv_buscadores_total) + ' ' + objJson.datos[0].conv_buscadores_total);
				$("#conversiones_social").html("<div class='guia_social'></div>"+DevolverContenido_Total_img(objJson.datos[0].conv_social_total,objJson.datos[1].conv_social_total,objJson.datos[0].conv_social_total,objJson.datos[1].conv_social_total) + ' ' + objJson.datos[0].conv_social_total);
				$("#conversiones_campanas").html("<div class='guia_campanas'></div>"+DevolverContenido_Total_img(objJson.datos[0].conv_campanas_total,objJson.datos[1].conv_campanas_total,objJson.datos[0].conv_campanas_total,objJson.datos[1].conv_campanas_total) + ' ' + objJson.datos[0].conv_campanas_total);
				$("#conversiones_directo").html("<div class='guia_directo'></div>"+DevolverContenido_Total_img(objJson.datos[0].conv_directo_total,objJson.datos[1].conv_directo_total,objJson.datos[0].conv_directo_total,objJson.datos[1].conv_directo_total) + ' ' + objJson.datos[0].conv_directo_total);

			}else{

				$("#sesiones_enlaces").html("<div class='guia_enlaces'></div>"+objJson.datos[0].enlaces_sesiones_total);
				$("#sesiones_buscadores").html("<div class='guia_buscadores'></div>"+objJson.datos[0].buscadores_sesiones_total);
				$("#sesiones_social").html("<div class='guia_social'></div>"+objJson.datos[0].social_sesiones_total);
				$("#sesiones_campanas").html("<div class='guia_campanas'></div>"+objJson.datos[0].campanas_sesiones_total);
				$("#sesiones_directo").html("<div class='guia_directo'></div>"+objJson.datos[0].directo_sesiones_total);
				
				$("#tasarebote_enlaces").html("<div class='guia_enlaces'></div>"+objJson.datos[0].enlaces_tasarebote_total)
				$("#tasarebote_buscadores").html("<div class='guia_buscadores'></div>"+objJson.datos[0].buscadores_tasarebote_total)
				$("#tasarebote_social").html("<div class='guia_social'></div>"+objJson.datos[0].social_tasarebote_total)
				$("#tasarebote_campanas").html("<div class='guia_campanas'></div>"+objJson.datos[0].campanas_tasarebote_total)
				$("#tasarebote_directo").html("<div class='guia_directo'></div>"+objJson.datos[0].directo_tasarebote_total)

				$("#tiemposesion_enlaces").html("<div class='guia_enlaces'></div>"+objJson.datos[0].enlaces_tsesion_total);
				$("#tiemposesion_buscadores").html("<div class='guia_buscadores'></div>"+objJson.datos[0].buscadores_tsesion_total);
				$("#tiemposesion_social").html("<div class='guia_social'></div>"+objJson.datos[0].social_tasarebote_total);
				$("#tiemposesion_campanas").html("<div class='guia_campanas'></div>"+objJson.datos[0].campanas_tsesion_total);
				$("#tiemposesion_directo").html("<div class='guia_directo'></div>"+objJson.datos[0].directo_tsesion_total);

				$("#pagsesion_enlaces").html("<div class='guia_enlaces'></div>"+objJson.datos[0].enlaces_pagsesion_total);
				$("#pagsesion_buscadores").html("<div class='guia_buscadores'></div>"+objJson.datos[0].buscadores_pagsesion_total);
				$("#pagsesion_social").html("<div class='guia_social'></div>"+objJson.datos[0].social_pagsesion_total);
				$("#pagsesion_campanas").html("<div class='guia_campanas'></div>"+objJson.datos[0].campanas_pagsesion_total);
				$("#pagsesion_directo").html("<div class='guia_directo'></div>"+objJson.datos[0].directo_pagsesion_total);

				$("#conversiones_enlaces").html("<div class='guia_enlaces'></div>"+objJson.datos[0].conv_enlaces_total);
				$("#conversiones_buscadores").html("<div class='guia_buscadores'></div>"+objJson.datos[0].conv_buscadores_total);
				$("#conversiones_social").html("<div class='guia_social'></div>"+objJson.datos[0].conv_social_total);
				$("#conversiones_campanas").html("<div class='guia_campanas'></div>"+objJson.datos[0].conv_campanas_total);
				$("#conversiones_directo").html("<div class='guia_directo'></div>"+objJson.datos[0].conv_directo_total);

			}

			//Vaciamos los contenidos del html de conversiones para que no se dupliquen;
			$("#objetivos_capa_enlaces").html("");
			$("#objetivos_capa_buscadores").html("");
			$("#objetivos_capa_social").html("");
			$("#objetivos_capa_directo").html("");
			$("#objetivos_capa_campanas").html("");

			//Primero debemos averiguar las conversiones que trae
			var conversiones = [];
			var cv = 0;
			if(objJson.datos[0].objson_enlaces != ""){
				for (var i = 0; i <= objJson.datos[0].objson_enlaces.length - 1;  i++) {
					//Hay que comprobar que no exista ya en la array
					if(conversiones.length != 0){
						var igual = 0;
						for (var d = 0; d <= conversiones.length-1; d++) {
							if( conversiones[d] == objJson.datos[0].objson_enlaces[i][1] ){
								igual =1;
							}	
						};	
						
						if(igual == 0){
							conversiones[cv] = objJson.datos[0].objson_enlaces[i][1];
							cv++;
						}

					}else{
						conversiones[cv] = objJson.datos[0].objson_enlaces[i][1];
						cv++;
					}				
					i++
				}
			}

			//alert(conversiones.length)
			if(objJson.datos[0].objson_buscadores != ""){
				for (var i = 0; i <= objJson.datos[0].objson_buscadores.length - 1;  i++) {
					//Hay que comprobar que no exista ya en la array
					if(conversiones.length != 0){
						var igual = 0;
						for (var d = 0; d <= conversiones.length-1; d++) {
							if( conversiones[d] == objJson.datos[0].objson_buscadores[i][1] ){
								igual =1;
							}	
						};	

						if(igual == 0){
							conversiones[cv] = objJson.datos[0].objson_buscadores[i][1];
							cv++;
						}
							
					}else{
						conversiones[cv] = objJson.datos[0].objson_buscadores[i][1];
						cv++;
					}		
					i++
				}
			}
			
			if(objJson.datos[0].objson_social != ""){
				for (var i = 0; i <= objJson.datos[0].objson_social.length - 1;  i++) {
					//Hay que comprobar que no exista ya en la array
					if(conversiones.length != 0){
						igual = 0;
						for (var d = 0; d <= conversiones.length-1; d++) {
							if( conversiones[d] == objJson.datos[0].objson_social[i][1] ){
								igual =1;
							}	
						};	
						if(igual == 0){
							conversiones[cv] = objJson.datos[0].objson_social[i][1];
							cv++;
						}	
					}else{
						conversiones[cv] = objJson.datos[0].objson_social[i][1];
						cv++;
					}		
					i++
				}
			}
			if(objJson.datos[0].objson_campanas != ""){
				for (var i = 0; i <= objJson.datos[0].objson_campanas.length - 1;  i++) {
					//Hay que comprobar que no exista ya en la array
					igual = 0;
					if(conversiones.length != 0){
						for (var d = 0; d <= conversiones.length-1; d++) {	
							if( conversiones[d] == objJson.datos[0].objson_campanas[i][1] ){
								igual =1;
							}	
						};	

						if(igual == 0){

							conversiones[cv] = objJson.datos[0].objson_campanas[i][1];
							cv++;
						}	
					}else{
						conversiones[cv] = objJson.datos[0].objson_campanas[i][1];
						cv++;
					}		
					i++
				}
			}
			if(objJson.datos[0].objson_directo != ""){
				for (var i = 0; i <= objJson.datos[0].objson_directo.length - 1;  i++) {
					//Hay que comprobar que no exista ya en la array
					igual = 0;
					if(conversiones.length != 0){
						for (var d = 0; d <= conversiones.length-1; d++) {
							if( conversiones[d] == objJson.datos[0].objson_directo[i][1] ){
								igual =1;
							}	
						};	

						if(igual == 0){
							conversiones[cv] = objJson.datos[0].objson_directo[i][1];
							cv++;
						}		
					}else{
						conversiones[cv] = objJson.datos[0].objson_directo[i][1];
						cv++;
					}		
					i++
				}
			}/**/
			//alert(conversiones);


			//Pintamos las conversiones
			var strA = ""; 
			for (var i = 0; i < conversiones.length; i++) {
				strA += "<tr>";
				strA += "<td class='columconver'>"+conversiones[i]+"</td>";

				//Enlaces
				if(objJson.datos[0].objson_enlaces != ""){
					var igualb = 0;
					for (var j = 0; j <= objJson.datos[0].objson_enlaces.length - 1;  j++) {
						
						if(conversiones[i] == objJson.datos[0].objson_enlaces[j][1] ){
							igualb = objJson.datos[0].objson_enlaces[j+1][1];
						}
						j++;
					};
					strA += "<td><div class='guia_enlaces'></div>"+igualb+"</td>";
				}else{
					strA += "<td><div class='guia_enlaces'></div>0</td>";
				}

				//Búsquedas
				if(objJson.datos[0].objson_buscadores != ""){
					var igualb = 0;
					for (var j = 0; j <= objJson.datos[0].objson_buscadores.length - 1;  j++) {
	
						if(conversiones[i] == objJson.datos[0].objson_buscadores[j][1] ){
							igualb = objJson.datos[0].objson_buscadores[j+1][1];
						}
						j++;

					};
					strA += "<td><div class='guia_buscadores'></div>"+igualb+"</td>";
				}else{
					strA += "<td><div class='guia_buscadores'></div>0</td>";
				}

				//Social
				if(objJson.datos[0].objson_social != ""){
					var igualb = 0;
					for (var j = 0; j <= objJson.datos[0].objson_social.length - 1;  j++) {
	
						if(conversiones[i] == objJson.datos[0].objson_social[j][1] ){
							igualb = objJson.datos[0].objson_social[j+1][1];
						}
						j++;

					};
					strA += "<td><div class='guia_social'></div>"+igualb+"</td>";
				}else{
					strA += "<td><div class='guia_social'></div>0</td>";
				}

				//Campañas
				if(objJson.datos[0].objson_campanas != ""){
					var igualb = 0;
					for (var j = 0; j <= objJson.datos[0].objson_campanas.length - 1;  j++) {
	
						if(conversiones[i] == objJson.datos[0].objson_campanas[j][1] ){
							igualb = objJson.datos[0].objson_campanas[j+1][1];
						}
						j++;

					};
					strA += "<td><div class='guia_campanas'></div>"+igualb+"</td>";
				}else{
					strA += "<td><div class='guia_campanas'></div>0</td>";
				}

				//Directo
				if(objJson.datos[0].objson_directo != ""){
					var igualb = 0;
					for (var j = 0; j <= objJson.datos[0].objson_directo.length - 1;  j++) {
	
						if(conversiones[i] == objJson.datos[0].objson_directo[j][1] ){
							igualb = objJson.datos[0].objson_directo[j+1][1];
						}
						j++;

					};
					strA += "<td><div class='guia_directo'></div>"+igualb+"</td>";
				}else{
					strA += "<td><div class='guia_directo'></div>0</td>";
				}

				strA += "</tr>";
			};
			$("#tabla_conversiones tfoot").html(strA);

			//Pintamos gráficos
			//Actual
			var data = google.visualization.arrayToDataTable(eval(objJson.datos[0].grafico_tarta));
			var colores_prueba = "0: { color: '#01d5fb'},1: { color: '#fe6b5b'},2: { color: '#fbc63c'},3: { color: '#01d9b2'},4: { color: '#7d5fa9'}";
	        var options = {
	         // slices: {eval(colores_prueba)},
	          slices:{0: { color: '#01d5fb'},1: { color: '#fe6b5b'},2: { color: '#fbc63c'},3: { color: '#01d9b2'},4: { color: '#7d5fa9'}},
	          title: '',
	          backgroundColor: 'transparent',
	          legend: 'none'
	        };
	        var chart = new google.visualization.PieChart(document.getElementById('piechart_actual'));
	        chart.draw(data, options);

	        //Anterior
	        if(blnComparacion){
				var data = google.visualization.arrayToDataTable(eval(objJson.datos[1].grafico_tarta));
		        var options = {
		          slices:{0: { color: '#01d5fb'},1: { color: '#fe6b5b'},2: { color: '#fbc63c'},3: { color: '#01d9b2'},4: { color: '#7d5fa9'}},
		          title: '',
		          backgroundColor: 'transparent',
		          legend: 'none'
		        };
		        //075.0.02366.3 23-07-2007
		        var chart = new google.visualization.PieChart(document.getElementById('piechart_anterior'));
		        chart.draw(data, options);
	    	}



	    	//Y ahora el grafico de las columnas
	    	data_columnchart = objJson.datos[0].grafico_colum;
	    	data_column = google.visualization.arrayToDataTable(eval(data_columnchart));
	    	data_column_original = data_column;

		      var options = {
		      	backgroundColor: 'transparent',
		        width: 325,
		        height: 185,
		        legend: { position: 'none', maxLines: 3 },
		        bar: { groupWidth: '75%' },
		        isStacked: true
		      };
		      chart_column = new google.visualization.ColumnChart(document.getElementById("columchart_valores"));
		      chart_colum_origin = chart_column;
     		  chart_column.draw(data_column, options);

     		  //Anterior
	       	 if(blnComparacion){
	       	 	
	       	 	data_columnchart_ant = objJson.datos[1].grafico_colum;
	       	 	data_column_ant = google.visualization.arrayToDataTable(eval(data_columnchart_ant));
	       	 	data_column_original_ant = data_column_ant;
			    var options = {
			      backgroundColor: 'transparent',
			      width: 325,
			      height: 185,
			      legend: { position: 'none', maxLines: 3 },
			      bar: { groupWidth: '75%' },
			      isStacked: true
			    };
			    chart_column_ant = new google.visualization.ColumnChart(document.getElementById("columchart_valores_ant"));
	     		chart_colum_origin_ant = chart_column_ant;
	     		chart_column_ant.draw(data_column_ant, options);

	       	 }

			//alert(objJson)
    	})//done
    }

    function anadir_columna(val) {

    	for (var i = 0; i < array_colums_off.length; i++) {

    		if(array_colums_off[i] == parseInt(val)){
    			array_colums_off.splice(i);
    		}
			
		};
      
      pintar_columchart()

    }

    function eliminar_columna(val) {
      array_colums_off.push(parseInt(val));
      pintar_columchart()

    }

    function pintar_columchart(){
      data_column = google.visualization.arrayToDataTable(eval(data_columnchart));
      data_column_ant = google.visualization.arrayToDataTable(eval(data_columnchart_ant));
     // alert(array_colums_off)
      for (var i = 0; i < array_colums_off.length; i++) {

      	data_column.removeColumn(array_colums_off[i]);
        data_column_ant.removeColumn(array_colums_off[i]);

      };

      recargar_columnchart(0);
	  recargar_columnchart(1);
    }

    function recargar_columnchart(cual){
    	var options = {
    	  backgroundColor: 'transparent',
		  width: 325,
		  height: 130,
		  legend: { position: 'none', maxLines: 3 },
		  bar: { groupWidth: '75%' },
		  isStacked: true
		};
    	if(cual == 0){
			chart_colum = new google.visualization.ColumnChart(document.getElementById("columchart_valores"));
	     	chart_colum.draw(data_column, options);
     	}else{
			chart_colum_ant = new google.visualization.ColumnChart(document.getElementById("columchart_valores_ant"));
	     	chart_colum_ant.draw(data_column_ant, options);
     	}
    }

    $(document).ready(function(){
    	$(".checkcolumn").on("click",function(){

    		if($(this).is(':checked')) {  
	           //Está activo por lo que añadimos 
	           anadir_columna( $(this).val() )
	        }else{  
	           //Está inactivo por lo que eliminamos la columna	
	           eliminar_columna( $(this).val() );
	        } 

    	})
    });


	function cargador(){
		Leer_Opcion_Comparacion();
		cargar_cabeceras();
	}

	function cargar_cabeceras_ini(){
		Leer_Opcion_Comparacion();
		//drawChart("idiomauser")
		//drawChart("idiomanav")
		Inicializar_datatables();
		//comparar_idiomas();
		cargar_datos_extra();
		/*cargar_datos_extra('enlaces');
		cargar_datos_extra('social');
		cargar_datos_extra('buscadores');
		cargar_datos_extra('campanas');*/
	}

	function cargar_cabeceras(){
		Leer_Opcion_Comparacion();
		Recargar_Datatables();
		cargar_datos_extra();
	}

	google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(cargar_cabeceras_ini);

	function Recargar_Datatables() {
		table_datatable_enlaces.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
		table_datatable_buscadores.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
		table_datatable_social.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
		table_datatable_campanas.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
	}

	function Inicializar_datatables(){
		//alert("entra")
		//DATATABLE DE ENLACES -----------------------------------------------------------------------------------------
		table_datatable_enlaces = $('#table_datatable_enlaces').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_enlaces'}
			}		
		});		

		//DATATABLE ENLACES ----------------------------------------------------------------------------------------- 		
		//DATATABLE DE BUSCADORES -----------------------------------------------------------------------------------------
		table_datatable_buscadores = $('#table_datatable_buscadores').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_buscadores'}
			}		
		});		
		//DATATABLE BUSCADORES ----------------------------------------------------------------------------------------- 		
		//DATATABLE DE SOCIAL -----------------------------------------------------------------------------------------
		table_datatable_social = $('#table_datatable_social').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_social'}
			}		
		});		
		//DATATABLE SOCIAL ----------------------------------------------------------------------------------------- 		
		//DATATABLE DE CAMPAÑAS -----------------------------------------------------------------------------------------
		table_datatable_campanas = $('#table_datatable_campanas').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[3,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_campanas'}
			}		
		});		
		//DATATABLE CAMPAÑAS ----------------------------------------------------------------------------------------- 		


	}


	$(document).ready(function() {

		
	});	

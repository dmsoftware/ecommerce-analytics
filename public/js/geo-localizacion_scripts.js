	var sexo_rickshaw_duracionmediasesion = [];
	var sexo_rickshaw_duracionmediasesion_legend=[];
	var sexo_rickshaw_sesiones=[];
	var sexo_rickshaw_sesiones_legend=[];
	var sexo_rickshaw_paginasvistassesion=[];
	var sexo_rickshaw_paginasvistassesion_legend=[];

	var sexo_rickshaw_tasarebote=[];
	var sexo_rickshaw_tasarebote_legend=[];
	var sexo_rickshaw_dimensiones=[];
	var sexo_rickshaw_dimensiones_legend=[];

	var edad_rickshaw_duracionmediasesion = [];
	var edad_rickshaw_duracionmediasesion_legend=[];
	var edad_rickshaw_sesiones=[];
	var edad_rickshaw_sesiones_legend=[];
	var edad_rickshaw_paginasvistassesion=[];
	var edad_rickshaw_paginasvistassesion_legend=[];

	var edad_rickshaw_tasarebote=[];
	var edad_rickshaw_tasarebote_legend=[];
	var edad_rickshaw_dimensiones=[];
	var edad_rickshaw_dimensiones_legend=[];

	var edad_nombre_dimension = [];
	var sexo_nombre_dimension = [];


	var dispositivos_rickshaw_duracionmediasesion = [];
	var dispositivos_rickshaw_duracionmediasesion_legend=[];
	var dispositivos_rickshaw_sesiones=[];
	var dispositivos_rickshaw_sesiones_legend=[];
	var dispositivos_rickshaw_paginasvistassesion=[];
	var dispositivos_rickshaw_paginasvistassesion_legend=[];

	var dispositivos_rickshaw_tasarebote=[];
	var dispositivos_rickshaw_tasarebote_legend=[];
	var dispositivos_rickshaw_dimensiones=[];
	var dispositivos_rickshaw_dimensiones_legend=[];

	var dispositivos_nombre_dimension = [];

	var rickshaw_sesiones=[];
	var rickshaw_sesiones_legend=[];
	var nombre_dimension = [];

	var cab_sesion = [];
	var cab_nombre_sesion = [];
	var cab_porcentaje = [];
	var cab_sesiones = [];

	var table_datatable_dispositivos;
	var t_datatable_edades;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion();			
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	})//.trigger('change');		
	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();

		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}





function formato_numero(numero, decimales, separador_decimal, separador_miles){     
	numero=parseFloat(numero);     
	if(isNaN(numero)){         
		return "";}     
	if(decimales!==undefined){        
	 	// Redondeamos         
		numero=numero.toFixed(decimales);     
	}     
	// Convertimos el punto en separador_decimal     
	numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
	if(separador_miles){         
		// Añadimos los separadores de miles         
		var miles=new RegExp("(-?[0-9]+)([0-9]{3})");         
		while(miles.test(numero)) {             
			numero=numero.replace(miles, "$1" + separador_miles + "$2");         
		}     
	}     
	return numero; 
}



	
			
	function Cargar_Datos_Extra(strtipo) {
		switch(strtipo){
			case "dispositivos":
				Cargar_datos_dispositivos();
				break;
			case "navegadores":
				Cargar_datos_navegadores();
				break;
		
		}
	}//funcion

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		Leer_Opcion_Comparacion();	
		Recargar_Datatable();
		Cargar_Datos_Extra("navegadores");
		Cargar_Datos_Extra("dispositivos");
		chart_dispositivos.destroy();
		cargar_mokup();
	}

	function Sesiones_Totales(){

		var ruta = "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val();
		//alert(ruta);
		var resp = 0;
		$.ajax({
		  type: 'POST',
		  url: ruta,
		  data: {
		    strFuncion: 'sesiones_totales'         
		    },
		  dataType: 'text',
		  success: function(data){
		  	//alert(parseInt(data.replace(" ","")));
		    resp = parseInt(data.replace(" ",""));
		    $("#dat_sestot").val(resp);
		    },
		  error: function(data){
		    //alert("Error: "+data);
		    resp = data;		  }
		})//fin ajax

		
		return resp;

	}

function trim(cadena){
       cadena=cadena.replace(/^\s+/,'').replace(/\s+$/,'');
       return(cadena);
} 

	function drawRegionsMap(straux) {

		//Llamamos al ajax para traer el json del char
		//alert($("#pais_comparar").val());
   		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'contenido_chart_'+straux}
		})
		.done(function (respuesta) {

			var data = google.visualization.arrayToDataTable(eval(respuesta));

			if(straux != "pais"){

				if ($("#pais_comparar").val() != "todos") {
					$.ajax({
						type: 'POST',
						url: '../informes/informes_ajax.php?strPais=' + $("#pais_comparar").val(),
						data: { strFuncion: 'contener_iso'}
					})
					.done(function (iso) {
						riso = trim(iso);
						var options = {
					       region: riso,
					       displayMode: 'markers',
					       colorAxis: {colors: ['green', 'blue']}
					    };

					    var chart = new google.visualization.GeoChart(document.getElementById(straux+'_div'));
						chart.draw(eval(data), options);
					});
				}else{

					//Comprobar el pais mas visto
					$.ajax({
						type: 'POST',
						url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
						data: { strFuncion: 'contenido_chart_paisPrin'}
					})
					.done(function (topPais) {
						//resp top pais

						//alert(topPais);
						$.ajax({
							type: 'POST',
							url: '../informes/informes_ajax.php?strPais='+trim(topPais),
							data: { strFuncion: 'contener_iso'}
						})
						.done(function (iso) {
							riso = trim(iso);
							var options = {
						       region: riso,
						       displayMode: 'markers',
						       colorAxis: {colors: ['green', 'blue']}
						    };

						    var chart = new google.visualization.GeoChart(document.getElementById(straux+'_div'));
							chart.draw(eval(data), options);
						});

					});
					//***************************
				}

			}else{

				var options = {};
				var chart = new google.visualization.GeoChart(document.getElementById(straux+'_div'));
				chart.draw(data, options);

			}
			
					
		})

	}
	function cargador(){
		Leer_Opcion_Comparacion();
		cargar_cabeceras();
	}

	function cargar_cabeceras_ini(){
		Leer_Opcion_Comparacion();
		drawRegionsMap("pais");
		drawRegionsMap("region");
		drawRegionsMap("ciudad");
		Inicializar_datatables();
		//cargar_datos_extra();
	}

	function cargar_cabeceras(){
		Leer_Opcion_Comparacion();
		drawRegionsMap("pais");
		drawRegionsMap("region");
		drawRegionsMap("ciudad");
		Recargar_Datatables();
		//cargar_datos_extra();
	}

	google.load("visualization", "1", {packages:["geochart"]});
	google.setOnLoadCallback(cargar_cabeceras_ini);


	function Recargar_Datatables() {
	table_datatable_pais.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
	table_datatable_region.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
	table_datatable_ciudad.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
	}

	function Inicializar_datatables(){

		if( $("#dat_app").val() == 0 ){	
			appdata = true;
		}else{
			appdata = false;
		}
		
		//DATATABLE DE PAIS -----------------------------------------------------------------------------------------
		table_datatable_pais = $('#table_datatable_pais').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			//"dom": 'Trt <filp "fondo">',
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		    {
		        "targets": [ 2 ],
		        "visible": appdata
		    }
		    ],
			"columns":[
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_pais'}
			}		
		});		
		//DATATABLE de PAIS ----------------------------------------------------------------------------------------- 		
		

		//DATATABLE DE REGION -----------------------------------------------------------------------------------------
		table_datatable_region = $('#table_datatable_region').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		    {
		        "targets": [ 2 ],
		        "visible": appdata
		    }
		    ],
			"columns":[
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_region'}
			}		
		});		

		//DATATABLE REGION ----------------------------------------------------------------------------------------- 		
		//DATATABLE DE CIUDAD -----------------------------------------------------------------------------------------
		table_datatable_ciudad = $('#table_datatable_ciudad').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		    {
		        "targets": [ 2 ],
		        "visible": appdata
		    }
		    ],
			"columns":[
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_ciudad'}
			}		
		});		
		//DATATABLE CIUDAD ----------------------------------------------------------------------------------------- 		


	}


	$(document).ready(function() {

		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion(1);	
		//Opciones de comparación de datos -------------------------------------------

		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		//Cargar_Datos_Extra("dispositivos");
		//Cargar_Datos_Extra("navegadores");
		//Cargar_Datos_Extra("interes");
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------



	

});	

$(document).ready(function(){


	$('.dropdown-toggle').dropdown();


	//Lo que ocurre al seleccionar una vista
	$("#combo_vistas").change(function(){
		//alert($(this+":selected").text());
		var dato = $(this).val()
		var vista = "";
		var proasoc ="";
		var sw=0;
		for (var i = 0; i < dato.length; i++) {
			if(sw==0){
				if (dato[i]!="*"){
					vista+=dato[i];
				}else{
					sw=1;
				}
			}else{
				proasoc+=dato[i];
			}
			
		};
		//alert("vista: "+vista+"  -  "+"proasoc: "+proasoc);
		
		//alert( $("#dat_filtro").val() );
		

		$("#dat_proyectoasociado").val(proasoc);
		$("#dat_idvista").val(vista);

		$.ajax({
			  type: 'POST',
			  url: '../head_ajax_nombre-propiedad.php',
			  data: {
			    idvista: vista         
			    },
			  dataType: 'text',
			  success: function(data){
				document.title = 'DmIntegra | '+ data;

				cargar_breadcrumb(vista);

				
				var rutaactual = location.href;
				//Comprobar si existe filtro de usuario
				
				if (/impactos1.php/.test(rutaactual)){  
					location.reload();
				}else if (/seo.php/.test(rutaactual)){  
					location.reload();
				}else if(/contenido_visto.php/.test(rutaactual)){
					//En esta página comprobamos la propiedad anterior con la nueva si ha cambiado
					//recargamos la página para que pueda comprobar si es enlazado o desenlazado
					$.ajax({
					  type: 'POST',
					  url: '../head_ajax_comprobar-propiedad.php',
					  data: {
					    idvista: vista
					    },
					  dataType: 'text',
					  success: function(data){
					  	//alert(data+" = "+$("#dat_propiedad").val());
					    if(data != $("#dat_propiedad").val()){
					    	location.reload();
					    }else{
					    	cargador_vista();
					    }

					    },
					  error: function(){
					    
					  }
					})//fin ajax
					
				}else if(/sitioweb.php/.test(rutaactual)){
					location.reload();
				}else{
					cargador();
				} 

				//
				$("#combo_idiomas").attr("data-placeholder","Todos los idiomas")
				$("#combo_paises").attr("data-placeholder","Todos los paises")
				cargarIdiomas();
				cargarPaises();
			    cargarFiltros();
			    },
			  error: function(){
			    $("#txt_propiedad").text("No se pudo mostrar la propiedad");
			  }
		})//fin ajax
		

	});

	/**
	 * Escuchar llamada desde un iframe en otro dominio
	 */
	ifrmamCrossDomainPath = 'http://dmintegra.crm-symfony.com';

	// Here "addEventListener" is for standards-compliant web browsers and "attachEvent" is for IE Browsers.
  var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
  var eventer = window[eventMethod];
  var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

  // Listen to message from child IFrame window
  eventer(messageEvent, function (e) {
    if (e.origin == ifrmamCrossDomainPath) {

      if(e.data === 'iframeclose'){
        $('#myModaliFrame').modal('hide');
      }
      if(e.data === 'iframeloginerror'){
        alert('LOGIN ERROR');
        $('#myModaliFrame').modal('hide');
      }
    }
  }, false);
  /**
	 * Fin esuchar llamada
	 */

});//ready

//Funciones
function cargar_breadcrumb(vista){

	//alert(vista);
	//PROPIEDAD
	$.ajax({
	  type: 'POST',
	  url: '../breadcrumb_ajax.php',
	  data: {
	    idvista: vista,
	    dev:    "propiedad"
	    },
	  dataType: 'text',
	  success: function(data){
	  	
	  	$("#txt_propiedad").html(data);

	    },
	  error: function(data){

	    cargar_breadcrumb(vista)

	  }
	})//fin ajax

	//VISTA
	$.ajax({
	  type: 'POST',
	  url: '../breadcrumb_ajax.php',
	  data: {
	    idvista: vista,
	    dev:    "vista"
	    },
	  dataType: 'text',
	  success: function(data){
	  	
	  	$("#txt_vista_br").html(data);

	    },
	  error: function(data){

	    cargar_breadcrumb(vista)

	  }
	})//fin ajax

	//URL
	$.ajax({
	  type: 'POST',
	  url: '../breadcrumb_ajax.php',
	  data: {
	    idvista: vista,
	    dev:    "url"
	    },
	  dataType: 'text',
	  success: function(data){
	  	
	  	$("#txt_url_br").html(data);

	    },
	  error: function(data){

	    cargar_breadcrumb(vista)

	  }
	})//fin ajax
	
}

function breadcrumb_rangofechas_comp(fechaIni,fechaFin,fechaIni_ant,fechaFin_ant){

	var fechatrans
	var fechatrans_ant
	var strS = "";

	$.ajax({
	  type: 'POST',
	  url: '../breadcrumb_ajax.php',
	  data: {
	    fechaini: fechaIni,
	    fechafin: fechaFin
	    },
	  dataType: 'text',
	  success: function(data){
	  	fechatrans = data;

		$.ajax({
		  type: 'POST',
		  url: '../breadcrumb_ajax.php',
		  data: {
		    fechaini: fechaIni_ant,
		    fechafin: fechaFin_ant
		    },
		  dataType: 'text',
		  success: function(data){
		  	fechatrans_ant = data;
		  	//alert(fechatrans);

		  	strS ="<span class='fechaactu' id='fecha_prim'> " + trans.__('Del') + fechatrans + ",</span><span class='fechaactu_com'> " + trans.__('comparado con el') + "</span><span class='fechaactu_comdata' id='fecha_sec'>"+fechatrans_ant+"</span>";
			$("#txt_fecha").html(strS);

		    },
		  error: function(data){

		    mostrar_fecha()

		  }
		})//fin ajax

	    },
	  error: function(data){

	    mostrar_fecha()

	  }
	})//fin ajax

}

function breadcrumb_rangofechas(fechaIni,fechaFin){

	var fechatrans
	var fechatrans_ant
	var strS = "";

	$.ajax({
	  type: 'POST',
	  url: '../breadcrumb_ajax.php',
	  data: {
	    fechaini: fechaIni,
	    fechafin: fechaFin
	    },
	  dataType: 'text',
	  success: function(data){
	  	
	  	fechatrans = data;

		strS ="<span class='fechaactu' id='fecha_prim'> Del "+fechatrans+"</span>";
		$("#txt_fecha").html(strS);
		
	    },
	  error: function(data){

	    mostrar_fecha()

	  }
	})//fin ajax

}

function mostrar_fecha() {

	//comparacion = 0
	datFechaInicioFiltro = new Date($("#dat_fechaini").val());
	datFechaFinFiltro = new Date($("#dat_fechafin").val());
	valComparacion = $("#dat_comparador").val();
	//alert(datFechaInicioFiltro);
	switch (valComparacion) {
		case "0":
			blnComparacion = false;
			//Si no se compara Añadimos solo el rango de fecha actual
			//$("#txt_fecha").load("")
			breadcrumb_rangofechas(DateToString(datFechaInicioFiltro),DateToString(datFechaFinFiltro));
			//$("#txt_fecha").html('Del <span id="fecha_del">'+ datFechaInicioFiltro +'</span> al <span id="fecha_hasta">'+datFechaFinFiltro+'</span>.');

			break;
		case "1": //Mismo periodo del año pasado
			blnComparacion = true;
			datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
			datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
			datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
			datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
			strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
			strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);

			breadcrumb_rangofechas_comp(DateToString(datFechaInicioFiltro),DateToString(datFechaFinFiltro),strFechaInicioFiltroAnt,strFechaFinFiltroAnt);

			break;
		case "2": //Periodo anterior
			blnComparacion = true;
			//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
			datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
			datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
			intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
			datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
			datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
			strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
			strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);	

			breadcrumb_rangofechas_comp(DateToString(datFechaInicioFiltro),DateToString(datFechaFinFiltro),strFechaInicioFiltroAnt,strFechaFinFiltroAnt);

			break;
	}
}

function lanzarModaliFrame(url){
	var w = $( window ).width();
	var h = $( window ).height(); 

  var modalContent = '<iframe width="' + w + '" height="' + h + '" src="' + url + '" frameborder="0"></iframe></div>';

  $("#myModaliFrame .iframContainer").css('padding-bottom', ((h * 100 / w)) + '%');
  $("#myModaliFrame .iframContainer").html(modalContent);

	$('#myModaliFrame').on('show.bs.modal', function (e) {
    window.scrollTo(0, 0);
	});

	$('#myModaliFrame').modal('show');
}

function mantenimientoCampana(id_propiedad){
	id_propiedad= id_propiedad ? id_propiedad : $('#dat_propiedad').val();
	var params = { 'lista' : 1, 'id_propiedad' : id_propiedad, 'lang': getCookie("idioma_usuario") };
	var url = 'http://dmintegra-admin.acc.com.es/externalAccess?route=campana&modal=0&params=' + encodeURIComponent(JSON.stringify(params));

	lanzarModaliFrame(url);
}

function mantenimientoMargenes(id_propiedad){
	id_propiedad= id_propiedad ? id_propiedad : $('#dat_propiedad').val();
	var params = { 'id' : id_propiedad, 'lang': getCookie("idioma_usuario") };
	var url = 'http://dmintegra-admin.acc.com.es/externalAccess?route=campana_margen&params=' + encodeURIComponent(JSON.stringify(params));

	lanzarModaliFrame(url);
}


function editarCampana(codigo, id_propiedad){
	var params = { 'id' : codigo, 'id_propiedad' : id_propiedad, 'lang': getCookie("idioma_usuario") };
	var url = 'http://dmintegra-admin.acc.com.es/externalAccess?route=campana_edit&params=' + encodeURIComponent(JSON.stringify(params));

	lanzarModaliFrame(url);
}

function mantenimientoLogEnvios(id_cliente){
	var params = { 'id_cliente' : id_cliente, 'lang': getCookie("idioma_usuario") };
	var url = 'http://dmintegra-admin.acc.com.es/externalAccess?route=envioemail2&modal=0&params=' + encodeURIComponent(JSON.stringify(params));

	lanzarModaliFrame(url);
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
} 
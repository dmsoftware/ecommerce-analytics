	var sexo_rickshaw_duracionmediasesion = [];
	var sexo_rickshaw_duracionmediasesion_legend=[];
	var sexo_rickshaw_sesiones=[];
	var sexo_rickshaw_sesiones_legend=[];
	var sexo_rickshaw_paginasvistassesion=[];
	var sexo_rickshaw_paginasvistassesion_legend=[];

	var sexo_rickshaw_tasarebote=[];
	var sexo_rickshaw_tasarebote_legend=[];
	var sexo_rickshaw_dimensiones=[];
	var sexo_rickshaw_dimensiones_legend=[];

	var edad_rickshaw_duracionmediasesion = [];
	var edad_rickshaw_duracionmediasesion_legend=[];
	var edad_rickshaw_sesiones=[];
	var edad_rickshaw_sesiones_legend=[];
	var edad_rickshaw_paginasvistassesion=[];
	var edad_rickshaw_paginasvistassesion_legend=[];

	var edad_rickshaw_tasarebote=[];
	var edad_rickshaw_tasarebote_legend=[];
	var edad_rickshaw_dimensiones=[];
	var edad_rickshaw_dimensiones_legend=[];

	var edad_nombre_dimension = [];
	var sexo_nombre_dimension = [];


	var dispositivos_rickshaw_duracionmediasesion = [];
	var dispositivos_rickshaw_duracionmediasesion_legend=[];
	var dispositivos_rickshaw_sesiones=[];
	var dispositivos_rickshaw_sesiones_legend=[];
	var dispositivos_rickshaw_paginasvistassesion=[];
	var dispositivos_rickshaw_paginasvistassesion_legend=[];

	var dispositivos_rickshaw_tasarebote=[];
	var dispositivos_rickshaw_tasarebote_legend=[];
	var dispositivos_rickshaw_dimensiones=[];
	var dispositivos_rickshaw_dimensiones_legend=[];

	var dispositivos_nombre_dimension = [];

	var rickshaw_sesiones=[];
	var rickshaw_sesiones_legend=[];
	var nombre_dimension = [];

	var cab_sesion = [];
	var cab_nombre_sesion = [];
	var cab_porcentaje = [];
	var cab_sesiones = [];

	var table_datatable_dispositivos;
	var t_datatable_edades;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion();			
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	})//.trigger('change');		
	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();

		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}

			

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX

	function trim(cadena){
	       cadena=cadena.replace(/^\s+/,'').replace(/\s+$/,'');
	       return(cadena);
	} 

	function drawChart(straux) {

		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'contenido_chart_'+straux}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);
			var data = google.visualization.arrayToDataTable(eval(objJson.registros));
	        
	        var idioma = [];
				
				sw=0;
				for (var x = 0; x < 9 ; x++) {

				  if(x >= objJson.colores[0].length){
				  	idioma[x] = 'gray';
				  }else{
				  	idioma[x] = objJson.colores[0][x];
				  }
				  

				  sw++;
				};

				//Hacemos hasta 10 idiomas

		        var options = {
		          	//title: 'Los que hablan en '+objJson.nombres_registros[i]+' leen en:',
		          	title: '',
		          	is3D: true,
					slices: {
						
					     0: { color: idioma[0]},
					     1: { color: idioma[1]},
					     2: { color: idioma[2]},
					     3: { color: idioma[3]},
					     4: { color: idioma[4]},
					     5: { color: idioma[5]},
					     6: { color: idioma[6]},
					     7: { color: idioma[7]},
					     8: { color: idioma[8]},
					     9: { color: idioma[9]}
						}
					}

	        var chart = new google.visualization.PieChart(document.getElementById(straux+'_div'));

	        chart.draw(data, options);

    	})//done
    }

    function comparar_idiomas(){
    	$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'comparar_idiomas'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);
			$(".listado_piecharts").html("");
			//alert(objJson.total_registros)

			var top = 6;
			var count_top = 1;
			for (var i = 0; i <= objJson.total_registros-1; i++) {
				//Vamos creando las tartas
				if (count_top <= top) {
				//Introducimos en el html un div
				$(".listado_piecharts").html($(".listado_piecharts").html()+"<div class='caja_tarta'><span class='tartatit' id='tarta_"+i+"_tit'></span><div id='tarta_"+i+"' class='cajaconttarta'></div></div>");

				//Y ahora pintamos las tartas en el div añadido
				var data = google.visualization.arrayToDataTable(eval(objJson.registros[i]));

				$("#tarta_"+i+"_tit").text(trans.__('Los que hablan en %1% leen en:').replace('%1%', trans.__(objJson.nombres_registros[i])))
				//alert(objJson.colores[i]);
				var red = 'red';

				var idioma = [];
				
				sw=0;
				for (var x = 0; x < 9 ; x++) {

				  if(x >= objJson.colores[i].length){
				  	idioma[x] = 'gray';
				  }else{
				  	idioma[x] = objJson.colores[i][x];
				  }
				  

				  sw++;
				};

				//Hacemos hasta 10 idiomas

		        var options = {
		          	//title: 'Los que hablan en '+objJson.nombres_registros[i]+' leen en:',
		          	is3D: true,
					slices: {
						
					     0: { color: idioma[0]},
					     1: { color: idioma[1]},
					     2: { color: idioma[2]},
					     3: { color: idioma[3]},
					     4: { color: idioma[4]},
					     5: { color: idioma[5]},
					     6: { color: idioma[6]},
					     7: { color: idioma[7]},
					     8: { color: idioma[8]},
					     9: { color: idioma[9]}
				}
		        };

		        var chart = new google.visualization.PieChart(document.getElementById('tarta_'+i));

		        chart.draw(data, options);

				count_top ++;
				};//fin de si es top	
			};


    	})//done
    }

	function cargador(){
		Leer_Opcion_Comparacion();
		cargar_cabeceras();
	}

	function cargar_cabeceras_ini(){
		Leer_Opcion_Comparacion();
		drawChart("idiomauser")
		drawChart("idiomanav")
		Inicializar_datatables();
		comparar_idiomas();
		//cargar_datos_extra();
	}

	function cargar_cabeceras(){
		Leer_Opcion_Comparacion();
		drawChart("idiomauser")
		drawChart("idiomanav")
		Recargar_Datatables();
		comparar_idiomas();
		cargarTablaIdiomas();
		//cargar_datos_extra();
	}

	google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(cargar_cabeceras_ini);

	function Recargar_Datatables() {
		table_datatable_idiomauser.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
		table_datatable_idiomanav.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
	}

	function Inicializar_datatables(){

		if( $("#dat_app").val() == 0 ){	
			appdata = true;
		}else{
			appdata = false;
		}
		
		//DATATABLE DE idiomauser -----------------------------------------------------------------------------------------
		table_datatable_idiomauser = $('#table_datatable_idiomauser').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		      {
		          "targets": [ 2 ],
		          "visible": appdata
		      }
		    ],
			"columns":[
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_idiomauser'}
			}		
		});		

		//DATATABLE idiomauser ----------------------------------------------------------------------------------------- 		
		//DATATABLE DE idiomanav -----------------------------------------------------------------------------------------
		table_datatable_idiomanav = $('#table_datatable_idiomanav').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [10, 10, 25, 50, -1], [10, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		      {
		          "targets": [ 2 ],
		          "visible": appdata
		      }
		    ],
			"columns":[
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_idiomanav'}
			}		
		});		
		//DATATABLE idiomanav ----------------------------------------------------------------------------------------- 		

		cargarTablaIdiomas();

	}

	function cargarTablaIdiomas(){

		$("#tabla_datatable_idioma").html("");
		$("#tabla_datatable_idioma").load("../informes/idiomas_ajax_tablaidiomas.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val());

	}


	$(document).ready(function() {

		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion(1);	
		//Opciones de comparación de datos -------------------------------------------

		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		//Cargar_Datos_Extra("dispositivos");
		//Cargar_Datos_Extra("navegadores");
		//Cargar_Datos_Extra("interes");
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------



	

});	

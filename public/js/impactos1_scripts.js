	var rickshaw_duracionmediasesion_legend=[];
	var rickshaw_sesiones=[];
	var rickshaw_sesiones_legend=[];
	var rickshaw_paginasvistas=[];
	var rickshaw_paginasvistas_legend=[];
	var rickshaw_paginasvistassesion=[];
	var rickshaw_paginasvistassesion_legend=[];
	var rickshaw_usuarios=[];
	var rickshaw_usuarios_legend=[];		
	var rickshaw_usuarios_nuevos=[];
	var rickshaw_usuarios_recurrentes=[];
	var rickshaw_usuarios_nuevos_legend=[];
	var rickshaw_usuarios_recurrentes_legend=[];
	var rickshaw_dimensiones;
	var rickshaw_tasarebote=[];
	var rickshaw_tasarebote_legend=[];
	var rickshaw_dimensiones=[];
	var rickshaw_dimensiones_legend=[];
	var t_datatable;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	var chart_sesiones;
	
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra();
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	/*$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion ();			
			Cargar_Datos_Extra();
		}
	}).trigger('change');	*/
	 


	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	function Recargar_Datatable() {
		t_datatable.api().ajax.url('../informes/informes_ajax.php?strPerspectiva=' + $("#dat_perspectiva").val() + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val()).load();
	}
	
		
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total_img(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (ValorF==""){ ValorF = Valor};
		if (ValorAntF==""){ ValorAntF = ValorAnt};

		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			} else {
				strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {

			if ((Valor =="0" || Valor=="0.0") && (ValorAnt =="0" || ValorAnt=="0.0")) {
				return "";
			};

			return strAux + ' ' + ValorF;
		}
	}

	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}
			
	function Cargar_Datos_Extra() {
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		//alert($("#dat_idvista").val());
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strPerspectiva=' + $("#dat_perspectiva").val() + '&blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val(),
			data: { strFuncion: 'Impactos1_datos_extra'}
		})
		.done(function (respuesta) {

			/*Negrita para las dos ultimas posiciones de la tabla*/
			$("tbody tr").each(function(){
				$(this).children("td:last").css({
					"color": "black"
				});
				$(this).children("td:nth-last-child(2)").css({
					"color": "black"
				});
			})

			//modifico los gráficos, medias, totales y destacados
			var objJson = jQuery.parseJSON(respuesta);
			

			nombre_dimension = objJson.datos[0].nombre_dimension;

			totales_datatable = objJson.datos[0].totales_datatable;
			totales_datatable_comp = objJson.datos[1].totales_datatable;
			var total = "";
			var total_comp = "";
			var total_array = [];
			var total_array_comp = [];
			var cont = 0;

			//rellenamos una array con datos del datatable
			for (var i = 0; i < totales_datatable.length; i++) {

				if(totales_datatable[i]==","){
					total_array[cont] = total;
					cont++;
					total="";
				}else{
					total += totales_datatable[i];
				}
				

			};
			total_array[cont++] = total;
			
			if(blnComparacion){

				total = "";
				cont  = 0;

				//Rellenamos una array con los datos de comparacion
				for (var i = 0; i < totales_datatable_comp.length; i++) {

					if(totales_datatable_comp[i]==","){
						total_array_comp[cont] = total;
						cont++;
						total="";
					}else{
						total += totales_datatable_comp[i];
					}
				

				};
				total_array_comp[cont++] = total;

			}

			//Al tener las dos arrays rellenas con los datos las recorremos y comparamos
			//Deberia tener la misma longitud

			for (var i = 0; i < totales_datatable.length; i++) {

				//Formateando texto
				numF = "";
				numF_comp = ""
				$("#totales"+i).text(total_array[i]);	
				if(i % 2){
					$("#totales"+i).number( true, 2 );
					numF = $("#totales"+i).text() + "%";

					//Si es 0 en blanco
					if(numF=="0.00%"){numF = ""}
					//alert(numF);
				}else{
					numF = $("#totales"+i).text()
					if(numF=="0"){numF = "";}
				}

				//Con flechitas de comparación
				if(blnComparacion){

					$("#totales"+i).text(total_array_comp[i]);	
					if(i % 2){
						$("#totales"+i).number( true, 2 );
						numF_comp = $("#totales"+i).text() + "%";

						//Si es 0 en blanco
						if(numF_comp=="0.00%"){numF_comp = ""}
						
					}else{
						numF_comp = $("#totales"+i).text()
						if(numF_comp=="0"){numF_comp = "";}
					}

					//Formateando texto
					//alert(total_array[i]);
					//aux = DevolverContenido_Total_img(total_array[i],total_array_comp[i],numF,numF_comp,false);
					aux = total_array[i];
					//alert(aux);
					$("#totales"+i).html(aux);

				}else{

					$("#totales"+i).html(numF)
				}

			};



			//y tmb ponemos en la ultima posicion el ultimo dato
			/*$("#totales"+cont).text(total);	
			if(cont % 2){
				$("#totales"+cont).number( true, 2 );
				$("#totales"+cont).text($("#totales"+cont).text() + "%");
				if($("#totales"+cont).text()=="0.00%"){$("#totales"+cont).text("");}

				}else{
					if($("#totales"+cont).text()=="0"){$("#totales"+cont).text("");}
				}*/


			if(blnComparacion){

				//$("#total_conversion").html(DevolverContenido_Total_img(objJson.datos[0].total_conversion,objJson.datos[1].total_conversion,objJson.datos[0].total_conversionF,objJson.datos[1].total_conversionF,false));
				//$("#total_completion").html(DevolverContenido_Total_img(objJson.datos[0].total_completion,objJson.datos[1].total_completion,objJson.datos[0].total_completionF,objJson.datos[1].total_completionF,false));
				$("#total_rate_ico").html(DevolverContenido_Total(objJson.datos[0].total_conversion,objJson.datos[1].total_conversion,objJson.datos[0].total_conversionF,objJson.datos[1].total_conversionF,true));
				$("#total_impactos_ico").html(DevolverContenido_Total(objJson.datos[0].total_completion,objJson.datos[1].total_completion,objJson.datos[0].total_completionF,objJson.datos[1].total_completionF,true));
				$("#total_completion").html(objJson.datos[0].total_completionF+" <span class='porcDT'>("+ objJson.datos[0].total_conversionF + ")</span>");

			}else{
				$("#total_completion").html(objJson.datos[0].total_completionF+" <span class='porcDT'>("+ objJson.datos[0].total_conversionF + ")</span>");
				$("#total_impactos_ico").html("");
				$("#total_rate_ico").html("");

			}




			total_rate= objJson.datos[0].total_conversion;
			total_impactos = objJson.datos[0].total_completion;
			/*if (blnComparacion==true) {numComparaciones=1} else {numComparaciones=0};
			for (I = 0; I <= numComparaciones; ++I) {
				totalales = objJson.datos[0].total_usuariosnuevosF; 
				total_usuarios_recurrentes = objJson.datos[0].total_usuariosrecurrentes;
			}*/

			//alert(objJson.datos[0].cont_tot);
	

			//4. DESTACADOS. Tile Stats
			$("#total_impactos").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('.num'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					//end = eval('objJson.datos[0].'+ $(el).find('.num').attr('id')),
					end = eval(total_impactos),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', ''),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + Math.round(o.curr).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix);
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

			//4. DESTACADOS. Tile Stats
			$("#total_rate").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('.num'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					//end = eval('objJson.datos[0].'+ $(el).find('.num').attr('id')),
					end = eval(total_rate),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix +"%");
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});
			
			//5. NOMBRE DIMENSIÓN (PERSPECTIVA)			
			$("#cab_perspectiva").text(nombre_dimension);	

		});
	}

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		Leer_Opcion_Comparacion();	
		Recargar_Datatable();

		Cargar_Datos_Extra();
		//chart_sesiones.destroy();
	}

	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());
		//alert(datFechaInicioFiltro);
		/*var valComparacion
		if(comparacion!=1){*/
			//valComparacion = $("#select_Comparacion").val();
			valComparacion = $("#dat_comparador").val();
		/*}else{
			valComparacion = 2
		}*/
		//alert(valComparacion);
		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}

		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}

	$(document).ready(function() {
	
		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion();	
		//Opciones de comparación de datos -------------------------------------------
		
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		Cargar_Datos_Extra();
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		

	});	
	var table_datatable_productos;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();
		//alert($("#dat_comparador").val())
		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	function Recargar_Datatable() {
		//alert(blnComparacion)
		$('#table_datatable_productos').dataTable().api().ajax.url('../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&perspectiva=" + $("#perspectiva").val() + "&perspectiva_seg=" + $("#perspectiva_seg").val() + "&idpropiedad="+$("#dat_propiedad").val() ).load();
	}
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------

	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico,sub){
		if(ValorAnt != ''){
			if (parseFloat(Valor) > parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + sub+'</div>';
			} else {
				if (parseFloat(Valor) == parseFloat(ValorAnt)) {
					// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
					strAux = '<div class="gris comparador"> ' + ValorAntF + sub+'</div>';
				} else {
					// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
					strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + sub+'</div>';
				}
			}
			if (blnSoloGrafico) {
				return strAux
			} else {
				return strAux + ' ' + ValorF;
			}
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}




	function formato_numero(numero, decimales, separador_decimal, separador_miles){     
		numero=parseFloat(numero);     
		if(isNaN(numero)){         
			return "";}     
		if(decimales!==undefined){        
		 	// Redondeamos         
			numero=numero.toFixed(decimales);     
		}     
		// Convertimos el punto en separador_decimal     
		numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
		if(separador_miles){         
			// Añadimos los separadores de miles         
			var miles=new RegExp("(-?[0-9]+)([0-9]{3})");         
			while(miles.test(numero)) {             
				numero=numero.replace(miles, "$1" + separador_miles + "$2");         
			}     
		}     
		return numero; 
	}

			

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		Leer_Opcion_Comparacion();	
		Recargar_Datatable();
		cargar_datos_extras()
	}

	function segundaperspectiva(){
		if( $("#perspectiva_seg").val() != "" ){

			table_datatable_productos.column( 1 ).visible( true );
			cargador();

		}else{

			table_datatable_productos.column( 1 ).visible( false );
			cargador();

		}
		
	}


	function DevolverContenido_Total_img(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			strAux = '<img src="../images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				strAux = '<img src="../images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			} else {
				strAux = '<img src="../images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}


	function cargar_datos_extras(){
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&idpropiedad="+$("#dat_propiedad").val(),
			data: { strFuncion: 'campanas_informes_productos_extra'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);

			$("#total_nv").html( DevolverContenido_Total_img( objJson.datos[0].total_nv,objJson.datos[1].total_nv,objJson.datos[0].total_nvF,objJson.datos[1].total_nvF,false)+'' );
			$("#total_nuv").html( DevolverContenido_Total_img( objJson.datos[0].total_nuv,objJson.datos[1].total_nuv,objJson.datos[0].total_nuvF,objJson.datos[1].total_nuvF,false)+'' );
			$("#total_tv").html( DevolverContenido_Total_img( parseFloat(objJson.datos[0].total_tv).toFixed(2),parseFloat(objJson.datos[1].total_tv).toFixed(2),objJson.datos[0].total_tvF,objJson.datos[1].total_tvF,false)+'€' );
			$("#total_pmpv").html( DevolverContenido_Total_img( parseFloat(objJson.datos[0].total_pmpv).toFixed(2),parseFloat(objJson.datos[1].total_pmpv).toFixed(2),objJson.datos[0].total_pmpvF,objJson.datos[1].total_pmpvF,false)+'€' );
			$("#total_mpp").html( DevolverContenido_Total_img( objJson.datos[0].total_mpp,objJson.datos[1].total_mpp,objJson.datos[0].total_mppF,objJson.datos[1].total_mppF,false)+'' );

			$("#resumen_producto1").html( DevolverContenido_Total( parseFloat(objJson.datos[0].resumen_producto1).toFixed(2),parseFloat(objJson.datos[1].resumen_producto1).toFixed(2),objJson.datos[0].resumen_producto1F,objJson.datos[1].resumen_producto1F,false,'€')+'€' );
			$("#resumen_producto2").html( DevolverContenido_Total( parseFloat(objJson.datos[0].resumen_producto2).toFixed(2),parseFloat(objJson.datos[1].resumen_producto2).toFixed(2),objJson.datos[0].resumen_producto2F,objJson.datos[1].resumen_producto2F,false,'€')+'€' );
			$("#resumen_marca1").html( DevolverContenido_Total( parseFloat(objJson.datos[0].resumen_marca1).toFixed(2),parseFloat(objJson.datos[1].resumen_marca1).toFixed(2),objJson.datos[0].resumen_marca1F,objJson.datos[1].resumen_marca1F,false,'€')+'€' );
			$("#resumen_marca2").html( DevolverContenido_Total( parseFloat(objJson.datos[0].resumen_marca2).toFixed(2),parseFloat(objJson.datos[1].resumen_marca2).toFixed(2),objJson.datos[0].resumen_marca2F,objJson.datos[1].resumen_marca2F,false,'€')+'€' );
			$("#resumen_categoria1").html( DevolverContenido_Total( parseFloat(objJson.datos[0].resumen_categoria1).toFixed(2),parseFloat(objJson.datos[1].resumen_categoria1).toFixed(2),objJson.datos[0].resumen_categoria1F,objJson.datos[1].resumen_categoria1F,false,'€')+'€' );
			$("#resumen_categoria2").html( DevolverContenido_Total( parseFloat(objJson.datos[0].resumen_categoria2).toFixed(2),parseFloat(objJson.datos[1].resumen_categoria2).toFixed(2),objJson.datos[0].resumen_categoria2F,objJson.datos[1].resumen_categoria2F,false,'€')+'€' );
			$("#resumen_listas1").html( DevolverContenido_Total( parseFloat(objJson.datos[0].resumen_listas1).toFixed(2),parseFloat(objJson.datos[1].resumen_listas1).toFixed(2),objJson.datos[0].resumen_listas1F,objJson.datos[1].resumen_listas1F,false,'€')+'€' );
			$("#resumen_listas2").html( DevolverContenido_Total( objJson.datos[0].resumen_listas2,objJson.datos[1].resumen_listas2,objJson.datos[0].resumen_listas2F,objJson.datos[1].resumen_listas2F,false,'€')+'€' );

			$("#resumen_nombre_producto1").html( objJson.datos[0].resumen_nombre_producto1 );
			$("#resumen_nombre_producto2").html( objJson.datos[0].resumen_nombre_producto2 );
			$("#resumen_nombre_marca1").html( objJson.datos[0].resumen_nombre_marca1 );
			$("#resumen_nombre_marca2").html( objJson.datos[0].resumen_nombre_marca2 );
			$("#resumen_nombre_categoria1").html( objJson.datos[0].resumen_nombre_categoria1 );
			$("#resumen_nombre_categoria2").html( objJson.datos[0].resumen_nombre_categoria2 );
			$("#resumen_nombre_listas1").html( objJson.datos[0].resumen_nombre_listas1 );
			$("#resumen_nombre_listas2").html( objJson.datos[0].resumen_nombre_listas2 );


		})
	}

	$(document).ready(function() {

		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion(1);	
		//Opciones de comparación de datos -------------------------------------------


		//DATATABLE DE PEDIDOS -----------------------------------------------------------------------------------------
		table_datatable_productos = $('#table_datatable_productos').DataTable({
			"language": {
				"url": "/public/assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[2,'desc']],
			"searching": false,
			//Exportaciones 
			//"dom": 'Trt <filp "fondo">',
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
		        },
		     "columnDefs": [
	        {
	            "targets": [ 1 ],
	            "visible": false
	        }
	        ],
			"columns":[
				null,
				null,
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val() + "&perspectiva=" + $("#perspectiva").val() + "&perspectiva_seg=" + $("#perspectiva_seg").val() + "&idpropiedad="+$("#dat_propiedad").val(), 
				"data": {"strFuncion": 'datatable_informes_productos'}
			}	
		});	

	cargar_datos_extras();


	$(".radiopers_per").on("click",function(){



		var data  = $(this).attr("value");
		var pers  = $("#perspectiva").val();
		var persb = $("#perspectiva_seg").val();

		if($("#perspectiva_camb").val() == ""){

			if(data != pers){
				//Si es diferente la primera perspectiva a la pinchada comprobamos si la segunda tiene perspectiva
				if(persb == ""){
					$(".radiopers_per").removeClass("active_seg");
					$(this).addClass("active_seg");
					$("#perspectiva_seg").val(data);
					segundaperspectiva()
					$("#cab_perspectiva_seg").text($(this).text());
					cargador();
				}else{
					//Si persb == data eliminamos la segunda perspectiva
					if(persb == data){
						$(".radiopers_per").removeClass("active_seg");
						$("#perspectiva_seg").val("");	
						segundaperspectiva()
						$("#cab_perspectiva_seg").text("");
						cargador();
					}else{
						$(".radiopers_per").removeClass("active_seg");
						$(this).addClass("active_seg");
						$("#perspectiva_seg").val(data);	
						segundaperspectiva()
						$("#cab_perspectiva_seg").text($(this).text());
						cargador();
					}						
				}
							
			}else{
				//Si es igual significa que se pone en modo cambio de perspectiva 1
				$("#perspectiva_camb").val(data);
				$(".radiopers_per").removeClass("active_pri");
				$(this).addClass("active_select");
			}		
		}else{

			$(".radiopers_per").removeClass("active_select");
			$(this).addClass("active_pri");
			$("#perspectiva_camb").val("");
			$("#perspectiva").val(data);
			segundaperspectiva()
			$("#cab_perspectiva").text($(this).text());
			cargador();
		}

	});


});	



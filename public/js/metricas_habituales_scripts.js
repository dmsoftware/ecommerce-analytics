	var rickshaw_duracionmediasesion_legend=[];
	var rickshaw_sesiones=[];
	var rickshaw_sesiones_legend=[];
	var rickshaw_paginasvistas=[];
	var rickshaw_paginasvistas_legend=[];
	var rickshaw_paginasvistassesion=[];
	var rickshaw_paginasvistassesion_legend=[];
	var rickshaw_usuarios=[];
	var rickshaw_usuarios_legend=[];		
	var rickshaw_usuarios_nuevos=[];
	var rickshaw_usuarios_recurrentes=[];
	var rickshaw_usuarios_nuevos_legend=[];
	var rickshaw_usuarios_recurrentes_legend=[];
	var rickshaw_dimensiones;
	var rickshaw_tasarebote=[];
	var rickshaw_tasarebote_legend=[];
	var rickshaw_dimensiones=[];
	var rickshaw_dimensiones_legend=[];
	var t_datatable;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	var chart_sesiones;
	var strPaisGa;
	
	//RICKSHAW: Colores series
	var strColorSerie1 = '#6CB5F4';
	var strColorSerie2 = '#E88731';	

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra();
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	/*$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion ();			
			Cargar_Datos_Extra();
		}
	}).trigger('change');	*/
	 


	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	function Recargar_Datatable() {
		t_datatable.api().ajax.url('../informes/informes_ajax.php?strPerspectiva=' + $("#dat_perspectiva").val() + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val()).load();
	}
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function Cargar_Graficos_Evolucion() {
		//alert("llega");
		//GRAFICOS EVOLUCIÓN SPARKLINE
		$.fn.sparkline.defaults.common.height = '60';			
		
		// VACIAMOS LA CAPA DEL GRAFICO EVOLUCIÓN AMPLIADO
		$('#rickshaw_ampliacion').text(''); //Eliminamos contenido anterior
		$('#y_axis_rickshaw_ampliacion').text(''); //Eliminamos contenido anterior
		$('#x_axis_rickshaw_ampliacion').text(''); //Eliminamos contenido anterior		
		
		// GRAFICOS EVOLUTIVOS RICKSHAW
		// 1.- SESIONES ---------------------------------------------------------------------------------
			$('#rickshaw_sesiones').text(''); //Eliminamos contenido anterior
			//Añadimos las series necesarias
			strAuxSeries='{name: \'Sesiones\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_sesiones[0] +'}'
			if (blnComparacion) {strAuxSeries=strAuxSeries + ',{name: \'Sesiones\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_sesiones[1] +'}'}	
			var graph_sesiones = new Rickshaw.Graph( {
				element: document.querySelector("#rickshaw_sesiones"),
				width: $('#rickshaw_sesiones').closest('th').width(),
				height: 60,	
				renderer: 'line',
				series: eval('['+strAuxSeries+']')
			} );
			graph_sesiones.render();
			$('#rickshaw_sesiones').append('<a id="rickshaw_sesiones_ampliar" class="zoomLupa"><i class="fa fa-search-plus"></i></a>');
		// 1.- SESIONES ---------------------------------------------------------------------------------
		// 2.- PÁGINAS VISTAS ---------------------------------------------------------------------------------
			$('#rickshaw_paginasvistas').text(''); //Eliminamos contenido anterior
			//Añadimos las series necesarias
			strAuxSeries='{name: \'Páginas vistas\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_paginasvistas[0] +'}'
			if (blnComparacion) {strAuxSeries=strAuxSeries + ',{name: \'Páginas vistas\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_paginasvistas[1] +'}'}				
			var graph_paginasvistas = new Rickshaw.Graph( {
				element: document.querySelector("#rickshaw_paginasvistas"),
				width: $('#rickshaw_paginasvistas').closest('th').width(),
				height: 60,	
				renderer: 'line',
				series: eval('['+strAuxSeries+']')
			} );
			graph_paginasvistas.render();
			$('#rickshaw_paginasvistas').append('<a id="rickshaw_paginasvistas_ampliar" class="zoomLupa"><i class="fa fa-search-plus"></i></a>');
		// 2.- PÁGINAS VISTAS ---------------------------------------------------------------------------------	
		// 3.- PÁGINAS VISTAS SESION---------------------------------------------------------------------------------
			$('#rickshaw_paginasvistassesion').text(''); //Eliminamos contenido anterior
			//Añadimos las series necesarias
			strAuxSeries='{name: \'Páginas vistas por sesión\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_paginasvistassesion[0] +'}'
			if (blnComparacion) {strAuxSeries=strAuxSeries + ',{name: \'Páginas vistas por sesión\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_paginasvistassesion[1] +'}'}				
			var graph_paginasvistassesion = new Rickshaw.Graph( {
				element: document.querySelector("#rickshaw_paginasvistassesion"),
				width: $('#rickshaw_paginasvistassesion').closest('th').width(),
				height: 60,	
				renderer: 'line',
				series: eval('['+strAuxSeries+']')
			} );
			graph_paginasvistassesion.render();
			$('#rickshaw_paginasvistassesion').append('<a id="rickshaw_paginasvistassesion_ampliar" class="zoomLupa"><i class="fa fa-search-plus"></i></a>');
		// 3.- PÁGINAS VISTAS SESION---------------------------------------------------------------------------------
		// 4.- USUARIOS ---------------------------------------------------------------------------------
			$('#rickshaw_usuarios').text(''); //Eliminamos contenido anterior
			//Añadimos las series necesarias
			strAuxSeries='{name: \'Usuarios\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_usuarios[0] +'}'
			if (blnComparacion) {strAuxSeries=strAuxSeries + ',{name: \'Usuarios\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_usuarios[1] +'}'}				
			var graph_usuarios = new Rickshaw.Graph( {
				element: document.querySelector("#rickshaw_usuarios"),
				width: $('#rickshaw_usuarios').closest('th').width(),
				height: 60,	
				renderer: 'line',
				series: eval('['+strAuxSeries+']')
			} );
			graph_usuarios.render();
			$('#rickshaw_usuarios').append('<a id="rickshaw_usuarios_ampliar" class="zoomLupa"><i class="fa fa-search-plus"></i></a>');
		// 4.- USUARIOS ---------------------------------------------------------------------------------

		// 5.- USUARIOS NUEVOS/RECURRENTES ---------------------------------------------------------------------------------		
	    // 5.- USUARIOS NUEVOS---------------------------------------------------------------------------------
			$('#rickshaw_usuariosnuevos').text(''); //Eliminamos contenido anterior
			//Añadimos las series necesarias
			strAuxSeries='{name: \'Usuarios nuevos\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_usuarios_nuevos[0] +'}'
			if (blnComparacion) {strAuxSeries=strAuxSeries + ',{name: \'Usuarios\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_usuarios_nuevos[1] +'}'}				
			var graph_usuarios = new Rickshaw.Graph( {
				element: document.querySelector("#rickshaw_usuariosnuevos"),
				width: $('#rickshaw_usuariosnuevos').closest('th').width(),
				height: 60,	
				renderer: 'line',
				series: eval('['+strAuxSeries+']')
			} );
			graph_usuarios.render();
			$('#rickshaw_usuariosnuevos').append('<a id="rickshaw_usuariosnuevos_ampliar" class="zoomLupa"><i class="fa fa-search-plus"></i></a>');
		// 5.- USUARIOS NUEVOS ---------------------------------------------------------------------------------	
		// 6.- USUARIOS RECURRENTES---------------------------------------------------------------------------------
			$('#rickshaw_usuariosrecurrentes').text(''); //Eliminamos contenido anterior
			//Añadimos las series necesarias
			strAuxSeries='{name: \'Usuarios recurrentes\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_usuarios_recurrentes[0] +'}'
			if (blnComparacion) {strAuxSeries=strAuxSeries + ',{name: \'Usuarios\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_usuarios_recurrentes[1] +'}'}				
			var graph_usuarios = new Rickshaw.Graph( {
				element: document.querySelector("#rickshaw_usuariosrecurrentes"),
				width: $('#rickshaw_usuariosrecurrentes').closest('th').width(),
				height: 60,	
				renderer: 'line',
				series: eval('['+strAuxSeries+']')
			} );
			graph_usuarios.render();
			$('#rickshaw_usuariosrecurrentes').append('<a id="rickshaw_usuariosrecurrentes_ampliar" class="zoomLupa"><i class="fa fa-search-plus"></i></a>');
		// 6.- USUARIOS RECURRENTES ---------------------------------------------------------------------------------linechart_tasarebote		
		if($("#dat_app").val() == 0){
		// 7.- TASA DE REBOTE---------------------------------------------------------------------------------
			$('#rickshaw_tasarebote').text(''); //Eliminamos contenido anterior
			//Añadimos las series necesarias
			strAuxSeries='{name: \'Tasa rebote\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_tasarebote[0] +'}'
			if (blnComparacion) {strAuxSeries=strAuxSeries + ',{name: \'Tasa rebote\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_tasarebote[1]+'}'}				
			var graph_usuarios = new Rickshaw.Graph( {
				element: document.querySelector("#rickshaw_tasarebote"),
				width: $('#rickshaw_tasarebote').closest('th').width(),
				height: 60,	
				renderer: 'line',
				series: eval('['+strAuxSeries+']')
			} );
			graph_usuarios.render();
			$('#rickshaw_tasarebote').append('<a id="rickshaw_tasarebote_ampliar" class="zoomLupa"><i class="fa fa-search-plus"></i></a>');
		// 7.- TASA DE REBOTE ---------------------------------------------------------------------------------
		}
		// 8.- DURACIÓN MEDIA ---------------------------------------------------------------------------------
			$('#rickshaw_duracionmediasesion').text(''); //Eliminamos contenido anterior
			//Añadimos las series necesarias
			strAuxSeries='{name: \'Duracion media\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_duracionmediasesion[0] +'}'
			if (blnComparacion) {strAuxSeries=strAuxSeries + ',{name: \'Duracion media\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_duracionmediasesion[1]+'}'}				
			var graph_usuarios = new Rickshaw.Graph( {
				element: document.querySelector("#rickshaw_duracionmediasesion"),
				width: $('#rickshaw_duracionmediasesion').closest('th').width(),
				height: 60,	
				renderer: 'line',
				series: eval('['+strAuxSeries+']')
			} );
			graph_usuarios.render();
			$('#rickshaw_duracionmediasesion').append('<a id="rickshaw_duracionmedia_ampliar" class="zoomLupa"><i class="fa fa-search-plus"></i></a>');
		// 8.- DURACIÓN MEDIA ---------------------------------------------------------------------------------
	}	

	//function redondeo_decimales(numero) { var original=parseFloat(numero); var result=Math.round(original*100)/100 ; return result; }

	function Cargar_Graficos_Evolucion_Ampliacion(metrica) {	

		// VACIAMOS LA CAPA DEL GRAFICO EVOLUCIÓN AMPLIADO
		$('#rickshaw_ampliacion').text(''); //Eliminamos contenido anterior
		$('#y_axis_rickshaw_ampliacion').text(''); //Eliminamos contenido anterior
		$('#x_axis_rickshaw_ampliacion').text(''); //Eliminamos contenido anterior
		$('#rickshaw_header').text('');

		//Añadimos las series necesarias
		switch(metrica) {
			case 'sesiones':
				$('#rickshaw_header').text(trans.__('Sesiones'));
				strAuxSeries='{id: 0, name: \'' + trans.__('Sesiones') + '\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_sesiones[0] +',dataFormato:' + rickshaw_sesiones_legend[0] +'}'
				if (blnComparacion) {strAuxSeries=strAuxSeries + ',{id: 1, name: \'' + trans.__('Sesiones') + '\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_sesiones[1] +',dataFormato:' + rickshaw_sesiones_legend[1] +'}'}	
				break;				
			case 'paginasvistas':
				$('#rickshaw_header').text(trans.__('Páginas vistas'));
				strAuxSeries='{id: 0, name: \'' + trans.__('Páginas vistas') + '\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_paginasvistas[0] +',dataFormato:' + rickshaw_paginasvistas_legend[0] +'}'
				if (blnComparacion) {strAuxSeries=strAuxSeries + ',{id: 1, name: \'' + trans.__('Páginas vistas') + '\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_paginasvistas[1] +',dataFormato:' + rickshaw_paginasvistas_legend[1] +'}'}
				break;	
			case 'paginasvistassesion':
				$('#rickshaw_header').text(trans.__('Páginas vistas por sesión'));
				strAuxSeries='{id: 0, name: \'' + trans.__('Páginas vistas por sesión') + '\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_paginasvistassesion[0] +',dataFormato:' + rickshaw_paginasvistassesion_legend[0] +'}'
				if (blnComparacion) {strAuxSeries=strAuxSeries + ',{id: 1, name: \'' + trans.__('Páginas vistas por sesión') + '\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_paginasvistassesion[1] +',dataFormato:' + rickshaw_paginasvistassesion_legend[1] +'}'}
				break;
			case 'usuarios':
				$('#rickshaw_header').text(trans.__('Usuarios'));
				strAuxSeries='{id: 0, name: \'' + trans.__('Usuarios') + '\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_usuarios[0] +',dataFormato:' + rickshaw_usuarios_legend[0] +'}'
				if (blnComparacion) {strAuxSeries=strAuxSeries + ',{id: 1, name: \'' + trans.__('Usuarios') + '\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_usuarios[1] +',dataFormato:' + rickshaw_usuarios_legend[1] +'}'}
				break;
			case 'usuariosnuevos':
				$('#rickshaw_header').text(trans.__('Usuarios nuevos'));
				strAuxSeries='{id: 0, name: \'' + trans.__('Usuarios nuevos') + '\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_usuarios_nuevos[0] +',dataFormato:' + rickshaw_usuarios_nuevos_legend[0] +'}'
				if (blnComparacion) {strAuxSeries=strAuxSeries + ',{id: 1, name: \'' + trans.__('Usuarios nuevos') + '\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_usuarios_nuevos[1] +',dataFormato:' + rickshaw_usuarios_nuevos_legend[1] +'}'}
				break;	
			case 'usuariosrecurrentes':
				$('#rickshaw_header').text(trans.__('Usuarios recurrentes'));
				strAuxSeries='{id: 0, name: \'' + trans.__('Usuarios recurrentes') + '\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_usuarios_recurrentes[0] +',dataFormato:' + rickshaw_usuarios_recurrentes_legend[0] +'}'
				if (blnComparacion) {strAuxSeries=strAuxSeries + ',{id: 1, name: \'' + trans.__('Usuarios recurrentes') + '\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_usuarios_recurrentes[1] +',dataFormato:' + rickshaw_usuarios_recurrentes_legend[1] +'}'}
				break;	
			case 'tasarebote':
				$('#rickshaw_header').text(trans.__('Tasa rebote'));
				strAuxSeries='{id: 0, name: \'' + trans.__('Tasa rebote') + '\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_tasarebote[0] +',dataFormato:' + rickshaw_tasarebote_legend[0] +'}'
				if (blnComparacion) {strAuxSeries=strAuxSeries + ',{id: 1, name: \'' + trans.__('Tasa rebote') + '\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_tasarebote[1] +',dataFormato:' + rickshaw_tasarebote_legend[1] +'}'}
				break;
			case 'duracionsesion':
				$('#rickshaw_header').text(trans.__('Duracion media'));
				strAuxSeries='{id: 0, name: \'' + trans.__('Duracion media') + '\',color: \''+ strColorSerie1 +'\',data:' + rickshaw_duracionmediasesion[0] +',dataFormato:' + rickshaw_duracionmediasesion_legend[0] +'}'
				if (blnComparacion) {strAuxSeries=strAuxSeries + ',{id: 1, name: \'' + trans.__('Duracion media') + '\',color: \''+ strColorSerie2 + '\',data:' + rickshaw_duracionmediasesion[1] +',dataFormato:' + rickshaw_duracionmediasesion_legend[1] +'}'}
				break;	
		}

		//alert($("#rickshaw_ampliacion").width());
		var graph_ampliacion = new Rickshaw.Graph( {
			element: document.querySelector("#rickshaw_ampliacion"),
			//width: $("#rickshaw_ampliacion").width(),
			width: 750,
			height: 400,	
			renderer: 'line',
			series: eval('['+strAuxSeries+']')
		} );			
		var x_ticks = new Rickshaw.Graph.Axis.X( {
			graph: graph_ampliacion,
			orientation: 'bottom',
			element: document.getElementById('x_axis_rickshaw_ampliacion'),
			tickFormat: function(x) {
				var map = rickshaw_dimensiones;
				return map[x];
			}
		} );		
		var y_axis = new Rickshaw.Graph.Axis.Y( {
				graph: graph_ampliacion,
				orientation: 'left',
				tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
				element: document.getElementById('y_axis_rickshaw_ampliacion'),
		} );	
		var hoverDetail = new Rickshaw.Graph.HoverDetail( {
			graph: graph_ampliacion,		
			formatter: function(series, x, y) {		
				arrayDatosFormato=eval(series.dataFormato);
				return series.name + ": " + arrayDatosFormato[x-1]  + '<br>' + nombre_dimension +': ' + rickshaw_dimensiones[x];		
			}
		} );		
		graph_ampliacion.render();

		$('#modal_ampliar').modal()
	}	
	
	//Si clickamos en el enlace de un gráfico evolutivo se amplia
	$('#rickshaw_sesiones').on( 'click', function () {
		Cargar_Graficos_Evolucion_Ampliacion ('sesiones');
	});	
	$('#rickshaw_paginasvistas').on( 'click', function () {
		Cargar_Graficos_Evolucion_Ampliacion ('paginasvistas');
	});
	$('#rickshaw_paginasvistassesion').on( 'click', function () {
		Cargar_Graficos_Evolucion_Ampliacion ('paginasvistassesion');
	});	
	$('#rickshaw_usuarios').on( 'click', function () {
		Cargar_Graficos_Evolucion_Ampliacion ('usuarios');
	});	
	$('#rickshaw_usuariosnuevos').on( 'click', function () {
		Cargar_Graficos_Evolucion_Ampliacion ('usuariosnuevos');
	});
	$('#rickshaw_usuariosrecurrentes').on( 'click', function () {
		Cargar_Graficos_Evolucion_Ampliacion ('usuariosrecurrentes');
	});	
	$('#rickshaw_tasarebote').on( 'click', function () {
		Cargar_Graficos_Evolucion_Ampliacion ('tasarebote');
	});
	$('#rickshaw_duracionmediasesion').on( 'click', function () {
		Cargar_Graficos_Evolucion_Ampliacion ('duracionsesion');
	});		
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}

	function DevolverContenido_Total_img(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			strAux = '<img src="../images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				strAux = '<img src="../images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			} else {
				strAux = '<img src="../images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}
			
	function Cargar_Datos_Extra() {
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		//alert($("#dat_idvista").val());
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strPerspectiva=' + $("#dat_perspectiva").val() + '&blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
			data: { strFuncion: 'metricas_habituales_graficos'}
		})
		.done(function (respuesta) {

			$(".sorting").append("<a class='ordenacion'></a>");

			//modifico los gráficos, medias, totales y destacados
			var objJson = jQuery.parseJSON(respuesta);
			
			//1. GRAFICOS EVOLUTIVOS
			//	 Variables con los datos para los gráficos evolutivos Sparkline
			//   Las guardamos para usar más adelante
			nombre_dimension = objJson.datos[0].nombre_dimension;				
			eval('rickshaw_dimensiones={' + objJson.datos[0].rickshaw_dimensiones +'}');	
			//Leemos las variables		
			if (blnComparacion==true) {numComparaciones=1} else {numComparaciones=0};
			for (I = 0; I <= numComparaciones; ++I) {
				rickshaw_sesiones[I] = '['+objJson.datos[I].rickshaw_sesiones+']';
				rickshaw_sesiones_legend[I] = '['+objJson.datos[I].rickshaw_sesiones_legend+']';
				rickshaw_paginasvistas[I] = '['+objJson.datos[I].rickshaw_paginasvistas+']';
				rickshaw_paginasvistas_legend[I] = '['+objJson.datos[I].rickshaw_paginasvistas_legend+']';				
				rickshaw_paginasvistassesion[I] = '['+objJson.datos[I].rickshaw_paginasvistassesion+']';
				rickshaw_paginasvistassesion_legend[I] = '['+objJson.datos[I].rickshaw_paginasvistassesion_legend+']';
				rickshaw_usuarios[I] = '['+objJson.datos[I].rickshaw_usuarios+']';
				rickshaw_usuarios_legend[I] = '['+objJson.datos[I].rickshaw_usuarios_legend+']';

				if( $("#dat_app").val() == 0 ){	
					rickshaw_tasarebote[I] = '['+objJson.datos[I].rickshaw_tasarebote+']';
					rickshaw_tasarebote_legend[I] = '['+objJson.datos[I].rickshaw_tasarebote_legend+']';
				}

				rickshaw_duracionmediasesion[I] = '['+objJson.datos[I].rickshaw_duracionmediasesion+']';
				rickshaw_duracionmediasesion_legend[I] = '['+objJson.datos[I].rickshaw_duracionmediasesion_legend+']';
				/***************************/
				rickshaw_usuarios_nuevos[I] = '['+objJson.datos[I].rickshaw_usuarios_nuevos+']';
				rickshaw_usuarios_recurrentes[I] = '['+objJson.datos[I].rickshaw_usuarios_recurrentes+']';		
				//rickshaw_usuarios_nuevos[I] = eval('['+objJson.datos[I].rickshaw_usuarios_nuevos+']');
				//rickshaw_usuarios_recurrentes[I] = eval('['+objJson.datos[I].rickshaw_usuarios_recurrentes+']');
				rickshaw_usuarios_nuevos_legend[I] = '['+objJson.datos[I].rickshaw_usuarios_nuevos_legend+']';
				//rickshaw_usuarios_nuevos_legend[I] = eval('['+objJson.datos[I].rickshaw_usuarios_nuevos_legend+']');
				rickshaw_usuarios_recurrentes_legend[I] = '['+objJson.datos[I].rickshaw_usuarios_recurrentes_legend+']';
				//rickshaw_usuarios_recurrentes_legend[I] = eval('['+objJson.datos[I].rickshaw_usuarios_recurrentes_legend+']');
				total_usuarios_nuevos = objJson.datos[0].total_usuariosnuevos; 
				total_usuarios_recurrentes = objJson.datos[0].total_usuariosrecurrentes;
			}
			
			Cargar_Graficos_Evolucion();
			
			//2. MEDIAS
			$('#media_sesiones').html(objJson.datos[0].media_sesionesF);
			$('#media_paginasvistas').text(objJson.datos[0].media_paginasvistasF);	
			$('#media_paginasvistassesion').text(objJson.datos[0].media_paginasvistassesionF);	
			$('#media_usuarios').text(objJson.datos[0].media_usuariosF);
			$('#media_usuariosnuevos').text(objJson.datos[0].media_usuariosnuevosF);
			$('#media_usuariosrecurrentes').text(objJson.datos[0].media_usuariosrecurrentesF);			
			$('#media_tasarebote').text(objJson.datos[0].media_tasareboteF);	
			$('#media_duracionmediasesion').text(objJson.datos[0].media_duracionmediasesionF);	

			//3. TOTALES		
			if (blnComparacion) {		
				$('#total_sesiones').html(DevolverContenido_Total_img(parseFloat(objJson.datos[0].total_sesiones).toFixed(2),parseFloat(objJson.datos[1].total_sesiones).toFixed(2),objJson.datos[0].total_sesionesF,objJson.datos[1].total_sesionesF,false));
				$('#total_sesiones_icono').html(DevolverContenido_Total(parseFloat(objJson.datos[0].total_sesiones).toFixed(2),parseFloat(objJson.datos[1].total_sesiones).toFixed(2),objJson.datos[0].total_sesionesF,objJson.datos[1].total_sesionesF,true));
				$('#total_paginasvistas').html(DevolverContenido_Total_img(parseFloat(objJson.datos[0].total_paginasvistas).toFixed(2),parseFloat(objJson.datos[1].total_paginasvistas).toFixed(2),objJson.datos[0].total_paginasvistasF,objJson.datos[1].total_paginasvistasF,false));
				$('#total_paginasvistassesion').html(DevolverContenido_Total_img(parseFloat(objJson.datos[0].total_paginasvistassesion).toFixed(2),parseFloat(objJson.datos[1].total_paginasvistassesion).toFixed(2),objJson.datos[0].total_paginasvistassesionF,objJson.datos[1].total_paginasvistassesionF,false));
				$('#total_paginasvistassesion_icono').html(DevolverContenido_Total(parseFloat(objJson.datos[0].total_paginasvistassesion).toFixed(2),parseFloat(objJson.datos[1].total_paginasvistassesion).toFixed(2),objJson.datos[0].total_paginasvistassesionF,objJson.datos[1].total_paginasvistassesionF,true));
				$('#total_usuarios').html(DevolverContenido_Total_img(parseFloat(objJson.datos[0].total_usuarios).toFixed(2),parseFloat(objJson.datos[1].total_usuarios).toFixed(2),objJson.datos[0].total_usuariosF,objJson.datos[1].total_usuariosF,false));
				$('#total_usuarios_icono').html(DevolverContenido_Total(parseFloat(objJson.datos[0].total_usuarios).toFixed(2),parseFloat(objJson.datos[1].total_usuarios).toFixed(2),objJson.datos[0].total_usuariosF,objJson.datos[1].total_usuariosF,true));
				$('#total_usuariosnuevos').html(DevolverContenido_Total_img(parseFloat(objJson.datos[0].total_usuariosnuevos).toFixed(2),parseFloat(objJson.datos[1].total_usuariosnuevos).toFixed(2),objJson.datos[0].total_usuariosnuevosF,objJson.datos[1].total_usuariosnuevosF,false));
				$('#total_usuariosrecurrentes').html(DevolverContenido_Total_img(parseFloat(objJson.datos[0].total_usuariosrecurrentes).toFixed(2),parseFloat(objJson.datos[1].total_usuariosrecurrentes).toFixed(2),objJson.datos[0].total_usuariosrecurrentesF,objJson.datos[1].total_usuariosrecurrentesF,false));
				$('#total_tasarebote').html(DevolverContenido_Total_img(parseFloat(objJson.datos[1].total_tasarebote).toFixed(2),parseFloat(objJson.datos[0].total_tasarebote).toFixed(2),objJson.datos[0].total_tasareboteF,objJson.datos[1].total_tasareboteF,false));
				//$('#total_tasarebote_icono').html(DevolverContenido_Total(objJson.datos[0].total_tasarebote,objJson.datos[1].total_tasarebote,objJson.datos[0].total_tasareboteF,objJson.datos[1].total_tasareboteF,true));
				$('#total_tasarebote_icono').html(DevolverContenido_Total(parseFloat(objJson.datos[1].total_tasarebote).toFixed(2),parseFloat(objJson.datos[0].total_tasarebote).toFixed(2),objJson.datos[0].total_tasareboteF,objJson.datos[1].total_tasareboteF,true));
				$('#total_duracionmediasesion').html(DevolverContenido_Total_img(parseFloat(objJson.datos[0].total_duracionmediasesion).toFixed(2),parseFloat(objJson.datos[1].total_duracionmediasesion).toFixed(2),objJson.datos[0].total_duracionmediasesionF,objJson.datos[1].total_duracionmediasesionF,false));
			} else {
				$('#total_sesiones_icono').html(''); 
				$('#total_sesiones').html(objJson.datos[0].total_sesionesF);
				$('#total_paginasvistas').html(objJson.datos[0].total_paginasvistasF);
				$('#total_paginasvistassesion_icono').html('');
				$('#total_paginasvistassesion').html(objJson.datos[0].total_paginasvistassesionF);
				$('#total_usuarios_icono').html('');
				$('#total_usuarios').html(objJson.datos[0].total_usuariosF);
				$('#total_usuariosnuevos').html(objJson.datos[0].total_usuariosnuevosF);
				
				$('#total_usuariosrecurrentes').html(objJson.datos[0].total_usuariosrecurrentesF);
				$('#total_tasarebote_icono').html('');
				$('#total_tasarebote').html(objJson.datos[0].total_tasareboteF);
				$('#total_duracionmediasesion').html(objJson.datos[0].total_duracionmediasesionF);	

			}	

			var pieData = [
				{
					value: parseInt(total_usuarios_nuevos),
					color:"#3fabe2",
					highlight: "#398dba",
					label: "Usuarios recurrentes"
				},
				{
					value: parseInt(total_usuarios_recurrentes),
					color: "#50b432",
					highlight: "#47962e",
					label: "Usuarios nuevos"
				}

			];

			//alert(total_usuarios_recurrentes);

		var ctx = document.getElementById("chart-area").getContext("2d");
		chart_sesiones = new Chart(ctx).Pie(pieData, {
			    showTooltips: false
			});

	


			//4. DESTACADOS. Tile Stats
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('.num'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval('objJson.datos[0].'+ $(el).find('.num').attr('id')),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', ''),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix);
								}
							});
							//Math.round
							tile_stats.destroy()
						});
					}
				}
			});
			
			//5. NOMBRE DIMENSIÓN (PERSPECTIVA)			
			$('#Nombre_Dimension').text(objJson.datos[0].nombre_dimension);		

		});
	}

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		Leer_Opcion_Comparacion();	
		Recargar_Datatable();
		Cargar_Datos_Extra();
		chart_sesiones.destroy();
	}

	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());
		//alert(datFechaInicioFiltro);
		/*var valComparacion
		if(comparacion!=1){*/
			//valComparacion = $("#select_Comparacion").val();
			valComparacion = $("#dat_comparador").val();
		/*}else{
			valComparacion = 2
		}*/
		//alert(valComparacion);
		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}

	$(document).ready(function() {
	
		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion();	
		//Opciones de comparación de datos -------------------------------------------

		if(daysBetween(datFechaInicioFiltro,datFechaFinFiltro)>365){
			$("#select_Comparacion").hide();
		}

		if( $("#dat_app").val() == 0 ){	
			appdata = true;
		}else{
			appdata = false;
		}
			//DATATABLE -----------------------------------------------------------------------------------------

		//DATATABLE -----------------------------------------------------------------------------------------
		t_datatable = $('#table_datatable').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[0,'asc']],
			"searching": false,
			//Exportaciones 
			//"dom": 'T<"clear">lfrtip',
			//"dom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",		
			"dom": 'rtT <filp "fondo">',
			//"tableTools": {"sSwfPath": "/public/assets/js/datatables/copy_csv_xls_pdf.swf"},
			"tableTools": {
		          "aButtons": [
		              {
	               "sExtends": "pdf",
	               "sPdfOrientation": "landscape",
	               //"sPdfMessage": "Your custom message would go here.",
	               "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
	           	},
	           	/*{
	               "sExtends": "csv",
	               "sPdfOrientation": "landscape",
	               //"sPdfMessage": "Your custom message would go here.",
	               "sButtonText": "CSV"
	           	},*/
	           	{
	               "sExtends": "xls",
	               "sPdfOrientation": "landscape",
	               //"sPdfMessage": "Your custom message would go here.",
	               "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
	           	},

		          ],
		          "sSwfPath": "../js/copy_csv_xls_pdf.swf"
		      	},
		       "columnDefs": [
		      {
		          "targets": [ 7 ],
		          "visible": appdata
		      }
		      ],
			"columns":[
				null,
				null,
				null,
				null,
				null,
				null,
				{"orderable": false }, //Usuarios recurrentes
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strPerspectiva=" + $("#dat_perspectiva").val() + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val() + "&strApp=" + $("#dat_app").val(),
				"data": {"strFuncion": 'metricas_habituales_datatable'}
			},				
		});	
		
		//DATATABLE ----------------------------------------------------------------------------------------- 		
		
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		Cargar_Datos_Extra();
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		
		//GRAFICOS EVOLUTIVOS SPARKLINE/RICKSHAW --------------------------------------------------------
		//Si cambia de tamaño alguna columna recalculamos los gráficos evolutivos para que se adapte al nuevo tamaño
		$('#table_datatable').on( 'column-sizing.dt', function () {
			//Cargar_Graficos_Evolucion();
		});
		//GRAFICOS EVOLUTIVOS SPARKLINE -----------------------------------------------------------------	
		
		$("#ToolTables_table_datatable_3").on("click",function(){
			alert("copiado");
		})
	});	
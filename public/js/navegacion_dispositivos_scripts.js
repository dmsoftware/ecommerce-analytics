	var sexo_rickshaw_duracionmediasesion = [];
	var sexo_rickshaw_duracionmediasesion_legend=[];
	var sexo_rickshaw_sesiones=[];
	var sexo_rickshaw_sesiones_legend=[];
	var sexo_rickshaw_paginasvistassesion=[];
	var sexo_rickshaw_paginasvistassesion_legend=[];

	var sexo_rickshaw_tasarebote=[];
	var sexo_rickshaw_tasarebote_legend=[];
	var sexo_rickshaw_dimensiones=[];
	var sexo_rickshaw_dimensiones_legend=[];

	var edad_rickshaw_duracionmediasesion = [];
	var edad_rickshaw_duracionmediasesion_legend=[];
	var edad_rickshaw_sesiones=[];
	var edad_rickshaw_sesiones_legend=[];
	var edad_rickshaw_paginasvistassesion=[];
	var edad_rickshaw_paginasvistassesion_legend=[];

	var edad_rickshaw_tasarebote=[];
	var edad_rickshaw_tasarebote_legend=[];
	var edad_rickshaw_dimensiones=[];
	var edad_rickshaw_dimensiones_legend=[];

	var edad_nombre_dimension = [];
	var sexo_nombre_dimension = [];


	var dispositivos_rickshaw_duracionmediasesion = [];
	var dispositivos_rickshaw_duracionmediasesion_legend=[];
	var dispositivos_rickshaw_sesiones=[];
	var dispositivos_rickshaw_sesiones_legend=[];
	var dispositivos_rickshaw_paginasvistassesion=[];
	var dispositivos_rickshaw_paginasvistassesion_legend=[];

	var dispositivos_rickshaw_tasarebote=[];
	var dispositivos_rickshaw_tasarebote_legend=[];
	var dispositivos_rickshaw_dimensiones=[];
	var dispositivos_rickshaw_dimensiones_legend=[];

	var dispositivos_nombre_dimension = [];

	var rickshaw_sesiones=[];
	var rickshaw_sesiones_legend=[];
	var nombre_dimension = [];

	var cab_sesion = [];
	var cab_nombre_sesion = [];
	var cab_porcentaje = [];
	var cab_sesiones = [];

	var table_datatable_dispositivos;
	var t_datatable_edades;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion();			
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	})//.trigger('change');		
	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();

		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	function Recargar_Datatable() {
		table_datatable_dispositivos.api().ajax.url('../informes/informes_ajax.php?strPerspectiva=' + $("#select_Dimensiones").val() + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
		table_datatable_navegadores.api().ajax.url('../informes/informes_ajax.php?strPerspectiva=' + $("#select_Dimensiones").val() + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
		cargar_mokup();
	}
	
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico,post){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + post + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + post + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + post + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}

	function Cargar_datos_dispositivos(){
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val(),
			data: { strFuncion: 'caracteristicas_graficos_dispositivos'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);	
			
			//alert(objJson.datos[0].data_mayor);
			

			if(blnComparacion==true){
				$("#total_dispositivos_ico").html(DevolverContenido_Total(objJson.datos[0].data_mayor,objJson.datos[1].data_mayor,objJson.datos[0].data_mayor_formateado,objJson.datos[1].data_mayor_formateado,true,'%'));
			}else{
				$("#total_dispositivos_ico").html("");
			}



			$("#txt_dispositivos_nombre").text(trans.__(objJson.datos[0].nombre_mayor));
			//4. DESTACADOS. Tile Stats
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('#tilestats_total_dispositivos'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval(objJson.datos[0].data_mayor),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix);
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

		});

	}



function formato_numero(numero, decimales, separador_decimal, separador_miles){     
	numero=parseFloat(numero);     
	if(isNaN(numero)){         
		return "";}     
	if(decimales!==undefined){        
	 	// Redondeamos         
		numero=numero.toFixed(decimales);     
	}     
	// Convertimos el punto en separador_decimal     
	numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
	if(separador_miles){         
		// Añadimos los separadores de miles         
		var miles=new RegExp("(-?[0-9]+)([0-9]{3})");         
		while(miles.test(numero)) {             
			numero=numero.replace(miles, "$1" + separador_miles + "$2");         
		}     
	}     
	return numero; 
}

function limpiarCamvas(idcanvas){
	var oCanvas = document.getElementById(idcanvas);
	var oContext = oCanvas.getContext("2d");
	oContext.clearRect(0, 0, oCanvas.width, oCanvas.height);
}

	function cargarGraficos(){
		graficoDispositivos();
		graficoNavegadores();
	}

	function graficoDispositivos(){

		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val(),
			data: { strFuncion: 'caracteristicas_graficos_bipolar_dispositivos'}
		})
		.done(function (respuesta) {

			limpiarCamvas("cvs_dispositivos");

			var objJson = jQuery.parseJSON(respuesta);	

			if(blnComparacion == 1){

				if(objJson.datos[1].ndata == 0){
					var ant = "[";
					for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
						if(i!=0){ ant +=",";}
						ant += "0";
					};
					ant += "]";
				}else{
					var ant = objJson.datos[1].col_dat;
				}
				
			}else{
				var ant = "[";
				for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
					if(i!=0){ ant +=",";}
					ant += "0";
				};
				ant += "]";
			}

			var noms;
			if(objJson.datos[1].ndata > objJson.datos[0].ndata){
				noms = objJson.datos[1].col_nom;
			}else{
				noms = objJson.datos[0].col_nom;
			}

			//alert(ant);
			var bipolar = new RGraph.Bipolar({
				id: 'cvs_dispositivos',
				left:  eval(ant),
				right: eval(objJson.datos[0].col_dat),
				options: {
				    shadow: {
				        color: '#bbb'
				    },
				    gutter: {
				        left: 15,
				        right: 15,
				        center: 0
				    },
				    text:{
				    	color: 'black',
				    	size: 8
				    },
				    axis:{
				    	color: 'rgba(179,179,179,0.5)'
				    },
				    labels: eval(noms),
					colors: [
				        'Gradient(#fa8011:#fa8011:#37bcf8:#37bcf8)'
				    ],	            
				    strokestyle: 'white',
				    linewidth: 1
				}
		    }).draw()

		});
	}	

	function graficoNavegadores(){

		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val(),
			data: { strFuncion: 'caracteristicas_graficos_bipolar_navegadores'}
		})
		.done(function (respuesta) {

			limpiarCamvas("cvs_navegadores");

			var objJson = jQuery.parseJSON(respuesta);	

			if(blnComparacion == 1){

				if(objJson.datos[1].ndata == 0){
					var ant = "[";
					for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
						if(i!=0){ ant +=",";}
						ant += "0";
					};
					ant += "]";
				}else{
					var ant = objJson.datos[1].col_dat;
				}
				
			}else{
				var ant = "[";
				for (var i = 0; i <= objJson.datos[0].ndata-1; i++) {
					if(i!=0){ ant +=",";}
					ant += "0";
				};
				ant += "]";
			}

			var noms;
			if(objJson.datos[1].ndata > objJson.datos[0].ndata){
				noms = objJson.datos[1].col_nom;
			}else{
				noms = objJson.datos[0].col_nom;
			}
			//alert(ant);
			var bipolar = new RGraph.Bipolar({
				id: 'cvs_navegadores',
				left:  eval(ant),
				right: eval(objJson.datos[0].col_dat),
				options: {
				    shadow: {
				        color: '#bbb'
				    },
				    gutter: {
				        left: 15,
				        right: 15,
				        center: 0
				    },
				    text:{
				    	color: 'black',
				    	size: 8
				    },
				    axis:{
				    	color: 'rgba(179,179,179,0.5)'
				    },
				    labels: eval(noms),
					colors: [
				        'Gradient(#fa8011:#fa8011:#37bcf8:#37bcf8)'
				    ],	            
				    strokestyle: 'white',
				    linewidth: 1
				}
		    }).draw()

		});
	}

	function Cargar_datos_navegadores(){
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?blnComparacion=' + blnComparacion + "&strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&strFiltro=" + $("#dat_filtro").val(),
			data: { strFuncion: 'caracteristicas_graficos_navegadores'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);

			//primero_nombre
			//primero_data
			//primero_data_f	
			if(blnComparacion==true){
				$("#total_navegadores_ico_primero").html(DevolverContenido_Total(objJson.datos[0].primero_data,objJson.datos[1].primero_data,objJson.datos[0].primero_data_f,objJson.datos[1].primero_data_f,true,'%'));
				$("#total_navegadores_ico_segundo").html(DevolverContenido_Total(objJson.datos[0].segundo_data,objJson.datos[1].segundo_data,objJson.datos[0].segundo_data_f,objJson.datos[1].segundo_data_f,true,'%'));
				$("#total_navegadores_ico_tercero").html(DevolverContenido_Total(objJson.datos[0].tercero_data,objJson.datos[1].tercero_data,objJson.datos[0].tercero_data_f,objJson.datos[1].tercero_data_f,true,'%'));
			}else{
				$("#total_navegadores_ico_primero").html("");
				$("#total_navegadores_ico_segundo").html("");
				$("#total_navegadores_ico_tercero").html("");
			}
			

			$("#txt_navegadores_nombre_primero").text(trans.__(objJson.datos[0].primero_nombre));
			$("#txt_navegadores_nombre_segundo").text(trans.__(objJson.datos[0].segundo_nombre));
			$("#txt_navegadores_nombre_tercero").text(trans.__(objJson.datos[0].tercero_nombre));

			//imgs
			$("#img_primero").html(objJson.datos[0].primero_img);
			$("#img_segundo").html(objJson.datos[0].segundo_img);
			$("#img_tercero").html(objJson.datos[0].tercero_img);

			//Primero
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('#tilestats_total_navegadores_primero'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval(objJson.datos[0].primero_data),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix+"%");
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

			//Segundo
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('#tilestats_total_navegadores_segundo'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval(objJson.datos[0].segundo_data),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix+"%");
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});

			//Tercero
			$(".tile-stats").each(function(i, el) {			
				var $this = $(el),
					$num = $this.find('#tilestats_total_navegadores_tercero'),
					start = attrDefault($num, 'start', 0),
					//end = attrDefault($num, 'end', 0),
					end = eval(objJson.datos[0].tercero_data),
					prefix = attrDefault($num, 'prefix', ''),
					postfix = attrDefault($num, 'postfix', '%'),
					duration = attrDefault($num, 'duration', 1000),
					delay = attrDefault($num, 'delay', 1000);
				if(start < end) {
					if(typeof scrollMonitor == 'undefined') {
						$num.html(prefix + end + postfix);
					} else {
						var tile_stats = scrollMonitor.create( el );
						tile_stats.fullyEnterViewport(function(){
							var o = {curr: start};
							TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function() {
									$num.html(prefix + o.curr.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.00','').replace(/,/g,'.') + postfix+"%");
								}
							});
							tile_stats.destroy()
						});
					}
				}
			});
		
			
						
		});
	}

	
			
	function Cargar_Datos_Extra(strtipo) {
		switch(strtipo){
			case "dispositivos":
				Cargar_datos_dispositivos();
				break;
			case "navegadores":
				Cargar_datos_navegadores();
				break;
		
		}
	}//funcion

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		Leer_Opcion_Comparacion();	
		Recargar_Datatable();
		Cargar_Datos_Extra("navegadores");
		Cargar_Datos_Extra("dispositivos");
		chart_dispositivos.destroy();
		cargar_mokup();
		cargarGraficos();
	}

	function Sesiones_Totales(){

		var ruta = "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val();
		//alert(ruta);
		var resp = 0;
		$.ajax({
		  type: 'POST',
		  url: ruta,
		  data: {
		    strFuncion: 'sesiones_totales'         
		    },
		  dataType: 'text',
		  success: function(data){
		  	//alert(parseInt(data.replace(" ","")));
		    resp = parseInt(data.replace(" ",""));
		    $("#dat_sestot").val(resp);
		    },
		  error: function(data){
		    //alert("Error: "+data);
		    resp = data;		  }
		})//fin ajax

		
		return resp;

	}


	$(document).ready(function() {

		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion(1);	
		//Opciones de comparación de datos -------------------------------------------



		//DATATABLE DE dispositivos -----------------------------------------------------------------------------------------
		table_datatable_dispositivos = $('#table_datatable_dispositivos').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			//"dom": 'Trt <filp "fondo">',
			"dom": 'rt',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false }
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_dispositivos'}
			}		
		});		
		//DATATABLE de dispositivos ----------------------------------------------------------------------------------------- 		
		

		//DATATABLE DE Intereses -----------------------------------------------------------------------------------------
		table_datatable_navegadores = $('#table_datatable_navegadores').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rt <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" +  blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_navegadores'}
			}		
		});		

		//DATATABLE EDADES ----------------------------------------------------------------------------------------- 		


		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------
		Cargar_Datos_Extra("dispositivos");
		Cargar_Datos_Extra("navegadores");
		//Cargar_Datos_Extra("interes");
		//TOTALES, MEDIAS, DESTACADOS  -----------------------------------------------------------------


		cargar_mokup();
		cargarGraficos();

});	


function cargar_mokup(){
	//alert("entra");
	$("#capaimgs_carga").html('<img id="imgescritorio"  src="../images/cargando_escritorio.png" /><br><img id="imgmovil" src="../images/cargando_movil.png" /><br>');
	$.ajax({
	  type: 'POST',
	  url: "/public/informes/informes_ajax.php?idVistaAnalytics=" + $("#dat_idvista").val(),
	  data: {
	    strFuncion: 'navegacion_mokup_imgs'       
	    },
	  dataType: 'text',
	  success: function(data){	
	  	//alert(data);
	  		$("#capaimgs_carga").html(data);
	  		$(".mini").fadeIn(1500)
		 	/*$("#tab_cabeceras").html(data).promise().done(function(){
				drawChart();
				Inicializar_datatables();
				cargar_datos_extra();
		    });*/
	    },
	  error: function(data){
	  	  //$("#tab_cabeceras").html(data)
	  }
	})//fin ajax

}

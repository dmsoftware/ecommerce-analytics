$(document).ready(function(){

	$("#combo_cliente").change(function(){						
		self.location = "../public/configuracion/permisos.php?cat=3&subcat=22&cliente="+$(this).val();
	});

	$(".fila").on("click",function(){
		var name  = $(this).parent().attr("name");

		var vista  = $(this).parent().attr("name");
		var email  = $(this).attr("name");
		var codigo = $(this).attr('data-codigo');
		//alert(#btn_eliminar_"+name);
		//alert($(this).css("background-color"));
		if($(this).parent().css("background-color") == "rgb(217, 237, 248)"){

			//Deselecciona
			$(".fila").parent().css("background-color", "transparent")
			$(this).parent().css("background-color", "transparent")
			$(".btnelimuser").hide();
			$(".btnedituser").hide();
			$("#btn_eliminar_"+name).hide();
			$("#btn_editar_"+name).hide();
			$("#btn_anadir_"+name).show();

			$("#txt_elimi_email").val("");
			$("#txt_elimi_vista").val("");
			$("#txt_elimi_codigo").val("");

		}else{

			$(".fila").parent().css("background-color", "transparent")
			$(this).parent().css("background-color", "rgb(217, 237, 248)")
			$(".btnelimuser").hide();
			$(".btnedituser").hide();
			$("#btn_eliminar_"+name).show();
			$("#btn_editar_"+name).show();
			$("#btn_anadir_"+name).hide();
			$("#capa_anadiruser_"+name).fadeOut(600);

			$("#txt_elimi_email").val(email);
			$("#txt_elimi_vista").val(vista);
			$("#txt_elimi_codigo").val(codigo);
		}
		
	});

//************************INFORMES****************************************//

	//Click en el boton de semanal
	$(".infor_sem_click").on("click",function(){

		var selector = $(this);
		//Primero conseguimos la vista y el email de donde pinchamos
		var actu = selector.attr("name");
		var vist = selector.parent().parent().attr("name");
		var user = selector.parent().parent().children(".filauser").attr("name");

		$.ajax({
        type: 'POST',
        url: '../configuracion/permisos_infosem_ajax.php',
        data: {
          modo:   "informe_semanal",
          actual: actu,
          vista:  vist,
          email:  user        
          },
        dataType: 'text',
        success: function(data){

        	if(actu == 1){
        		selector.attr("src",  "../images/infor-rojo.png");
        		selector.attr("name", "0");
        	}else{
        		selector.attr("src",  "../images/infor-verde.png");
        		selector.attr("name", "1");
        	}
          
          },
        error: function(data){
          
        }
      })
		
	})//btn

	//Click en el boton de mensual
	$(".infor_men_click").on("click",function(){

		var selector = $(this);
		//Primero conseguimos la vista y el email de donde pinchamos
		var actu = selector.attr("name");
		var vist = selector.parent().parent().attr("name");
		var user = selector.parent().parent().children(".filauser").attr("name");

		$.ajax({
        type: 'POST',
        url: '../configuracion/permisos_infosem_ajax.php',
        data: {
          modo:   "informe_mensual",
          actual: actu,
          vista:  vist,
          email:  user        
          },
        dataType: 'text',
        success: function(data){

        	if(actu == 1){
        		selector.attr("src",  "../images/infor-rojo.png");
        		selector.attr("name", "0");
        	}else{
        		selector.attr("src",  "../images/infor-verde.png");
        		selector.attr("name", "1");
        	}
          
          },
        error: function(data){
          
        }
      })
		
	})//btn

	$(".infor_tri_click").on("click",function(){

		var selector = $(this);
		//Primero conseguimos la vista y el email de donde pinchamos
		var actu = selector.attr("name");
		var vist = selector.parent().parent().attr("name");
		var user = selector.parent().parent().children(".filauser").attr("name");

		$.ajax({
        type: 'POST',
        url: '../configuracion/permisos_infosem_ajax.php',
        data: {
          modo:   "informe_trimestral",
          actual: actu,
          vista:  vist,
          email:  user        
          },
        dataType: 'text',
        success: function(data){

        	if(actu == 1){
        		selector.attr("src",  "../images/infor-rojo.png");
        		selector.attr("name", "0");
        	}else{
        		selector.attr("src",  "../images/infor-verde.png");
        		selector.attr("name", "1");
        	}
          
          },
        error: function(data){
          
        }
      })
		
	})//btn

//************************INFORMES****************************************//

//************************CAMPANAS****************************************//

	//Click en el boton de semanal
	$(".camp_sem_click").on("click",function(){

		var selector = $(this);
		//Primero conseguimos la vista y el email de donde pinchamos
		var actu = selector.attr("name");
		var vist = selector.parent().parent().attr("name");
		var user = selector.parent().parent().children(".filauser").attr("name");

		$.ajax({
        type: 'POST',
        url: '../configuracion/permisos_infosem_ajax.php',
        data: {
          modo:   "campana_semanal",
          actual: actu,
          vista:  vist,
          email:  user        
          },
        dataType: 'text',
        success: function(data){

        	if(actu == 1){
        		selector.attr("src",  "../images/infor-rojo.png");
        		selector.attr("name", "0");
        	}else{
        		selector.attr("src",  "../images/infor-verde.png");
        		selector.attr("name", "1");
        	}
          
          },
        error: function(data){
          
        }
      })
		
	})//btn

	//Click en el boton de mensual
	$(".camp_men_click").on("click",function(){

		var selector = $(this);
		//Primero conseguimos la vista y el email de donde pinchamos
		var actu = selector.attr("name");
		var vist = selector.parent().parent().attr("name");
		var user = selector.parent().parent().children(".filauser").attr("name");

		$.ajax({
        type: 'POST',
        url: '../configuracion/permisos_infosem_ajax.php',
        data: {
          modo:   "campana_mensual",
          actual: actu,
          vista:  vist,
          email:  user        
          },
        dataType: 'text',
        success: function(data){

        	if(actu == 1){
        		selector.attr("src",  "../images/infor-rojo.png");
        		selector.attr("name", "0");
        	}else{
        		selector.attr("src",  "../images/infor-verde.png");
        		selector.attr("name", "1");
        	}
          
          },
        error: function(data){
          
        }
      })
		
	})//btn

	$(".camp_tri_click").on("click",function(){

		var selector = $(this);
		//Primero conseguimos la vista y el email de donde pinchamos
		var actu = selector.attr("name");
		var vist = selector.parent().parent().attr("name");
		var user = selector.parent().parent().children(".filauser").attr("name");

		$.ajax({
        type: 'POST',
        url: '../configuracion/permisos_infosem_ajax.php',
        data: {
          modo:   "campana_trimestral",
          actual: actu,
          vista:  vist,
          email:  user        
          },
        dataType: 'text',
        success: function(data){

        	if(actu == 1){
        		selector.attr("src",  "../images/infor-rojo.png");
        		selector.attr("name", "0");
        	}else{
        		selector.attr("src",  "../images/infor-verde.png");
        		selector.attr("name", "1");
        	}
          
          },
        error: function(data){
          
        }
      })
		
	})//btn

//************************CAMAPANAS****************************************//


	//Click en el boton de añadir usuario pri
	$(".btnanadiruser").on("click",function(){
		
		var vista = $(this).attr("name");
		
		$(this).hide();
		$("#capa_anadiruser_"+vista).fadeIn(600,function(){
			
		})

		//$('#myModal').modal('show')

	})//btn




	//Click en el boton de cancelar añadir usuario
	$(".btncancel").on("click",function(){
		
		var vista = $(this).attr("name");

		$(this).parent().parent().hide();
		$("#btn_anadir_"+vista).fadeIn(600,function(){
			
		})

	})//btn

	//Click en el combo de remomendación de emails
	$(".select_recomendados").on("change",function(){
		
		var vista = $(this).attr("name");

		$("#txt_user_"+vista).val( $(this).val() )
		

	})//btn

	//Click en el boton añadir usuario definitivo
	$(".btn_defi_anauser").on("click",function(){
		
		//Necesitamos vista y usuario
		var vista = $(this).attr("name");
		var user = $("#txt_user_"+vista).val();

		var error = "";
		//Comprobamos que el email introducido no esté en blanco
		if(user == ""){
			error += "Debe rellenar un email para añadir un usuario."
		}else{

			if( validarEmail(user) == false ){
				error += "El email introducido no es válido."
			}

		}
		//Comprobamos que el email introducido sea correcto

		if (error!=""){
			//Hay error por lo que se muestra el error
			$("#p_error_"+vista).stop();
			$("#p_error_"+vista).text(error);
			$("#p_error_"+vista).fadeIn(600,function(){
				$("#p_error_"+vista).delay(3000);
				$("#p_error_"+vista).fadeOut(600);
			})
		}else{

			//Todo ok para la inserción del usuario
			$.ajax({
		        type: 'POST',
		        url: '../configuracion/permisos_infosem_ajax.php',
		        data: {
		          modo:   "anadir_usuario",
		          vista:  vista,
		          email:  user
		          },
		        dataType: 'text',
		        success: function(data){
		        	self.location = "../public/configuracion/permisos.php?cat=3&subcat=22&cliente="+$("#client_id").val()+"&codigo="+$.trim(data);
		          
		          },
		        error: function(data){
		          
		        }
		     })

		}
		

	})//btn

	$(".btnelimuser").on("click",function(){

		//Recogemos variables vista y usuario
		var vista = $("#txt_elimi_vista").val();
		var email = $("#txt_elimi_email").val();

		//Eliminos el permiso de la vista
		var r = confirm("¿Estás seguro de eliminar los permisos a "+email+"?");
		if (r == true) {
		  
		  eliminarUser(vista,email);

		} 

	})

	$(".btnedituser").on("click",function(){

		//Recogemos variables vista y usuario
		var codigo = $("#txt_elimi_codigo").val();
		var cliente = $("#txt_edit_cliente").val();

		//Lanzar modal para editar usuario
		editarUser(codigo,cliente);
	})

	

	$(".btn_anadirfper").on("click",function(){

		var selector = $(this);
		var cod = selector.attr("name");
		var vista = selector.parent().children("#loc_"+cod).attr("name");
		var user = selector.parent().attr("name");
		//alert(user);
		var cab = 0;
		if( selector.parent().children(".loc_fpers").html() == "" ){
			selector.parent().children(".loc_fpers").html('<ul class="filafiltro filtrotit"><li class="cajat"><span>Descripción</span></li><li class="cajat"><span>Técnico</span></li><li class="cajab"></li><li class="cajab"></li></ul>')
			cab=1;
		}

		selector.parent().children(".loc_fpers").html(selector.parent().children(".loc_fpers").html() + '<ul class="filafiltro"><li class="cajat"><input id="txt_filtrodesc_'+cod+'" class="inptex" type="text" value=""</li><li class="cajat"><input id="txt_filtro_'+cod+'" class="inptex" type="text" value=""></li><li class="cajab"><button type="button" class="btn btn-primary" onClick="anadirFiltro('+vista+',\''+user+'\','+cod+')" ><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button></li><li class="cajab"><button type="button" class="btn btn-primary eliminarfilapers" onClick="eliminarfilapers('+cab+','+cod+')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></li></ul>');
		selector.hide();
	})


	$(".eliminarfilapers").on("click",function(){

		alert("entra");
		//$(this).parent().parent().hide();

	})

	var codigo_usuario_nuevo = $('#usuario_nuevo').val();
	if(codigo_usuario_nuevo){
		setTimeout(function() {
		var cliente = $("#txt_edit_cliente").val();
	    editarUser(codigo_usuario_nuevo,cliente);
	  }, 0)
		
	}	

})//ready

function eliminarfilapers(cab,cod){
	//alert(vista)
	if(cab == 1){
		//si cab es uno significa que solo queda la cabecera por lo que eliminamos todo
		$("#loc_"+cod).html("");
		$("#btn"+cod).fadeIn(600);
	}else{
		$("#loc_"+cod).children("ul:last-child").hide();
		$("#btn"+cod).fadeIn(600);
	}
}


function eliminarUser(vista,user){

	$.ajax({
	    type: 'POST',
	    url: '../configuracion/permisos_infosem_ajax.php',
	    data: {
	      modo:   "eliminar_usuario",
	      vista:  vista,
	      email:  user
	      },
	    dataType: 'text',
	    success: function(data){

	    	self.location = "../public/configuracion/permisos.php?cat=3&subcat=22&cliente="+$("#client_id").val();
	      
	      },
	    error: function(data){
	      
	    }
	 })

}

function editarUser(codigo,cliente){
	var params = { 'id' : codigo, 'cliente' : cliente,  };
	var url = 'http://dmintegra-admin.acc.com.es/externalAccess?route=locusuario_edit&params=' + encodeURIComponent(JSON.stringify(params));

	lanzarModaliFrame(url);
}


function validarEmail( email ) {

    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if ( !expr.test(email) ){

    	return false;

    }else{

    	return true;

    }
        
}


function anadirFiltro(vista,user,cod){

	//alert($("#txt_filtro_"+cod).val() + "  -----   " + $("#txt_filtrodesc_"+cod).val() );
	var filtro = $("#txt_filtro_"+cod).val();
	var filtrodesc = $("#txt_filtrodesc_"+cod).val();

	if (filtro == "" || filtrodesc == "") {

		alert("El filtro no puede quedar vacio.");

	}else{

		$.ajax({
		    type: 'POST',
		    url: '../configuracion/permisos_infosem_ajax.php',
		    data: {
		      modo:   "anadir_filtro",
		      vista:  vista,
		      email:  user,
		      filtro:  filtro,
		      filtrodesc:  filtrodesc
		      },
		    dataType: 'text',
		    success: function(data){

		    	self.location = "../public/configuracion/permisos.php?cat=3&subcat=22&cliente="+$("#client_id").val();
		      
		      },
		    error: function(data){
		      
		    }
		 })

	}

}//f

function EliminarFiltro(id){

	var r = confirm("¿Estás seguro de eliminar este filtro?");
	if (r == true) {
	  
		$.ajax({
		    type: 'POST',
		    url: '../configuracion/permisos_infosem_ajax.php',
		    data: {
		      modo:   "eliminar_filtro",
		      idf:  id
		      },
		    dataType: 'text',
		    success: function(data){

		    	self.location = "../public/configuracion/permisos.php?cat=3&subcat=22&cliente="+$("#client_id").val();
		      
		      },
		    error: function(data){
		      
		    }
		 })

	} 

}//f

function GuardarFiltro(id){

	var filtrodesc = $("#txt_filtrodesc_save_"+id).val();
	var filtro = $("#txt_filtro_save_"+id).val();
	
	if (filtro == "" || filtrodesc == "") {

		alert("El filtro no puede quedar vacio.");

	}else{
	  
		$.ajax({
		    type: 'POST',
		    url: '../configuracion/permisos_infosem_ajax.php',
		    data: {
		      modo:   "guardar_filtro",
		      idf:  id,
		      filtrof:  filtro,
		      filtrodescf:  filtrodesc
		      },
		    dataType: 'text',
		    success: function(data){

		    	self.location = "../public/configuracion/permisos.php?cat=3&subcat=22&cliente="+$("#client_id").val();
		      
		      },
		    error: function(data){
		      
		    }
		 })

	}

}//f
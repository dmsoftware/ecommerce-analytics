//Variables globales
var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;



function drawRegionsMap() {

	//Llamamos al ajax para traer el json del char
	//alert($("#pais_comparar").val());
	$.ajax({
		type: 'POST',
		url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=todos&strIdioma=todos&strFiltro=" + $("#dat_filtro").val(),
		data: { strFuncion: 'contenido_chart_pais'}
	})
	.done(function (respuesta) {

		var data = google.visualization.arrayToDataTable(eval(respuesta));
		var options = {};
		var chart = new google.visualization.GeoChart(document.getElementById('pais_div'));
		chart.draw(data, options);
		
				
	})

}
function drawChart(straux) {

	$.ajax({
		type: 'POST',
		url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strFiltro=" + $("#dat_filtro").val(),
		data: { strFuncion: 'resumen_piechart_'+straux}
	})
	.done(function (respuesta) {

		var objJson = jQuery.parseJSON(respuesta);
		//alert(objJson.colores)

		var data = google.visualization.arrayToDataTable(eval(objJson.llamada));
		//var options = eval(objJson.colores);
		var options = {
	       	title: '',
	        is3D: true,
	        legend: 'none',
	        slices: //objJson.colores
	        {
	            0: { color: '#2e63cb' },
	            1: { color: '#dc3812' }
          	},
          	heigth: 240,
          	width:  240

	    };
	    var chart = new google.visualization.PieChart(document.getElementById(straux+'_div'));
	    chart.draw(data, options);
    })//done
 }


function trim(cadena){
    cadena=cadena.replace(/^\s+/,'').replace(/\s+$/,'');
    return(cadena);
} 

/*Funciones para la fecha*/
function DateToString (datDate) {
	return datDate.toISOString().slice(0,10);
}

function daysBetween(startDate, endDate) {
	var millisecondsPerDay = 24 * 60 * 60 * 1000;
	return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
}

function treatAsUTC(date) {
	var result = new Date(date);
	result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
	return result;
}

function Comparacion_rango_fechas(){
	
	datFechaInicioFiltro = new Date($("#dat_fechaini").val());
	datFechaFinFiltro = new Date($("#dat_fechafin").val());

	datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
	datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
	intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
	datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
	datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
	strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
	strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);

	$("#dat_fechaini_ant").val(strFechaInicioFiltroAnt);	
	$("#dat_fechafin_ant").val(strFechaFinFiltroAnt);			

}
/*Funciones para la fecha*/


function cargar_cabeceras_ini(){
	Comparacion_rango_fechas();
	//Inicializar_datatables();
	drawRegionsMap();
	drawChart("sexo");
	drawChart("edad");
	drawChart("trafico_sem");
	drawChart("trafico_anno");

}



google.load("visualization", "1", {
	packages:["geochart"],
	packages:["corechart"]
});
google.setOnLoadCallback(cargar_cabeceras_ini);


	var nombre_dimension = [];

	var cab_sesion = [];
	var cab_nombre_sesion = [];
	var cab_porcentaje = [];
	var cab_sesiones = [];

	var table_datatable_enlaces_dominiostrafico;
	var table_datatable_control;
	var table_datatable_enlaces_enlacestrafico;
	var table_datatable_enlaces_dominiosconversiones;
	var table_datatable_enlaces_enlacesconversiones;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	var data_column;
	var data_column_ant;
	var data_column_original;
	var data_column_original_ant;

	var chart_column;
	var chart_column_ant;

	var chart_colum_origin_ant;
	var chart_colum_origin;
	var array_colums_off = [];

	var data_columnchart;
	var data_columnchart_ant;

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion();			
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	})//.trigger('change');		
	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();

		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '</div>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="gris comparador"> ' + ValorAntF + '</div>';
			} else {
				// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '</div>';
			}
		}
		if (blnSoloGrafico) {
			return strAux
		} else {
			return strAux + ' ' + ValorF;
		}
	}
	
	function DevolverContenido_Total_img(Valor,ValorAnt,ValorF,ValorAntF){

		if (parseFloat(Valor) > parseFloat(ValorAnt)) {
			strAux = '<img src="../images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
		} else {
			if (parseFloat(Valor) == parseFloat(ValorAnt)) {
				strAux = '<img src="../images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			} else {
				strAux = '<img src="../images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
			}
		}

		return strAux;

	}
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}

			

	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX

	function trim(cadena){
	       cadena=cadena.replace(/^\s+/,'').replace(/\s+$/,'');
	       return(cadena);
	} 

	function cargarCabeceras(modo){
		if(modo == 1){

			$.ajax({
				type: 'POST',
				url: 'seo_ajax.php',
				data: {
				  modo: 1,
				  proyectosasoc: $("#dat_proyectoasociado").val(),
				  vista: 		 $("#dat_idvista").val(),
				  fechaini:      $("#dat_fechaini").val(),
				  fechafin:      $("#dat_fechafin").val(),
				  filtro_pais:   $("#pais_comparar").val(),
				  filtro_idioma: $("#idioma_comparar").val()
				  },
				dataType: 'text',
				success: function(data){
				  $("#total_contenido_idioma").html(data);
				},
				error: function(){

				}				  
			})//fin ajax				

		}else if(modo == 2){

			$.ajax({
				type: 'POST',
				url: 'seo_ajax.php',
				data: {
				  modo: 2,
				  proyectosasoc: $("#dat_proyectoasociado").val(),
				  vista: 		 $("#dat_idvista").val(),
				  fechaini:      $("#dat_fechaini").val(),
				  fechafin:      $("#dat_fechafin").val(),
				  filtro_pais:   $("#pais_comparar").val(),
				  filtro_idioma: $("#idioma_comparar").val()
				  },
				dataType: 'text',
				success: function(data){
				  $("#total_contenido_info").html(data);
				  lineChart_seo()
				},
				error: function(){

				}				  
			})//fin ajax
		}

	}



	google.load("visualization", "1.1", {
			packages:["geochart"],
			packages:["corechart"]
		});
	google.load("visualization", "1.1", {packages:["bar"]});

	google.setOnLoadCallback(cargar_cabeceras_ini);

	function cargar_cabeceras_ini(){
		Leer_Opcion_Comparacion();
		if(getPaginaSeoActual() === 0){
			cargarCabeceras(1);
			cargarCabeceras(2);
		}
		if(getPaginaSeoActual() === 2){
			cargar_enlaces();
	  }
	  if(getPaginaSeoActual() === 1 || getPaginaSeoActual() === 2){
			cargar_extras();
		}
	}

	function cargar_enlaces(){

		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val(),
			data: { strFuncion: 'seo_enlaces_externos'}
		})
		.done(function (respuesta) {
			var objJson = jQuery.parseJSON(respuesta);

			var numdominios = objJson.total_enlaces_dominios;
			var numpaginas = objJson.total_enlaces_paginas;

			$("#total_dominios_enlaces").html(numdominios);
			$("#total_paginas_enlaces").html(numpaginas);
		});

	}

function cargarCatSeo(){
	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 9,
		  url_vista: $("#dat_vistaurl").val(),
		  tipo: $("#dat_seo_t").val()
		  },
		dataType: 'text',
		success: function(respuesta){
		  
			$(".combocatseo").html(respuesta)

		},
		error: function(){

		}				  
	})//fin ajax
}

function cargador(){
	cargar_cabeceras();
}

function cargador_cambiomes(){
	cargar_extras_Seo();
	table_datatable_control.api().ajax.url("../informes/informes_ajax.php?idVistaAnalytics=" + $("#dat_idvista").val() + "&vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val() + "&pag=" + $("#dat_pag").val() + "&tipo=" + $("#dat_seo_t").val() ).load();
	//table_datatable_pagsentradal.api().ajax.url("../informes/informes_ajax.php?vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val()).load();

}

function cargar_cabeceras(){
	Leer_Opcion_Comparacion();
	cargarCabeceras(1);
	cargarCabeceras(2);
	cargar_enlaces();
	cargarCatSeo()
}

function cargar_extras_b(){
	$("#contenedor_combomeses").load("seo_ajax_combomeses.php?urlVista="+$("#dat_vistaurl").val() + "&tipo=" + $("#dat_seo_t").val(),function(){

		//Despues de cargar el combo para las fechas creamos los extras
		Recargar_Datatables();
		cargar_extras_Seo();

	});
}

function cargar_extras(){
	$("#contenedor_combomeses").load("seo_ajax_combomeses.php?urlVista="+$("#dat_vistaurl").val() + "&tipo=" + $("#dat_seo_t").val(),function(){

		//Despues de cargar el combo para las fechas creamos los extras
		Inicializar_datatables();
		if(getPaginaSeoActual() === 1){
		  cargar_extras_Seo();
		}

	});
}

function Recargar_Datatables() {
	table_datatable_enlaces_dominiostrafico.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val()).load();
	table_datatable_enlaces_enlacestrafico.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val()).load();
	table_datatable_enlaces_dominiosconversiones.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val()).load();
	table_datatable_enlaces_enlacesconversiones.api().ajax.url("../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val()).load();
	table_datatable_control.api().ajax.url("../informes/informes_ajax.php?idVistaAnalytics=" + $("#dat_idvista").val() + "&vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val() + "&pag=" + $("#dat_pag").val() + "&tipo=" + $("#dat_seo_t").val() ).load();
	//table_datatable_pagsentradal.api().ajax.url("../informes/informes_ajax.php?vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val()).load();
}

function cargar_extras_Seo(){
	cargarCatSeo()
	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 3,
		  proyectosasoc: $("#dat_proyectoasociado").val(),
		  vista: 		 $("#dat_idvista").val(),
		  fechaini:      $("#dat_fechaini").val(),
		  fechafin:      $("#dat_fechafin").val(),
		  filtro_pais:   $("#pais_comparar").val(),
		  filtro_idioma: $("#idioma_comparar").val(),
		  url_vista:   	 $("#dat_vistaurl").val(),
		  mes_combo: 	 $("#dat_seo_meses").val(),
		  control:       $("#data_terminoscontrol").val(),
		  tipo:  		 $("#dat_seo_t").val()
		  },
		dataType: 'text',
		success: function(respuesta){
		  	
			var objJson = jQuery.parseJSON(respuesta);
			if(objJson.datos[0].comparacion == 1){
				//Comparamos en alguna etiqueta sino no
				$("#seo_terminos_w_ant").html(compararAnt(objJson.datos[1].seo_terminos_w,objJson.datos[0].seo_terminos_w,objJson.datos[1].seo_terminos_w_f,''))
				$("#seo_terminos_m_ant").html(compararAnt(objJson.datos[1].seo_terminos_m,objJson.datos[0].seo_terminos_m,objJson.datos[1].seo_terminos_m_f,''))
				$("#seo_pagsaterrizaje_ant").html(compararAnt(objJson.datos[1].seo_pagsaterrizaje,objJson.datos[0].seo_pagsaterrizaje,objJson.datos[1].seo_pagsaterrizaje_f,''))
				$("#seo_impresiones_w_ant").html(compararAnt(objJson.datos[1].seo_impresiones_w,objJson.datos[0].seo_impresiones_w,objJson.datos[1].seo_impresiones_w_f,''))
				$("#seo_impresiones_m_ant").html(compararAnt(objJson.datos[1].seo_impresiones_m,objJson.datos[0].seo_impresiones_m,objJson.datos[1].seo_impresiones_m_f,''))
				$("#seo_clicks_w_ant").html(compararAnt(objJson.datos[1].seo_clicks_w,objJson.datos[0].seo_clicks_w,objJson.datos[1].seo_clicks_w_f,''))
				$("#seo_clicks_m_ant").html(compararAnt(objJson.datos[1].seo_clicks_m,objJson.datos[0].seo_clicks_m,objJson.datos[1].seo_clicks_m_f,''))
				$("#seo_ctrmedio_w_ant").html(compararAnt(objJson.datos[1].seo_ctrmedio_w,objJson.datos[0].seo_ctrmedio_w,objJson.datos[1].seo_ctrmedio_w_f,'%'))
				$("#seo_ctrmedio_m_ant").html(compararAnt(objJson.datos[1].seo_ctrmedio_m,objJson.datos[0].seo_ctrmedio_m,objJson.datos[1].seo_ctrmedio_m_f,'%'))
				$("#seo_posmedia_w_ant").html(compararAnt(objJson.datos[1].seo_posmedia_w,objJson.datos[0].seo_posmedia_w,objJson.datos[1].seo_posmedia_w_f,''))
				$("#seo_posmedia_m_ant").html(compararAnt(objJson.datos[1].seo_posmedia_m,objJson.datos[0].seo_posmedia_m,objJson.datos[1].seo_posmedia_m_f,''))
				$("#seo_terminos_ant").html(compararAnt(objJson.datos[1].seo_terminos,objJson.datos[0].seo_terminos,objJson.datos[1].seo_terminos_f,''))

				$("#seo_tpag1_w_ant").html(compararAnt(objJson.datos[1].seo_pag1_w,objJson.datos[0].seo_pag1_w,objJson.datos[1].seo_pag1_w_f,''));
				$("#seo_tpag1_m_ant").html(compararAnt(objJson.datos[1].seo_pag1_m,objJson.datos[0].seo_pag1_m,objJson.datos[1].seo_pag1_m_f,''));
				$("#seo_tpag2_w_ant").html(compararAnt(objJson.datos[1].seo_pag2_w,objJson.datos[0].seo_pag2_w,objJson.datos[1].seo_pag2_w_f,''));
				$("#seo_tpag2_m_ant").html(compararAnt(objJson.datos[1].seo_pag2_m,objJson.datos[0].seo_pag2_m,objJson.datos[1].seo_pag2_m_f,''));
				$("#seo_tpag3_w_ant").html(compararAnt(objJson.datos[1].seo_pag3_w,objJson.datos[0].seo_pag3_w,objJson.datos[1].seo_pag3_w_f,''));
				//$("#seo_tpag3_w_ant").html(objJson.datos[1].seo_pag3_w+' - '+objJson.datos[0].seo_pag3_w);
				$("#seo_tpag3_m_ant").html(compararAnt(objJson.datos[1].seo_pag3_m,objJson.datos[0].seo_pag3_m,objJson.datos[1].seo_pag3_m_f,''));
				$("#seo_tpag4_w_ant").html(compararAnt(objJson.datos[1].seo_pag4_w,objJson.datos[0].seo_pag4_w,objJson.datos[1].seo_pag4_w_f,''));
				$("#seo_tpag4_m_ant").html(compararAnt(objJson.datos[1].seo_pag4_m,objJson.datos[0].seo_pag4_m,objJson.datos[1].seo_pag4_m_f,''));
			}else{
				//Comparamos en alguna etiqueta sino no
				$("#seo_terminos_w_ant").html("");
				$("#seo_terminos_m_ant").html("");
				$("#seo_pagsaterrizaje_ant").html("");
				$("#seo_impresiones_w_ant").html("");
				$("#seo_impresiones_m_ant").html("");
				$("#seo_clicks_w_ant").html("");
				$("#seo_clicks_m_ant").html("");
				$("#seo_ctrmedio_w_ant").html("");
				$("#seo_ctrmedio_m_ant").html("");
				$("#seo_posmedia_w_ant").html("");
				$("#seo_posmedia_m_ant").html("");
				$("#seo_terminos_ant").html("");

				$("#seo_tpag1_w_ant").html("");
				$("#seo_tpag1_m_ant").html("");
				$("#seo_tpag2_w_ant").html("");
				$("#seo_tpag2_m_ant").html("");
				$("#seo_tpag3_w_ant").html("");
				$("#seo_tpag3_m_ant").html("");
				$("#seo_tpag4_w_ant").html("");
				$("#seo_tpag4_m_ant").html("");
			}
			$("#seo_terminos_w").html(objJson.datos[0].seo_terminos_w_f);
			$("#seo_terminos_m").html(objJson.datos[0].seo_terminos_m_f);
			$("#seo_pagsaterrizaje").html(objJson.datos[0].seo_pagsaterrizaje_f);
			$("#seo_impresiones_w").html(objJson.datos[0].seo_impresiones_w_f);
			$("#seo_impresiones_m").html(objJson.datos[0].seo_impresiones_m_f);
			$("#seo_clicks_w").html(objJson.datos[0].seo_clicks_w_f);
			$("#seo_clicks_m").html(objJson.datos[0].seo_clicks_m_f);
			$("#seo_ctrmedio_w").html(objJson.datos[0].seo_ctrmedio_w_f+"%");
			$("#seo_ctrmedio_m").html(objJson.datos[0].seo_ctrmedio_m_f+"%");
			$("#seo_posmedia_w").html(objJson.datos[0].seo_posmedia_w_f);
			$("#seo_posmedia_m").html(objJson.datos[0].seo_posmedia_m_f);

			$("#seo_tpag1_w").html("<span class='tpags'>" + objJson.datos[0].seo_pag1_w_f + "</span><span class='rateseo'>("+objJson.datos[0].seo_pag1_w_rate_f+")</span>");
			$("#seo_tpag1_m").html("<span class='tpags'>" + objJson.datos[0].seo_pag1_m_f + "</span><span class='rateseo'>("+objJson.datos[0].seo_pag1_m_rate_f+")</span>");
			$("#seo_tpag2_w").html("<span class='tpags'>" + objJson.datos[0].seo_pag2_w_f + "</span><span class='rateseo'>("+objJson.datos[0].seo_pag2_w_rate_f+")</span>");
			$("#seo_tpag2_m").html("<span class='tpags'>" + objJson.datos[0].seo_pag2_m_f + "</span><span class='rateseo'>("+objJson.datos[0].seo_pag2_m_rate_f+")</span>");
			$("#seo_tpag3_w").html("<span class='tpags'>" + objJson.datos[0].seo_pag3_w_f + "</span><span class='rateseo'>("+objJson.datos[0].seo_pag3_w_rate_f+")</span>");
			$("#seo_tpag3_m").html("<span class='tpags'>" + objJson.datos[0].seo_pag3_m_f + "</span><span class='rateseo'>("+objJson.datos[0].seo_pag3_m_rate_f+")</span>");
			$("#seo_tpag4_w").html("<span class='tpags'>" + objJson.datos[0].seo_pag4_w_f + "</span><span class='rateseo'>("+objJson.datos[0].seo_pag4_w_rate_f+")</span>");
			$("#seo_tpag4_m").html("<span class='tpags'>" + objJson.datos[0].seo_pag4_m_f + "</span><span class='rateseo'>("+objJson.datos[0].seo_pag4_m_rate_f+")</span>");


			//Titulos
			if($("#dat_seo_t").val() == "term"){
				$("#tittotal").html(trans.__("Términos de búsqueda"));
				$("#pagtot1").html(trans.__("Términos en página 1"));
				$("#pagtot2").html(trans.__("Términos en página 2"));
				$("#pagtot3").html(trans.__("Términos en página 3"));
				$("#pagtot4").html(trans.__("Términos en página 4+"));
				$("#tablecabtit").html(trans.__("Término de búsqueda"));
				$("#solotercontrol").html(trans.__("Sólo términos de control"));
				$("#sinperm").html(trans.__("*No tiene permisos para editar los términos de control."));
				$("#terctr").html(trans.__("Término de control"));
				$("#agragarter").html(trans.__("Agregar término"));
				$("#ntermpro").html(trans.__("Nuevo término de control"));
			}else{
				$("#tittotal").html(trans.__("Páginas de aterrizaje"));
				$("#pagtot1").html(trans.__("URLS en página 1"));
				$("#pagtot2").html(trans.__("URLS en página 2"));
				$("#pagtot3").html(trans.__("URLS en página 3"));
				$("#pagtot4").html(trans.__("URLS en página 4+"));
				$("#tablecabtit").html(trans.__("Página de aterrizaje"));
				$("#solotercontrol").html(trans.__("Sólo páginas de control"));
				$("#sinperm").html(trans.__("*No tiene permisos para editar las páginas de control."));
				$("#terctr").html(trans.__("Página de control"));
				$("#agragarter").html(trans.__("Agregar página"));
				$("#ntermpro").html(trans.__("Nueva página de control"));
			}

		},
		error: function(){

		}				  
	})//fin ajax

}

function compararAnt(anterior,actual,anterior_f,post){

	if(anterior == ''){
		var anterior = 0;
	}
	if(actual == ''){
		var actual = 0;
	}

	var anterior = eval(parseFloat(anterior));
	var actual   = eval(parseFloat(actual));

	if(anterior > actual){
		return '<div class="rojo comparador"><i class="fa fa-caret-down"></i> '+anterior_f+post+'</div>';
	}else if(anterior == actual){
		return '<div class="gris comparador">'+anterior_f+post+'</div>';
	}else{
		return '<div class="verde comparador"><i class="fa fa-caret-up"></i> '+anterior_f+post+'</div>';
	}
}

function Inicializar_datatables(){
	//jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = 3;
	$.fn.DataTable.ext.pager.numbers_length = 9;
	//alert("entra")

	if(getPaginaSeoActual() === 2){
	//DATATABLE DE ENLACES -----------------------------------------------------------------------------------------
	table_datatable_enlaces_dominiostrafico = $('#table_datatable_enlaces_dominiostrafico').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[0,'asc']],
		"searching": false,
		//Exportaciones 
		"dom": 'rtT <filp "fondo">',
		"tableTools": {
	         "aButtons": [
	             {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	         ],
	         "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			{"orderable": false },
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val(), 
			"data": {"strFuncion": 'table_datatable_enlaces_dominiostrafico'}
		}		
	});	

	table_datatable_enlaces_enlacestrafico = $('#table_datatable_enlaces_enlacestrafico').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[0,'asc']],
		"searching": false,
		//Exportaciones 
		"dom": 'rtT <filp "fondo">',
		"tableTools": {
	         "aButtons": [
	             {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	         ],
	         "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			{"orderable": false },
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val(), 
			"data": {"strFuncion": 'table_datatable_enlaces_enlacestrafico'}
		}		
	});	

	table_datatable_enlaces_dominiosconversiones = $('#table_datatable_enlaces_dominiosconversiones').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[0,'asc']],
		"searching": false,
		//Exportaciones 
		"dom": 'rtT <filp "fondo">',
		"tableTools": {
	         "aButtons": [
	             {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	         ],
	         "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			{"orderable": false },
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val(), 
			"data": {"strFuncion": 'table_datatable_enlaces_dominiosconversiones'}
		}		
	});	

	table_datatable_enlaces_enlacesconversiones = $('#table_datatable_enlaces_enlacesconversiones').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[0,'asc']],
		"searching": false,
		//Exportaciones 
		"dom": 'rtT <filp "fondo">',
		"tableTools": {
	         "aButtons": [
	             {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	         ],
	         "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			{"orderable": false },
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val(), 
			"data": {"strFuncion": 'table_datatable_enlaces_enlacesconversiones'}
		}		
	});	
  }

  if(getPaginaSeoActual() === 1){
	table_datatable_control = $('#table_datatable_control').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[2,'desc']],
		//Exportaciones 
		"dom": 'frtT <ilp "ondo">',
		"tableTools": {
	         "aButtons": [
             	{
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	         ],
	         "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?idVistaAnalytics=" + $("#dat_idvista").val()+ "&vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val() + "&pag=" + $("#dat_pag").val() + "&tipo=" + $("#dat_seo_t").val(),
			"data": {"strFuncion": 'table_datatable_terminoscontrol'}
		}		
	});
  }

	/*table_datatable_pagsentradal = $('#table_datatable_pagsentradal').dataTable({
		"language": {
			"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
		},
		"paging": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
		"ordering": true,
		"order": [[1,'desc']],
		"searching": true,
		//Exportaciones 
		"dom": 'frtT <ilp "ondo">',
		"tableTools": {
	         "aButtons": [
	             {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
              	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	         ],
	         "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	     },
		"columns":[
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null
			],
		"processing": true,			
		"serverSide": true,
		"ajax": {
			"type": "POST",
			"url":  "../informes/informes_ajax.php?vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val(),
			"data": {"strFuncion": 'table_datatable_pagsentradal'}
		}		
	});*/

}

$(document).ready(function(){
	$("#check_terminoscontrol").on("click",function(){

		if( $('#check_terminoscontrol').is(':checked') ) {
			$("#data_terminoscontrol").val("1");
		}else{
			$("#data_terminoscontrol").val("0");
		}

		recargaterm();	
		cargar_extras_Seo();
	
	});

	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 4,
		  vista: $("#dat_idvista").val()
		  },
		dataType: 'text',
		success: function(respuesta){
		  
			if(respuesta == 0){
				//$("#anadir_termino").html('<p class="infotrunc">*No tiene permisos para editar los términos de búsqueda.</p>');
				$("#anadir_termino").hide();
				$("#sin_permiso").fadeIn(600);
			}else{
				//$("#anadir_termino").html('<button type="button" class="btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Nuevo término de búsqueda</button>');
				$("#sin_permiso").hide();
				$("#anadir_termino").fadeIn(600);
			}
		},
		error: function(){

		}				  
	})//fin ajax

	$("#btn_abrirmodaltermino").on("click",function(){

		$(".ntermform").stop();
		if( $(".ntermform").css("display") == "none" ){
			$("#txt_termino").val("");
			$("#txt_categoria").val("");
			$(".ntermform").fadeIn(600);
		}else{
			$(".ntermform").fadeOut(300);
		}

	})

	$(".cerrarmodalterm").on("click",function(){
		$(".ntermform").stop();
		$(".ntermform").fadeOut(300);	
	})

	$("#btn_ntermino").on("click",function(){
		$("#txt_termino").css("border-color","#C8CDD7");
		$("#txt_categoria").css("border-color","#C8CDD7");
		var termino = $("#txt_termino").val();
		var cat = $("#txt_categoria").val();
		if(termino==""){
			$("#txt_termino").css("border-color","red");
		}else if(cat==""){
			$("#txt_categoria").css("border-color","red");
		}else{
			//Guardamos el termino en la db
			$.ajax({
				type: 'POST',
				url: 'seo_ajax.php',
				data: {
				  modo: 5,
				  url_vista: $("#dat_vistaurl").val(),
				  terminos: termino,
				  categoria: cat,
				  tipo: $("#dat_seo_t").val()
				  },
				dataType: 'text',
				success: function(respuesta){		  
						
					if(respuesta==1){
						$(".ntermform").stop();
						$(".ntermform").fadeOut(300);
						recargaterm()						
						cargarCatSeo()
					}else{
						$("#txt_termino").css("border-color","red");
					}
				},
				error: function(respuesta){
					alert(respuesta);
				}				  
			})//fin ajax
		}	
	})

	/*$('#myInput').on( 'keyup', function () {
	    table.search( this.value ).draw();
	});*/

	/*$(".contpags").on("click",function(){

		var pag = $(this).attr("name");
		$(".contpags").removeClass("selecpag");
		
		if($("#dat_pag").val() == pag){
			$("#dat_pag").val(0);

			table_datatable_control.api().ajax.url("../informes/informes_ajax.php?idVistaAnalytics=" + $("#dat_idvista").val()+ "&vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val() + "&pag=" + $("#dat_pag").val()).load();

		}else{
			$("#dat_pag").val(pag);
			$(this).addClass("selecpag");

			table_datatable_control.api().ajax.url("../informes/informes_ajax.php?idVistaAnalytics=" + $("#dat_idvista").val()+ "&vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val() + "&pag=" + $("#dat_pag").val()).load();

		}
		
	})*/

	$(".cajabotones a").on("click",function(){
		var pag = $(this).attr("name");
		$(".cajabotones").removeClass("selecpagb");
		/*var pag = dat[1];
		var dis = dat[0];*/
		if($("#dat_pag").val() == pag){

			$("#dat_pag").val(0);
			recargaterm()
		}else{
			$("#dat_pag").val(pag);
			$(this).parent().addClass("selecpagb");
			recargaterm()
		}

	})


	$(".opctpags").on("click",function(){
		var valor = $(this).attr("name");
		if($("#dat_seo_t").val() != valor){
			$("#dat_seo_t").val(valor);
			$(".troyt").html($(this).html());
			//Leer_Opcion_Comparacion();
			cargar_extras_Seo();
			recargaterm()
			cargarCatSeo()

		}
	});

});

function estrellaDown_term(vistaurl,termino){
	
	//alert(vistaurl + "  -  " + termino);
	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 6,
		  url_vista: $("#dat_vistaurl").val(),
		  terminos: termino,
		  tipo: 'term'
		  },
		dataType: 'text',
		success: function(respuesta){		  
			cargarCatSeo()	
			recargaterm()
		},
		error: function(respuesta){
			//alert(respuesta);
		}				  
	})//fin ajax

}

function estrellaDown_pags(vistaurl,termino){
	
	//alert(vistaurl + "  -  " + termino);
	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 6,
		  url_vista: $("#dat_vistaurl").val(),
		  terminos: termino,
		  tipo: 'pags'
		  },
		dataType: 'text',
		success: function(respuesta){		  
			cargarCatSeo()	
			recargaterm()
		},
		error: function(respuesta){
			//alert(respuesta);
		}				  
	})//fin ajax

}

function recargaterm(){
	table_datatable_control.api().ajax.url("../informes/informes_ajax.php?idVistaAnalytics=" + $("#dat_idvista").val()+ "&vistaUrl=" + $("#dat_vistaurl").val() + "&mesSelect=" + $("#dat_seo_meses").val() + "&control="+ $("#data_terminoscontrol").val() + "&pag=" + $("#dat_pag").val() + "&tipo=" + $("#dat_seo_t").val() ).load();
}

function estrellaUp_term(vistaurl,termino){
	
	//alert(vistaurl + "  -  " + termino);
	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 7,
		  url_vista: $("#dat_vistaurl").val(),
		  terminos: termino,
		  tipo: 'term'
		  },
		dataType: 'text',
		success: function(respuesta){		  
			cargarCatSeo()	
			recargaterm()
		},
		error: function(respuesta){
			//alert(respuesta);
		}				  
	})//fin ajax

}

function estrellaUp_pags(vistaurl,termino){
	
	//alert(vistaurl + "  -  " + termino);
	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 7,
		  url_vista: $("#dat_vistaurl").val(),
		  terminos: termino,
		  tipo: 'pags'
		  },
		dataType: 'text',
		success: function(respuesta){		  
			cargarCatSeo()	
			recargaterm()
		},
		error: function(respuesta){
			//alert(respuesta);
		}				  
	})//fin ajax

}

function editarpencil(key){

	$("#caja"+key).hide();
	$("#pencil"+key).hide();
	$("#input"+key).fadeIn(300);
}

function cancelareditar(key){

	$("#input"+key).hide();
	$("#caja"+key).fadeIn(300);
	$("#pencil"+key).fadeIn(300);
	
}

function editarGuardar_term(key,url,terminoant,catant){
	//alert(terminoant + "  --  " + url + " -- " + $("#dat_seo_meses").val() + "  --  "+ $("#inputdata"+key).val());
	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 8,
		  url_vista: $("#dat_vistaurl").val(),
		  terminos: $("#inputdata"+key).val(),
		  cat: $("#select"+key).val(),
		  terminosant: terminoant,
		  mes: $("#dat_seo_meses").val(),
		  tipo: 'term'
		  },
		dataType: 'text',
		success: function(respuesta){		  
				
			recargaterm()
		},
		error: function(respuesta){
			//alert(respuesta);
		}				  
	})//fin ajax
	
}

function editarGuardar_pags(key,url,terminoant,catant){
	//alert(terminoant + "  --  " + url + " -- " + $("#dat_seo_meses").val() + "  --  "+ $("#inputdata"+key).val());
	$.ajax({
		type: 'POST',
		url: 'seo_ajax.php',
		data: {
		  modo: 8,
		  url_vista: $("#dat_vistaurl").val(),
		  terminos: $("#inputdata"+key).val(),
		  cat: $("#select"+key).val(),
		  terminosant: terminoant,
		  mes: $("#dat_seo_meses").val(),
		  tipo: 'pags'
		  },
		dataType: 'text',
		success: function(respuesta){		  
				
			recargaterm()
		},
		error: function(respuesta){
			//alert(respuesta);
		}				  
	})//fin ajax
	
}

function anadiralselect(key,cat){
	$("#select"+key).val(cat);

}

function getPaginaSeoActual(){
	return $('.contenedor_ruta ul a.sel').closest('li').index();
}

	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var strPaisGa;
	var table_datatable_sitioweb;
	
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra();
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------


	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}
			


	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		
		
	}

	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());
		//alert(datFechaInicioFiltro);
		/*var valComparacion
		if(comparacion!=1){*/
			//valComparacion = $("#select_Comparacion").val();
			valComparacion = $("#dat_comparador").val();
		/*}else{
			valComparacion = 2
		}*/
		//alert(valComparacion);
		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}

	function cargar_datos_extras(){
		$.ajax({
			type: 'POST',
			url: '../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val(),
			data: { strFuncion: 'antispam_datosextra'}
		})
		.done(function (respuesta) {

			var objJson = jQuery.parseJSON(respuesta);

			$("#tot_sesiones").html( objJson.datos[0].tot_sesionesF );
			$("#tot_paginasvistassesion").html( objJson.datos[0].tot_paginasvistassesionF );
			$("#tot_tasarebote").html( objJson.datos[0].tot_tasareboteF );

			$("#tot_tot_sesiones").html( objJson.datos[0].tot_tot_sesionesF );
			$("#tot_tot_paginasvistassesion").html( objJson.datos[0].tot_tot_paginasvistassesionF );
			$("#tot_tot_tasarebote").html( objJson.datos[0].tot_tot_tasareboteF );

		})
	}

	$(document).ready(function() {
		
		//DATATABLE DE PAIS -----------------------------------------------------------------------------------------
		table_datatable_sitioweb = $('#table_datatable_sitioweb').dataTable({
			"language": {
				"url": "../assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[3,'desc']],
			"searching": false,
			//Exportaciones 
			//"dom": 'Trt <filp "fondo">',
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
	        "columnDefs": [
		    {
		        "targets": [ 2 ],
		        "visible": true
		    }
		    ],
			"columns":[
				null,
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strApp=" + $("#dat_app").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_sitioweb'}
			}		
		});		
		//DATATABLE de PAIS -----------------------------------------------------------------------------------------

		cargar_datos_extras();
		
	});	
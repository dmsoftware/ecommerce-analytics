	var sexo_rickshaw_duracionmediasesion = [];
	var sexo_rickshaw_duracionmediasesion_legend=[];
	var sexo_rickshaw_sesiones=[];
	var sexo_rickshaw_sesiones_legend=[];
	var sexo_rickshaw_paginasvistassesion=[];
	var sexo_rickshaw_paginasvistassesion_legend=[];

	var sexo_rickshaw_tasarebote=[];
	var sexo_rickshaw_tasarebote_legend=[];
	var sexo_rickshaw_dimensiones=[];
	var sexo_rickshaw_dimensiones_legend=[];

	var edad_rickshaw_duracionmediasesion = [];
	var edad_rickshaw_duracionmediasesion_legend=[];
	var edad_rickshaw_sesiones=[];
	var edad_rickshaw_sesiones_legend=[];
	var edad_rickshaw_paginasvistassesion=[];
	var edad_rickshaw_paginasvistassesion_legend=[];

	var edad_rickshaw_tasarebote=[];
	var edad_rickshaw_tasarebote_legend=[];
	var edad_rickshaw_dimensiones=[];
	var edad_rickshaw_dimensiones_legend=[];

	var edad_nombre_dimension = [];
	var sexo_nombre_dimension = [];


	var rickshaw_sesiones=[];
	var rickshaw_sesiones_legend=[];
	var nombre_dimension = [];

	var cab_sesion = [];
	var cab_nombre_sesion = [];
	var cab_porcentaje = [];
	var cab_sesiones = [];

	var t_datatable_sexo;
	var t_datatable_edades;
	var blnComparacion;
	var datFechaInicioFiltroAnt, datFechaFinFiltroAnt,strFechaInicioFiltroAnt,strFechaFinFiltroAnt;
	var intNumDiasPeriodoActual;
	var total_usuarios_nuevos = 0;
	var total_usuarios_recurrentes = 0;
	
	var ses_totales;
	//RICKSHAW: Colores series
	var strColorSerie1 = 'steelblue';
	var strColorSerie2 = 'lightblue';	

	var cab_sesiones = [];

	var chart_sexo;
	var chart_edad;

	//RECARGA DE DIMENSION/PERSPECTIVA -----------------------------------------------------------------------
	//$("select#Dimensiones").selectBoxIt();
	$("#select_Dimensiones").bind({
		"change": function(ev, obj) {
			Recargar_Datatable();
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	});	
	//DIMENSION/PERSPECTIVA -----------------------------------------------------------------------------------

	//RECARGA DE LAS COMPARACIONES -----------------------------------------------------------------------
	$("#select_Comparacion").bind({
		"change": function(ev, obj) {
			Leer_Opcion_Comparacion();			
			Cargar_Datos_Extra("sexo");
			Cargar_Datos_Extra("edad");
		}
	})//.trigger('change');		
	
	function Leer_Opcion_Comparacion() {
		//comparacion = 0
		datFechaInicioFiltro = new Date($("#dat_fechaini").val());
		datFechaFinFiltro = new Date($("#dat_fechafin").val());

		valComparacion = $("#dat_comparador").val();
		//alert($("#dat_comparador").val())
		switch (valComparacion) {
			case "0":
				blnComparacion = false;
				break;
			case "1": //Mismo periodo del año pasado
				blnComparacion = true;
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				datFechaInicioFiltroAnt.setMonth (datFechaInicioFiltroAnt.getMonth() - 12);
				datFechaFinFiltroAnt.setMonth (datFechaFinFiltroAnt.getMonth() - 12);
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);
				break;
			case "2": //Periodo anterior
				blnComparacion = true;
				//alert(datFechaInicioFiltro+" -- "+datFechaFinFiltro);
				datFechaInicioFiltroAnt = new Date(datFechaInicioFiltro);
				datFechaFinFiltroAnt = new Date(datFechaFinFiltro);
				intNumDiasPeriodoActual = daysBetween(datFechaInicioFiltro,datFechaFinFiltro);
				datFechaInicioFiltroAnt.setDate (datFechaInicioFiltroAnt.getDate() - intNumDiasPeriodoActual);
				datFechaFinFiltroAnt.setDate (datFechaFinFiltroAnt.getDate() - intNumDiasPeriodoActual);					
				strFechaInicioFiltroAnt = DateToString(datFechaInicioFiltroAnt);
				strFechaFinFiltroAnt = DateToString(datFechaFinFiltroAnt);			
				break;
		}
		//$('#Comparacion_Opciones').html('Fecha incio: ' + strFechaInicioFiltroAnt + ' Fecha fin: ' + strFechaFinFiltroAnt );
	}
	//COMPARACIONES -----------------------------------------------------------------------------------	
	
	function Recargar_Datatable() {
		//alert(blnComparacion)
		t_datatable_sexo.api().ajax.url('../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
		t_datatable_edades.api().ajax.url('../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
		t_datatable_intereses.api().ajax.url('../informes/informes_ajax.php?strProyectoAsociado=' + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val()).load();
	}
	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------

	
	//CARGA DE GRÁFICOS DE EVOLUCIONES ----------------------------------------------------------------------------
	function DevolverContenido_Total(Valor,ValorAnt,ValorF,ValorAntF,blnSoloGrafico){
		if(ValorAnt != ''){
			if (parseFloat(Valor) > parseFloat(ValorAnt)) {
				// strAux = '<img src="/public/images/flecha_arriba.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
				strAux = '<div class="verde comparador"><i class="fa fa-caret-up"></i> ' + ValorAntF + '%</div>';
			} else {
				if (parseFloat(Valor) == parseFloat(ValorAnt)) {
					// strAux = '<img src="/public/images/flecha_igual.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
					strAux = '<div class="gris comparador"> ' + ValorAntF + '%</div>';
				} else {
					// strAux = '<img src="/public/images/flecha_abajo.gif" Title="Actual: ' + ValorF +' | Anterior: ' + ValorAntF + '"/>';
					strAux = '<div class="rojo comparador"><i class="fa fa-caret-down"></i> ' + ValorAntF + '%</div>';
				}
			}
			if (blnSoloGrafico) {
				return strAux
			} else {
				return strAux + ' ' + ValorF;
			}
		}
	}
	
	function treatAsUTC(date) {
		var result = new Date(date);
		result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
		return result;
	}

	function daysBetween(startDate, endDate) {
		var millisecondsPerDay = 24 * 60 * 60 * 1000;
		return ((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay) + 1; //sumamos un día
	}
	
	function DateToString (datDate) {
		return datDate.toISOString().slice(0,10);
	}

	





function formato_numero(numero, decimales, separador_decimal, separador_miles){     
	numero=parseFloat(numero);     
	if(isNaN(numero)){         
		return "";}     
	if(decimales!==undefined){        
	 	// Redondeamos         
		numero=numero.toFixed(decimales);     
	}     
	// Convertimos el punto en separador_decimal     
	numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
	if(separador_miles){         
		// Añadimos los separadores de miles         
		var miles=new RegExp("(-?[0-9]+)([0-9]{3})");         
		while(miles.test(numero)) {             
			numero=numero.replace(miles, "$1" + separador_miles + "$2");         
		}     
	}     
	return numero; 
}


	//CARGAMOS TODAS LA FUNCIONES NECESARIAS PARA UNA RECARGA DE AJAX
	function cargador(){
		Leer_Opcion_Comparacion();	
		Recargar_Datatable();

	}


	$(document).ready(function() {

		//Opciones de comparación de datos -------------------------------------------
		Leer_Opcion_Comparacion(1);	
		//Opciones de comparación de datos -------------------------------------------



		//DATATABLE DE TÉRMINOS -----------------------------------------------------------------------------------------
		table_datatable_terminos = $('#table_datatable_terminos').dataTable({
			"language": {
				"url": "/public/assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				null,
				null,
				null,
				null,
				null
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_terminos'}
			}		
		});		

		//DATATABLE de TÉRMINOS ----------------------------------------------------------------------------------------- 		
		
		//DATATABLE DE TÉRMINOS CONV -----------------------------------------------------------------------------------------
		table_datatable_terminos_conversiones = $('#table_datatable_terminos_conversiones').dataTable({
			"language": {
				"url": "/public/assets/js/datatables/" + trans.getDataTableLangFile() + ".txt"
			},
			"paging": true,
			"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"] ],
			"ordering": true,
			"order": [[1,'asc']],
			"searching": false,
			//Exportaciones 
			"dom": 'rtT <filp "fondo">',
			"tableTools": {
	            "aButtons": [
	                {
                 "sExtends": "pdf",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/pdf_ico.png'/>"
             	},
             	/*{
                 "sExtends": "csv",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "CSV"
             	},*/
             	{
                 "sExtends": "xls",
                 "sPdfOrientation": "landscape",
                 //"sPdfMessage": "Your custom message would go here.",
                 "sButtonText": "<img width='30px' src='../images/csv_ico.png'/>"
             	},

	            ],
	            "sSwfPath": "../js/copy_csv_xls_pdf.swf"
	        },
			"columns":[
				{"orderable": false },
				{"orderable": false },
				{"orderable": false },
				{"orderable": false }
				],
			"processing": true,			
			"serverSide": true,
			"ajax": {
				"type": "POST",
				"url":  "../informes/informes_ajax.php?strProyectoAsociado=" + $("#dat_proyectoasociado").val() + "&idVistaAnalytics=" + $("#dat_idvista").val() + "&strFechaInicioFiltro=" + $("#dat_fechaini").val() + "&strFechaFinFiltro=" + $("#dat_fechafin").val() + "&strPais=" + $("#pais_comparar").val() + "&strIdioma=" + $("#idioma_comparar").val() + "&blnComparacion=" + blnComparacion + "&strFechaInicioFiltroAnt=" + strFechaInicioFiltroAnt + "&strFechaFinFiltroAnt=" + strFechaFinFiltroAnt + "&strFiltro=" + $("#dat_filtro").val(), 
				"data": {"strFuncion": 'caracteristicas_datatable_terminos_conversiones'}
			}		
		});		

		//DATATABLE de TÉRMINOS CONV ----------------------------------------------------------------------------------------- 		
		
});	


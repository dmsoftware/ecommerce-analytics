function translationClass() {
	var _self = this;
	var lang = {};
	var language = getCookieLang();

  this.init = function(){
    var path = '../public/includes/lang/' + language + '.txt';
  	return $.ajax({
			url: path,
			dataType: 'json',
			async: false,
			success: function(data){
				lang[language] = data;
        return true;
			},
			error: function(error){
				return true;
			}
		});
  };

  this.__ = function(str){
  	if (lang[language].hasOwnProperty(str)) {
      return lang[language][str] === '' ? str : lang[language][str];
    }else{
    	 return str;
    } 
  };

  this.getLang = function(){
    return language;
  };

  this.getDataTableLangFile = function(){
  	var file = 'spanish';

  	if(language != 'es'){
  		file = language;
  	}

  	return file;
  }

  function getCookieLang() {
    var nameEQ = "idioma_usuario=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }

    return 'es';
  }
}

trans = new translationClass();
trans.init();
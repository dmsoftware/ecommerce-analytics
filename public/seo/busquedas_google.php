<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{

	include("../includes/head.php");
	include("../includes/side_seo.php"); 
	
?>

<script src="../js/Chart.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<style type="text/css">
	.fechas_index{ width: 100%; text-align: center; top: 200px !important;}
	.ftm{ margin: 15px;margin-right: 55px;margin-left: 55px;}
	.restit{ text-align: center;
						display: block;
						color: #969696;
						font-size: 18px;
						margin: auto;}
	.res_titindex{color: black;font-weight: bold;font-size: 54px;}
	.spanadet{ font-size: 14px; margin-top: 15px; display: block;}
	#total_contenido_idioma{height: 252px;}
	.tittit{border-bottom: 1px solid #636363;
			padding: 5px;
			padding-left: 0px;
			width: 97%;
			margin: auto;
			margin-bottom: 15px;
			}
	.desc_enlaces{text-align: center; display: block; width: 200px;color: #969696;
					font-size: 18px; margin: auto;}
	.number_enlaces{ color: #696969;
					font-size: 50px;
					font-weight: bold;
					text-align: center;
					display: block;}
	.tittablemin{     display: block;
				    color: #969696;
				    font-size: 18px;
				}


	.DTTT_container { margin-top: 0px !important;}

	.titconsultas{ text-align: center;
				display: block;
				font-size: 15px;}

	.opcmeses{ cursor: pointer;}
	.cont_combot{ display: block;}
	.troy{ min-width: 90px;}
	.infocombo{ float: left; width: 35px;display: block;padding-top: 7px;}
	.datacombo{ float: left;  display: block;}
	.sep{ clear: both; margin-bottom: 20px;display: block;}
	.seo_extras{color: #6C6C6C;
				text-align: center;
				display: block;
				font-size: 20px;
				font-weight: bold;}
	.derecha_caja{ float: left;display: block;
					min-height: 17px;
					margin-top: 30px;
					margin-bottom: 5px;
					padding-left: 0px;}
.izquierda_caja{ width: 200px; float: left; margin-left: 30px;margin-bottom: 0px;}
#table_datatable_control_wrapper{ clear: both;}
#table_datatable_control_filter{position: absolute;
right: 0px;
top: -50px;
background-color: transparent;
border: medium none;}
.termc{ width: auto;
float: left;
padding-top: 6px;}

.termcb{ /*width: 335px*/;
display: block;
position: relative;
}

.termcb input{ padding: 7px; width: 250px; margin-right: 15px;}
.trium{}

.oculto{display: none;}
.derecha_caja{ position: relative;}
.ntermform{ 
	    position: absolute;
	    width: 200px;
	    height: 188px;
	    display: block;
	    background-color: #FFF;
	    z-index: 99;
	    border: 1px solid gray;
	    padding: 10px;
	    border-radius: 5px;
	    box-shadow: 5px 5px 17px;
	    left: 70px;
	    top: -20px;
	    display: none;
	}
#btn_ntermino{ float: right;}
.derecha_caja{ display: none;}
.cerrarmodalterm{ position: absolute;
			right: 5px;
			top: 2px;
			font-weight: bold;
			cursor: pointer;}
.editar_termino{ float: right;}
.eliminar_termino{ float: right;}
.controlterm{background-color: #C8D4DE;}
.imgwidcontrol{ width: 30px; margin: auto; display: block;}
.btncontrolsn {display: block; overflow: auto; width: 33px;}


.selecpag{background-color: #F5E4D4 !important; border: 1px solid #D17F15 !important;}
.ttinfo{margin-left: 15px;
		font-size: 16px;
		margin-top: 21px;
		margin-bottom: 14px;}
.inputre{ padding: 7px !important;}
.curve_chart{margin-left: -15px;}
</style>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />


<div class="row"  style="display:none">

	<h2 class="tittit"><?php $trans->__('Páginas indexadas'); ?></h2>
	
	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray" id="total_contenido_idioma">	

		</div>

	</div>
	
	<div class="col-sm-9">
	
		<div class="tile-stats tile-white-gray" id="total_contenido_info">
			
		</div>
		
	</div>
	
</div>

<br />

<style type="text/css">
	
	.opctpags{ cursor: pointer;}
	#contenedor_combotpags{ margin-left: 10px; margin-right: 10px;}
	.maswid{ width: 65px;}
</style>
<div class="row">
	<h2 class="tittit"><?php $trans->__('Consultas de búsqueda en Google'); ?></h2>

	<div class="col-sm-12">
		<div class="datacombo" id="contenedor_combotpags">
			<label class="infocombo maswid control-label label-contexto"><b><?php $trans->__('Consultar'); ?>:</b></label>
			<div class="btn-group">
				<button type="button" class="btn btn-default troyt"><?php $trans->__('Términos de búsqueda'); ?></button>
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				  <span class="caret"></span>
				  <span class="sr-only"><?php $trans->__('Toggle Dropdown'); ?></span>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li><a name="term" class="opctpags"><?php $trans->__('Términos de búsqueda'); ?></a></li>
					<li><a name="pags" class="opctpags"><?php $trans->__('Páginas de entrada'); ?></a></li>
				</ul>
				<input type="hidden" value="term" id="dat_seo_t">
			</div>	

		</div>

		<div class="cont_combot">
			<label class="infocombo control-label label-contexto"><b><?php $trans->__('Mes'); ?>:</b></label>
			<div class="datacombo" id="contenedor_combomeses">

			</div>
		</div>

		<div class="checkbox izquierda_caja">
		  <label>
		    <input type="checkbox" name="tcontrol" id="check_terminoscontrol" value="" >
		    <span id="solotercontrol"></span>
		  </label>
		  <input type="hidden" id="data_terminoscontrol" value="0">
		</div>
	</div>

	<br class="sep">
	<style type="text/css">
		.caja_comparador{ width: auto; position: absolute; top: 5px; right: 5px;}
		.titconsultas { text-align: center;
						display: block;
						color: #969696;
						font-size: 18px;
						margin: auto;}
		.rateseo{ display: block;
				color: #9F9F9F;
				font-size: 12px;
				margin-bottom: 15px;
				margin-top: -5px;}
		.caja_comparadorb{ max-width: 69px;
						width: auto;
						display: block;
						margin: auto;
					overflow: hidden;
					margin-top: 6px;}
		.titconsultasb { text-align: center;
						display: block;
						color: #969696;
						font-size: 18px;
						margin: auto;
						margin-bottom: 15px; }
		.doble{ width: 22%; float: left;}
		.mono{ width: 12%; float: left; }
		.titfil{ float: left;text-align: center;
						display: block;
						color: #969696;
						font-size: 18px;;
						margin: 5px 5px auto auto;}
		.seo_extras_fill{float: left;color: #6C6C6C; text-align: center; font-size: 25px;  font-weight: bold;}
		.caja_comparador_fill{ display: block; float: left; }
		.col-sm-3,.col-sm-5{ padding-left: 10px; padding-right: 10px;}
		.tpags{ display: block; margin-top: 5px;}
		.titpant{ display: block; width: 50%; float: left;}
		.titpantb{ padding-left: 8%}
		.cont_titpant{  overflow: hidden;
				    float: left;
				    width: 380px;
				    margin-left: 10px;
				}
		.icotitot{
					width: 30px;
					float: left;
					margin-left: 5px;
					margin-right: 5px;
					margin-top: 8px;
					}
		.tret{
			margin-top: 5px;
			margin-left: 10px;
		}
	</style>

	<div class="col-sm-6">
		<div class="tile-stats tile-white-gray">		
			<span class="titfil" id="tittotal" ></span>
			<div class="cont_titpant">
				<div class="titpant">
					<i class="fa fa-desktop icotitb icotitot"></i>
					<span class="seo_extras_fill" id="seo_terminos_w"></span>
					<div class="caja_comparador_fill tret" id="seo_terminos_w_ant"></div>
				</div>
				<div class="titpant titpantb">
					<i class="fa fa-mobile icotitb icotitot"></i>
					<span class="seo_extras_fill" id="seo_terminos_m"></span>
					<div class="caja_comparador_fill tret" id="seo_terminos_m_ant"></div>
				</div>	
			</div>		
		</div>
	</div>
	<br style="clear:both">
	
	<div class="col-sm-3">
		<div class="tile-stats tile-white-gray">
			<span class="titconsultasb" ><?php $trans->__('Impresiones'); ?></span>
			<div class="col-sm-6">
				<span><i class="fa fa-desktop icotitb"></i></span>
				<span class="seo_extras" id="seo_impresiones_w"></span>
				<div class="caja_comparadorb" id="seo_impresiones_w_ant"></div>
			</div>
			<div class="col-sm-6">
				<i class="fa fa-mobile icotitb"></i>
				<span class="seo_extras" id="seo_impresiones_m"></span>
				<div class="caja_comparadorb" id="seo_impresiones_m_ant"></div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="tile-stats tile-white-gray">
			<span class="titconsultasb" ><?php $trans->__('Clicks'); ?></span>
			<div class="col-sm-6">
				<span><i class="fa fa-desktop icotitb"></i></span>
				<span class="seo_extras" id="seo_clicks_w"></span>
				<div class="caja_comparadorb" id="seo_clicks_w_ant"></div>
			</div>
			<div class="col-sm-6">
				<i class="fa fa-mobile icotitb"></i>
				<span class="seo_extras" id="seo_clicks_m"></span>
				<div class="caja_comparadorb" id="seo_clicks_m_ant"></div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="tile-stats tile-white-gray">
			<span class="titconsultasb" ><?php $trans->__('CTR medio'); ?></span>
			<div class="col-sm-6">
				<span><i class="fa fa-desktop icotitb"></i></span>
				<span class="seo_extras" id="seo_ctrmedio_w"></span>
				<div class="caja_comparadorb" id="seo_ctrmedio_w_ant"></div>
			</div>
			<div class="col-sm-6">
				<i class="fa fa-mobile icotitb"></i>
				<span class="seo_extras" id="seo_ctrmedio_m"></span>
				<div class="caja_comparadorb" id="seo_ctrmedio_m_ant"></div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="tile-stats tile-white-gray">
			<span class="titconsultasb" ><?php $trans->__('Posición media'); ?></span>
			<div class="col-sm-6">
				<span><i class="fa fa-desktop icotitb"></i></span>
				<span class="seo_extras" id="seo_posmedia_w"></span>
				<div class="caja_comparadorb" id="seo_posmedia_w_ant"></div>
			</div>
			<div class="col-sm-6">
				<i class="fa fa-mobile icotitb"></i>
				<span class="seo_extras" id="seo_posmedia_m"></span>
				<div class="caja_comparadorb" id="seo_posmedia_m_ant"></div>
			</div>
		</div>
	</div>

	<br style="clear:both">
	<style type="text/css">
	.cajabotones{ display: block; width: 50%; float: left;}
	.titmodi{ padding: 0px !important}
	.cont_top{ display: block;width: 100%; overflow: hidden; padding: 15px;}
	.cajabotones { display: block; overflow: hidden;padding: 10px; border: 1px solid #CFCFCF;
			color: gray; cursor: pointer;}
	.cba{border-radius: 0px 0px 0px 7px;}
	.cbb{border-radius: 0px 0px 7px 0px;}
	.comppags{float: right;}
	.cajabotones:hover {border: 1px solid #BF6C00;color: #E48100;
			background-color: rgba(233, 154, 77, 0.23);}
	.selecpagb{border: 1px solid #BF6C00 !important;color: #E48100 !important;
			background-color: rgba(233, 154, 77, 0.23) !important;}
	</style>
	<div class="col-sm-3">
		<div class="tile-stats titmodi tile-white-gray contpags" name="1">
			<div class="cont_top">
				<span class="titconsultas" id="pagtot1"></span>
			</div>
			<div class="cajabotones cba">
				<a name="w1">
					<span><i class="fa fa-desktop icotitb"></i></span>
					<span class="seo_extras" id="seo_tpag1_w"></span>
					<div class="caja_comparadorb" id="seo_tpag1_w_ant"></div>
				</a>
			</div>
			<div class="cajabotones cbb">
				<a name="m1">
					<i class="fa fa-mobile icotitb"></i>
					<span class="seo_extras" id="seo_tpag1_m"></span>
					<div class="caja_comparadorb" id="seo_tpag1_m_ant"></div>
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="tile-stats titmodi tile-white-gray contpags" name="2">
			<div class="cont_top">
				<span class="titconsultas"  id="pagtot2" ></span>
			</div>
			<div class="cajabotones cba">
				<a name="w2">
					<span><i class="fa fa-desktop icotitb"></i></span>
					<span class="seo_extras" id="seo_tpag2_w"></span>
					<div class="caja_comparadorb" id="seo_tpag2_w_ant"></div>
				</a>
			</div>
			<div class="cajabotones cbb">
				<a name="m2">
					<i class="fa fa-mobile icotitb"></i>
					<span class="seo_extras" id="seo_tpag2_m"></span>
					<div class="caja_comparadorb" id="seo_tpag2_m_ant"></div>
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="tile-stats titmodi tile-white-gray contpags" name="3">
			<div class="cont_top">
				<span class="titconsultas"  id="pagtot3"></span>
			</div>
			<div class="cajabotones cba">
				<a name="w3">
					<span><i class="fa fa-desktop icotitb"></i></span>
					<span class="seo_extras" id="seo_tpag3_w"></span>
					<div class="caja_comparadorb" id="seo_tpag3_w_ant"></div>
				</a>
			</div>
			<div class="cajabotones cbb">
				<a name="m3">
					<i class="fa fa-mobile icotitb"></i>
					<span class="seo_extras" id="seo_tpag3_m"></span>
					<div class="caja_comparadorb" id="seo_tpag3_m_ant"></div>
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="tile-stats titmodi tile-white-gray contpags" name="4">
			<div class="cont_top">
				<span class="titconsultas"  id="pagtot4"></span>
			</div>
			<div class="cajabotones cba">
				<a name="w4">
					<span><i class="fa fa-desktop icotitb"></i></span>
					<span class="seo_extras" id="seo_tpag4_w"></span>
					<div class="caja_comparadorb" id="seo_tpag4_w_ant"></div>
				</a>
			</div>
			<div class="cajabotones cbb">
				<a name="m4">
					<i class="fa fa-mobile icotitb"></i>
					<span class="seo_extras" id="seo_tpag4_m"></span>
					<div class="caja_comparadorb" id="seo_tpag4_m_ant"></div>
				</a>
			</div>
		</div>
	</div>
	
	<input type="hidden" id="dat_pag" value="0" />
</div>

<div class="row">
	<div class="col-sm-12">
		


		<div class="derecha_caja" id="anadir_termino">
			<div class="ntermform">
				<a class="cerrarmodalterm">X</a>
				<form>

					 <div class="form-group">
					    <label for="txt_termino" id="terctr"></label>
					    <input type="text" class="form-control" id="txt_termino" placeholder="">
					 </div>
					
					<div class="form-group">
						
						<label for="txt_categoria"><?php $trans->__('Categoría'); ?></label>
					  	<div class="input-group">
					  		
					        <input class="form-control" aria-label="Text input with segmented button dropdown" type="text" id="txt_categoria">
					        <div class="input-group-btn">
					          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					            <span class="caret"></span>
					            <span class="sr-only">Toggle Dropdown</span>
					          </button>
					          <ul class="dropdown-menu dropdown-menu-right combocatseo" role="menu">

					          </ul>
					        </div>
					    </div>

					</div> 	

					<style type="text/css">
					.linkselectcat{ cursor: pointer;}
					.contcatsc{ width: 180px; float: left;}
					.trium{ float: left;}
					.titpagsentradas{ display: block; overflow: hidden;}
					.dataTables_filter{ border: none !important}
					.libre_caja{ background-color: transparent !important; border: none !important;}
					.icotit{ font-size: 22px; text-align: center; display: block !important;}
					.icotitb{ font-size: 20px; text-align: center; display: block !important;}
					.thicos{ padding: 6px !important;}
					.centrar{ text-align: center;}
					.catizq{ text-align: left; display: block;}
					.cajatable{ width: 100%;}

					.dataTables_wrapper a { margin: auto;}


					#table_datatable_control tbody tr td:nth-child(4),
					#table_datatable_control tbody tr td:nth-child(6),
					#table_datatable_control tbody tr td:nth-child(8),
					#table_datatable_control tbody tr td:nth-child(10) {border-left:solid 2px #cdcdcd;}
					#table_datatable_control tbody tr td:nth-child(5),
					#table_datatable_control tbody tr td:nth-child(7),
					#table_datatable_control tbody tr td:nth-child(9),
					#table_datatable_control tbody tr td:nth-child(11) {background:#e9f8ff}


					#table_datatable_control thead tr:nth-child(1) th:nth-child(4),
					#table_datatable_control thead tr:nth-child(1) th:nth-child(5),
					#table_datatable_control thead tr:nth-child(1) th:nth-child(6),
					#table_datatable_control thead tr:nth-child(1) th:nth-child(7) {border-left:solid 2px #fff;}
					#table_datatable_control thead tr:nth-child(2) th:nth-child(4),
					#table_datatable_control thead tr:nth-child(2) th:nth-child(6),
					#table_datatable_control thead tr:nth-child(2) th:nth-child(8),
					#table_datatable_control thead tr:nth-child(2) th:nth-child(10) {border-left:solid 2px #fff;}

					#table_datatable_pagsentradal { border: none !important}
					#table_datatable_pagsentradal tbody tr td:nth-child(2) 
					#table_datatable_pagsentradal tbody tr td:nth-child(4),
					#table_datatable_pagsentradal tbody tr td:nth-child(6),
					#table_datatable_pagsentradal tbody tr td:nth-child(8){border-left:solid 2px #cdcdcd;}
					#table_datatable_pagsentradal tbody tr td:nth-child(3),
					#table_datatable_pagsentradal tbody tr td:nth-child(5),
					#table_datatable_pagsentradal tbody tr td:nth-child(7),
					#table_datatable_pagsentradal tbody tr td:nth-child(9){background:#e9f8ff}


					#table_datatable_pagsentradal thead tr:nth-child(1) th:nth-child(2),
					#table_datatable_pagsentradal thead tr:nth-child(1) th:nth-child(3),
					#table_datatable_pagsentradal thead tr:nth-child(1) th:nth-child(4),
					#table_datatable_pagsentradal thead tr:nth-child(1) th:nth-child(5) {border-left:solid 2px #fff;}
					#table_datatable_pagsentradal thead tr:nth-child(2) th:nth-child(2),
					#table_datatable_pagsentradal thead tr:nth-child(2) th:nth-child(4),
					#table_datatable_pagsentradal thead tr:nth-child(2) th:nth-child(6),
					#table_datatable_pagsentradal thead tr:nth-child(2) th:nth-child(8) {border-left:solid 2px #fff;}

					#curve_chart{width: 700px; display: block; margin: auto;}

					</style>
				    <button type="button" class="btn btn-default" id="btn_ntermino"><span id="agragarter"></span></button>
				</form>
			</div>
			<button id="btn_abrirmodaltermino" type="button" class="btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span><span id="ntermpro"></span></button>
		</div>



		<div class="derecha_caja" id="sin_permiso">
			<p class="infotrunc" id="sinperm"></p>
		</div>

		<table class="table table-bordered datatable" id="table_datatable_control" style="clear: both; border: none;">
			<thead>
				<tr>
					<th class="libre_caja"></th>
					<th class="libre_caja"></th>
					<th class="libre_caja"></th>
					<th width="16%" colspan="2" class="centrar"><?php $trans->__('Impresiones'); ?></th>
					<th width="16%" colspan="2" class="centrar"><?php $trans->__('Clicks'); ?></th>
					<th width="16%" colspan="2" class="centrar"><?php $trans->__('CTR'); ?></th>
					<th width="16%" colspan="2" class="centrar"><?php $trans->__('Pos. media'); ?></th>
				</tr>
				<tr>
					<th><span class="cab_tit" id="tablecabtit"></span></th>
					<th><?php $trans->__('Categoría'); ?></th>
					<th><?php $trans->__('Control'); ?></th>
					<th width="8%" class="thicos"><i class="fa fa-desktop icotitb"></i></th><th width="8%" class="thicos"><i class="fa fa-mobile icotit"></i></th>
					<th width="8%" class="thicos"><i class="fa fa-desktop icotitb"></i></th><th width="8%" class="thicos"><i class="fa fa-mobile icotit"></i></th>
					<th width="8%" class="thicos"><i class="fa fa-desktop icotitb"></i></th><th width="8%" class="thicos"><i class="fa fa-mobile icotit"></i></th>
					<th width="8%" class="thicos"><i class="fa fa-desktop icotitb"></i></th><th width="8%" class="thicos"><i class="fa fa-mobile icotit"></i></th>
				</tr>

			</thead>
			<tbody>
		</table>
		<?php 

		?>
				
	</div>
</div>

<br />

<div class="row"  style="display:none">

	<h2 class="tittit"><?php $trans->__('Enlaces externos activos'); ?></h2>

	<p class="ttinfo"><?php $trans->__('Los datos de este apartado tienen en cuenta los enlaces que se han seguido al menos una vez en el último año.'); ?></p>
	
	<div class="col-sm-6">
	
		<div class="tile-stats tile-white-gray" id="dominios_enlaces">	
			<span class="desc_enlaces"><?php $trans->__('Dominios con enlaces a nuestro sitio web'); ?></span>
			<span class="number_enlaces" id="total_dominios_enlaces"></span>
		</div>

	</div>
	
	<div class="col-sm-6">
	
		<div class="tile-stats tile-white-gray" id="paginas_enlaces">
			<span class="desc_enlaces"><?php $trans->__('Páginas con enlaces a nuestro sitio web'); ?></span>	
			<span class="number_enlaces" id="total_paginas_enlaces"></span>
		</div>
		
	</div>

	<br />

	<!--Datatables-->
	<div class="col-sm-6">
		<h3 class="tittablemin"><?php $trans->__('Dominios que atraen más tráfico al sitio web'); ?></h3>
		<table class="table table-bordered datatable" id="table_datatable_enlaces_dominiostrafico">
			<thead>
				<tr>
					<th><?php $trans->__('Dominios'); ?></th>
					<th><?php $trans->__('Sesiones atraídas'); ?></th>
				</tr>	
			</thead>
			<tbody>
		</table>
	</div>

	<div class="col-sm-6">
		<h3 class="tittablemin"><?php $trans->__('Enlaces que atraen más tráfico al sitio web'); ?></h3>
		<table class="table table-bordered datatable" id="table_datatable_enlaces_enlacestrafico">
			<thead>
				<tr>
					<th><span class="cab_tit"><?php $trans->__('Dominios'); ?></span></th>
					<th><?php $trans->__('Sesiones atraídas'); ?></th>
				</tr>	
			</thead>
			<tbody>
		</table>
	</div>

	<br />

	<div class="col-sm-6">
		<h3 class="tittablemin"><?php $trans->__('Dominios que obtienen más conversiones'); ?></h3>
		<table class="table table-bordered datatable" id="table_datatable_enlaces_dominiosconversiones">
			<thead>
				<tr>
					<th><span class="cab_tit"><?php $trans->__('Enlaces'); ?></span></th>
					<th><?php $trans->__('Conversiones'); ?></th>
				</tr>	
			</thead>
			<tbody>
		</table>
	</div>

	<div class="col-sm-6">
		<h3 class="tittablemin"><?php $trans->__('Enlaces que obtienen más conversiones'); ?></h3>
		<table class="table table-bordered datatable" id="table_datatable_enlaces_enlacesconversiones">
			<thead>
				<tr>
					<th><span class="cab_tit"><?php $trans->__('Enlaces'); ?></span></th>
					<th><?php $trans->__('Conversiones'); ?></th>
				</tr>	
			</thead>
			<tbody>
		</table>
	</div>

	
</div>

<br />


<script src="<?=RUTA_ABSOLUTA?>js/seo_scripts.js"></script>
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



<?php

if(!isset($_COOKIE["usuario"])) {

	//echo "No tiene usuario";
	//Si no tiene COOKIE GUARNAMOS EN UNA COOKIE LA URL DONDE QUERÍA ENTRAR
	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

	setcookie( "ruta", $url, time() + (86400), "/"); //86400 es un dia
	
	header('Location: ../public/index.php');



}else{


	include("../includes/head.php");
	include("../includes/side_seo.php"); 
	
?>

<script src="../js/Chart.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<style type="text/css">
	.fechas_index{ width: 100%; text-align: center; top: 200px !important;}
	.ftm{ margin: 15px;margin-right: 55px;margin-left: 55px;}
	.restit{ text-align: center;
						display: block;
						color: #969696;
						font-size: 18px;
						margin: auto;}
	.res_titindex{color: black;font-weight: bold;font-size: 54px;}
	.spanadet{ font-size: 14px; margin-top: 15px; display: block;}
	#total_contenido_idioma{height: 252px;}
	.tittit{border-bottom: 1px solid #636363;
			padding: 5px;
			padding-left: 0px;
			width: 97%;
			margin: auto;
			margin-bottom: 15px;
			}
	.desc_enlaces{text-align: center; display: block; width: 200px;color: #969696;
					font-size: 18px; margin: auto;}
	.number_enlaces{ color: #696969;
					font-size: 50px;
					font-weight: bold;
					text-align: center;
					display: block;}
	.tittablemin{     display: block;
				    color: #969696;
				    font-size: 18px;
				}


	.DTTT_container { margin-top: 0px !important;}

	.titconsultas{ text-align: center;
				display: block;
				font-size: 15px;}

	.opcmeses{ cursor: pointer;}
	.cont_combot{ display: block;}
	.troy{ min-width: 90px;}
	.infocombo{ float: left; width: 35px;display: block;padding-top: 7px;}
	.datacombo{ float: left;  display: block;}
	.sep{ clear: both; margin-bottom: 20px;display: block;}
	.seo_extras{color: #6C6C6C;
				text-align: center;
				display: block;
				font-size: 20px;
				font-weight: bold;}
	.derecha_caja{ float: left;display: block;
					min-height: 17px;
					margin-top: 30px;
					margin-bottom: 5px;
					padding-left: 0px;}
	.izquierda_caja{ width: 200px; float: left; margin-left: 30px;margin-bottom: 0px;}
	#table_datatable_control_wrapper{ clear: both;}
	#table_datatable_control_filter{position: absolute;
	right: 0px;
	top: -50px;
	background-color: transparent;
	border: medium none;}
	.termc{ width: auto;
	float: left;
	padding-top: 6px;}

	.termcb{ /*width: 335px*/;
	display: block;
	position: relative;
	}

	.termcb input{ padding: 7px; width: 250px; margin-right: 15px;}
	.trium{}

	.oculto{display: none;}
	.derecha_caja{ position: relative;}
	.ntermform{ 
		    position: absolute;
		    width: 200px;
		    height: 188px;
		    display: block;
		    background-color: #FFF;
		    z-index: 99;
		    border: 1px solid gray;
		    padding: 10px;
		    border-radius: 5px;
		    box-shadow: 5px 5px 17px;
		    left: 70px;
		    top: -20px;
		    display: none;
		}
	#btn_ntermino{ float: right;}
	.derecha_caja{ display: none;}
	.cerrarmodalterm{ position: absolute;
				right: 5px;
				top: 2px;
				font-weight: bold;
				cursor: pointer;}
	.editar_termino{ float: right;}
	.eliminar_termino{ float: right;}
	.controlterm{background-color: #C8D4DE;}
	.imgwidcontrol{ width: 30px; margin: auto; display: block;}
	.btncontrolsn {display: block; overflow: auto; width: 33px;}


	.selecpag{background-color: #F5E4D4 !important; border: 1px solid #D17F15 !important;}
	.ttinfo{margin-left: 15px;
			font-size: 16px;
			margin-top: 21px;
			margin-bottom: 14px;}
	.inputre{ padding: 7px !important;}
	.curve_chart{margin-left: -15px;}
	.opctpags{ cursor: pointer;}
	#contenedor_combotpags{ margin-left: 10px; margin-right: 10px;}
	.maswid{ width: 65px;}
	.caja_comparador{ width: auto; position: absolute; top: 5px; right: 5px;}
	.titconsultas { text-align: center;
					display: block;
					color: #969696;
					font-size: 18px;
					margin: auto;}
	.rateseo{ display: block;
			color: #9F9F9F;
			font-size: 12px;
			margin-bottom: 15px;
			margin-top: -5px;}
	.caja_comparadorb{ max-width: 69px;
					width: auto;
					display: block;
					margin: auto;
				overflow: hidden;
				margin-top: 6px;}
	.titconsultasb { text-align: center;
					display: block;
					color: #969696;
					font-size: 18px;
					margin: auto;
					margin-bottom: 15px; }
	.doble{ width: 22%; float: left;}
	.mono{ width: 12%; float: left; }
	.titfil{ float: left;text-align: center;
					display: block;
					color: #969696;
					font-size: 18px;;
					margin: 5px 5px auto auto;}
	.seo_extras_fill{float: left;color: #6C6C6C; text-align: center; font-size: 25px;  font-weight: bold;}
	.caja_comparador_fill{ display: block; float: left; }
	.col-sm-3,.col-sm-5{ padding-left: 10px; padding-right: 10px;}
	.tpags{ display: block; margin-top: 5px;}
	.titpant{ display: block; width: 50%; float: left;}
	.titpantb{ padding-left: 8%}
	.cont_titpant{  overflow: hidden;
			    float: left;
			    width: 380px;
			    margin-left: 10px;
			}
	.icotitot{
				width: 30px;
				float: left;
				margin-left: 5px;
				margin-right: 5px;
				margin-top: 8px;
				}
	.tret{
		margin-top: 5px;
		margin-left: 10px;
	}
</style>

<div class="main-content">

<?php
include("../includes/breadcrumb.php")
?>

<br />


<div class="row">

	
	<div class="col-sm-3">
	
		<div class="tile-stats tile-white-gray" id="total_contenido_idioma" style="text-align: center;">	

		</div>

	</div>
	
	<div class="col-sm-9">
	
		<div class="tile-stats tile-white-gray" id="total_contenido_info">
			
		</div>
		
	</div>
	
</div>

<br />

<script src="<?=RUTA_ABSOLUTA?>js/seo_scripts.js"></script>
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/datatable-tools.css">
<link rel="stylesheet" type="text/css" href="<?=RUTA_ABSOLUTA?>css/estilos_metricas_habituales.css">

<? include("../includes/footer.php"); } ?>



<?php

include("../includes/conf.php"); 
include("../informes/funciones_ajax.php"); 

$modo = $_POST["modo"];
$strProyectoAsociado = $_POST["proyectosasoc"];
$idVistaAnalytics = $_POST["vista"];
$datFechaInicioFiltro = $_POST["fechaini"];
$datFechaFinFiltro = $_POST["fechafin"];
$paisGa = $_POST["filtro_pais"];
$idIdioma = $_POST["filtro_idioma"];
$mes_combo = $_POST["mes_combo"];
$url_vista = $_POST["url_vista"];

$datFechaFinFiltro = date ('Y-m-d' , strtotime('-2 days', strtotime(date('Y-m-01'))) );
$datFechaInicioFiltro = date ('Y-m-d' , strtotime ( '-1 year' , strtotime ( $datFechaFinFiltro ) ) );



function getUltimoDiaMes($elAnio,$elMes) {
  return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
}

//modo 1
if($modo==1){

	
	?>
	<h2 class="restit"><?php $trans->__('Media de páginas indexadas del periodo en Google'); ?></h2>
	<?php
	$pagstotal = pagsIndexadas($idVistaAnalytics,$datFechaInicioFiltro,$datFechaFinFiltro,'media');
	$pasporapi = resumen_indexacion_com ($strProyectoAsociado, $idVistaAnalytics, $datFechaInicioFiltro, $datFechaFinFiltro, $paisGa );
	?>
	<span class="spanadet"><span class="res_titindex"><?=number_format($pagstotal,0,",",".")?></span><br><span class="res_descindex"> <?php $trans->__('páginas indexadas');?></span></span><br>
	<?php
	if($pagstotal>$pasporapi){
		//bien
		?><span class="descimg"><i class="fa fa-thumbs-o-up tamico"></i> <?php $trans->__('Estado óptimo de indexación'); ?></span><?php
	}else{
		//mal
		?><span class="descimg"><i class="fa fa-warning tamico"></i> <?php $trans->__('Revisar indexación'); ?></span><?php
	}
//modo 2		
}else if($modo==2){
	?>
	<h2 class="restit"><?php $trans->__('Evolución del último año'); ?></h2>
	<div id="curve_chart" style="margin: auto; display: block; max-width: 637px;" >
					
	</div>
	<!--<p class="fechas_index"><span class="fecha1 ftm"></span><span class="fecha2 ftm"></span><span  class="fecha3 ftm"></span></p>-->
	<script type="text/javascript">

		function lineChart_seo() {
		     var data = google.visualization.arrayToDataTable([
		     	[trans.__('Fecha'), trans.__('Páginas indexadas')],
		     	<?php

					$fecha1 = "";
					$fecha2 = "";
					$fecha3 = "";

					$fechaCom = $datFechaInicioFiltro;
					$fechaCom_manno = strtotime ( '-1 year' , strtotime ( $fechaCom ) ) ; 
					$fechaCom_manno = date ( 'Y-m' , $fechaCom_manno );
					$fechaCom_ano = date("Y", strtotime($fechaCom_manno)); 
					$fechaCom_mes = date("m", strtotime($fechaCom_manno)); 
					$fechaCom_dia = date("d", strtotime($fechaCom_manno)); 
					//Aqui tenemos la fecha del año anterior
					//Recorremos 12 veces hasta llegar la fecha inicial y en la 6 pintamos cabecera
					$fechaCom_manno = $fechaCom_ano."-".$fechaCom_mes."-01";
					$mes_format = date("m", strtotime($fechaCom_manno)); 
					$mes_trac = traducirMes($mes_format);
					$nfecha = $fechaCom_ano."-".$mes_trac[intval($mes_format)]["nom"];
					$nfechab =  ucwords($mes_trac[intval($mes_format)]["nom"])." ".$fechaCom_ano;
					$nfechab =  $trans->__(ucwords($mes_trac[intval($mes_format)]["res"]),false)." ".$fechaCom_ano;
					$fechacomp_ini = $fechaCom_ano."-".$fechaCom_mes."-01";
					$diasMes = cal_days_in_month(CAL_GREGORIAN, $fechaCom_mes, $fechaCom_ano);
					$fechacomp_fin = $fechaCom_ano."-".$fechaCom_mes."-".$diasMes;
					//parche
					$fecha1 = $nfechab;
					//$nfecha = "";
					$linechartData = "['".$nfechab ."',".pagsIndexadasMes($idVistaAnalytics,$fechacomp_ini,$fechacomp_fin)."],";
					
					$count = 0;
					while ($count < 11) {

						$fechaCom_mes++;
						if($fechaCom_mes > 12){
							$fechaCom_mes = 1;
							$fechaCom_ano++;
						}
						$nfecha = $fechaCom_ano."-".$fechaCom_mes."-01";
						$mes_format = date("m", strtotime($nfecha)); 
						$mes_trac = traducirMes($mes_format);
						$nfecha = $fechaCom_ano."-".$mes_trac[intval($mes_format)]["nom"];
						if($mes_trac[intval($mes_format)]["res"] == "Ene"){
							$nfecha = $trans->__($mes_trac[intval($mes_format)]["res"], false)." ".$fechaCom_ano;
						}else{
							$nfecha = $trans->__($mes_trac[intval($mes_format)]["res"], false);
						}
						
						$nfechab =  $trans->__(ucwords($mes_trac[intval($mes_format)]["nom"]), false)." ".$fechaCom_ano;
						$fechacomp_ini = $fechaCom_ano."-".$fechaCom_mes."-01";
						$diasMes = cal_days_in_month(CAL_GREGORIAN, $fechaCom_mes, $fechaCom_ano);
						$fechacomp_fin = $fechaCom_ano."-".$fechaCom_mes."-".$diasMes;
						if($count == 5){
							//parche
							$fecha2 = $nfechab;
							//$nfecha = "";
							$linechartData .= "['".$nfecha."',".pagsIndexadasMes($idVistaAnalytics,$fechacomp_ini,$fechacomp_fin)."],";
						}else{
							$linechartData .= "['".$nfecha."',".pagsIndexadasMes($idVistaAnalytics,$fechacomp_ini,$fechacomp_fin)."],";
						}
						
						
						$count++;

					}

					$fechaCom_mes++;
					if($fechaCom_mes > 12){
						$fechaCom_mes = 1;
						$fechaCom_ano++;
					}
					$nfecha = $fechaCom_ano."-".$fechaCom_mes."-01";
					$mes_format = date("m", strtotime($nfecha)); 
					$mes_trac = traducirMes($mes_format);
					$nfecha = $fechaCom_ano."-".$mes_trac[intval($mes_format)]["nom"];
					if($mes_trac[intval($mes_format)]["res"] == "Ene"){
						$nfecha = $mes_trac[intval($mes_format)]["res"]." ".$fechaCom_ano;
					}else{
						$nfecha = $mes_trac[intval($mes_format)]["res"];
					}
					//$nfecha = $mes_trac[intval($mes_format)]["res"]." ".$fechaCom_ano;
					$nfechab =  ucwords($mes_trac[intval($mes_format)]["nom"])." ".$fechaCom_ano;
					$fechacomp_ini = $fechaCom_ano."-".$fechaCom_mes."-01";
					$diasMes = cal_days_in_month(CAL_GREGORIAN, $fechaCom_mes, $fechaCom_ano);
					$fechacomp_fin = $fechaCom_ano."-".$fechaCom_mes."-".$diasMes;
					$fechacomp_ini = $fechaCom_ano."-".$fechaCom_mes."-01";
					$fechacomp_fin = $fechaCom_ano."-".$fechaCom_mes."-".$diasMes;

					//parche
					$fecha3 = $nfechab;
					//$nfecha = "";
					$linechartData .= "['".$nfecha."',".pagsIndexadasMes($idVistaAnalytics,$fechacomp_ini,$fechacomp_fin)."]";

					echo $linechartData;


				?>
		     ]);

		     var options = {
		       title: '',
		       width: 700,
		       legend: 'none',
		       backgroundColor: 'transparent',
		       colors:['#ff6867']
		     };

		     var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

		     chart.draw(data, options);

		     //Pintamos las fecha
		     $(".fecha1").text("<?=$fecha1?>");
		     $(".fecha2").text("<?=$fecha2?>");
		     $(".fecha3").text("<?=$fecha3?>");


		   
	}
</script>

<?php

}else if($modo==3){

	if (!empty($mes_combo)) {
		
		$tipo = $_POST["tipo"];
		//Devuelvo un ajax con los totales 
		$mesSelect = $mes_combo;
		$mesSelect_anno = $mesSelect[0].$mesSelect[1].$mesSelect[2].$mesSelect[3];
		$mesSelect_mes  = $mesSelect[4].$mesSelect[5];
		//Restamos un mes
		//Primero comprobamos si es 01 si es asi restamos un año y el mes 12
		if($mesSelect_mes == 01){
		  $mesSelect_anno_ant = intval($mesSelect_anno) -1;
		  $mesSelect_mes_ant = 12;
		}else{
		  $mesSelect_anno_ant = $mesSelect_anno;
		  $mesSelect_mes_ant = intval($mesSelect_mes) -1;
		  if($mesSelect_mes_ant < 10){
		    $mesSelect_mes_ant = '0'.$mesSelect_mes_ant;
		  }
		}
		$mesSelect_ant = $mesSelect_anno_ant.$mesSelect_mes_ant;
		//Comprobamos si existe ese mes en los terminos
		$sw=0;
		$meses = comboSeo($url_vista,$tipo);
		if($meses != 0){
			foreach ($meses as $key => $mes) {
			  if($mes == $mesSelect_ant){
			    $sw=1;
			  }
			}
		}
		if ($tipo == "term") {
			$resultadoFinal = SeoExtras_term($url_vista,$mesSelect,$sw,$_POST["control"]);
		}else{
			$resultadoFinal = SeoExtras_pags($url_vista,$mesSelect,$sw,$_POST["control"]);
		}
		
		if ($sw==1) {
				if ($tipo == "term") {
					$resultadoAuxiliar .= SeoExtras_term($url_vista,$mesSelect_ant,$sw,$_POST["control"]);
				}else{
					$resultadoAuxiliar .= SeoExtras_pags($url_vista,$mesSelect_ant,$sw,$_POST["control"]);
				}
				$resultadoFinal .= ',' . $resultadoAuxiliar;
			}
		print ('{"datos":['.$resultadoFinal.']}');
		
	}

}else if($modo==4){
	//Devuelvo un ajax con los totales 
	if( IsAdmin($idVistaAnalytics,$_COOKIE["usuario"]["email"]) ){
		echo 1;
	}else{
		echo 0;
	}
	
}else if($modo==5){
	//Devuelvo un ajax con los totales 
	if( guardarTermino($_POST["terminos"],$_POST["categoria"],$url_vista,$_POST["tipo"]) ){
		echo 1;
	}else{
		echo 0;
	}
	
}else if($modo==6){
	//Devuelvo un ajax con los totales 
	if( eliminarControl($_POST["terminos"],$url_vista,$_POST["tipo"]) ){
		echo 1;
	}else{
		echo 0;
	}
}else if($modo==7){
	//Devuelvo un ajax con los totales 
	if( anadirControl($_POST["terminos"],$url_vista,$_POST["tipo"]) ){
		echo 1;
	}else{
		echo 0;
	}
}else if($modo==8){
	//Devuelvo un ajax con los totales 
	if( editarTermino($_POST["terminos"],$_POST["terminosant"],$url_vista,$_POST["mes"],$_POST["cat"],$_POST["tipo"]) ){
		echo 1;
	}else{
		echo 0;
	}
}else if($modo==9){

	//Devuelvo un ajax con los totales 
	$cat_seo = categorias_seo($url_vista,$_POST["tipo"]);
	if ($cat_seo["cantidad"] == 0) {
		?><li>No hay ninguna categoría</li><?php
	}else{
		foreach ($cat_seo["elementos"] as $key => $cs) {
		?>
		<li><a name="<?=$cs['categoria']?>" class="linkselectcat"><?=$cs['categoria']?>(<?=$cs['contador']?>)</a></li>
		<?php
		}
		?>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".linkselectcat").on("click",function(){
					var cat = $(this).attr("name");
					$("#txt_categoria").val(cat);
				})
			})
		</script>
		<?php
	}
	
}

?>
<?php include("../includes/conf.php");

$urlVista = $_GET["urlVista"];
$tipo = $_GET["tipo"];

?>


<div class="btn-group">

	<?php

	$opciones = comboSeo($urlVista,$tipo);
	$sw=0;
	if($opciones == 0){
		?>
		<button type="button" class="btn btn-default troy">No se encuentra ningún dato</button>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		  <span class="caret"></span>
		  <span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu" role="menu">
    		<li><a href="#" style="padding:5px">No se encuentra ningún dato</a></li>
		<?php
	}else{

		foreach ($opciones as $key => $opc) {

			$opcsin = $opc;
			//Transformamos el formato actual al nuevo de fecha
			$anno="";
			$mes="";
			for ($i=0; $i < 4; $i++) {
				$anno .= $opc[$i];
			}
			for ($i=4; $i < 6; $i++) {
				$mes .= $opc[$i];
			}

			$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
			$opccon = vsprintf($trans->__('%1$s del %2$s', false), array($trans->__($meses[intval($mes-1)], false), $anno));
			
			if($sw==0){
				?>
				<button type="button" class="btn btn-default troy"><?=$opccon?></button>
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				  <span class="caret"></span>
				  <span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php
				$opcPri = $opcsin;
			}

			?><li><a name="<?=$opcsin?>" class="opcmeses"><?=$opccon?></a></li><?php

			$sw++;
		}

	}



	?>
  </ul>
</div>
<input type="hidden" value="<?=$opcPri?>" id="dat_seo_meses">
<!--</select>-->
<script type="text/javascript">
$(document).ready(function(){

	$(".opcmeses").on("click",function(){
		var valor = $(this).attr("name");
		if($("#dat_seo_meses").val() != valor){
			$("#dat_seo_meses").val(valor);
			$(".troy").html($(this).html());
			cargador_cambiomes();
		}
	});

})//ready
</script>	


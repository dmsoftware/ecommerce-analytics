<?php include("includes/conf.php");
	  include("informes/funciones_ajax.php");


if(isset($_POST["filtroper"]) && isset($_POST["filtroperdesc"])){

		//Al no ser la primera vez que entra soo recojemos los dos datos traidos y guardarmlos en sesión
		setcookie( "filtroper", $_POST["filtroper"], time() + (86400), "/"); //86400 es un dia
		setcookie( "filtroperdesc", $_POST["filtroperdesc"], time() + (86400), "/"); //86400 es un dia
		echo 'ok';

}else{ 

		if( isset($_GET["idvista"]) ){
		
			if(isset($_COOKIE["usuario"])) {
				//Si existe vista y está logeado cargamos el combo
				$filtros = comprobar_filtro_usuario($_GET["idvista"],$_COOKIE["usuario"]["email"]);
				if ($filtros["cantidad"] == 0){
					//No existen filtros sobre ese usuario en esa vista
					?>
					<script type="text/javascript">
						$(document).ready(function(){
							$("#contenedor_filtro").hide();
							$("#caja_br_filtro").hide();
						})
					</script>
					<?php			
				}else{
					?>
					<script type="text/javascript">
						$(document).ready(function(){
							$("#contenedor_filtro").fadeIn(600);
							$("#caja_br_filtro").fadeIn(600);
						})
					</script>
				<?php
					
					foreach ($filtros["elementos"] as $key => $filt){
						$filid = str_replace("==","**",$filt["filtro"]);
						?><option value="<?=$filid?>#<?=$filt["desc"]?>"><?=$trans->__($filt["desc"])?></option><?php	
					}
				}


			}else{
				//Si no existe usuario no esta logeado
			}

		}else{
			//Si es la primara vez que entra y no se le entrega una vista no se puede saber los filtros
		}
		

}

?>